<?php
/**
 * Manage chado in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_chado_form($form, &$form_state, $op = 'view', $cv_id = NULL) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Adds the instruction.
  $form['manage'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Manage Chado data in BIMS',
  );
  $form['manage']['desc'] = array(
    '#markup' => t('Manage the data in Chado schema in BIMS.'),
  );

  // Chado cv.
  _bims_admin_chado_cv($form['manage'], $op);

  // Chado project.
  _bims_admin_chado_project($form['manage'], $op);

  // BIMS imported projects.
  if ($op == 'view') {
    $form['manage']['bims_imported'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'BIMS imported projects',
    );
    _bims_admin_bims_imported($form['manage']['bims_imported']);
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-chado-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Adds Chado cv.
 *
 * @param array $form
 * @param string $op
 */
function _bims_admin_chado_cv(&$form, $op = 'view') {

  // OP : View.
  if ($op == 'view') {

    // List of CVs
    $form['cv'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Chado cv data',
    );

    // Adds 'add a CV' link.
    $form['cv']['link_add_cv']= array(
      '#markup' => '<div style="margin-top:15px;margin-bottom:15px;"><b>' . l('Add a CV', 'admin/bims/chado/add_cv') . '</b></div>',
    );

    // Lists all cv in BIMS.
    $sql = "
      SELECT CC.*, CV.cv_id AS chado_cv_id, C.name AS crop_name
      FROM {bims_chado_cv} CC
        INNER JOIN {bims_crop} C on C.crop_id = CC.crop_id
        LEFT JOIN {chado.cv} CV on CV.cv_id = CC.cv_id
      ORDER BY CC.cv_name
    ";
    $results = db_query($sql);
    $rows = array();
    while ($obj = $results->fetchObject()) {
      $exist = $obj->chado_cv_id ? 'YES' : 'NO';
      $rows []= array(
        $obj->cv_id,
        $obj->cv_name,
        $obj->crop_name,
        $obj->type,
        $exist,
      );
    }

    // Sets the header.
    $header = array(
      array('data' => 'CV&nbsp;ID', 'width' => '30'),
      array('data' => 'CV&nbsp;Name', 'width' => '10'),
      array('data' => 'Crop', 'width' => '10'),
      array('data' => 'Project&nbsp;Type', 'width' => '10'),
      array('data' => 'Exists', 'width' => '10'),
    );

    // Generates the table.
    $table_vars = array(
      'header'      => $header,
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%;'),
      'sticky'      => TRUE,
      'empty'       => '<em>No cv has been added to BIMS</em>',
      'param'       => array(),
    );
    $prefix = '<div><b>bims_chado_cv table</b></div>';
    $form['cv']['bims_chado_cv']['table'] = array(
      '#markup' => $prefix . bims_theme_table($table_vars),
    );
  }

  // Adds cv to BIMS.
  else if ($op == 'add_cv') {

    // CV properties.
    $form['cv'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'BIMS Chado cv properties',
    );
    $form['cv']['bims_chado_cv']['cv_name'] = array(
      '#type'               => 'textfield',
      '#title'              => t('CV Name'),
      '#description'        => t("Please type the name of CV to be added to BIMS"),
      '#attributes'         => array('style' => 'width:250px;'),
      '#autocomplete_path'  => "bims/bims_autocomplete_cv",
    );
    $opt_crops = BIMS_CROP::getCrops(BIMS_OPTION);
    $form['cv']['bims_chado_cv']['crop'] = array(
      '#type'         => 'select',
      '#options'       => $opt_crops,
      '#title'        => t('Crop'),
      '#description'  => t("Please choose a crop for the selected CV"),
      '#attributes'   => array('style' => 'width:250px;'),
    );
    $form['cv']['bims_chado_cv']['add_cv_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'add_cv_btn',
      '#value'      => 'Add',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_admin_chado_form_ajax_callback',
        'wrapper'  => 'bims-admin-chado-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
}

/**
 * Adds Chado project.
 *
 * @param array $form
 * @param string $op
 */
function _bims_admin_chado_project(&$form, $op = 'view') {

  // OP : View.
  if ($op == 'view') {

    // List of the Projects.
    $form['project'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Chado project data',
    );

    // Lists phenotype projects in BIMS.
    $sql = "
      SELECT CP.*, P.project_id AS chado_project_id
      FROM {bims_chado_project} CP
        LEFT JOIN {chado.project} P on P.project_id = CP.project_id
      WHERE CP.type = :type
      ORDER BY CP.type, CP.project_name
    ";
    $args = array(':type' => 'phenotype');
    $results = db_query($sql, $args);
    $data = array();
    while ($obj = $results->fetchObject()) {
      $cv_id    = $obj->cv_id;
      $cv_name  = $obj->cv_name;

      // Add a new CV.
      if (!array_key_exists($cv_id, $data)) {
        $data[$cv_id] = array(
          'cv_name'   => $obj->cv_name,
          'type'      => $obj->type,
          'projects'  => array(),
        );
      }
      $data[$cv_id]['projects'] []= sprintf("[%d] %s", $obj->project_id, $obj->project_name);
    }
    $rows = array();
    foreach ((array)$data as $cv_id => $props) {
      $project_list = '<em>N/A</em>';
      if (!empty($props['projects'])) {
        $project_list = '<textarea style="min-height:50px;max-height:100px;width:400px;">' . implode("\n", $props['projects']) . '</textarea>';
      }
      $rows []= array(
        $cv_id,
        $props['cv_name'],
        $props['type'],
        $project_list,
      );
    }

    // Sets the header.
    $header = array(
      array('data' => 'CV&nbsp;ID', 'width' => '30'),
      array('data' => 'CV&nbsp;Name', 'width' => '30'),
      array('data' => 'Project&nbsp;Type', 'width' => '30'),
      array('data' => 'Projects', '300'),
    );

    // Generates the table.
    $table_vars = array(
      'header'      => $header,
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%;'),
      'sticky'      => TRUE,
      'empty'       => "<em>No phenotype project has been added to BIMS</em>",
      'param'       => array(),
    );

    // Adds a link to phenotype project.
    $link = l('Add a phenotype project', 'admin/bims/chado/add_phenotype_proj');
    $desc = "<div style='margin-top:15px;margin-bottom:15px;'><b>$link</b></div>";
    $form['project']['phenotype-table'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Phenotype Projects',
      '#description'  => $desc,
    );
    $form['project']['phenotype-table']['contents'] = array(
      '#markup' => "<div style='max-height:300px;overflow-y: auto'>" . bims_theme_table($table_vars) . '</div>',
    );

    // Lists all project in BIMS by type.
    $types = array(
      'genotype'  => 'Genotype',
      'cross'     => 'Cross',
    );
    foreach ($types as $type => $label) {
      $sql = "
        SELECT CP.*, P.project_id AS chado_project_id, BC.name AS crop
        FROM {bims_chado_project} CP
          LEFT JOIN {chado.project} P on P.project_id = CP.project_id
          LEFT JOIN {bims_crop} BC on BC.crop_id = CP.crop_id
        WHERE CP.type = :type
        ORDER BY BC.name, CP.type, CP.project_name
      ";
      $args = array(':type' => $type);
      $results = db_query($sql, $args);
      $rows = array();
      while ($obj = $results->fetchObject()) {
        $exist = $obj->chado_project_id ? 'YES' : 'NO';

        // Creates a delete link.
        $delete_link = '<em>N/A</em>';
        if ($exist == 'YES') {
          $delete_link = '<em>In Use</em>';
          if (BIMS_IMPORTED_PROJECT::canDeleteProject($obj->chado_project_id)) {
            $confirm_attr = array(
              'attributes' => array(
                'id' => 'delete-chado-project-' . $obj->chado_project_id,
                'class' => array('use-ajax','bims-confirm'),
              ),
            );
            $delete_link = l('Delete', 'bims/ajax_delete_chado_project/' . $obj->chado_project_id, $confirm_attr);
          }
        }

        // Adds a row.
        if ($type == 'genotype') {
          $rows []= array(
            $obj->project_id,
            $obj->type,
            $obj->sub_type,
            $obj->project_name,
            $obj->crop,
            $exist,
            $delete_link,
          );
        }
        else {
          $rows []= array(
            $obj->project_id,
            $obj->type,
            $obj->project_name,
            $obj->crop,
            $exist,
            $delete_link,
          );
        }
      }

      // Sets the header.
      $header = array();
      if ($type == 'genotype') {
        $header = array(
          array('data' => 'Project ID', 'width' => '20'),
          array('data' => 'Type', 'width' => '10'),
          array('data' => 'Sub Type', 'width' => '10'),
          array('data' => 'Project Name', 'width' => '10'),
          array('data' => 'Crop', 'width' => '10'),
          array('data' => 'Exists', 'width' => '10'),
          array('data' => 'Delete', 'width' => '10'),
        );
      }
      else {
        $header = array(
          array('data' => 'Project ID', 'width' => '20'),
          array('data' => 'Type', 'width' => '10'),
          array('data' => 'Project Name', 'width' => '10'),
          array('data' => 'Crop', 'width' => '10'),
          array('data' => 'Exists', 'width' => '10'),
          array('data' => 'Delete', 'width' => '10'),
        );
      }

      // Generates the table.
      $table_vars = array(
        'header'      => $header,
        'rows'        => $rows,
        'attributes'  => array('style' => 'width:100%;'),
        'sticky'      => TRUE,
        'empty'       => "<em>No $type project has been added to BIMS</em>",
        'param'       => array(),
      );
      $url = 'admin/bims/chado/add_' . $type . '_proj';
      $link = l("Add a $type project", $url);
      $desc = "<div style='margin-top:15px;margin-bottom:15px;'><b>$link</b></div>";
      $form['project'][$type . '-table'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => TRUE,
        '#collapsible'  => TRUE,
        '#title'        => "$label Projects",
        '#description'  => $desc,
      );
      $form['project'][$type . '-table']['contents'] = array(
        '#markup' => "<div style='max-height:300px;overflow-y: auto'>" . bims_theme_table($table_vars) . '</div>',
      );
    }
  }

  // OP: Add a phenotype project.
  if ($op == 'add_phenotype_proj') {

    // Project properties.
    $form['project'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Chado project data',
    );

    // Adds project to BIMS.
    $form['project']['project_name'] = array(
      '#type'               => 'textfield',
      '#title'              => t('Add Project by Project Name'),
      '#description'        => t("Please type the name of project to be added to BIMS"),
      '#attributes'         => array('style' => 'width:400px;'),
      '#autocomplete_path'  => "bims/bims_autocomplete_project/phenotype",
    );
    $form['project']['cv_name'] = array(
      '#type'               => 'textfield',
      '#title'              => t('Add Project by CV'),
      '#description'        => t("Please type the name of CV of the projects to be added to BIMS"),
      '#attributes'         => array('style' => 'width:200px;'),
      '#autocomplete_path'  => "bims/bims_autocomplete_avail_cv",
    );
    $form['project']['add_phenotype_proj_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'add_phenotype_proj_btn',
      '#value'      => 'Add',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_admin_chado_form_ajax_callback',
        'wrapper'  => 'bims-admin-chado-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // OP: Add a project (non-phenotype).
  if (preg_match("/add_(cross|genotype)_proj/", $op, $matches)) {
    $type = $matches[1];
    $form['project']['project_name'] = array(
      '#type'               => 'textfield',
      '#title'              => t('Add Project by Project Name'),
      '#description'        => t("Please type the name of project to be added to BIMS"),
      '#attributes'         => array('style' => 'width:400px;'),
      '#autocomplete_path'  => "bims/bims_autocomplete_project/$type",
    );
    $opt_crops = BIMS_CROP::getCrops(BIMS_OPTION);
    $form['project']['crop'] = array(
      '#type'         => 'select',
      '#options'       => $opt_crops,
      '#title'        => t('Crop'),
      '#description'  => t("Please choose a crop for the selected project"),
      '#attributes'   => array('style' => 'width:200px;'),
    );
    $form['project']['add_proj_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'add_proj_btn',
      '#value'      => 'Add',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_admin_chado_form_ajax_callback',
        'wrapper'  => 'bims-admin-chado-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
}

/**
 * Adds BIMS imported projects.
 *
 * @param array $form
 */
function _bims_admin_bims_imported(&$form) {

  // Lists imported project.
  $sql = "
    SELECT I.*, P.project_id AS chado_project_id
    FROM {bims_imported_project} I
      LEFT JOIN {chado.project} P on P.project_id = I.project_id
    ORDER BY I.program_id, I.type, I.sub_type
  ";
  $rows = array();
  $results = db_query($sql);
  while ($obj = $results->fetchObject()) {
    $exist = $obj->chado_project_id ? 'YES' : 'NO';
    $rows []= array(
      $obj->project_id,
      $obj->program_id,
      $obj->type,
      $obj->sub_type,
      $obj->project_name,
      $exist,
      '<em>N/A</em>',
    );
  }

  // Sets the header.
  $header = array(
    array('data' => 'Project ID', 'width' => '20'),
    array('data' => 'Program ID', 'width' => '10'),
    array('data' => 'Type', 'width' => '10'),
    array('data' => 'Sub-Type', 'width' => '10'),
    array('data' => 'Label', 'width' => '10'),
    array('data' => 'Exists', 'width' => '10'),
    array('data' => 'Delete', 'width' => '10'),
  );

  // Generates the table.
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'empty'       => '<em>No project has been imported</em>',
    'param'       => array(),
  );
  $form['bims_imported_table'] = array(
    '#markup' => "<div style='max-height:300px;overflow-y: auto'>" . bims_theme_table($table_vars) . '</div>',
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_chado_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_chado_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' for cv was clicked.
  if ($trigger_elem == 'add_cv_btn') {

    // Gets and checks the cv name.
    $cv_name = trim($form_state['values']['manage']['cv']['bims_chado_cv']['cv_name']);
    $chado_cv = MCL_CHADO_CV::byName($cv_name);
    if (!$chado_cv) {
      form_set_error("manage][cv][bims_chado_cv][cv_name", "Error : CV ($cv_name) does not exist.");
      return;
    }
  }

  // If 'Add' for phenotyping project was clicked.
  else if ($trigger_elem == 'add_phenotype_proj_btn') {

    // Gets and checks the project name.
    $project_name = trim($form_state['values']['manage']['project']['project_name']);
    $cv_name      = trim($form_state['values']['manage']['project']['cv_name']);

    // Checks both project and cv.
    if ($project_name && $cv_name) {
      form_set_error("manage][cv][project_name", "Please type only project name or cv name.");
      return;
    }
    else if (!$project_name && !$cv_name) {
      form_set_error("manage][cv][project_name", "Please type project name or cv name.");
      return;
    }

    // Checks the project.
    else if ($project_name) {
      $chado_project = MCL_CHADO_DATASET::byName($project_name);
      if (!$chado_project) {
        form_set_error("manage][cv][project_name", "Error : The project ($project_name) does not exist.");
        return;
      }
    }

    // Checks the CV.
    else if ($cv_name) {
      $chado_cv = MCL_CHADO_CV::byName($cv_name);
      if (!$chado_cv) {
        form_set_error("manage][cv][cv_name", "Error : The project ($cv_name) does not exist.");
        return;
      }
    }
  }

  // If 'Add' for project was clicked.
  else if ($trigger_elem == 'add_proj_btn') {

    // Gets and checks the project.
    $project_name = trim($form_state['values']['manage']['project']['project_name']);
    $chado_project = MCL_CHADO_DATASET::byName($project_name);
    if (!$chado_project) {
      form_set_error("manage][cv][project_name", "Error : The project ($project_name) does not exist.");
      return;
    }

    // Checks for duplication.
    $bims_chado_project = PUBLIC_BIMS_CHADO_PROJECT::byKey(array('project_id' => $chado_project->getProjectID()));
    if ($bims_chado_project) {
      form_set_error("manage][cv][project_name", "Error : The project '$project_name' has already been added.");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_chado_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' for cv was clicked.
  if ($trigger_elem == 'add_cv_btn') {

    // Gets the cv name.
    $cv_name = trim($form_state['values']['manage']['cv']['bims_chado_cv']['cv_name']);
    $chado_cv = MCL_CHADO_CV::byName($cv_name);
    $cv_id = $chado_cv->getCvID();

    // Gets the crop ID.
    $crop_id = $form_state['values']['manage']['cv']['bims_chado_cv']['crop'];

    // Calls drush command to add CV.
    $drush = bims_get_config_setting('bims_drush_binary');
    $cmd = "$drush bims-add-bims-chado-cv $cv_id $crop_id > /dev/null 2>/dev/null  & echo $!";
    $pid = exec($cmd, $output, $return_var);
    if ($return_var) {
      drupal_set_message("Error : Failed to add '$cv_name' to BIMS.", 'error');
    }
    else {
      drupal_set_message("The cv '$cv_name' has been added to BIMS.");
    }
  }

  // If 'Add' for phenotyping project was clicked.
  else if ($trigger_elem == 'add_phenotype_proj_btn') {

    // Gets project properties.
    $project_name = trim($form_state['values']['manage']['project']['project_name']);
    $cv_name      = trim($form_state['values']['manage']['project']['cv_name']);

    // Adds a project by project name.
    if ($project_name) {
      $chado_project  = MCL_CHADO_DATASET::byName($project_name);
      $chado_cv       = $chado_project->getCv();
      $details = array(
        'project_id'    => $chado_project->getProjectID(),
        'project_name'  => $project_name,
        'type'          => $chado_project->getType(),
        'sub_type'      => $chado_project->getSubType(),
        'cv_id'         => $chado_cv->getCvID(),
        'cv_name'       => $chado_cv->getCvName(),
      );
      if (BIMS_IMPORTED_PROJECT::addProject($details)) {
        drupal_set_message("The project '$project_name' has been added to BIMS.");
      }
      else {
        drupal_set_message("Error : Failed to add '$prject_name' to BIMS.", 'error');
      }
    }

    // Adds a project by cv name.
    else if ($cv_name) {

      // Gets the CV.
      $chado_cv = MCL_CHADO_CV::byName($cv_name);
      if ($chado_cv) {

        // Gets the all phenotypic projects.
        $chado_projects = MCL_CHADO_DATASET::getDatasetByCV($cv_name);
        if (!empty($chado_projects)) {
          foreach ($chado_projects as $chado_project) {
            $project_name = $chado_project->getName();
            $details = array(
              'project_id'    => $chado_project->getProjectID(),
              'project_name'  => $project_name,
              'type'          => $chado_project->getType(),
              'sub_type'      => $chado_project->getSubType(),
              'cv_id'         => $chado_cv->getCvID(),
              'cv_name'       => $chado_cv->getName(),
            );
            if (BIMS_IMPORTED_PROJECT::addProject($details)) {
              drupal_set_message("The project '$project_name' has been added to BIMS.");
            }
            else {
              drupal_set_message("Error : Failed to add '$prject_name' to BIMS.", 'error');
            }
          }
        }
      }
    }
  }

  // If 'Add' for project was clicked.
  else if ($trigger_elem == 'add_proj_btn') {

    // Gets project properties.
    $project_name = trim($form_state['values']['manage']['project']['project_name']);
    $crop_id      = $form_state['values']['manage']['project']['crop'];

    // Imports the project.
    $chado_project = MCL_CHADO_DATASET::byName($project_name);
    $details = array(
      'project_id'    => $chado_project->getProjectID(),
      'project_name'  => $project_name,
      'type'          => $chado_project->getType(),
      'sub_type'      => $chado_project->getSubType(),
      'crop_id'       => $crop_id,
    );
    if (BIMS_IMPORTED_PROJECT::addProject($details)) {
      drupal_set_message("The project '$project_name' has been added to BIMS.");
    }
    else {
      drupal_set_message("Error : Failed to add '$prject_name' to BIMS.", 'error');
    }
  }
}
