<?php
/**
 * Manages Field Book in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_field_book_form($form, &$form_state) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Adds the instruction.
  $form['field_book'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Field Book configuration in BIMS',
  );
  $form['field_book']['desc'] = array(
    '#markup' => t(''),
  );

  // Adds 3 types of Field Book ontologeis in BIMS in vertical tabs.
  $form['field_tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  // BIMS_FIELD_BOOK.
  $param = array(
    'cv_name' => 'BIMS_FIELD_BOOK',
    'desc'    => '',
  );
  _bims_admin_field_book_form($form['field_tabs'], $param);

  // BIMS_FIELD_BOOK_FORMAT.
  $param = array(
    'cv_name' => 'BIMS_FIELD_BOOK_FORMAT',
    'desc'    => 'BIMS matches the formats of traits used in Field Book app. When a new trait format is added to Field Book app, use the form to add it.',
  );
  _bims_admin_field_book_form($form['field_tabs'], $param);

  // BIMS_FIELD_BOOK_FORMAT_PROP.
  $param = array(
    'cv_name' => 'BIMS_FIELD_BOOK_FORMAT_PROP',
    'desc'    => '',
  );
  _bims_admin_field_book_form($form['field_tabs'], $param);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-field-book-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Adds form elements for managing Field Book ontologies.
 *
 * @param array $form
 * @param array $param
 */
function _bims_admin_field_book_form(&$form, $param) {
  $max_size = 6;

  // Local variables.
  $cv_name    = $param['cv_name'];
  $desc       = $param['desc'];

  // Adds fieldset.
  $form[$cv_name] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#group'        => 'vertical_tabs',
    '#title'        => $cv_name,
  );
  $form[$cv_name]['desc'] = array('#markup' => $desc);
  $options = BIMS_FIELD_BOOK::getOntologies($cv_name, TRUE);
  $size = sizeof($options);
  $size = ($max_size < $size) ? $max_size : $size;
  $form[$cv_name]['ontology']= array(
    '#type'       => 'select',
    '#options'    => $options,
    '#size'       => $size,
    '#attributes' => array('style' => 'width:300px;background-color:#FFFFFF;'),
  );
  $form[$cv_name]['name'] = array(
    '#type'         => 'textfield',
    '#title'        => 'name',
    '#description'  => "Please type a new ontology",
    '#attributes'   => array('style' => 'width:250px;'),
  );
  $form[$cv_name]['description'] = array(
    '#type'       => 'textarea',
    '#suffix'     => '<br />',
    '#title'      => t('Please type the description'),
    '#rows'       => 5,
    '#attributes' => array('style' => 'width:400px;'),
  );

  $form[$cv_name]['add_btn']= array(
    '#type'       => 'submit',
    '#name'       => 'add_btn_' . $cv_name,
    '#value'      => t('Add'),
    '#attributes' => array('style' => 'width:200px;'),
    '#ajax'       => array(
      'callback' => 'bims_admin_field_book_form_ajax_callback',
      'wrapper'  => 'bims-admin-field-book-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_field_book_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_field_book_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Add was clicked.
  if (preg_match("/^add_btn_([A-Z_]+)$/", $trigger_elem, $matches)) {
    $cv_name = $matches[1];

    // Gets the name.
    $name = trim($form_state['values']['field_tabs'][$cv_name]['name']);

    // Checks a format name.
    if (!preg_match("/^[a-zA-z0-9_\-]+$/", $name)) {
      form_set_error("field_tabs][$cv_name][name", "Invalid name");
      return;
    }

    // Checks for a duplication.
    $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $name);
    if ($cvterm) {
      form_set_error("field_tabs][$cv_name][name", "$name for $cv_name has alredy existed.");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_field_book_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Add was clicked.
  if (preg_match("/^add_btn_([A-Z_]+)$/", $trigger_elem, $matches)) {
    $cv_name = $matches[1];

    // Gets the properties.
    $name = trim($form_state['values']['field_tabs'][$cv_name]['name']);
    $desc = trim($form_state['values']['field_tabs'][$cv_name]['description']);

    // Adds the cvterm.
    if (MCL_CHADO_CVTERM::addCvterm(NULL, 'SITE_DB', $cv_name, $name, $desc)) {
      drupal_set_message("$name has been added");
    }
    else {
      drupal_set_message("Error : Failed to add $name", 'error');
    }
  }
}