<?php
/**
 * Manage public descriptors in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_descriptor_form($form, &$form_state) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Lists the public descriptors.
  $form['manage'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Public Descriptors',
  );
  $form['manage']['desc'] = array(
    '#markup' => t('List the public descriptors.'),
  );




  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-descriptor-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Returns the table with the descriptors.
 *
 * @param integer $crop_id
 *
 * @return string
 */
function _bims_admin_descriptor_form_descriptor_table($crop_id) {





  return 'table';
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_descriptor_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_descriptor_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_descriptor_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

}