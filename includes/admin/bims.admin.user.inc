<?php

/**
 * Manage BIMS users.
 *
 * @ingroup bims_admin
 */
function bims_admin_user_form() {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Create form.
  $form = array();
  $form['#tree'] = TRUE;

  // Add the instruction for BIMS user section.
  $form['users'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'BIMS Users',
    '#description'  => t('Manage BIMS users.') . '<br /><br />',
  );

  // Add the "Breeders" section.
  $form['breeders'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Breeders',
    '#description'  => t('Set "BREEDERS" to a user. A user with "BREEDER" permission can create a breeding program.') . '<br /><br />',
  );

  // Show all current breeders.
  $form['breeders']['breeder_list'] = array(
    '#type'       => 'select',
    '#title'      => t('Current Breeders'),
    '#options'    => _get_opt_breeder_list(),
    '#validated'  => TRUE,
    '#size'       => 5,
    '#attributes' => array('style' => 'width:200px;'),
  );

  // Gets all non-breeders.
  $form['breeders']['non_breeder_list'] = array(
    '#type'       => 'select',
    '#title'      => t('Select a user to add "BREEDER" permission'),
    '#options'    => _get_opt_non_beeders(),
    '#validated'  => TRUE,
    '#attributes' => array('style' => 'width:200px;'),
  );
  $form['breeders']['add_permission_btn'] = array(
    '#type'   => 'submit',
    '#name'   => 'add_permission_btn',
    '#value'  => 'Add permission',
    '#ajax'   => array(
      'callback' => "bims_admin_user_form_ajax_callback",
      'wrapper'  => 'bims-admin-user-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['breeders']['remove_permission_btn'] = array(
    '#type'   => 'submit',
    '#name'   => 'remove_permission_btn',
    '#value'  => 'Remove permission',
    '#ajax'   => array(
      'callback' => "bims_admin_user_form_ajax_callback",
      'wrapper'  => 'bims-admin-user-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Links between breeder (druapal user) to chado contact.
  $form['contact'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Contact',
    '#description'  => 'Links between "<b>Breeder</b>" (public.users) and "<b>Contact</b>" (chado.contact)<br /><br />',
  );

  // Show all current breeders.
  $form['contact']['breeder_list'] = array(
    '#type'       => 'select',
    '#title'      => t('Current breeders in BIMS'),
    '#options'    => _get_opt_breeder_list(),
    '#validated'  => TRUE,
    '#size'       => 5,
    '#attributes' => array('style' => 'width:200px;'),
  );

  // Show all contacts.
  $form['contact']['contact_list'] = array(
      '#type'       => 'select',
      '#title'      => t('Current contacts in Chado'),
      '#options'    => _get_opt_contact_list(),
      '#validated'  => TRUE,
      '#size'       => 5,
      '#attributes' => array('style' => 'width:200px;'),
  );
  $form['contact']['link_btn'] = array(
    '#type'   => 'submit',
    '#name'   => 'link_btn',
    '#value'  => 'Link breeder and contact',
    '#attributes' => array('style' => 'width:250px;'),
    '#ajax'   => array(
      'callback' => "bims_admin_user_form_ajax_callback",
      'wrapper'  => 'bims-admin-user-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['contact']['list'] = array(
    '#markup' => _bims_get_contact_list(),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-user-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Returns a list of breeders.
 *
 * @return array
 */
function _bims_get_contact_list() {

  // Gets all breeders.
  $breeders = BIMS_USER::getBreeders();
  $rows = array();
  foreach ($breeders as $breeder) {

    $contact_id = $breeder->getContactID();
    $contact = '<em>N/A</em>';
    if ($contact_id) {
      $contact = MCL_CHADO_CONTACT::byKey(array('contact_id' => $contact_id))->getName();
    }
    else {
      $contact_id = '<em>N/A</em>';
    }

    // Adds a user.
    $row = array(
      $breeder->getName(),
      $breeder->getUserID(),
      $contact,
      $contact_id,
    );
    $rows[] = $row;
  }

  // Sets the headers.
  $headers = array(
    array('data' => 'Breeder Name', 'style' => 'width:15%'),
    array('data' => 'user_id (Drupal)', 'style' => 'width:10%'),
    array('data' => 'Contact Name (Chado)', 'style' => 'width:10%'),
    array('data' => 'contact_id', 'style' => 'width:10%'),
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Returns a list of breeders.
 *
 * @return array
 */
function _get_opt_breeder_list() {
  $breeders = array();
  $users = BIMS_USER::getUserObjects('breeder');
  foreach ($users as $user) {
    $breeders[$user->uid] = $user->name;
  }
  return $breeders;
}

/**
 * Returns a list of contact.
 *
 * @return array
 */
function _get_opt_contact_list() {
  $sql = "
    SELECT C.name, C.contact_id
    FROM {chado.contact} C
    ORDER BY C.name
  ";
  $results = db_query($sql);
  $contacts = array('NULL' => '-- Unlink --');
  while ($obj = $results->fetchObject()) {
    $contacts[$obj->contact_id] = $obj->name;
  }
  return $contacts;
}

/**
 * Returns a list of non-breeders.
 *
 * @return array
 */
function _get_opt_non_beeders() {
  $non_breeders = array();
  $users = BIMS_USER::getUserObjects('non-breeder');
  foreach ($users as $user) {
    $non_breeders[$user->uid] = $user->name;
  }
  return $non_breeders;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_user_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add" was clicked.
  if ($trigger_elem == "add_permission_btn") {

    // Add 'Breeder' permssion.
    $user_id = $form_state['values']['breeders']['non_breeder_list'];
    $bims_user = BIMS_USER::addUser($user_id);
    $bims_user->makeBreeder();

    // Updates DDLs.
    $form['breeders']['breeder_list']['#options'] = _get_opt_breeder_list();
    $form['breeders']['non_breeder_list']['#options'] = _get_opt_non_beeders();
  }

  // If "Remove" was clicked.
  if ($trigger_elem == "remove_permission_btn") {

    // Remove 'Breeder' permssion.
    $user_id = $form_state['values']['breeders']['breeder_list'];
    if ($user_id) {
      $bims_user = BIMS_USER::byKey(array('user_id' => $user_id));
      $bims_user->makeNonBreeder();

      // Updates DDLs.
      $form['breeders']['breeder_list']['#options'] = _get_opt_breeder_list();
      $form['breeders']['non_breeder_list']['#options'] = _get_opt_non_beeders();
    }
  }

  // If "Link" was clicked.
  if ($trigger_elem == "link_btn") {
    $user_id = $form_state['values']['contact']['breeder_list'];
    $contact_id = $form_state['values']['contact']['contact_list'];

    // Links them.
    if ($user_id && $contact_id) {

      // Gets the breeder.
      $breeder = BIMS_USER::byID($user_id);

      // updats the contact_id.
      if ($contact_id == 'NULL') {
        $breeder->nullContactID();
      }
      else {
        $breeder->setContactID($contact_id);
        if (!$breeder->update()) {
          drupal_set_message("Error : Failed to link the breeder with contact_id", 'error');
        }
      }

      // Updates the table.
      $form['contact']['list']['#markup'] = _bims_get_contact_list();
    }
    else {
      drupal_set_message("Please choose a breeder and a contact");
    }
  }
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_user_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_user_form_submit($form, &$form_state) {}