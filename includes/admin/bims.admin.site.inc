<?php
/**
 * Manage sites in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_site_form($form, &$form_state, $op = 'view', $site_id = NULL) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Updates the operation.
  if (isset($form_state['values']['op'])) {
    $op = $form_state['values']['op'];
  }

  // Gets map_id and BIMS_CROP.
  $bims_site = NULL;
  if ($site_id) {
    $bims_site = BIMS_SITE::byID($site_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves the site ID.
  $form['site_id'] = array(
    '#type'   => 'value',
    '#value'  => $site_id,
  );

  // Adds the instruction.
  $form['manage'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'BIMS sites',
    '#description'  =>  t('Manage sites in BIMS.'),
  );

  // Admin BIMS view a site.
  if ($op == 'view') {

    // Adds 'add a site' link.
    $form['manage']['add_site_link']= array(
      '#markup' => '<div style="margin-top:15px;margin-bottom:15px;"><b>' . l('Add a site', 'admin/bims/site/add') . '</b></div>',
    );

    // Adds site table.
    $form['manage']['site_table']= array(
      '#markup' => _bims_admin_site_form_site_table(),
    );
  }

  // Admin BIMS add/edit a site.
  else if ($op == 'add' || $op == 'edit') {
    $btn_name       = 'add_site_btn';
    $btn_label      = 'Add';
    $def_label      = '';
    $def_type       = '';
    $def_latitude   = '';
    $def_longitude  = '';
    $def_icon       = '';
    $def_notes      = '';
    if ($bims_site) {
      $btn_name       = 'edit_site_btn';
      $btn_label      = 'Update';
      $def_label      = $bims_site->getLabel();
      $def_type       = $bims_site->getType();
      $def_latitude   = $bims_site->getLatitude();
      $def_longitude  = $bims_site->getLongitude();
      $def_icon       = $bims_site->getIcon();
      $def_notes      = $bims_site->getNotes();
    }
    $form['manage']['site_prop']['label'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Label',
      '#description'    => 'Please provide site label',
      '#default_value'  => $def_label,
      '#required'       => TRUE,
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['manage']['site_prop']['type'] = array(
      '#type'           => 'select',
      '#title'          => 'Type',
      '#options'        => BIMS_SITE::getSiteTypeOpts(),
      '#description'    => 'Please choose a type',
      '#default_value'  => $def_type,
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['manage']['site_prop']['latitude'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Latitude',
      '#description'    => 'Please provide latitude',
      '#default_value'  => $def_latitude,
      '#required'       => TRUE,
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['manage']['site_prop']['longitude'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Longitude',
      '#description'    => 'Please provide longitude',
      '#default_value'  => $def_longitude,
      '#required'       => TRUE,
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['manage']['site_prop']['icon'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Icon',
      '#description'    => 'Please provide filename of the icon',
      '#default_value'  => $def_icon,
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['manage']['site_prop']['notes'] = array(
      '#type'           => 'textarea',
      '#title'          => 'Notes',
      '#description'    => 'Please provide notes',
      '#default_value'  => $def_notes,
      '#attributes'     => array('style' => 'width:400px;'),
    );

    // Adds a button.
    $form['manage'][$btn_name]= array(
      '#type'       => 'submit',
      '#name'       => $btn_name,
      '#value'      => $btn_label,
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_admin_site_form_ajax_callback',
        'wrapper'  => 'bims-admin-site-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-site-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Returns the site table.
 *
 * @return string
 */
function _bims_admin_site_form_site_table() {

  // Sets the header.
  $header = array(
    array('data' => 'Label', 'width' => '120'),
    array('data' => 'Type', 'width' => '20'),
    array('data' => 'Latitude', 'width' => '10'),
    array('data' => 'Longitude', 'width' => '10'),
    array('data' => 'Edit', 'width' => '10'),
    array('data' => 'Delete', 'width' => '10'),
  );

  // Populates the rows with the sites.
  $rows = array();
  $sites = BIMS_SITE::getSites(BIMS_OBJECT);
  foreach ($sites as $site_obj) {

    // Creates 'Edit' and 'Delete' links.
    $confirm_attr = array(
      'attributes' => array(
        'id' => 'delete_' . $site_obj->site_id,
        'class' => array('use-ajax','bims-confirm'),
      ),
    );
    $link_delete = l('Delete', 'admin/bims/site/delete/' . $site_obj->site_id, $confirm_attr);
    $link_edit = l('Edit', 'admin/bims/site/edit/' . $site_obj->site_id);
    $rows []= array(
      $site_obj->label,
      $site_obj->type,
      $site_obj->latitude,
      $site_obj->longitude,
      $link_edit,
      $link_delete,
    );
  }
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  return bims_theme_table($table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_site_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_site_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' or 'Eidt' was clicked.
  if ($trigger_elem == 'add_site_btn' || $trigger_elem == 'edit_site_btn') {

    // Gets the properties of the site.
    $site_id        = $form_state['values']['site_id'];
    $site_label     = trim($form_state['values']['manage']['site_prop']['label']);
    $site_latitude  = trim($form_state['values']['manage']['site_prop']['latitude']);
    $site_longitude = trim($form_state['values']['manage']['site_prop']['longitude']);

    // Checks the label for duplication.
    if ($trigger_elem == 'add_site_btn') {
      $bims_site = BIMS_SITE::byLabel($site_label);
      if ($bims_site) {
        form_set_error('manage][site_prop][name', "Error : '$site_label' has already existed.$site_id");
        return;
      }
    }

    // Checks the label for duplication if changed.
    if ($trigger_elem == 'edit_site_btn') {
      $cur_bims_site = BIMS_SITE::byID($site_id);
      if ($site_label != $cur_bims_site->getLabel()) {
        $bims_site = BIMS_SITE::byLabel($site_label);
        if ($bims_site) {
          form_set_error('manage][site_prop][name', "Error : '$site_label' has already existed.");
          return;
        }
      }
    }

    // Checks the coordinates for real.
    if (!is_numeric($site_latitude)) {
      form_set_error('manage][site_prop][latitude', "Error : Latitude ($site_latitude) must be float.");
      return;
    }
    if (!is_numeric($site_longitude)) {
      form_set_error('manage][site_prop][longitude', "Error : Longitude ($site_longitude) must be float.");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_site_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the current BIMS_SITE.
  $bims_site = $form_state['values']['bims_site'];

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' or 'Edit' was clicked.
  if ($trigger_elem == 'add_site_btn' || $trigger_elem == 'edit_site_btn') {

    // Gets the properties of the site.
    $site_label     = trim($form_state['values']['manage']['site_prop']['label']);
    $site_type      = $form_state['values']['manage']['site_prop']['type'];
    $site_latitude  = floatval(trim($form_state['values']['manage']['site_prop']['latitude']));
    $site_longitude = floatval(trim($form_state['values']['manage']['site_prop']['longitude']));
    $site_icon      = trim($form_state['values']['manage']['site_prop']['icon']);
    $site_notes     = trim($form_state['values']['manage']['site_prop']['notes']);

    // Adds a site.
    if ($trigger_elem == 'add_site_btn') {

      // Sets the properties.
      $details = array(
        'label'     => $site_label,
        'type'      => $site_type,
        'latitude'  => $site_latitude,
        'longitude' => $site_longitude,
        'icon'      => $site_icon,
        'notes'     => $site_notes,
      );
      $bims_site = new BIMS_SITE($details);
      if ($bims_site->insert()) {
        drupal_set_message("The site ($site_label) has been added");
      }
      else {
        drupal_set_message("Error : Failed to add the site ($site_label)", 'error');
      }
    }

    // Edits the site.
    else if ($trigger_elem == 'edit_site_btn') {

      // Gets the site ID.
      $site_id = $form_state['values']['site_id'];
      $bims_site = BIMS_SITE::byID($site_id);

      // Updates the properties.
      $bims_site->setLabel($site_label);
      $bims_site->setType($site_type);
      $bims_site->setLatitude($site_latitude);
      $bims_site->setLongitude($site_longitude);
      $bims_site->setIcon($site_icon);
      $bims_site->setNotes($site_notes);
      if ($bims_site->update()) {
        drupal_set_message("The site ($site_label) has been updated");
      }
      else {
        drupal_set_message("Error : Failed to update the properties of the site '$site_label'", 'error');
      }
    }
  }
  $form_state['values']['op'] = 'view';
}