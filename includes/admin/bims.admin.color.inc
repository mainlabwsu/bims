<?php
/**
 * Manage color in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_color_form($form, &$form_state) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Adds the instruction.
  $form['manage'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Manage BIMS colors',
  );
  $form['manage']['desc'] = array(
    '#markup' => t('Manage color schema in BIMS.'),
  );

  // Color.
  $form['manage']['color'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Color schema',
  );
  $form['manage']['color']['table'] = array(
    '#markup' => _bims_admin_color_form_color_table('color')
  );

  // Gray color.
  $form['manage']['graycolor'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Graycolors schema',
  );
  $form['manage']['graycolor']['table'] = array(
    '#markup' => _bims_admin_color_form_color_table('gray')
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-color-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Returns the color table.
 *
 * @return string
 */
function _bims_admin_color_form_color_table($type) {

  // Sets the header.
  $header = array(
    array('data' => 'Code', 'width' => '20'),
    array('data' => 'Color', 'width' => '10'),
    array('data' => 'Rank', 'width' => '10'),
    array('data' => 'Edit', 'width' => '10'),
    array('data' => 'Delete', 'width' => '10'),
  );

  // Populates the rows with the colors.
  $rows = array();
  $colors = BIMS_COLOR::getColors($type, BIMS_OBJECT);
  foreach ($colors as $color_obj) {

    // Creates 'Edit' and 'Delete' links.
    $confirm_attr = array(
      'attributes' => array(
        'id' => 'delete-color-' . $color_obj->color_id,
        'class' => array('use-ajax','bims-confirm'),
      ),
    );
    $link_delete = 'DELETE'; //l('Delete', 'admin/bims/color/delete/' . $color_obj->color_id, $confirm_attr);
    $link_edit = 'EDIT'; //l('Edit', 'admin/bims/color/edit/' . $color_obj->color_id);
    $rows []= array(
      $color_obj->code,
      "<div style='width:150px;background-color:" . $color_obj->code . "'>&nbsp;</div>",
      $color_obj->rank,
      $link_edit,
      $link_delete,
    );
  }

  // Generates the table.
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  return bims_theme_table($table_vars);
}

/**
 * Returns the graycolor table.
 *
 * @return string
 */
function _bims_admin_color_form_graycolor_table() {
  return 'graycolor_tabdle';
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_color_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_color_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_color_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}