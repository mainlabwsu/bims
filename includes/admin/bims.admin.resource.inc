<?php
/**
 * Manage resources in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_resource_form($form, &$form_state) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Adds the instruction.
  $form['manage'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Manage BIMS resources',
  );
  $form['manage']['desc'] = array(
    '#markup' => t('Manage resources in BIMS.'),
  );

  // Lists all resources in BIMS.
  $form['manage']['resource'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'BIMS resources',
  );
  $form['manage']['resource']['table'] = array(
    '#markup' => _bims_admin_resource_form_table(),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-resource-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Returns the resource table.
 *
 * @return string
 */
function _bims_admin_resource_form_table() {

  // Sets the header.
  $header = array(
    array('data' => 'Name', 'width' => '20'),
    array('data' => 'Type', 'width' => '10'),
    array('data' => 'Enabled', 'width' => '5'),
    array('data' => 'Rank', 'width' => '10'),
    array('data' => 'Max Slot', 'width' => '10'),
    array('data' => 'Description', 'width' => '10'),
    array('data' => 'Edit', 'width' => '10'),
    array('data' => 'Delete', 'width' => '10'),
  );

  // Populates the rows with the resources.
  $rows = array();
  $resources = BIMS_RESOURCE::getResources(BIMS_OBJECT);
  foreach ($resources as $resource) {
    $enabled      = ($resource->enabled) ? 'YES' : 'NO';
    $description  = ($resource->description) ? $resource->description : '<em>N/A</em>';

    // Creates 'Edit' and 'Delete' links.
    $confirm_attr = array(
      'attributes' => array(
        'id' => 'delete-resource-' . $resource->resource_id,
        'class' => array('use-ajax','bims-confirm'),
      ),
    );
    $link_delete = 'DELETE'; //l('Delete', 'admin/bims/resource/delete/' . $resource->resource_id, $confirm_attr);
    $link_edit = 'EDIT'; //l('Edit', 'admin/bims/resource/edit/' . $resource->resource_id);

    $rows []= array(
      $resource->name,
      $resource->type,
      $enabled,
      $resource->rank,
      $resource->max_slots,
      $description,
      $link_edit,
      $link_delete,
    );
  }

  // Generates the table.
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  return bims_theme_table($table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_resource_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_resource_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_resource_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}