<?php
/**
 * @file
 * This file contains the functions used for administration of the module
 *
 * @ingroup bims_admin
 */

/**
 * Admin page
 *
 */
function bims_admin_page() {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  drupal_set_breadcrumb($breadcrumb);

  // Page contents.
  $content = '<b>BIMS admin main page</b><br /><br />';

  // Checks BIMS directories.
  $dirs = array();
  $dirs['bims_working_dir'] = bims_get_config_setting('bims_working_dir');
  $dirs['bims_tmp_dir']     = bims_get_config_setting('bims_tmp_dir');
  $dirs['bims_lock_dir']    = $dirs['bims_working_dir'] . '/lock';
  $content .= '<b>Checking BIMS directories</b><br /><br />';
  $err_msg = '';
  foreach ($dirs as $name => $dir) {
    $content .= "<b>$name :</b> $dir";
    if (file_exists($dir)) {
      $content .= " ... ok<br />";
    }
    else {
      $content .= '<br />The directory does not exist, trying to create a directory<br />';
      if(!file_prepare_directory($dir, FILE_CREATE_DIRECTORY)) {
        $content .= 'Fail to create a directory<br /><br />';
        $err_msg .= 'Fail to create a directory<br />';
      }
      else {
        $content .= 'The directory has been created<br /><br />';
      }
    }
  }
  if ($err_msg) {
    watchdog('BIMS', $err_msg, array(), WATCHDOG_ERROR);
  }
  return $content;
}
