<?php
/**
 * Manage BIMS config page.
 *
 * @ingroup bims_admin
 */
function bims_admin_config_form() {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Create the setting form.
  $form = array();
  $form['#tree'] = TRUE;

  // Adds 'Manage BIMS'.
  $form['bims'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Manage BIMS',
  );
  // BIMS working directory.
  $form['bims']['bims_public_program'] = array(
    '#title'          => t('BIMS Public Program'),
    '#type'           => t('checkbox'),
    '#default_value'  => bims_get_config_setting('bims_public_program'),
    '#description'    => t("All the public programs owned by BIMS listed in the program selection if checked."),
  );

  // Adds 'Manage BIMS Directories'.
  $form['directory'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Manage BIMS Directories',
  );

  // Drupal file paths.
  $file_path_public   = drupal_realpath('public://');
  $file_path_private  = drupal_realpath('private://');

  // Uploading file directory.
  $form['directory']['bims_file_dir'] = array(
    '#markup' => t("<br /><b>Uploading File Directory</b><br />" .
        "Uploaded files are saved in drupal public direcotry ($file_path_public)<br /><br />")
  );

  // BIMS working directory.
  $form['directory']['bims_working_dir'] = array(
    '#title'          => t('BIMS Working Directory'),
    '#type'           => t('textfield'),
    '#description'    => t("Please specify the working directory for BIMS."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_working_dir'),
  );

  // BIMS image directory.
  $form['directory']['bims_img_dir'] = array(
    '#title'          => t('BIMS Image Directory'),
    '#type'           => t('textfield'),
    '#description'    => t("Please specify the image directory of BIMS."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_img_dir'),
  );

  // BIMS Temporary directory.
  $form['directory']['bims_tmp_dir'] = array(
    '#title'          => t('BIMS Temporary Directory'),
    '#type'           => t('textfield'),
    '#description'    => t("Please specify the temporary directory of BIMS."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_tmp_dir'),
  );

  // Adds 'Manage BIMS Variables'.
  $form['map'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Manage Google Map Variables',
  );
  $form['map']['bims_def_latitude'] = array(
    '#title'          => t('Default Latitude'),
    '#type'           => t('textfield'),
    '#description'    => t("The default latitude."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_def_latitude'),
  );
  $form['map']['bims_def_longitude'] = array(
    '#title'          => t('Default Longitude'),
    '#type'           => t('textfield'),
    '#description'    => t("The default longitude."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_def_longitude'),
  );
  $form['map']['bims_def_zoom'] = array(
    '#title'          => t('Default Zoom'),
    '#type'           => t('textfield'),
    '#description'    => t("The default zoom."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_def_zoom'),
  );
  $form['map']['bims_def_icon'] = array(
    '#title'          => t('Default Icon'),
    '#type'           => t('textfield'),
    '#description'    => t("The default icon."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_def_icon'),
  );
  $form['map']['bims_map_img_path'] = array(
    '#title'          => t('Map Image Path'),
    '#type'           => t('textfield'),
    '#description'    => t("The map image path."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_map_img_path'),
  );

  // Adds 'Manage BIMS Programs'.
  $form['program'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Manage BIMS External Tools',
  );
  $form['program']['bims_drush_binary'] = array(
    '#title'          => t('Drush Binary'),
    '#type'           => t('textfield'),
    '#description'    => t("The full path to the drush binary on this server."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_drush_binary'),
  );

  // Adds 'Manage BIMS Variables'.
  $form['variable'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Manage BIMS Variables',
  );
  $form['variable']['bims_precision_format'] = array(
    '#title'          => t('Precision format'),
    '#type'           => t('textfield'),
    '#description'    => t("The default precision format of phenotyping data to be displayed."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_precision_format'),
  );
  $form['variable']['bims_accession'] = array(
    '#title'          => t('Accession'),
    '#type'           => t('textfield'),
    '#description'    => t("The default column name for accession."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_accession'),
  );
  $form['variable']['bims_unique_id'] = array(
    '#title'          => t('Uniuqe identifier'),
    '#type'           => t('textfield'),
    '#description'    => t("The default column name for unique ID."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_unique_id'),
  );
  $form['variable']['bims_primary_order'] = array(
    '#title'          => t('Primary order'),
    '#type'           => t('textfield'),
    '#description'    => t("The default column name for primary order."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_primary_order'),
  );
  $form['variable']['bims_secondary_order'] = array(
    '#title'          => t('Secondary order'),
    '#type'           => t('textfield'),
    '#description'    => t("The default column name for secondary order."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_secondary_order'),
  );
  $form['variable']['bims_excel_version'] = array(
    '#title'          => t('Excel version'),
    '#type'           => t('textfield'),
    '#description'    => t("Specify the version of the installed Excel library supports."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_excel_version'),
  );
  $form['variable']['bims_excel_max_rows'] = array(
    '#title'          => t('Excel maximum numbers of rows'),
    '#type'           => t('textfield'),
    '#description'    => t("Specify the maximum number of rows that the installed Excel library supports."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_excel_max_rows'),
  );
  $form['variable']['bims_excel_max_cols'] = array(
    '#title'          => t('Excel maximum numbers of columns'),
    '#type'           => t('textfield'),
    '#description'    => t("Specify the maximum number of columns that the installed Excel library supports."),
    '#required'       => TRUE,
    '#default_value'  => bims_get_config_setting('bims_excel_max_cols'),
  );
  return system_settings_form($form);
}

/**
 * Admin form validate.
 *
 * @ingroup bims_admin
 */
function bims_admin_config_form_validate($form, &$form_state) {

  // Gets the form values.
  $bims_public_program        = trim($form_state['values']['bims']['bims_public_program']);
  $bims_working_dir           = trim($form_state['values']['directory']['bims_working_dir']);
  $bims_img_dir               = trim($form_state['values']['directory']['bims_img_dir']);
  $bims_tmp_dir               = trim($form_state['values']['directory']['bims_tmp_dir']);
  $bims_def_latitude          = trim($form_state['values']['map']['bims_def_latitude']);
  $bims_def_longitude         = trim($form_state['values']['map']['bims_def_longitude']);
  $bims_def_zoom              = trim($form_state['values']['map']['bims_def_zoom']);
  $bims_def_icon              = trim($form_state['values']['map']['bims_def_icon']);
  $bims_map_img_path          = trim($form_state['values']['map']['bims_map_img_path']);
  $bims_drush_binary          = trim($form_state['values']['program']['bims_drush_binary']);
  $bims_precision_format      = trim($form_state['values']['variable']['bims_precision_format']);
  $bims_accession             = trim($form_state['values']['variable']['bims_accession']);
  $bims_unique_id             = trim($form_state['values']['variable']['bims_unique_id']);
  $bims_primary_order         = trim($form_state['values']['variable']['bims_primary_order']);
  $bims_secondary_order       = trim($form_state['values']['variable']['bims_secondary_order']);
  $bims_excel_version         = trim($form_state['values']['variable']['bims_excel_version']);
  $bims_excel_max_rows        = trim($form_state['values']['variable']['bims_excel_max_rows']);
  $bims_excel_max_cols        = trim($form_state['values']['variable']['bims_excel_max_cols']);

  // Checks the directories.
  if (!is_writable($bims_working_dir)) {
    form_set_error('directory][bims_working_dir', t("The working directory, $bims_working_dir, does not exists or is not writeable by the web server."));
    return;
  }
  if (!is_writable($bims_tmp_dir)) {
    form_set_error('directory][bims_tmp_dir', t("The temporary directory, $bims_tmp_dir, does not exists or is not writeable by the web server."));
    return;
  }

  // Checks drush binary.
  if (!file_exists($bims_drush_binary)) {
    form_set_error('program][bims_drush_binary', t("The drush path does not appear to be correct. Cannot find the binary."));
    return;
  }

  // Sets the variables.
  variable_set('bims_public_program', $bims_public_program);
  variable_set('bims_working_dir', $bims_working_dir);
  variable_set('bims_img_dir', $bims_img_dir);
  variable_set('bims_tmp_dir', $bims_tmp_dir);
  variable_set('bims_def_latitude', $bims_def_latitude);
  variable_set('bims_def_longitude', $bims_def_longitude);
  variable_set('bims_def_zoom', $bims_def_zoom);
  variable_set('bims_def_icon', $bims_def_icon);
  variable_set('bims_map_img_path', $bims_map_img_path);
  variable_set('bims_drush_binary', $bims_drush_binary);
  variable_set('bims_precision_format', $bims_precision_format);
  variable_set('bims_accession', $bims_accession);
  variable_set('bims_unique_id', $bims_unique_id);
  variable_set('bims_primary_order', $bims_primary_order);
  variable_set('bims_secondary_order', $bims_secondary_order);
  variable_set('bims_excel_version', $bims_excel_version);
  variable_set('bims_excel_max_rows', $bims_excel_max_rows);
  variable_set('bims_excel_max_cols', $bims_excel_max_cols);
}
