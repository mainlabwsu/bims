<?php
/**
 * Manage crops in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_crop_form($form, &$form_state, $op = 'view', $crop_id = NULL) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Gets BIMS_CROP and saves it.
  $bims_crop = NULL;
  if (isset($form_state['values']['crop']['crop_list'])) {
    $crop_id = $form_state['values']['crop']['crop_list'];
  }
  if ($crop_id) {
    $bims_crop = BIMS_CROP::byID($crop_id);
  }


  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves BIMS_CROP.
  $form['bims_crop'] = array(
    '#type'   => 'value',
    '#value'  => $bims_crop,
  );

  // Adds the instruction.
  $form['manage'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'BIMS crops',
  );
  $form['manage']['desc'] = array(
    '#markup' => t('Manage crops in BIMS.'),
  );

  // OP : View.
  if ($op == 'view') {

    // Adds 'add a crop' link.
    $form['manage']['add_crop_link']= array(
      '#markup' => '<div style="margin-top:15px;margin-bottom:15px;"><b>' . l('Add a crop', 'admin/bims/crop/add') . '</b></div>',
    );

    // Gets the crop list.
    $opt_crop_list = BIMS_CROP::getCrops(BIMS_OPTION);
    if (empty($opt_crop_list)) {
      $form['manage']['no_crop'] = array(
        '#markup' => t('Please add a crop to BIMS.'),
      );
    }
    else {

      // Gets the selected crop or first crop in the list.
      if (!$bims_crop) {
        $tmp        = array_keys($opt_crop_list);
        $crop_id    = $tmp[0];
        $bims_crop  = BIMS_CROP::byID($crop_id);
      }
      $crop_label = $bims_crop->getLabel();

      // Crop list.
      $size = sizeof($opt_crop_list);
      $size = ($size < 5) ? $size : 5;
      $form['manage']['crop_list'] = array(
        '#type'           => 'select',
        '#title'          => t('Crops in BIMS'),
        '#options'        => $opt_crop_list,
        '#default_value'  => $crop_id,
        '#size'           => $size,
        '#attributes'     => array('style' => 'width:200px;background-color:#FFFFFF;'),
        '#ajax'       => array(
          'callback' => 'bims_admin_crop_form_ajax_callback',
          'wrapper'  => 'bims-admin-crop-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );

      // Organism table.
      $form['manage']['org_table'] = array(
        '#markup' => _bims_admin_crop_form_info_table($bims_crop),
      );

      // Adds 'Delete' message and button.
      if ($bims_crop->getNumPrograms()) {
        $form['manage']['delete_msg']= array(
          '#markup' => "<div style='margin:15px 0px 10px 0px;'><b>'$crop_label'</b/> cannot be deleted as at leaset one BIMS program belong to this crop.</div>",
        );
      }

        $form['manage']['delete_btn']= array(
          '#type'       => 'submit',
          '#name'       => 'delete_btn',
          '#value'      => 'Delete',
          '#attributes' => array('style' => 'width:200px;'),
          '#ajax'       => array(
            'callback' => 'bims_admin_crop_form_ajax_callback',
            'wrapper'  => 'bims-admin-crop-form',
            'effect'   => 'fade',
            'method'   => 'replace',
          ),
        );
      if ($bims_crop->getNumPrograms()) {
        hide($form['manage']['delete_btn']);
      }
    }
  }

  // Opearation : Edit organism.
  else if ($op == 'edit_org') {
    $crop_label = $bims_crop->getLabel();

    // Organisms in Chado.
    $options = $bims_crop->getChadoOrganisms();
    $size = sizeof($options);
    $size = ($size < 5) ? $size : 5;
    $form['manage']['chado_org_list'] = array(
      '#type'       => 'select',
      '#title'      => "Organism not associated with crop ($crop_label)",
      '#options'    => $options,
      '#size'       => $size,
      '#multiple'   => TRUE,
      '#attributes' => array('style' => 'width:250px;'),
    );
    $form['manage']['add_org_btn']= array(
      '#type'       => 'submit',
      '#name'       => 'add_org_btn',
      '#value'      => t('Add Organism'),
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_admin_crop_form_ajax_callback',
        'wrapper'  => 'bims-admin-crop-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Organisms associated with this crop.
    $opt_org  = $bims_crop->getOrganisms(BIMS_OPTION);
    $size     = sizeof($opt_org);
    if ($size == 0) {
      $form['manage']['no_org'] = array(
        '#markup' => "<div style='margin:10px 0px 10px 0px;'><em><b>No organism has been set for $crop_label</b></em></div>",
      );
    }
    else {
      $size = ($size < 5) ? $size : 5;
      $form['manage']['delete_org_list'] = array(
        '#type'       => 'select',
        '#title'      => "Organism associated with $crop_label",
        '#options'    => $opt_org,
        '#size'       => $size,
        '#multiple'   => TRUE,
        '#attributes' => array('style' => 'width:250px;'),
      );
      $form['manage']['delete_org_btn']= array(
        '#type'       => 'submit',
        '#name'       => 'delete_org_btn',
        '#value'      => t('Delete Organism'),
        '#attributes' => array('style' => 'width:200px;'),
      );
    }
  }

  // Opearation : Add / Edit.
  else if ($op == 'add' || $op == 'edit') {

    // Sets the crop properties.
    $title        = 'Add a crop';
    $crop_label   = '';
    $crop_ploidy  = BIMS_DEF_PLOIDY;
    $btn_name     = 'add_btn';
    $btn_title    = 'Add';
    $crop_cv_id   = '';
    if ($bims_crop) {
      $title        = 'Update the crop';
      $crop_label   = $bims_crop->getLabel();
      $crop_ploidy  = $bims_crop->getPloidy();
      $crop_cv_id   = $bims_crop->getTraitDescriptorSet();
      $btn_name     = 'update_btn';
      $btn_title    = 'Update';
    }

    // Manages crop.
    $form['manage']['crop_prop'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => $title,
    );
    $form['manage']['crop_prop']['label'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Crop Label'),
      '#default_value'  => $crop_label,
      '#required'       => TRUE,
      '#description'    => t("Please provide a crop name"),
      '#attributes'     => array('style' => 'width:250px;'),
    );
    $form['manage']['crop_prop']['ploidy'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Ploidy'),
      '#default_value'  => $crop_ploidy,
      '#required'       => TRUE,
      '#description'    => t("Please set ploidy of the crop"),
      '#attributes'     => array('style' => 'width:250px;'),
    );
    $form['manage']['crop_prop']['trait_descriptor_set'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Public Trait Descriptor Set'),
      '#default_value'  => $crop_cv_id,
      '#description'    => t("Please type CV ID of the public trait descriptor set"),
      '#attributes'     => array('style' => 'width:250px;'),
    );
    $form['manage']['crop_prop'][$btn_name]= array(
      '#type'       => 'submit',
      '#name'       => $btn_name,
      '#value'      => $btn_title,
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_admin_crop_form_ajax_callback',
        'wrapper'  => 'bims-admin-crop-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-crop-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Returns the organism table for the given crop.
 *
 * @param BIMS_CROP $bims_crop
 *
 * @return string
 */
function _bims_admin_crop_form_info_table(BIMS_CROP $bims_crop) {

  // Sets the number of programs.
  $num_program = $bims_crop->getNumPrograms();
  if ($num_program == 0) {
    $num_program = '<em>No associated BIMS program</em>';
  }

  // Gets the all organism.
  $org_arr = array();
  $org_objs = $bims_crop->getOrganisms(BIMS_OBJECT);
  foreach ((array)$org_objs as $org_obj) {
    $org_arr []= $org_obj->genus . ' ' . $org_obj->species;
  }
  $organisms = '<em>No organism has been associated</em>';
  if (!empty($org_arr)) {
    $organisms = implode('<br />', $org_arr);
  }
  $tds = '<em>Trait descriptor set has not been assigned</em>';
  $cv_id = $bims_crop->getTraitDescriptorSet();
  if ($cv_id) {
    $cv = MCL_CHADO_CV::byID($cv_id);
    if ($cv) {
      $tds = "[$cv_id] " . $cv->getName();
    }
    else {
      $tds = "<em>Error : cv ID [$cv_id] does not exist</em>";
    }
  }

  // Adds the actions.
  $edit_prop  = l('Edit the properties', '/admin/bims/crop/edit/' . $bims_crop->getCropID());
  $edit_org   = l('Edit the organisms', '/admin/bims/crop/edit_org/' . $bims_crop->getCropID());

  // Prefix.
  $prefix = "<div><b>Crop properties</b></div>";

  // Lists the organism in table.
  $rows = array(
    array(array('data' => 'Name', 'header' => TRUE), $bims_crop->getName()),
    array(array('data' => 'Label', 'header' => TRUE),  $bims_crop->getLabel()),
    array(array('data' => 'Ploidy', 'header' => TRUE), $bims_crop->getPloidy()),
    array(array('data' => 'Organisms', 'header' => TRUE), $organisms),
    array(array('data' => 'Public Trait<br />Descriptor Set', 'header' => TRUE), $tds),
    array(array('data' => '# Programs', 'header' => TRUE), $num_program),
    array(array('data' => 'Actions', 'header' => TRUE), "$edit_prop<br />$edit_org"),
  );
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:450px'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  return $prefix . bims_theme_table($table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_crop_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Cros selection has been changed.
  if ($trigger_elem == 'manage[crop_list]') {
    $crop_id = $form_state['values']['manage']['crop_list'];
    $bims_crop = BIMS_CROP::byID($crop_id);
    $crop_label = $bims_crop->getLabel();
    $form['manage']['org_table']['#markup'] = _bims_admin_crop_form_info_table($bims_crop);

    // Hides 'Delete' button.
    $msg = '';
    if ($bims_crop->getNumPrograms()) {
      $msg = "<div style='margin:15px 0px 10px 0px;'><b>'$crop_label'</b/> cannot be deleted as at leaset one BIMS program belong to this crop.</div>";
      hide($form['manage']['delete_btn']);
    }
    else {
      show($form['manage']['delete_btn']);
    }
    $form['manage']['delete_msg']['#markup'] = $msg;
  }
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_crop_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'add_btn') {

    // Checks the label for duplication.
    $label = trim($form_state['values']['manage']['crop_prop']['label']);
    $bims_crop = BIMS_CROP::byKey(array('label' => $label));
    if ($bims_crop) {
      form_set_error("crop][manage_crop][add_crop][label", "Error : Crop ($label) has already existed.");
      return;
    }

    // Checks the ploidy.
    $ploidy = trim($form_state['values']['manage']['crop_prop']['ploidy']);
    if (!bims_is_int($ploidy)) {
      form_set_error("crop][manage][crop_prop][ploidy", "Error : Please type an integer.");
      return;
    }
    if (!$ploidy) {
      form_set_error("crop][manage][crop_prop][ploidy", "Error : Please set ploidy.");
      return;
    }

    // Checks the cv ID of the trait descriptor set.
    $cv_id = trim($form_state['values']['manage']['crop_prop']['trait_descriptor_set']);
    if ($cv_id) {
      $cv = MCL_CHADO_CV::byID($cv_id);
      if (!$cv) {
        form_set_error("crop][manage][crop_prop][trait_descriptor_set", "Error : cv ID [$cv_id] does not exist.");
        return;
      }
    }
  }

  // If 'Edit' was clicked.
  else if ($trigger_elem == 'update_btn') {

    // Gets BIMS_CROP.
    $bims_crop        = $form['bims_crop']['#value'];
    $cur_label_clean  = BIMS_CROP::cleanLabel($bims_crop->getLabel());

    // Checks the label for duplication.
    $label = trim($form_state['values']['manage']['crop_prop']['label']);
    $label_clean = BIMS_CROP::cleanLabel($label);
    if ($cur_label_clean != $label_clean) {
      $bims_crop = BIMS_CROP::byName($label_clean);
      if ($bims_crop) {
        form_set_error("crop][manage_crop][add_crop][label", "Error : Crop ($label) has already existed.");
        return;
      }
    }

    // Checks the ploidy.
    $ploidy = trim($form_state['values']['manage']['crop_prop']['ploidy']);
    if (!$ploidy) {
      form_set_error("crop][manage][crop_prop][ploidy", "Error : Please set ploidy.");
      return;
    }

    // Checks the cv ID of the trait descriptor set.
    $cv_id = trim($form_state['values']['manage']['crop_prop']['trait_descriptor_set']);
    if ($cv_id) {
      $cv = MCL_CHADO_CV::byID($cv_id);
      if (!$cv) {
        form_set_error("crop][manage][crop_prop][trait_descriptor_set", "Error : cv ID [$cv_id] does not exist.");
        return;
      }
    }
  }

  // If the 'Add Organism' was clicked.
  else if ($trigger_elem == 'add_org_btn') {
    $organism_id =  $form_state['values']['manage']['chado_org_list'];
    if (!$organism_id) {
      form_set_error("manage][chado_org_list", "Please choose an organism");
      return;
    }
  }

  // If 'Delete Organism' was clicked.
  else if ($trigger_elem == 'delete_org_btn') {
    $organism_id =  $form_state['values']['manage']['delete_org_list'];
    if (!$organism_id) {
      form_set_error("manage][delete_org_list", "Please choose an organism");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_crop_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_CROP.
  $bims_crop = $form_state['values']['bims_crop'];

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'add_btn') {

    // Gets the crop properties.
    $label  = trim($form_state['values']['manage']['crop_prop']['label']);
    $ploidy = trim($form_state['values']['manage']['crop_prop']['ploidy']);
    $cv_id  = trim($form_state['values']['manage']['crop_prop']['trait_descriptor_set']);

    // Adds a new crop.
    $bims_crop = BIMS_CROP::addCrop($label, $ploidy, $cv_id);
    if ($bims_crop) {
      drupal_set_message("The crop ($label) has been added");
    }
    else {
      drupal_set_message("Error : Failed to add the crop ($label)", 'error');
    }
  }

  // If 'Edit' was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets the crop properties.
    $label  = trim($form_state['values']['manage']['crop_prop']['label']);
    $ploidy = trim($form_state['values']['manage']['crop_prop']['ploidy']);
    $cv_id  = trim($form_state['values']['manage']['crop_prop']['trait_descriptor_set']);

    // Edits the crop.
    $bims_crop->setLabel($label);
    $bims_crop->setPloidy($ploidy);
    if ($cv_id) {
      $bims_crop->setTraitDescriptorSet($cv_id);
    }
    if ($bims_crop->update()) {
      drupal_set_message("The crop ($label) has been updated");
    }
    else {
      drupal_set_message("Error : Failed to update the crop ($label)", 'error');
    }
  }

  // If 'Delete' was clicked.
  else if ($trigger_elem == 'delete_btn') {

    // Gets BIMS_CROP.
    $crop_id = $form_state['values']['manage']['crop_list'];
    $bims_crop = BIMS_CROP::byID($crop_id);
    if ($bims_crop) {

      // Deletes the crop.
      $label = $bims_crop->getLabel();
      if ($bims_crop->delete()) {
        drupal_set_message("The crop ($label) has been deleted");
      }
      else {
        drupal_set_message("Error : Failed to delete the crop ($label)", 'error');
      }
    }
  }

  // If 'Add Organism' was clicked.
  else if ($trigger_elem == 'add_org_btn') {
    $organism_ids =  $form_state['values']['manage']['chado_org_list'];
    foreach ($organism_ids as $organism_id) {
      if ($bims_crop->addOrganism($organism_id)) {
        drupal_set_message("The organism has been added");
      }
      else {
        drupal_set_message("Error : Failed to add an organism", 'error');
      }
    }
  }

  // If 'Delete Organism' was clicked.
  else if ($trigger_elem == 'delete_org_btn') {
    $organism_ids =  $form_state['values']['manage']['delete_org_list'];
    foreach ($organism_ids as $organism_id) {
      if ($bims_crop->removeOrganism($organism_id)) {
        drupal_set_message("The organism has been deleted");
      }
      else {
        drupal_set_message("Error : Failed to delete the organism", 'error');
      }
    }
  }
}