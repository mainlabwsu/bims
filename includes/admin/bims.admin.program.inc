<?php
/**
 * Manage programs in BIMS.
 *
 * @ingroup bims_admin
 */
function bims_admin_program_form($form, &$form_state) {

  // Sets the breadcrumb.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  $breadcrumb[] = l('Administration', 'admin');
  $breadcrumb[] = l('BIMS', 'admin/bims');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Adds the instruction.
  $form['manage'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'BIMS programs',
  );
  $form['manage']['desc'] = array(
    '#markup' => "<div>Manage programs in BIMS.</div>",
  );

  // Adds program table.
  $form['manage']['program_table']= array(
    '#markup' => '<br />' . _bims_admin_program_form_program_table(),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-admin-program-form">';
  $form['#suffix'] = '</div>';
  return $form;
}

/**
 * Returns the program table.
 *
 * @return string
 */
function _bims_admin_program_form_program_table() {

  // Sets the header.
  $header = array(
    array('data' => 'Crop', 'width' => '20'),
    array('data' => 'Program ID', 'width' => '10'),
    array('data' => 'Name', 'width' => '20'),
    array('data' => 'Owner', 'width' => '10'),
    array('data' => 'E-mail', 'width' => '10'),
    array('data' => 'Access', 'width' => '10'),
//    array('data' => 'Delete', 'width' => '10'),
  );

  // Populates the rows with the program.
  $rows = array();
  $bims_programs = BIMS_PROGRAM::getPrograms();
  foreach ($bims_programs as $bims_program) {
    $owner  = $bims_program->getOwner();
    $access = $bims_program->isPublic() ? 'Public' : 'Private';

    // Adds the program information.
    $rows []= array(
      $bims_program->getCrop()->getLabel(),
      $bims_program->getProgramID(),
      $bims_program->getName(),
      $owner->getName(),
      $owner->getMail(),
      $access,
    );
  }
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  return bims_theme_table($table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_admin_program_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_program_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @ingroup bims_admin
 */
function bims_admin_program_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the current BIMS_PROGRAM.
  $bims_program = $form_state['values']['bims_program'];

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Update' was clicked.
  if ($trigger_elem == 'update_btn') {

    if ($bims_program->update()) {
      drupal_set_message("The program has been updated");
    }
    else {
      drupal_set_message("Error : Failed to update the program", 'error');
    }
  }
}