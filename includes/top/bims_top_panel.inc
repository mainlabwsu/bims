<?php
/**
 * @file
*/
/**
 * @param array $form
 * @param array $form_state
 */
function bims_top_panel_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Sets the default names.
  $crop_name      = 'Select a crop';
  $program_name   = 'Select a program';
  $class_crop     = 'bims_grayout';
  $class_program  = 'bims_grayout';

  // Gets the crop_id.
  $crop_id = $bims_user->getCropID();
  if ($crop_id) {
    $bims_crop = BIMS_CROP::byKey(array('crop_id' => $crop_id));
    if ($bims_crop) {
      $class_crop = '';
      $crop_name = $bims_crop->getLabel();
    }
    else {
      $bims_user->resetCrop();
    }
  }

  // Gets the program_id
  $program_id = $bims_user->getProgramID();
  if ($program_id) {
    $bims_program = BIMS_PROGRAM::byID($program_id);
    if ($bims_program) {
      $class_program = '';
      $program_name = $bims_program->getName();
    }
    else {
      $bims_user->setProgramID('');
    }
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Shows the current crop if this site holds multiple crops.
  if (BIMS_CROP::getNumCrops() > 1) {
    $form['selected']['crop']= array(
      '#type'       => 'textfield',
      '#value'      => $crop_name,
      '#attributes' => array(
        'style'     => 'width:170px;color:black;',
        'class'     => array($class_crop),
        'readonly'  => 'readonly',
      ),
    );
  }

  // Shows the current program.
  $form['selected']['program'] = array(
    '#type'       => 'textfield',
    '#value'      => $program_name,
    '#attributes' => array(
      'style'     => 'width:248px;color:black;',
      'class'     => array($class_program),
      'readonly'  => 'readonly',
    ),
  );
  if ($bims_user->isBIMSAdmin() && $program_id) {
     $form['selected']['program']['#attributes']['title'] = "Program ID : $program_id";
  }

  // Sets properties of the form.
  $form['#theme'] = 'bims_top_panel_form';
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_top_panel_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_top_panel_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_top_panel_form($variables) {
  $form = $variables['form'];

  // Adds crop section if this site holds multiple crops.
  $layout = '';
  if (BIMS_CROP::getNumCrops() > 1) {
    $layout .= "<div style='float:left;'>" . drupal_render($form['selected']['crop']) . "</div>";
    $layout .= '<input type="button" style="float:left;width:80px;height:30px;" title="Change a crop" value="Crop" onclick="bims.load_primary_panel(\'bims_panel_main_crop\')" />';
  }

  // Adds proejct section.
  $layout .= "<div style='float:left;'>" . drupal_render($form['selected']['program']) . "</div>";
  $layout .= '<input type="button" style="float:left;width:80px;height:30px;" title="Change a program" value="Program" onclick="bims.load_primary_panel(\'bims_panel_main_program\')" />';
  $layout .= drupal_render_children($form);
  return $layout;
}