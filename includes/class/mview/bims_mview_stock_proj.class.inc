<?php
/**
 * The declaration of BIMS_MVIEW_STOCK_PROJ class.
*
*/
class BIMS_MVIEW_STOCK_PROJ extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView() {
    return 'bims.bims_' . $this->getNodeID() . '_mview_stock_proj';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_program = BIMS_PROGRAM::byID($node_id);
    if ($bims_program) {
      $bims_mview = new BIMS_MVIEW_STOCK_PROJ(array('node_id' => $node_id));
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for stock project',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'uniquename' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'stock_id')
      ),
    );
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($drop = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {
      $mview    = $this->getMView();
      $node_id  = $this->getNodeID();

      // Clears the mview.
      if ($drop) {
        $this->dropMView();
        $this->createMView();
      }
      else {
        $this->clear();
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($node_id);
      ;
      // Gets BIMS_CHADO tables.
      $bc_accession     = new BIMS_CHADO_ACCESSION($this->node_id);
      $table_accession  = $bc_accession->getTableName('accession');

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumns($bims_program, $bc_accession);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the all the stocks.
      $sql = "
        SELECT DISTINCT STOCK.stock_id, STOCK.uniquename, STOCK.name
          $select_str
        FROM {$table_accession} STOCK
          $join_str
        WHERE 1=1
      ";

      // Gets all the project IDs.
      $project_ids = $bc_accession->getProjectID();
      foreach ((array)$project_ids as $project_id) {

        // Gets the results for this project.
        $result = db_query($sql, array(':project_id' => $project_id));
        $dup_check = array();
        while ($arr = $result->fetchAssoc()) {
          if (array_key_exists($arr['stock_id'], $dup_check)) {
            continue;
          }
          else {
            $dup_check[$arr['stock_id']] = TRUE;
          }

          // Adds node_id and project_id.
          $arr['node_id']     = $node_id;
          $arr['project_id']  = $project_id;

          // Checks the data.
          $data = '';
          foreach ($arr as $key => $value) {
            if (preg_match("/^p\d+$/", $key)) {
              $data .= trim($value);
            }
          }

          // Inserts into mview.
          if ($data) {
            db_insert($mview)
              ->fields($arr)
              ->execute();
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param BIMS_CHADO_ACCESSION $bc_accession
   *
   * @return boolean
   */
  private function _updateColumns(BIMS_PROGRAM $bims_program, $bc_accession) {
    $mview      = $this->getMView();
    $program_id = $bims_program->getProgramID();

    // Gets the table name.
    $table_accessionprop = $bc_accession->getTableName('accessionprop');

    // Adds the property columns.
    $props = $bims_program->getProperties('accession_proj', 'custom', BIMS_OPTION);
    foreach ((array)$props as $cvterm_id => $name) {
      $field = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($mview, $field)) {
        db_add_field($mview, $field, array('type' => 'text'));
      }

      // Updates the select / join.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";
      $join_str .= "
        LEFT JOIN (
          SELECT PROP.stock_id, PROP.value, PROP.rank AS project_id
          FROM {$table_accessionprop} PROP
          WHERE PROP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.stock_id = STOCK.stock_id AND JP$cvterm_id.project_id = :project_id
      ";
    }
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Updates the record.
   *
   * @param array $fields
   */
  public function update($fields) {
    $transaction = db_transaction();
    try {
      $project_id = $fields['project_id'];
      $stock_id   = $fields['stock_id'];

      // Updates the record.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('project_id', $project_id, '=')
        ->condition('stock_id', $stock_id, '=')
      ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the stock.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateStock($details) {

    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'project_id'  => $details['project_id'],
        'stock_id'    => $details['stock_id'],
        'uniquename'  => $details['uniquename'],
        'name'        => $details['name'],
      );

      // Updates the stock.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('project_id', $details['project_id'], '=')
        ->condition('stock_id', $details['stock_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Checks if data exist.
   *
   * @param integer $node_id
   *
   * @return boolean
   */
  public static function existData($node_id) {

    // Gets the mview table.
    $bims_mview = new BIMS_MVIEW_MARKER(array('node_id' => $node_id));
    $mview = $bims_mview->getMView(TRUE);

    // Checks if the mview table exists first.
    if (!db_table_exists($mview)) {
      return FALSE;
    }

    // Checks if the mivew has data.
    $sql = "SELECT COUNT(*) FROM {$mview}";
    $num = db_query($sql)->fetchField();
    return ($num) ? TRUE : FALSE;
  }

  /**
   * Deletes the stock.
   *
   * @param integer $stock_id
   *
   * @return boolean
   */
  public function deleteStock($stock_id) {

    // Deletes the stock.
    $transaction = db_transaction();
    try {

      // Deletes the stock.
      db_delete($this->getMView())
        ->condition('stock_id', $stock_id)
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the stock by project.
   *
   * @param integer $project_id
   * @param integer $stock_id
   *
   * @return boolean
   */
  public function deleteStockByProject($project_id, $stock_id) {

    // Deletes the stock.
    $transaction = db_transaction();
    try {

      // Deletes the stock.
      db_delete($this->getMView())
      ->condition('project_id', $project_id)
      ->condition('stock_id', $stock_id)
      ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new stock.
   *
   * @param array
   *
   * @return boolean
   */
  public function addStock($details) {

    // Add a stock.
    $mview = $this->getMView();
    $node_id = $this->getNodeID();
    $cols = 'node_id';
    $vals = $node_id;
    foreach ($details as $key => $val) {
      $cols .= ", $key";
      $vals .= ", '$val'";
    }
    $sql = "INSERT INTO $mview ($cols) VALUES ($vals)";
    db_query($sql);
    return TRUE;
  }

  /**
   * Returns stock object.
   *
   * @param integer $project_id
   * @param integer $stock_id
   * @param integer $flag
   *
   * @return object|array
   */
  public function getStock($project_id, $stock_id, $flag = BIMS_OBJECT) {
    if ($project_id && $stock_id) {
      $mview = $this->getMView();
      $sql = "
        SELECT MV.* FROM $mview MV
        WHERE MV.project_id = :project_id AND MV.stock_id = :stock_id
      ";
      $args = array(
        ':project_id' => $project_id,
        ':stock_id'   => $stock_id,
      );
      $result = db_query($sql, $args);
      if ($flag == BIMS_OBJECT) {
        return $result->fetchObject();
      }
      return $result->fetchAssoc();
    }
    return NULL;
  }
}