<?php
/**
 * The declaration of BIMS_MVIEW_SSR class.
 *
 */
class BIMS_MVIEW_SSR extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_ssr';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_mview = new BIMS_MVIEW_GENOTYPE(array('node_id' => $node_id));
    if ($bims_mview) {
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for genotyping data.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'root_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'dataset_name' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'stock_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'stock_uniquename' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'accession' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'genus' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'species' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'stock_id_m' => array(
          'type' => 'int',
        ),
        'maternal' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'stock_id_p' => array(
          'type' => 'int',
        ),
        'paternal' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'pedigree' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'origin_detail' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'cultivar' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'feature_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'feature_uniquename' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'marker' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'genotype_id' => array(
          'type'     => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'allele' => array(
          'type'      => 'varchar',
          'length'    => '500',
          'not null'  => TRUE,
        ),
        'chado' => array(
          'type'      => 'int',
          'not null'  => TRUE,
          'default'   => 0,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'stock_id', 'feature_id', 'genotype_id')
      ),
      'indexes' => array(
        'mgssr_node_id'    => array('node_id'),
        'mgssr_project_id' => array('project_id'),
        'mgssr_stock_id'   => array('stock_id'),
        'mgssr_feature_id' => array('feature_id'),
        'mgssr_accession'  => array('accession'),
        'mgssr_marker'     => array('marker'),
        'mgssr_allele'     => array('allele'),
      ),
    );
  }

  /**
   * Returns the alleles of the ginven marker.
   *
   * @param integer $feature_id
   * @param boolean $order
   *
   * @return array
   */
  public function getAlleles($marker, $order = FALSE) {
    $m_ssr = $bims_mview->getMView();
    $sql = "
      SELECT DISTINCT MV.allele FROM {$m_ssr} MV
      WHERE feature_id = :feature_id
    ";
    if ($order) {
      $sql .= " ORDER BY MV.allele";
    }
    $args = array(':feature_id' => $marker->feature_id);
    $alleles = array();
    $results = db_query($sql, $args);
    while ($allele = $results->fetchField()) {
      $alleles[$allele] = $allele;
    }
    return $alleles;
  }

  /**
   * Returns the distinct markers.
   *
   * @return array
   */
  public function getMarkers() {
    $m_ssr  = $bims_mview->getMView();
    $sql    = "SELECT DISTINCT MV.feature_id, MV.marker FROM {$m_ssr} MV";
    $results = db_query($sql);
    $markers = array();
    while ($obj = $results->fetchObject()) {
      $markers[$obj->feature_id] = $obj->marker;
    }
    return $markers;
  }

  /**
   * Returns the marker.
   *
   * @param integer $feature_id
   *
   * @return object|NULL
   */
  public function getMarker($feature_id) {
    $m_ssr  = $this->getMView();
    $sql    = "SELECT MV.* FROM {$m_ssr} MV WHERE MV.feature_id = :feature_id";
    return db_query($sql, array(':feature_id' => $feature_id))->fetchObject();
  }

  /**
   * Returns the distinct accessions.
   *
   * @return array
   */
  public function getAccessions() {
    $m_ssr    = $bims_mview->getMView();
    $sql      = "SELECT DISTINCT MV.stock_id, MV.accession FROM {$m_ssr} MV";
    $results  = db_query($sql);
    $accessions = array();
    while ($obj = $results->fetchObject()) {
      $accessions[$obj->stock_id] = $obj->accession;
    }
    return $accessions;
  }

  /**
   * Returns the stock.
   *
   * @param integer $stock_id
   *
   * @return object|NULL
   */
  public function getStock($stock_id) {
    $m_ssr = $this->getMView();
    $sql = "SELECT MV.* FROM {$m_ssr} MV WHERE MV.stock_id = :stock_id";
    return db_query($sql, array(':stock_id' => $stock_id))->fetchObject();
  }

  /**
   * Updates the accession.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateStock($details) {

    // Updates the accession properties.
    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'stock_uniquename'  => $details['uniquename'],
        'accession'         => $details['name'],
        'genus'             => $details['genus'],
        'species'           => $details['species'],
        'stock_id_m'        => $details['stock_id_m'],
        'maternal'          => $details['maternal'],
        'stock_id_p'        => $details['stock_id_p'],
        'paternal'          => $details['paternal'],
      );

      // Updates the stock.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('stock_id', $details['stock_id'], '=')
        ->execute();

      // Updates the maternal.
      db_update($this->getMView())
        ->fields(array('maternal' => $details['name']))
        ->condition('stock_id_m', $details['stock_id'], '=')
        ->execute();

      // Updates the paternal.
      db_update($this->getMView())
        ->fields(array('paternal' => $details['name']))
        ->condition('stock_id_p', $details['stock_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
    * Updates the marker.
    *
    * @param array $details
    *
    * @return boolean
    */
  public function updateMarker($details) {

    // Updates the marker properties.
    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'feature_uniquename'  => $details['uniquename'],
        'marker'              => $details['name'],
      );

      // Updates the marker.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('feature_id', $details['feature_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::deleteByKey()
   */
  public function deleteByKey($key, $value) {

    // Deletes the data.
    $transaction = db_transaction();
    try {

      // Deletes the data by key.
      db_delete($this->getMView())
        ->condition($key, $value)
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Clears the data of the provided trial ID in the mview.
   *
   * @param integer $trial_id
   *
   * @return boolean
   */
  public function clearTrial($trial_id) {

    // Clears the data in the mview.
    if ($this->exists()) {
      db_delete($this->getMView())
        ->condition('node_id', $trial_id, '=')
        ->execute();
    }

    // Creates the mview if not exist.
    else {
      $this->createMView();
    }
    return TRUE;
  }

  /**
   * Updates the numbers in the mview.
   *
   * @param strring $type
   * @param array $project_ids
   *
   * @return boolean
   */
  public function updateNumbers($type = 'all', $project_ids = array()) {

    // Gets the numbers.
    foreach ((array)$project_ids as $project_id) {
      $trial = BIMS_TRIAL::byProjectID($project_id, $this->node_id);
      if (!$trial) {
        continue;
      }

      // TYPE : number of data.
      if ($type == 'all' || $type == 'data') {

         // Gets the number of genotype in BIMS_MVIEW_GENOTYPE.
        $bm_ssr  = new BIMS_MVIEW_SSR(array('node_id' => $this->node_id));
        $m_ssr   = $bm_ssr->getMView();
        $sql = "
          SELECT COUNT(allele) FROM {$m_ssr}
          WHERE project_id = :project_id
        ";
        $num_data = db_query($sql, array(':project_id' => $project_id))->fetchField();
        $trial->updatePropByKey('num_data', $num_data);
      }
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE, $mview_option = '') {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Finds new trials and adds them to the program if exist.
      $bims_program->updateTrials();

      // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($grouped_ids)) {
        return TRUE;
      }

      // Updates the mview option.
      if (!$mview_option) {
        $mview_option = 'data';
      }

      // Updating the mview for data.
      $flag_data = FALSE;
      if (preg_match("/data/", $mview_option)) {
        $flag_data = TRUE;

        // Clears the mview.
        if ($clear) {
          BIMS_MVIEW::clearByGroupID($this->getMView(), 'project_id', $grouped_ids);
        }

        // Updates the mviews by schema.
        if (!$this->_updateMViewByChado($bims_program, $grouped_ids['chado'])) {
          throw new Exception("Error : Failed to update the mview data in Chado");
        }
        if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['bims'])) {
          throw new Exception("Error : Failed to update the mview data in BIMS");
        }
      }

      // Updating the numbers.
      if (preg_match("/num/", $mview_option) && !$flag_data) {
        if (!$this->updateNumbers('all', $grouped_ids['chado'])) {
          throw new Exception("Error : Failed to update the numbers");
        }
        if (!$this->updateNumbers('all', $grouped_ids['bims'])) {
          throw new Exception("Error : Failed to update the numbers");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByChado(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsChado($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'cultivar'      => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'cultivar')->getCvtermID(),
        'origin_detail' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'origin_detail')->getCvtermID(),
        'pedigree'      => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'pedigree')->getCvtermID(),
        'cross_number'  => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'cross_number')->getCvtermID(),
      );

      // Populates the mview.
      $accessions = array();
      $markers    = array();
      foreach ($grouped_ids as $project_id) {
        $trial = BIMS_TRIAL::byProjectID($project_id, $this->node_id);
        $sql = "
          SELECT VARIETY.* $select_str
          FROM (
            SELECT P.project_id, P.name AS dataset_name, NEPR.nd_experiment_id AS nd_experiment_id_ssr,
              S.stock_id, S.uniquename AS accession, S.uniquename AS stock_uniquename,
              F.feature_id, F.uniquename AS marker, F.uniquename AS feature_uniquename,
              O.genus, O.species, G.genotype_id, G.description AS allele,
              CULTIVAR.value AS cultivar, PEDIGREE.value AS pedigree,
              CROSS_DATA.nd_experiment_id, CROSS_DATA.cross_number,
              ORIGIN_DETAIL.value AS origin_detail
            FROM {chado.project} P
              INNER JOIN {chado.nd_experiment_project} NEPR on NEPR.project_id = P.project_id
              INNER JOIN {chado.nd_experiment_stock} NES on NES.nd_experiment_id = NEPR.nd_experiment_id
              INNER JOIN {chado.nd_experiment_genotype} NEG on NEG.nd_experiment_id = NEPR.nd_experiment_id
              INNER JOIN {chado.stock} SAMPLE on SAMPLE.stock_id = NES.stock_id
              INNER JOIN {chado.stock_relationship} SR on SR.subject_id = SAMPLE.stock_id
              INNER JOIN {chado.stock} S on S.stock_id = SR.object_id
              INNER JOIN {chado.organism} O on O.organism_id = S.organism_id
              INNER JOIN {chado.feature_genotype} FG on FG.genotype_id = NEG.genotype_id
              INNER JOIN {chado.feature} F on F.feature_id = FG.feature_id
              INNER JOIN {chado.genotype} G on G.genotype_id = NEG.genotype_id
              LEFT JOIN (
                SELECT SP.stock_id, SP.value
                FROM {chado.stockprop} SP
                WHERE SP.type_id = :cultivar
              ) CULTIVAR on CULTIVAR.stock_id = S.stock_id
              LEFT JOIN (
                SELECT SP.stock_id, SP.value
                FROM {chado.stockprop} SP
                WHERE SP.type_id = :origin_detail
              ) ORIGIN_DETAIL on ORIGIN_DETAIL.stock_id = S.stock_id
              LEFT JOIN (
                SELECT SP.stock_id, SP.value
                FROM {chado.stockprop} SP
                WHERE SP.type_id = :pedigree
              ) PEDIGREE on PEDIGREE.stock_id = S.stock_id
              LEFT JOIN (
                SELECT NES.stock_id, NES.nd_experiment_id, NEP.value AS cross_number
                FROM {chado.nd_experiment_stock} NES
                INNER JOIN {chado.nd_experimentprop} NEP on NEP.nd_experiment_id = NES.nd_experiment_id
                WHERE NEP.type_id = :cross_number
              ) CROSS_DATA on CROSS_DATA.stock_id = S.stock_id
            WHERE P.project_id = :project_id
          ) VARIETY $join_str
        ";
        $args = array(
          ':project_id'     => $project_id,
          ':cultivar'       => $cvterms['cultivar'],
          ':origin_detail'  => $cvterms['origin_detail'],
          ':pedigree'       => $cvterms['pedigree'],
          ':cross_number'   => $cvterms['cross_number'],
        );

        // Counts the number of data.
        $sql_count  = "SELECT COUNT(T.*) FROM ($sql) T";
        $total_data = db_query($sql_count, $args)->fetchField();

        // Adds the data.
        $num_data = 0;
        if ($total_data) {
          $results = db_query($sql, $args);
          while ($arr = $results->fetchAssoc()) {
            $accessions[$arr['stock_id']] = TRUE;
            $markers[$arr['feature_id']]  = TRUE;
            $num_data++;

            // Shows the progress.
            if ($num_data % 5000 == 0) {
              $msg = sprintf("%d / %d = %.03f %%", $num_data, $total_data, $num_data / $total_data * 100.0);
              bims_print($msg);
            }
            // Initializes the fields.
            $fields = array();

            // Adds node ID.
            $fields['node_id']  = $trial->getNodeID();
            $fields['root_id']  = $trial->getRootID();
            $fields['chado']    = 1;
            foreach ($arr as $key => $val) {

              // Skips if no value.
              if (!($val || $val == '0')) {
                continue;
              }
              if ($key == 'nd_experiment_id_ssr') {
                $fields['nd_experiment_id'] = $val;
              }
              else {
                $fields[$key] = $val;
              }
            }

            // Inserts into mview.
            db_insert($this->getMView())
              ->fields($fields)
              ->execute();
          }
        }

        // Stores the number of data.
        $trial->setPropByKey('num_data', $num_data);
        if (!$trial->update()) {
          throw new Exception("Error : Failed to update the number of the data");
        }
      }

      // Updates BIMS_MVIEW_STOCK.
      if (!empty($accessions)) {
        $bims_mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $trial->getRootID()));

        // Imports the accessions.
        $bims_mview_stock->updateMView(FALSE, 'stock', array_keys($accessions), FALSE);

        // Updates the number of SNP.
        $bims_mview_stock->updateNumbers('ssr', array_keys($accessions));
      }

      // Updates BIMS_MVIEW_MARKER.
      if (!empty($markers)) {
        $bm_marker = new BIMS_MVIEW_MARKER(array('node_id' => $trial->getRootID()));

        // Imports the markers.
        $bm_marker->updateMView(FALSE, 'feature', array_keys($markers), FALSE);

        // Updates the number of SSR.
        $bm_marker->updateNumbers('ssr', array_keys($markers));
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByBIMS(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Local variables.
      $program_id = $bims_program->getProgramID();

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsBIMS($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'ar_sample'     => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'sample_of')->getCvtermID(),
        'ar_maternal'   => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'is_a_maternal_parent_of')->getCvtermID(),
        'ar_paternal'   => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'is_a_paternal_parent_of')->getCvtermID(),
        'cultivar'      => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'cultivar')->getCvtermID(),
        'origin_detail' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'origin_detail')->getCvtermID(),
        'pedigree'      => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'pedigree')->getCvtermID(),
      );

      // Gets BIMS_CHADO tables.
      $bc_project           = new BIMS_CHADO_PROJECT($program_id);
      $bc_accession         = new BIMS_CHADO_ACCESSION($program_id);
      $bc_feature           = new BIMS_CHADO_FEATURE($program_id);
      $bc_nde               = new BIMS_CHADO_ND_EXPERIMENT($program_id);
      $bc_genotype          = new BIMS_CHADO_GENOTYPE($program_id);
      $table_project        = $bc_project->getTableName('project');
      $table_accession      = $bc_accession->getTableName('accession');
      $table_accessionprop  = $bc_accession->getTableName('accessionprop');
      $table_accessionrel   = $bc_accession->getTableName('accessionrel');
      $table_genotype       = $bc_genotype->getTableName('genotype');
      $table_feature        = $bc_feature->getTableName('feature');
      $table_fg             = $bc_feature->getTableName('feature_genotype');
      $table_nde            = $bc_nde->getTableName('nd_experiment');

      // Populates the mview.
      $accessions = array();
      $markers    = array();
      foreach ($grouped_ids as $project_id) {
        $trial = BIMS_TRIAL::byProjectID($project_id, $this->node_id);
        $sql = "
          SELECT VARIETY.* $select_str
          FROM (
            SELECT DISTINCT PROJ.project_id, PROJ.name AS dataset_name, NE.nd_experiment_id,
              ACCESSION.stock_id, ACCESSION.uniquename AS stock_uniquename,
              ACCESSION.name AS accession, O.genus, O.species, MATERNAL.stock_id_m,
              MATERNAL.maternal, PATERNAL.stock_id_p, PATERNAL.paternal,
              F.feature_id, F.name AS marker, F.uniquename AS feature_uniquename,
              CULTIVAR.value AS cultivar, PEDIGREE.value AS pedigree,
              ORIGIN_DETAIL.value AS origin_detail, G.genotype_id, G.description AS allele
            FROM {$table_nde} NE
              INNER JOIN {$table_project} PROJ on PROJ.project_id = NE.project_id
              INNER JOIN {$table_accession} SAMPLE on SAMPLE.stock_id = NE.stock_id
              INNER JOIN {$table_accessionrel} AR on AR.subject_id = SAMPLE.stock_id
              INNER JOIN {$table_accession} ACCESSION on ACCESSION.stock_id = AR.object_id
              INNER JOIN {chado.organism} O on O.organism_id = ACCESSION.organism_id
              INNER JOIN {$table_genotype} G on G.genotype_id = NE.genotype_id
              INNER JOIN {$table_fg} FG on FG.genotype_id = G.genotype_id
              INNER JOIN {$table_feature} F on F.feature_id = FG.feature_id
              LEFT JOIN (
                SELECT AR.subject_id, A.stock_id AS stock_id_m, A.name AS maternal
                FROM {$table_accessionrel} AR
                  LEFT JOIN $table_accession} A on A.stock_id = AR.object_id
                WHERE AR.type_id = :ar_maternal
              ) MATERNAL on MATERNAL.subject_id = ACCESSION.stock_id
              LEFT JOIN (
                SELECT AR.subject_id, A.stock_id AS stock_id_p, A.name AS paternal
                FROM {$table_accessionrel} AR
                  LEFT JOIN $table_accession} A on A.stock_id = AR.object_id
                WHERE AR.type_id = :ar_paternal
              ) PATERNAL on PATERNAL.subject_id = ACCESSION.stock_id
              LEFT JOIN (
                SELECT SP.stock_id, SP.value
                FROM {$table_accessionprop} SP
                WHERE SP.type_id = :cultivar
              ) CULTIVAR on CULTIVAR.stock_id = ACCESSION.stock_id
              LEFT JOIN (
                SELECT SP.stock_id, SP.value
                FROM {$table_accessionprop} SP
                WHERE SP.type_id = :origin_detail
              ) ORIGIN_DETAIL on ORIGIN_DETAIL.stock_id = ACCESSION.stock_id
              LEFT JOIN (
                SELECT SP.stock_id, SP.value
                FROM {$table_accessionprop} SP
                WHERE SP.type_id = :pedigree
              ) PEDIGREE on PEDIGREE.stock_id = ACCESSION.stock_id
            WHERE NE.project_id = :project_id AND AR.type_id = :ar_sample
          ) VARIETY $join_str
        ";
        $args = array(
          ':project_id'     => $project_id,
          ':ar_sample'      => $cvterms['ar_sample'],
          ':ar_maternal'    => $cvterms['ar_maternal'],
          ':ar_paternal'    => $cvterms['ar_paternal'],
          ':cultivar'       => $cvterms['cultivar'],
          ':origin_detail'  => $cvterms['origin_detail'],
          ':pedigree'       => $cvterms['pedigree'],
        );

        // Counts the number of data.
        $sql_count  = "SELECT COUNT(T.*) FROM ($sql) T";
        $total_data = db_query($sql_count, $args)->fetchField();

        // Adds the data.
        $num_data = 0;
        if ($total_data) {
          $results = db_query($sql, $args);
          while ($arr = $results->fetchAssoc()) {
            $accessions[$arr['stock_id']] = TRUE;
            $markers[$arr['feature_id']]  = TRUE;
            $num_data++;

            // Shows the progress.
            if ($num_data_point % 5000 == 0) {
              $msg = sprintf("%d / %d = %.03f %%", $num_data, $total_data, $num_data / $total_data * 100.0);
              bims_print($msg);
            }

            // Initializes the fields.
            $fields = array();

            // Adds node ID.
            $fields['node_id'] = $trial->getNodeID();
            $fields['root_id'] = $trial->getRootID();
            foreach ($arr as $key => $val) {

              // Skips if no value.
              if (!($val || $val == '0')) {
                continue;
              }

              // Adds the value.
              $fields[$key] = $val;
            }

            // Inserts into the mview.
            db_insert($this->getMView())
              ->fields($fields)
              ->execute();
          }
        }

        // Stores the number of data in BIMS_TRIAL prop.
        $trial->setPropByKey('num_data', $num_data);
        if (!$trial->update()) {
          throw new Exception('Error : Failed to update trial properties');
        }
      }

      // Updates the number of SSR on BIMS_MVIEW_STOCK.
      if (!empty($accessions)) {
        $bims_mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
        $bims_mview_stock->updateNumbers('ssr', array_keys($accessions));
      }

      // Updates the number of SSR on BIMS_MVIEW_MARKER.
      if (!empty($markers)) {
        $bm_marker = new BIMS_MVIEW_MARKER(array('node_id' => $program_id));
        $bm_marker->updateNumbers('ssr', array_keys($markers));
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsChado(BIMS_PROGRAM $bims_program) {
    return array(
      'error'       => FALSE,
      'select_str'  => '',
      'join_str'    => '',
    );
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsBIMS(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_ssr      = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
    $bc_feature   = new BIMS_CHADO_FEATURE($program_id);
    $bc_genotype  = new BIMS_CHADO_GENOTYPE($program_id);
    $bc_nde       = new BIMS_CHADO_ND_EXPERIMENT($program_id);
    $table_ap     = $bc_accession->getTableName('accessionprop');
    $table_fp     = $bc_feature->getTableName('featureprop');
    $table_gp     = $bc_genotype->getTableName('genotypeprop');
    $table_nde    = $bc_nde->getTableName('nd_experiment');
    $table_ndep   = $bc_nde->getTableName('nd_experimentprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';

    // Returns if no BIMS CHADO.
    if (!db_table_exists($table_ap)) {
      return array(
        'error'       => FALSE,
        'select_str'  => $select_str,
        'join_str'    => $join_str,
      );
    }

    // Adds accession custom properties columns.
    $accession_props  = $bims_program->getProperties('accession', 'custom', BIMS_OPTION);
    foreach ((array)$accession_props as $cvterm_id => $name) {
      $field_name = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_ssr, $field_name)) {
        db_add_field($m_ssr, $field_name, array('type' => 'text'));
      }

      // Updates the select statement.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";

      // Updates the join statement.
      $join_str .= "
        LEFT JOIN (
          SELECT AP.stock_id, AP.value
          FROM {$table_ap} AP
          WHERE AP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.stock_id = VARIETY.stock_id
      ";
    }

    // Adds marker custom properties columns.
    $marker_props = $bims_program->getProperties('marker', 'custom', BIMS_OPTION);
    foreach ((array)$marker_props as $cvterm_id => $name) {
      $field_name = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_ssr, $field_name)) {
        db_add_field($m_ssr, $field_name, array('type' => 'text'));
      }

      // Updates the select / join.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";
      $join_str .= "
        LEFT JOIN (
          SELECT FP.feature_id, FP.value
          FROM {$table_fp} FP
          WHERE FP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.feature_id = VARIETY.feature_id
      ";
    }

    // Adds the genotype custom properties columns.
    $genotype_props = $bims_program->getProperyByCV('genotype');
    foreach ((array)$genotype_props as $cvterm_id => $name) {
      $field_name = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_ssr, $field_name)) {
        db_add_field($m_ssr, $field_name, array('type' => 'text'));
      }

      // Updates the select / join.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";
      $join_str .= "
        LEFT JOIN (
          SELECT GP.genotype_id, GP.value
          FROM {$table_gp} GP
          WHERE GP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.genotype_id = VARIETY.genotype_id
      ";
    }
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Initializes the returning array.
    $grouped_ids = array('chado' => array(), 'bims' => array());

    // GROUP : private.
    $grouped_ids = array('chado' => array(), 'bims' => array());
    if ($group == 'private') {
      $sql = "
        SELECT N.project_id FROM {bims_node} N
        WHERE LOWER(N.project_sub_type) = :sub_type
          AND N.root_id = :program_id AND N.project_id NOT IN (
            SELECT project_id
            FROM {bims_imported_project}
            WHERE program_id = :program_id
          )";
      $args = array(
        ':program_id'  => $this->node_id,
        ':sub_type'    => 'ssr',
      );
      $results = db_query($sql, $args);
      while ($project_id = $results->fetchField()) {
        $grouped_ids['bims'][$project_id] = TRUE;
      }
    }

    // GROUP : public.
    else if ($group == 'public') {
      $sql = "
        SELECT N.project_id
        FROM {bims_node} N
          INNER JOIN {bims_imported_project} IP on IP.project_id = N.project_id
        WHERE LOWER(N.project_sub_type) = :sub_type AND IP.program_id = :program_id
      ";
      $args = array(
        ':program_id'  => $this->node_id,
        ':sub_type'    => 'ssr',
      );
      $results = db_query($sql, $args);
      while ($project_id = $results->fetchField()) {
        $grouped_ids['chado'][$project_id] = TRUE;
      }
    }

    // GROUP : project.
    else if ($group == 'project') {
      $id_list = implode(",", $group_ids);
      $sql = "
        SELECT N.project_id, BIP.project_id AS imported
        FROM {bims_node} N
          LEFT JOIN {bims_imported_project} BIP on BIP.project_id = N.project_id
        WHERE LOWER(N.project_sub_type) = :sub_type
          AND N.root_id = :program_id AND N.project_id IN ($id_list)
      ";
      $args = array(
        ':program_id'  => $this->node_id,
        ':sub_type'    => 'ssr',
      );
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        $schema = $obj->imported ? 'chado' : 'bims';
        $grouped_ids[$schema][$obj->project_id] = TRUE;
      }
    }

    // GROUP : program (private + public).
    else {
      $sql = "
        SELECT N.project_id, IP.project_id AS imported
        FROM {bims_node} N
          LEFT JOIN {bims_imported_project} IP on IP.project_id = N.project_id
        WHERE LOWER(N.project_sub_type) = :sub_type AND N.root_id = :program_id
      ";
      $args = array(
        ':program_id'  => $this->node_id,
        ':sub_type'    => 'ssr',
      );
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        $schema = $obj->imported ? 'chado' : 'bims';
        $grouped_ids[$schema][$obj->project_id] = TRUE;
      }
    }

    // Returns the grouped IDs.
    $ret_array = array('bims' => array(), 'chado' => array());
    if (empty($grouped_ids['chado']) && empty($grouped_ids['bims'])) {
      return array();
    }
    if (!empty($grouped_ids['bims'])) {
      $ret_array['bims'] = array_keys($grouped_ids['bims']);
    }
    if (!empty($grouped_ids['chado'])) {
      $ret_array['chado'] = array_keys($grouped_ids['chado']);
    }
    return $ret_array;
  }
}
