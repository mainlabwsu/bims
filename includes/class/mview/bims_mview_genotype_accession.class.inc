<?php
/**
 * The declaration of BIMS_MVIEW_GENOTYPE_ACCESSION class.
 *
 */
class BIMS_MVIEW_GENOTYPE_ACCESSION extends BIMS_MVIEW_GENOTYPE {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_genotype_accession';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_mview = new BIMS_MVIEW_GENOTYPE_ACCESSION(array('node_id' => $node_id));
    if ($bims_mview) {
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for genotyping data for accession columns.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'root_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'dataset_name' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'organism_id' => array(
          'type'     => 'int',
          'not null' => TRUE,
        ),
        'genus' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'species' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'stock_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'accession' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'stock_uniquename' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'stock_id_m' => array(
          'type' => 'int',
        ),
        'maternal' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'stock_id_p' => array(
          'type' => 'int',
        ),
        'paternal' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'nd_experiment_id' => array(
          'type' => 'int',
        ),
        'cross_number' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'pedigree' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'origin_detail' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'cultivar' => array(
          'type'    => 'varchar',
          'length'  => '500',
        ),
        'chado' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'unique keys' => array(),
      'indexes' => array(
        'mg_ga_node_id'           => array('node_id'),
        'mg_ga_project_id'        => array('project_id'),
        'mg_ga_stock_id'          => array('stock_id'),
        'mg_ga_accession'         => array('accession'),
        'mg_ga_stock_id_m'        => array('stock_id_m'),
        'mg_ga_stock_id_p'        => array('stock_id_p'),
        'mg_ga_nd_experiment_id'  => array('nd_experiment_id'),
      ),
    );
  }

  /**
   * Returns the stock.
   *
   * @param integer $stock_id
   *
   * @return object|NULL
   */
  public function getStock($stock_id) {
    $m_ga = $this->getMView();
    $sql = "SELECT MV.* FROM {$m_ga} MV WHERE MV.stock_id = :stock_id";
    return db_query($sql, array(':stock_id' => $stock_id))->fetchObject();
  }

  /**
   * Updates the accession.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateStock($details) {

    // Updates the accession properties.
    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'stock_uniquename'  => $details['uniquename'],
        'accession'         => $details['name'],
        'genus'             => $details['genus'],
        'species'           => $details['species'],
        'stock_id_m'        => $details['stock_id_m'],
        'maternal'          => $details['maternal'],
        'stock_id_p'        => $details['stock_id_p'],
        'paternal'          => $details['paternal'],
      );

      // Updates the stock.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('stock_id', $details['stock_id'], '=')
        ->execute();

      // Updates the maternal.
      db_update($this->getMView())
        ->fields(array('maternal' => $details['name']))
        ->condition('stock_id_m', $details['stock_id'], '=')
        ->execute();

      // Updates the paternal.
      db_update($this->getMView())
        ->fields(array('paternal' => $details['name']))
        ->condition('stock_id_p', $details['stock_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::deleteByKey()
   */
  public function deleteByKey($key, $value) {

    // Deletes the data.
    $transaction = db_transaction();
    try {

      // Deletes the data by key.
      db_delete($this->getMView())
        ->condition($key, $value)
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Clears the data of the provided trial ID in the mview.
   *
   * @param integer $trial_id
   *
   * @return boolean
   */
  public function clearTrial($trial_id) {

    // Clears the data in the mview.
    if ($this->exists()) {
      db_delete($this->getMView())
        ->condition('node_id', $trial_id, '=')
        ->execute();
    }

    // Creates the mview if not exist.
    else {
      $this->createMView();
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE, $mview_option = '') {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids, TRUE);
      if (empty($grouped_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'project_id', $grouped_ids, TRUE);
      }

      // Updates the mview.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);
      if (!$this->_updateMView($bims_program, $grouped_ids)) {
        throw new Exception("Error : Failed to update the mview data");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumns(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_ga       = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
    $table_ap     = $bc_accession->getTableName('accessionprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';

    // Adds the accession custom properties columns.
    $accession_props = $bims_program->getProperties('accession', 'custom', BIMS_OPTION);
    foreach ((array)$accession_props as $cvterm_id => $name) {
      $field_name = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_ga, $field_name)) {
        db_add_field($m_ga, $field_name, array('type' => 'text'));
      }

      // Updates the select statement.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";

      // Updates the join statement.
      $join_str .= "
        LEFT JOIN (
          SELECT SP.stock_id, SP.value
          FROM {$table_ap} SP
          WHERE SP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.stock_id = G.stock_id
      ";
    }
    return array(
      'error'           => FALSE,
      'select_str'      => $select_str,
      'join_str'        => $join_str,
      'accession_props' => $accession_props,
    );
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMView(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumns($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets BIMS_MVIEW tables.
      $bm_genotype  = new BIMS_MVIEW_GENOTYPE(array('node_id' => $this->node_id));
      $bm_stock     = new BIMS_MVIEW_STOCK(array('node_id' => $this->node_id));
      $m_genotype   = $bm_genotype->getMView();
      $m_stock      = $bm_stock->getMView();

      // Populates the mview.
      $id_list = implode(',', $grouped_ids);
      $sql = "
        SELECT DISTINCT G.project_id, G.dataset_name, G.stock_id,
          S.uniquename AS stock_uniquename, S.name AS accession,
          S.organism_id, S.genus, S.species, S.stock_id_m, S.maternal,
          S.stock_id_p, S.paternal, S.nd_experiment_id, S.cross_number,
          S.pedigree, S.origin_detail, S.cultivar, S.chado,
          N.root_id, N.node_id
          $select_str
        FROM {$m_genotype} G
          INNER JOIN {bims_node} N on N.project_id = G.project_id
          INNER JOIN {$m_stock} S on S.stock_id = G.stock_id
          $join_str
        WHERE G.project_id IN ($id_list)
      ";
      $results = db_query($sql, array(':project_id' => $project_id));
      while ($arr = $results->fetchAssoc()) {

        // Inserts into the mview.
        db_insert($this->getMView())
          ->fields($arr)
          ->execute();
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }
}
