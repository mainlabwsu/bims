<?php
/**
 * The declaration of BIMS_MVIEW_ALIAS class.
*
*/
class BIMS_MVIEW_ALIAS extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_alias';
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for alias.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'target_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
//        'alias_id' => array(
//          'type' => 'int',
//          'not null' => TRUE,
//        ),
        'type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'alias' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'chado' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('target_id', 'type', 'alias')
      ),
      'indexes' => array(
        'idx_001' => array('alias'),
      ),
    );
  }

  /**
   * Clears the alias by IDs.
   *
   * @param integer $type
   * @param string $target_id
   *
   * @return boolean
   */
  public function clearAliasByID($type, $target_ids) {
    if (!empty($target_ids)) {
      $m_alias  = $this->getMView();
      $id_list  = implode(',', $target_ids);
      $sql = "
        DELETE FROM {$m_alias}
        WHERE node_id = :node_id AND target_id IN ($id_list)
      ";
      db_query($sql, array(':node_id' => $this->node_id));
    }
    return TRUE;
  }

  /**
   * Deletes the aliases by type and ID.
   *
   * @param integer $type
   * @param string $target_id
   *
   * @return boolean
   */
  public function deleteAliasByID($type, $target_id) {
    db_delete($this->getMView())
      ->condition('type', strtolower($type))
      ->condition('target_id', $target_id)
      ->execute();
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE) {
    // Recreates the mview.
    if ($recreate) {
      $this->dropMView();
      $this->createMView();
    }
    return TRUE;
  }

  /**
   * Update the mview of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $group_ids
   * @param boolean $clear
   *
   * @return boolean
   */
  public function updateAlias($schema, $group_ids = array(), $clear = TRUE) {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Update the mview of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $group_ids
   *
   * @return boolean
   */
  protected function _updateAliasChado(BIMS_PROGRAM $bims_program, $group_ids) {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Update the mview of the data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $group_ids
   *
   * @return boolean
   */
  protected function _updateAliasBIMS(BIMS_PROGRAM $bims_program, $group_ids) {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Updates the record.
   *
   * @param $fields
   *
   * @return boolean
   */
  public function update($fields) {
    $transaction = db_transaction();
    try {
      $target_id  = $fields['target_id'];

      // Updates the record.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('node_id', $this->node_id, '=')
        ->condition('target_id', $target_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the alias by the list of aliases.
   *
   * @param string $type
   * @param integer $target_id
   * @param string $alias_list
   * @param string $separator
   *
   * @return boolean
   */
  public function updateAliasByList($type, $target_id, $alias_list, $separator = '[,;]') {

    // Updates the alias the current aliases are not the same.
    $alias_str = bims_cleanup_multi_itmes($alias_list, '[\/]',  ' / ');
    $cur_aliases = $this->getAlias($type, $target_id, BIMS_STRING, ' / ');
    if ($alias_str == $cur_aliases) {
      return TRUE;
    }

    // Updates the aliases.
    $transaction = db_transaction();
    try {

      // Deletes all aliases.
      $this->deleteAliasByID($type, $target_id);

      // Adds the aliases.
      $aliases = preg_split(BIMS_CHADO_TABLE::getSepRegex($separator), $alias_list, -1, PREG_SPLIT_NO_EMPTY);
      foreach ($aliases as $alias) {
        $alias = trim($alias);
        if (!$alias) {
          continue;
        }
        $details = array(
          'target_id' => $target_id,
          'type'      => $type,
          'alias'     => $alias,
        );
        if (!$this->addAlias($details)) {
          throw new Exception("Error : Failed to add an alias");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new alias.
   *
   * @param array
   *
   * @return boolean
   */
  public function addAlias($details) {

    // Checks for duplication.
    $m_alias = $this->getMView();
    $sql = "
      SELECT COUNT(target_id) FROM {$m_alias}
      WHERE target_id = :target_id AND LOWER(type) = LOWER(:type)
        AND LOWER(alias) = LOWER(:alias)
    ";
    $args = array(
      ':target_id' => $details['target_id'],
      ':type'      => $details['type'],
      ':alias'     => $details['alias'],
    );
    $num = db_query($sql, $args)->fetchField();
    if (!$num) {

      // Add an alias.
      $transaction = db_transaction();
      try {
        $details['node_id'] = $this->getNodeID();
        db_insert($m_alias)
          ->fields($details)
          ->execute();
      }
      catch (\Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Returns all aliases.
   *
   * @param string $type
   * @param integer $target_id
   * @param integer $flag
   *
   * @return array
  */
  public function getAlias($type, $target_id, $flag = BIMS_OBJECT, $glue = '; ') {
    $aliases = array();

    // Gets all the alias of the given target ID.
    $m_alias = $this->getMView();
    if (db_table_exists($m_alias)) {
      $sql = "
        SELECT MV.* FROM $m_alias MV
        WHERE LOWER(MV.type) = LOWER(:type) AND MV.target_id = :target_id
        ORDER BY MV.alias
      ";
      $args = array(
        ':type'       => $type,
        ':target_id'  => $target_id,
      );
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        if ($flag == BIMS_OPTION) {
          $aliases[$obj->alias_id] = $obj->alias;
        }
        if ($flag == BIMS_STRING) {
          $aliases []= $obj->alias;
        }
        else {
          $aliases []= $obj;
        }
      }
      if ($flag == BIMS_STRING) {
        return implode($glue, $aliases);
      }
    }
    return aliases;
  }
}