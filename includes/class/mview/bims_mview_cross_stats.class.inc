<?php
/**
 * The declaration of BIMS_MVIEW_CROSS_STATS class.
 *
 */
class BIMS_MVIEW_CROSS_STATS extends PUBLIC_BIMS_MVIEW_CROSS_STATS {

  /**
   * Class data members.
   */
  protected $stats_arr = NULL;

  /**
   * @see PUBLIC_BIMS_MVIEW_CROSS_STATS::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates stats array ($this->stats_arr).
    if ($this->stats == '') {
      $this->stats_arr = array();
    }
    else {
      $this->stats_arr = json_decode($this->stats, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_CROSS_STATS::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_CROSS_STATS::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_CROSS_STATS::insert()
   */
  public function insert() {

    // Updates the parent:$stats fields.
    $this->stats = json_encode($this->stats_arr);

    // Inserts a new dataset stats.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_CROSS_STATS::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$stats fields.
    $this->stats = json_encode($this->stats_arr);

    // Updates the dataset stats.
    return parent::update($new_values);
  }

  /**
   * Clear the previous data.
   *
   * @param array $nd_experiment_ids
   *
   * @return boolean
   */
  public function clear($nd_experiment_ids = array()) {
    $sql = "DELETE FROM {bims_mview_cross_stats} WHERE node_id = :node_id";
    if (!empty($nd_experiment_ids)) {
      $sql .= ' AND nd_experiment_id IN (' . implode(',', $nd_experiment_ids) . ')';
    }
    db_query($sql, array(':node_id' => $this->node_id));
  }

  /**
   * Updates the statistics.
   *
   * @param integer $group
   * @param array $group_ids
   * @param boolean $update_num
   *
   * @return boolean
   */
  public function updateStats($group, $group_ids = array(), $update_num = TRUE) {

    // Updates the stats.
    $transaction = db_transaction();
    try {

      // Gets the cross IDs by the group.
      $cross_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($cross_ids)) {
        return TRUE;
      }

      // Clears the stats.
      $this->clear($cross_ids);

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Calculates the stats.
      if (!$this->_calcStats($bims_program, $cross_ids)) {
        throw new Exception("Error : Failed to caluctate the cross stats");
      }

      // Updates the numbers.
      if ($update_num) {
        $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $this->node_id));
        if (!$bm_cross->updateNumbers('all', $cross_ids)) {
          throw new Exception("Error : Failed to update the breeding in BIMS_MVIEW_CROSS");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Calculates the statistics.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $nd_experiment_ids
   *
   * @return boolean
   */
  private function _calcStats(BIMS_PROGRAM $bims_program, $nd_experiment_ids) {

    // Local variables.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
    $m_phenotype  = $bm_phenotype->getMView();
    $bims_stats   = new BIMS_STATS();

    // Calculates the stats.
    $transaction = db_transaction();
    try {

      // Computes the stats of the crosses.
      foreach ($nd_experiment_ids as $nd_experiment_id) {

        // Gets all progenies.
        $details = array(
          'node_id'           => $this->node_id,
          'nd_experiment_id'  => $nd_experiment_id,
        );
        $bm_cross     = new BIMS_MVIEW_CROSS($details);
        $progeny_ids  = $bm_cross->getProgenies($nd_experiment_id, BIMS_ID);

        // Calculates the stats for each active descriptor.
        $params = array(
          'flag'    => BIMS_CLASS,
          'program' => 'program',
        );
        $descriptors = $bims_program->getActiveDescriptors($params);
        foreach ((array)$descriptors as $descriptor) {

          // Prepare the calculation of the stats for the descriptor.
          $param = array(
            'args'    => array(),
            'from'    => $bm_phenotype->getMView(),
            'name'    => $descriptor->getName(),
            'field'   => 't' . $descriptor->getCvtermID(),
            'format'  => $descriptor->getFormat(),
          );

          // Adds the progeny list as a condition.
          if (!empty($progeny_ids)) {
            $progeny_list = implode(',', $progeny_ids);
            $param['condition'] = " stock_id IN ($progeny_list) ";
          }

          // Calculate the stats.
          $stats_arr = $bims_stats->calcStats($param);
          if (!empty($stats_arr)) {
            $details = array(
              'node_id'           => $this->node_id,
              'nd_experiment_id'  => $nd_experiment_id,
              'cvterm_id'         => $descriptor->getCvtermID(),
              'name'              => $descriptor->getName(),
              'num_data'          => $stats_arr['num'],
              'stats'             => json_encode($stats_arr),
            );
            $bm_cross_stats = new BIMS_MVIEW_CROSS_STATS($details);
            if (!$bm_cross_stats->insert()) {
              throw new Exception("Error : Failed to insert the stats for $cvterm_id");
            }
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the statistics for the cross.
   */
  public function updateMView() {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      $this->clear();

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Gets BIMS_CROSS and BIMS_MVIEW_PHENOTYPE.
      $bm_cross     = new BIMS_MVIEW_CROSS(array('node_id' => $this->node_id));
      $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
      $m_phenotype  = $bm_phenotype->getMView();

      // Gets all crosses.
      $crosses = $bm_cross->getCrosses(BIMS_OBJECT);
      foreach ($crosses as $cross) {

        // Gets all progenies.
        $details = array(
          'node_id'           => $cross->node_id,
          'nd_experiment_id'  => $cross->nd_experiment_id,
        );
        $bm_cross_sub = new BIMS_MVIEW_CROSS($details);
        $progeny_ids  = $bm_cross_sub->getProgenies($cross->nd_experiment_id, BIMS_ID);

        // Calculates the stats for each active descriptor.
        $params = array(
          'flag'    => BIMS_CLASS,
          'program' => 'program',
        );
        $descriptors = $bims_program->getActiveDescriptors($params);
        $bims_stats = new BIMS_STATS();
        foreach ((array)$descriptors as $descriptor) {

          // Prepare the calculation of the stats for the descriptor.
          $param = array(
            'args'    => array(),
            'from'    => $m_phenotype,
            'name'    => $descriptor->getName(),
            'field'   => 't' . $descriptor->getCvtermID(),
            'format'  => $descriptor->getFormat(),
          );

          // Adds the progeny list as a condition.
          if (!empty($progeny_ids)) {
            $progeny_list = implode(',', $progeny_ids);
            $param['condition'] = " stock_id IN ($progeny_list) ";
          }

          // Calculate the stats.
          $stats_arr = $bims_stats->calcStats($param);
          if (!empty($stats_arr)) {
            $details = array(
              'node_id'           => $cross->node_id,
              'nd_experiment_id'  => $cross->nd_experiment_id,
              'cvterm_id'         => $descriptor->getCvtermID(),
              'name'              => $descriptor->getName(),
              'num_data'          => $stats_arr['num'],
              'stats'             => json_encode($stats_arr),
            );
            $bm_cross_stats = new BIMS_MVIEW_CROSS_STATS($details);
            if (!$bm_cross_stats->insert()) {
              throw new Exception("Error : Failed to insert the stats for $cvterm_id");
            }
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the statistical descriptors.
   *
   * @param integer $nd_experiment_id
   * @param integer $flag
   *
   * @return array
   */
  public static function getStatDescriptors($nd_experiment_id, $flag = BIMS_OPTION) {
    $sql = "
      SELECT S.node_id, S.cvterm_id, S.name, S.num_data
      FROM {bims_mview_cross_stats} S
      WHERE S.nd_experiment_id = :nd_experiment_id
      ORDER BY S.name
    ";
    $results = db_query($sql, array(':nd_experiment_id' => $nd_experiment_id));
    $descriptors = array();
    while ($obj = $results->fetchObject()) {
      if ($obj->num_data) {
        if ($flag == BIMS_CLASS) {
          $descriptors []= BIMS_MVIEW_DESCRIPTOR::byID($obj->node_id, $obj->cvterm_id);
        }
        else if ($flag == BIMS_OBJECT) {
          $descriptors []= $obj;
        }
        else {
          $descriptors[$obj->cvterm_id] = $obj->name;
        }
      }
    }
    return $descriptors;
  }

  /**
   * Returns the statistical descriptors for the provided cvterm and nd_experiment ID.
   *
   * @param integer $cvterm_id
   * @param integer $node_id
   * @param integer $nd_experiment_id
   *
   * @return string
   */
  public static function getStatsTable($cvterm_id, $node_id, $nd_experiment_id) {

    // Gets BIMS_MVIEW_CROSS_STATS and the stats array.
    $args = array(
      'cvterm_id'         => $cvterm_id,
      'node_id'           => $node_id,
      'nd_experiment_id'  => $nd_experiment_id,
    );
    $bm_cross_stats = BIMS_MVIEW_CROSS_STATS::byKey($args);
    if (!$bm_cross_stats) {
      return "<em>No stats for the trait ($cvterm_id) was found.</em>";
    }
    $stats_arr = $bm_cross_stats->getStatsArr();

    // Returns the stats table.
    return BIMS_STATS::getStatsTable($node_id, $stats_arr, $cvterm_id);
  }

  /**
   * Returns the statistical information of the given trait.
   *
   * @param integer $node_id
   * @param integer $nd_experiment_id
   * @param integer $cvterm_id
   *
   * @return array
   */
  public static function getStatsByID($node_id, $nd_experiment_id, $cvterm_id) {

    // Gets BIMS_MVIEW_CROSS_STATS and the stats array.
    $args = array(
      'node_id'           => $node_id,
      'nd_experiment_id'  => $nd_experiment_id,
      'cvterm_id'         => $cvterm_id,
    );
    $bm_cross_stats = BIMS_MVIEW_CROSS_STATS::byKey($args);
    if ($bm_cross_stats) {
      return $bm_cross_stats->getStatsArr();
    }
    return array();
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the stats array.
   *
   * @return array
   */
  public function getStatsArr() {
    return $this->stats_arr;
  }

  /**
   * Sets the stats array.
   *
   * @param array $stats
   */
  public function setStatsArr($stats) {
    $this->stats_arr = $stats;
  }

  /**
   * Returns the value of the given key in stats.
   */
  public function getStatsByKey($key) {
    if (array_key_exists($key, $this->stats_arr)) {
      return $this->stats_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in stats.
   *
   * @param string $key
   * @param string $value
   */
  public function setStatsByKey($key, $value) {
    $this->stats_arr[$key] = $value;
  }

  /**
   * Returns the IDs by the group.
   *
   * @param BIMS_MVIEW_PHENOTYPE $bm_phenotype
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Local variables.
    $bm_cross     = new BIMS_MVIEW_CROSS(array('node_id' => $this->node_id));
    $bm_cp        = new BIMS_MVIEW_CROSS_PROGENY(array('node_id' => $this->node_id));
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
    $m_cross      = $bm_cross->getMView();
    $m_cp         = $bm_cp->getMView();
    $m_phenotype  = $bm_phenotype->getMView();

    // Initializes the returning array.
    $nd_experiment_ids = array();

    // GROUP : private.
    if ($group == 'private') {
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_cross}
        WHERE nd_experiment_id IS NOT NULL AND chado = :chado";
      $results = db_query($sql, array(':chado' => 0));
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }

      // GROUP : public.
    else if ($group == 'public') {
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_cross}
        WHERE nd_experiment_id IS NOT NULL AND chado = :chado
      ";
      $results = db_query($sql, array(':chado' => 1));
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }

    // GROUP : node.
    else if ($group == 'node') {
      $id_list =  implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_phenotype}
        WHERE nd_experiment_id IS NOT NULL AND node_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }

    // GROUP : project.
    else if ($group == 'project') {
      $id_list =  implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_phenotype}
        WHERE nd_experiment_id IS NOT NULL AND project_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }

    // GROUP : stock.
    else if ($group == 'stock') {
      $id_list =  implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_cp} CP
        WHERE stock_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }

    // GROUP : sample.
    else if ($group == 'sample') {
      $id_list =  implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_phenotype}
        WHERE nd_experiment_id IS NOT NULL AND sample_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }

    // GROUP : cross.
    else if ($group == 'cross') {
      $id_list =  implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_cross}
        WHERE nd_experiment_id IS NOT NULL AND nd_experiment_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }

    // GROUP : cvterm.
    else if ($group == 'cvterm') {
      foreach ($group_ids as $cvterm_id) {
        $field = "t$cvterm_id";
        if (!db_field_exists($m_phenotype, $field)) {
          $sql = "
            SELECT DISTINCT nd_experiment_id FROM {$m_phenotype}
            WHERE nd_experiment_id IS NOT NULL AND $field != ''
          ";
          $results = db_query($sql);
          while ($nd_experiment_id = $results->fetchField()) {
            $nd_experiment_ids[$nd_experiment_id] = TRUE;
          }
        }
      }
    }

    // GROUP : program (private + public).
    else {
      $sql = "
        SELECT DISTINCT nd_experiment_id FROM {$m_cross}
        WHERE nd_experiment_id IS NOT NULL
      ";
      $results = db_query($sql);
      while ($nd_experiment_id = $results->fetchField()) {
        $nd_experiment_ids[$nd_experiment_id] = TRUE;
      }
    }
    return array_keys($nd_experiment_ids);
  }
}