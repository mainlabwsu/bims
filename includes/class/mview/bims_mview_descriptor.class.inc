<?php
/**
 * The declaration of BIMS_MVIEW_DESCRIPTOR class.
 *
 */
class BIMS_MVIEW_DESCRIPTOR extends PUBLIC_BIMS_MVIEW_DESCRIPTOR {

  /**
   * Class data members.
   */
  public $program_id          = NULL;
  protected $prop_arr         = NULL;
  private $format_stats       = NULL;
  private $format_numeric     = NULL;
  private $format_data_point  = NULL;

 /**
   * @see PUBLIC_BIMS_MVIEW_DESCRIPTOR::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);

    // Updates the program ID.
    if (!$this->program_id) {
      $this->program_id = $this->_getProgramID();
    }

    // Updates the cv ID.
    if (!$this->cv_id) {
      $bims_program = BIMS_PROGRAM::byID($this->program_id);
      $this->cv_id = $bims_program->getCvID('descriptor');
    }

    // The formats for stats.
    $this->format_stats       = BIMS_FIELD_BOOK::getStatsFormats();
    $this->format_numeric     = BIMS_FIELD_BOOK::getNumericFormats();
    $this->format_data_point  = BIMS_FIELD_BOOK::getDataPointFormats();

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * Returns the program ID from chado.cv.name
   *
   * @param string $cv_name
   *
   * @return integer|NULL
   */
  private function _getProgramID() {
    $cv = MCL_CHADO_CV::byID($this->cv_id);
    if ($cv) {
      if (preg_match("/^(\d+)\./", $cv->getName(), $matches)) {
        return $matches[1];
      }
    }
    return NULL;
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_DESCRIPTOR::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_MVIEW_DESCRIPTOR by program ID and cvterm ID.
   *
   * @param integer $program_id
   * @param integer $cvterm_id
   *
   * @return BIMS_MVIEW_DESCRIPTOR|NULL
   */
  public static function byID($program_id, $cvterm_id) {
    if (!bims_is_int($program_id)) {
      return NULL;
    }
    if (!bims_is_int($cvterm_id)) {
      return NULL;
    }
    $keys = array(
      'program_id'  => $program_id,
      'cvterm_id'   => $cvterm_id,
    );
    return BIMS_MVIEW_DESCRIPTOR::byKey($keys);
  }

  /**
   * Returns BIMS_MVIEW_DESCRIPTOR by a program ID and a name.
   *
   * @param integer $program_id
   * @param string $name
   *
   * @return BIMS_MVIEW_DESCRIPTOR|NULL
   */
  public static function byName($program_id, $name) {
    $keys = array(
      'program_id'  => $program_id,
      'name'        => $name,
    );
    return BIMS_MVIEW_DESCRIPTOR::byKey($keys);
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_DESCRIPTOR::__destruct()
   */
  public function __destruct() {}

  /**
   * @see PUBLIC_BIMS_MVIEW_DESCRIPTOR::insert()
   */
  public function insert() {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new file.
    if (parent::insert()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_DESCRIPTOR::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Updates the properties.
    return parent::update($new_values);
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_DESCRIPTOR::delete()
   */
  public function delete() {

    // Deletes the descriptor.
    return parent::update();
  }

  /**
   * Checks if it can be calculated the stats for this descriptor.
   *
   * @return boolean
   */
  public function canStats() {
    return in_array(strtolower($this->format), $this->format_stats);
  }

  /**
   * Checks if this descriptor can have only data point as stats.
   *
   * @return boolean
   */
  public function isDataPointOnly() {
    return in_array(strtolower($this->format), $this->format_data_point);
  }

  /**
   * Checks if it is numeric.
   *
   * @return boolean
   */
  public function isNumeric() {
    return in_array(strtolower($this->format), $this->format_numeric);
  }

  /**
   * Checks if it is date.
   *
   * @return boolean
   */
  public function isDate() {
    return ($this->format == 'date');
  }

  /**
   * Checks if it is analyzable.
   *
   * @return boolean
   */
  public function isAnalyzable() {
    if ($this->format == 'date') {
      return TRUE;
    }
    return in_array(strtolower($this->format), $this->format_numeric);
  }

  /**
   * Updates the rank of the descriptor.
   *
   * @param integer $rank
   *
   * @return boolean
   */
  public function updateRank($rank) {

    // Updates the rank if it exists in table.
    $keys = array(
      'program_id'  => $this->program_id,
      'cvterm_id'   => $this->cvterm_id
    );
    $descriptor_rank = PUBLIC_BIMS_DESCRIPTOR_RANK::byKey($keys);
    if ($descriptor_rank) {
      $descriptor_rank->setRank($rank);
      return $descriptor_rank->update();
    }

    // Adds a rank.
    else {
      $details = array(
        'program_id'  => $this->program_id,
        'cvterm_id'   => $this->cvterm_id,
        'rank'        => $rank,
      );
      $descriptor_rank = new PUBLIC_BIMS_DESCRIPTOR_RANK($details);
      return $descriptor_rank->insert();
    }
  }

  /**
   * Updates the properties of the descriptor.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function updateProp() {

    // Gets MCL_CHADO_CVTREM.
    $cvterm = MCL_CHADO_CVTERM::byID($this->cvterm_id);
    if (!$cvterm) {
      return FALSE;
    }

    // Gets the properties of the format.
    $cv = MCL_CHADO_CV::byName('BIMS_FIELD_BOOK_FORMAT_PROP');
    $format_prop_cv_id = $cv->getCvID();
    $format_props = MCL_CHADO_CVTERM::getCvtermsByCvID($format_prop_cv_id);

    // Go through all format props.
    $prop = array();
    foreach ($format_props as $format_prop) {
      $prop_name = $format_prop->getName();
      $value = $cvterm->getCvtermprop('BIMS_FIELD_BOOK_FORMAT_PROP', $prop_name);
      if ($value || $value == '0') {
        $prop[$prop_name] = $value;
      }
    }

    // Adds the code to $prop.
    $codes = $cvterm->getCodes(TRUE);
    if (!empty($codes)) {
      $prop['codes'] = $codes;
    }

    // Sets the properties.
    $this->setPropArr($prop);
    return $this->update();
  }

  /**
   * Clear the previous data.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return boolean
   */
  public function clear($group, $group_ids = array()) {

    // Deletes all descriptors.
    $sql = "
      DELETE FROM {bims_mview_descriptor}
      WHERE program_id = :program_id
    ";
    $args = array(':program_id' => $this->program_id);

    // GROUP : private.
    if ($group == 'private') {
      $bims_program = BIMS_PROGRAM::byID($this->program_id);
      $sql .= ' AND cv_id = :cv_id ';
      $args[':cv_id'] = $bims_program->getCvID('descriptor');
    }

    // GROUP : public.
    else if ($group == 'public') {
      $bims_program = BIMS_PROGRAM::byID($this->program_id);
      $sql .= ' AND cv_id != :cv_id ';
      $args[':cv_id'] = $bims_program->getCvID('descriptor');
    }
    else if ($group == 'cv') {
      $id_str = implode(',', $group_ids);
      $sql .= " AND cv_id IN ($id_str) ";
    }
    else if ($group == 'cvterm') {
      $id_str = implode(',', $group_ids);
      $sql .= " AND cvterm_id IN ($id_str) ";
    }
    db_query($sql, $args);
    return TRUE;
  }

  /**
   * Updates the materialized views for the descriptors.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return boolean
   */
  public function updateMView($group, $group_ids = array()) {

    $transaction = db_transaction();
    try {

      // Clears the mview.
      $this->clear($group, $group_ids);

      // Gets the program cv_id.
      $bims_program   = BIMS_PROGRAM::byID($this->program_id);
      $program_cv_id  = $bims_program->getCvID('descriptor');

      // Adds all cvterms to be updated into $all_cvterms array.
      $all_cvterms = array();

      // GROUP : cvterm.
      if ($group == 'cvterm') {
        foreach ((array)$group_ids as $cvterm_id) {
          $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
          if ($cvterm) {
            $all_cvterms[$cvterm_id] = $cvterm;
          }
        }
      }

      // GROUP : cv.
      else if ($group == 'cv') {
        foreach ((array)$group_ids as $cv_id) {
          $tmp_cvterms = MCL_CHADO_CVTERM::getCvtermsByCvID($cv_id);
          foreach ((array)$tmp_cvterms as $cvterm) {
            $all_cvterms[$cvterm->getCvtermID()] = $cvterm;
          }
        }
      }

      // GROUP : public.
      else if ($group == 'public') {
        $tmp_cvterms = $bims_program->getPublicDescriptor();
        foreach((array)$tmp_cvterms as $cvterm) {
          $all_cvterms[$cvterm->getCvtermID()] = $cvterm;
        }
      }

      // GROUP : private.
      else if ($group == 'private') {
        $tmp_cvterms = MCL_CHADO_CVTERM::getCvtermsByCvID($program_cv_id);
        foreach((array)$tmp_cvterms as $cvterm) {
          $all_cvterms[$cvterm->getCvtermID()] = $cvterm;
        }
      }

      // Adds all the descriptors (private + public) of this progarm.
      else {

        // Adds the private descriptors.
        $tmp_cvterms = MCL_CHADO_CVTERM::getCvtermsByCvID($program_cv_id);
        foreach((array)$tmp_cvterms as $cvterm) {
          $all_cvterms[$cvterm->getCvtermID()] = $cvterm;
        }

        // Adds the public descriptors.
        $tmp_cvterms = $bims_program->getPublicDescriptor();
        foreach((array)$tmp_cvterms as $cvterm) {
          $all_cvterms[$cvterm->getCvtermID()] = $cvterm;
        }
      }

      // Adds the descriptor to the mview.
      foreach ((array)$all_cvterms as $cvterm_id => $cvterm) {
        if (!$cvterm) {
          continue;
        }

        // Gets the format.
        $format = $cvterm->getCvtermprop('SITE_CV', 'format');
        if (!$format) {
          continue;
        }

        // Updates chado.
        $cv_id = $cvterm->getCvID();
        $chado = ($program_cv_id == $cv_id) ? 0 : 1;

        // Adds a descriptor.
        $details = array(
          'program_id'  => $this->program_id,
          'cv_id'       => $cv_id,
          'cvterm_id'   => $cvterm_id,
          'name'        => $cvterm->getName(),
          'format'      => $format,
          'definition'  => $cvterm->getDefinition(),
          'chado'       => $chado,
        );
        $bm_descriptor = new BIMS_MVIEW_DESCRIPTOR($details);
        if ($bm_descriptor->insert()) {

          // Gets BIMS_MVIEW_DESCRIPTOR.
          $bm_descriptor = BIMS_MVIEW_DESCRIPTOR::byID($this->program_id, $cvterm_id);

          // Updates the properties.
          if (!$bm_descriptor->updateProp()) {
            throw new Exception("Error : Failed to update the properties of the descriptor");
          }
        }
        else {
          throw new Exception("Error : Failed to insert the descriptor");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Checks if the provided descriptor has an image.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function hasImage($cvterm_id) {
    return FALSE;
  }

  /**
   * Returns the categories of the categorical trait.
   *
   * @return array
   */
  public function getCategories() {
    $categories = array();

    // Tries to get the codes from the prop if exists.
    $codes = $this->getPropByKey('codes');
    if (!empty($codes)) {

      foreach ($codes as $code) {
        $categories[$code['key']] = $code['val'];
      }
      return $categories;
    }

    // Gets the $categories.
    $categories_str = $this->getPropByKey('categories');
    $tmp = preg_split("/\//", $categories_str, -1, PREG_SPLIT_NO_EMPTY);
    foreach ($tmp as $label) {
      $categories[$label] = $label;
    }
    return $categories;
  }

  /**
   * Sorts the descriptors based on preference.
   *
   * @param integer $program_id
   * @param array $descriptors
   * @param string $type
   * @return array
   */
  public static function sortDescriptors($program_id, $descriptors, $type = '') {
    $cvterm_ids     = array_keys($descriptors);
    $cvterm_id_str  = implode(',', $cvterm_ids);
    $sql = "
      SELECT MV.*, DR.rank
      FROM {bims_mview_descriptor} MV
        LEFT JOIN bims_descriptor_rank DR on DR.cvterm_id = MV.cvterm_id
      WHERE MV.program_id = :program_id AND MV.cvterm_id IN ($cvterm_id_str)
      ORDER BY DR.rank, MV.name
    ";
    $results = db_query($sql, array(':program_id' => $program_id));
    $descriptors = array();
    while ($obj = $results->fetchObject()) {
      if ($type == 'name') {
        $descriptors []= $obj->name;
      }
      else if ($type == 'cverm_id') {
        $descriptors []= $obj->cvterm_id;
      }
      else {
        $descriptors[$obj->cvterm_id]= $obj->name;
      }
    }
    return $descriptors;
  }

  /**
   * Returns the descriptors for the list of cv ID.
   *
   * @param integer $program_id
   * @param array $cv_id_list
   * @param array $formats
   * @param integer $flag
   *
   * @return array
   */
  public static function getDescriptorsByCVs($program_id, $cv_id_list, $formats = NULL, $flag = BIMS_OBJECT) {

    // Adds the format conditions.
    $format_cond = '';
    if (!empty($formats)) {
      if ($formats == 'STATABLE') {
        $arr = array('numeric','counter','percent');
        $format_str = implode("','", $arr);
        $format_cond = " AND MV.format IN ('$format_str')";
      }
      else {
        $format_str = implode("','", $formats);
        $format_cond = " AND MV.format IN ('$format_str')";
      }
    }

    // Adds the list of cv IDs.
    $cv_id_cond = '';
    if (!empty($cv_id_list)) {
      $cv_id_cond = ' AND MV.cv_id IN (' . implode(',', $cv_id_list) . ') ';
    }

    // Generates the SQL.
    $sql = "
      SELECT MV.*, DR.rank
      FROM {bims_mview_descriptor} MV
        LEFT JOIN bims_descriptor_rank DR on DR.cvterm_id = MV.cvterm_id
      WHERE MV.program_id = :program_id $cv_id_cond $format_cond
      ORDER BY DR.rank, MV.name
    ";
    $results = db_query($sql, array(':program_id' => $program_id));
    $descriptors = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $descriptors[$obj->cvterm_id]= $obj->name;
      }
      else if ($flag == BIMS_FIELD) {
        $descriptors['t' . $obj->cvterm_id]= $obj->name;
      }
      else if ($flag == BIMS_CLASS) {
        $descriptors[]= BIMS_MVIEW_DESCRIPTOR::byID($program_id, $obj->cvterm_id);
      }
      else {
        $descriptors[]= $obj;
      }
    }
    return $descriptors;
  }

  /**
   * Returns the descriptors for the provide cv ID.
   *
   * @param integer $program_id
   * @param integer $cv_id
   * @param array $formats
   * @param integer $flag
   * @param boolean $all
   *
   * @return array
   */
  public static function getDescriptors($program_id, $cv_id, $formats = NULL, $flag = BIMS_OBJECT, $all = FALSE) {

    // Return empty array if cv ID is empty.
    $descriptors = array();
    if (!$cv_id && !$all) {
      return $descriptors;
    }

    // Adds the format conditions.
    $format_cond = '';
    if (!empty($formats)) {
      if ($formats == 'STATABLE') {
        $arr = array('numeric','counter','percent');
        $format_str = implode("','", $arr);
        $format_cond = " AND MV.format IN ('$format_str')";
      }
      else {
        $format_str = implode("','", $formats);
        $format_cond = " AND MV.format IN ('$format_str')";
      }
    }

    // Generates the SQL.
    $sql = "
      SELECT MV.*, DR.rank
      FROM {bims_mview_descriptor} MV
        LEFT JOIN bims_descriptor_rank DR on DR.cvterm_id = MV.cvterm_id
      WHERE MV.program_id = :program_id $format_cond
    ";
    $args = array(':program_id' => $program_id);
    if (!$all) {
      $args[':cv_id'] = $cv_id;
      $sql .= ' AND MV.cv_id = :cv_id ';
    }
    $sql .= ' ORDER BY DR.rank, MV.name';
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $descriptors[$obj->cvterm_id]= $obj->name;
      }
      else if ($flag == BIMS_FIELD) {
        $descriptors['t' . $obj->cvterm_id]= $obj->name;
      }
      else if ($flag == BIMS_CLASS) {
        $descriptors[]= BIMS_MVIEW_DESCRIPTOR::byID($obj->program_id, $obj->cvterm_id);
      }
      else {
        $descriptors[]= $obj;
      }
    }
    return $descriptors;
  }

  /**
   * Returns the trait list for trait search. Lists only the descriptors that
   * has data (active_descriptor).
   *
   * @param BIMS_PROGRAM $bims_program
   * @param arary $filters
   *
   * @return array
   */
  public static function getTraitList($bims_program, $filters = NULL) {

    // Gets the active descriptors.
    $params = array(
      'flag'    => BIMS_OPTION,
      'program' => 'program',
    );
    $active_descriptors = $bims_program->getActiveDescriptors($params);

    // Applies the filters.
    if ($filters) {
      $filter_str = " AND D.format IN ('" . implode("','", $filters) . "') ";
    }

    // Generates SQL.
    $sql = "
      SELECT D.cvterm_id, D.name, PS.stats
      FROM {bims_mview_descriptor} D
        INNER JOIN {bims_mview_phenotype_stats} PS on PS.cvterm_id = D.cvterm_id
        LEFT JOIN bims_descriptor_rank DR on DR.cvterm_id = D.cvterm_id
      WHERE PS.node_id = :node_id $filter_str
        AND PS.num_data != :num_data
      ORDER BY DR.rank, D.name
    ";
    $args = array(
      ':node_id'  => $bims_program->getNodeID(),
      ':num_data' => 0,
    );
    $results = db_query($sql, $args);
    $trait_list = array();
    while ($obj = $results->fetchObject()) {
      if (array_key_exists($obj->cvterm_id, $active_descriptors)) {
        $trait_list[$obj->cvterm_id] = $obj->name;
      }
    }
    return $trait_list;
  }

  /**
   * Returns the trait information in table.
   *
   * @param integer $cvterm_id
   *
   * @return string
   */
  public static function getTraitTable($cvterm_id) {

    // Adds the trial information.
    if (!$cvterm_id) {
      return "<em>No statistical trait found.</em>";
    }

    // Adds the trial information.
    $descriptor = BIMS_MVIEW_DESCRIPTOR::byKey(array('cvterm_id' => $cvterm_id));
    if (!$descriptor) {
      return "<em>The trait ($cvterm_id) not found.</em>";
    }

    // Trait info.
    $prop_arr = $descriptor->getPropArr();
    $def = "<textarea style='width:100%;' rows=2 READONLY>" . $descriptor->getDefinition() . "</textarea>";
    $rows = array();
    $rows []= array(array('data' => '<b>Trait Information</b>', 'colspan' => 2));
    $rows []= array(array('data' => 'Name', 'style' => 'width:120px;'), $descriptor->getName());
    $rows []= array('Format', $descriptor->getFormat());
    $alias = $descriptor->getAlias();
    if ($alias) {
      $rows []= array('Alias', $alias);
    }
    if (array_key_exists('data_unit', $prop_arr)) {
      $rows []= array('Unit', $prop_arr['data_unit']);
    }
    $rows []= array('Definition', $def);
    $table_vars = array(
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%;'),
    );
    return theme('table', $table_vars);
  }

  /**
   * Returns the optons for the categorical format.
   *
   * @param array $default_arr
   *
   * @retrun array
   */
  public function getOptCategorical($default_arr = array()) {
    $options = array();
    if (!empty($default_arr)) {
      $options = $default_arr;
    }
    $categories = $this->getPropByKey('categories');
    $tmp = explode('/', $categories);
    foreach ($tmp as $category) {
      $options[$category] = $category;
    }
    return $options;
  }

  /**
   * Returns the base descriptor if exists.
   *
   * @return BIMS_MVIEW_DESCRIPTOR|NULL
   */
  public function getBaseDescriptor() {
    $sql = "
      SELECT cvterm_id_base FROM {bims_descriptor_match}
      WHERE program_id = :program_id AND cvterm_id = :cvterm_id
    ";
    $args = array(
      ':program_id' => $this->program_id,
      ':cvterm_id'  => $this->cvterm_id,
    );
    $cvterm_id = db_query($sql, $args)->fetchField();
    if ($cvterm_id) {
      return BIMS_MVIEW_DESCRIPTOR::byID($this->program_id, $cvterm_id);
    }
    return NULL;
  }

  /**
   * Returns the matched descriptor if exists.
   *
   * @return BIMS_MVIEW_DESCRIPTOR|NULL
   */
  public function getMatchedDescriptor() {
    $sql = "
      SELECT cvterm_id FROM {bims_descriptor_match}
      WHERE program_id = :program_id AND cvterm_id_base = :cvterm_id_base
    ";
    $args = array(
      ':program_id'     => $this->program_id,
      ':cvterm_id_base' => $this->cvterm_id,
    );
    $cvterm_id = db_query($sql, $args)->fetchField();
    if ($cvterm_id) {
      return BIMS_MVIEW_DESCRIPTOR::byID($this->program_id, $cvterm_id);
    }
    return NULL;
  }

  /**
   * Returns the name of the descriptor group.
   *
   * @return string
   */
  public function getDescriptorGroup() {
    if ($this->chado) {
      $cv = MCL_CHADO_CV::byID($this->cv_id);
      if ($cv) {
        return $cv->getName();
      }
    }
    return 'program';
  }

  /**
   * Return TRUE if this descritptor is in Chado.
   *
   * @return integer
   */
  public function isChado() {
    return $this->chado ? TRUE : FALSE;
  }

  /**
   * Returns MCL_CHADO_CV.
   *
   * @return MCL_CHADO_CV|NULL
   */
  public function getCv() {
    return MCL_CHADO_CV::byID($this->cv_id);
  }

  /**
   * Returns cv name.
   *
   * @return string
   */
  public function getCvName() {
    $cv = MCL_CHADO_CV::byID($this->cv_id);
    if ($cv) {
      return $cv->getName();
    }
    return '';
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the program ID.
   *
   * @return integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program ID.
   *
   * @param array $prop_arr
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }

  /**
   * Retrieves the prop array.
   *
   * @return array
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}