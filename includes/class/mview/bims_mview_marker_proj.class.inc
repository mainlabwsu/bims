<?php
/**
 * The declaration of BIMS_MVIEW_MARKER_PROJ class.
*
*/
class BIMS_MVIEW_MARKER_PROJ extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView() {
    return 'bims.bims_' . $this->getNodeID() . '_mview_marker_proj';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_program = BIMS_PROGRAM::byID($node_id);
    if ($bims_program) {
      $bims_mview = new BIMS_MVIEW_MARKER_PROJ(array('node_id' => $node_id));
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for marker project.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'feature_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'uniquename' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'feature_id')
      ),
    );
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($drop = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {
      $mview    = $this->getMView();
      $node_id  = $this->getNodeID();

      // Clears the mview.
      if ($drop) {
        $this->dropMView();
        $this->createMView();
      }
      else {
        $this->clear();
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($node_id);

      // Gets BIMS_CHADO tables.
      $bc_feature     = new BIMS_CHADO_FEATURE($this->node_id);
      $table_feature  = $bc_feature->getTableName('feature');

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumns($bims_program, $bc_feature);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the all the markers.
      $sql = "
        SELECT DISTINCT MARKER.feature_id, MARKER.uniquename, MARKER.name
          $select_str
        FROM {$table_feature} MARKER
          $join_str
        WHERE 1=1
      ";

      // Gets all the project IDs.
      $project_ids = $bc_feature->getProjectID();
      foreach ((array)$project_ids as $project_id) {

        // Gets the results for this project.
        $result = db_query($sql, array(':project_id' => $project_id));

        // Populates the mview.
        $dup_check = array();
        while ($arr = $result->fetchAssoc()) {
          if (array_key_exists($arr['feature_id'], $dup_check)) {
            continue;
          }
          else {
            $dup_check[$arr['feature_id']] = TRUE;
          }

          // Adds node_id and project_id.
          $arr['node_id']     = $node_id;
          $arr['project_id']  = $project_id;

          // Checks the data.
          $data = '';
          foreach ($arr as $key => $value) {
            if (preg_match("/^p\d+$/", $key)) {
              $data .= trim($value);
            }
          }

          // Inserts into mview.
          if ($data) {
            db_insert($mview)
              ->fields($arr)
              ->execute();
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param BIMS_CHADO_FEATURE $bc_feature
   *
   * @return boolean
   */
  private function _updateColumns(BIMS_PROGRAM $bims_program, BIMS_CHADO_FEATURE $bc_feature) {
    $mview      = $this->getMView();
    $program_id = $bims_program->getProgramID();

    // Gets the table name.
    $table_featureprop = $bc_feature->getTableName('featureprop');

    // Adds the property columns.
    $props = $bims_program->getProperties('marker_proj', 'custom', BIMS_OPTION);
    foreach ((array)$props as $cvterm_id => $name) {
      $field = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($mview, $field)) {
        db_add_field($mview, $field, array('type' => 'text'));
      }

      // Updates the select / join.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";
      $join_str .= "
        LEFT JOIN (
          SELECT PROP.feature_id, PROP.value, PROP.rank AS project_id
          FROM {$table_featureprop} PROP
          WHERE PROP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.feature_id = MARKER.feature_id AND JP$cvterm_id.project_id = :project_id
      ";
    }
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Updates the record.
   *
   * @param array $fields
   */
  public function update($fields) {
    $transaction = db_transaction();
    try {
      $project_id = $fields['project_id'];
      $feature_id = $fields['feature_id'];

      // Updates the record.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('project_id', $project_id, '=')
        ->condition('feature_id', $feature_id, '=')
      ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the marker.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateMarker($details) {

    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'project_id'  => $details['project_id'],
        'feature_id'  => $details['feature_id'],
        'uniquename'  => $details['uniquename'],
        'name'        => $details['name'],
      );

      // Updates the marker.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('project_id', $details['project_id'], '=')
        ->condition('feature_id', $details['feature_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Checks if data exist.
   *
   * @param integer $node_id
   *
   * @return boolean
   */
  public static function existData($node_id) {

    // Gets the mview table.
    $bims_mview = new BIMS_MVIEW_MARKER(array('node_id' => $node_id));
    $mview = $bims_mview->getMView(TRUE);

    // Checks if the mview table exists first.
    if (!db_table_exists($mview)) {
      return FALSE;
    }

    // Checks if the mivew has data.
    $sql = "SELECT COUNT(*) FROM {$mview}";
    $num = db_query($sql)->fetchField();
    return ($num) ? TRUE : FALSE;
  }

  /**
   * Deletes the marker.
   *
   * @param integer $project_id
   * @param integer $feature_id
   *
   * @return boolean
   */
  public function deleteMarker($project_id, $feature_id) {

    // Deletes the marker.
    $transaction = db_transaction();
    try {

      // Deletes the marker.
      db_delete($this->getMView())
        ->condition('project_id', $project_id)
        ->condition('feature_id', $feature_id)
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new marker.
   *
   * @param array
   *
   * @return boolean
   */
  public function addMarker($details) {

    // Add a marker.
    $mview = $this->getMView();
    $node_id = $this->getNodeID();
    $cols = 'node_id';
    $vals = $node_id;
    foreach ($details as $key => $val) {
      $cols .= ", $key";
      $vals .= ", '$val'";
    }
    $sql = "INSERT INTO $mview ($cols) VALUES ($vals)";
    db_query($sql);
    return TRUE;
  }

  /**
   * Returns marker object.
   *
   * @param integer $project_id
   * @param integer $feature_id
   * @param integer $flag
   *
   * @return object|array
   */
  public function getMarker($project_id, $feature_id, $flag = BIMS_OBJECT) {
    if ($project_id && $feature_id) {
      $mview = $this->getMView();
      $sql = "
        SELECT MV.* FROM $mview MV
        WHERE MV.project_id = :project_id AND MV.feature_id = :feature_id
      ";
      $args = array(
        ':project_id' => $project_id,
        ':feature_id' => $feature_id,
      );
      $result = db_query($sql, $args);
      if ($flag == BIMS_OBJECT) {
        return $result->fetchObject();
      }
      return $result->fetchAssoc();
    }
    return NULL;
  }
}