<?php
/**
 * The declaration of BIMS_MVIEW_IMAGE_TAG class.
*
*/
class BIMS_MVIEW_IMAGE_TAG extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_image_tag';
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for image tags.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'eimage_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'tag' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('eimage_id', 'type', 'tag')
      ),
    );
  }

  /**
   * Deletes the images by the tag.
   *
   * @param string $tag
   *
   * @return boolean
   */
  public function deleteImageTag($tag) {

    $transaction = db_transaction();
    try {

      // Gets BIMS_MVIEW_IMAGE.
      $bm_image = new BIMS_MVIEW_IMAGE(array('node_id' => $this->node_id));

      // Gets all images by the tag.
      $table = $this->getMView();
      $sql = "SELECT DISTINCT eimage FROM {$table} WHERE LOWER(tag) = LOWER(:tag)";
      $results = db_query($sql, array(':tag' => $tag));
      while ($eimage_id = $results->fetchField()) {
        if (!$bm_image->deleteImage($eimage_id)) {
          throw new Exception("Error : Fail to delete the image [$eimage_id]");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($grouped_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'feature_id', $grouped_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Updates the mviews by schema.
      if (!$this->_updateMViewByChado($bims_program, $grouped_ids['chado'])) {
        throw new Exception("Error : Failed to update the mview data in Chado");
      }
      if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['bims'])) {
        throw new Exception("Error : Failed to update the mview data in BIMS");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByChado(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Gets the cvterms.
      $cvterms = array(
        'image_tag' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'image_tag')->getCvtermID(),
      );

      // Gets the all the image tags for this program.
      $id_str = implode(',', $grouped_ids);
      $sql = "
        SELECT E.eimage_id, E.eimage_type AS type, EP.value AS tag
        FROM {chado.eimageprop} EP
          INNER JOIN {chado.eimage} E on E.eimage_id = EP.eimage_id
        WHERE EP.type_id = :type_id AND E.eimage_id IN ($id_str)
      ";
      $args = array(
        ':type_id' => $cvterms['image_tag'],
      );
      $results = db_query($sql, $args);
      $dup_feature = array();
      while ($arr = $results->fetchAssoc()) {

        // Adds node_id.
        $arr['node_id'] = $this->node_id;
        $arr['chado']   = 1;

        // Inserts into the mview.
        db_insert($this->getMView())
          ->fields($arr)
          ->execute();
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  public function _updateMViewByBIMS(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Gets BIMS_CHADO tables.
      $bc_image         = new BIMS_CHADO_IMAGE($this->node_id);
      $table_eimage     = $bc_image->getTableName('eimage');
      $table_eimageprop = $bc_image->getTableName('eimageprop');
      $m_image_tag      = $this->getMView();

      // Gets the cvterms.
      $cvterms = array(
        'image_tag' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'image_tag')->getCvtermID(),
      );

      // Gets the all the image tags for this program.
      $id_str = implode(',', $grouped_ids);
      $sql = "
        SELECT E.eimage_id, E.eimage_type AS type, EP.value AS tag
        FROM {$table_eimageprop} EP
          INNER JOIN {$table_eimage} E on E.eimage_id = EP.eimage_id
        WHERE EP.type_id = :type_id AND E.eimage_id IN ($id_str)
      ";
      $args = array(
        ':type_id' => $cvterms['image_tag'],
      );
      $results = db_query($sql, $args);
      while ($arr = $results->fetchAssoc()) {
        $cols = 'node_id';
        $vals = $node_id;
        foreach ($arr as $key => $val) {
          $cols .= ", $key";
          $vals .= ", '$val'";
        }
        $sql = "INSERT INTO $m_image_tag ($cols) VALUES ($vals)";
        db_query($sql);
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   *
   * @param $fields
   */
  public function update($fields) {
    $transaction = db_transaction();
    try {
      $type = $fields['type'];
      $tag  = $fields['tag'];

      // Updates the record.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('type', $type, '=')
        ->condition('tag', $tag, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new image tag.
   *
   * @param array
   *
   * @return boolean
   */
  public function addImageTag($details) {

    // Add a new image tag.
    $m_image_tag  = $this->getMView();
    $node_id      = $this->getNodeID();
    $cols         = 'node_id';
    $vals         = $node_id;
    foreach ($details as $key => $val) {
      $cols .= ", $key";
      $vals .= ", '$val'";
    }
    $sql = "INSERT INTO $m_image_tag ($cols) VALUES ($vals)";
    db_query($sql);
    return TRUE;
  }

  /**
   * Returns the images of the provided type.
   *
   * @param string $type
   * @param integer $flag
   *
   * @return array
   */
  public function getImageTags($type = 'any', $flag = BIMS_OBJECT) {
    $images = array();

    // Gets all images.
    $m_image_tag = $this->getMView();
    if (db_table_exists($m_image_tag)) {
      $sql = "SELECT MV.* FROM $m_image_tag MV";
      $args = array();
      if ($type != 'any') {
        $sql .= " WHERE LOWER(type) = LOWER(:type) ";
        $args[':type'] = $type;
      }
      $sql .= ' ORDER BY MV.type';
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        if ($flag == BIMS_OPTION) {
          $images[strtolower($obj->tag)] = $obj->tag;
        }
        else {
          $images []= $obj;
        }
      }
    }
    return $images;
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Local variables.
    $bc_eimage    = new BIMS_CHADO_IMAGE($this->node_id);
    $table_ep     = $bc_image->getTableName('eimageprop');
    $id_image_tag = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'image_tag')->getCvtermID();

    // Initializes the returning array.
    $grouped_ids = array('chado' => array(), 'bims' => array());

    // GROUP : private.
    if ($group == 'private') {
      if (db_table_exists($table_ep)) {
        $sql = "
          SELECT DISTINCT eimage_id FROM {$table_ep}
          WHERE type_id = :type_id
        ";
        $results = db_query($sql, array(':type_id' =>$id_image_tag));
        while ($eimage_id = $results->fetchField()) {
          $grouped_ids['bims'][$eimage_id] = TRUE;
        }
      }
    }

    // GROUP : public.
    else if ($group == 'public') {
        //....
    }

    // GROUP : eimage.
    else if ($group == 'eimage') {

      // BIMS.
      if (db_table_exists($table_ep)) {
        $id_list = implode(",", $group_ids);
        $sql = "
          SELECT eimage_id FROM {$table_ep}
          WHERE eimage_id IN ($id_list)
        ";
        $results = db_query($sql);
        while ($eimage_id = $results->fetchField()) {
          $grouped_ids['bims'][$eimage_id] = TRUE;
        }
      }

      // Chado.
      //....

    }

    // GROUP : program (private + public).
    else {

      // BIMS.
      if (db_table_exists($table_ep)) {
        $sql = "
          SELECT DISTINCT eimage_id FROM {$table_ep}
          WHERE type_id = :type_id
        ";
        $results = db_query($sql, array(':type_id' =>$id_image_tag));
        while ($eimage_id = $results->fetchField()) {
          $grouped_ids['bims'][$eimage_id] = TRUE;
        }
      }

      // Chado.
      //....
    }

    // Returns the grouped IDs.
    $ret_array = array('bims' => array(), 'chado' => array());
    if (empty($grouped_ids['chado']) && empty($grouped_ids['bims'])) {
      return array();
    }
    if (!empty($grouped_ids['bims'])) {
      $ret_array['bims'] = array_keys($grouped_ids['bims']);
    }
    if (!empty($grouped_ids['chado'])) {
      $ret_array['chado'] = array_keys($grouped_ids['chado']);
    }
    return $ret_array;
  }
}