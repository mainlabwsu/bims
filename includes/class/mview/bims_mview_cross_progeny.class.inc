<?php
/**
 * The declaration of BIMS_MVIEW_CROSS_PROGENY class.
*
*/
class BIMS_MVIEW_CROSS_PROGENY extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_cross_progeny';
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for progenies.',
      'fields' => array(
        'nd_experiment_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('nd_experiment_id', 'stock_id')
      ),
    );
  }

  /**
   * Updates the progenies.
   *
   * @param integer $nd_experiment_id
   * @param array $stock_ids
   *
   * @return boolean.
   */
  public function updateProgeny($nd_experiment_id, $stock_ids = array()) {

    // Deletes the all progenies.
    if (!$this->deleteProgeny($nd_experiment_id)) {
      return FALSE;
    }

    // Then adds the the given progenies.
    if (!$this->addProgeny($nd_experiment_id, $stock_ids)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds the associated progenies.
   *
   * @param integer $nd_experiment_id
   * @param array $stock_ids
   *
   * @return boolean
   */
  public function addProgeny($nd_experiment_id, $stock_ids = array()) {
    $transaction = db_transaction();
    try {

      // Add new progenies.
      foreach ((array)$stock_ids as $stock_id) {
        $fields = array(
          'nd_experiment_id'  => $nd_experiment_id,
          'stock_id'          => $stock_id,
        );
        db_insert($this->getMView())
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the associated progenies.
   *
   * @param integer $nd_experiment_id
   * @param array $stock_ids
   *
   * @return boolean
   */
  public function deleteProgeny($nd_experiment_id, $stock_ids = array()) {
    $transaction = db_transaction();
    try {

      // Deletes the all progenies of the cross.
      if (empty($stock_ids)) {
        db_delete($this->getMView())
          ->condition('nd_experiment_id', $nd_experiment_id, '=')
          ->execute();
      }
      else {
        foreach ((array)$stock_ids as $stock_id) {
          db_delete($this->getMView())
            ->condition('nd_experiment_id', $nd_experiment_id, '=')
            ->condition('stock_id', $stock_id, '=')
            ->execute();
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }
}