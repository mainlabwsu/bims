<?php
/**
 * The declaration of BIMS_MVIEW_HAPLOTYPE_SNP class.
 *
 */
class BIMS_MVIEW_HAPLOTYPE_SNP extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_haplotype_snp';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_mview = new BIMS_MVIEW_HAPLOTYPE_SNP(array('node_id' => $node_id));
    if ($bims_mview) {
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for haplotype SNP data.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'root_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'haplotype_block_fid' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'haplotype_block' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'genus' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'species' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'marker_fid' => array(
          'type' => 'int',
        ),
        'marker' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'haplotype_fid' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'haplotype' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'snp' => array(
          'type' => 'varchar',
          'length' => '10',
        ),
        'chado' => array(
          'type'      => 'int',
          'not null'  => TRUE,
          'default'   => 0,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('haplotype_block_fid', 'marker_fid', 'haplotype_fid')
      ),
      'indexes' => array(
        'mhs_haplotype_block' => array('haplotype_block'),
        'mhs_marker' => array('marker'),
        'mhs_haplotype' => array('haplotype'),
      ),
    );
  }


  /**
   * Returns a haploptye SNP object by the provied type
   * (haplotype_block or marker).
   *
   * @param string $type
   * @param integer $hb_fid
   *
   * @param integer $flag
   *
   * @return object|array
   */
  public function getHaplotypeSNP($type, $feature_id, $flag = BIMS_OBJECT) {

    // Sets the field.
    $field = '';
    if ($type == 'haplotype_block') {
      $field = 'MV.haplotype_block_fid';
    }
    else if ($type == 'marker') {
      $field = 'MV.marker_fid';
    }

    // Gets the haplotype SNP.
    if ($feature_id && $field) {
      $m_hs = $this->getMView();
      $sql = "
        SELECT MV.* FROM $m_hs MV
        WHERE $field = :feature_id
      ";
      $results = db_query($sql, array(':feature_id' => $feature_id));
      if ($flag == BIMS_ASSOC) {
        return $results->fetchAssoc();
      }
      return $results->fetchObject();
    }
    return NULL;
  }

  /**
   * Returns the distinct haplotype blocks.
   *
   * @return array
   */
  public function getHaplotypeBlocks() {
    $bm_hs  = new BIMS_MVIEW_HAPLOTYPE_SNP(array('node_id' => $this->node_id));
    $m_hs   = $bm_hs->getMView();
    $sql = "
      SELECT DISTINCT MV.haplotype_block_fid, MV.haplotype_block
      FROM {$m_hs} MV
    ";
    $results = db_query($sql);
    $haplotype_blocks = array();
    while ($obj = $results->fetchObject()) {
      $haplotype_blocks[$obj->haplotype_block_fid] = $obj->haplotype_block;
    }
    return $haplotype_blocks;
  }

  /**
    * Updates the marker.
    *
    * @param array $details
    *
    * @return boolean
    */
  public function updateMarker($details) {

    // Updates the marker properties.
    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'marker' => $details['name'],
      );

      // Updates the marker.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('marker_fid', $details['feature_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the distinct markers.
   *
   * @return array
   */
  public function getMarkers() {
    $m_hs     = $this->getMView();
    $sql      = "SELECT DISTINCT MV.marker_fid, MV.marker FROM {$m_hs} MV";
    $results  = db_query($sql);
    $markers  = array();
    while ($obj = $results->fetchObject()) {
      $markers[$obj->marker_fid] = $obj->marker;
    }
    return $markers;
  }

  /**
   * @see BIMS_MVIEW::deleteByKey()
   */
  public function deleteByKey($key, $value) {

    // Deletes the data.
    $transaction = db_transaction();
    try {

      // Deletes the data by key.
      db_delete($this->getMView())
      ->condition($key, $value)
      ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Clears the data of the provided trial ID in the mview.
   *
   * @param integer $trial_id
   *
   * @return boolean
   */
  public function clearTrial($trial_id) {

    // Clears the data in the mview.
    if ($this->exists()) {
      db_delete($this->getMView())
        ->condition('node_id', $trial_id, '=')
        ->execute();
    }

    // Creates the mview if not exist.
    else {
      $this->createMView();
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'nd_geolocation_id', $grouped_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Finds new trials and adds them to the program if exist.
      $bims_program->updateTrials();

      // Updates the mviews by schema.
      if (!$this->_updateMViewByChado($bims_program, $grouped_ids['chado'])) {
        throw new Exception("Error : Failed to update the mview data in Chado");
      }
      if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['bims'])) {
        throw new Exception("Error : Failed to update the mview data in BIMS");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByChado(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsChado($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'marker'          => MCL_CHADO_CVTERM::getCvterm('sequence', 'genetic_marker')->getCvtermID(),
        'haplotype_block' => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID(),
        'haplotype'       => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype')->getCvtermID(),
        'contains'        => MCL_CHADO_CVTERM::getCvterm('sequence', 'contains')->getCvtermID(),
        'variant_of'      => MCL_CHADO_CVTERM::getCvterm('sequence', 'variant_of')->getCvtermID(),
        'snp'             => MCL_CHADO_CVTERM::getCvterm('sequence', 'SNP')->getCvtermID(),
      );

      // Populates the mview.
      foreach($grouped_ids as $feature_id) {
        $sql = "
          SELECT DISTINCT H.feature_id AS haplotype_fid,
            H.uniquename AS haplotype, O.genus, O.species,
            GM.feature_id AS marker_fid, GM.uniquename AS marker,
            HB.feature_id AS haplotype_block_fid,
            HB.uniquename AS haplotype_block,
            SNP.value AS snp $select_str
          FROM {chado.feature} H
            $join_str
            INNER JOIN {chado.organism} O on O.organism_id = H.organism_id
            INNER JOIN {chado.feature_relationship} FR_H_GM on FR_H_GM.subject_id = H.feature_id
            INNER JOIN {chado.feature} GM on GM.feature_id = FR_H_GM.object_id
            INNER JOIN {chado.feature_relationshipprop} SNP
              ON SNP.feature_relationship_id = FR_H_GM.feature_relationship_id
            INNER JOIN {chado.feature_relationship} FR_H_HB on FR_H_HB.subject_id = H.feature_id
            INNER JOIN {chado.feature} HB on HB.feature_id = FR_H_HB.object_id
            INNER JOIN {chado.feature_relationship} FR_HB_GM
              ON FR_HB_GM.subject_id = HB.feature_id AND FR_HB_GM.object_id = GM.feature_id
          WHERE HB.feature_id = :feature_id AND HB.type_id = :haplotype_block
            AND FR_HB_GM.type_id = :contains AND GM.type_id = :genetic_marker
            AND FR_H_HB.type_id = :variant_of AND H.type_id = :haplotype
            AND FR_H_GM.type_id = :contains AND SNP.type_id = :snp
          ORDER BY HB.uniquename, GM.uniquename, H.uniquename
        ";
        $args = array(
          ':haplotype_block'  => $cvterms['haplotype_block'],
          ':genetic_marker'   => $cvterms['marker'],
          ':haplotype'        => $cvterms['haplotype'],
          ':contains'         => $cvterms['contains'],
          ':variant_of'       => $cvterms['variant_of'],
          ':snp'              => $cvterms['snp'],
          ':feature_id'       => $feature_id,
        );
        $results = db_query($sql, $args);
        $dup_data = array();
        while ($arr = $results->fetchAssoc()) {

          // Checks for a duplication.
          $keys = array(
            $arr['marker_fid'],
            $arr['haplotype_block_fid'],
            $arr['haplotype_fid'],
          );
          $dup_key = vsprintf("%d-%d-%d", $keys);
          if (array_key_exists($dup_key, $dup_data)) {
            continue;
          }
          else {
            $dup_data[$dup_key] = TRUE;
          }

          // Adds node_id.
          $arr['node_id'] = $this->node_id;
          $arr['root_id'] = $this->node_id;
          $arr['chado']   = 1;

          // Inserts into the mview.
          db_insert($this->getMView())
            ->fields($arr)
            ->execute();
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

    /**
   * Update the mviews of the data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByBIMS(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Local variables.
      $program_id = $bims_program->getProgramID();

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsBIMS($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'haplotype_block' => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID(),
        'haplotype'       => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype')->getCvtermID(),
        'marker'          => MCL_CHADO_CVTERM::getCvterm('sequence', 'genetic_marker')->getCvtermID(),
        'contains'        => MCL_CHADO_CVTERM::getCvterm('sequence', 'contains')->getCvtermID(),
        'variant_of'      => MCL_CHADO_CVTERM::getCvterm('sequence', 'variant_of')->getCvtermID(),
        'snp'             => MCL_CHADO_CVTERM::getCvterm('sequence', 'SNP')->getCvtermID(),
      );

      // Gets BIMS_CHADO tables.
      $bc_feature             = new BIMS_CHADO_FEATURE($this->node_id);
      $table_feature          = $bc_feature->getTableName('feature');
      $table_featureprop      = $bc_feature->getTableName('featureprop');
      $table_feature_rel      = $bc_feature->getTableName('feature_relationship');
      $table_feature_relprop  = $bc_feature->getTableName('feature_relationshipprop');

      // Populates the mview.
      foreach ($grouped_ids as $feature_id) {
        $sql = "
          SELECT DISTINCT RELPROP.value AS snp,
            HAPLOTYPE_BLOCK.feature_id AS haplotype_block_fid,
            HAPLOTYPE_BLOCK.name AS haplotype_block,
            MARKER.feature_id AS marker_fid, MARKER.name AS marker,
            HAPLOTYPE.feature_id AS haplotype_fid, HAPLOTYPE.name AS haplotype
            $select_str
          FROM {$table_feature} HAPLOTYPE_BLOCK
            INNER JOIN {chado.organism} O on O.organism_id = HAPLOTYPE_BLOCK.organism_id
            INNER JOIN {$table_feature_rel} FREL_H_HB on FREL_H_HB.object_id = HAPLOTYPE_BLOCK.feature_id
            INNER JOIN {$table_feature} HAPLOTYPE on HAPLOTYPE.feature_id = FREL_H_HB.subject_id
            INNER JOIN {$table_feature_rel} FREL_H_GM on FREL_H_GM.subject_id = HAPLOTYPE.feature_id
            INNER JOIN {$table_feature} MARKER on MARKER.feature_id = FREL_H_GM.object_id
            LEFT  JOIN {$table_feature_relprop} RELPROP on RELPROP.feature_relationship_id = FREL_H_GM.feature_relationship_id
            $join_str
          WHERE HAPLOTYPE_BLOCK.type_id = :type_id_haplotype_block
            AND MARKER.type_id = :type_id_marker
            AND HAPLOTYPE.type_id = :type_id_haplotype
            AND FREL_H_HB.type_id = :type_id_variant_of
            AND FREL_H_GM.type_id = :type_id_contains
            AND RELPROP.type_id = :type_id_snp
          ORDER BY HAPLOTYPE_BLOCK.name, MARKER.name, HAPLOTYPE.name
        ";
        $args = array(
          ':type_id_haplotype_block'  => $cvterms['haplotype_block'],
          ':type_id_marker'           => $cvterms['marker'],
          ':type_id_haplotype'        => $cvterms['haplotype'],
          ':type_id_contains'         => $cvterms['contains'],
          ':type_id_variant_of'       => $cvterms['variant_of'],
          ':type_id_snp'              => $cvterms['snp'],
          ':feature_id'               => $feature_id,
        );

        // Counts the number of data.
        $sql_count  = "SELECT COUNT(A.*) FROM ($sql) A";
        $total_data = db_query($sql_count, $args)->fetchField();

        // Adds the data.
        $num_data = 0;
        if ($total_data) {
          $results = db_query($sql, $args);
          while ($arr = $results->fetchAssoc()) {
            $num_data++;
            $key = $arr['haplotype_fid'] . ':' . $arr['marker_fid'];
            if (array_key_exists($key, $dup_haplotype_snp)) {
              continue;
            }
            else {
              $dup_haplotype_snp[$key] = TRUE;
            }

            // Initializes the fields.
            $fields = array();

            // Adds node ID.
            $fields['node_id'] = $this->node_id;
            $fields['root_id'] = $this->node_id;

            // Inserts into the mview.
            db_insert($this->getMView())
              ->fields($fields)
              ->execute();
          }

          // Shows the progress.
          if ($num_hb % 100 == 0) {
            $msg = sprintf("%d / %d = %.03f %%", $num_hs, $total_hs, $num_hs / $total_hs * 100.0);
            bims_print($msg);
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsChado(BIMS_PROGRAM $bims_program) {

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';




    // Returns SQL variables.
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsBIMS(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_hs       = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_feature             = new BIMS_CHADO_FEATURE($program_id);
    $table_feature          = $bc_feature->getTableName('feature');
    $table_fp               = $bc_feature->getTableName('featureprop');
    $table_feature_rel      = $bc_feature->getTableName('feature_relationship');
    $table_feature_relprop  = $bc_feature->getTableName('feature_relationshipprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';




    // Returns SQL variables.
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Initializes the returning array.
    $grouped_ids = array('chado' => array(), 'bims' => array());

    // GROUP : private.
    if ($group == 'private') {

    }

    // GROUP : public.
    else if ($group == 'public') {

    }

    // GROUP : project.
    else if ($group == 'project') {

    }

    // GROUP : program (private + public).
    else {

    }

    // Returns the grouped IDs.
    $ret_array = array('bims' => array(), 'chado' => array());
    if (empty($grouped_ids['chado']) && empty($grouped_ids['bims'])) {
      return array();
    }
    if (!empty($grouped_ids['bims'])) {
      $ret_array['bims'] = array_keys($grouped_ids['bims']);
    }
    if (!empty($grouped_ids['chado'])) {
      $ret_array['chado'] = array_keys($grouped_ids['chado']);
    }
    return $ret_array;
  }
}
