<?php
/**
 * The declaration of BIMS_MVIEW_IMAGE class.
*
*/
class BIMS_MVIEW_IMAGE extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_image';
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for images.',
      'fields' => array(
        'node_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'eimage_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'target_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'target_type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'filename' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'cvterm_id' => array(
          'type' => 'int',
        ),
        'caption' => array(
          'type' => 'text',
        ),
        'description' => array(
          'type' => 'text',
        ),
        'chado' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('eimage_id'),
    );
  }

  /**
   * Deletes the image by the target type and ID.
   *
   * @param string $target_type
   * @param integer $target_id
   *
   * @return boolean
   */
  public function deleteImageByTarget($target_type, $target_id) {

    // Deletes the images of the target.
    $transaction = db_transaction();
    try {

      // Gets all images.
      $table = $this->getMView();
      $sql = "
        SELECT eimage_id FROM {$table}
        WHERE target_type = LOWER(:target_type) AND target_id = :target_id
      ";
      $args = array(
        ':target_type'  => $target_type,
        ':target_id'    => $target_id,
      );
      $results = db_query($sql, $args);
      while ($eimage_id = $results->fetchField()) {
        if (!$this->deleteImage($eimage_id)) {
          throw new Exception("Error : Fail to delete the image");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the image of the provided eimage ID.
   *
   * @param integer $eimage_id
   *
   * @return boolean
   */
  public function deleteImage($eimage_id) {

    // Deletes the images and their tags.
    $transaction = db_transaction();
    try {

      // Deletes the tags.
      $bm_image_tag = new BIMS_MVIEW_IMAGE_TAG(array('node_id' => $this->node_id));
      db_delete($bm_image_tag->getMView())
        ->condition('eimage_id', $eimage_id)
        ->execute();

      // Deletes the images.
      db_delete($this->getMView())
        ->condition('eimage_id', $eimage_id)
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($grouped_ids)) {
        return TRUE;;
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'eimage_id', $grouped_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Updates the mviews by schema.
      if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['chado'])) {
        throw new Exception("Error : Failed to update the mview data in Chado");
      }
      if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['bims'])) {
        throw new Exception("Error : Failed to update the mview data in BIMS");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  public function _updateMViewByChado(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {









    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  public function _updateMViewByBIMS(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Local variables.
      $program_id = $bims_program->getProgramID();
      $m_image    = $this->getMView();
      $node_id    = $this->getNodeID();

      // Gets BIMS_CHADO tables.
      $bc_eimage          = new BIMS_CHADO_IMAGE($program_id);
      $table_eimage       = $bc_eimage->getTableName('eimage');
      $table_eimageprop   = $bc_eimage->getTableName('eimageprop');
      $table_eimage_link  = $bc_eimage->getTableName('eimage_link');

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsBIMS($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'photo_descriptor'  => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'photo_descriptor')->getCvtermID(),
        'caption'           => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'caption')->getCvtermID(),
        'description'       => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'description')->getCvtermID(),
      );

      // Gets the all the image for this program.
      $id_str = implode(',', $grouped_ids);
      $sql = "
        SELECT E.image_uri AS filename, E.eimage_id, E.eimage_type AS type,
          EIMAGE_LINK.target_type, EIMAGE_LINK.target_id,
          DESCRIPTOR.value AS cvterm_id, CAPTION.value AS caption,
          DESCRIPTION.value AS description $select_str
        FROM {$table_eimage} E
          $join_str
          INNER JOIN (
            SELECT EL.eimage_id, EL.target_type, EL.target_id
            FROM $table_eimage_link EL
          ) EIMAGE_LINK on EIMAGE_LINK.eimage_id = E.eimage_id
          LEFT JOIN (
            SELECT EP.eimage_id, EP.value
            FROM $table_eimageprop EP
            WHERE EP.type_id = :photo_descriptor
          ) DESCRIPTOR on DESCRIPTOR.eimage_id = E.eimage_id
          LEFT JOIN (
            SELECT EP.eimage_id, EP.value
            FROM $table_eimageprop EP
            WHERE EP.type_id = :caption
          ) CAPTION on CAPTION.eimage_id = E.eimage_id
          LEFT JOIN (
            SELECT EP.eimage_id, EP.value
            FROM $table_eimageprop EP
            WHERE EP.type_id = :description
          ) DESCRIPTION on DESCRIPTION.eimage_id = E.eimage_id
        WHERE E.eimage_id IN ($id_str)
      ";
      $args = array(
        ':photo_descriptor' => $cvterms['photo_descriptor'],
        ':caption'          => $cvterms['caption'],
        ':description'      => $cvterms['description'],
      );
      $results = db_query($sql, $args);
      while ($arr = $results->fetchAssoc()) {
        $cols = 'node_id';
        $vals = $node_id;
        foreach ($arr as $key => $val) {

          if ($val || $val == '0') {
            $cols .= ", $key";
            $vals .= ", '$val'";
          }
        }
        $sql = "INSERT INTO $m_image ($cols) VALUES ($vals)";
        db_query($sql);
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   *
   * @param $fields
   */
  public function update($fields) {
    $transaction = db_transaction();
    try {
      $eimage_id = $fields['eimage_id'];

      // Updates the record.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('eimage_id', $eimage_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsBIMS(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_image    = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_eimage         = new BIMS_CHADO_IMAGE($program_id);
    $table_eimageprop  = $bc_eimage->getTableName('eimageprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';

    // Returns if no BIMS CHADO.
    if (!db_table_exists($table_accessionprop)) {
      return array(
        'error'       => FALSE,
        'select_str'  => $select_str,
        'join_str'    => $join_str,
      );
    }

    // Adds the property columns.
    $props = $bims_program->getProperties('eimage', 'custom', BIMS_OPTION);
    foreach ((array)$props as $cvterm_id => $name) {
      $field = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_image, $field)) {
        db_add_field($m_image, $field, array('type' => 'text'));
      }

      // Updates the select statement.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";

      // Updates the join statement.
      $join_str .= "
        LEFT JOIN (
          SELECT EP.eimage_id, EP.value
          FROM {$table_eimageprop} EP
          WHERE EP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.eimage_id = T.eimage_id
      ";
    }
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Adds a new image.
   *
   * @param array
   *
   * @return boolean
   */
  public function addImage($details) {

    // Add a new image.
    $m_image  = $this->getMView();
    $node_id  = $this->getNodeID();
    $cols     = 'node_id';
    $vals     = $node_id;
    foreach ($details as $key => $val) {
      $cols .= ", $key";
      $vals .= ", '$val'";
    }
    $sql = "INSERT INTO $m_image ($cols) VALUES ($vals)";
    db_query($sql);
    return TRUE;
  }

  /**
   * Returns the images of the provided type.
   *
   * @param string $type
   *
   * @return array
   */
  public function getImages($type = 'any', $target_id = NULL) {
    $images = array();

    // Gets all images.
    $m_image = $this->getMView();
    if (db_table_exists($m_image)) {
      $sql = "SELECT MV.* FROM $m_image MV WHERE 1=1 ";
      $args = array();
      if ($type != 'any') {
        $sql .= " AND LOWER(type) = LOWER(:type) ";
        $args[':type'] = $type;
      }
      if ($target_id) {
        $sql .= " AND target_id = :target_id ";
        $args[':target_id'] = $target_id;
      }
      $sql .= ' ORDER BY MV.type, MV.filename ';
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        $images []= $obj;
      }
    }
    return $images;
  }

  /**
   * Returns image object.
   *
   * @param integer $eimage_id
   *
   * @return object
   */
  public function getImage($eimage_id) {
    $m_image = $this->getMView();
    if ($eimage_id != '' && db_table_exists($m_image)) {
      $sql = "SELECT MV.* FROM $m_image MV WHERE MV.eimage_id = :eimage_id";
      $results = db_query($sql, array(':eimage_id' => $eimage_id));
      return $results->fetchObject();
    }
    return NULL;
  }

  /**
   * Returns the number of the images of the provided type.
   *
   * @param string $type
   *
   * @return integer
   */
  public function getNumImage($type = 'any', $stock_id = NULL) {
    return sizeof($this->getImages($type, $stock_id));
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Local variables.
    $bc_image     = new BIMS_CHADO_IMAGE($this->node_id);
    $table_eimage = $bc_image->getTableName('eimage');

    // Initializes the returning array.
    $grouped_ids = array('chado' => array(), 'bims' => array());

    // GROUP : private.
    if ($group == 'private') {
      if (db_table_exists($table_eimage)) {
        $sql = "SELECT eimage_id FROM {$table_eimage}";
        $results = db_query($sql);
        while ($eimage_id = $results->fetchField()) {
          $grouped_ids['bims'][$eimage_id] = TRUE;
        }
      }
    }

    // GROUP : public.
    else if ($group == 'public') {
      //....


    }

    // GROUP : eimage.
    else if ($group == 'eimage') {
      $id_list = implode(",", $group_ids);

      // BIMS.
      if (db_table_exists($table_eimage)) {
        $sql = "
          SELECT eimage_id FROM {$table_ep}
          WHERE eimage_id IN ($id_list)
        ";
        $results = db_query($sql);
        while ($eimage_id = $results->fetchField()) {
          $grouped_ids['bims'][$eimage_id] = TRUE;
        }
      }

      // Chado.
      //....

    }

    // GROUP : program (private + public).
    else {

      // BIMS.
      if (db_table_exists($table_eimage)) {
        $sql = "SELECT eimage_id FROM {$table_eimage}";
        $results = db_query($sql);
        while ($eimage_id = $results->fetchField()) {
          $grouped_ids['bims'][$eimage_id] = TRUE;
        }
      }

      // Chado.
      //....
    }

    // Returns the grouped IDs.
    $ret_array = array('bims' => array(), 'chado' => array());
    if (empty($grouped_ids['chado']) && empty($grouped_ids['bims'])) {
      return array();
    }
    if (!empty($grouped_ids['bims'])) {
      $ret_array['bims'] = array_keys($grouped_ids['bims']);
    }
    if (!empty($grouped_ids['chado'])) {
      $ret_array['chado'] = array_keys($grouped_ids['chado']);
    }
    return $ret_array;
  }
}