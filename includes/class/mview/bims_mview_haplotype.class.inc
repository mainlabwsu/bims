<?php
/**
 * The declaration of BIMS_MVIEW_HAPLOTYPE class.
 *
 */
class BIMS_MVIEW_HAPLOTYPE extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_haplotype';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_mview = new BIMS_MVIEW_HAPLOTYPE(array('node_id' => $node_id));
    if ($bims_mview) {
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for haplotype data.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'root_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'dataset_name' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'haplotype_block_fid' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'haplotype_block' => array(
          'type'      => 'varchar',
          'length'    => '255',
          'not null'  => TRUE,
        ),
        'stock_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'accession' => array(
          'type'      => 'varchar',
          'length'    => '255',
          'not null'  => TRUE,
        ),
        'genus_accession' => array(
          'type'      => 'varchar',
          'length'    => '255',
          'not null'  => TRUE,
        ),
        'species_accession' => array(
          'type'      => 'varchar',
          'length'    => '255',
          'not null'  => TRUE,
        ),
        'chado' => array(
          'type'      => 'int',
          'not null'  => TRUE,
          'default'   => 0,
        ),
        'genotype_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'haplotype' => array(
          'type' => 'text',
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'haplotype_block_fid', 'stock_id', 'genotype_id')
      ),
      'indexes' => array(
        'mh_accession' => array('accession'),
        'mh_haplotype_block' => array('haplotype_block'),
      ),
    );
  }

  /**
   * Returns the distinct haplotype blocks.
   *
   * @return array
   */
  public function getHaplotypeBlocks() {
    $m_haplotype = $bims_mview->getMView();
    $sql = "SELECT DISTINCT MV.feature_id, MV.haplotype_block FROM {$m_haplotype} MV";
    $results = db_query($sql);
    $haplotype_blocks = array();
    while ($obj = $results->fetchObject()) {
      $haplotype_blocks[$obj->feature_id] = $obj->haplotype_block;
    }
    return $haplotype_blocks;
  }

  /**
   * Returns the distinct accessions.
   *
   * @return array
   */
  public function getAccessions() {
    $m_haplotype = $bims_mview->getMView();
    $sql = "SELECT DISTINCT MV.stock_id, MV.accession FROM {$m_haplotype} MV";
    $results = db_query($sql);
    $accessions = array();
    while ($obj = $results->fetchObject()) {
      $accessions[$obj->stock_id] = $obj->accession;
    }
    return $accessions;
  }

  /**
   * @see BIMS_MVIEW::deleteByKey()
   */
  public function deleteByKey($key, $value) {

    // Deletes the data.
    $transaction = db_transaction();
    try {

      // Deletes the data by key.
      db_delete($this->getMView())
      ->condition($key, $value)
      ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Clears the data of the provided trial ID in the mview.
   *
   * @param integer $trial_id
   *
   * @return boolean
   */
  public function clearTrial($trial_id) {

    // Clears the data in the mview.
    if ($this->exists()) {
      db_delete($this->getMView())
        ->condition('node_id', $trial_id, '=')
        ->execute();
    }

    // Creates the mview if not exist.
    else {
      $this->createMView();
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

          // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($grouped_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'nd_experiment_id', $grouped_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Finds new trials and adds them to the program if exist.
      $bims_program->updateTrials();

      // Updates the mviews by schema.
      if (!$this->_updateMViewByChado($bims_program, $grouped_ids['chado'])) {
        throw new Exception("Error : Failed to update the mview data in Chado");
      }
      if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['bims'])) {
        throw new Exception("Error : Failed to update the mview data in BIMS");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the mview for genotyping data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByChado(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsChado($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'haplotype_block' => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID(),
        'chromosome'      => MCL_CHADO_CVTERM::getCvterm('sequence', 'chromosome')->getCvtermID(),
      );

      // Populates the mview.
      $accessions       = array();
      $haplotype_blocks = array();
      foreach ($grouped_ids as $project_id) {
        $trial = BIMS_TRIAL::byProjectID($project_id, $this->node_id);
        $sql = "
          SELECT VARIETY.* $select_str
          FROM (
            SELECT DISTINCT P.project_id, P.name AS dataset_name,
              S.stock_id, S.uniquename AS accession,
              F.feature_id AS haplotype_block_fid, F.uniquename AS haplotype_block,
              G.genotype_id, G.description AS genotype
            FROM {chado.project} P
              INNER JOIN {chado.nd_experiment_project} NEPR on NEPR.project_id = P.project_id
              INNER JOIN {chado.nd_experiment} NE on NE.nd_experiment_id = NEPR.nd_experiment_id
              INNER JOIN {chado.nd_experimentprop} NEPROP on NEPROP.nd_experiment_id = NE.nd_experiment_id
              INNER JOIN {chado.nd_experiment_stock} NES on NES.nd_experiment_id = NE.nd_experiment_id
              INNER JOIN {chado.stock} S on S.stock_id = NES.stock_id
              INNER JOIN {chado.nd_experiment_genotype} NEG on NEG.nd_experiment_id = NE.nd_experiment_id
              INNER JOIN {chado.genotype} G on G.genotype_id = NEG.genotype_id
              INNER JOIN {chado.feature_genotype} FG on FG.genotype_id = G.genotype_id
              INNER JOIN {chado.feature} F on F.feature_id = FG.feature_id
            WHERE P.project_id = :project_id AND F.type_id = :haplotype_block
          ) VARIETY $join_str
        ";
        $args = array(
          ':project_id'       => $trial->getProjectID(),
          ':haplotype_block'  => $cvterms['haplotype_block'],
        );

        // Counts the number of data.
        $sql_count  = "SELECT COUNT(T.*) FROM ($sql) T";
        $total_data = db_query($sql_count, $args)->fetchField();

        // Adds the data.
        $num_data = 0;
        if ($total_data) {
          $results = db_query($sql, $args);
          while ($arr = $results->fetchAssoc()) {
            $accessions[$arr['stock_id']] = TRUE;
            $haplotype_blocks[$arr['haplotype_block_fid']]  = TRUE;
            $num_data++;

            // Initializes the fields.
            $fields = array();

            // Adds node ID.
            $fields['node_id']  = $trial->getNodeID();
            $fields['root_id']  = $trial->getRootID();
            $fields['chado']    = 1;
            foreach ($arr as $key => $val) {

              // Skips if no value.
              if (!($val || $val == '0')) {
                continue;
              }
              $fields[$key] = $val;
            }

            // Inserts into mview.
            db_insert($this->getMView())
              ->fields($fields)
              ->execute();

            // Shows the progress.
            if ($num_data % 5000 == 0) {
              $msg = sprintf("%d / %d = %.03f %%", $num_data, $total_data, $num_data / $total_data * 100.0);
              bims_print($msg);
            }
          }
        }

        // Stores the number of the data.
        $trial->setPropByKey('num_data', $num_data);
        if (!$trial->update()) {
          throw new Exception("Error : Failed to update the number of the data");
        }
      }

      // Updates BIMS_MVIEW_STOCK.
      if (!empty($accessions)) {
        $bims_mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $trial->getRootID()));

        // Imports the accessions.
        $bims_mview_stock->updateMView(FALSE, 'stock', array_keys($accessions));
      }

      // Updates BIMS_MVIEW_HAPLOTYPE_BLOCK.
      if (!empty($haplotype_blocks)) {
        $bims_mview_hb = new BIMS_MVIEW_HAPLOTYPE_BLOCK(array('node_id' => $trial->getRootID()));

        // Imports the haplotype blocks.
        $bims_mview_hb->updateMView(FALSE, 'feature', array_keys($haplotype_blocks));
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the mview for genotyping data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByBIMS(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Local variables.
      $program_id = $bims_program->getProgramID();

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsChado($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'haplotype_block' => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID(),
        'haplotype'       => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype')->getCvtermID(),
      );

      // Gets BIMS_CHADO tables.
      $bc_project         = new BIMS_CHADO_PROJECT($program_id);
      $bc_accession       = new BIMS_CHADO_ACCESSION($program_id);
      $bc_feature         = new BIMS_CHADO_FEATURE($program_id);
      $bc_nd_experiment   = new BIMS_CHADO_ND_EXPERIMENT($program_id);
      $bc_genotype        = new BIMS_CHADO_GENOTYPE($program_id);
      $table_project      = $bc_project->getTableName('project');
      $table_ne           = $bc_nd_experiment->getTableName('nd_experiment');
      $table_feature      = $bc_feature->getTableName('feature');
      $table_featureprop  = $bc_feature->getTableName('featureprop');
      $table_fg           = $bc_feature->getTableName('feature_genotype');
      $table_accession    = $bc_accession->getTableName('accession');
      $table_genotype     = $bc_genotype->getTableName('genotype');

      // Populates the mview.
      foreach ($grouped_ids as $project_id) {
        $trial = BIMS_TRIAL::byProjectID($project_id, $this->node_id);
        $sql = "
          SELECT DISTINCT P.project_id, P.name AS dataset_name,
            HAPLOTYPE_BLOCK.feature_id AS haplotype_block_fid,
            HAPLOTYPE_BLOCK.name AS haplotype_block, S.stock_id,
            S.name AS accession, S_O.genus AS genus_accession,
            S_O.species AS species_accession,
            G.genotype_id, G.description AS haplotype
            $select_str
          FROM {$table_project} P
            INNER JOIN {$table_ne} NE on NE.project_id = P.project_id
            INNER JOIN {$table_accession} S on S.stock_id = NE.stock_id
            INNER JOIN {chado.organism} S_O on S_O.organism_id = S.organism_id
            INNER JOIN {$table_genotype} G on G.genotype_id = NE.genotype_id
            INNER JOIN {$table_fg} FG on FG.genotype_id = G.genotype_id
            INNER JOIN {$table_feature} HAPLOTYPE_BLOCK on HAPLOTYPE_BLOCK.feature_id = FG.feature_id
            $join_str
          WHERE P.project_id = :project_id AND
            HAPLOTYPE_BLOCK.type_id = :type_id_hb
          ORDER BY P.name, HAPLOTYPE_BLOCK.name, S.name, G.description
        ";
        $args = array(
          ':project_id' => $trial->getProjectID(),
          ':type_id_hb' => $cvterms['haplotype_block'],
        );

        // Counts the number of data.
        $sql_count  = "SELECT COUNT(A.*) FROM ($sql) A";
        $total_data = db_query($sql_count, $args)->fetchField();

        // Updates the mview for haplotype SNP.
        $dup_haplotype  = array();
        $num_data       = 0;
        $results        = db_query($sql, $args);
        while ($arr = $results->fetchAssoc()) {
          $num_data++;

          // Adds node_id.
          $arr['node_id'] = $this->node_id;
          $arr['root_id'] = $this->node_id;

          // Inserts into mview.
          db_insert($this->getMView())
            ->fields($arr)
            ->execute();

          // Shows the progress.
          if ($num_data % 100 == 0) {
            $msg = sprintf("%d / %d = %.03f %%", $num_data, $total_data, $num_data / $total_data * 100.0);
            bims_print($msg);
          }
        }

        // Stores the number of data in BIMS_TRIAL prop.
        $trial->setPropByKey('num_data', $num_data);
        if (!$trial->update()) {
          throw new Exception("Error : Failed to update the trial properties");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsChado(BIMS_PROGRAM $bims_program) {
    return array(
      'error'       => FALSE,
      'select_str'  => '',
      'join_str'    => '',
    );
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsBIMS(BIMS_PROGRAM $bims_program) {

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';



    // Returns SQL variables.
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Initializes the returning array.
    $grouped_ids = array('chado' => array(), 'bims' => array());

    // GROUP : private.
    if ($group == 'private') {
      $sql = "
        SELECT N.project_id FROM {bims_node} N
        WHERE LOWER(N.project_sub_type) = :sub_type AND N.project_id NOT IN (
          SELECT project_id
          FROM {bims_imported_project}
          WHERE program_id = :program_id
      )";
      $args = array(
        ':program_id'  => $this->node_id,
        ':sub_type'    => 'haplotype',
      );
      $results = db_query($sql, $args);
      while ($project_id = $results->fetchField()) {
        $grouped_ids['chado'][$project_id] = TRUE;
      }
    }

    // GROUP : public.
    else if ($group == 'public') {
      $sql = "
        SELECT N.project_id
        FROM {bims_node} N
          INNER JOIN {bims_imported_project} IP on IP.project_id = N.project_id
        WHERE LOWER(N.project_sub_type) = :sub_type AND IP.program_id = :program_id
      ";
      $args = array(
        ':program_id'  => $this->node_id,
        ':sub_type'    => 'haplotype',
      );
      $results = db_query($sql, $args);
      while ($project_id = $results->fetchField()) {
        $grouped_ids['chado'][$project_id] = TRUE;
      }
    }

    // GROUP : project.
    else if ($group == 'project') {
      foreach ((array)$group_ids as $project_id) {
        $keys = array(
          'program_id' => $this->node_id,
          'project_id' => $project_id,
        );
        $imported_proj = BIMS_IMPORTED_PROJECT::byKey($keys);
        $schema = $imported_proj ? 'chado' : 'bims';
        $grouped_ids[$schema][$project_id] = TRUE;
      }
    }

    // GROUP : program (private + public).
    else {
      $trials = $bims_program->getTrialsByProjectSubType('haplotype', BIMS_CLASS);
      foreach ((array)$trials as $trial) {
        $project_ids[$trial->getProjectID()] = TRUE;
      }
    }

    // Returns the grouped IDs.
    $ret_array = array('bims' => array(), 'chado' => array());
    if (empty($grouped_ids['chado']) && empty($grouped_ids['bims'])) {
      return array();
    }
    if (!empty($grouped_ids['bims'])) {
      $ret_array['bims'] = array_keys($grouped_ids['bims']);
    }
    if (!empty($grouped_ids['chado'])) {
      $ret_array['chado'] = array_keys($grouped_ids['chado']);
    }
    return $ret_array;
  }
}
