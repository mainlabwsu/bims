<?php
/**
 * The declaration of BIMS_MVIEW_STOCK_LOC class.
*
*/
class BIMS_MVIEW_STOCK_LOC extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_stock_loc';
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for locations.',
      'fields' => array(
        'stock_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('stock_id', 'nd_geolocation_id')
      ),
    );
  }

  /**
   * Updates the locations.
   *
   * @param integer $stock_id
   * @param array $nd_geolocation_ids
   *
   * @return boolean.
   */
  public function updateLocation($stock_id, $nd_geolocation_ids = array()) {

    // Deletes the all associated locations.
    if (!$this->deleteLocation($stock_id)) {
      return FALSE;
    }

    // Then adds the the given locations.
    if (!$this->addLocation($stock_id, $nd_geolocation_ids)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds the associated locations.
   *
   * @param integer $stock_id
   * @param array $nd_geolocation_ids
   *
   * @return boolean
   */
  public function addLocation($stock_id, $nd_geolocation_ids = array()) {
    $transaction = db_transaction();
    try {

      // Adds the associated locations.
      foreach ((array)$nd_geolocation_ids as $nd_geolocation_id) {
        $fields = array(
          'stock_id'          => $stock_id,
          'nd_geolocation_id' => $nd_geolocation_id,
        );
        db_insert($this->getMView())
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the associated locations.
   *
   * @param integer $stock_id
   * @param array $nd_geolocation_ids
   *
   * @return boolean
   */
  public function deleteLocation($stock_id, $nd_geolocation_ids = array()) {
    $transaction = db_transaction();
    try {

      // Deletes the all associated locations.
      if (empty($stock_ids)) {
        db_delete($this->getMView())
          ->condition('stock_id', $stock_id, '=')
          ->execute();
      }
      else {
        foreach ((array)$nd_geolocation_ids as $nd_geolocation_id) {
          db_delete($this->getMView())
            ->condition('stock_id', $stock_id, '=')
            ->condition('nd_geolocation_id', $nd_geolocation_id, '=')
            ->execute();
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }
}