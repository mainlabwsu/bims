<?php
/**
 * The declaration of BIMS_MVIEW_ALIAS_ACCESSION class.
 *
 */
class BIMS_MVIEW_ALIAS_ACCESSION extends BIMS_MVIEW_ALIAS {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW_ALIAS::updateAlias()
   */
  public function updateAlias($schema, $stock_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Checks the stock IDs.
      if (empty($stock_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        $group_ids = array();
        $group_ids[$schema] = $stock_ids;
        $this->clearAliasByID('accession', $stock_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Updates the mview.
      $func_name = ($schema == 'chado') ? 'Chado' : 'BIMS';
      if (!$this->{'_updateAlias' . $func_name}($bims_program, $stock_ids)) {
        throw new Exception("Error : Failed to update the mview data");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW_ALIAS::_updateAliasChado()
   */
  protected function _updateAliasChado(BIMS_PROGRAM $bims_program, $stock_ids) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Gets the cvterms.
      $id_alias = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'alias')->getCvtermID();

      // Gets all the aliases.
      foreach ((array)$stock_ids as $stock_id) {
        $sql = "
          SELECT SP.value
          FROM {chado.stockprop} SP
          WHERE SP.type_id = :type_id AND SP.stock_id = :stock_id
        ";
        $args = array(
          ':stock_id' => $stock_id,
          ':type_id'  => $id_alias,
        );
        $results = db_query($sql, $args);
        while ($alias = $results->fetchField()) {

          // Adds the alias.
          $details = array(
            'target_id' => $stock_id,
            'type'      => 'accession',
            'alias'     => $alias,
          );
          if (!$this->addAlias($details)) {
            throw new Exception('Error : Failed to add an alias');
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW_ALIAS::_updateAliasBIMS()
   */
  protected function _updateAliasBIMS(BIMS_PROGRAM $bims_program, $stock_ids) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Gets the aliases from BIMS_CHADO and Adds them.
      $bc_accession = new BIMS_CHADO_ACCESSION($this->node_id);
      foreach ((array)$stock_ids as $stock_id) {
        $bc_accession->setStockID($stock_id);

        // Gets the aliases of this accession.
        $aliases = $bc_accession->getAlias(BIMS_OBJECT);
        foreach ((array)$aliases as $alias) {

          // Adds the alias.
          $details = array(
            'target_id' => $stock_id,
            'type'      => 'accession',
            'alias'     => $alias->value,
          );
          if (!$this->addAlias($details)) {
            throw new Exception('Error : Failed to add an alias');
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }
}
