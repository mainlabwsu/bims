<?php
/**
 * The declaration of BIMS_MVIEW_MARKER_MAP class.
*
*/
class BIMS_MVIEW_MARKER_MAP extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_marker_map';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_program = BIMS_PROGRAM::byID($node_id);
    if ($bims_program) {
      $bm_marker = new BIMS_MVIEW_MARKER(array('node_id' => $node_id));
      return $bm_marker->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for marker map.',
      'fields' => array(
        'node_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'feature_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'chado_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'genome' => array(
          'type' => 'varchar',
          'length' => '500',
        ),
        'analysis_id' => array(
          'type' => 'int',
        ),
        'chromosome' => array(
          'type' => 'varchar',
          'length' => '500',
        ),
        'chromosome_fid' => array(
          'type' => 'int',
        ),
        'loc_start' => array(
          'type' => 'int',
        ),
        'loc_stop' => array(
          'type' => 'int',
        ),
      ),
      'primary key' => array('feature_id'),
    );
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $trials = array()) {

    // Updates the mview.
    $transaction = db_transaction();
    try {
      $m_mm     = $this->getMView();
      $node_id  = $this->getNodeID();

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($node_id);

      // Updates BIMS_MVIEW_MARKER_MAP with data in BIMS_CHADO.
      if (!$this->updateMViewBIMSChado($bims_program, !$recreate)) {
        throw new Exception("Error : Failed to update BIMS_MVIEW_MARKER_MAP from the BIMS Chado");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates BIMS_MVIEW_MARKER with data in BIMS Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param boolean $clear
   *
   * @return boolean
   */
  public function updateMViewBIMSChado(BIMS_PROGRAM $bims_program, $clear = FALSE) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_mm       = $this->getMView();
    $node_id    = $this->getNodeID();

    // Gets BIMS_CHADO tables.
    $bc_feature         = new BIMS_CHADO_FEATURE($program_id);
    $table_feature      = $bc_feature->getTableName('feature');
    $table_featureprop  = $bc_feature->getTableName('featureprop');

    // Updates the mview with data in BIMS_CHADO.
    if (!db_table_exists($table_feature)) {
      return TRUE;
    }

    // Clears the data.
    if ($clear) {
      $this->clear(array('chado' => '0'));
    }

    // Updates the columns of the materialized view.
    $ret_arr = $this->_updateColumns($bims_program);
    if ($ret_arr['error']) {
      return FALSE;
    }
    $select_str = $ret_arr['select_str'];
    $join_str   = $ret_arr['join_str'];

    // Gets the cvterms.
    $cvterms = array(
      'comments'  => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'comments')->getCvtermID(),
      'rs_id'     => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'rs_id')->getCvtermID(),
      'ss_id'     => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'ss_id')->getCvtermID(),
    );

    // Gets the all the markers.
    $sql = "
      SELECT DISTINCT MARKER.feature_id, MARKER.uniquename, MARKER.name,
        MARKER.type_id, C.name AS type, O.genus, O.species, O.organism_id,
        COMMENTS.value AS comments, SS_ID.value AS ss_id, RS_ID.value as rs_id,
        GENOME.name AS genome
        $select_str
      FROM {$table_feature} MARKER
        $join_str
      WHERE 1=1
    ";
    $args = array();
    $results = db_query($sql, $args);
    while ($arr = $results->fetchAssoc()) {


    }

    // Adds node_id.
    $arr['node_id'] = $node_id;

    // Inserts into mview.
    db_insert($m_mm)
      ->fields($arr)
      ->execute();
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return boolean
   */
  private function _updateColumns(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_mm       = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_feature         = new BIMS_CHADO_FEATURE($program_id);
    $table_featureprop  = $bc_feature->getTableName('featureprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';

    // Returns if no BIMS CHADO.
    if (!db_table_exists($table_featureprop)) {
      return array(
        'error'       => FALSE,
        'select_str'  => $select_str,
        'join_str'    => $join_str,
      );
    }
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Updates the record.
   *
   * @param array $fields
   */
  public function update($fields) {
    $transaction = db_transaction();
    try {
       $feature_id = $fields['feature_id'];

       // Updates the record.
       db_update($this->getMView())
         ->fields($fields)
         ->condition('feature_id', $feature_id, '=')
         ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the marker map.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateMarkerMap($details) {

    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'feature_id'      => $details['feature_id'],
        'chado_id'        => $details['chado_id'],
        'genome'          => $details['genome'],
        'analysis_id'     => $details['analysis_id'],
        'chromosome'      => $details['chromosome'],
        'chromosome_fid'  => $details['chromosome_fid'],
        'loc_start'       => $details['loc_start'],
        'loc_stop'        => $details['loc_stop'],
      );

      // Updates the marker.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('feature_id', $details['feature_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new marker map.
   *
   * @param array
   *
   * @return boolean
   */
  public function addMakerMap($details) {

    // Add a marker.
    $m_mm     = $this->getMView();
    $node_id  = $this->getNodeID();
    $cols     = 'node_id';
    $vals     = $node_id;
    foreach ($details as $key => $val) {
      $cols .= ", $key";
      $vals .= ", '$val'";
    }
    $sql = "INSERT INTO $m_mm ($cols) VALUES ($vals)";
    db_query($sql);
    return TRUE;
  }

  /**
   * Returns all mapped markers.
   *
   * @param integer $flag
   *
   * @return array
   */
  public function getMappedMarkers($flag = BIMS_OPTION) {

    // Gets all markers.
    $m_mm = $this->getMView();
    if (db_table_exists($m_mm)) {
      $sql = "SELECT MV.* FROM $m_mm MV ORDER BY MV.name";
      $results = db_query($sql);
      $markers = array();
      while ($obj = $results->fetchObject()) {
        if ($flag == BIMS_OPTION) {
          $markers [$obj->feature_id]= $obj->name;
        }
        else if ($flag == BIMS_OBJECT) {
          $markers []= $obj;
        }
      }
      return $markers;
    }
    return array();
  }

  /**
   * Returns the marker object.
   *
   * @param integer $feature_id
   * @param integer $flag
   *
   * @return object|array
   */
  public function getMarker($feature_id, $flag = BIMS_OBJECT) {
    if ($feature_id) {
      $m_mm = $this->getMView();
      $sql = "SELECT MV.* FROM $m_mm MV WHERE MV.feature_id = :feature_id";
      $results = db_query($sql, array(':feature_id' => $feature_id));
      if ($flag == BIMS_OBJECT) {
        return $results->fetchObject();
      }
      return $results->fetchAssoc();
    }
    return NULL;
  }
}