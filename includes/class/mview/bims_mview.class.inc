<?php
/**
 * The declaration of BIMS_MVIEW class.
 *
 */
class BIMS_MVIEW {

  /**
   * Class data members.
   */
  protected $project_id   = NULL;
  protected $node_id      = NULL;
  protected $root_id      = NULL;

 /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {

    // Initializes data members.
    if (is_array($details)) {
      $this->project_id = (array_key_exists('project_id', $details))  ? $details['project_id']  : NULL;
      $this->node_id    = (array_key_exists('node_id', $details))     ? $details['node_id']     : NULL;
      $this->root_id    = (array_key_exists('root_id', $details))     ? $details['root_id']     : NULL;
    }
    else {
      $this->node_id = $details;
    }

    // Updates project_id and root_id.
    if ($this->node_id) {
      $bims_node = BIMS_NODE::byID($this->node_id);
      if ($bims_node) {
        if (!$this->project_id) {
          $this->project_id = $bims_node->getProjectID();
        }
        if (!$this->root_id) {
          $this->root_id = $bims_node->getRootID();
        }
      }
    }

    // Creates a mview if not exists.
    if (get_class() != 'BIMS_MVIEW') {
      $this->createMView();
    }
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Returns the information the materialized views.
   */
  public static function getMViewInfo() {
    $info = '';

    // MView description.
    $rows = array(
      array('1', 'descriptor', 'Descriptor group', 'The information about all descriptors in a descriptor group'),
      array('2', 'program-descriptor-group', 'Program', 'It keeps the relationship between program and descriptor group'),
      array('3', 'location', 'Program', 'All locaions of phenotyping data of all trials in a program.'),
      array('4', 'stock', 'Program', 'All accessions of phenotyping data of all trials in a program.'),
      array('5', 'phenotype', 'Trial', 'Phenotyping data of trials.'),
      array('6', 'phenotype-stats', 'Trial', 'Statistical information about the phenotyping data of trials.'),
    );
    $headers = array(
      array('data' => '', 'width' => '1%'),
      array('data' => 'Type', 'width' => '20%'),
      array('data' => 'Target', 'width' => '10%'),
      'Description',
    );
    $table_vars = array(
      'header'      => $headers,
      'rows'        => $rows,
      'attributes'  => array('style' => '100%'),
    );
    $info .= theme('table', $table_vars);
    return $info;
  }

  /**
   * Returns the mview by keys.
   *
   * @param array $keys
   *
   * @return various
   */
  public function getByKey($keys) {
    $mview = $this->getMView();
    $sql = "SELECT MV.* FROM $mview MV WHERE MV.node_id = :node_id ";
    $args = array(':node_id' => $this->node_id);
    foreach ($keys as $key => $value) {
      $args[":$key"] = $value;
      $sql .= " AND LOWER(MV.$key) = LOWER(:$key)";
    }
    return db_query($sql, $args)->fetchObject();
  }

  /**
   * Returns the name of the materialized view.
   *
   * @param boolean $flag
   *
   * @return string
   */
  public function getMView($flag = TRUE) {
    // To be overridden by Child class.
    return '';
  }

  /**
   * Returns the name of the materialized view by node ID.
   *
   * @param integer $node_id
   *
   * @return string
   */
  public static function getMViewByNodeID($node_id) {
    // To be overridden by Child class.
    return '';
  }

  /**
   * Clear a materialized view.
   *
   * @param array $conditions
   *
   * @return boolean
   */
  public function clear($conditions = array()) {

    // Creates the mview if not exist.
    if ($this->exists()) {
      return $this->deleteMView($conditions);
    }
    return $this->createMView();
  }

  /**
   * Clears the group data.
   *
   * @param string $mview
   * @param string $attr_name
   * @param array $grouped_ids
   * @param boolean $merged
   *
   * @return boolean
   */
  public static function clearByGroupID($mview, $attr_name, $grouped_ids = array(), $merged = FALSE) {

    // Clears the data.
    if (!empty($grouped_ids)) {
      $sql = "DELETE FROM {$mview} WHERE 1=1 ";
      $id_list = '';
      if ($merged) {
        $id_list = implode(',', $grouped_ids);
      }
      else {
        $all_ids  = array();
        if (array_key_exists('chado', $grouped_ids) && !empty($grouped_ids['chado'])) {
          $all_ids = array_merge($all_ids, $grouped_ids['chado']);
        }
        if (array_key_exists('bims', $grouped_ids) && !empty($grouped_ids['bims'])) {
          $all_ids = array_merge($all_ids, $grouped_ids['bims']);
        }
        if (!empty($all_ids)) {
          $id_list = implode(',', $all_ids);
        }
      }
      if ($id_list) {
        db_query("$sql AND $attr_name IN ($id_list) ");
      }
    }
    return TRUE;
  }

  /**
   * Creates a materialized view.
   *
   * @param boolean $force
   *
   * @return boolean
   */
  public function createMView($force = FALSE) {
    if ($this->exists() && $force) {
      $this->dropMView();
    }
    if (!$this->exists()) {
      $transaction = db_transaction();
      try {
        db_create_table($this->getMView(), $this->getTableSchema());
      }
      catch (Exception $e) {
        $transaction->rollback();
        drupal_set_message($e->getMessage(), 'error');
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Deletes the materialized view table.
   *
   * @param array $conditions
   *
   * @return boolean
   */
  public function deleteMView($conditions = array()) {
    if ($this->exists()) {

      // Deletes the mview.
      $transaction = db_transaction();
      try {
        $mview = $this->getMView();
        $sql = "DELETE FROM {$mview} WHERE 1=1 ";
        $args = array();
        foreach ((array)$conditions as $key => $value) {
          $sql .= " AND $key = :$key ";
          $args[":$key"] = $value;
        }
        db_query($sql, $args);
      }
      catch (Exception $e) {
        $transaction->rollback();
        drupal_set_message($e->getMessage(), 'error');
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Drops a materialized view table.
   *
   * @return boolean
   */
  public function dropMView() {
    $mview = $this->getMView();
    if (db_table_exists($mview)) {
      $transaction = db_transaction();
      try {
        db_drop_table($mview);
      }
      catch (Exception $e) {
        $transaction->rollback();
        drupal_set_message($e->getMessage(), 'error');
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Checks if the materialized view exists.
   *
   * @return boolean
   */
  public function exists() {
    return db_table_exists($this->getMView());
  }

  /**
   * Checks if it has data.
   *
   * @return boolean
   */
  public function hasData() {
    if ($this->exists()) {
      $mview = $this->getMView();
      $sql = "SELECT COUNT(*) FROM {$mview} LIMIT 1";
      $num = db_query($sql)->fetchField();
      return $num ? TRUE : FALSE;
    }
    return FALSE;
  }

  /**
   * Returns the schema array of the mview.
   */
  public function getTableSchema() {
    // To be overridden by Child class.
    return array();
  }
  /**
   * Adds the data to the mview. Checks for duplication if $dup_key is not
   * empty.
   *
   * @param string $mview
   * @param array $fields
   * @param array $keys
   * @param array $dup_keys
   *
   * @return boolean
   */
  protected function addData($mview, $fields, $dup_keys = array()) {

    // Checks for a duplication.
    if (!empty($dup_keys)) {
      $sql = "SELECT COUNT(*) FROM {$mview} WHERE 1=1" ;
      $args = array();
      foreach ($dup_keys as $key => $val) {
        $args[':' . $key] = $val;
        $sql .= " AND $key = :$key";
      }
      $num = db_query($sql, $args)->fetchField();
      if ($num) {
        return TRUE;
      }
    }

    // Adds the data.
    db_insert($mview)
      ->fields($fields)
      ->execute();
    return TRUE;
  }

  /**
   * Gets the feature IDs by the project.
   *
   * @param integer $feature_type_id
   * @param array $project_ids
   * @param array $group_ids
   */
  protected function getFeatureIDByProject($feature_type_id, $project_ids, &$group_ids) {

    foreach ((array)$project_ids as $project_id) {

      // Gets BIMS_TRIAL.
      $trial        = BIMS_TRIAL::byProjectID($project_id, $this->node_id);
      $sub_type     = $trial->getProjectSubType();
      $is_imported  = $trial->isImported();
      $args         = array(':project_id' => $project_id);

      // BIMS.
      if (!$is_imported) {
        $sql = '';
        if ($sub_type == 'ssr') {
          $bc_ssr     = new BIMS_CHADO_SSR($this->node_id);
          $table_name = $bc_ssr->getTableName('ssr');
          $sql = "
            SELECT DISTINCT feature_id FROM {$table_name}
            WHERE proejct_id = :project_id
          ";
        }
        else if ($sub_type == 'snp') {
          $bc_gc      = new BIMS_CHADO_GENOTYPE_CALL($this->node_id);
          $table_name = $bc_gc->getTableName('genotype_call');
          $sql = "
            SELECT DISTINCT feature_id FROM {$table_name}
            WHERE proejct_id = :project_id
          ";
        }
        else if ($sub_type == 'haplotype_block') {
//          $bc_hb      = new BIMS_CHADO_HAPLOTYPE($this->node_id);
//          $table_name = $bc_feature->getTableName('haplotype_block');
//          $sql = "
//            SELECT DISTINCT feature_id FROM {$table_name}
//            WHERE proejct_id = :project_id AND type_id = :type_id
//          ";
        }
      }

      // Chado.
      else {
        $args[':typet_id'] = $type_id;
        if (preg_match("/^(ssr|haplotype)$/", $sub_type)) {

          $sql = "
            SELECT DISTINCT FG.feature_id
            FROM {chado.feature_genotype} FG
              INNER JOIN {chado.feature} F on F.feature_id = FG.feature_id
              INNER JOIN {chado.nd_experiment_genotype} NEG on NG.genotype_id = FG.genotype_id
              INNER JOIN {chado.nd_experiment_project} NEPRJ on NEPRJ.nd_experiment_id = NEG.nd_experiment_id
            WHERE NEPRJ.project_id = :project_id AND F.type_id = :type_id
          ";
        }
        else if ($sub_type == 'snp') {
          $sql = "
            SELECT DISTINCT GC.feature_id
            FROM {chado.genotype_call} GC
              INNER JOIN {chado.feature} F on F.feature_id = GC.feature_id
            WHERE GC.project_id = :project_id AND F.type_id = :type_id
          ";
        }
        else if ($sub_type == 'haplotype_block') {
          $sql = "
            SELECT DISTINCT FG.feature_id
            FROM {chado.feature_genotype} FG
              INNER JOIN {chado.feature} F on F.feature_id = FG.feature_id
              INNER JOIN {chado.nd_experiment_genotype} NEG on NG.genotype_id = FG.genotype_id
              INNER JOIN {chado.nd_experiment_project} NEPRJ on NEPRJ.nd_experiment_id = NEG.nd_experiment_id
            WHERE NEPRJ.project_id = :project_id AND F.type_id = :type_id
          ";
        }
      }

      // Adds the feature IDs.
      if ($sql) {
        $results = db_query($sql, $args);
        $schema = ($is_imported) ? 'chado' : 'bims';
        while ($feature_id = $results->fetchField()) {
          $group_ids[$schema][$feature_id] = TRUE;
        }
      }
    }
  }

  /**
   * Update the materialized view.
   *
   * @param boolean $recreate
   * @param string $group
   * @param array $group_ids
   *
   * @return boolean
   */
  //public function updateMView($recreate = FALSE, $group = '', $group_ids = array(), $clear = TRUE, $stats_flag = FALSE) {
    // To be overridden by Child class.
  //  return TRUE;
  //}

  /**
   * Returns columns of the table.
   *
   * @return array
   */
  function getColumns() {
    $mview = $this->getMView();
    $sql = "SELECT MV.* FROM {$mview} MV";
    $assoc = db_query($sql)->fetchAssoc();
    return array_keys($assoc);
  }

  /**
   * Updates the trial name.
   *
   * @param integer $node_id
   * @param string $name
   *
   * @return boolean
   */
  public function updateMViewTrialName($node_id, $name) {

    // Updates the trial name in a mview.
    $transaction = db_transaction();
    try {
      db_update($this->getMView())
        ->fields(array('dataset_name' => $name))
        ->condition('node_id', $node_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the parent names.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateParentName($details) {

    // Updates the parent names in a mview.
    $transaction = db_transaction();
    try {
      $name     = $details['name'];
      $stock_id = $details['stock_id'];

      // Updates the maternal.
      $stock_id_m = ($name) ? $stock_id : NULL;
      db_update($this->getMView())
        ->fields(array('maternal' => $name, 'stock_id_m' => $stock_id_m))
        ->condition('stock_id_m', $stock_id, '=')
        ->execute();

      // Updates the paternal.
      $stock_id_p = ($name) ? $stock_id : NULL;
      db_update($this->getMView())
        ->fields(array('paternal' => $name, 'stock_id_p' => $stock_id_p))
        ->condition('stock_id_p', $stock_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the data by key.
   *
   * @param string $key
   * @param integer $value
   *
   * @return boolean
   */
  public function deleteByKey($key, $value) {

    // Deletes the data.
    $transaction = db_transaction();
    try {

      // Deletes the data by key.
      db_delete($this->getMView())
        ->condition($key, $value)
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
  * Retrieves root_id
  *
  * @retrun integer
  */
  public function getRootID() {
    return $this->root_id;
  }

  /**
   * Sets the root_id.
   *
   * @param integer $root_id
   */
  public function setRootID($root_id) {
    $this->root_id = $root_id;
  }

  /**
   * Retrieves project_id
   *
   * @retrun integer
   */
  public function getProjectID() {
    return $this->project_id;
  }

  /**
   * Sets the project_id.
   *
   * @param integer $project_id
   */
  public function setProjectID($project_id) {
    $this->project_id = $project_id;
  }

  /**
   * Retrieves node_id
   *
   * @retrun integer
   */
  public function getNodeID() {
    return $this->node_id;
  }

  /**
   * Sets the node_id.
   *
   * @param integer $node_id
   */
  public function setNodeID($node_id) {
    $this->node_id = $node_id;
  }
}