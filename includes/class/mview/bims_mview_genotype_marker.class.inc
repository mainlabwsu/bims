<?php
/**
 * The declaration of BIMS_MVIEW_GENOTYPE_MARKER class.
 *
 */
class BIMS_MVIEW_GENOTYPE_MARKER extends BIMS_MVIEW_GENOTYPE {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_genotype_marker';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_mview = new BIMS_MVIEW_GENOTYPE_ACCESSION(array('node_id' => $node_id));
    if ($bims_mview) {
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for genotyping data for marker columns.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'root_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'dataset_name' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'feature_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'feature_uniquename' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'marker' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'genus' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'species' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'genome' => array(
          'type' => 'varchar',
          'length' => '600',
        ),
        'ss_id' => array(
          'type' => 'varchar',
          'length' => '255',
          ),
        'rs_id' => array(
          'type' => 'varchar',
          'length' => '255',
          ),
        'analysis_id' => array(
          'type' => 'int',
        ),
        'chromosome' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'chromosome_fid' => array(
          'type' => 'int',
        ),
        'loc_start' => array(
          'type' => 'int',
        ),
        'loc_stop' => array(
          'type' => 'int',
        ),
        'location' => array(
          'type' => 'text',
        ),
        'chado' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'unique keys' => array(),
      'indexes' => array(
        'mg_gm_node_id'           => array('node_id'),
        'mg_gm_project_id'        => array('project_id'),
        'mg_gm_feature_id'        => array('feature_id'),
        'mg_gm_marker'            => array('marker'),
      ),
    );
  }

  /**
   * Returns the marker.
   *
   * @param integer $feature_id
   *
   * @return object|NULL
   */
  public function getMarker($feature_id) {
    $m_gm = $this->getMView();
    $sql = "SELECT MV.* FROM {$m_gm} MV WHERE MV.feature_id = :feature_id";
    return db_query($sql, array(':feature_id' => $feature_id))->fetchObject();
  }

  /**
   * Returns the genomes.
   *
   * @return array
   */
  public function getGenomes() {

    // Gets BIMS_MVIEW_MARKER.
    $bm_marker  = new BIMS_MVIEW_MARKER(array('node_id' => $this->node_id));
    $m_marker   = $bm_marker->getMView();

    // Gets the genomes.
    $genomes = array();
    $m_gm = $this->getMView();
    $sql = "
      SELECT DISTINCT M.analysis_id, M.genome
      FROM $m_gm GM
        INNER JOIN $m_marker M on M.feature_id = GM.feature_id
      WHERE M.analysis_id IS NOT NULL
      ORDER BY M.genome
    ";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $genomes[$obj->analysis_id] = $obj->genome;
    }
    return $genomes;
  }

  /**
  * Returns the chromosomes.
  *
  * @param integer $analysis_id
  *
  * @return array
  */
  public function getChromosomes($analysis_id = NULL) {

    // Gets BIMS_MVIEW_MARKER.
    $bm_marker  = new BIMS_MVIEW_MARKER(array('node_id' => $this->node_id));
    $m_marker   = $bm_marker->getMView();

    $chromosomes = array();
    if ($analysis_id) {
      $m_gm = $this->getMView();
      $sql = "SELECT DISTINCT MV.feature_id_c, MV.chromosome FROM $m_gm MV";
      $results = db_query($sql);
      while ($obj = $results->fetchObject()) {
        $chromosomes[$obj->feature_id_c] = $obj->chromosome;
      }
    }
    return $chromosomes;
  }

  /**
    * Updates the marker.
    *
    * @param array $details
    *
    * @return boolean
    */
  public function updateMarker($details) {

    // Updates the marker properties.
    $transaction = db_transaction();
    try {

      // Sets the properties.
      $fields = array(
        'feature_uniquename'  => $details['uniquename'],
        'marker'              => $details['name'],
        'genus'               => $details['genus'],
        'species'             => $details['species'],
        'analysis_id'         => $details['analysis_id'],
        'genome'              => $details['genome'],
        'ss_id'               => $details['ss_id'],
        'rs_id'               => $details['rs_id'],
        'chromosome_fid'      => $details['chromosome_fid'],
        'chromosome'          => $details['chromosome'],
        'loc_start'           => $details['loc_start'],
        'loc_stop'            => $details['loc_stop'],
        'location'            => $details['location'],
      );

      // Updates the marker.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('feature_id', $details['feature_id'], '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Clears the data of the provided trial ID in the mview.
   *
   * @param integer $trial_id
   *
   * @return boolean
   */
  public function clearTrial($trial_id) {

    // Clears the data in the mview.
    if ($this->exists()) {
      db_delete($this->getMView())
        ->condition('node_id', $trial_id, '=')
        ->execute();
    }

    // Creates the mview if not exist.
    else {
      $this->createMView();
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE, $mview_option = '') {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids, TRUE);
      if (empty($grouped_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'project_id', $grouped_ids, TRUE);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Updates the mview.
      if (!$this->_updateMView($bims_program, $grouped_ids)) {
        throw new Exception("Error : Failed to update the mview data");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumns(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_gm       = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_feature = new BIMS_CHADO_FEATURE($program_id);
    $table_fp   = $bc_feature->getTableName('featureprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';

    // Adds marker custom properties columns.
    $marker_props = $bims_program->getProperties('marker', 'custom', BIMS_OPTION);
    foreach ((array)$marker_props as $cvterm_id => $name) {
      $field_name = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_gm, $field_name)) {
        db_add_field($m_gm, $field_name, array('type' => 'text'));
      }

      // Updates the select / join.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";
      $join_str .= "
        LEFT JOIN (
          SELECT FP.feature_id, FP.value
          FROM {$table_fp} FP
          WHERE FP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.feature_id = G.feature_id
      ";
    }
    return array(
      'error'        => FALSE,
      'select_str'   => $select_str,
      'join_str'     => $join_str,
      'marker_props' => $marker_props,
    );
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMView(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumns($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets BIMS_MVIEW tables.
      $bm_genotype  = new BIMS_MVIEW_GENOTYPE(array('node_id' => $this->node_id));
      $bm_marker    = new BIMS_MVIEW_MARKER(array('node_id' => $this->node_id));
      $m_genotype   = $bm_genotype->getMView();
      $m_marker     = $bm_marker->getMView();

      // Populates the mview.
      $id_list = implode(',', $grouped_ids);
      $sql = "
        SELECT DISTINCT G.project_id, G.dataset_name, G.feature_id,
          M.uniquename AS feature_uniquename, M.name AS marker,
          M.genus, M.species, M.ss_id, M.rs_id, M.genome,
          M.analysis_id, M.chromosome_fid, M.chromosome, M.loc_start,
          M.loc_stop, M.location, M.chado, N.root_id, N.node_id
          $select_str
        FROM {$m_genotype} G
          INNER JOIN {$m_marker} M on M.feature_id = G.feature_id
          INNER JOIN {bims_node} N on N.project_id = G.project_id
          $join_str
        WHERE G.project_id IN ($id_list)
      ";
      $results = db_query($sql, array(':project_id' => $project_id));
      while ($arr = $results->fetchAssoc()) {

        // Inserts into the mview.
        db_insert($this->getMView())
          ->fields($arr)
          ->execute();
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }
}
