<?php
/**
 * The declaration of BIMS_MVIEW_HAPLOTYPE_BLOCK class.
 *
 */
class BIMS_MVIEW_HAPLOTYPE_BLOCK extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_haplotype_block';
  }

  /**
   * @see BIMS_MVIEW::getMViewByNodeID()
   */
  public static function getMViewByNodeID($node_id) {
    $bims_mview = new BIMS_MVIEW_HAPLOTYPE_BLOCK(array('node_id' => $node_id));
    if ($bims_mview) {
      return $bims_mview->getMView();
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for haplotype block data.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'root_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'feature_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'chado_fid' => array(
          'type' => 'int',
        ),
        'uniquename' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'organism_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'genus' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'species' => array(
          'type'     => 'varchar',
          'length'   => '500',
          'not null' => TRUE,
        ),
        'analysis_id' => array(
          'type' => 'int',
        ),
        'genome' => array(
          'type' => 'varchar',
          'length' => '600',
        ),
        'chromosome_fid' => array(
          'type' => 'int',
        ),
        'chromosome' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'loc_start' => array(
          'type' => 'int',
        ),
        'loc_stop' => array(
          'type' => 'int',
        ),
        'strand' => array(
          'type' => 'varchar',
          'length' => '10',
        ),
        'trait_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'trait' => array(
          'type' => 'varchar',
          'length' => '500',
          'not null' => TRUE,
        ),
        'chado' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('feature_id')
      ),
      'indexes' => array(
        'mhb_name' => array('name'),
      ),
    );
  }

  /**
   * Adds a new haplotype block.
   *
   * @param array
   *
   * @return boolean
   */
  public function addHaplotypeBlock($details) {

    // Add a haplotype block.
    $m_hb     = $this->getMView();
    $node_id  = $this->getNodeID();
    $cols     = 'node_id';
    $vals     = $node_id;
    foreach ($details as $key => $val) {
      $cols .= ", $key";
      $vals .= ", '$val'";
    }
    $sql = "INSERT INTO $m_hb ($cols) VALUES ($vals)";
    db_query($sql);
    return TRUE;
  }

  /**
   * Returns a haploptye block object.
   *
   * @param integer $feature_id
   * @param integer $flag
   *
   * @return object|array
   */
  public function getHaplotypeBlock($feature_id, $flag = BIMS_OBJECT) {
    if ($feature_id) {
      $m_hb = $this->getMView();
      $sql = "SELECT MV.* FROM $m_hb MV WHERE MV.feature_id = :feature_id";
      $results = db_query($sql, array(':feature_id' => $feature_id));
      if ($flag == BIMS_ASSOC) {
        return $results->fetchAssoc();
      }
      return $results->fetchObject();
    }
    return NULL;
  }

  /**
   * Returns haploptye blocks.
   *
   * @param integer $flag
   * @param string $condition
   * @param string $value
   *
   * @return array
   */
  public function getHaplotypeBlocks($flag = BIMS_OPTION, $condition = 'ALL', $value = '') {

    // Gets the haplotye blocks.
    $haplotype_blocks = array();
    $m_hb = $this->getMView();
    if (db_table_exists($m_hb)) {

      // Gets all haplotype blocks.
      $sql = "SELECT MV.* FROM $m_hb MV ORDER BY MV.name";
      $results = db_query($sql);
      while ($obj = $results->fetchObject()) {
        if ($flag == BIMS_OPTION) {
          $haplotype_blocks[$obj->feature_id] = $obj->name;
        }
        else if ($flag == BIMS_OBJECT) {
          $haplotype_blocks []= $obj;
        }
      }
    }
    return $haplotype_blocks;
  }

  /**
   * @see BIMS_MVIEW::deleteByKey()
   */
  public function deleteByKey($key, $value) {

    // Deletes the data.
    $transaction = db_transaction();
    try {

      // Deletes the data by key.
      db_delete($this->getMView())
      ->condition($key, $value)
      ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Clears the data of the provided trial ID in the mview.
   *
   * @param integer $trial_id
   *
   * @return boolean
   */
  public function clearTrial($trial_id) {

    // Clears the data in the mview.
    if ($this->exists()) {
      db_delete($this->getMView())
        ->condition('node_id', $trial_id, '=')
        ->execute();
    }

    // Creates the mview if not exist.
    else {
      $this->createMView();
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($grouped_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'nd_geolocation_id', $grouped_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Finds new trials and adds them to the program if exist.
      $bims_program->updateTrials();

      // Updates the mviews by schema.
      if (!$this->_updateMViewByChado($bims_program, $grouped_ids['chado'])) {
        throw new Exception("Error : Failed to update the mview data in Chado");
      }
      if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['bims'])) {
        throw new Exception("Error : Failed to update the mview data in BIMS");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByChado(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsChado($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'haplotype_block' => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID(),
        'chromosome'      => MCL_CHADO_CVTERM::getCvterm('sequence', 'chromosome')->getCvtermID(),
      );

      // Populates the mview.
      foreach($grouped_ids as $feature_id) {
        $sql = "
          SET SEARCH_PATH = chado, public, bims;
          SELECT DISTINCT HB.feature_id, HB.uniquename, HB.uniquename AS name,
            HB.type_id, C.name AS type, O.organism_id, O.genus, O.species,
            TRAIT.cvterm_id AS trait_id, TRAIT.name AS trait,
            FLOC.analysis_id, FLOC.genome, HB.feature_id AS chado_fid,
            FLOC.chromosome_fid, FLOC.chromosome, FLOC.loc_start, FLOC.loc_stop,
            FLOC.strand $select_str
          FROM {chado.feature} HB
            $join_str
            INNER JOIN chado.organism O on O.organism_id = HB.organism_id
            INNER JOIN chado.cvterm C on C.cvterm_id = HB.type_id
            INNER JOIN chado.feature_cvterm FC on FC.feature_id = HB.feature_id
            INNER JOIN chado.cvterm TRAIT on TRAIT.cvterm_id = FC.cvterm_id
            LEFT JOIN (
              SELECT FL.feature_id, AF.analysis_id, A.name AS genome,
                CHR.feature_id AS chromosome_fid, CHR.uniquename AS chromosome,
                FL.fmin AS loc_start, FL.fmax AS loc_stop, FL.strand
              FROM {chado.featureloc} FL
                LEFT JOIN {chado.feature} CHR on CHR.feature_id = FL.srcfeature_id
                LEFT JOIN chado.analysisfeature AF on AF.feature_id = CHR.feature_id
                LEFT JOIN chado.analysis A on A.analysis_id = AF.analysis_id
              WHERE CHR.type_id = :chromosome
            ) FLOC on FLOC.feature_id = HB.feature_id
          WHERE HB.feature_id = :feature_id AND HB.type_id = :haplotype_block
          ORDER BY HB.uniquename
        ";
        $args = array(
          ':haplotype_block'  => $cvterms['haplotype_block'],
          ':chromosome'       => $cvterms['chromosome'],
          ':feature_id'       => $feature_id,
        );
        $results = db_query($sql, $args);
        $dup_feature = array();
        while ($arr = $results->fetchAssoc()) {

          // Checks for a duplication.
          if (array_key_exists($arr['feature_id'], $dup_feature)) {
            continue;
          }
          else {
            $dup_feature[$arr['feature_id']] = TRUE;
            $features[$arr['feature_id']] = TRUE;
          }

          // Adds node_id.
          $arr['node_id'] = $this->node_id;
          $arr['root_id'] = $this->node_id;
          $arr['chado']   = 1;

          // Inserts into mview.
          db_insert($this->getMView())
            ->fields($arr)
            ->execute();
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByBIMS(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Local variables.
      $program_id = $bims_program->getProgramID();

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumnsBIMS($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets BIMS_CHADO tables.
      $bc_feature           = new BIMS_CHADO_FEATURE($this->node_id);
      $table_feature        = $bc_feature->getTableName('feature');
      $table_featureprop    = $bc_feature->getTableName('featureprop');
      $table_feature_cvterm = $bc_feature->getTableName('feature_cvterm');
      $table_featureloc     = $bc_feature->getTableName('featureloc');

      // Gets the cvterms.
      $cvterms = array(
        'haplotype_block' => MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID(),
        'chromosome'      => MCL_CHADO_CVTERM::getCvterm('sequence', 'chromosome')->getCvtermID(),
      );

      // Populates the mview.
      foreach ($grouped_ids as $project_id) {
        $trial = BIMS_TRIAL::byProjectID($project_id, $this->node_id);
        $sql = "
          SELECT DISTINCT HAPLOTYPE_BLOCK.feature_id, HAPLOTYPE_BLOCK.uniquename,
            HAPLOTYPE_BLOCK.name, O.organism_id, O.genus, O.species,
            HAPLOTYPE_BLOCK.type_id, C.name AS type,
            CHROMOSOME.feature_id AS chromosome_fid, CHROMOSOME.uniquename AS chromosome,
            FLOC.fmax AS loc_start, FLOC.fmin AS loc_stop, FLOC.strand, TRAIT.name AS trait,
            TRAIT.cvterm_id AS trait_id
            $select_str
          FROM {$table_feature} HAPLOTYPE_BLOCK
            INNER JOIN {chado.cvterm} C on C.cvterm_id = HAPLOTYPE_BLOCK.type_id
            $join_str
            INNER JOIN {chado.organism} O on O.organism_id = HAPLOTYPE_BLOCK.organism_id
            INNER JOIN {$table_feature_cvterm} FC on FC.feature_id = HAPLOTYPE_BLOCK.feature_id
            INNER JOIN {chado.cvterm} TRAIT on TRAIT.cvterm_id = FC.cvterm_id
            INNER JOIN {$table_featureloc} FLOC on FLOC.feature_id = HAPLOTYPE_BLOCK.feature_id
            INNER JOIN {chado.feature} CHROMOSOME on CHROMOSOME.feature_id = FLOC.srcfeature_id
          WHERE HAPLOTYPE_BLOCK.type_id = :type_id_haplotype_block
            AND CHROMOSOME.type_id = :type_id_chromosome
        ";
        $args = array(
          ':type_id_haplotype_block'  => $cvterms['haplotype_block'],
          ':type_id_chromosome'       => $cvterms['chromosome'],
        );

        // Counts the number of data.
        $sql_count  = "SELECT COUNT(A.*) FROM ($sql) A";
        $total_data = db_query($sql_count, $args)->fetchField();

        // Adds the data.
        $num_data = 0;
        if ($total_data) {
          $results = db_query($sql, $args);
          while ($arr = $results->fetchAssoc()) {
            $num_data++;

            // Shows the progress.
            if ($num_data % 5000 == 0) {
              $msg = sprintf("%d / %d = %.03f %%", $num_hb, $total_hb, $num_hb / $total_hb * 100.0);
              bims_print($msg);
            }

            // Initializes the fields.
            $fields = array();

            // Adds node ID.
            $fields['node_id'] = $this->node_id;
            $fields['root_id'] = $this->node_id;
            foreach ($arr as $key => $val) {

              // Skips if no value.
              if (!($val || $val == '0')) {
                continue;
              }

              // Adds the value.
              $fields[$key] = $val;
            }

            // Inserts into the mview.
            db_insert($this->getMView())
              ->fields($arr)
              ->execute();
          }
        }
      }

      // Stores the number of data in BIMS_TRIAL prop.
      $trial->setPropByKey('num_data', $num_data);
      if (!$trial->update()) {
        throw new Exception('Error : Failed to update trial properties');
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

    /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsChado(BIMS_PROGRAM $bims_program) {
    return array(
      'error'       => FALSE,
      'select_str'  => '',
      'join_str'    => '',
    );
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumnsBIMS(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_hb       = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_feature = new BIMS_CHADO_FEATURE($program_id);
    $table_fp   = $bc_feature->getTableName('featureprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';

    // Returns if no BIMS CHADO.
    if (!db_table_exists($table_fp)) {
      return array(
        'error'       => FALSE,
        'select_str'  => $select_str,
        'join_str'    => $join_str,
      );
    }

    // Adds haplotype block custom properties columns.
    $accession_props  = $bims_program->getProperties('haplotype_block', 'custom', BIMS_OPTION);
    foreach ((array)$accession_props as $cvterm_id => $name) {
      $field_name = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_hb, $field_name)) {
        db_add_field($m_hb, $field_name, array('type' => 'text'));
      }

      // Updates the select statement.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";

      // Updates the join statement.
      $join_str .= "
        LEFT JOIN (
          SELECT FP.feature_id, FP.value
          FROM {$table_fp} FP
          WHERE FP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.feature_id = HAPLOTYPE_BLOCK.feature_id
      ";
    }
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Local variables.
    $bc_feature         = new BIMS_CHADO_FEATURE($this->node_id);
    $table_feature      = $bc_feature->getTableName('feature');
    $id_haplotype_block = MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID();


    // Initializes the returning array.
    $grouped_ids = array('chado' => array(), 'bims' => array());

    // GROUP : private.
    if ($group == 'private') {
      if (db_table_exists($table_feature)) {
        $sql = "SELECT feature_id FROM {$table_feature}";
        $results = db_query($sql);
        while ($feature_id = $results->fetchField()) {
          $grouped_ids['bims'][$feature_id] = TRUE;
        }
      }
    }

    // GROUP : public.
    else if ($group == 'public') {
      $imported_projs = BIMS_IMPORTED_PROJECT::getImportedProjByType($this->node_id, 'haplotype');
      //....
    }

    // GROUP : project.
    else if ($group == 'project') {
      $this->getFeatureIDByProject($id_haplotype_block, $group_ids, $grouped_ids);
    }

    // GROUP : feature.
    else if ($group == 'feature') {
      $id_list = implode(",", $group_ids);

      // BIMS.
      if (db_table_exists($table_feature)) {
        $sql = "
          SELECT feature_id FROM {$table_feature}
          WHERE type_id = :type_id AND feature_id IN ($id_list)
        ";
        $results = db_query($sql, array(':type_id' => $id_haplotype_block));
        while ($feature_id = $results->fetchField()) {
          $grouped_ids['bims'][$feature_id] = TRUE;
        }
      }

      // Chado.
      $sql = "
        SELECT feature_id FROM {chado.feature}
        WHERE feature_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($feature_id = $results->fetchField()) {
        $grouped_ids['chado'][$feature_id] = TRUE;
      }
    }

    // GROUP : program (private + public).
    else {

      // Adds private feature.
      if (db_table_exists($table_feature)) {
        $sql = "SELECT feature_id FROM {$table_feature}";
        $results = db_query($sql);
        while ($feature_id = $results->fetchField()) {
          $grouped_ids['bims'][$feature_id] = TRUE;
        }
      }

      // Adds public feature.
      $imported_projs = BIMS_IMPORTED_PROJECT::getImportedProjByType($this->node_id, 'genotype');
      //....


    }

    // Returns the grouped IDs.
    $ret_array = array('bims' => array(), 'chado' => array());
    if (empty($grouped_ids['chado']) && empty($grouped_ids['bims'])) {
      return array();
    }
    if (!empty($grouped_ids['bims'])) {
      $ret_array['bims'] = array_keys($grouped_ids['bims']);
    }
    if (!empty($grouped_ids['chado'])) {
      $ret_array['chado'] = array_keys($grouped_ids['chado']);
    }
    return $ret_array;
  }
}
