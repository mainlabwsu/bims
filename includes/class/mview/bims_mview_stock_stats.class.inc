<?php
/**
 * The declaration of BIMS_MVIEW_STOCK_STATS class.
 *
 */
class BIMS_MVIEW_STOCK_STATS extends PUBLIC_BIMS_MVIEW_STOCK_STATS {

  /**
   * Class data members.
   */
  protected $stats_arr = NULL;

  /**
   * @see PUBLIC_BIMS_MVIEW_STOCK_STATS::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates stats array ($this->stats_arr).
    if ($this->stats == '') {
      $this->stats_arr = array();
    }
    else {
      $this->stats_arr = json_decode($this->stats, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_STOCK_STATS::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_STOCK_STATS::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_STOCK_STATS::insert()
   */
  public function insert() {

    // Updates the parent:$stats fields.
    $this->stats = json_encode($this->stats_arr);

    // Inserts a new dataset stats.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_STOCK_STATS::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$stats fields.
    $this->stats = json_encode($this->stats_arr);

    // Updates the dataset stats.
    return parent::update($new_values);
  }

  /**
   * Clears the stats.
   *
   * @param array $stock_ids
   *
   * @return boolean
   */
  public function clear($stock_ids = array()) {
    $sql = 'DELETE FROM {bims_mview_stock_stats} WHERE node_id = :node_id ';
    if (!empty($stock_ids)) {
      $sql .= ' AND stock_id IN (' . implode(',', $stock_ids) . ')';
    }
    db_query($sql, array(':node_id' => $this->node_id));
  }

  /**
   * Updates the statistics.
   *
   * @param integer $group
   * @param array $group_ids
   * @param boolean $update_num
   *
   * @return boolean
   */
  public function updateStats($group, $group_ids = array(), $update_num = TRUE) {

    // Updates the stats.
    $transaction = db_transaction();
    try {

      // Gets the stock IDs by the group.
      $stock_ids = $this->getIDByGroup($bm_phenotype, $bm_stock, $group, $group_ids);
      if (empty($stock_ids)) {
        return TRUE;
      }

      // Clears the stats.
      $this->clear($stock_ids);

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Calculates the stats.
      if (!$this->_calcStats($bims_program, $stock_ids)) {
        throw new Exception("Error : Failed to caluctate the accession stats");
      }

      // Updates the numbers.
      if ($update_num) {
        $bm_stock = new BIMS_MVIEW_STOCK(array('node_id' => $this->node_id));

        // Updates breeding numbers.
        if (!$bm_stock->updateNumbers('all', $stock_ids)) {
          throw new Exception("Error : Failed to update the breeding in BIMS_MVIEW_STOCK");
        }

        // Updates the locations.
        if (!$bm_stock->updateLocations($stock_ids)) {
          throw new Exception("Error : Failed to update the locations in BIMS_MVIEW_STOCK");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Calculates the statistics.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param BIMS_MVIEW_PHENOTYPE $bm_phenotype
   * @param array $stock_ids
   *
   * @return boolean
   */
  private function _calcStats(BIMS_PROGRAM $bims_program, $stock_ids) {

    // Local variables.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
    $bm_stock     = new BIMS_MVIEW_STOCK(array('node_id' => $this->node_id));
    $bims_stats   = new BIMS_STATS();

    // Calculates the stats.
    $transaction = db_transaction();
    try {

      $total_stocks = sizeof($stock_ids);
      $num_stocks   = 0;
      $blocks       = intval($total_stocks / 20);
      foreach ($stock_ids as $stock_id) {

        // Shows the progress.
        $num_stocks++;
        if ($blocks != 0 && ($num_stocks % $blocks == 0)) {
          $msg = sprintf("%d / %d = %.03f %%", $num_stocks, $total_stocks, $num_stocks / $total_stocks * 100.0);
          bims_print($msg);
        }

        // Calculates the stats for each active descriptor.
        $params = array(
          'flag'  => BIMS_CLASS,
          'type'  => 'accession',
          'id'    => $stock_id,
          'stats' => TRUE,
        );
        $descriptors = $bims_program->getActiveDescriptors($params);
        foreach ((array)$descriptors as $descriptor) {
          $cvterm_id  = $descriptor->getCvtermID();
          $name       = $descriptor->getName();
          $format     = $descriptor->getFormat();

          // Prepares the calculation of the stats for the descriptor.
          $param = array(
            'from'      => $bm_phenotype->getMView(),
            'args'      => array(':stock_id' => $stock_id),
            'condition' => ' stock_id = :stock_id ',
            'name'      => $name,
            'field'     => "t$cvterm_id",
            'format'    => $format,
          );

          // Calculates the stats.
          $stats_arr = $bims_stats->calcStats($param);

          // Gets the number of the data.
          $num = (empty($stats_arr)) ? 0 : $stats_arr['num'];

          // Checks the condition.
          if (!($num == 0 && !$flag_add)) {

            // Adds the stats.
            if (!empty($stats_arr)) {;
              $details = array(
                'node_id'   => $this->node_id,
                'stock_id'  => $stock_id,
                'cvterm_id' => $cvterm_id,
                'name'      => $name,
                'num_data'  => $stats_arr['num'],
                'stats'     => json_encode($stats_arr),
              );
              $bm_stock_stats = new BIMS_MVIEW_STOCK_STATS($details);
              if (!$bm_stock_stats->insert()) {
                throw new Exception("Error : Failed to insert the stats for $cvterm_id.");
              }
            }

            // Adds a trait column if not exists.
            $m_stock = $bm_stock->getMView();
            $field = "t$cvterm_id";
            if (!db_field_exists($m_stock, $field)) {
              db_add_field($m_stock, $field, array('type' => 'text'));
            }

            // Updates trait column of bims_mview_stock table.
            if ($stats_arr['num']) {
              db_update($m_stock)
                ->fields(array($field => $stats_arr['mean']))
                ->condition('stock_id', $stock_id, '=')
                ->execute();
            }
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the statistics for the accession. Do not add the no data stats
   * if $flag_add is TRUE.
   *
   * @param integer $cvterm_id
   * @param boolean $flag_add
   *
   * @return boolean
   */
  public function updateMView($target_cvterm_id = NULL, $flag_add = TRUE) {

    // Gets BIMS_PROGRAM.
    $bims_program = BIMS_PROGRAM::byID($this->node_id);

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Clears the mview.
      $this->clear();

      // Gets BIMS_MVIEW_PHENOTYPE.
      $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));

      // Gets BIMS_STATS.
      $bims_stats = new BIMS_STATS();

      // Gets BIMS_STOCK.
      $bm_stock = new BIMS_MVIEW_STOCK(array('node_id' => $this->node_id));

      // Gets all accessions.
      $stocks       = $bm_stock->getStocks(BIMS_OBJECT);
      $total_stocks = sizeof($stocks);
      $num_stocks   = 0;
      $blocks       = intval($total_stocks/20);
      foreach ($stocks as $stock_obj) {
        $stock_id = $stock_obj->stock_id;

        // Shows the progress.
        $num_stocks++;
        if ($blocks != 0 && ($num_stocks % $blocks == 0)) {
          $msg = sprintf("%d / %d = %.03f %%", $num_stocks, $total_stocks, $num_stocks / $total_stocks * 100.0);
          bims_print($msg);
        }

        // Calculates the stats for each active descriptor.
        $descriptors = array();
        if ($target_cvterm_id) {
          $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($this->node_id, $target_cvterm_id);
          if ($descriptor->canStats()) {
            $descriptors []= $descriptor;
          }
        }
        else {
          $params = array(
            'flag'  => BIMS_CLASS,
            'type'  => 'accession',
            'id'    => $stock_id,
            'stats' => TRUE,
          );
          $descriptors = $bims_program->getActiveDescriptors($params);
        }
        foreach ((array)$descriptors as $descriptor) {
          $cvterm_id  = $descriptor->getCvtermID();
          $name       = $descriptor->getName();
          $format     = $descriptor->getFormat();

          // Prepares the calculation of the stats for the descriptor.
          $param = array(
            'from'      => $bm_phenotype->getMView(),
            'args'      => array(':stock_id' => $stock_id),
            'condition' => ' stock_id = :stock_id ',
            'name'      => $name,
            'field'     => "t$cvterm_id",
            'format'    => $format,
          );

          // Calculates the stats.
          $stats_arr = $bims_stats->calcStats($param);

          // Gets the number of the data.
          $num = (empty($stats_arr)) ? 0 : $stats_arr['num'];

          // Checks the condition.
          if (!($num == 0 && !$flag_add)) {

            // Adds the stats.
            if (!empty($stats_arr)) {;
              $details = array(
                'node_id'   => $stock_obj->node_id,
                'stock_id'  => $stock_id,
                'cvterm_id' => $cvterm_id,
                'name'      => $name,
                'num_data'  => $stats_arr['num'],
                'stats'     => json_encode($stats_arr),
              );
              $bm_stock_stats = new BIMS_MVIEW_STOCK_STATS($details);
              if (!$bm_stock_stats->insert()) {
                throw new Exception("Error : Failed to insert the stats for $cvterm_id");
              }
            }

            // Adds a trait column if not exists.
            $m_stock = $bm_stock->getMView();
            $field = "t$cvterm_id";
            if (!db_field_exists($m_stock, $field)) {
              db_add_field($m_stock, $field, array('type' => 'text'));
            }

            // Updates trait column of bims_mview_stock table.
            if ($stats_arr['num']) {
              db_update($m_stock)
                ->fields(array($field => $stats_arr['mean']))
                ->condition('stock_id', $stock_id, '=')
                ->execute();
            }
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the statistical descriptors.
   *
   * @param integer $stock_id
   * @param integer $flag
   *
   * @return array
   */
  public static function getStatDescriptors($stock_id, $flag = BIMS_OPTION) {
    $sql = "
      SELECT S.node_id, S.cvterm_id, S.name, S.num_data
      FROM {bims_mview_stock_stats} S
      WHERE S.stock_id = :stock_id
      ORDER BY S.name
    ";
    $results = db_query($sql, array(':stock_id' => $stock_id));
    $descriptors = array();
    while ($obj = $results->fetchObject()) {
      if ($obj->num_data) {
        if ($flag == BIMS_CLASS) {
          $descriptors []= BIMS_MVIEW_DESCRIPTOR::byID($obj->node_id, $obj->cvterm_id);
        }
        else if ($flag == BIMS_OBJECT) {
          $descriptors []= $obj;
        }
        else {
          $descriptors[$obj->cvterm_id] = $obj->name;
        }
      }
    }
    return $descriptors;
  }

  /**
   * Returns the statistical information of the given trait.
   *
   * @param integer $node_id
   * @param integer $stock_id
   * @param integer $cvterm_id
   *
   * @return array
   */
  public static function getStatsByID($node_id, $stock_id, $cvterm_id) {

    // Gets BIMS_MVIEW_PHENOTYPE_STATS and the stats array.
    $args = array(
      'node_id'   => $node_id,
      'stock_id'  => $stock_id,
      'cvterm_id' => $cvterm_id,
    );
    $bm_stock_stats = BIMS_MVIEW_STOCK_STATS::byKey($args);
    if ($bm_stock_stats) {
      return $bm_stock_stats->getStatsArr();
    }
    return array();
  }

  /**
   * Returns the statistical information for the provided cvterm.
   *
   * @param integer $cvterm_id
   * @param integer $node_id
   * @param various $variable
   * @param array $ret_stats
   *
   * @return string
   */
  public static function getStatsTable($cvterm_id, $node_id, $variable, &$ret_stats = array()) {

    // If $variable is an array.
    if (is_array($variable)) {

      // The variable is filter.
      if (array_key_exists('preference', $variable)) {
        $ret_stats = self::_getStatsTable($cvterm_id, $node_id, 'filter', $variable);
      }

      // The variable is a list of stock IDs.
      else {
        $ret_stats = self::_getStatsTable($cvterm_id, $node_id, 'list', $variable);
      }
    }

    // The variable is a stock IDs.
    else if (preg_match("/^\d+$/", $variable)) {
      $args = array(
        'cvterm_id' => $cvterm_id,
        'node_id'   => $node_id,
        'stock_id'  => $variable,
      );
      $bm_stock_stats = BIMS_MVIEW_STOCK_STATS::byKey($args);
      if ($bm_stock_stats) {
        $ret_stats = $bm_stock_stats->getStatsArr();
      }
    }

    // Returns the stats table.
    if (empty($ret_stats)) {
      return "<div style='margin:14px;'><em>No statistical information about this trait was found.</em></div>";
    }

    // Returns the stats table.
    return BIMS_STATS::getStatsTable($node_id, $ret_stats, $cvterm_id);
  }

  /**
   * Helper function for getStatsTable.
   *
   * @param integer $cvterm_id
   * @param integer $node_id
   * @param string $type
   * @param various $variable
   *
   * @retrun array
   */
  private static function _getStatsTable($cvterm_id, $node_id, $type, $variable) {

    // Local varibles.
    $bims_stats   = new BIMS_STATS();
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $node_id));
    $descriptor   = BIMS_MVIEW_DESCRIPTOR::byID($bm_phenotype->getRootID(), $cvterm_id);

    // The type is a list of stock_id.
    if ($type == 'list') {

      // Calucalates the stats for the list of stocks or samples.
      if (!empty($variable)) {
        $stock_id_list = implode(',', $variable);

        // Finds the list contains stocks or samples and sets the parameters.
        $stock_type = $bm_phenotype->isSample($variable[0]) ? 'sample_id' : 'stock_id';
        $param = array(
          'condition' => " $stock_type IN ($stock_id_list)",
          'args'      => array(),
          'from'      => $bm_phenotype->getMView(),
          'name'      => $descriptor->getName(),
          'field'     => "t$cvterm_id",
          'format'    => $descriptor->getFormat(),
        );
        return $bims_stats->calcStats($param);
      }
    }

    // The type is a filter.
    else if ($type == 'filter') {

      // Calucalates the stats for the give list of stocks or samples.
      $key        = $variable['key'];
      $sql        = $variable['sql'];
      $args       = $variable['args'];
      $from       = $variable['from'];
      $condition  = " $key IN (SELECT Z.$key FROM ($sql) Z)";
      $param = array(
        'condition' => $condition,
        'sql'       => $sql,
        'args'      => $args,
        'from'      => $from,
        'name'      => $descriptor->getName(),
        'field'     => "t$cvterm_id",
        'format'    => $descriptor->getFormat(),
      );
      return $bims_stats->calcStats($param);
    }
    return array();
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the stats array.
   *
   * @return array
   */
  public function getStatsArr() {
    return $this->stats_arr;
  }

  /**
   * Sets the stats array.
   *
   * @param array $stats
   */
  public function setStatsArr($stats) {
    $this->stats_arr = $stats;
  }

  /**
   * Returns the value of the given key in stats.
   */
  public function getStatsByKey($key) {
    if (array_key_exists($key, $this->stats_arr)) {
      return $this->stats_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in stats.
   *
   * @param string $key
   * @param string $value
   */
  public function setStatsByKey($key, $value) {
    $this->stats_arr[$key] = $value;
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Local variables.
    $bm_stock     = new BIMS_MVIEW_STOCK(array('node_id' => $this->node_id));
    $bm_cross     = new BIMS_MVIEW_CROSS(array('node_id' => $this->node_id));
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
    $m_stock      = $bm_stock->getMView();
    $m_cross      = $bm_cross->getMView();
    $m_phenotype  = $bm_phenotype->getMView();

    // Initializes the returning array.
    $stock_ids = array();

    // GROUP : private.
    if ($group == 'private') {
      $sql = "SELECT DISTINCT stock_id FROM {$m_stock} WHERE chado = :chado";
      $results = db_query($sql, array(':chado' => 0));
      while ($stock_id = $results->fetchField()) {
        $stock_ids[$stock_id] = TRUE;
      }
    }

      // GROUP : public.
    else if ($group == 'public') {
      $sql = "SELECT DISTINCT stock_id FROM {$m_stock} WHERE chado = :chado";
      $results = db_query($sql, array(':chado' => 1));
      while ($stock_id = $results->fetchField()) {
        $stock_ids[$stock_id] = TRUE;
      }
    }

    // GROUP : node.
    else if ($group == 'node') {
      $sql = "SELECT DISTINCT stock_id FROM {$m_phenotype} WHERE 1=1 ";
      $sql .= ' AND node_id IN (' . implode(',', $group_ids) . ')';
      $results = db_query($sql);
      while ($stock_id = $results->fetchField()) {
        $stock_ids[$stock_id] = TRUE;
      }
    }

    // GROUP : project.
    else if ($group == 'project') {
      $sql = "SELECT DISTINCT stock_id FROM {$m_phenotype} WHERE 1=1 ";
      $sql .= ' AND project_id IN (' . implode(',', $group_ids) . ')';
      $results = db_query($sql);
      while ($stock_id = $results->fetchField()) {
        $stock_ids[$stock_id] = TRUE;
      }
    }

    // GROUP : stock.
    else if ($group == 'stock') {
      $sql = "SELECT DISTINCT stock_id FROM {$m_stock} WHERE 1=1 ";
      $sql .= ' AND stock_id IN (' . implode(',', $group_ids) . ')';
      $results = db_query($sql);
      while ($stock_id = $results->fetchField()) {
        $stock_ids[$stock_id] = TRUE;
      }
    }

      // GROUP : sample.
    else if ($group == 'sample') {
      $sql = "SELECT DISTINCT node_id FROM {$m_phenotype} WHERE 1=1 ";
      $sql .= ' AND sample_id IN (' . implode(',', $group_ids) . ')';
      $results = db_query($sql);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

    // GROUP : cross.
    else if ($group == 'cross') {
      $sql = "SELECT DISTINCT progeny FROM {$m_cross} WHERE 1=1 ";
      $sql .= ' AND nd_experiment_id IN (' . implode(',', $group_ids) . ')';
      $results = db_query($sql);
      $progeny_arr = array();
      while ($progeny = $results->fetchField()) {
        $tmp = json_decode($progeny, TRUE);
        if (!empty($tmp)) {
          foreach ($tmp as $id => $name) {
            $stock_ids[$id] = TRUE;
          }
        }
      }
    }

    // GROUP : cvterm.
    else if ($group == 'cvterm') {
      foreach ($group_ids as $cvterm_id) {
        $field = "t$cvterm_id";
        if (!db_field_exists($m_phenotype, $field)) {
          $sql = "
            SELECT DISTINCT stock_id FROM {$m_phenotype}
            WHERE $field != '' AND
          ";
          $results = db_query($sql);
          while ($stock_id = $results->fetchField()) {
            $stock_ids[$stock_id] = TRUE;
          }
        }
      }
    }

    // GROUP : program (private + public).
    else {
      $sql = "SELECT DISTINCT stock_id FROM {$m_stock}";
      $results = db_query($sql);
      while ($stock_id = $results->fetchField()) {
        $stock_ids[$stock_id] = TRUE;
      }
    }
    return array_keys($stock_ids);
  }
}