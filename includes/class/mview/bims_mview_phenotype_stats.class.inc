<?php
/**
 * The declaration of BIMS_MVIEW_PHENOTYPE_STATS class.
 *
 */
class BIMS_MVIEW_PHENOTYPE_STATS extends PUBLIC_BIMS_MVIEW_PHENOTYPE_STATS {

  /**
   * Class data members.
   */
  protected $stats_arr = NULL;

  /**
   * @see PUBLIC_BIMS_MVIEW_PHENOTYPE_STATS::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates stats array ($this->stats_arr).
    if ($this->stats == '') {
      $this->stats_arr = array();
    }
    else {
      $this->stats_arr = json_decode($this->stats, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_PHENOTYPE_STATS::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_PHENOTYPE_STATS::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_PHENOTYPE_STATS::insert()
   */
  public function insert() {

    // Updates the parent:$stats fields.
    $this->stats = json_encode($this->stats_arr);

    // Inserts a new dataset stats.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_MVIEW_PHENOTYPE_STATS::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$stats fields.
    $this->stats = json_encode($this->stats_arr);

    // Updates the dataset stats.
    return parent::update($new_values);
  }

  /**
   * Clears the stats.
   *
   * @param array $stock_ids
   */
  public function clear($node_ids = array()) {
    if (!empty($node_ids)) {
      $node_id_list = implode(",", $node_ids);
      $sql = "
        DELETE FROM {bims_mview_phenotype_stats}
        WHERE node_id IN ($node_id_list)
      ";
      db_query($sql);
    }
  }

  /**
   * Clear the previous data.
   *
   * @param integer $node_id
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function clearDEL($node_id = NULL, $cvterm_id = NULL) {

    // Sets the argument.
    $args = array(':root_id' => $this->node_id);

    // Sets where cluase.
    $where_str = ' SELECT node_id FROM {bims_node} WHERE root_id = :root_id ';
    if ($node_id) {
      $where_str .= ' AND node_id = :node_id ';
      $args[':node_id'] = $node_id;
    }
    if ($cvterm_id) {
      $where_str .= ' AND cvterm_id = :cvterm_id ';
      $args[':cvterm_id'] = $cvterm_id;
    }
    $sql = "DELETE FROM {bims_mview_phenotype_stats} WHERE node_id IN($where_str)";
    db_query($sql, $args);
  }

  /**
   * Updates the statistics.
   *
   * @param integer $group
   * @param array $group_ids
   * @param boolean $program_flag
   *
   * @return boolean
   */
  public function updateStats($group, $group_ids = array(), $update_num = TRUE, $program_flag = TRUE) {

    // Updates the stats.
    $transaction = db_transaction();
    try {

      // Gets the project IDs by the group.
      $node_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($node_ids)) {
        return TRUE;
      }

      // Clears the stats of the program and trials.
      if ($program_flag) {
        $node_ids []= $this->node_id;
      }
      $this->clear($node_ids);

      // Calculates the stats.
      if (!$this->_calcStats($node_ids)) {
        throw new Exception("Error : Failed to update the mview phenotype stats");
      }

      // Updates the numbers.
      if ($update_num) {
        $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
        if (!$bm_phenotype->updateNumbers('all', 'node_id', $node_ids)) {
          throw new Exception("Error : Failed to update the numbers");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Calculates the statistics.
   *
   * @param array $node_ids
   *
   * @return boolean
   */
  private function _calcStats($node_ids) {

    // Calculates the stats.
    $transaction = db_transaction();
    try {

      // Local variables.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);
      $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
      $bims_stats   = new BIMS_STATS();
      $m_phenotype  = $bm_phenotype->getMView();

      // Computes the stats of the trials.
      foreach ($node_ids as $node_id) {
        $bims_node          = BIMS_NODE::byID($node_id);
        $is_program         = ($bims_node->getType() == 'PROGRAM') ? TRUE : FALSE;
        $active_descriptors = array();

        // Gets the active descriptors.
        $params = array('flag' => BIMS_CLASS);
        if ($is_program) {
          $params['type'] = 'program';
        }
        else {
          $params['type'] = 'trial';
          $params['id']   = $node_id;
        }
        $descriptors = $bims_program->getActiveDescriptors($params);

        // Calculates the stats for the descriptors.
        $num_data = 0;
        foreach ((array)$descriptors as $descriptor) {
          $cvterm_id  = $descriptor->getCvtermID();
          $field      = "t$cvterm_id";
          $name       = $descriptor->getName();
          $format     = $descriptor->getFormat();

          // Calculates the stats for the descriptor.
          $param = array(
            'from'    => $m_phenotype,
            'name'    => $name,
            'field'   => $field,
            'format'  => $format,
          );
          if ($is_program) {
            $param['condition'] = 'root_id = :root_id';
            $param['args']      = array(':root_id' => $node_id);
          }
          else {
            $param['condition'] = 'node_id = :node_id';
            $param['args']      = array(':node_id' => $node_id);
          }
          $stats_arr = $bims_stats->calcStats($param);

          // Gets the number of the data.
          $num = (empty($stats_arr)) ? 0 : $stats_arr['num'];

          // If $num is not 0, adds it to the acitve descriptor array. If not 0,
          // drop the descriptor column of BIMS_PHNEOTYPE.
          if ($num) {
            $active_descriptors[$cvterm_id] = array(
              'name'    => $name,
              'format'  => $format,
            );
          }
          else {
            if (db_field_exists($m_phenotype, $field)) {
              db_drop_field($m_phenotype, $field);
            }
          }

          // Updates the number of the data.
          $num_data += $num;

          // Checks the condition.
          if (!($num == 0 && !$flag_add)) {

            // Adds the stats.
            $num = (empty($stats_arr)) ? 0 : $stats_arr['num'];
            $details = array(
              'node_id'   => $node_id,
              'cvterm_id' => $descriptor->getCvtermID(),
              'name'      => $descriptor->getName(),
              'num_data'  => $num,
              'stats'     => json_encode($stats_arr),
            );
            $mview_stats = new BIMS_MVIEW_PHENOTYPE_STATS($details);
            if (!$mview_stats->insert()) {
              throw new Exception("Error : Failed to insert the stats for " . $descriptor->getName() . " for the program");
            }
          }
        }

        // Updates the data number.
        if ($is_program) {
          $bims_program->updatePropByKey('num_data', $num_data);
          $bims_program->updateStoredActiveDescriptors($active_descriptors);
        }
        else {
          $bims_node->updatePropByKey('num_data', $num_data);
          $bims_node->updateStoredActiveDescriptors($active_descriptors);
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the statistics for the phenotyping data of the program. Do not add
   * the no data stats if $flag_add is TRUE.
   *
   * @param array $trial
   * @param integer $cvterm_id
   * @param boolean $flag_add
   *
   * @return boolean
   */
  public function updateMView($trials = array(), $cvterm_id = NULL, $flag_add = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Gets the all trials and updates their stats.
      if (empty($trials)) {
        $trials = $bims_program->getTrialsByProjectType('phenotype', BIMS_CLASS);
      }
      foreach ((array)$trials as $trial) {

        // Updates the stats for this trial.
        bims_print('Updating phenotype-stats for ' . $trial->getName(), 1, 2);
        if (!$this->updateMViewTrial($trial, $cvterm_id, $flag_add)) {
          throw new Exception("Error: Failed to update the stats for trials");
        }
      }

      // Updates the stats for program.
      bims_print('Updating phenotype-stats for program : ' . $bims_program->getName(), 1, 2);
      if (!$this->updateMViewProgram($bims_program, $cvterm_id, $flag_add)) {
        throw new Exception("Error: Failed to update the stats for progaram");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the stats for a program.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param integer $cvterm_id
   * @param boolean $flag_add
   *
   * @return boolean
   */
  private function updateMViewProgram(BIMS_PROGRAM $bims_program, $cvterm_id = NULL, $flag_add = TRUE) {

    // Gets BIMS_MVIEW_PHENOTYPE.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
    $m_phenotype  = $bm_phenotype->getMView();

    // Calculates the stats for each active descriptor.
    $descriptors = array();
    if ($cvterm_id) {
      $descriptors []= BIMS_MVIEW_DESCRIPTOR::byID($bims_program->getNodeID(), $cvterm_id);
    }
    else {
      $params = array(
        'flag'  => BIMS_CLASS,
        'type'  => 'program',
      );
      $descriptors = $bims_program->getActiveDescriptors($params);
    }
    foreach ((array)$descriptors as $descriptor) {

      // Deletes the current stats.
      $keys = array(
        'node_id'   => $bims_program->getNodeID(),
        'cvterm_id' => $descriptor->getCvtermID(),
      );
      $mview_stats = BIMS_MVIEW_PHENOTYPE_STATS::byKey($keys);
      if ($mview_stats) {
        $mview_stats->delete();
      }

      // Calculates the stats for the descriptor.
      $param = array(
        'condition' => 'root_id = :root_id',
        'args'      => array(':root_id' => $bims_program->getNodeID()),
        'from'      => $m_phenotype,
        'name'      => $descriptor->getName(),
        'field'     => 't' . $descriptor->getCvtermID(),
        'format'    => $descriptor->getFormat(),
      );
      $bims_stats = new BIMS_STATS();
      $stats_arr = $bims_stats->calcStats($param);

      // Gets the number of the data.
      $num = (empty($stats_arr)) ? 0 : $stats_arr['num'];

      // Checks the condition.
      if (!($num == 0 && !$flag_add)) {

        // Adds the stats.
        $num = (empty($stats_arr)) ? 0 : $stats_arr['num'];
        $details = array(
          'node_id'   => $bims_program->getNodeID(),
          'cvterm_id' => $descriptor->getCvtermID(),
          'name'      => $descriptor->getName(),
          'num_data'  => $num,
          'stats'     => json_encode($stats_arr),
        );
        $mview_stats = new BIMS_MVIEW_PHENOTYPE_STATS($details);
        if (!$mview_stats->insert()) {
          print "Error : Failed to insert the stats for " . $descriptor->getName() . " for the program.\n";
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * Updates the stats for trials.
   *
   * @param BIMS_TRIAL $trial
   * @param integer $cvterm_id
   * @param boolean $flag_add
   *
   * @return boolean
   */
  private function updateMViewTrial(BIMS_TRIAL $trial, $cvterm_id = NULL, $flag_add = TRUE) {

    // Gets BIMS_PROGRAM.
    $bims_program = BIMS_PROGRAM::byID($trial->getRootID());

    // Gets BIMS_MVIEW_PHENOTYPE.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $trial->getRootID()));
    $m_phenotype  = $bm_phenotype->getMView();

    // Calculates the stats for each active descriptor.
    $descriptors = array();
    if ($cvterm_id) {
      $descriptors []= BIMS_MVIEW_DESCRIPTOR::byID($trial->getRootID(), $cvterm_id);
    }
    else {
      $params = array(
        'flag'  => BIMS_CLASS,
        'type'  => 'trial',
        'id'    => $trial->getNodeID(),
      );
      $descriptors = $bims_program->getActiveDescriptors($params);
    }
    $total_descriptors = sizeof($descriptors);
    $num_descriptors = 0;
    foreach ((array)$descriptors as $cvterm_id => $descriptor) {
      $num_descriptors++;
      $cvterm_id = $descriptor->getCvtermID();

      // Clears the mview.
      $this->clear($trial->getNodeID(), $cvterm_id);

      // Shows the progress.
      if ($num_descriptors % 3 == 0) {
        $msg = sprintf("%d / %d = %.03f %%", $num_descriptors, $total_descriptors, $num_descriptors / $total_descriptors * 100.0);
        bims_print($msg);
      }

      // Deletes the current stats.
      $keys = array(
        'node_id'   => $trial->getNodeID(),
        'cvterm_id' => $cvterm_id,
      );
      $mview_stats = BIMS_MVIEW_PHENOTYPE_STATS::byKey($keys);
      if ($mview_stats) {
        $mview_stats->delete();
      }

      // Calculates the stats for the descriptor.
      $bims_stats = new BIMS_STATS();
      $param = array(
        'condition' => 'node_id = :node_id',
        'args'      => array(':node_id' => $trial->getNodeID()),
        'from'      => $m_phenotype,
        'name'      => $descriptor->getName(),
        'field'     => "t$cvterm_id",
        'format'    => $descriptor->getFormat(),
      );
      $stats_arr = $bims_stats->calcStats($param);

      // Gets the number of the data.
      $num = (empty($stats_arr)) ? 0 : $stats_arr['num'];

      // Checks the condition.
      if (!($num == 0 && !$flag_add)) {

        // Adds the stats.
        $details = array(
          'node_id'   => $trial->getNodeID(),
          'cvterm_id' => $cvterm_id,
          'name'      => $descriptor->getName(),
          'num_data'  => $num,
          'stats'     => json_encode($stats_arr),
        );
        $mview_stats = new BIMS_MVIEW_PHENOTYPE_STATS($details);
        if (!$mview_stats->insert()) {
          print "Error : Failed to insert the stats for " . $descriptor->getName() . " for the trial.\n";
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * Returns the statistical descriptors for the provided trial.
   *
   * @param integer $node_id
   * @param integer $flag
   *
   * @return array
   */
  public static function getStatDescriptors($node_id, $flag = BIMS_OPTION) {
    $sql = "
      SELECT S.*
      FROM {bims_mview_phenotype_stats} S
      WHERE S.node_id = :node_id
      ORDER BY S.name
    ";
    $results = db_query($sql, array(':node_id' => $node_id));
    $descriptors = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_CLASS) {
        $descriptors []= BIMS_MVIEW_DESCRIPTOR::byID($obj->node_id, $obj->cvterm_id);
      }
      else if ($flag == BIMS_OBJECT) {
        $descriptors []= $obj;
      }
      else {
        $descriptors[$obj->cvterm_id] = $obj->name;
      }
    }
    return $descriptors;
  }

  /**
   * Returns the statistical descriptors for the provided cvterm and node ID of the trial.
   *
   * Returns the stats of the given trait in table.
   *
   * @param integer $cvterm_id
   * @param integer $node_id
   * @param array $stats_arr
   * @param boolean $row_only
   *
   * @return string
   */
  public static function getStatsTable($cvterm_id, $node_id, &$stats_arr = array(), $row_only = FALSE) {

    // Gets BIMS_MVIEW_PHENOTYPE_STATS and the stats array.
    $args = array(
      'node_id'   => $node_id,
      'cvterm_id' => $cvterm_id,
    );
    $bm_phenotype_stats = BIMS_MVIEW_PHENOTYPE_STATS::byKey($args);
    if (!$bm_phenotype_stats) {
      return "<em>No stats for the trait ($cvterm_id) was found.";
    }
    $stats_arr = $bm_phenotype_stats->getStatsArr();

    // Returns the stats table.
    return BIMS_STATS::getStatsTable($node_id, $stats_arr, $cvterm_id, $row_only);
  }

  /**
   * Returns the statistical information of the given trait.
   *
   * @param integer $node_id
   * @param integer $cvterm_id
   *
   * @return array
   */
  public static function getStatsByID($node_id, $cvterm_id) {

    // Gets BIMS_MVIEW_PHENOTYPE_STATS and the stats array.
    $args = array(
      'node_id'   => $node_id,
      'cvterm_id' => $cvterm_id,
    );
    $bm_phenotype_stats = BIMS_MVIEW_PHENOTYPE_STATS::byKey($args);
    if ($bm_phenotype_stats) {
      return $bm_phenotype_stats->getStatsArr();
    }
    return array();
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the stats array.
   *
   * @return array
   */
  public function getStatsArr() {
    return $this->stats_arr;
  }

  /**
   * Sets the stats array.
   *
   * @param array $stats
   */
  public function setStatsArr($stats) {
    $this->stats_arr = $stats;
  }

  /**
   * Returns the value of the given key in stats.
   */
  public function getStatsByKey($key) {
    if (array_key_exists($key, $this->stats_arr)) {
      return $this->stats_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in stats.
   *
   * @param string $key
   * @param string $value
   */
  public function setStatsByKey($key, $value) {
    $this->stats_arr[$key] = $value;
  }

 /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Local variables.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
    $m_phenotype  = $bm_phenotype->getMView();

    // Initializes the returning array.
    $node_ids = array();

    // GROUP : private.
    if ($group == 'private') {
      $sql = "
        SELECT DISTINCT node_id FROM {bims_node}
        WHERE root_id = :root_id AND project_type = :type AND project_id NOT IN (
          SELECT project_id FROM {bims_imported_project}
          WHERE program_id = :root_id
        )
      ";
      $args = array(
        ':root_id'  => $this->node_id,
        ':type'     => 'phenotype',
      );
      $results = db_query($sql, $args);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

      // GROUP : public.
    else if ($group == 'public') {
      $sql = "
        SELECT DISTINCT N.node_id
        FROM {bims_node} N
          INNER JOIN {bims_imported_project} IP on IP.project_id = N.project_id
        WHERE N.root_id = :root_id AND N.project_type = :type
      ";
      $args = array(
        ':root_id'  => $this->node_id,
        ':type'     => 'phenotype',
      );
      $results = db_query($sql, $args);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

    // GROUP : node.
    else if ($group == 'node') {
      $id_list = implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT node_id FROM {bims_node}
        WHERE node_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

    // GROUP : project.
    else if ($group == 'project') {
      $id_list = implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT node_id FROM {bims_node}
        WHERE project_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

    // GROUP : stock.
    else if ($group == 'stock') {
      $id_list = implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT node_id FROM {$m_phenotype}
        WHERE stock_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

    // GROUP : sample.
    else if ($group == 'sample') {
      $id_list = implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT node_id FROM {$m_phenotype}
        WHERE sample_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

    // GROUP : cross.
    else if ($group == 'cross') {
      $id_list = implode(',', $group_ids);
      $sql = "
        SELECT DISTINCT node_id FROM {$m_phenotype}
        WHERE nd_experiment_id IN ($id_list)
      ";
      $results = db_query($sql);
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }

    // GROUP : cvterm.
    else if ($group == 'cvterm') {
      foreach ($group_ids as $cvterm_id) {
        $field = "t$cvterm_id";
        if (!db_field_exists($m_phenotype, $field)) {
          $sql = "
            SELECT DISTINCT node_id FROM {$m_phenotype}
            WHERE $field != '' AND
          ";
          $results = db_query($sql);
          while ($node_id = $results->fetchField()) {
            $node_ids[$node_id] = TRUE;
          }
        }
      }
    }

    // GROUP : program (private + public).
    else {
      $sql = "
        SELECT DISTINCT node_id FROM {bims_node}
        WHERE  project_type = :type
      ";
      $results = db_query($sql, array(':type' => 'phenotype'));
      while ($node_id = $results->fetchField()) {
        $node_ids[$node_id] = TRUE;
      }
    }
    return array_keys($node_ids);
  }
}