<?php
/**
 * The declaration of BIMS_MVIEW_ALIAS_MARKER class.
 *
 */
class BIMS_MVIEW_ALIAS_MARKER extends BIMS_MVIEW_ALIAS {

  /**
   * Class data members.
   */
  /**
    * @see BIMS_MVIEW::__construct()
    *
    * @param $details
    */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_MVIEW::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see BIMS_MVIEW_ALIAS::updateAlias()
   */
  public function updateAlias($schema, $feature_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Checks the stock IDs.
      if (empty($feature_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        $group_ids = array();
        $group_ids[$schema] = $feature_ids;
        $this->clearAliasByID('feature', $feature_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Updates the mview.
      $func_name = ($schema == 'chado') ? 'Chado' : 'BIMS';
      if (!$this->{'_updateAlias' . $func_name}($bims_program, $feature_ids)) {
        throw new Exception("Error : Failed to update the mview data");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW_ALIAS::_updateAliasChado()
   */
  protected function _updateAliasChado(BIMS_PROGRAM $bims_program, $feature_ids) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Gets the cvterms.
      $id_alias = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'alias')->getCvtermID();

      // Gets all the aliases.
      foreach ((array)$feature_ids as $feature_id) {
        $sql = "
          SELECT S.name
          FROM {chado.synonym} S
            INNER JOIN {chado.feature_synonym} FS on FS.synonym_id = S.synonym_id
          WHERE S.type_id = :type_id AND FS.feature_id = :feature_id
        ";
        $args = array(
          ':feature_id' => $feature_id,
          ':type_id'    => $id_alias,
        );
        $results = db_query($sql, $args);
        while ($alias = $results->fetchField()) {

          // Adds the alias.
          $details = array(
            'target_id' => $feature_id,
            'type'      => 'feature',
            'alias'     => $alias,
          );
          if (!$this->addAlias($details)) {
            throw new Exception('Error : Failed to add an alias');
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_MVIEW_ALIAS::_updateAliasBIMS()
   */
  protected function _updateAliasBIMS(BIMS_PROGRAM $bims_program, $feature_ids) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Gets the aliases from BIMS_CHADO and Adds them.
      $bc_feature = new BIMS_CHADO_FEATURE($this->node_id);
      foreach ((array)$feature_ids as $feature_id) {
        $bc_feature->setFeatureID($feature_id);

        // Gets the aliases of this marker.
        $aliases = $bc_feature->getAlias(BIMS_OBJECT);
        foreach ((array)$aliases as $alias) {

          // Adds the alias.
          $details = array(
            'target_id' => $feature_id,
            'type'      => 'feature',
            'alias'     => $alias->name,
          );
          if (!$this->addAlias($details)) {
            throw new Exception('Error : Failed to add an alias');
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }
}
