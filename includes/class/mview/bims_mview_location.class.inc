<?php
/**
 * The declaration of BIMS_MVIEW_LOCATION class.
*
*/
class BIMS_MVIEW_LOCATION extends BIMS_MVIEW {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_MVIEW::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_MVIEW::getMView()
   */
  public function getMView($flag = TRUE) {
    $schema = $flag ? 'bims.' : '';
    return $schema . 'bims_' . $this->getNodeID() . '_mview_location';
  }

  /**
   * @see BIMS_MVIEW::getTableSchema()
   */
  public function getTableSchema() {
    return array(
      'description' => 'The materialized view table for location.',
      'fields' => array(
        'node_id' => array(
          'type'      => 'int',
          'not null'  => TRUE,
        ),
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'description' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'long_name' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'type' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'latitude' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'longitude' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'altitude' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'country' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'state' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'region' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'address' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'chado' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_geolocation_id'),
    );
  }

  /**
   * @see BIMS_MVIEW::updateMView()
   */
  public function updateMView($recreate = FALSE, $group, $group_ids = array(), $clear = TRUE) {

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Recreates the mview.
      if ($recreate) {
        $this->dropMView();
        $this->createMView();
      }

      // Gets the IDs by the group.
      $grouped_ids = $this->getIDByGroup($group, $group_ids);
      if (empty($grouped_ids)) {
        return TRUE;
      }

      // Clears the mview.
      if ($clear) {
        BIMS_MVIEW::clearByGroupID($this->getMView(), 'nd_geolocation_id', $grouped_ids);
      }

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($this->node_id);

      // Updates the mviews by schema.
      if (!$this->_updateMViewByChado($bims_program, $grouped_ids['chado'])) {
        throw new Exception("Error : Failed to update the mview data in Chado");
      }
      if (!$this->_updateMViewByBIMS($bims_program, $grouped_ids['bims'])) {
        throw new Exception("Error : Failed to update the mview data in BIMS");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByChado(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumns($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets the cvterms.
      $cvterms = array(
        'type'            => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'type')->getCvtermID(),
        'address'         => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'address')->getCvtermID(),
        'region'          => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'region')->getCvtermID(),
        'state'           => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'state')->getCvtermID(),
        'country'         => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'country')->getCvtermID(),
        'site_long_name'  => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'site_long_name')->getCvtermID(),
      );

      // Popluates the mview.
      foreach ((array)$grouped_ids as $nd_geolocation_id) {
        $sql = "
          SELECT DISTINCT NG.description, NG.nd_geolocation_id,
            NG.latitude, NG.longitude, NG.altitude, TYPE.value AS type,
            ADDRESS.value AS address, REGION.value AS region,
            STATE.value AS state, COUNTRY.value AS country,
            SITE_LONG_NAME.value AS long_name $select_str
          FROM {chado.nd_geolocation} NG
            $join_str
            LEFT JOIN (
              SELECT NGP.nd_geolocation_id, NGP.value
              FROM {chado.nd_geolocationprop} NGP
              WHERE NGP.type_id = :type
            ) TYPE on TYPE.nd_geolocation_id = NG.nd_geolocation_id
            LEFT JOIN (
              SELECT NGP.nd_geolocation_id, NGP.value
              FROM {chado.nd_geolocationprop} NGP
              WHERE NGP.type_id = :address
            ) ADDRESS on ADDRESS.nd_geolocation_id = NG.nd_geolocation_id
            LEFT JOIN (
              SELECT NGP.nd_geolocation_id, NGP.value
              FROM {chado.nd_geolocationprop} NGP
              WHERE NGP.type_id = :region
            ) REGION on REGION.nd_geolocation_id = NG.nd_geolocation_id
            LEFT JOIN (
              SELECT NGP.nd_geolocation_id, NGP.value
              FROM {chado.nd_geolocationprop} NGP
              WHERE NGP.type_id = :state
            ) STATE on STATE.nd_geolocation_id = NG.nd_geolocation_id
            LEFT JOIN (
              SELECT NGP.nd_geolocation_id, NGP.value
              FROM {chado.nd_geolocationprop} NGP
              WHERE NGP.type_id = :country
            ) COUNTRY on COUNTRY.nd_geolocation_id = NG.nd_geolocation_id
            LEFT JOIN (
              SELECT NGP.nd_geolocation_id, NGP.value
              FROM {chado.nd_geolocationprop} NGP
              WHERE NGP.type_id = :site_long_name
            ) SITE_LONG_NAME on SITE_LONG_NAME.nd_geolocation_id = NG.nd_geolocation_id
          WHERE NG.nd_geolocation_id = :nd_geolocation_id
        ";
        $args = array(
          ':type'               => $cvterms['type'],
          ':address'            => $cvterms['address'],
          ':region'             => $cvterms['region'],
          ':state'              => $cvterms['state'],
          ':country'            => $cvterms['country'],
          ':site_long_name'     => $cvterms['site_long_name'],
          ':nd_geolocation_id'  => $nd_geolocation_id,
        );
        $results = db_query($sql, $args);
        while ($arr = $results->fetchAssoc()) {
          $nd_geolocation_id = $arr['nd_geolocation_id'];

          // Adds the field values.
          $arr['node_id'] = $this->node_id;
          $arr['chado']   = 1;
          $arr['name']    = BIMS_PROGRAM::rmPrefix($arr['description']);

          // Inserts into the mview.
          if (!$this->addData($this->getMView(), $arr, array('nd_geolocation_id' => $nd_geolocation_id))) {
            throw new Exception("Error : Failed to add the data to the mview");
          }
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Update the mviews of the data in BIMS.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $grouped_ids
   *
   * @return boolean
   */
  private function _updateMViewByBIMS(BIMS_PROGRAM $bims_program, $grouped_ids) {

    // Checks the IDs.
    if (empty($grouped_ids)) {
      return TRUE;
    }

    // Updates the mview.
    $transaction = db_transaction();
    try {

      // Updates the columns of the materialized view.
      $ret_arr = $this->_updateColumns($bims_program);
      if ($ret_arr['error']) {
        throw new Exception("Error : Failed to update columns");
      }
      $select_str = $ret_arr['select_str'];
      $join_str   = $ret_arr['join_str'];

      // Gets BIMS_CHADO table.
      $bc_site        = new BIMS_CHADO_SITE($this->node_id);
      $table_site     = $bc_site->getTableName('site');
      $table_siteprop = $bc_site->getTableName('siteprop');

      // Gets the cvterms.
      $cvterms = array(
        'type'            => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'type')->getCvtermID(),
        'address'         => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'address')->getCvtermID(),
        'region'          => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'region')->getCvtermID(),
        'state'           => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'state')->getCvtermID(),
        'country'         => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'country')->getCvtermID(),
        'site_long_name'  => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'site_long_name')->getCvtermID(),
      );

      // Gets the all the locations for this program.
      $id_str = implode(',', $grouped_ids);
      $sql = "
        SELECT DISTINCT T.description, T.nd_geolocation_id,
          T.latitude, T.longitude, T.altitude, TYPE.value AS type,
           ADDRESS.value AS address, REGION.value AS region,
           STATE.value AS state, COUNTRY.value AS country,
           SITE_LONG_NAME.value AS long_name $select_str
        FROM {$table_site} T
          $join_str
          LEFT JOIN (
            SELECT SP.nd_geolocation_id, SP.value
            FROM $table_siteprop SP
            WHERE SP.type_id = :type
          ) TYPE on TYPE.nd_geolocation_id = T.nd_geolocation_id
          LEFT JOIN (
            SELECT SP.nd_geolocation_id, SP.value
            FROM $table_siteprop SP
            WHERE SP.type_id = :address
          ) ADDRESS on ADDRESS.nd_geolocation_id = T.nd_geolocation_id
          LEFT JOIN (
            SELECT SP.nd_geolocation_id, SP.value
            FROM $table_siteprop SP
            WHERE SP.type_id = :region
          ) REGION on REGION.nd_geolocation_id = T.nd_geolocation_id
          LEFT JOIN (
            SELECT SP.nd_geolocation_id, SP.value
            FROM $table_siteprop SP
            WHERE SP.type_id = :state
          ) STATE on STATE.nd_geolocation_id = T.nd_geolocation_id
          LEFT JOIN (
            SELECT SP.nd_geolocation_id, SP.value
            FROM $table_siteprop SP
            WHERE SP.type_id = :country
          ) COUNTRY on COUNTRY.nd_geolocation_id = T.nd_geolocation_id
          LEFT JOIN (
            SELECT SP.nd_geolocation_id, SP.value
            FROM $table_siteprop SP
            WHERE SP.type_id = :site_long_name
          ) SITE_LONG_NAME on SITE_LONG_NAME.nd_geolocation_id = T.nd_geolocation_id
        WHERE T.nd_geolocation_id IN ($id_str)
      ";
      $args = array(
        ':type'           => $cvterms['type'],
        ':address'        => $cvterms['address'],
        ':region'         => $cvterms['region'],
        ':state'          => $cvterms['state'],
        ':country'        => $cvterms['country'],
        ':site_long_name' => $cvterms['site_long_name'],
      );
      $results = db_query($sql, $args);
      while ($arr = $results->fetchAssoc()) {
        $nd_geolocation_id = $arr['nd_geolocation_id'];

        // Adds the field values.
        $arr['node_id'] = $this->node_id;
        $arr['chado']   = 0;
        $arr['name']    = BIMS_PROGRAM::rmPrefix($arr['description']);

        // Inserts into the mview.
        if (!$this->addData($this->getMView(), $arr, array('nd_geolocation_id' => $nd_geolocation_id))) {
          throw new Exception("Error : Failed to add the data to the mview");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   *
   * @param $fields
   */
  public function update($fields) {
    $transaction = db_transaction();
    try {
      $nd_geolocation_id = $fields['nd_geolocation_id'];

      // Updates the record.
      db_update($this->getMView())
        ->fields($fields)
        ->condition('node_id', $this->node_id, '=')
        ->condition('nd_geolocation_id', $nd_geolocation_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the columns of the the materialized view.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private function _updateColumns(BIMS_PROGRAM $bims_program) {

    // Local variables.
    $program_id = $bims_program->getProgramID();
    $m_location = $this->getMView();

    // Gets BIMS_CHADO tables.
    $bc_site         = new BIMS_CHADO_SITE($program_id);
    $table_siteprop  = $bc_site->getTableName('siteprop');

    // Initializes the variables.
    $select_str = '';
    $join_str   = '';

    // Returns if no BIMS CHADO.
    if (!db_table_exists($table_siteprop)) {
      return array(
        'error'       => FALSE,
        'select_str'  => $select_str,
        'join_str'    => $join_str,
      );
    }

    // Adds the property columns.
    $select_str = '';
    $join_str   = '';
    $props      = $bims_program->getProperties('site', 'custom', BIMS_OPTION);
    foreach ((array)$props as $cvterm_id => $name) {
      $field = "p$cvterm_id";

      // Adds the fields if not exist.
      if (!db_field_exists($m_location, $field)) {
        db_add_field($m_location, $field, array('type' => 'text'));
      }

      // Updates the select statement.
      $select_str .= ", JP$cvterm_id.value AS p$cvterm_id";

      // Updates the join statement.
      $join_str .= "
        LEFT JOIN (
          SELECT NGP.nd_geolocation_id, NGP.value
          FROM {$table_siteprop} NGP
          WHERE NGP.type_id = $cvterm_id
        ) JP$cvterm_id on JP$cvterm_id.nd_geolocation_id = T.nd_geolocation_id
      ";
    }
    return array(
      'error'       => FALSE,
      'select_str'  => $select_str,
      'join_str'    => $join_str,
    );
  }

  /**
   * Checks if any location with coordinates exists.
   *
   * @return boolean
   */
  public function hasCoordinates() {
    $m_location = $this->getMView();
    $sql = "
      SELECT COUNT(nd_geolocation_id)
      FROM $m_location
      WHERE latitude != '' AND longitude != ''
    ";
    $num = db_query($sql)->fetchField();
    return ($num) ? TRUE : FALSE;
  }

  /**
   * Adds a new location.
   *
   * @param array
   *
   * @return boolean
   */
  public function addLocation($details) {

    // Add a location.
    $m_location = $this->getMView();
    $node_id    = $this->getNodeID();
    $cols       = 'node_id';
    $vals       = $node_id;
    foreach ($details as $key => $val) {
      $cols .= ", $key";
      $vals .= ", '$val'";
    }
    $sql = "INSERT INTO $m_location ($cols) VALUES ($vals)";
    db_query($sql);
    return TRUE;
  }

  /**
   * Returns all locations.
   *
   * @param integer $flag
   * @param boolean $coordinates
   *
   * @return array
   */
  public function getLocations($flag = BIMS_OBJECT, $coordinates = FALSE) {
    $locations = array();

    // Gets all the locations.
    $m_location = $this->getMView();
    if (db_table_exists($m_location)) {
      $conditions = '';
      if ($coordinates) {
        $conditions = "AND (latitude != '' AND longitude != '')";
      }
      $sql = "SELECT MV.* FROM $m_location MV WHERE 1=1 $conditions ORDER BY MV.name";
      $results = db_query($sql);
      while ($obj = $results->fetchObject()) {
        if ($flag == BIMS_OPTION) {
          $locations[$obj->nd_geolocation_id] = $obj->name;
        }
        else {
          $locations []= $obj;
        }
      }
    }
    return $locations;
  }

  /**
   * Returns the location object.
   *
   * @param integer $nd_geolocation_id
   * @param integer $flag
   *
   * @return object
   */
  public function getLocation($nd_geolocation_id, $flag = BIMS_OBJECT) {
    $m_location = $this->getMView();
    if ($nd_geolocation_id != '' && db_table_exists($m_location)) {
      $sql = "SELECT MV.* FROM $m_location MV WHERE MV.nd_geolocation_id = :nd_geolocation_id";
      $results = db_query($sql, array(':nd_geolocation_id' => $nd_geolocation_id));
      return $results->fetchObject();
    }
    return NULL;
  }

  /**
   * Returns the IDs by the group.
   *
   * @param string $group
   * @param array $group_ids
   *
   * @return array
   */
  public function getIDByGroup($group, $group_ids = array()) {

    // Local variables.
    $bc_site    = new BIMS_CHADO_SITE($this->node_id);
    $bc_pc      = new BIMS_CHADO_PHENOTYPE_CALL($this->node_id);
    $table_site = $bc_site->getTableName('site');
    $table_pc   = $bc_pc->getTableName('phenotype_call');

    // Gets the list of the imported projects.
    $projects = BIMS_IMPORTED_PROJECT::getImportedProjByType($this->node_id, 'phenotype');
    $tmp = array();
    foreach ((array)$projects as $project_obj) {
      $tmp[$project_obj->project_id] = TRUE;
    }
    $imported_project_ids = empty($tmp) ? '' : implode(",", array_keys($tmp));

    // Initializes the returning array.
    $grouped_ids = array('chado' => array(), 'bims' => array());

    // GROUP : private.
    if ($group == 'private') {
      if (db_table_exists($table_site)) {
        $sql = "SELECT nd_geolocation_id FROM {$table_site}";
        $results = db_query($sql);
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['bims'][$nd_geolocation_id] = TRUE;
        }
      }
    }

    // GROUP : public.
    else if ($group == 'public') {
      if ($imported_project_ids) {
        $sql = "
          SELECT DISTINCT NE.nd_geolocation_id
          FROM {chado.nd_experiment} NE
            INNER JOIN {chado.nd_experiment_project} NEP on NEP.nd_experiment_id = NE.nd_experiment_id
          WHERE NEP.project_id IN ($imported_project_ids)
        ";
        $results = db_query($sql);
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['chado'][$nd_geolocation_id] = TRUE;
        }
      }
    }

    // GROUP : project.
    else if ($group == 'project') {

      // BIMS.
      if (db_table_exists($table_pc)) {
        $sql = "
          SELECT DISTINCT PC.nd_geolocation_id
          FROM {$table_pc} PC
          WHERE PC.project_id = :project_id
        ";
        $results = db_query($sql, array(':project_id' => $project_id));
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['bims'][$nd_geolocation_id] = TRUE;
        }
      }

      // Chado.
      if ($imported_project_ids) {
        $sql = "
          SELECT DISTINCT NE.nd_geolocation_id
          FROM {chado.nd_experiment} NE
            INNER JOIN {chado.nd_experiment_project} NEP on NEP.nd_experiment_id = NE.nd_experiment_id
          WHERE NEP.project_id IN ($imported_project_ids)
        ";
        $results = db_query($sql);
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['chado'][$nd_geolocation_id] = TRUE;
        }
      }
    }

    // GROUP : site.
    else if ($group == 'site') {
      $id_list = implode(",", $group_ids);

      // BIMS.
      if (db_table_exists($table_site)) {
        $sql = "
          SELECT nd_geolocation_id FROM {$table_site}
          WHERE nd_geolocation_id IN ($id_list)
        ";
        $results = db_query($sql);
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['bims'][$nd_geolocation_id] = TRUE;
        }
      }

      // Chado.
      if ($imported_project_ids) {
        $sql = "
          SELECT DISTINCT NE.nd_geolocation_id
          FROM {chado.nd_experiment} NE
            INNER JOIN {chado.nd_experiment_project} NEP on NEP.nd_experiment_id = NE.nd_experiment_id
          WHERE nd_geolocation_id IN ($id_list) AND NEP.project_id IN ($imported_project_ids)
        ";
        $results = db_query($sql);
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['chado'][$nd_geolocation_id] = TRUE;
        }
      }
    }

    // GROUP : program (private + public).
    else {

      // BIMS.
      if (db_table_exists($table_site)) {
        $sql = "SELECT nd_geolocation_id FROM {$table_site}";
        $results = db_query($sql);
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['bims'][$nd_geolocation_id] = TRUE;
        }
      }

      // Chado.
      if ($imported_project_ids) {
        $sql = "
          SELECT DISTINCT NE.nd_geolocation_id
          FROM {chado.nd_experiment} NE
            INNER JOIN {chado.nd_experiment_project} NEP on NEP.nd_experiment_id = NE.nd_experiment_id
          WHERE NEP.project_id IN ($imported_project_ids);
        ";
        $results = db_query($sql);
        while ($nd_geolocation_id = $results->fetchField()) {
          $grouped_ids['chado'][$nd_geolocation_id] = TRUE;
        }
      }
    }

    // Returns the grouped IDs.
    $ret_array = array('bims' => array(), 'chado' => array());
    if (empty($grouped_ids['chado']) && empty($grouped_ids['bims'])) {
      return array();
    }
    if (!empty($grouped_ids['bims'])) {
      $ret_array['bims'] = array_keys($grouped_ids['bims']);
    }
    if (!empty($grouped_ids['chado'])) {
      $ret_array['chado'] = array_keys($grouped_ids['chado']);
    }
    return $ret_array;
  }
}