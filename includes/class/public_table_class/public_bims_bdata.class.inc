<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_BDATA class.
 *
 */
class PUBLIC_BIMS_BDATA  {

  /**
   *  Data members for PUBLIC_BIMS_BDATA.
   */
  protected $member_arr  = NULL;
  protected $bdata_id    = NULL;
  protected $crop_id     = NULL;
  protected $user_id     = NULL;
  protected $name        = NULL;
  protected $cvterm_id   = NULL;
  protected $description = NULL;
  protected $filename    = NULL;
  protected $status      = NULL;
  protected $access      = NULL;
  protected $prop        = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'bdata_id'    => 'serial',
    'crop_id'     => 'integer',
    'user_id'     => 'integer',
    'name'        => 'character_varying(255)',
    'cvterm_id'   => 'integer',
    'description' => 'text',
    'filename'    => 'character_varying(255)',
    'status'      => 'integer',
    'access'      => 'integer',
    'prop'        => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'bdata_id'    => TRUE,
    'crop_id'     => TRUE,
    'user_id'     => TRUE,
    'name'        => TRUE,
    'cvterm_id'   => TRUE,
    'description' => FALSE,
    'filename'    => TRUE,
    'status'      => TRUE,
    'access'      => FALSE,
    'prop'        => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr  = $details;
    $this->bdata_id    = array_key_exists('bdata_id', $details)    ? $details['bdata_id']    : '';
    $this->crop_id     = array_key_exists('crop_id', $details)     ? $details['crop_id']     : '';
    $this->user_id     = array_key_exists('user_id', $details)     ? $details['user_id']     : '';
    $this->name        = array_key_exists('name', $details)        ? $details['name']        : '';
    $this->cvterm_id   = array_key_exists('cvterm_id', $details)   ? $details['cvterm_id']   : '';
    $this->description = array_key_exists('description', $details) ? $details['description'] : '';
    $this->filename    = array_key_exists('filename', $details)    ? $details['filename']    : '';
    $this->status      = array_key_exists('status', $details)      ? $details['status']      : 0;
    $this->access      = array_key_exists('access', $details)      ? $details['access']      : '';
    $this->prop        = array_key_exists('prop', $details)        ? $details['prop']        : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_BDATA by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_bdata WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'bdata_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_bdata_bims_bdata_ukey_001_key' => array('crop_id','name','user_id'),
      'bims_bdata_ukey_001_key' => array('crop_id','name'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['crop_id']     = $this->crop_id;
        $fields['user_id']     = $this->user_id;
        $fields['name']        = $this->name;
        $fields['cvterm_id']   = $this->cvterm_id;
        $fields['description'] = $this->description;
        $fields['filename']    = $this->filename;
        $fields['status']      = (is_numeric($this->status)) ? $this->status : 0;
        $fields['prop']        = $this->prop;
        if (is_numeric($this->access)) { $fields['access'] = $this->access; }

        // Inserts the record.
        $this->bdata_id = db_insert('bims_bdata')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['crop_id']     = $this->crop_id;
      $fields['user_id']     = $this->user_id;
      $fields['name']        = $this->name;
      $fields['cvterm_id']   = $this->cvterm_id;
      $fields['description'] = $this->description;
      $fields['filename']    = $this->filename;
      $fields['status']      = (is_numeric($this->status)) ? $this->status : 0;
      $fields['prop']        = $this->prop;
      if (is_numeric($this->access)) { $fields['access'] = $this->access; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_bdata')
        ->fields($fields)
        ->condition('bdata_id', $this->bdata_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_bdata')
        ->condition('bdata_id', $this->bdata_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the bdata_id.
   *
   * @retrun serial
   */
  public function getBdataID() {
    return $this->bdata_id;
  }

  /**
   * Sets the bdata_id.
   *
   * @param serial $bdata_id
   */
  public function setBdataID($bdata_id) {
    $this->bdata_id = $bdata_id;
  }

  /**
   * Retrieves the crop_id.
   *
   * @retrun integer
   */
  public function getCropID() {
    return $this->crop_id;
  }

  /**
   * Sets the crop_id.
   *
   * @param integer $crop_id
   */
  public function setCropID($crop_id) {
    $this->crop_id = $crop_id;
  }

  /**
   * Retrieves the user_id.
   *
   * @retrun integer
   */
  public function getUserID() {
    return $this->user_id;
  }

  /**
   * Sets the user_id.
   *
   * @param integer $user_id
   */
  public function setUserID($user_id) {
    $this->user_id = $user_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the cvterm_id.
   *
   * @retrun integer
   */
  public function getCvtermID() {
    return $this->cvterm_id;
  }

  /**
   * Sets the cvterm_id.
   *
   * @param integer $cvterm_id
   */
  public function setCvtermID($cvterm_id) {
    $this->cvterm_id = $cvterm_id;
  }

  /**
   * Retrieves the description.
   *
   * @retrun text
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description.
   *
   * @param text $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Retrieves the filename.
   *
   * @retrun character_varying(255)
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * Sets the filename.
   *
   * @param character_varying(255) $filename
   */
  public function setFilename($filename) {
    $this->filename = $filename;
  }

  /**
   * Retrieves the status.
   *
   * @retrun integer
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Sets the status.
   *
   * @param integer $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * Retrieves the access.
   *
   * @retrun integer
   */
  public function getAccess() {
    return $this->access;
  }

  /**
   * Sets the access.
   *
   * @param integer $access
   */
  public function setAccess($access) {
    $this->access = $access;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }
}