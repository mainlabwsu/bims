<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_MAPPING_FEATURE class.
 *
 */
class PUBLIC_BIMS_MAPPING_FEATURE  {

  /**
   *  Data members for PUBLIC_BIMS_MAPPING_FEATURE.
   */
  protected $member_arr       = NULL;
  protected $program_id       = NULL;
  protected $chado_feature_id = NULL;
  protected $bims_feature_id  = NULL;
  protected $prop             = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'program_id'       => 'integer',
    'chado_feature_id' => 'integer',
    'bims_feature_id'  => 'integer',
    'prop'             => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'program_id'       => TRUE,
    'chado_feature_id' => TRUE,
    'bims_feature_id'  => TRUE,
    'prop'             => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr       = $details;
    $this->program_id       = array_key_exists('program_id', $details)       ? $details['program_id']       : '';
    $this->chado_feature_id = array_key_exists('chado_feature_id', $details) ? $details['chado_feature_id'] : '';
    $this->bims_feature_id  = array_key_exists('bims_feature_id', $details)  ? $details['bims_feature_id']  : '';
    $this->prop             = array_key_exists('prop', $details)             ? $details['prop']             : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_MAPPING_FEATURE by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_mapping_feature WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return NULL;
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_mapping_feature_ukey_001_key' => array('program_id','chado_feature_id','bims_feature_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['program_id']       = $this->program_id;
        $fields['chado_feature_id'] = $this->chado_feature_id;
        $fields['bims_feature_id']  = $this->bims_feature_id;
        $fields['prop']             = $this->prop;


        // Inserts the record.
        db_insert('bims_mapping_feature')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['program_id']       = $this->program_id;
      $fields['chado_feature_id'] = $this->chado_feature_id;
      $fields['bims_feature_id']  = $this->bims_feature_id;
      $fields['prop']             = $this->prop;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_mapping_feature')
        ->fields($fields)
        ->condition('program_id', $this->program_id, '=')
        ->condition('chado_feature_id', $this->chado_feature_id, '=')
        ->condition('bims_feature_id', $this->bims_feature_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_mapping_feature')
        ->condition('program_id', $this->program_id, '=')
        ->condition('chado_feature_id', $this->chado_feature_id, '=')
        ->condition('bims_feature_id', $this->bims_feature_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the program_id.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program_id.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }

  /**
   * Retrieves the chado_feature_id.
   *
   * @retrun integer
   */
  public function getChadoFeatureID() {
    return $this->chado_feature_id;
  }

  /**
   * Sets the chado_feature_id.
   *
   * @param integer $chado_feature_id
   */
  public function setChadoFeatureID($chado_feature_id) {
    $this->chado_feature_id = $chado_feature_id;
  }

  /**
   * Retrieves the bims_feature_id.
   *
   * @retrun integer
   */
  public function getBimsFeatureID() {
    return $this->bims_feature_id;
  }

  /**
   * Sets the bims_feature_id.
   *
   * @param integer $bims_feature_id
   */
  public function setBimsFeatureID($bims_feature_id) {
    $this->bims_feature_id = $bims_feature_id;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }
}