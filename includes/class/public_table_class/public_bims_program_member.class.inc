<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_PROGRAM_MEMBER class.
 *
 */
class PUBLIC_BIMS_PROGRAM_MEMBER  {

  /**
   *  Data members for PUBLIC_BIMS_PROGRAM_MEMBER.
   */
  protected $member_arr        = NULL;
  protected $program_member_id = NULL;
  protected $program_id        = NULL;
  protected $user_id           = NULL;
  protected $permission        = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'program_member_id' => 'serial',
    'program_id'        => 'integer',
    'user_id'           => 'integer',
    'permission'        => 'character_varying(255)',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'program_member_id' => TRUE,
    'program_id'        => TRUE,
    'user_id'           => TRUE,
    'permission'        => TRUE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr        = $details;
    $this->program_member_id = array_key_exists('program_member_id', $details) ? $details['program_member_id'] : '';
    $this->program_id        = array_key_exists('program_id', $details)        ? $details['program_id']        : '';
    $this->user_id           = array_key_exists('user_id', $details)           ? $details['user_id']           : '';
    $this->permission        = array_key_exists('permission', $details)        ? $details['permission']        : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_PROGRAM_MEMBER by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_program_member WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'program_member_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_program_member_ukey_bims_program_member_program_id_user_id' => array('program_id','user_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['program_id'] = $this->program_id;
        $fields['user_id']    = $this->user_id;
        $fields['permission'] = $this->permission;


        // Inserts the record.
        $this->program_member_id = db_insert('bims_program_member')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['program_id'] = $this->program_id;
      $fields['user_id']    = $this->user_id;
      $fields['permission'] = $this->permission;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_program_member')
        ->fields($fields)
        ->condition('program_member_id', $this->program_member_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_program_member')
        ->condition('program_member_id', $this->program_member_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the program_member_id.
   *
   * @retrun serial
   */
  public function getProgramMemberID() {
    return $this->program_member_id;
  }

  /**
   * Sets the program_member_id.
   *
   * @param serial $program_member_id
   */
  public function setProgramMemberID($program_member_id) {
    $this->program_member_id = $program_member_id;
  }

  /**
   * Retrieves the program_id.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program_id.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }

  /**
   * Retrieves the user_id.
   *
   * @retrun integer
   */
  public function getUserID() {
    return $this->user_id;
  }

  /**
   * Sets the user_id.
   *
   * @param integer $user_id
   */
  public function setUserID($user_id) {
    $this->user_id = $user_id;
  }

  /**
   * Retrieves the permission.
   *
   * @retrun character_varying(255)
   */
  public function getPermission() {
    return $this->permission;
  }

  /**
   * Sets the permission.
   *
   * @param character_varying(255) $permission
   */
  public function setPermission($permission) {
    $this->permission = $permission;
  }
}