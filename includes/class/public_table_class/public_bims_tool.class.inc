<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_TOOL class.
 *
 */
class PUBLIC_BIMS_TOOL  {

  /**
   *  Data members for PUBLIC_BIMS_TOOL.
   */
  protected $member_arr  = NULL;
  protected $tool_id     = NULL;
  protected $name        = NULL;
  protected $type        = NULL;
  protected $label       = NULL;
  protected $description = NULL;
  protected $version     = NULL;
  protected $profile_num = NULL;
  protected $url         = NULL;
  protected $tool_path   = NULL;
  protected $enabled     = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'tool_id'     => 'serial',
    'name'        => 'character_varying(255)',
    'type'        => 'character_varying(255)',
    'label'       => 'character_varying(255)',
    'description' => 'text',
    'version'     => 'character_varying(50)',
    'profile_num' => 'integer',
    'url'         => 'character_varying(100)',
    'tool_path'   => 'character_varying(255)',
    'enabled'     => 'integer',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'tool_id'     => TRUE,
    'name'        => TRUE,
    'type'        => TRUE,
    'label'       => TRUE,
    'description' => FALSE,
    'version'     => FALSE,
    'profile_num' => TRUE,
    'url'         => FALSE,
    'tool_path'   => FALSE,
    'enabled'     => TRUE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr  = $details;
    $this->tool_id     = array_key_exists('tool_id', $details)     ? $details['tool_id']     : '';
    $this->name        = array_key_exists('name', $details)        ? $details['name']        : '';
    $this->type        = array_key_exists('type', $details)        ? $details['type']        : '';
    $this->label       = array_key_exists('label', $details)       ? $details['label']       : '';
    $this->description = array_key_exists('description', $details) ? $details['description'] : '';
    $this->version     = array_key_exists('version', $details)     ? $details['version']     : '';
    $this->profile_num = array_key_exists('profile_num', $details) ? $details['profile_num'] : 1;
    $this->url         = array_key_exists('url', $details)         ? $details['url']         : '';
    $this->tool_path   = array_key_exists('tool_path', $details)   ? $details['tool_path']   : '';
    $this->enabled     = array_key_exists('enabled', $details)     ? $details['enabled']     : 0;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_TOOL by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_tool WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'tool_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_tool_name_key' => array('name'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['name']        = $this->name;
        $fields['type']        = $this->type;
        $fields['label']       = $this->label;
        $fields['description'] = $this->description;
        $fields['version']     = $this->version;
        $fields['profile_num'] = (is_numeric($this->profile_num)) ? $this->profile_num : 1;
        $fields['url']         = $this->url;
        $fields['tool_path']   = $this->tool_path;
        $fields['enabled']     = (is_numeric($this->enabled))     ? $this->enabled     : 0;


        // Inserts the record.
        $this->tool_id = db_insert('bims_tool')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['name']        = $this->name;
      $fields['type']        = $this->type;
      $fields['label']       = $this->label;
      $fields['description'] = $this->description;
      $fields['version']     = $this->version;
      $fields['profile_num'] = (is_numeric($this->profile_num)) ? $this->profile_num : 1;
      $fields['url']         = $this->url;
      $fields['tool_path']   = $this->tool_path;
      $fields['enabled']     = (is_numeric($this->enabled))     ? $this->enabled     : 0;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_tool')
        ->fields($fields)
        ->condition('tool_id', $this->tool_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_tool')
        ->condition('tool_id', $this->tool_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the tool_id.
   *
   * @retrun serial
   */
  public function getToolID() {
    return $this->tool_id;
  }

  /**
   * Sets the tool_id.
   *
   * @param serial $tool_id
   */
  public function setToolID($tool_id) {
    $this->tool_id = $tool_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(255)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(255) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the label.
   *
   * @retrun character_varying(255)
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Sets the label.
   *
   * @param character_varying(255) $label
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * Retrieves the description.
   *
   * @retrun text
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description.
   *
   * @param text $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Retrieves the version.
   *
   * @retrun character_varying(50)
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * Sets the version.
   *
   * @param character_varying(50) $version
   */
  public function setVersion($version) {
    $this->version = $version;
  }

  /**
   * Retrieves the profile_num.
   *
   * @retrun integer
   */
  public function getProfileNum() {
    return $this->profile_num;
  }

  /**
   * Sets the profile_num.
   *
   * @param integer $profile_num
   */
  public function setProfileNum($profile_num) {
    $this->profile_num = $profile_num;
  }

  /**
   * Retrieves the url.
   *
   * @retrun character_varying(100)
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * Sets the url.
   *
   * @param character_varying(100) $url
   */
  public function setUrl($url) {
    $this->url = $url;
  }

  /**
   * Retrieves the tool_path.
   *
   * @retrun character_varying(255)
   */
  public function getToolPath() {
    return $this->tool_path;
  }

  /**
   * Sets the tool_path.
   *
   * @param character_varying(255) $tool_path
   */
  public function setToolPath($tool_path) {
    $this->tool_path = $tool_path;
  }

  /**
   * Retrieves the enabled.
   *
   * @retrun integer
   */
  public function getEnabled() {
    return $this->enabled;
  }

  /**
   * Sets the enabled.
   *
   * @param integer $enabled
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }
}