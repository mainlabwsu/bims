<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_EVENT class.
 *
 */
class PUBLIC_BIMS_EVENT  {

  /**
   *  Data members for PUBLIC_BIMS_EVENT.
   */
  protected $member_arr = NULL;
  protected $event_id   = NULL;
  protected $type       = NULL;
  protected $event      = NULL;
  protected $user_id    = NULL;
  protected $event_date = NULL;
  protected $prop       = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'event_id'   => 'integer',
    'type'       => 'character_varying(255)',
    'event'      => 'text',
    'user_id'    => 'integer',
    'event_date' => 'timestamp_without_time_zone',
    'prop'       => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'event_id'   => TRUE,
    'type'       => TRUE,
    'event'      => FALSE,
    'user_id'    => TRUE,
    'event_date' => TRUE,
    'prop'       => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr = $details;
    $this->event_id   = array_key_exists('event_id', $details)   ? $details['event_id']   : '';
    $this->type       = array_key_exists('type', $details)       ? $details['type']       : '';
    $this->event      = array_key_exists('event', $details)      ? $details['event']      : '';
    $this->user_id    = array_key_exists('user_id', $details)    ? $details['user_id']    : '';
    $this->event_date = array_key_exists('event_date', $details) ? $details['event_date'] : NULL;
    $this->prop       = array_key_exists('prop', $details)       ? $details['prop']       : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_EVENT by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_event WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return NULL;
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return NULL;
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['event_id']   = $this->event_id;
        $fields['type']       = $this->type;
        $fields['event']      = $this->event;
        $fields['user_id']    = $this->user_id;
        $fields['event_date'] = $this->event_date;
        $fields['prop']       = $this->prop;


        // Inserts the record.
        db_insert('bims_event')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['event_id']   = $this->event_id;
      $fields['type']       = $this->type;
      $fields['event']      = $this->event;
      $fields['user_id']    = $this->user_id;
      $fields['event_date'] = $this->event_date;
      $fields['prop']       = $this->prop;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_event')
        ->fields($fields)

        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_event')

        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the event_id.
   *
   * @retrun integer
   */
  public function getEventID() {
    return $this->event_id;
  }

  /**
   * Sets the event_id.
   *
   * @param integer $event_id
   */
  public function setEventID($event_id) {
    $this->event_id = $event_id;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(255)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(255) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the event.
   *
   * @retrun text
   */
  public function getEvent() {
    return $this->event;
  }

  /**
   * Sets the event.
   *
   * @param text $event
   */
  public function setEvent($event) {
    $this->event = $event;
  }

  /**
   * Retrieves the user_id.
   *
   * @retrun integer
   */
  public function getUserID() {
    return $this->user_id;
  }

  /**
   * Sets the user_id.
   *
   * @param integer $user_id
   */
  public function setUserID($user_id) {
    $this->user_id = $user_id;
  }

  /**
   * Retrieves the event_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getEventDate() {
    return $this->event_date;
  }

  /**
   * Sets the event_date.
   *
   * @param timestamp_without_time_zone $event_date
   */
  public function setEventDate($event_date) {
    $this->event_date = $event_date;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }
}