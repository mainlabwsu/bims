<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_CRON_LOG class.
 *
 */
class PUBLIC_BIMS_CRON_LOG  {

  /**
   *  Data members for PUBLIC_BIMS_CRON_LOG.
   */
  protected $member_arr  = NULL;
  protected $cron_log_id = NULL;
  protected $year        = NULL;
  protected $mon         = NULL;
  protected $wday        = NULL;
  protected $mday        = NULL;
  protected $yday        = NULL;
  protected $hours       = NULL;
  protected $minutes     = NULL;
  protected $seconds     = NULL;
  protected $called_date = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'cron_log_id' => 'serial',
    'year'        => 'integer',
    'mon'         => 'integer',
    'wday'        => 'integer',
    'mday'        => 'integer',
    'yday'        => 'integer',
    'hours'       => 'integer',
    'minutes'     => 'integer',
    'seconds'     => 'integer',
    'called_date' => 'timestamp_without_time_zone',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'cron_log_id' => TRUE,
    'year'        => TRUE,
    'mon'         => TRUE,
    'wday'        => TRUE,
    'mday'        => TRUE,
    'yday'        => TRUE,
    'hours'       => TRUE,
    'minutes'     => TRUE,
    'seconds'     => TRUE,
    'called_date' => TRUE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr  = $details;
    $this->cron_log_id = array_key_exists('cron_log_id', $details) ? $details['cron_log_id'] : '';
    $this->year        = array_key_exists('year', $details)        ? $details['year']        : '';
    $this->mon         = array_key_exists('mon', $details)         ? $details['mon']         : '';
    $this->wday        = array_key_exists('wday', $details)        ? $details['wday']        : '';
    $this->mday        = array_key_exists('mday', $details)        ? $details['mday']        : '';
    $this->yday        = array_key_exists('yday', $details)        ? $details['yday']        : '';
    $this->hours       = array_key_exists('hours', $details)       ? $details['hours']       : '';
    $this->minutes     = array_key_exists('minutes', $details)     ? $details['minutes']     : '';
    $this->seconds     = array_key_exists('seconds', $details)     ? $details['seconds']     : '';
    $this->called_date = array_key_exists('called_date', $details) ? $details['called_date'] : NULL;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_CRON_LOG by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_cron_log WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'cron_log_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return NULL;
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['year']        = $this->year;
        $fields['mon']         = $this->mon;
        $fields['wday']        = $this->wday;
        $fields['mday']        = $this->mday;
        $fields['yday']        = $this->yday;
        $fields['hours']       = $this->hours;
        $fields['minutes']     = $this->minutes;
        $fields['seconds']     = $this->seconds;
        $fields['called_date'] = $this->called_date;


        // Inserts the record.
        $this->cron_log_id = db_insert('bims_cron_log')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['year']        = $this->year;
      $fields['mon']         = $this->mon;
      $fields['wday']        = $this->wday;
      $fields['mday']        = $this->mday;
      $fields['yday']        = $this->yday;
      $fields['hours']       = $this->hours;
      $fields['minutes']     = $this->minutes;
      $fields['seconds']     = $this->seconds;
      $fields['called_date'] = $this->called_date;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_cron_log')
        ->fields($fields)
        ->condition('cron_log_id', $this->cron_log_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_cron_log')
        ->condition('cron_log_id', $this->cron_log_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the cron_log_id.
   *
   * @retrun serial
   */
  public function getCronLogID() {
    return $this->cron_log_id;
  }

  /**
   * Sets the cron_log_id.
   *
   * @param serial $cron_log_id
   */
  public function setCronLogID($cron_log_id) {
    $this->cron_log_id = $cron_log_id;
  }

  /**
   * Retrieves the year.
   *
   * @retrun integer
   */
  public function getYear() {
    return $this->year;
  }

  /**
   * Sets the year.
   *
   * @param integer $year
   */
  public function setYear($year) {
    $this->year = $year;
  }

  /**
   * Retrieves the mon.
   *
   * @retrun integer
   */
  public function getMon() {
    return $this->mon;
  }

  /**
   * Sets the mon.
   *
   * @param integer $mon
   */
  public function setMon($mon) {
    $this->mon = $mon;
  }

  /**
   * Retrieves the wday.
   *
   * @retrun integer
   */
  public function getWday() {
    return $this->wday;
  }

  /**
   * Sets the wday.
   *
   * @param integer $wday
   */
  public function setWday($wday) {
    $this->wday = $wday;
  }

  /**
   * Retrieves the mday.
   *
   * @retrun integer
   */
  public function getMday() {
    return $this->mday;
  }

  /**
   * Sets the mday.
   *
   * @param integer $mday
   */
  public function setMday($mday) {
    $this->mday = $mday;
  }

  /**
   * Retrieves the yday.
   *
   * @retrun integer
   */
  public function getYday() {
    return $this->yday;
  }

  /**
   * Sets the yday.
   *
   * @param integer $yday
   */
  public function setYday($yday) {
    $this->yday = $yday;
  }

  /**
   * Retrieves the hours.
   *
   * @retrun integer
   */
  public function getHours() {
    return $this->hours;
  }

  /**
   * Sets the hours.
   *
   * @param integer $hours
   */
  public function setHours($hours) {
    $this->hours = $hours;
  }

  /**
   * Retrieves the minutes.
   *
   * @retrun integer
   */
  public function getMinutes() {
    return $this->minutes;
  }

  /**
   * Sets the minutes.
   *
   * @param integer $minutes
   */
  public function setMinutes($minutes) {
    $this->minutes = $minutes;
  }

  /**
   * Retrieves the seconds.
   *
   * @retrun integer
   */
  public function getSeconds() {
    return $this->seconds;
  }

  /**
   * Sets the seconds.
   *
   * @param integer $seconds
   */
  public function setSeconds($seconds) {
    $this->seconds = $seconds;
  }

  /**
   * Retrieves the called_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getCalledDate() {
    return $this->called_date;
  }

  /**
   * Sets the called_date.
   *
   * @param timestamp_without_time_zone $called_date
   */
  public function setCalledDate($called_date) {
    $this->called_date = $called_date;
  }
}