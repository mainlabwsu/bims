<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_JOB class.
 *
 */
class PUBLIC_BIMS_JOB  {

  /**
   *  Data members for PUBLIC_BIMS_JOB.
   */
  protected $member_arr    = NULL;
  protected $job_id        = NULL;
  protected $user_id       = NULL;
  protected $name          = NULL;
  protected $tool_id       = NULL;
  protected $resource_id   = NULL;
  protected $exec_id       = NULL;
  protected $status        = NULL;
  protected $submit_date   = NULL;
  protected $complete_date = NULL;
  protected $prop          = NULL;
  protected $program_id    = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'job_id'        => 'serial',
    'user_id'       => 'integer',
    'name'          => 'character_varying(255)',
    'tool_id'       => 'integer',
    'resource_id'   => 'integer',
    'exec_id'       => 'integer',
    'status'        => 'integer',
    'submit_date'   => 'timestamp_without_time_zone',
    'complete_date' => 'timestamp_without_time_zone',
    'prop'          => 'text',
    'program_id'    => 'integer',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'job_id'        => TRUE,
    'user_id'       => TRUE,
    'name'          => TRUE,
    'tool_id'       => FALSE,
    'resource_id'   => FALSE,
    'exec_id'       => FALSE,
    'status'        => TRUE,
    'submit_date'   => TRUE,
    'complete_date' => FALSE,
    'prop'          => FALSE,
    'program_id'    => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr    = $details;
    $this->job_id        = array_key_exists('job_id', $details)        ? $details['job_id']        : '';
    $this->user_id       = array_key_exists('user_id', $details)       ? $details['user_id']       : '';
    $this->name          = array_key_exists('name', $details)          ? $details['name']          : '';
    $this->tool_id       = array_key_exists('tool_id', $details)       ? $details['tool_id']       : '';
    $this->resource_id   = array_key_exists('resource_id', $details)   ? $details['resource_id']   : '';
    $this->exec_id       = array_key_exists('exec_id', $details)       ? $details['exec_id']       : '';
    $this->status        = array_key_exists('status', $details)        ? $details['status']        : 0;
    $this->submit_date   = array_key_exists('submit_date', $details)   ? $details['submit_date']   : NULL;
    $this->complete_date = array_key_exists('complete_date', $details) ? $details['complete_date'] : NULL;
    $this->prop          = array_key_exists('prop', $details)          ? $details['prop']          : '';
    $this->program_id    = array_key_exists('program_id', $details)    ? $details['program_id']    : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_JOB by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_job WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'job_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return NULL;
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['user_id']     = $this->user_id;
        $fields['name']        = $this->name;
        $fields['status']      = (is_numeric($this->status)) ? $this->status : 0;
        $fields['submit_date'] = $this->submit_date;
        $fields['prop']        = $this->prop;
        if (is_numeric($this->tool_id))     { $fields['tool_id']       = $this->tool_id; }
        if (is_numeric($this->resource_id)) { $fields['resource_id']   = $this->resource_id; }
        if (is_numeric($this->exec_id))     { $fields['exec_id']       = $this->exec_id; }
        if (!empty($this->complete_date))   { $fields['complete_date'] = $this->complete_date; }
        if (is_numeric($this->program_id))  { $fields['program_id']    = $this->program_id; }

        // Inserts the record.
        $this->job_id = db_insert('bims_job')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['user_id']     = $this->user_id;
      $fields['name']        = $this->name;
      $fields['status']      = (is_numeric($this->status)) ? $this->status : 0;
      $fields['submit_date'] = $this->submit_date;
      $fields['prop']        = $this->prop;
      if (is_numeric($this->tool_id))     { $fields['tool_id']       = $this->tool_id; }
      if (is_numeric($this->resource_id)) { $fields['resource_id']   = $this->resource_id; }
      if (is_numeric($this->exec_id))     { $fields['exec_id']       = $this->exec_id; }
      if (!empty($this->complete_date))   { $fields['complete_date'] = $this->complete_date; }
      if (is_numeric($this->program_id))  { $fields['program_id']    = $this->program_id; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_job')
        ->fields($fields)
        ->condition('job_id', $this->job_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_job')
        ->condition('job_id', $this->job_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the job_id.
   *
   * @retrun serial
   */
  public function getJobID() {
    return $this->job_id;
  }

  /**
   * Sets the job_id.
   *
   * @param serial $job_id
   */
  public function setJobID($job_id) {
    $this->job_id = $job_id;
  }

  /**
   * Retrieves the user_id.
   *
   * @retrun integer
   */
  public function getUserID() {
    return $this->user_id;
  }

  /**
   * Sets the user_id.
   *
   * @param integer $user_id
   */
  public function setUserID($user_id) {
    $this->user_id = $user_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the tool_id.
   *
   * @retrun integer
   */
  public function getToolID() {
    return $this->tool_id;
  }

  /**
   * Sets the tool_id.
   *
   * @param integer $tool_id
   */
  public function setToolID($tool_id) {
    $this->tool_id = $tool_id;
  }

  /**
   * Retrieves the resource_id.
   *
   * @retrun integer
   */
  public function getResourceID() {
    return $this->resource_id;
  }

  /**
   * Sets the resource_id.
   *
   * @param integer $resource_id
   */
  public function setResourceID($resource_id) {
    $this->resource_id = $resource_id;
  }

  /**
   * Retrieves the exec_id.
   *
   * @retrun integer
   */
  public function getExecID() {
    return $this->exec_id;
  }

  /**
   * Sets the exec_id.
   *
   * @param integer $exec_id
   */
  public function setExecID($exec_id) {
    $this->exec_id = $exec_id;
  }

  /**
   * Retrieves the status.
   *
   * @retrun integer
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Sets the status.
   *
   * @param integer $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * Retrieves the submit_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getSubmitDate() {
    return $this->submit_date;
  }

  /**
   * Sets the submit_date.
   *
   * @param timestamp_without_time_zone $submit_date
   */
  public function setSubmitDate($submit_date) {
    $this->submit_date = $submit_date;
  }

  /**
   * Retrieves the complete_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getCompleteDate() {
    return $this->complete_date;
  }

  /**
   * Sets the complete_date.
   *
   * @param timestamp_without_time_zone $complete_date
   */
  public function setCompleteDate($complete_date) {
    $this->complete_date = $complete_date;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the program_id.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program_id.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }
}