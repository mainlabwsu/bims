<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_IMPORTED_CV class.
 *
 */
class PUBLIC_BIMS_IMPORTED_CV  {

  /**
   *  Data members for PUBLIC_BIMS_IMPORTED_CV.
   */
  protected $member_arr  = NULL;
  protected $program_id  = NULL;
  protected $cv_id       = NULL;
  protected $cv_name     = NULL;
  protected $import_date = NULL;
  protected $prop        = NULL;
  protected $status      = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'program_id'  => 'integer',
    'cv_id'       => 'integer',
    'cv_name'     => 'character_varying(255)',
    'import_date' => 'timestamp_without_time_zone',
    'prop'        => 'text',
    'status'      => 'character_varying(255)',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'program_id'  => TRUE,
    'cv_id'       => TRUE,
    'cv_name'     => TRUE,
    'import_date' => TRUE,
    'prop'        => FALSE,
    'status'      => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr  = $details;
    $this->program_id  = array_key_exists('program_id', $details)  ? $details['program_id']  : '';
    $this->cv_id       = array_key_exists('cv_id', $details)       ? $details['cv_id']       : '';
    $this->cv_name     = array_key_exists('cv_name', $details)     ? $details['cv_name']     : '';
    $this->import_date = array_key_exists('import_date', $details) ? $details['import_date'] : NULL;
    $this->prop        = array_key_exists('prop', $details)        ? $details['prop']        : '';
    $this->status      = array_key_exists('status', $details)      ? $details['status']      : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_IMPORTED_CV by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_imported_cv WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return NULL;
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_imported_cv_ukey_001_key' => array('program_id','cv_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['program_id']  = $this->program_id;
        $fields['cv_id']       = $this->cv_id;
        $fields['cv_name']     = $this->cv_name;
        $fields['import_date'] = $this->import_date;
        $fields['prop']        = $this->prop;
        $fields['status']      = $this->status;


        // Inserts the record.
        db_insert('bims_imported_cv')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['program_id']  = $this->program_id;
      $fields['cv_id']       = $this->cv_id;
      $fields['cv_name']     = $this->cv_name;
      $fields['import_date'] = $this->import_date;
      $fields['prop']        = $this->prop;
      $fields['status']      = $this->status;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_imported_cv')
        ->fields($fields)
        ->condition('program_id', $this->program_id, '=')
        ->condition('cv_id', $this->cv_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_imported_cv')
        ->condition('program_id', $this->program_id, '=')
        ->condition('cv_id', $this->cv_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the program_id.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program_id.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }

  /**
   * Retrieves the cv_id.
   *
   * @retrun integer
   */
  public function getCvID() {
    return $this->cv_id;
  }

  /**
   * Sets the cv_id.
   *
   * @param integer $cv_id
   */
  public function setCvID($cv_id) {
    $this->cv_id = $cv_id;
  }

  /**
   * Retrieves the cv_name.
   *
   * @retrun character_varying(255)
   */
  public function getCvName() {
    return $this->cv_name;
  }

  /**
   * Sets the cv_name.
   *
   * @param character_varying(255) $cv_name
   */
  public function setCvName($cv_name) {
    $this->cv_name = $cv_name;
  }

  /**
   * Retrieves the import_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getImportDate() {
    return $this->import_date;
  }

  /**
   * Sets the import_date.
   *
   * @param timestamp_without_time_zone $import_date
   */
  public function setImportDate($import_date) {
    $this->import_date = $import_date;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the status.
   *
   * @retrun character_varying(255)
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Sets the status.
   *
   * @param character_varying(255) $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }
}