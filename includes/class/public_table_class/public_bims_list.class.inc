<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_LIST class.
 *
 */
class PUBLIC_BIMS_LIST  {

  /**
   *  Data members for PUBLIC_BIMS_LIST.
   */
  protected $member_arr  = NULL;
  protected $list_id     = NULL;
  protected $name        = NULL;
  protected $type        = NULL;
  protected $program_id  = NULL;
  protected $user_id     = NULL;
  protected $shared      = NULL;
  protected $prop        = NULL;
  protected $create_date = NULL;
  protected $update_date = NULL;
  protected $description = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'list_id'     => 'serial',
    'name'        => 'character_varying(255)',
    'type'        => 'character_varying(255)',
    'program_id'  => 'integer',
    'user_id'     => 'integer',
    'shared'      => 'integer',
    'prop'        => 'text',
    'create_date' => 'timestamp_without_time_zone',
    'update_date' => 'timestamp_without_time_zone',
    'description' => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'list_id'     => TRUE,
    'name'        => TRUE,
    'type'        => TRUE,
    'program_id'  => TRUE,
    'user_id'     => TRUE,
    'shared'      => TRUE,
    'prop'        => FALSE,
    'create_date' => TRUE,
    'update_date' => FALSE,
    'description' => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr  = $details;
    $this->list_id     = array_key_exists('list_id', $details)     ? $details['list_id']     : '';
    $this->name        = array_key_exists('name', $details)        ? $details['name']        : '';
    $this->type        = array_key_exists('type', $details)        ? $details['type']        : '';
    $this->program_id  = array_key_exists('program_id', $details)  ? $details['program_id']  : '';
    $this->user_id     = array_key_exists('user_id', $details)     ? $details['user_id']     : '';
    $this->shared      = array_key_exists('shared', $details)      ? $details['shared']      : 0;
    $this->prop        = array_key_exists('prop', $details)        ? $details['prop']        : '';
    $this->create_date = array_key_exists('create_date', $details) ? $details['create_date'] : NULL;
    $this->update_date = array_key_exists('update_date', $details) ? $details['update_date'] : NULL;
    $this->description = array_key_exists('description', $details) ? $details['description'] : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_LIST by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_list WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'list_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_list_ukey_bims_list_name_type_program_id_user_id_key' => array('name','type','program_id','user_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['name']        = $this->name;
        $fields['type']        = $this->type;
        $fields['program_id']  = $this->program_id;
        $fields['user_id']     = $this->user_id;
        $fields['shared']      = (is_numeric($this->shared)) ? $this->shared : 0;
        $fields['prop']        = $this->prop;
        $fields['create_date'] = $this->create_date;
        $fields['description'] = $this->description;
        if (!empty($this->update_date)) { $fields['update_date'] = $this->update_date; }

        // Inserts the record.
        $this->list_id = db_insert('bims_list')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['name']        = $this->name;
      $fields['type']        = $this->type;
      $fields['program_id']  = $this->program_id;
      $fields['user_id']     = $this->user_id;
      $fields['shared']      = (is_numeric($this->shared)) ? $this->shared : 0;
      $fields['prop']        = $this->prop;
      $fields['create_date'] = $this->create_date;
      $fields['description'] = $this->description;
      if (!empty($this->update_date)) { $fields['update_date'] = $this->update_date; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_list')
        ->fields($fields)
        ->condition('list_id', $this->list_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_list')
        ->condition('list_id', $this->list_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the list_id.
   *
   * @retrun serial
   */
  public function getListID() {
    return $this->list_id;
  }

  /**
   * Sets the list_id.
   *
   * @param serial $list_id
   */
  public function setListID($list_id) {
    $this->list_id = $list_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(255)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(255) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the program_id.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program_id.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }

  /**
   * Retrieves the user_id.
   *
   * @retrun integer
   */
  public function getUserID() {
    return $this->user_id;
  }

  /**
   * Sets the user_id.
   *
   * @param integer $user_id
   */
  public function setUserID($user_id) {
    $this->user_id = $user_id;
  }

  /**
   * Retrieves the shared.
   *
   * @retrun integer
   */
  public function getShared() {
    return $this->shared;
  }

  /**
   * Sets the shared.
   *
   * @param integer $shared
   */
  public function setShared($shared) {
    $this->shared = $shared;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the create_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getCreateDate() {
    return $this->create_date;
  }

  /**
   * Sets the create_date.
   *
   * @param timestamp_without_time_zone $create_date
   */
  public function setCreateDate($create_date) {
    $this->create_date = $create_date;
  }

  /**
   * Retrieves the update_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getUpdateDate() {
    return $this->update_date;
  }

  /**
   * Sets the update_date.
   *
   * @param timestamp_without_time_zone $update_date
   */
  public function setUpdateDate($update_date) {
    $this->update_date = $update_date;
  }

  /**
   * Retrieves the description.
   *
   * @retrun text
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description.
   *
   * @param text $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }
}