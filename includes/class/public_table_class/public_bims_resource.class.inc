<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_RESOURCE class.
 *
 */
class PUBLIC_BIMS_RESOURCE  {

  /**
   *  Data members for PUBLIC_BIMS_RESOURCE.
   */
  protected $member_arr  = NULL;
  protected $resource_id = NULL;
  protected $name        = NULL;
  protected $type        = NULL;
  protected $max_slots   = NULL;
  protected $rank        = NULL;
  protected $description = NULL;
  protected $enabled     = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'resource_id' => 'serial',
    'name'        => 'character_varying(255)',
    'type'        => 'character_varying(255)',
    'max_slots'   => 'integer',
    'rank'        => 'integer',
    'description' => 'text',
    'enabled'     => 'integer',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'resource_id' => TRUE,
    'name'        => TRUE,
    'type'        => TRUE,
    'max_slots'   => TRUE,
    'rank'        => TRUE,
    'description' => FALSE,
    'enabled'     => TRUE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr  = $details;
    $this->resource_id = array_key_exists('resource_id', $details) ? $details['resource_id'] : '';
    $this->name        = array_key_exists('name', $details)        ? $details['name']        : '';
    $this->type        = array_key_exists('type', $details)        ? $details['type']        : '';
    $this->max_slots   = array_key_exists('max_slots', $details)   ? $details['max_slots']   : 1;
    $this->rank        = array_key_exists('rank', $details)        ? $details['rank']        : 1;
    $this->description = array_key_exists('description', $details) ? $details['description'] : '';
    $this->enabled     = array_key_exists('enabled', $details)     ? $details['enabled']     : 0;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_RESOURCE by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_resource WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'resource_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_resource_ukey_001_key' => array('name'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['name']        = $this->name;
        $fields['type']        = $this->type;
        $fields['max_slots']   = (is_numeric($this->max_slots)) ? $this->max_slots : 1;
        $fields['rank']        = (is_numeric($this->rank))      ? $this->rank      : 1;
        $fields['description'] = $this->description;
        $fields['enabled']     = (is_numeric($this->enabled))   ? $this->enabled   : 0;


        // Inserts the record.
        $this->resource_id = db_insert('bims_resource')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['name']        = $this->name;
      $fields['type']        = $this->type;
      $fields['max_slots']   = (is_numeric($this->max_slots)) ? $this->max_slots : 1;
      $fields['rank']        = (is_numeric($this->rank))      ? $this->rank      : 1;
      $fields['description'] = $this->description;
      $fields['enabled']     = (is_numeric($this->enabled))   ? $this->enabled   : 0;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_resource')
        ->fields($fields)
        ->condition('resource_id', $this->resource_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_resource')
        ->condition('resource_id', $this->resource_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the resource_id.
   *
   * @retrun serial
   */
  public function getResourceID() {
    return $this->resource_id;
  }

  /**
   * Sets the resource_id.
   *
   * @param serial $resource_id
   */
  public function setResourceID($resource_id) {
    $this->resource_id = $resource_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(255)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(255) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the max_slots.
   *
   * @retrun integer
   */
  public function getMaxSlots() {
    return $this->max_slots;
  }

  /**
   * Sets the max_slots.
   *
   * @param integer $max_slots
   */
  public function setMaxSlots($max_slots) {
    $this->max_slots = $max_slots;
  }

  /**
   * Retrieves the rank.
   *
   * @retrun integer
   */
  public function getRank() {
    return $this->rank;
  }

  /**
   * Sets the rank.
   *
   * @param integer $rank
   */
  public function setRank($rank) {
    $this->rank = $rank;
  }

  /**
   * Retrieves the description.
   *
   * @retrun text
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description.
   *
   * @param text $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Retrieves the enabled.
   *
   * @retrun integer
   */
  public function getEnabled() {
    return $this->enabled;
  }

  /**
   * Sets the enabled.
   *
   * @param integer $enabled
   */
  public function setEnabled($enabled) {
    $this->enabled = $enabled;
  }
}