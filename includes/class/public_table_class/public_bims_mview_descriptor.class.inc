<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_MVIEW_DESCRIPTOR class.
 *
 */
class PUBLIC_BIMS_MVIEW_DESCRIPTOR  {

  /**
   *  Data members for PUBLIC_BIMS_MVIEW_DESCRIPTOR.
   */
  protected $member_arr = NULL;
  protected $cv_id      = NULL;
  protected $category   = NULL;
  protected $name       = NULL;
  protected $cvterm_id  = NULL;
  protected $alias      = NULL;
  protected $format     = NULL;
  protected $prop       = NULL;
  protected $definition = NULL;
  protected $chado      = NULL;
  protected $program_id = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'cv_id'      => 'integer',
    'category'   => 'character_varying(255)',
    'name'       => 'character_varying(255)',
    'cvterm_id'  => 'integer',
    'alias'      => 'character_varying(255)',
    'format'     => 'character_varying(255)',
    'prop'       => 'text',
    'definition' => 'text',
    'chado'      => 'integer',
    'program_id' => 'integer',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'cv_id'      => TRUE,
    'category'   => FALSE,
    'name'       => TRUE,
    'cvterm_id'  => TRUE,
    'alias'      => FALSE,
    'format'     => FALSE,
    'prop'       => FALSE,
    'definition' => FALSE,
    'chado'      => TRUE,
    'program_id' => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr = $details;
    $this->cv_id      = array_key_exists('cv_id', $details)      ? $details['cv_id']      : '';
    $this->category   = array_key_exists('category', $details)   ? $details['category']   : '';
    $this->name       = array_key_exists('name', $details)       ? $details['name']       : '';
    $this->cvterm_id  = array_key_exists('cvterm_id', $details)  ? $details['cvterm_id']  : '';
    $this->alias      = array_key_exists('alias', $details)      ? $details['alias']      : '';
    $this->format     = array_key_exists('format', $details)     ? $details['format']     : '';
    $this->prop       = array_key_exists('prop', $details)       ? $details['prop']       : '';
    $this->definition = array_key_exists('definition', $details) ? $details['definition'] : '';
    $this->chado      = array_key_exists('chado', $details)      ? $details['chado']      : 0;
    $this->program_id = array_key_exists('program_id', $details) ? $details['program_id'] : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_MVIEW_DESCRIPTOR by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_mview_descriptor WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return NULL;
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_mview_descriptor_bims_mview_descriptor_ukey_001_key' => array('program_id','cvterm_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['cv_id']      = $this->cv_id;
        $fields['category']   = $this->category;
        $fields['name']       = $this->name;
        $fields['cvterm_id']  = $this->cvterm_id;
        $fields['alias']      = $this->alias;
        $fields['format']     = $this->format;
        $fields['prop']       = $this->prop;
        $fields['definition'] = $this->definition;
        $fields['chado']      = (is_numeric($this->chado)) ? $this->chado : 0;
        if (is_numeric($this->program_id)) { $fields['program_id'] = $this->program_id; }

        // Inserts the record.
        db_insert('bims_mview_descriptor')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['cv_id']      = $this->cv_id;
      $fields['category']   = $this->category;
      $fields['name']       = $this->name;
      $fields['cvterm_id']  = $this->cvterm_id;
      $fields['alias']      = $this->alias;
      $fields['format']     = $this->format;
      $fields['prop']       = $this->prop;
      $fields['definition'] = $this->definition;
      $fields['chado']      = (is_numeric($this->chado)) ? $this->chado : 0;
      if (is_numeric($this->program_id)) { $fields['program_id'] = $this->program_id; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_mview_descriptor')
        ->fields($fields)
        ->condition('program_id', $this->program_id, '=')
        ->condition('cvterm_id', $this->cvterm_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_mview_descriptor')
        ->condition('program_id', $this->program_id, '=')
        ->condition('cvterm_id', $this->cvterm_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the cv_id.
   *
   * @retrun integer
   */
  public function getCvID() {
    return $this->cv_id;
  }

  /**
   * Sets the cv_id.
   *
   * @param integer $cv_id
   */
  public function setCvID($cv_id) {
    $this->cv_id = $cv_id;
  }

  /**
   * Retrieves the category.
   *
   * @retrun character_varying(255)
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * Sets the category.
   *
   * @param character_varying(255) $category
   */
  public function setCategory($category) {
    $this->category = $category;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the cvterm_id.
   *
   * @retrun integer
   */
  public function getCvtermID() {
    return $this->cvterm_id;
  }

  /**
   * Sets the cvterm_id.
   *
   * @param integer $cvterm_id
   */
  public function setCvtermID($cvterm_id) {
    $this->cvterm_id = $cvterm_id;
  }

  /**
   * Retrieves the alias.
   *
   * @retrun character_varying(255)
   */
  public function getAlias() {
    return $this->alias;
  }

  /**
   * Sets the alias.
   *
   * @param character_varying(255) $alias
   */
  public function setAlias($alias) {
    $this->alias = $alias;
  }

  /**
   * Retrieves the format.
   *
   * @retrun character_varying(255)
   */
  public function getFormat() {
    return $this->format;
  }

  /**
   * Sets the format.
   *
   * @param character_varying(255) $format
   */
  public function setFormat($format) {
    $this->format = $format;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the definition.
   *
   * @retrun text
   */
  public function getDefinition() {
    return $this->definition;
  }

  /**
   * Sets the definition.
   *
   * @param text $definition
   */
  public function setDefinition($definition) {
    $this->definition = $definition;
  }

  /**
   * Retrieves the chado.
   *
   * @retrun integer
   */
  public function getChado() {
    return $this->chado;
  }

  /**
   * Sets the chado.
   *
   * @param integer $chado
   */
  public function setChado($chado) {
    $this->chado = $chado;
  }

  /**
   * Retrieves the program_id.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program_id.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }
}