<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_ENVIRONMENT class.
 *
 */
class PUBLIC_BIMS_ENVIRONMENT  {

  /**
   *  Data members for PUBLIC_BIMS_ENVIRONMENT.
   */
  protected $member_arr     = NULL;
  protected $environment_id = NULL;
  protected $bdata_id       = NULL;
  protected $name           = NULL;
  protected $description    = NULL;
  protected $prop           = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'environment_id' => 'serial',
    'bdata_id'       => 'integer',
    'name'           => 'character_varying(255)',
    'description'    => 'text',
    'prop'           => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'environment_id' => TRUE,
    'bdata_id'       => TRUE,
    'name'           => TRUE,
    'description'    => FALSE,
    'prop'           => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr     = $details;
    $this->environment_id = array_key_exists('environment_id', $details) ? $details['environment_id'] : '';
    $this->bdata_id       = array_key_exists('bdata_id', $details)       ? $details['bdata_id']       : '';
    $this->name           = array_key_exists('name', $details)           ? $details['name']           : '';
    $this->description    = array_key_exists('description', $details)    ? $details['description']    : '';
    $this->prop           = array_key_exists('prop', $details)           ? $details['prop']           : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_ENVIRONMENT by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_environment WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'environment_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_environment_ukey_001_key' => array('bdata_id','name'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['bdata_id']    = $this->bdata_id;
        $fields['name']        = $this->name;
        $fields['description'] = $this->description;
        $fields['prop']        = $this->prop;


        // Inserts the record.
        $this->environment_id = db_insert('bims_environment')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['bdata_id']    = $this->bdata_id;
      $fields['name']        = $this->name;
      $fields['description'] = $this->description;
      $fields['prop']        = $this->prop;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_environment')
        ->fields($fields)
        ->condition('environment_id', $this->environment_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_environment')
        ->condition('environment_id', $this->environment_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the environment_id.
   *
   * @retrun serial
   */
  public function getEnvironmentID() {
    return $this->environment_id;
  }

  /**
   * Sets the environment_id.
   *
   * @param serial $environment_id
   */
  public function setEnvironmentID($environment_id) {
    $this->environment_id = $environment_id;
  }

  /**
   * Retrieves the bdata_id.
   *
   * @retrun integer
   */
  public function getBdataID() {
    return $this->bdata_id;
  }

  /**
   * Sets the bdata_id.
   *
   * @param integer $bdata_id
   */
  public function setBdataID($bdata_id) {
    $this->bdata_id = $bdata_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the description.
   *
   * @retrun text
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description.
   *
   * @param text $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }
}