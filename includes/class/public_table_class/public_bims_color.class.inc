<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_COLOR class.
 *
 */
class PUBLIC_BIMS_COLOR  {

  /**
   *  Data members for PUBLIC_BIMS_COLOR.
   */
  protected $member_arr = NULL;
  protected $color_id   = NULL;
  protected $type       = NULL;
  protected $code       = NULL;
  protected $name       = NULL;
  protected $rgb        = NULL;
  protected $rank       = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'color_id' => 'serial',
    'type'     => 'character_varying(255)',
    'code'     => 'character_varying(255)',
    'name'     => 'character_varying(255)',
    'rgb'      => 'character_varying(255)',
    'rank'     => 'integer',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'color_id' => TRUE,
    'type'     => TRUE,
    'code'     => TRUE,
    'name'     => FALSE,
    'rgb'      => FALSE,
    'rank'     => TRUE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr = $details;
    $this->color_id   = array_key_exists('color_id', $details) ? $details['color_id'] : '';
    $this->type       = array_key_exists('type', $details)     ? $details['type']     : '';
    $this->code       = array_key_exists('code', $details)     ? $details['code']     : '';
    $this->name       = array_key_exists('name', $details)     ? $details['name']     : '';
    $this->rgb        = array_key_exists('rgb', $details)      ? $details['rgb']      : '';
    $this->rank       = array_key_exists('rank', $details)     ? $details['rank']     : 0;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_COLOR by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_color WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'color_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_color_ukey_bims_color_001_key' => array('type','code'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['type'] = $this->type;
        $fields['code'] = $this->code;
        $fields['name'] = $this->name;
        $fields['rgb']  = $this->rgb;
        $fields['rank'] = (is_numeric($this->rank)) ? $this->rank : 0;


        // Inserts the record.
        $this->color_id = db_insert('bims_color')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['type'] = $this->type;
      $fields['code'] = $this->code;
      $fields['name'] = $this->name;
      $fields['rgb']  = $this->rgb;
      $fields['rank'] = (is_numeric($this->rank)) ? $this->rank : 0;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_color')
        ->fields($fields)
        ->condition('color_id', $this->color_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_color')
        ->condition('color_id', $this->color_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the color_id.
   *
   * @retrun serial
   */
  public function getColorID() {
    return $this->color_id;
  }

  /**
   * Sets the color_id.
   *
   * @param serial $color_id
   */
  public function setColorID($color_id) {
    $this->color_id = $color_id;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(255)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(255) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the code.
   *
   * @retrun character_varying(255)
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * Sets the code.
   *
   * @param character_varying(255) $code
   */
  public function setCode($code) {
    $this->code = $code;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the rgb.
   *
   * @retrun character_varying(255)
   */
  public function getRgb() {
    return $this->rgb;
  }

  /**
   * Sets the rgb.
   *
   * @param character_varying(255) $rgb
   */
  public function setRgb($rgb) {
    $this->rgb = $rgb;
  }

  /**
   * Retrieves the rank.
   *
   * @retrun integer
   */
  public function getRank() {
    return $this->rank;
  }

  /**
   * Sets the rank.
   *
   * @param integer $rank
   */
  public function setRank($rank) {
    $this->rank = $rank;
  }
}