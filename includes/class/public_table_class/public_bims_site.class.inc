<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_SITE class.
 *
 */
class PUBLIC_BIMS_SITE  {

  /**
   *  Data members for PUBLIC_BIMS_SITE.
   */
  protected $member_arr = NULL;
  protected $site_id    = NULL;
  protected $label      = NULL;
  protected $type       = NULL;
  protected $latitude   = NULL;
  protected $longitude  = NULL;
  protected $icon       = NULL;
  protected $prop       = NULL;
  protected $notes      = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'site_id'   => 'serial',
    'label'     => 'character_varying(255)',
    'type'      => 'character_varying(255)',
    'latitude'  => 'real',
    'longitude' => 'real',
    'icon'      => 'character_varying(255)',
    'prop'      => 'text',
    'notes'     => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'site_id'   => TRUE,
    'label'     => TRUE,
    'type'      => TRUE,
    'latitude'  => TRUE,
    'longitude' => TRUE,
    'icon'      => FALSE,
    'prop'      => FALSE,
    'notes'     => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr = $details;
    $this->site_id    = array_key_exists('site_id', $details)   ? $details['site_id']   : '';
    $this->label      = array_key_exists('label', $details)     ? $details['label']     : '';
    $this->type       = array_key_exists('type', $details)      ? $details['type']      : '';
    $this->latitude   = array_key_exists('latitude', $details)  ? $details['latitude']  : '';
    $this->longitude  = array_key_exists('longitude', $details) ? $details['longitude'] : '';
    $this->icon       = array_key_exists('icon', $details)      ? $details['icon']      : '';
    $this->prop       = array_key_exists('prop', $details)      ? $details['prop']      : '';
    $this->notes      = array_key_exists('notes', $details)     ? $details['notes']     : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_SITE by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_site WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'site_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return NULL;
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['label']     = $this->label;
        $fields['type']      = $this->type;
        $fields['latitude']  = $this->latitude;
        $fields['longitude'] = $this->longitude;
        $fields['icon']      = $this->icon;
        $fields['prop']      = $this->prop;
        $fields['notes']     = $this->notes;


        // Inserts the record.
        $this->site_id = db_insert('bims_site')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['label']     = $this->label;
      $fields['type']      = $this->type;
      $fields['latitude']  = $this->latitude;
      $fields['longitude'] = $this->longitude;
      $fields['icon']      = $this->icon;
      $fields['prop']      = $this->prop;
      $fields['notes']     = $this->notes;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_site')
        ->fields($fields)
        ->condition('site_id', $this->site_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_site')
        ->condition('site_id', $this->site_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the site_id.
   *
   * @retrun serial
   */
  public function getSiteID() {
    return $this->site_id;
  }

  /**
   * Sets the site_id.
   *
   * @param serial $site_id
   */
  public function setSiteID($site_id) {
    $this->site_id = $site_id;
  }

  /**
   * Retrieves the label.
   *
   * @retrun character_varying(255)
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Sets the label.
   *
   * @param character_varying(255) $label
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(255)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(255) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the latitude.
   *
   * @retrun real
   */
  public function getLatitude() {
    return $this->latitude;
  }

  /**
   * Sets the latitude.
   *
   * @param real $latitude
   */
  public function setLatitude($latitude) {
    $this->latitude = $latitude;
  }

  /**
   * Retrieves the longitude.
   *
   * @retrun real
   */
  public function getLongitude() {
    return $this->longitude;
  }

  /**
   * Sets the longitude.
   *
   * @param real $longitude
   */
  public function setLongitude($longitude) {
    $this->longitude = $longitude;
  }

  /**
   * Retrieves the icon.
   *
   * @retrun character_varying(255)
   */
  public function getIcon() {
    return $this->icon;
  }

  /**
   * Sets the icon.
   *
   * @param character_varying(255) $icon
   */
  public function setIcon($icon) {
    $this->icon = $icon;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the notes.
   *
   * @retrun text
   */
  public function getNotes() {
    return $this->notes;
  }

  /**
   * Sets the notes.
   *
   * @param text $notes
   */
  public function setNotes($notes) {
    $this->notes = $notes;
  }
}