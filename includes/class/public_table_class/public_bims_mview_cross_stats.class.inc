<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_MVIEW_CROSS_STATS class.
 *
 */
class PUBLIC_BIMS_MVIEW_CROSS_STATS  {

  /**
   *  Data members for PUBLIC_BIMS_MVIEW_CROSS_STATS.
   */
  protected $member_arr       = NULL;
  protected $node_id          = NULL;
  protected $nd_experiment_id = NULL;
  protected $name             = NULL;
  protected $cvterm_id        = NULL;
  protected $num_data         = NULL;
  protected $stats            = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'node_id'          => 'integer',
    'nd_experiment_id' => 'integer',
    'name'             => 'character_varying(255)',
    'cvterm_id'        => 'integer',
    'num_data'         => 'integer',
    'stats'            => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'node_id'          => TRUE,
    'nd_experiment_id' => TRUE,
    'name'             => TRUE,
    'cvterm_id'        => TRUE,
    'num_data'         => TRUE,
    'stats'            => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr       = $details;
    $this->node_id          = array_key_exists('node_id', $details)          ? $details['node_id']          : '';
    $this->nd_experiment_id = array_key_exists('nd_experiment_id', $details) ? $details['nd_experiment_id'] : '';
    $this->name             = array_key_exists('name', $details)             ? $details['name']             : '';
    $this->cvterm_id        = array_key_exists('cvterm_id', $details)        ? $details['cvterm_id']        : '';
    $this->num_data         = array_key_exists('num_data', $details)         ? $details['num_data']         : 0;
    $this->stats            = array_key_exists('stats', $details)            ? $details['stats']            : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_MVIEW_CROSS_STATS by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_mview_cross_stats WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return NULL;
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_mview_cross_stats_ukey_bims_mview_cross_stats_node_id_nd_e' => array('node_id','nd_experiment_id','cvterm_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['node_id']          = $this->node_id;
        $fields['nd_experiment_id'] = $this->nd_experiment_id;
        $fields['name']             = $this->name;
        $fields['cvterm_id']        = $this->cvterm_id;
        $fields['num_data']         = (is_numeric($this->num_data)) ? $this->num_data : 0;
        $fields['stats']            = $this->stats;


        // Inserts the record.
        db_insert('bims_mview_cross_stats')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['node_id']          = $this->node_id;
      $fields['nd_experiment_id'] = $this->nd_experiment_id;
      $fields['name']             = $this->name;
      $fields['cvterm_id']        = $this->cvterm_id;
      $fields['num_data']         = (is_numeric($this->num_data)) ? $this->num_data : 0;
      $fields['stats']            = $this->stats;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_mview_cross_stats')
        ->fields($fields)
        ->condition('node_id', $this->node_id, '=')
        ->condition('nd_experiment_id', $this->nd_experiment_id, '=')
        ->condition('cvterm_id', $this->cvterm_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_mview_cross_stats')
        ->condition('node_id', $this->node_id, '=')
        ->condition('nd_experiment_id', $this->nd_experiment_id, '=')
        ->condition('cvterm_id', $this->cvterm_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the node_id.
   *
   * @retrun integer
   */
  public function getNodeID() {
    return $this->node_id;
  }

  /**
   * Sets the node_id.
   *
   * @param integer $node_id
   */
  public function setNodeID($node_id) {
    $this->node_id = $node_id;
  }

  /**
   * Retrieves the nd_experiment_id.
   *
   * @retrun integer
   */
  public function getNdExperimentID() {
    return $this->nd_experiment_id;
  }

  /**
   * Sets the nd_experiment_id.
   *
   * @param integer $nd_experiment_id
   */
  public function setNdExperimentID($nd_experiment_id) {
    $this->nd_experiment_id = $nd_experiment_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the cvterm_id.
   *
   * @retrun integer
   */
  public function getCvtermID() {
    return $this->cvterm_id;
  }

  /**
   * Sets the cvterm_id.
   *
   * @param integer $cvterm_id
   */
  public function setCvtermID($cvterm_id) {
    $this->cvterm_id = $cvterm_id;
  }

  /**
   * Retrieves the num_data.
   *
   * @retrun integer
   */
  public function getNumData() {
    return $this->num_data;
  }

  /**
   * Sets the num_data.
   *
   * @param integer $num_data
   */
  public function setNumData($num_data) {
    $this->num_data = $num_data;
  }

  /**
   * Retrieves the stats.
   *
   * @retrun text
   */
  public function getStats() {
    return $this->stats;
  }

  /**
   * Sets the stats.
   *
   * @param text $stats
   */
  public function setStats($stats) {
    $this->stats = $stats;
  }
}