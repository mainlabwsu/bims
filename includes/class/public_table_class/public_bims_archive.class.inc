<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_ARCHIVE class.
 *
 */
class PUBLIC_BIMS_ARCHIVE  {

  /**
   *  Data members for PUBLIC_BIMS_ARCHIVE.
   */
  protected $member_arr     = NULL;
  protected $archive_id     = NULL;
  protected $name           = NULL;
  protected $params         = NULL;
  protected $download_files = NULL;
  protected $program_id     = NULL;
  protected $user_id        = NULL;
  protected $description    = NULL;
  protected $status         = NULL;
  protected $submit_date    = NULL;
  protected $complete_date  = NULL;
  protected $prop           = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'archive_id'     => 'serial',
    'name'           => 'character_varying(255)',
    'params'         => 'text',
    'download_files' => 'text',
    'program_id'     => 'integer',
    'user_id'        => 'integer',
    'description'    => 'text',
    'status'         => 'integer',
    'submit_date'    => 'timestamp_without_time_zone',
    'complete_date'  => 'timestamp_without_time_zone',
    'prop'           => 'text',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'archive_id'     => TRUE,
    'name'           => TRUE,
    'params'         => FALSE,
    'download_files' => FALSE,
    'program_id'     => TRUE,
    'user_id'        => TRUE,
    'description'    => FALSE,
    'status'         => TRUE,
    'submit_date'    => TRUE,
    'complete_date'  => FALSE,
    'prop'           => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr     = $details;
    $this->archive_id     = array_key_exists('archive_id', $details)     ? $details['archive_id']     : '';
    $this->name           = array_key_exists('name', $details)           ? $details['name']           : '';
    $this->params         = array_key_exists('params', $details)         ? $details['params']         : '';
    $this->download_files = array_key_exists('download_files', $details) ? $details['download_files'] : '';
    $this->program_id     = array_key_exists('program_id', $details)     ? $details['program_id']     : '';
    $this->user_id        = array_key_exists('user_id', $details)        ? $details['user_id']        : '';
    $this->description    = array_key_exists('description', $details)    ? $details['description']    : '';
    $this->status         = array_key_exists('status', $details)         ? $details['status']         : 0;
    $this->submit_date    = array_key_exists('submit_date', $details)    ? $details['submit_date']    : NULL;
    $this->complete_date  = array_key_exists('complete_date', $details)  ? $details['complete_date']  : NULL;
    $this->prop           = array_key_exists('prop', $details)           ? $details['prop']           : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_ARCHIVE by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_archive WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'archive_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_archive_bims_archive_ukey_001_key' => array('program_id','user_id','submit_date'),
      'bims_archive_ukey_001_key' => array('name'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['name']           = $this->name;
        $fields['params']         = $this->params;
        $fields['download_files'] = $this->download_files;
        $fields['program_id']     = $this->program_id;
        $fields['user_id']        = $this->user_id;
        $fields['description']    = $this->description;
        $fields['status']         = (is_numeric($this->status)) ? $this->status : 0;
        $fields['submit_date']    = $this->submit_date;
        $fields['prop']           = $this->prop;
        if (!empty($this->complete_date)) { $fields['complete_date'] = $this->complete_date; }

        // Inserts the record.
        $this->archive_id = db_insert('bims_archive')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['name']           = $this->name;
      $fields['params']         = $this->params;
      $fields['download_files'] = $this->download_files;
      $fields['program_id']     = $this->program_id;
      $fields['user_id']        = $this->user_id;
      $fields['description']    = $this->description;
      $fields['status']         = (is_numeric($this->status)) ? $this->status : 0;
      $fields['submit_date']    = $this->submit_date;
      $fields['prop']           = $this->prop;
      if (!empty($this->complete_date)) { $fields['complete_date'] = $this->complete_date; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_archive')
        ->fields($fields)
        ->condition('archive_id', $this->archive_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_archive')
        ->condition('archive_id', $this->archive_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the archive_id.
   *
   * @retrun serial
   */
  public function getArchiveID() {
    return $this->archive_id;
  }

  /**
   * Sets the archive_id.
   *
   * @param serial $archive_id
   */
  public function setArchiveID($archive_id) {
    $this->archive_id = $archive_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the params.
   *
   * @retrun text
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * Sets the params.
   *
   * @param text $params
   */
  public function setParams($params) {
    $this->params = $params;
  }

  /**
   * Retrieves the download_files.
   *
   * @retrun text
   */
  public function getDownloadFiles() {
    return $this->download_files;
  }

  /**
   * Sets the download_files.
   *
   * @param text $download_files
   */
  public function setDownloadFiles($download_files) {
    $this->download_files = $download_files;
  }

  /**
   * Retrieves the program_id.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program_id.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }

  /**
   * Retrieves the user_id.
   *
   * @retrun integer
   */
  public function getUserID() {
    return $this->user_id;
  }

  /**
   * Sets the user_id.
   *
   * @param integer $user_id
   */
  public function setUserID($user_id) {
    $this->user_id = $user_id;
  }

  /**
   * Retrieves the description.
   *
   * @retrun text
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description.
   *
   * @param text $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Retrieves the status.
   *
   * @retrun integer
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Sets the status.
   *
   * @param integer $status
   */
  public function setStatus($status) {
    $this->status = $status;
  }

  /**
   * Retrieves the submit_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getSubmitDate() {
    return $this->submit_date;
  }

  /**
   * Sets the submit_date.
   *
   * @param timestamp_without_time_zone $submit_date
   */
  public function setSubmitDate($submit_date) {
    $this->submit_date = $submit_date;
  }

  /**
   * Retrieves the complete_date.
   *
   * @retrun timestamp_without_time_zone
   */
  public function getCompleteDate() {
    return $this->complete_date;
  }

  /**
   * Sets the complete_date.
   *
   * @param timestamp_without_time_zone $complete_date
   */
  public function setCompleteDate($complete_date) {
    $this->complete_date = $complete_date;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }
}