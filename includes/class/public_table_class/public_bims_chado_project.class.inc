<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_CHADO_PROJECT class.
 *
 */
class PUBLIC_BIMS_CHADO_PROJECT  {

  /**
   *  Data members for PUBLIC_BIMS_CHADO_PROJECT.
   */
  protected $member_arr   = NULL;
  protected $type         = NULL;
  protected $sub_type     = NULL;
  protected $cv_id        = NULL;
  protected $cv_name      = NULL;
  protected $project_id   = NULL;
  protected $project_name = NULL;
  protected $crop_id      = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'type'         => 'character_varying(255)',
    'sub_type'     => 'character_varying(255)',
    'cv_id'        => 'integer',
    'cv_name'      => 'character_varying(255)',
    'project_id'   => 'integer',
    'project_name' => 'character_varying(255)',
    'crop_id'      => 'integer',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'type'         => TRUE,
    'sub_type'     => FALSE,
    'cv_id'        => FALSE,
    'cv_name'      => FALSE,
    'project_id'   => TRUE,
    'project_name' => TRUE,
    'crop_id'      => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr   = $details;
    $this->type         = array_key_exists('type', $details)         ? $details['type']         : '';
    $this->sub_type     = array_key_exists('sub_type', $details)     ? $details['sub_type']     : '';
    $this->cv_id        = array_key_exists('cv_id', $details)        ? $details['cv_id']        : '';
    $this->cv_name      = array_key_exists('cv_name', $details)      ? $details['cv_name']      : '';
    $this->project_id   = array_key_exists('project_id', $details)   ? $details['project_id']   : '';
    $this->project_name = array_key_exists('project_name', $details) ? $details['project_name'] : '';
    $this->crop_id      = array_key_exists('crop_id', $details)      ? $details['crop_id']      : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_CHADO_PROJECT by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_chado_project WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return NULL;
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_chado_project_ukey_001_key' => array('project_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['type']         = $this->type;
        $fields['sub_type']     = $this->sub_type;
        $fields['cv_name']      = $this->cv_name;
        $fields['project_id']   = $this->project_id;
        $fields['project_name'] = $this->project_name;
        if (is_numeric($this->cv_id))   { $fields['cv_id']   = $this->cv_id; }
        if (is_numeric($this->crop_id)) { $fields['crop_id'] = $this->crop_id; }

        // Inserts the record.
        db_insert('bims_chado_project')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['type']         = $this->type;
      $fields['sub_type']     = $this->sub_type;
      $fields['cv_name']      = $this->cv_name;
      $fields['project_id']   = $this->project_id;
      $fields['project_name'] = $this->project_name;
      if (is_numeric($this->cv_id))   { $fields['cv_id']   = $this->cv_id; }
      if (is_numeric($this->crop_id)) { $fields['crop_id'] = $this->crop_id; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_chado_project')
        ->fields($fields)
        ->condition('project_id', $this->project_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_chado_project')
        ->condition('project_id', $this->project_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(255)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(255) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the sub_type.
   *
   * @retrun character_varying(255)
   */
  public function getSubType() {
    return $this->sub_type;
  }

  /**
   * Sets the sub_type.
   *
   * @param character_varying(255) $sub_type
   */
  public function setSubType($sub_type) {
    $this->sub_type = $sub_type;
  }

  /**
   * Retrieves the cv_id.
   *
   * @retrun integer
   */
  public function getCvID() {
    return $this->cv_id;
  }

  /**
   * Sets the cv_id.
   *
   * @param integer $cv_id
   */
  public function setCvID($cv_id) {
    $this->cv_id = $cv_id;
  }

  /**
   * Retrieves the cv_name.
   *
   * @retrun character_varying(255)
   */
  public function getCvName() {
    return $this->cv_name;
  }

  /**
   * Sets the cv_name.
   *
   * @param character_varying(255) $cv_name
   */
  public function setCvName($cv_name) {
    $this->cv_name = $cv_name;
  }

  /**
   * Retrieves the project_id.
   *
   * @retrun integer
   */
  public function getProjectID() {
    return $this->project_id;
  }

  /**
   * Sets the project_id.
   *
   * @param integer $project_id
   */
  public function setProjectID($project_id) {
    $this->project_id = $project_id;
  }

  /**
   * Retrieves the project_name.
   *
   * @retrun character_varying(255)
   */
  public function getProjectName() {
    return $this->project_name;
  }

  /**
   * Sets the project_name.
   *
   * @param character_varying(255) $project_name
   */
  public function setProjectName($project_name) {
    $this->project_name = $project_name;
  }

  /**
   * Retrieves the crop_id.
   *
   * @retrun integer
   */
  public function getCropID() {
    return $this->crop_id;
  }

  /**
   * Sets the crop_id.
   *
   * @param integer $crop_id
   */
  public function setCropID($crop_id) {
    $this->crop_id = $crop_id;
  }
}