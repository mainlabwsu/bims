<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_CROP class.
 *
 */
class PUBLIC_BIMS_CROP  {

  /**
   *  Data members for PUBLIC_BIMS_CROP.
   */
  protected $member_arr = NULL;
  protected $crop_id    = NULL;
  protected $name       = NULL;
  protected $label      = NULL;
  protected $image_url  = NULL;
  protected $prop       = NULL;
  protected $ploidy     = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'crop_id'   => 'serial',
    'name'      => 'character_varying(255)',
    'label'     => 'character_varying(255)',
    'image_url' => 'character_varying(255)',
    'prop'      => 'text',
    'ploidy'    => 'integer',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'crop_id'   => TRUE,
    'name'      => TRUE,
    'label'     => TRUE,
    'image_url' => TRUE,
    'prop'      => FALSE,
    'ploidy'    => TRUE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr = $details;
    $this->crop_id    = array_key_exists('crop_id', $details)   ? $details['crop_id']   : '';
    $this->name       = array_key_exists('name', $details)      ? $details['name']      : '';
    $this->label      = array_key_exists('label', $details)     ? $details['label']     : '';
    $this->image_url  = array_key_exists('image_url', $details) ? $details['image_url'] : '';
    $this->prop       = array_key_exists('prop', $details)      ? $details['prop']      : '';
    $this->ploidy     = array_key_exists('ploidy', $details)    ? $details['ploidy']    : 2;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_CROP by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_crop WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'crop_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_crop_ukey_bims_crop_name_key' => array('name'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['name']      = $this->name;
        $fields['label']     = $this->label;
        $fields['image_url'] = $this->image_url;
        $fields['prop']      = $this->prop;
        $fields['ploidy']    = (is_numeric($this->ploidy)) ? $this->ploidy : 2;


        // Inserts the record.
        $this->crop_id = db_insert('bims_crop')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['name']      = $this->name;
      $fields['label']     = $this->label;
      $fields['image_url'] = $this->image_url;
      $fields['prop']      = $this->prop;
      $fields['ploidy']    = (is_numeric($this->ploidy)) ? $this->ploidy : 2;


      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_crop')
        ->fields($fields)
        ->condition('crop_id', $this->crop_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_crop')
        ->condition('crop_id', $this->crop_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the crop_id.
   *
   * @retrun serial
   */
  public function getCropID() {
    return $this->crop_id;
  }

  /**
   * Sets the crop_id.
   *
   * @param serial $crop_id
   */
  public function setCropID($crop_id) {
    $this->crop_id = $crop_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(255)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(255) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the label.
   *
   * @retrun character_varying(255)
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * Sets the label.
   *
   * @param character_varying(255) $label
   */
  public function setLabel($label) {
    $this->label = $label;
  }

  /**
   * Retrieves the image_url.
   *
   * @retrun character_varying(255)
   */
  public function getImageUrl() {
    return $this->image_url;
  }

  /**
   * Sets the image_url.
   *
   * @param character_varying(255) $image_url
   */
  public function setImageUrl($image_url) {
    $this->image_url = $image_url;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the ploidy.
   *
   * @retrun integer
   */
  public function getPloidy() {
    return $this->ploidy;
  }

  /**
   * Sets the ploidy.
   *
   * @param integer $ploidy
   */
  public function setPloidy($ploidy) {
    $this->ploidy = $ploidy;
  }
}