<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_MARKER_LOCATION class.
 *
 */
class PUBLIC_BIMS_MARKER_LOCATION  {

  /**
   *  Data members for PUBLIC_BIMS_MARKER_LOCATION.
   */
  protected $member_arr     = NULL;
  protected $feature_id     = NULL;
  protected $ss_id          = NULL;
  protected $analysis_id    = NULL;
  protected $genome         = NULL;
  protected $chromosome_fid = NULL;
  protected $chromosome     = NULL;
  protected $loc_start      = NULL;
  protected $loc_stop       = NULL;
  protected $prop           = NULL;
  protected $marker         = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'feature_id'     => 'integer',
    'ss_id'          => 'character_varying(500)',
    'analysis_id'    => 'integer',
    'genome'         => 'character_varying(500)',
    'chromosome_fid' => 'integer',
    'chromosome'     => 'character_varying(500)',
    'loc_start'      => 'character_varying(255)',
    'loc_stop'       => 'character_varying(255)',
    'prop'           => 'text',
    'marker'         => 'character_varying(500)',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'feature_id'     => TRUE,
    'ss_id'          => FALSE,
    'analysis_id'    => FALSE,
    'genome'         => FALSE,
    'chromosome_fid' => FALSE,
    'chromosome'     => FALSE,
    'loc_start'      => FALSE,
    'loc_stop'       => FALSE,
    'prop'           => FALSE,
    'marker'         => TRUE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr     = $details;
    $this->feature_id     = array_key_exists('feature_id', $details)     ? $details['feature_id']     : '';
    $this->ss_id          = array_key_exists('ss_id', $details)          ? $details['ss_id']          : '';
    $this->analysis_id    = array_key_exists('analysis_id', $details)    ? $details['analysis_id']    : '';
    $this->genome         = array_key_exists('genome', $details)         ? $details['genome']         : '';
    $this->chromosome_fid = array_key_exists('chromosome_fid', $details) ? $details['chromosome_fid'] : '';
    $this->chromosome     = array_key_exists('chromosome', $details)     ? $details['chromosome']     : '';
    $this->loc_start      = array_key_exists('loc_start', $details)      ? $details['loc_start']      : '';
    $this->loc_stop       = array_key_exists('loc_stop', $details)       ? $details['loc_stop']       : '';
    $this->prop           = array_key_exists('prop', $details)           ? $details['prop']           : '';
    $this->marker         = array_key_exists('marker', $details)         ? $details['marker']         : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_MARKER_LOCATION by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_marker_location WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return NULL;
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_marker_location_ukey_001_key' => array('feature_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['feature_id'] = $this->feature_id;
        $fields['ss_id']      = $this->ss_id;
        $fields['genome']     = $this->genome;
        $fields['chromosome'] = $this->chromosome;
        $fields['loc_start']  = $this->loc_start;
        $fields['loc_stop']   = $this->loc_stop;
        $fields['prop']       = $this->prop;
        $fields['marker']     = $this->marker;
        if (is_numeric($this->analysis_id))    { $fields['analysis_id']    = $this->analysis_id; }
        if (is_numeric($this->chromosome_fid)) { $fields['chromosome_fid'] = $this->chromosome_fid; }

        // Inserts the record.
        db_insert('bims_marker_location')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['feature_id'] = $this->feature_id;
      $fields['ss_id']      = $this->ss_id;
      $fields['genome']     = $this->genome;
      $fields['chromosome'] = $this->chromosome;
      $fields['loc_start']  = $this->loc_start;
      $fields['loc_stop']   = $this->loc_stop;
      $fields['prop']       = $this->prop;
      $fields['marker']     = $this->marker;
      if (is_numeric($this->analysis_id))    { $fields['analysis_id']    = $this->analysis_id; }
      if (is_numeric($this->chromosome_fid)) { $fields['chromosome_fid'] = $this->chromosome_fid; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_marker_location')
        ->fields($fields)
        ->condition('feature_id', $this->feature_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_marker_location')
        ->condition('feature_id', $this->feature_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the feature_id.
   *
   * @retrun integer
   */
  public function getFeatureID() {
    return $this->feature_id;
  }

  /**
   * Sets the feature_id.
   *
   * @param integer $feature_id
   */
  public function setFeatureID($feature_id) {
    $this->feature_id = $feature_id;
  }

  /**
   * Retrieves the ss_id.
   *
   * @retrun character_varying(500)
   */
  public function getSsID() {
    return $this->ss_id;
  }

  /**
   * Sets the ss_id.
   *
   * @param character_varying(500) $ss_id
   */
  public function setSsID($ss_id) {
    $this->ss_id = $ss_id;
  }

  /**
   * Retrieves the analysis_id.
   *
   * @retrun integer
   */
  public function getAnalysisID() {
    return $this->analysis_id;
  }

  /**
   * Sets the analysis_id.
   *
   * @param integer $analysis_id
   */
  public function setAnalysisID($analysis_id) {
    $this->analysis_id = $analysis_id;
  }

  /**
   * Retrieves the genome.
   *
   * @retrun character_varying(500)
   */
  public function getGenome() {
    return $this->genome;
  }

  /**
   * Sets the genome.
   *
   * @param character_varying(500) $genome
   */
  public function setGenome($genome) {
    $this->genome = $genome;
  }

  /**
   * Retrieves the chromosome_fid.
   *
   * @retrun integer
   */
  public function getChromosomeFid() {
    return $this->chromosome_fid;
  }

  /**
   * Sets the chromosome_fid.
   *
   * @param integer $chromosome_fid
   */
  public function setChromosomeFid($chromosome_fid) {
    $this->chromosome_fid = $chromosome_fid;
  }

  /**
   * Retrieves the chromosome.
   *
   * @retrun character_varying(500)
   */
  public function getChromosome() {
    return $this->chromosome;
  }

  /**
   * Sets the chromosome.
   *
   * @param character_varying(500) $chromosome
   */
  public function setChromosome($chromosome) {
    $this->chromosome = $chromosome;
  }

  /**
   * Retrieves the loc_start.
   *
   * @retrun character_varying(255)
   */
  public function getLocStart() {
    return $this->loc_start;
  }

  /**
   * Sets the loc_start.
   *
   * @param character_varying(255) $loc_start
   */
  public function setLocStart($loc_start) {
    $this->loc_start = $loc_start;
  }

  /**
   * Retrieves the loc_stop.
   *
   * @retrun character_varying(255)
   */
  public function getLocStop() {
    return $this->loc_stop;
  }

  /**
   * Sets the loc_stop.
   *
   * @param character_varying(255) $loc_stop
   */
  public function setLocStop($loc_stop) {
    $this->loc_stop = $loc_stop;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the marker.
   *
   * @retrun character_varying(500)
   */
  public function getMarker() {
    return $this->marker;
  }

  /**
   * Sets the marker.
   *
   * @param character_varying(500) $marker
   */
  public function setMarker($marker) {
    $this->marker = $marker;
  }
}