<?php
/**
 * Adds namespace.
 */

/**
 * The declaration of PUBLIC_BIMS_NODE class.
 *
 */
class PUBLIC_BIMS_NODE  {

  /**
   *  Data members for PUBLIC_BIMS_NODE.
   */
  protected $member_arr       = NULL;
  protected $node_id          = NULL;
  protected $name             = NULL;
  protected $type             = NULL;
  protected $prop             = NULL;
  protected $root_id          = NULL;
  protected $crop_id          = NULL;
  protected $owner_id         = NULL;
  protected $project_id       = NULL;
  protected $trial_tree       = NULL;
  protected $breed_line_tree  = NULL;
  protected $cross_tree       = NULL;
  protected $description      = NULL;
  protected $access           = NULL;
  protected $project_type     = NULL;
  protected $project_sub_type = NULL;

  /**
   *  Data types for the data members.
   */
  protected static $data_types = array(
    'node_id'          => 'serial',
    'name'             => 'character_varying(100)',
    'type'             => 'character_varying(100)',
    'prop'             => 'text',
    'root_id'          => 'integer',
    'crop_id'          => 'integer',
    'owner_id'         => 'integer',
    'project_id'       => 'integer',
    'trial_tree'       => 'text',
    'breed_line_tree'  => 'text',
    'cross_tree'       => 'text',
    'description'      => 'text',
    'access'           => 'integer',
    'project_type'     => 'character_varying(100)',
    'project_sub_type' => 'character_varying(255)',
  );

  /**
   *  'NOT NULL' for the data members.
   */
  protected static $not_nulls = array(
    'node_id'          => TRUE,
    'name'             => TRUE,
    'type'             => TRUE,
    'prop'             => FALSE,
    'root_id'          => FALSE,
    'crop_id'          => FALSE,
    'owner_id'         => TRUE,
    'project_id'       => FALSE,
    'trial_tree'       => FALSE,
    'breed_line_tree'  => FALSE,
    'cross_tree'       => FALSE,
    'description'      => FALSE,
    'access'           => FALSE,
    'project_type'     => FALSE,
    'project_sub_type' => FALSE,
  );

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    // Initializes data members.
    $this->member_arr       = $details;
    $this->node_id          = array_key_exists('node_id', $details)          ? $details['node_id']          : '';
    $this->name             = array_key_exists('name', $details)             ? $details['name']             : '';
    $this->type             = array_key_exists('type', $details)             ? $details['type']             : '';
    $this->prop             = array_key_exists('prop', $details)             ? $details['prop']             : '';
    $this->root_id          = array_key_exists('root_id', $details)          ? $details['root_id']          : '';
    $this->crop_id          = array_key_exists('crop_id', $details)          ? $details['crop_id']          : '';
    $this->owner_id         = array_key_exists('owner_id', $details)         ? $details['owner_id']         : '';
    $this->project_id       = array_key_exists('project_id', $details)       ? $details['project_id']       : '';
    $this->trial_tree       = array_key_exists('trial_tree', $details)       ? $details['trial_tree']       : '';
    $this->breed_line_tree  = array_key_exists('breed_line_tree', $details)  ? $details['breed_line_tree']  : '';
    $this->cross_tree       = array_key_exists('cross_tree', $details)       ? $details['cross_tree']       : '';
    $this->description      = array_key_exists('description', $details)      ? $details['description']      : '';
    $this->access           = array_key_exists('access', $details)           ? $details['access']           : '';
    $this->project_type     = array_key_exists('project_type', $details)     ? $details['project_type']     : '';
    $this->project_sub_type = array_key_exists('project_sub_type', $details) ? $details['project_sub_type'] : '';
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {
  }

  /**
   * Generate PUBLIC_BIMS_NODE by key(s).
   */
  public static function byKey($keys) {

    // Returns NULL if non-integer value are given to the data member whose
    // data type is 'integer'.
    $where_arr = array();
    $args      = array();
    foreach ($keys as $key => $value) {

      // Gets data type.
      $data_type = self::$data_types[$key];

      // Gets 'NOT NULL'.
      $is_not_null = self::$not_nulls[$key];

      // Checks the value if data type is 'integer'.
      if (in_array($data_type, array('serial', 'integer'))) {

        // Returns NULL if $value is empty string and it is 'NOT NULL'.
        if ($value == '' && $value != '0') {
          if ($is_not_null) {
            return NULL;
          }
          continue;
        }
        else if (!is_int((int)$value)) {
          return NULL;
        }
      }

      // Adds LOWER() function to the characters.
      if (preg_match("/^(char|text)/", $data_type)) {
        $where_arr []= " LOWER($key) = LOWER(:$key) ";
      }
      else {
        $where_arr []= " $key = :$key ";
      }
      $args[":$key"] = $keys[$key];
    }

    // Gets the table properties.
    $sql = "SELECT * FROM bims_node WHERE " . implode(" AND " , $where_arr);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    if ($details) {
      return new self($details);
    }
    return NULL;
  }

  /**
   * Returns the primary key.
   */
  public static function getPK() {
    return 'node_id';
  }

  /**
   * Returns the foreign keys.
   */
  public static function getFK() {
    return NULL;
  }

  /**
   * Returns the unique keys.
   */
  public static function getUK() {
    return array(
      'bims_node_ukey_bims_node_root_id_name_owner_id_key' => array('root_id','name','owner_id'),
    );
  }

  /**
   * Returns the related tables.
   */
  public static function getRelTable() {
    return NULL;
  }

  /**
   * Adds a new record.
   */
  public function insert() {

    $transaction = db_transaction();
    try {

      // TODO:Checks for duplication.
      $dup = FALSE;

      // Inserts the record if not duplicated.
      if (!$dup) {

        // Populates all fields. The attribute with 'NOT NULL' would be skipped
        // if the value is empty.
        $fields = array();
        $fields['name']             = $this->name;
        $fields['type']             = $this->type;
        $fields['prop']             = $this->prop;
        $fields['owner_id']         = $this->owner_id;
        $fields['trial_tree']       = $this->trial_tree;
        $fields['breed_line_tree']  = $this->breed_line_tree;
        $fields['cross_tree']       = $this->cross_tree;
        $fields['description']      = $this->description;
        $fields['project_type']     = $this->project_type;
        $fields['project_sub_type'] = $this->project_sub_type;
        if (is_numeric($this->root_id))    { $fields['root_id']    = $this->root_id; }
        if (is_numeric($this->crop_id))    { $fields['crop_id']    = $this->crop_id; }
        if (is_numeric($this->project_id)) { $fields['project_id'] = $this->project_id; }
        if (is_numeric($this->access))     { $fields['access']     = $this->access; }

        // Inserts the record.
        $this->node_id = db_insert('bims_node')
          ->fields($fields)
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Updates the record.
   */
  public function update($new_values = array()) {

    $transaction = db_transaction();
    try {

      // Populates all fields. The attribute with 'NOT NULL' would be skipped
      // if the value is empty.
      $fields = array();
      $fields['name']             = $this->name;
      $fields['type']             = $this->type;
      $fields['prop']             = $this->prop;
      $fields['owner_id']         = $this->owner_id;
      $fields['trial_tree']       = $this->trial_tree;
      $fields['breed_line_tree']  = $this->breed_line_tree;
      $fields['cross_tree']       = $this->cross_tree;
      $fields['description']      = $this->description;
      $fields['project_type']     = $this->project_type;
      $fields['project_sub_type'] = $this->project_sub_type;
      if (is_numeric($this->root_id))    { $fields['root_id']    = $this->root_id; }
      if (is_numeric($this->crop_id))    { $fields['crop_id']    = $this->crop_id; }
      if (is_numeric($this->project_id)) { $fields['project_id'] = $this->project_id; }
      if (is_numeric($this->access))     { $fields['access']     = $this->access; }

      // Updates the fields with the new values.
      foreach ($fields as $attr => $value) {
        if (array_key_exists($attr, $new_values)) {
          $fields[$attr] = $new_values[$attr];
        }
      }

      // Updates the record.
      db_update('bims_node')
        ->fields($fields)
        ->condition('node_id', $this->node_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the record.
   */
  public function delete() {

    $transaction = db_transaction();
    try {

      db_delete('bims_node')
        ->condition('node_id', $this->node_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('mod', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /*
   * Defines the data query functions below.
   */
  /**
   * Retrieves the data type of the member.
   *
   * @retrun various
   */
  public function getDataType($member) {
    if (is_array(self::$data_types) && array_key_exists($member, self::$data_types)) {
      return self::$data_types[$member];
    }
    return '';
  }
  /**
   * Retrieves the 'NOT NULL' of the member.
   *
   * @retrun various
   */
  public function isNotNull($member) {
    if (is_array(self::$not_nulls) && array_key_exists($member, self::$not_nulls)) {
      return self::$not_nulls[$member];
    }
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the data member array.
   *
   * @retrun array
   */
  public function getMemberArr() {
    return $this->member_arr;
  }

  /**
   * Sets the data member array.
   *
   * @param array $member_arr
   */
  public function setMemberArr($member_arr) {
    $this->member_arr = $member_arr;
  }

  /**
   * Retrieves the node_id.
   *
   * @retrun serial
   */
  public function getNodeID() {
    return $this->node_id;
  }

  /**
   * Sets the node_id.
   *
   * @param serial $node_id
   */
  public function setNodeID($node_id) {
    $this->node_id = $node_id;
  }

  /**
   * Retrieves the name.
   *
   * @retrun character_varying(100)
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Sets the name.
   *
   * @param character_varying(100) $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * Retrieves the type.
   *
   * @retrun character_varying(100)
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Sets the type.
   *
   * @param character_varying(100) $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * Retrieves the prop.
   *
   * @retrun text
   */
  public function getProp() {
    return $this->prop;
  }

  /**
   * Sets the prop.
   *
   * @param text $prop
   */
  public function setProp($prop) {
    $this->prop = $prop;
  }

  /**
   * Retrieves the root_id.
   *
   * @retrun integer
   */
  public function getRootID() {
    return $this->root_id;
  }

  /**
   * Sets the root_id.
   *
   * @param integer $root_id
   */
  public function setRootID($root_id) {
    $this->root_id = $root_id;
  }

  /**
   * Retrieves the crop_id.
   *
   * @retrun integer
   */
  public function getCropID() {
    return $this->crop_id;
  }

  /**
   * Sets the crop_id.
   *
   * @param integer $crop_id
   */
  public function setCropID($crop_id) {
    $this->crop_id = $crop_id;
  }

  /**
   * Retrieves the owner_id.
   *
   * @retrun integer
   */
  public function getOwnerID() {
    return $this->owner_id;
  }

  /**
   * Sets the owner_id.
   *
   * @param integer $owner_id
   */
  public function setOwnerID($owner_id) {
    $this->owner_id = $owner_id;
  }

  /**
   * Retrieves the project_id.
   *
   * @retrun integer
   */
  public function getProjectID() {
    return $this->project_id;
  }

  /**
   * Sets the project_id.
   *
   * @param integer $project_id
   */
  public function setProjectID($project_id) {
    $this->project_id = $project_id;
  }

  /**
   * Retrieves the trial_tree.
   *
   * @retrun text
   */
  public function getTrialTree() {
    return $this->trial_tree;
  }

  /**
   * Sets the trial_tree.
   *
   * @param text $trial_tree
   */
  public function setTrialTree($trial_tree) {
    $this->trial_tree = $trial_tree;
  }

  /**
   * Retrieves the breed_line_tree.
   *
   * @retrun text
   */
  public function getBreedLineTree() {
    return $this->breed_line_tree;
  }

  /**
   * Sets the breed_line_tree.
   *
   * @param text $breed_line_tree
   */
  public function setBreedLineTree($breed_line_tree) {
    $this->breed_line_tree = $breed_line_tree;
  }

  /**
   * Retrieves the cross_tree.
   *
   * @retrun text
   */
  public function getCrossTree() {
    return $this->cross_tree;
  }

  /**
   * Sets the cross_tree.
   *
   * @param text $cross_tree
   */
  public function setCrossTree($cross_tree) {
    $this->cross_tree = $cross_tree;
  }

  /**
   * Retrieves the description.
   *
   * @retrun text
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description.
   *
   * @param text $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Retrieves the access.
   *
   * @retrun integer
   */
  public function getAccess() {
    return $this->access;
  }

  /**
   * Sets the access.
   *
   * @param integer $access
   */
  public function setAccess($access) {
    $this->access = $access;
  }

  /**
   * Retrieves the project_type.
   *
   * @retrun character_varying(100)
   */
  public function getProjectType() {
    return $this->project_type;
  }

  /**
   * Sets the project_type.
   *
   * @param character_varying(100) $project_type
   */
  public function setProjectType($project_type) {
    $this->project_type = $project_type;
  }

  /**
   * Retrieves the project_sub_type.
   *
   * @retrun character_varying(255)
   */
  public function getProjectSubType() {
    return $this->project_sub_type;
  }

  /**
   * Sets the project_sub_type.
   *
   * @param character_varying(255) $project_sub_type
   */
  public function setProjectSubType($project_sub_type) {
    $this->project_sub_type = $project_sub_type;
  }
}