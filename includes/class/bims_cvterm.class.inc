<?php
/**
 * The declaration of BIMS_CVTERM class.
 *
 */
class BIMS_CVTERM {

  /**
   * Class data members.
   */
  protected $program_id   = NULL;
  protected $cv_id        = NULL;
  protected $cv_name      = NULL;
  protected $cvterm_id    = NULL;
  protected $cvterm_name  = NULL;

 /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($cvterm_id, $program_id = NULL) {

    // Gets and sets the properties of the cvterm.
    $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
    if ($cvterm) {
      $cv = MCL_CHADO_CV::byID($cvterm->getCvID());
      if ($cv) {
        $this->cv_id        = $cv->getCvID();
        $this->cv_name      = $cv->getName();
        $this->cvterm_id    = $cvterm_id;
        $this->cvterm_name  = $cvterm->getName();
      }
    }

    // Sets the program ID.
    $this->program_id = ($program_id) ? $program_id : $this->_getProgramID($this->cv_name);
  }

  /**
   * Returns the program ID from chado.cv.name.
   *
   * @param integer $program_id
   *
   * @return integer|NULL
   */
  private function _getProgramID($cv_name) {
    if ($cv_name) {
      if (preg_match("/^(\d+)\./", $cv_name, $matches)) {
        return $matches[1];
      }
    }
    return NULL;
  }

  /**
   * Returns the all associated data.
   *
   * @param integer $cvterm_id
   *
   * @return array
   */
  public function getAssocData($cvterm_id) {

    // Initializes the data counts.
    $data_counts = array();
    if (!$this->program_id) {
      return $data_counts;
    }

    // Counts the data in BIMS_CHADO.
    $data_count = bims_count_data_bc('cvterm', $cvterm_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['bims_chado'] = $data_count;
    }

    // Counts the data in mviews.
    $data_count = bims_count_data_mview('cvterm', $cvterm_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['mview'] = $data_count;
    }

    // Counts the data in public schema.
    $data_count = bims_count_data_public('cvterm', $cvterm_id);
    if (!empty($data_count)) {
      $data_counts['public'] = $data_count;
    }
    return $data_counts;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Checks if this cvterm can have a statistic.
   *
   * @param MCL_CHADO_CVTERM $cvterm
   *
   * @return boolean
   */
  public static function canStats(MCL_CHADO_CVTERM $cvterm) {

    // Checks the cvterm.
    if (!$cvterm) {
      return FALSE;
    }

    // Try to get the format of the cvterm.
    $format = $cvterm->getFormat();
    if (!$format) {
      return FALSE;
    }

    // Checks the type.
    $valid_formats = BIMS_FIELD_BOOK::getStatsFormats();
    return in_array($format, $valid_formats);
   }

  /**
   * Returns TRUE if this cvterm is chado (public).
   *
   * @return boolean
   */
  public function isChado() {
    return !preg_match("/BIMS_DESCRIPTOR_PROP$/", $this->cv_name);
  }

  /**
   * Returns all the traits.
   *
   * @param integer $program_id
   *
   * @return array
   */
  public static function getTraits($flag = BIMS_OPTION) {

    // Gets the cv_id of the site ontology.
    $cv_name = MCL_SITE_VAR::getValueByName('SITE_TRAIT_ONTOLOGY');
    $cv = MCL_CHADO_CV::byName($cv_name);

    // Gets the traits.
    $traits = array();
    if ($cv) {
      $sql = "SELECT C.* FROM {chado.cvterm} C WHERE C.cv_id = :cv_id";
      $results = db_query($sql, array(':cv_id' => $cv->getCvID()));
      while ($obj = $results->fetchObject()) {
        if ($flag == BIMS_CLASS) {
          $traits []= MCL_CHADO_CVTERM::byID($obj->cvterm_id);
        }
        else if ($flag == BIMS_OBJECT) {
          $traits []= $obj;
        }
        else {
          $traits[$obj->cvterm_id] = $obj->name;
        }
      }
    }
    return $traits;
  }

  /**
   * Returns the trait by name.
   *
   * @param string $name
   *
   * @return various
   */
  public static function getTraitByName($name) {
    $cv_name = MCL_SITE_VAR::getValueByName('SITE_TRAIT_ONTOLOGY');
    $cv = MCL_CHADO_CV::byName($cv_name);
    if ($cv) {
      $keys = array(
        'cv_id' => $cv->getCvID(),
        'name'  => $name,
      );
      return MCL_CHADO_CVTERM::byKey();
    }
    return NULL;
  }

  /**
   * Matches two descriptors (BIMS and Chado).
   *
   * @param intger $program_id
   * @param string $action
   * @param intger $cvterm_id_base
   * @param intger $cvterm_id
   * @param boolean $force
   *
   * returun boolean
   */
  public static function matchDescriptors($program_id, $action, $cvterm_id_base, $cvterm_id = '', $force = FALSE) {

    $transaction = db_transaction();
    try {

      // Gets BIMS_PROGRAM.
      $bims_program = BIMS_PROGRAM::byID($program_id);

      // Checks the base cvterms.
      $cvterm_base  = MCL_CHADO_CVTERM::byID($cvterm_id_base);
      if (!$cvterm_base) {
        throw new Exception("The base cvterm ($cvterm_id_base) not found");
      }

      // Checks the matching/matched cvterm.
      if ($cvterm_id) {
        $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
        if (!$cvterm) {
          throw new Exception("The matching cvterm ($cvterm_id) not found");
        }
      }

      // Matching the descriptors.
      if ($action == 'match') {
        $err = self::_matchDescriptors($bims_program, $cvterm_id_base, $cvterm_id, $force);
      }

      // Unmatching the descriptors.
      else if ($action == 'unmatch') {
        $err = self::_unmatchDescriptors($bims_program, $cvterm_id_base, $cvterm_id, $force);
      }
      if ($err) {
        throw new Exception($err);
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      throw new DrupalUpdateException($error);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Matches two descriptors (BIMS and Chado).
   *
   * @param BIMS_PROGRAM $bims_program
   * @param intger $cvterm_id_base
   * @param intger $cvterm_id
   * @param boolean $force
   *
   * returun string
   */
  public static function _matchDescriptors(BIMS_PROGRAM $bims_program, $cvterm_id_base, $cvterm_id, $force) {

    // Gets the program ID.
    $program_id = $bims_program->getProgramID();

    // Copies the data in matched to base t-column.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    if (!$bm_phenotype->copyData($cvterm_id_base, $cvterm_id)) {
      return "Fail to copy between the descriptor coumns in BIMS_MVIEW_PHENOTYPE";
    }

    // Removes the matching cvterm.
    $bims_cvterm = new BIMS_CVTERM($cvterm_id, $program_id);
    if (!$bims_cvterm->deleteCvterm()) {
      return "Fail to delete the cvterm ($cvterm_id)";
    }

    // Adds the pairing cvterms to bims_descriptor_match table..
    $keys = array(
      'program_id'      => $program_id,
      'cvterm_id_base'  => $cvterm_id_base,
      'cvterm_id'       => $cvterm_id,
    );
    $dm = PUBLIC_BIMS_DESCRIPTOR_MATCH::byKey($keys);
    if (!$dm) {
      $dm = new PUBLIC_BIMS_DESCRIPTOR_MATCH($keys);
      if (!$dm->insert()) {
        return "Fail to add bims_descriptor_match";
      }
    }
    return '';

      /*
      // Gets the mviews.
      $bims_mview_p   = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
      $bims_mview_ps  = new BIMS_MVIEW_PHENOTYPE_STATS(array('node_id' => $program_id));
      $bims_mview_s   = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
      $bims_mview_ss  = new BIMS_MVIEW_STOCK_STATS(array('node_id' => $program_id));
      $mview_p        = $bims_mview_p->getMView();
      $mview_s        = $bims_mview_s->getMView();



      // Deletes the descriptor in BIMS_MVIEW_PHENOTYPE.
      if (!$bims_mview_p->deleteDescriptor($cvterm_id)) {
        return "Fail to remove the descriptor from BIMS_MVIEW_PHENOTYPE";
      }

      // Deletes the descriptor in BIMS_MVIEW_STOCK.
      if (!$bims_mview_s->deleteDescriptor($cvterm_id)) {
        return "Fail to remove the descriptor from BIMS_MVIEW_PHENOTYPE";
      }

      // Updates the stats in BIMS_MVIEW_PHENOTYPE_STATS.
      if (!$bims_mview_ps->updateMView(NULL, $cvterm_id_base, FALSE)) {
        return "Fail to update the mview(BIMS_MVIEW_PHENOTYPE_STATS)";
      }

      // Updates the stats in BIMS_MVIEW_STOCK_STATS.
      if (!$bims_mview_ss->updateMView($cvterm_id_base, FALSE)){
        return "ail to update the mview(BIMS_MVIEW_STOCK_STATS)";
      }

      // Updates the stored active descriptors.
      if (!$bims_program->updateActiveDescriptors($cvterm_id)) {
        return "Fail to remove the descriptor from the stored active descriptors";
      }
      if (!$bims_program->updateActiveDescriptors($cvterm_id_base)) {
        return "Fail to add the descriptor to the stored active descriptors";
      }*/

  }

  /**
   * Matches two descriptors (BIMS and Chado).
   *
   * @param BIMS_PROGRAM $bims_program
   * @param intger $cvterm_id_base
   * @param intger $cvterm_id_matched
   * @param boolean $force
   *
   * returun string
   */
  public static function _unmatchDescriptors(BIMS_PROGRAM $bims_program, $cvterm_id_base, $cvterm_id_matched = '', $force) {

    // Gets the matched descriptors.
    $program_id = $bims_program->getProgramID();
    $sql = "
      SELECT DISTINCT cvterm_id_base, cvterm_id
      FROM {bims_descriptor_match} DM
      WHERE DM.program_id = :program_id AND DM.cvterm_id_base = :cvterm_id_base
    ";
    $args = array(
      ':program_id'     => $program_id,
      ':cvterm_id_base' => $cvterm_id_base,
    );
    if ($cvterm_id_matched) {
      $sql .= " AND cvterm_id = :cvterm_id ";
      $args[':cvterm_id'] = $cvterm_id_matched;
    }
    $results = db_query($sql, $args);
    $cvterms = array();
    while ($obj = $results->fetchObject()) {
      $cvterms []= array(
        'cvterm_id_base'  => $obj->cvterm_id_base,
        'cvterm_id'       => $obj->cvterm_id,
      );
    }

    // Gets the mviews.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    $m_phenotype  = $bm_phenotype->getMView();

    // Unmatches the descriptors.
    foreach ((array)$cvterms as $info) {
      $cvterm_id_base     = $info['cvterm_id_base'];
      $cvterm_id_match    = $info['cvterm_id'];
      $keys = array(
        'program_id'      => $program_id,
        'cvterm_id_base'  => $cvterm_id_base,
        'cvterm_id'       => $cvterm_id_match,
      );
      $dm = PUBLIC_BIMS_DESCRIPTOR_MATCH::byKey($keys);
      $update = FALSE;
      if ($dm) {
        $update = TRUE;
        if (!$dm->delete()) {
          return "Fail to delete bims_descriptor_match";
        }
      }

      // Updates the mviews if data exist.
      if ($update || $force) {

        // Sets the fields.
        $field_base   = "t$cvterm_id_base";
        $field_match  = "t$cvterm_id_match";

        // Checks for data.
        $sql = "
          SELECT COUNT(*) FROM {$m_phenotype}
          WHERE $field_base IS NOT NULL AND chado = :chado
        ";
        $num = db_query($sql, array(':chado' => 1))->fetchField();
        if ($num) {

          // Adds the trait column to BIMS_MVIEW_PHENOTYPE.
          if (!db_field_exists($m_phenotype, $field_match)) {
            db_add_field($m_phenotype, $field_match, array('type' => 'text'));
          }

          // Copies the data in the t-column (descriptor column) of the mviews.
          $sql = "
            UPDATE $m_phenotype
            SET $field_match = $field_base, $field_base = NULL
            WHERE chado = :chado
          ";
          db_query($sql, array(':chado' => 1));

          // Copies the data to julian column if it is a date descriptor.
          $cvterm_base = MCL_CHADO_CVTERM::byID($cvterm_id_base);
          if ($cvterm_base->getFormat() == 'date') {
            $field_base_jd  = $field_base . '_jd';
            $field_match_jd = $field_match . '_jd';

            // Adds the julian column.
            if (!db_field_exists($m_phenotype, $field_match_jd)) {
              db_add_field($m_phenotype, $field_match_jd, array('type' => 'text'));
            }

            // Copies the data in the t-column (descriptor column) of the mviews.
            $sql = "UPDATE $m_phenotype SET $field_match_jd = $field_base_jd, $field_base_jd = NULL WHERE chado = :chado";
            db_query($sql, array(':chado' => 1));
          }

          // Updates the mview stats.
         // if (!$this->deleteCvterm()) {
        //    return "Fail to delete the cvterm ($)";
        //  }
         // if (!$this->->deleteCvterm()) {




/*
 *
 *
//    $bims_mview_ps  = new BIMS_MVIEW_PHENOTYPE_STATS(array('node_id' => $program_id));
//    $bims_mview_s   = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
//    $bims_mview_ss  = new BIMS_MVIEW_STOCK_STATS(array('node_id' => $program_id));
//    $mview_p        = $bims_mview_p->getMView();
//    $mview_s        = $bims_mview_s->getMView();
 *
           // Updates the stats in BIMS_MVIEW_PHENOTYPE_STATS.
          if (!$bims_mview_ps->updateMView(NULL, $cvterm_id_match, FALSE)) {
            return "Fail to update the mview(BIMS_MVIEW_PHENOTYPE_STATS)";
          }
          if (!$bims_mview_ps->updateMView(NULL, $cvterm_id_base, FALSE)) {
            return "Fail to update the mview(BIMS_MVIEW_PHENOTYPE_STATS)";
          }

          // Updates the stats in BIMS_MVIEW_STOCK_STATS.
          if (!$bims_mview_ss->updateMView($cvterm_id_match, FALSE)){
            return "Fail to update the mview(BIMS_MVIEW_STOCK_STATS)";
          }
          if (!$bims_mview_ss->updateMView($cvterm_id_base, FALSE)){
            return "Fail to update the mview(BIMS_MVIEW_STOCK_STATS)";
          }

          // Updates the stored active descriptors.
          if (!$bims_program->updateActiveDescriptors($cvterm_id_match)) {
            return "Fail to update the descriptor ($cvterm_id_match) in the stored active descriptors";
          }
          if (!$bims_program->updateActiveDescriptors($cvterm_id_base)) {
            return "Fail to update the descriptor ($cvterm_id_base) in the stored active descriptors";
          }
*/

        }
      }
    }
    return '';
  }

  /**
   * Returns the images associated with the given cvterm.
   *
   * @param integer $cvterm_id
   *
   * @return array
   */
  public function getImageIDs($cvterm_id) {

    // Gets the associated images.
    $results = NULL;
    if ($this->isChado()) {

      // Gets the images from chado.
      $sql = "
        SELECT DISTINCT eimage_id FROM {chado.cvterm_image}
        WHERE cvterm_id = :cvterm_id
      ";
      $results = db_query($sql, array(':cvterm_id' => $cvterm_id));
    }
    else {

      // Gets the images from BIMS_CHADO.
      $bc_image = new BIMS_CHADO_IMAGE($this->program_id);
      $table    = $bc_image->getTableName('eimage_link');
      $sql = "
        SELECT DISTINCT eimage_id FROM {$table}
        WHERE target_id = :target_id AND LOWER(target_type) = :target_type
      ";
      $args = array(
        ':target_id'    => $cvterm_id,
        ':target_type'  => 'cvterm_id',
    );
      $results = db_query($sql, $args);
    }

    // Returns the eimage IDs.
    $eimage_ids = array();
    if ($results) {
      while ($eimage_id = $results->fetchField()) {
        $eimage_ids []= $eimage_id;
      }
    }
    return $eimage_ids;
  }

  /**
   * Deletes the data in BIMS_CHADO.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function deleteDataBIMS($cvterm_id) {

    // Skips if this program is a Public Chado program.
    $bims_program = BIMS_PROGRAM::byID($this->program_id);
    if ($bims_program->isPublicChado()) {
      return TRUE;
    }

    // Deletes the phenotype_call by cvterm.
    $bc_pc = new BIMS_CHADO_PHENOTYPE_CALL($this->program_id);
    if (!$bc_pc->deleteTrait($cvterm_id)) {
      return FALSE;
    }

    // Deletes the images by cvterm.
    $bc_image = new BIMS_CHADO_IMAGE($this->program_id);
    if (!$bc_image->deleteImageByTarget('cvterm', $cvterm_id)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the data in mview.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function deleteDataMView($cvterm_id) {

    // Prepare for deletion.
    $fkeys_delete = array(
      'BIMS_MVIEW_LOCATION'           => array('p-column'),
      'BIMS_MVIEW_MARKER'             => array('p-column'),
      'BIMS_MVIEW_CROSS'              => array('p-column'),
      'BIMS_MVIEW_STOCK'              => array('p-column', 't-column'),
      'BIMS_MVIEW_PHENOTYPE'          => array('p-column', 't-column'),
      'BIMS_MVIEW_GENOTYPE'           => array('p-column'),
      'BIMS_MVIEW_GENOTYPE_MARKER'    => array('p-column'),
      'BIMS_MVIEW_GENOTYPE_ACCESSION' => array('p-column'),
      'BIMS_MVIEW_SSR'                => array('p-column'),
    );

    // Deletes the data.
    return bims_delete_data_mview($this->program_id, array($cvterm_id), $fkeys_delete);
  }

  /**
   * Deletes the data in public.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function deleteDataPublic($cvterm_id) {

    // Gets conditions.
    $conds = array(
      'bims_mview_descriptor' => array(
        'program_id'  => $this->program_id,
        'cvterm_id'   => $cvterm_id,
      ),
      'bims_descriptor_rank' => array(
        'program_id'  => $this->program_id,
        'cvterm_id'   => $cvterm_id,
      ),

      'bims_mview_stock_stats' => array(
        'node_id'   => $this->program_id,
        'cvterm_id' => $cvterm_id,
      ),
      'bims_mview_cross_stats' => array(
        'node_id'   => $this->program_id,
        'cvterm_id' => $cvterm_id,
      ),
      'bims_mview_phenotype_stats' => array(
        'node_id'   => $this->program_id,
        'cvterm_id' => $cvterm_id,
      ),
      'bims_descriptor_match' => array(
        array(
          'program_id'      => $this->program_id,
          'cvterm_id_base'  => $cvterm_id,
        ),
        array(
          'program_id'  => $this->program_id,
          'cvterm_id'   => $cvterm_id,
        ),
      ),
    );

    // Deletes the data.
    if (!bims_delete_data_public($conds)) {
      return FALSE;
    }

    // Deletes the rest of the data in bims_mview_phenotype_stats.
    $sql = "
      DELETE FROM {bims_mview_phenotype_stats}
      WHERE cvterm_id = :cvterm_id AND node_id IN (
        SELECT node_id FROM {bims_node}
        WHERE root_id = :root_id
      )
    ";
    $args = array(
      ':root_id'    => $this->program_id,
      ':cvterm_id'  => $cvterm_id,
    );
    db_query($sql, $args);
    return TRUE;
  }

  /**
   * Deletes all the cvterms of the given CV.
   *
   * @param integer $program_id
   * @param integer $cv_id
   *
   * @retrun boolean
   */
  public static function deleteCvtermByCV($program_id, $cv_id) {
    $transaction = db_transaction();
    try {

      // Gets the all cvterms.
      $sql = "
        SELECT cvterm_id FROM {bims_mview_descriptor}
        WHERE cv_id = :cv_id AND program_id = :program_id
      ";
      $args = array(
        ':cv_id'      => $cv_id,
        ':program_id' => $program_id,
      );
      $results = db_query($sql, $args);
      while ($cvterm_id = $results->fetchField()) {
        $bims_cvterm = new BIMS_CVTERM($cvterm_id, $program_id);
        if (!$bims_cvterm->deleteCvterm()) {
          throw new Exception("Fail to delete the cvterm");
        }
      }

      // Deletes from bims_imported_cv.
      db_delete('bims_imported_cv')
        ->condition('program_id', $program_id, '=')
        ->condition('cv_id', $cv_id, '=')
        ->execute();
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Deletes the cvterm.
   *
   * @retrun boolean
   */
  public function deleteCvterm() {

    $transaction = db_transaction();
    try {

      // Gets the callbacks.
      $callbacks = $this->deleteCallbacks($this->cvterm_id);

      // Deletes the data in BIMS_CHADO.
      if (!$this->deleteDataBIMS($this->cvterm_id)) {
        throw new Exception("Fail to delete the data in BIMS_CHADO");
      }

      // Deletes the data in BIMS_MVIEW.
      if (!$this->deleteDataMView($this->cvterm_id)) {
        throw new Exception("Fail to delete the data in BIMS_MVIEW");
      }

      // Deletes the data in public.
      if (!$this->deleteDataPublic($this->cvterm_id)) {
        throw new Exception("Fail to delete the data (cvterm) in public schema");
      }

      // Deletes the cvterm from chado.cvterm.
      if (!$this->isChado()) {
        $cvterm = MCL_CHADO_CVTERM::byID($this->cvterm_id);
        if (!$cvterm->delete()) {
          throw new Exception("Error : Failed to delete the cvterm from chado.cvterm");
        }
      }

      // Submit the callbacks.
      if (!empty($callbacks)) {
        if (!bims_submit_drush_cmds($this->program_id, 'delete-cvterm', $callbacks)) {
          throw new Exception("Error : Failed to submit callbacks");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets the callback called after deletion.
   *
   * @param integer $cvterm_id
   *
   * @return string
   */
  public function deleteCallbacks($cvterm_id) {
    $callbacks = array();

    // If this cvterm is a descriptor, find all the associated phenotype trials.
    $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($this->program_id, $cvterm_id);
    if ($descriptor) {

      // Gets the related projects and samples.
      $project_id_arr = array();
      $stock_id_arr   = array();
      $cross_id_arr   = array();
      $sample_id_arr  = array();

      // BIMS_MVIEW_PHENOTYPE.
      $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->program_id));
      $m_phenotype  = $bm_phenotype->getMView();
      $field        = "t$cvterm_id";

      // Checks if the field exists.
      if (!db_field_exists($m_phenotype, $field)) {
        return $callbacks;
      }
      $sql = "
        SELECT DISTINCT project_id, stock_id, nd_experiment_id, sample_id
        FROM {$m_phenotype} WHERE ($field IS NOT NULL OR $field != '')
      ";
      $results = db_query($sql);
      while ($obj = $results->fetchObject()) {
        $project_id_arr[$obj->project_id] = TRUE;
        $stock_id_arr[$obj->stock_id]     = TRUE;
        $sample_id_arr[$obj->sample_id]   = TRUE;
        if ($obj->nd_experiment_id) {
          $cross_id_arr[$obj->nd_experiment_id] = TRUE;
        }
      }
      if (!empty($project_id_arr)) {
        $id_list    = implode(':', array_keys($project_id_arr));
        $args       = $this->program_id . " --group=project --group_ids=$id_list --mview=stats";
        $callbacks  []= "bims-update-mview-phenotype $args";
      }
      if (!empty($sample_id_arr)) {
        $id_list    = implode(':', array_keys($sample_id_arr));
        $args       = $this->program_id . " --sample_ids=$id_list";
        $callbacks  []= "bims-update-mview-sample-numbers $args";
      }
      if (!empty($cross_id_arr)) {
        $id_list    = implode(':', array_keys($cross_id_arr));
        $args       = $this->program_id . " --group=cross --group_ids=$id_list --mview=stats";
        $callbacks  []= "bims-update-mview-cross $args";
      }
      if (!empty($stock_id_arr)) {
        $id_list    = implode(':', array_keys($stock_id_arr));
        $args       = $this->program_id . " --group=stock --group_ids=$id_list --mview=stats";
        $callbacks  []= "bims-update-mview-stock $args";
      }
    }
    return $callbacks;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the program ID.
   *
   * @retrun integer
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Sets the program ID.
   *
   * @param integer $program_id
   */
  public function setProgramID($program_id) {
    $this->program_id = $program_id;
  }

  /**
   * Retrieves the cv ID.
   *
   * @retrun integer
   */
  public function getCvID() {
    return $this->cv_id;
  }

  /**
   * Sets the cv ID.
   *
   * @param integer $cv_id
   */
  public function setCvID($cv_id) {
    $this->cv_id = $cv_id;
  }

  /**
   * Retrieves the cv name.
   *
   * @retrun string
   */
  public function getCvName() {
    return $this->cv_name;
  }

  /**
   * Retrieves the cvterm ID.
   *
   * @retrun integer
   */
  public function getCvtermID() {
    return $this->cvterm_id;
  }

  /**
   * Sets the cvterm ID.
   *
   * @param integer $cvterm_id
   */
  public function setCvtermID($cvterm_id) {
    $this->cvterm_id = $cvterm_id;
  }

  /**
   * Retrieves the cvterm name.
   *
   * @retrun string
   */
  public function getCvtermName() {
    return $this->cvterm_name;
  }
}