<?php
/**
 * The declaration of BIMS_INFO class.
 *
 */
class BIMS_INFO extends PUBLIC_BIMS_INFO {

  /**
   *  Data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_INFO::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_INFO::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_INFO by name.
   *
   * @param string $name
   *
   * @return BIMS_INFO
   */
  public static function byName($name) {
    return self::byKey(array('name' => $name));
  }

  /**
   * @see PUBLIC_BIMS_INFO::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_INFO::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_INFO::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Adds the BIMS information.
   *
   * @param array $details
   *
   * @return boolean
   */
  public static function addInfo($details) {
    $transaction = db_transaction();
    try {
      $new_info = new BIMS_IFNO($details);
      if (!$new_info->insert()) {
        throw new Exception("Failed to add a new site");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      throw new DrupalUpdateException($error);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Exports the BIMS information.
   *
   * @param BIMS_USER $bims_user
   * @param string $prefix
   * @param string $dest_dir
   *
   * @return string
   */
  public static function export(BIMS_USER $bims_user, $prefix = 'bims_export_info', $dest_dir = 'exported') {

    // Gets the destination directory.
    $exported_dir = $bims_user->getPath($dest_dir);

    // Sets the filepath.
    $filename = $prefix . date("-Y-m-d-G-i-s") . '.csv';
    $filepath = "$exported_dir/$filename";

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return FALSE;
    }

    // Exports the BIMS info.
    $sql = "SELECT I.* FROM {bims_info} I ORDER BY I.name";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      fputcsv($fdw, array($obj->name, $obj->label, $obj->prop));
    }
    fclose($fdw);
    return $filepath;
  }

  /**
   * Imports the BIMS information.
   *
   * @param BIMS_USER $bims_user
   * @param string $filename
   * @param boolean $flag_update
   *
   * @return boolean
   */
  public static function import(BIMS_USER $bims_user, $filename, $flag_update = FALSE) {

    // Checks the file.
    $filepath = $bims_user->getUserDir() . '/'. $filename;
    if (!file_exists($filepath)) {
      bims_print("Error : The $filepath does not exist", 1, 2);
      return FALSE;
    }

    // Creates a backup.
    if ($flag_update) {
      $backup = self::export($bims_user, 'bims_backup_info', 'backup');
      if (!file_exists($backup)) {
        return FALSE;
      }
    }

    // Imports the BIMS info.
    if (!($fdr = fopen($filepath, 'r'))) {
      bims_print("Error : Failed to open the BIMS information file", 1, 2);
      return FALSE;
    }
    while (!feof($fdr)) {
      $line = fgetcsv($fdr);
      if (!empty($line)) {

        // Adds a site if not exist.
        $name = $line[0];
        $details = array(
          'name'  => $name,
          'label' => $line[1],
          'prop'  => $line[2],
        );
        $bims_info = self::byName($name);
        if ($bims_info) {
          if ($flag_update) {
            $bims_info->setLabel($details['label']);
            $prop_arr = json_decode($details['prop'], TRUE);
            if (array_key_exists('description', $prop_arr)) {
              $bims_info->setPropByKey('description', $prop_arr['description']);
            }
            if ($bims_info->update()) {
              bims_print("'$name' has been updated", 1, 2);
            }
            else {
              bims_print("Error : Failed to update '$name'", 1, 2);
            }
          }
          else {
            bims_print("'$name' has already existed", 1, 2);
          }
        }
        else {
          if (self::addInfo($details)) {
            bims_print("'$name' has been added", 1, 2);
          }
          else {
            bims_print("Error: Failed to add BIMS info ($name)", 1, 2);
            return FALSE;
          }
        }
      }
    }
    fclose($fdr);
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the value of the given key in prop.
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}