<?php
/**
 * The declaration of BIMS_COLOR class.
 *
 */
class BIMS_COLOR extends PUBLIC_BIMS_COLOR {

  /**
   * Class data members.
   */
  /**
   * @see PUBLIC_BIMS_COLOR::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see PUBLIC_BIMS_COLOR::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_COLOR by color ID.
   *
   * @param integer $color_id
   *
   * @return BIMS_COLOR|NULL
   */
  public static function byID($color_id) {
    if (!bims_is_int($color_id)) {
      return NULL;
    }
    return self::byKey(array('color_id' => $color_id));
  }

  /**
   * Returns BIMS_COLOR by code.
   *
   * @param string $code
   *
   * @return BIMS_COLOR|NULL
   */
  public static function byCode($code) {
    return self::byKey(array('code' => $code));
  }

  /**
   * @see PUBLIC_BIMS_COLOR::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Returns the colors.
   *
   * @param string $type
   * @param integer $flag
   *
   * @return array
   */
  public static function getColors($type, $flag = BIMS_OBJECT) {

    // Gets the colors.
    $sql = "
      SELECT *
      FROM {bims_color}
      WHERE LOWER(type) = LOWER(:type)
      ORDER BY rank, code
    ";
    $results = db_query($sql, array(':type' => $type));
    $colors = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $colors[$obj->color_id] = $obj->code;
      }
      else if ($flag == BIMS_CLASS) {
        $colors[] = BIMS_COLOR::byID($obj->color_id);
      }
      else {
        $colors []= $obj;
      }
    }
    return $colors;
  }

  /**
   * Returns the color codes.
   *
   * @param string $type
   * @param integer $num
   *
   * @return array
   */
  public static function getCodes($type, $num) {
    $sql = "
      SELECT code
      FROM {bims_color}
      WHERE LOWER(type) = LOWER(:type)
      ORDER BY rank, code
    ";
    $results = db_query($sql, array(':type' => $type));
    $codes = array();
    while ($code = $results->fetchField()) {
      $codes []= $code;
    }
    return $codes;
  }

  /**
   * Returns the code.
   *
   * @param string $type
   * @param integer $idx
   *
   * @return string
   */
  public static function getCodeByIdx($type, $idx = 0) {

    // Gets the colors.
    $sql = "
      SELECT code FROM {bims_color}
      WHERE LOWER(type) = LOWER(:type)
      ORDER BY rank, code
    ";
    $results = db_query($sql, array(':type' => $type));
    $codes = array();
    $size = 0;
    while ($code = $results->fetchField()) {
      $size++;
      $codes []= $code;
    }
    $idx = $idx % $size;
    return $codes[$idx];
  }

  /**
   * Add a color.
   *
   * @param array $details
   *
   * @return BIMS_COLOR
   */
  public static function addColor($details) {

    $transaction = db_transaction();
    try {

      // Checks for duplication
      $code = $details['code'];
      $bims_color = BIMS_COLOR::byCode($code);
      if (!$bims_color) {

        // Adds a color.
        $bims_color = new BIMS_COLOR($details);
        if (!$bims_color->insert()) {
          throw new Exception("Fail to add a color");
        }
      }
      return $bims_color;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
    return NULL;
  }

  /**
   * Return the colors.
   */

  /**
   * Return the gray colors.
   */

  /**
   * Return the color.
   */

  /**
   * Return the gray color.
   */


}