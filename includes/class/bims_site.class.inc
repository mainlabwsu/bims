<?php
/**
 * The declaration of BIMS_SITE class.
 *
 */
class BIMS_SITE extends PUBLIC_BIMS_SITE {

  /**
   *  Data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_SITE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_SITE::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_SITE by ID.
   *
   * @param integer $site_id
   *
   * @return BIMS_SITE
   */
  public static function byID($site_id) {
    if (!bims_is_int($site_id)) {
      return NULL;
    }
    return self::byKey(array('site_id' => $site_id));
  }

  /**
   * Returns BIMS_SITE by label.
   *
   * @param string $label
   *
   * @return BIMS_SITE
   */
  public static function byLabel($label) {
    return self::byKey(array('label' => $label));
  }

  /**
   * @see PUBLIC_BIMS_SITE::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_SITE::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_SITE::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Returns the options for the site types.
   *
   * @return array
   */
  public static function getSiteTypeOpts() {
    return array(
      'University' => 'University',
      'Government' => 'Government',
    );
  }

  /**
   * Returns the sites.
   *
   * @param integer $flag
   *
   * @return boolean
   */
  public static function getSites($flag = BIMS_OBJECT) {

    // Gets the sites.
    $sql = "SELECT * FROM {bims_site} ORDER BY label";
    $results = db_query($sql);
    $sites = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $sites[$obj->site_id] = $obj->label;
      }
      else if ($flag == BIMS_CLASS) {
        $sites[] = BIMS_SITE::byID($obj->site_id);
      }
      else {
        $sites []= $obj;
      }
    }
    return $sites;
  }

  /**
   * Adds the site.
   *
   * @param array $details
   *
   * @return boolean
   */
  public static function addSite($details) {
    $transaction = db_transaction();
    try {
      $new_site = new BIMS_SITE($details);
      if (!$new_site->insert()) {
        throw new Exception("Failed to add a new site");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      throw new DrupalUpdateException($error);
     return FALSE;
    }
    return TRUE;
  }

  /**
   * Exports the sites.
   *
   * @param BIMS_USER $bims_user
   * @param string $prefix
   * @param string $dest_dir
   *
   * @return string
   */
  public static function export(BIMS_USER $bims_user, $prefix = 'bims_export_site', $dest_dir = 'exported') {

    // Gets the destination directory.
    $exported_dir = $bims_user->getPath($dest_dir);

    // Sets the filepath.
    $filename = $prefix . date("-Y-m-d-G-i-s") . '.csv';
    $filepath = "$exported_dir/$filename";

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return FALSE;
    }

    // Exports the sites.
    $sql = "SELECT S.* FROM {bims_site} S ORDER BY S.label";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $row = array(
        $obj->label,
        $obj->type,
        $obj->latitude,
        $obj->longitude,
        $obj->icon,
        $obj->prop,
        $obj->notes,
      );
      fputcsv($fdw, $row);
    }
    fclose($fdw);
    return $filepath;
  }

  /**
   * Imports the sites.
   *
   * @param BIMS_USER $bims_user
   * @param string $filename
   * @param boolean $flag_update
   *
   * @return boolean
   */
  public static function import(BIMS_USER $bims_user, $filename, $flag_update = FALSE) {

    // Checks the file.
    $filepath = $bims_user->getUserDir() . '/'. $filename;
    if (!file_exists($filepath)) {
      bims_print("Error : The $filepath does not exist", 1, 2);
      return FALSE;
    }

    // Creates a backup.
    if ($flag_update) {
      $backup = self::export($bims_user, 'bims_backup_site', 'backup');
      if (!file_exists($backup)) {
        return FALSE;
      }
    }

    // Imports the sites.
    if (!($fdr = fopen($filepath, 'r'))) {
      bims_print("Error : Failed to open the site information file", 1, 2);
      return FALSE;
    }
    while (!feof($fdr)) {
      $line = fgetcsv($fdr);
      if (!empty($line)) {

        // Adds a site if not exist.
        $label = $line[0];
        $details = array(
          'label'     => $label,
          'type'      => $line[1],
          'latitude'  => $line[2],
          'longitude' => $line[3],
          'icon'      => $line[4],
          'prop'      => $line[5],
          'notes'     => $line[6],
        );
        $bims_site = self::byLabel($label);
        if ($bims_site) {
          if ($flag_update) {
            $bims_site->setType($details['type']);
            $bims_site->setLatitude($details['latitude']);
            $bims_site->setLongitude($details['longitude']);
            $bims_site->setIcon($details['icon']);
            $bims_site->setProp($details['prop']);
            $bims_site->setNotes($details['notes']);
            if ($bims_site->update()) {
              bims_print("'$label' has been updated", 1, 2);
            }
            else {
              bims_print("Error : Failed to update '$label'", 1, 2);
            }
          }
          else {
            bims_print("'$label' has already existed", 1, 2);
          }
        }
        else {
          if (self::addSite($details)) {
            bims_print("'$label' has been added", 1, 2);
          }
          else {
            bims_print("Error: Failed to add a site ($label)", 1, 2);
            return FALSE;
          }
        }
      }
    }
    fclose($fdr);
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the prop array.
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}