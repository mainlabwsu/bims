 <?php
/**
 * The declaration of BIMS_JOB_GPT class.
 *
 */
class BIMS_JOB_GPT extends BIMS_JOB {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_JOB::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_JOB::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_JOB_GPT by job ID.
   *
   * @param integer $job_id
   *
   * @return BIMS_JOB|NULL
   */
  public static function byID($job_id) {
    if (!bims_is_int($job_id)) {
      return NULL;
    }
    return self::byKey(array('job_id' => $job_id));
  }

  /**
   * @see BIMS_JOB::insert()
   */
  public function insert() {
    return parent::insert();
  }

  /**
   * @see BIMS_JOB::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Returns the filepath of the given types of result file.
   *
   * @return string
   */
  public function getResultsFile($type) {
    if (preg_match("/^(csv|table|log)$/", $type)) {
      return $this->getWorkingDir() . '/gpt_results.' . $type;
    }
    return '';
  }

  /**
   * Checks if the given types of result file exists.
   *
   * @return boolean
   */
  public function isResultsFile($type) {
    if (preg_match("/^(csv|table|log)$/", $type)) {
      return file_exists($this->getResultsFile($type));
    }
    return FALSE;
  }

  /**
   * @see BIMS_JOB::getJobs()
   */
  public static function getJobs($user_id, $flag = BIMS_OBJECT, $status = '') {

    // Gets tool ID.
    $bims_tool = BIMS_TOOL::byName('gpt');

    // Gets all GPT jobs.
    $sql = "
      SELECT * FROM {bims_job}
      WHERE user_id = :user_id AND tool_id = :tool_id
      ORDER BY submit_date, name
    ";
    $args = array(
      ':user_id' => $user_id,
      ':tool_id' => $bims_tool->getToolID(),
    );
    $results = db_query($sql, $args);
    $jobs = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $jobs[$obj->job_id] = $obj->name;
      }
      else if ($flag == BIMS_CLASS) {
        $jobs []= BIMS_JOB_GPT::byID($obj->job_id);
      }
      else {
        $jobs []= $obj;
      }
    }
    return $jobs;
  }

  /**
   * Add a job.
   *
   * @param array $details
   *
   * @return BIMS_JOB_GPT
   */
  public static function addJob($details) {

    $transaction = db_transaction();
    try {

      // Gets the BIMS_TOOL.
      $bims_tool = BIMS_TOOL::byName('gpt');

      // Updates the $details.
      $details['tool_id']     = $bims_tool->getToolID();
      $details['submit_date'] = date("Y-m-d G:i:s");

      // Adds a job.
      $bims_job_gpt = new BIMS_JOB_GPT($details);
      if (!$bims_job_gpt->insert()) {
        throw new Exception("Fail to add a job");
      }
      return $bims_job_gpt;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
    return NULL;
  }

  /**
   * Returns the number of the GPT jobs owned by the user.
   *
   * @param integer $user_id
   *
   * @return integer
   */
  public static function getNumJobs($user_id) {
    $gpt = BIMS_TOOL::byName('gpt');
    $sql = "
      SELECT COUNT(job_id) FROM {bims_job}
      WHERE tool_id = :tool_id AND user_id = :user_id
    ";
    $args = array(
      'tool_id' => $gpt->getToolID(),
      'user_id' => $user_id,
    );
    return db_query($sql, $args)->fetchField();
  }

  /**
   * Converts
   *
   * @param string $value
   *
   * @return string
   */
   private function _convNumeric($value) {
     if ($value == 'AA') {
       return '0';
     }
     else if ($value == 'AB') {
       return '1';
     }
     else if ($value == 'BB') {
       return '2';
     }
     else if ($value == 'NA') {
       return 'NA';
     }
     return 'err';
   }

   /**
    * Moves the all files from the previous run.
    */
   private function _movePrevRun() {

     // Checks the 'prev' foulder.
     $prev_dir = $this->getWorkingDir() . '/prev';
     bims_create_dir($prev_dir);

     // Moves the file.


   }

  /**
   * Processes GPT.
   *
   * P = M * B /ns
   *
   * P = M (uploaded file) x B (B-DATA file) / ns
   *
   * where
   *   M is accession x locins
   *   B is loci x environment
   *   ns is the number of SNP loci in M
   *
   * @param array $genotype_mapping
   *
   * @return boolean
   */
  public function _processGPT($genotype_mapping) {

    // Gets the variables.
    $regex    = $genotype_mapping['regex'];
    $mapping  = array('AA' => '0', 'AB' => '1', 'BB' => '2', 'NA' => 'NA');
    $gpt_separator = ",";

    // Opens the log file.
    $log_file = $this->getResultsFile('log');
    if (!($fd_log = fopen($log_file, 'w'))) {
      bims_print("Cannot open a file for a log ($log_file)", 1, 1);
      return FALSE;
    }

    // Gets the uploaded file
    $m_file = $this->getPropByKey('uploaded');
    if (!file_exists($m_file)) {
      fputs($fd_log, "Cannot open the uploaded file ($m_file).");
      return FALSE;
    }

    // Gets BIMS_BDATA.
    $bdata_id     = $this->getPropByKey('bdata_id');
    $bims_bdata   = BIMS_BDATA::byID($bdata_id);
    $b_separator  = $bims_bdata->getPropByKey('separator');
    $num_env      = $bims_bdata->getPropByKey('num_env');
    $means        = $bims_bdata->getPropByKey('means');
    $precision    = $bims_bdata->getPropByKey('precision');
    $format       = '%.0' . $precision . 'f';

    // Opens the B-DATA file.
    $b_file       = $bims_bdata->getFilepath();
    if (!file_exists($b_file)) {
      fputs($fd_log, "Cannot open the B-DATA file ($b_file).");
      return FALSE;
    }

    // Checks the makers in B.
    if (!($fd_b = fopen($b_file, 'r'))) {
      fputs($fd_log, "Cannot open file ($b_file).");
      return FALSE;
    }
    $b_markers    = array();
    $flag_env     = FALSE;
    $flag_mean    = FALSE;
    $env_arr     = array();
    while ($line = fgets($fd_b)) {
      $line = trim($line);
      if (!$line) {
        continue;
      }

      // Header section.
      if (preg_match("/^#/", $line)) {

        // Environment row.
        if (!$flag_env) {
          $flag_env = TRUE;
          $env_arr = explode($b_separator, $line);
          continue;
        }

        // Mean row.
        else if (!$flag_mean) {
          $flag_mean = TRUE;
          continue;
        }
      }

      // Data section.
      else {

        // Gets the marker name from the first row.
        $tmp = explode($b_separator, $line);
        $marker = strtolower(trim($tmp[0]));
         $b_markers[$marker]++;
        if ($b_markers[$marker] > 1) {
          fputs($fd_log, "Error : B-DATA file has duplicated markers ($b_file - $marker).");
          return FALSE;
        }
      }
    }
    fclose($fd_b);

    // Checks the markers in M.
    if (!($fd_m = fopen($m_file, 'r'))) {
      fputs($fd_log, "Error : Cannot open file ($m_file).");
      return FALSE;
    }
    $common_markers = array();
    $line = fgets($fd_m);
    fclose($fd_m);

    // Gets the separater of M file.
    $m_separator = $this->getDelimiter($line);
    if (!$m_separator) {
      fputs($fd_log, "Error : Cannot find the delimiter of the file ($m_file).");
      return FALSE;
    }

    // Finds the common markers.
    $tmp = explode($m_separator, $line);
    for ($i = 1; $i < sizeof($tmp); $i++) {
      $marker = trim(strtolower($tmp[$i]));
      if (array_key_exists($marker, $b_markers)) {
        $common_markers[$marker]++;
      }
      if ($common_markers[$marker] > 1) {
        fputs($fd_log, "Error : The uploaded file has duplicated markers ($m_file - $marker).");
        return FALSE;
      }
    }

    // Creates the cleaned M file.
    $m_cleaned = $this->getWorkingDir() . '/m_cleaned.tab';
    if (!($fd_m = fopen($m_file, 'r'))) {
      fputs($fd_log, "Error : Cannot open file ($m_file).");
      return FALSE;
    }
    if (!($fd_mc = fopen($m_cleaned, 'w'))) {
      fputs($fd_log, "Error : Cannot open file ($m_cleaned).");
      return FALSE;
    }
    $header_flag = FALSE;
    $map = array(TRUE);
    $tmp = NULL;
    $row = NULL;
    $num_cols = 0;
    $idx_markers = array();
    while ($line = fgets($fd_m)) {
      $line = trim($line);
      if (!$line) {
        continue;
      }

      // Processes the header line.
      if (!$header_flag) {
        $header_flag = TRUE;
        $tmp = explode($m_separator, $line);
        $num_cols = sizeof($tmp);
        $row = array($tmp[0]);
        $idx = 1;
        for ($i = 1; $i < $num_cols; $i++) {
          $marker     = trim($tmp[$i]);
          $marker_lc  = strtolower($marker);
          if (array_key_exists($marker_lc, $common_markers)) {
            $map[$i] = TRUE;
            $row []= $marker;
            $idx_markers[$idx] = $marker_lc;
            $idx++;
          }
        }
        fputs($fd_mc, implode($m_separator, $row) . "\n");
        continue;
      }

      // Writes only comman marker columns.
      $tmp = explode($m_separator, $line);
      $row = array($tmp[0]);
      for ($i = 1; $i < $num_cols; $i++) {
        if ($map[$i]) {
          $value = $tmp[$i];
          if (!preg_match($regex, $value)) {
            $value = $this->_convNumeric($value);
            if ($value == 'err') {
              fputs($fd_log, 'Error : Invalid genotype found (' . $tmp[$i] . ').');
              return FALSE;
            }
          }
          $row []= $value;
        }
      }
      fputs($fd_mc, implode($m_separator, $row) . "\n");
    }
    fclose($fd_m);
    fclose($fd_mc);

    // Creates the cleaned B file.
    $b_cleaned = $this->getWorkingDir() . '/b_cleaned.tab';
    if (!($fd_b = fopen($b_file, 'r'))) {
      fputs($fd_log, "Error : Cannot open file ($b_file).");
      return FALSE;
    }
    if (!($fd_bc = fopen($b_cleaned, 'w'))) {
      fputs($fd_log, "Error : Cannot open file ($b_cleaned).");
      return FALSE;
    }

    // Opens the files for all environments.
    $tmp_dir = $this->getTmpDir();
    $fdw = array();
    $env_filepaths = array();
    for ($i = 1; $i <= $num_env; $i++) {
      $row_file = "$tmp_dir/row$i";
      $env_filepaths[$i] = $row_file;
      if (!($fdw[$i] = fopen($row_file, 'w'))) {
        fputs($fd_log, "Error : Cannot open file ($row_file).");
        return FALSE;
      }
    }
    while ($line = fgets($fd_b)) {
      $line = trim($line);
      if (!$line) {
        continue;
      }
      $tmp = explode($b_separator, $line);
      $marker_lc = strtolower(trim($tmp[0]));
      if (array_key_exists($marker_lc, $common_markers)) {
        for ($i = 1; $i <= $num_env; $i++) {
          fputs($fdw[$i], sprintf(",\"%s\":\"%f\"", $marker_lc, $tmp[$i]));
        }
        fputs($fd_bc, $line);
      }
    }

    // Closes the files.
    for ($i = 1; $i <= $num_env; $i++) {
      fclose($fdw[$i]);
    }
    fclose($fd_b);
    fclose($fd_bc);

    // Multiplies the cleaned matrix B and M.
    if (!($fd_m = fopen($m_cleaned, 'r'))) {
      fputs($fd_log, "Error : Cannot open file ($m_cleaned).");
      return FALSE;
    }
    $result_file = $this->getResultsFile('csv');
    $table_file  = $this->getResultsFile('table');
    if (!($fd_gpt = fopen($result_file, 'w'))) {
      fputs($fd_log, "Error : Cannot open file ($result_file)");
      return FALSE;
    }
    if (!($fd_table = fopen($table_file, 'w'))) {
      fputs($fd_log, "Error : Cannot open file ($table_file)");
      return FALSE;
    }
    $total_lines  = mcl_get_num_lines($m_cleaned);
    $gpt_results  = array();
    $line_arr     = array();
    $header_flag  = FALSE;
    $mean_arr     = array('#mean');
    $line_no      = 0;
    $tr           = '';
    while ($line = fgets($fd_m)) {
      $line = trim($line);
      if (!$line) {
        continue;
      }
      $line_no++;
      $tmp = explode($m_separator, $line);
      if (!$header_flag) {
        $header_flag = TRUE;

        // Adds 'environment' row.
        fputcsv($fd_gpt, $env_arr);

        // Adds 'mean' row.
        $tr_env = '';
        $tr_mean = '';
        for ($i = 1; $i <= $num_env; $i++) {
          $mean_arr[$i] = $means[strtolower($env_arr[$i])];
          $tr_env .= '<th>' . $env_arr[$i] . '</th>';
          $tr_mean .= '<th>' . sprintf($format, $mean_arr[$i]) . '</th>';
        }
        fputcsv($fd_gpt, $mean_arr);
        fputs($fd_table, "<thead><tr><th>#environment</th>$tr_env</tr>");
        fputs($fd_table, "<tr><th>#mean</th>$tr_mean</tr></thead><tbody>");
        continue;
      }

      // Processes the line.
      $line_arr = explode($m_separator, $line);
      $arr = $this->_gptCrossProduct($fd_log, $line_arr, $idx_markers, $env_filepaths, $mean_arr, $format);
      fputcsv($fd_gpt, $arr);
      fputs($fd_table, '<tr><td>' . implode('</td><td>', $arr) . '</td></tr>');
      if ($line_no % 2 == 0) {
        mcl_display_memory_usage("MEMORY-CHECK = $line_no");
        $progress = sprintf("GPT : %d / %d = %.03f %% [Processing]", $line_no, $total_lines, $line_no / $total_lines * 100.0);
        bims_print($progress, 1, 1, '', FALSE);
        $this->setProgress($progress);
      }
    }
    fputs($fd_table, '</tbody>');
    fclose($fd_gpt);
    fclose($fd_table);
    fclose($fd_m);

    // Saves the results.
    $this->setPropByKey('b_cleaned', $b_cleaned);
    $this->setPropByKey('m_cleaned', $m_cleaned);
    $this->setPropByKey('gpt_results', $result_file);
    $this->setPropByKey('log_file', $log_file);
    $ret_status = TRUE;
    if ($this->update()) {
      fputs($fd_log, "Job completed successfully.");
    }
    else {
      fputs($fd_log, "Error : Failed to update the job status.");
      $ret_status = FALSE;
    }
    fclose($fd_log);
    return $ret_status;
  }

  /**
   * Returns the results.
   *
   * @param string $fd_log
   * @param array $line_arr
   * @param array $idx_markers
   * @param array $env_filepaths
   * @param array $mean_arr
   * @param string $format
   *
   * @return array
   */
  private function _gptCrossProduct($fd_log, $line_arr, &$idx_markers, &$env_filepaths, &$mean_arr, $format) {

    // Performs the cross products.
    $row = array($line_arr[0]);
    $size = sizeof($line_arr);
    $num_markers = 0;
    $mean = 0.0;
    foreach ($env_filepaths as $idx => $env_filepath) {
      $json = '{' . substr(file_get_contents($env_filepath), 1) . '}';
      $weight_by_markers = json_decode($json, TRUE);
      $mean = $mean_arr[$idx];

      // Gets the sum of the product.
      $sum = 0.0;
      for ($i = 1; $i < $size; $i++) {
        if ($line_arr[$i] != 'NA') {
          $num_markers++;
          $sum += $line_arr[$i] * $weight_by_markers[$idx_markers[$i]];

          //$ma = $idx_markers[$i];
          //print sprintf("$ma : $i : %.10f\n", $weight_by_markers[$idx_markers[$i]]);
          //print sprintf("%.10f%.10f%.10f\n", $sum, $tmp[$i], $weight_by_markers[$idx_markers[$i]]);
        }
      }

      // Gets the average and save it for this accesion for the current environment.
      $value = $mean + $sum / $num_markers;
      $row[$idx] = sprintf($format, $value);
      fputs($fd_log, sprintf("%s = %s + %d / %d\n", $row[$idx], $mean, $sum, $num_markers));
    }
    return $row;
  }

  /**
   * Initializes the job.
   *
   * @return boolean
   */
  public function init() {

    // Sets the status.
    $this->setStatus(10);
    $this->update();

    // Re-creates the temporary directory.
    $this->clearTmpDir();

    // Moves all the files from the previous run.
    $this->_movePrevRun();

    // Resets the result files.
    $this->setPropByKey('b_cleaned', '');
    $this->setPropByKey('m_cleaned', '');
    $this->setPropByKey('gpt_results', '');
    return $this->update();
  }

  /**
   * Runs the job.
   *
   * @return boolean
   */
  public function run() {

    // Initializes the job.
    if (!$this->init()) {
      bims_print("Error : Failed to initialize the job", 1, 1);
      return FALSE;
    }

    // Processes the GPT.
    $genotype_mapping = array(
      'regex' => "/^[012]$/",
    );
    if ($this->_processGPT($genotype_mapping)) {
      $this->setProgress("Job completed successfully");
      $this->setStatus(100);
    }
    else {
      $this->setProgress("Job failed");
      $this->setStatus(-200);
    }
    $this->setCompleteDate(date("Y-m-d G:i:s"));
    return $this->update();
  }
}