<?php
/**
 * The declaration of BIMS_RESOURCE class.
 *
 */
class BIMS_RESOURCE extends PUBLIC_BIMS_RESOURCE {

  /**
   * Class data members.
   */
  /**
   * @see PUBLIC_BIMS_RESOURCE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see PUBLIC_BIMS_RESOURCE::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_RESOURCE by resource ID.
   *
   * @param integer $resource_id
   *
   * @return BIMS_RESOURCE|NULL
   */
  public static function byID($resource_id) {
    return self::byKey(array('resource_id' => $resource_id));
  }

  /**
   * @see PUBLIC_BIMS_RESOURCE::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Returns the resources.
   *
   * @param integer $flag
   *
   * @return BIMS_RESOURCE
   */
  public static function getResources($flag = BIMS_OBJECT) {
    $sql = "SELECT R.* FROM {bims_resource} R ORDER BY R.type, R.name";
    $results = db_query($sql);
    $resources = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_CLASS) {
        $resources []= BIMS_RESOURCE::byID($obj->resource_id);
      }
      else if ($flag == BIMS_OPTION) {
        $resources[$obj->resource_id] = $obj->name;
      }
      else {
        $resources []= $obj;
      }
    }
    return $resources;
  }

  /**
   * Add a resource.
   *
   * @param array $details
   *
   * @return BIMS_RESOURCE
   */
  public static function addResource($details) {
    $bims_resource = NULL;
    $transaction = db_transaction();
    try {

      // Adds a resource.
      $name = $details['name'];
      $bims_resource = new BIMS_RESOURCE($details);
      if (!$bims_resource->insert()) {
        throw new Exception("Fail to add a resource ($name)");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('bims', $e->getMessage(), array(), WATCHDOG_ERROR);

    }
    return $bims_resource;
  }
}