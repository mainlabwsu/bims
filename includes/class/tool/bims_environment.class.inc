<?php
/**
 * The declaration of BIMS_ENVIRONMENT class.
 *
 */
class BIMS_ENVIRONMENT extends PUBLIC_BIMS_ENVIRONMENT {

  /**
   * Class data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_ENVIRONMENT::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_ENVIRONMENT::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_ENVIRONMENT by environment ID.
   *
   * @param integer $environment_id
   *
   * @return BIMS_ENVIRONMENT|NULL
   */
  public static function byID($environment_id) {
    if (!bims_is_int($environment_id)) {
      return NULL;
    }
    return self::byKey(array('environment_id' => $environment_id));
  }

  /**
   * @see PUBLIC_BIMS_ENVIRONMENT::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_ENVIRONMENT::insert()
   */
  public function insert() {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Insert data.
    if (parent::insert()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @see PUBLIC_BIMS_ENVIRONMENT::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Adds an environment.
   *
   * @param array $details
   *
   * @return BIMS_ENVIRONMENT
   */
  public static function addEnvironment($details) {
    $bims_environment = NULL;
    $transaction = db_transaction();
    try {

      // Checks for duplication.
      $keys = array(
        'name'      => $details['name'],
        'bdata_id'  => $details['bdata_id'],
      );
      $bims_environment = BIMS_ENVIRONMENT::byKey($keys);
      if (!$bims_environment) {

        // Adds an environment.
        $bims_environment = new BIMS_ENVIRONMENT($details);
        if (!$bims_environment->insert()) {
          throw new Exception("Fail to add an environment");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return $bims_environment;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the prop array.
   *
   * @return array
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   *
   * @param string $key
   *
   * @return various
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * $param string $key
   * $param string $value
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}