<?php
/**
 * The declaration of BIMS_BDATA class.
 *
 */
class BIMS_BDATA extends PUBLIC_BIMS_BDATA {

  /**
   * Class data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_BDATA::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_BDATA::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_BDATA by bdata ID.
   *
   * @param integer $bdata_id
   *
   * @return BIMS_BDATA|NULL
   */
  public static function byID($bdata_id) {
    if (!bims_is_int($bdata_id)) {
      return NULL;
    }
    return self::byKey(array('bdata_id' => $bdata_id));
  }

  /**
   * @see PUBLIC_BIMS_BDATA::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_BDATA::insert()
   */
  public function insert() {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Insert data.
    if (parent::insert()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @see PUBLIC_BIMS_BDATA::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * @see PUBLIC_BIMS_BDATA::delete()
   */
  public function delete() {

    // Kills the process if exists and is running.
    $pid = $this->getPropByKey('pid');
    if ($pid) {
      if (file_exists("/proc/$pid" )) {
        exec("kill -9 $pid", $output, $status);
      }
    }

    // Deletes the all environments associated with this B-DATA.
    db_delete('bims_environment')
      ->condition('bdata_id', $this->bdata_id, '=')
      ->execute();

    // Deletes the working directory.
    $working_dir = $this->getWorkingDir();
    if (file_exists($working_dir)) {
      bims_remove_dir($working_dir);
    }
    return parent::delete();
  }

  /**
   * Validats the uploaded file. Checks the first 2 lines.
   *
   * @param object $drupal_file
   *
   * @return string
   */
  public static function validateFile($drupal_file) {

    // Gets the filepath.
    $filepath  = drupal_realpath($drupal_file->uri);

    // Checks the first 2 lines.
    if (!($fdr = fopen($filepath, 'r'))) {
      return "Cannot open file ($filepath).";
    }
    $line_env   = '';
    $line_mean  = '';
    while ($line = fgets($fdr)) {
      $line = trim($line);
      if (!$line) {
        continue;
      }

      // Obtains the required headers.
      if (preg_match("/^#env/", $line)) {
        $line_env = $line;
        continue;
      }
      if (preg_match("/^#mean/", $line)) {
        $line_mean = $line;
        continue;
      }

      // Checks the required headers.
      if (!$line_env) {
        return "The environment row '#environmet' not found";
      }
      if (!$line_mean) {
        return "The mean row '#mean' not found";
      }
      if ($line_env && $line_mean) {
        break;
      }
      return "The required header rows (Environment and Mean) not found";
    }
    fclose($fdr);

    // Gets the delimiter.
    $separator = '';
    if (preg_match("/,/", $line_mean)) {
      $separator = ',';
    }
    else if (preg_match("/\t/", $line_mean)) {
      $separator = "\t";
    }
    else {
      return "The delimiter could not found. Only comman or tab delimiter can be used.";
    }
    $num_env  = sizeof(explode($separator, $line_env));
    $num_mean = sizeof(explode($separator, $line_mean));
    if ($num_env != $num_mean) {
      return "The number of columns of The required header rows (Environment and Mean) are differnt ($num_env x $num_mean).";
    }
    return '';
  }

  /**
   * Returns the B-DATA.
   *
   * @param integer $crop_id
   * @param integer $flag
   * @param integer $status
   *
   * @return array
   */
  public static function getBData($crop_id, $flag = BIMS_OBJECT, $status = '') {

    // Gets all the B-DATA.
    $args = array(':crop_id' => $crop_id);
    $sql = "SELECT * FROM {bims_bdata} WHERE crop_id = :crop_id";
    if ($status == '0') {
      $sql .= " AND status = 0 ";
    }
    else if ($status) {
      $sql .= " AND status = :status ";
      $args[':status'] = $status;
    }
    $results = db_query("$sql ORDER BY name", $args);
    $bdata_arr = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $bdata_arr[$obj->bdata_id] = $obj->name;
      }
      else if ($flag == BIMS_CLASS) {
        $bdata_arr []= BIMS_BDATA::byID($obj->bdata_id);
      }
      else {
        $bdata_arr []= $obj;
      }
    }
    return $bdata_arr;
  }

  /**
   * Adds a B-DATA.
   *
   * @param array $details
   *
   * @return BIMS_BDATA
   */
  public static function addBDATA($details) {
    $bims_bdata = NULL;
    $transaction = db_transaction();
    try {

      // Adds a B-DATA.
      $bims_bdata = new BIMS_BDATA($details);
      if (!$bims_bdata->insert()) {
        throw new Exception("Fail to add a B-DATA");
      }

      // Sets the default precision.
      $precision = $bims_bdata->getPropByKey('precision');
      if (!$precision) {
        $bims_bdata->setPropByKey('precision', BIMS_TOOL_GPT_PRECISION, TRUE);
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return NULL;
    }
    return $bims_bdata;
  }

  /**
   * Processes the B-DATA.
   *
   * @param boolean
   */
  public function processBData() {

    // Opens the B-Data file.
    $filepath = $this->getWorkingDir() . '/' . $this->filename;
    if (!file_exists($filepath)) {
      return FALSE;
    }

    // Gets the file inforamtion from the required rows (envrionment and mean)
    // and save them.
    $err = $this->_saveFileInfo($filepath);
    if ($err) {
      bims_print($err, 1, 1);
      return FALSE;
    }

    // Adds the envrionments.
    $envs = $this->getPropByKey('envs');
    foreach ((array)$envs as $env => $mean) {

      // Adds a new environment.
      $details = array(
        'name'        => $env,
        'bdata_id'    => $this->bdata_id,
        'description' => 'N/A',
      );
      $bims_env = BIMS_ENVIRONMENT::addEnvironment($details);
      if (!$bims_env) {
        return FALSE;
      }
    }
    $this->setStatus(100);
    return $this->update();
  }

  /**
   * Reads and saves the file inforamtion.
   *
   * @param string $filepath
   *
   * @return string
   */
  private function _saveFileInfo($filepath) {

    // Gets the file info.
    if (!($fdr = fopen($filepath, 'r'))) {
      return FALSE;
    }
    $line_env   = '';
    $line_mean  = '';
    $num_marker = 0;
    $check_flag = FALSE;
    while ($line = fgets($fdr)) {
      $line = trim($line);
      if (!$line) {
        continue;
      }

      // Obtains the required headers.
      if (preg_match("/^#env/", $line)) {
        $line_env = $line;
        continue;
      }
      if (preg_match("/^#mean/", $line)) {
        $line_mean = $line;
        continue;
      }

      // Checks the required headers.
      if (!$check_flag) {
        $check_flag = TRUE;
        if (!$line_env && !$line_mean) {
          return "The required header rows (Environment and Mean) not found";
        }
        else if (!$line_env) {
          return "The environment row '#environmet' not found";
        }
        else if (!$line_mean) {
          return "The mean row '#mean' not found";
        }
      }
      $num_marker++;
    }
    fclose($fdr);

    // Gets the delimiter.
    $separator = '';
    if (preg_match("/,/", $line_mean)) {
      $separator = ',';
    }
    else if (preg_match("/\t/", $line_mean)) {
      $separator = "\t";
    }
    else {
      return "The delimiter could not found. Only comman or tab delimiter can be used.";
    }
    $arr_env  = explode($separator, $line_env);
    $num_env  = sizeof($arr_env);
    $arr_mean = explode($separator, $line_mean);
    $num_mean = sizeof($arr_mean);
    if ($num_env != $num_mean) {
      return "The number of columns of The required header rows (Environment and Mean) are differnt ($num_env x $num_mean).";
    }
    $means = array();
    $envs  = array();
    for ($i = 1; $i < $num_env; $i++) {
      $means[strtolower($arr_env[$i])] = $arr_mean[$i];
      $envs[strtolower($arr_env[$i])] = $arr_env[$i];
    }

    // Saves the information.
    $this->setPropByKey('num_env', $num_env - 1);
    $this->setPropByKey('num_marker', $num_marker);
    $this->setPropByKey('means', $means);
    $this->setPropByKey('envs', $envs);
    $this->setPropByKey('separator', $separator);
    if (!$this->update()) {
      return "Failed to save the file information";
    }
    return '';
  }

  /**
   * Returns the file information.
   *
   * @param string $type
   *
   * @return string
   */
  public function getFileInfo($type) {
    if ($type == 'stats') {
      $num_env = $this->getPropByKey('num_env');
      $num_marker = $this->getPropByKey('num_marker');
      return "$num_env environments x $num_marker loci.";
    }
    return '';
  }

  /**
   * Adds an environments.
   *
   * @param integer $environment_id
   *
   * @return boolean
   */
  public function addEnvironment($environment_id) {
    $details = array(
      'bdata_id'        => $this->getBdataID(),
      'environment_id'  => $environment_id,
    );

    // Checks for a duplication.
    $bdata_env = PUBLIC_BIMS_BDATA_ENVIRONMENT::byKey($details);
    if (!$bdata_env) {

      // Adds the environment.
      $bims_bdata_env = new PUBLIC_BIMS_BDATA_ENVIRONMENT($details);
      return $bims_bdata_env->insert();
    }
    return TRUE;
  }

  /**
   * Reurns the environments.
   *
   * @param integer $flag
   *
   * @return various
   */
  public function getEnvironments($flag = BIMS_OBJECT) {

    // Gets all the environmetns that belong to this B-DATA.
    $sql = "
      SELECT ENV.*
      FROM {bims_environment} ENV
      WHERE ENV.bdata_id = :bdata_id
      ORDER BY ENV.name
    ";
    $results = db_query($sql, array(':bdata_id' => $this->bdata_id));
    $environments = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_CLASS) {
        $environments []= BIMS_ENVIRONMENT::byID($obj->environment_id);
      }
      else if ($flag == BIMS_OPTION) {
        $environments[$obj->environment_id] = $obj->name;
      }
      else {
        $environments []= $obj;
      }
    }
    return $environments;
  }

  /**
   * Returns the weights of the markers.
   *
   * @param string marker_name
   * @param integer $flag
   *
   * @return string
   */
  public function getWeightsByMarker($marker_name, $flag = BIMS_ARRAY) {

    // Opens the B-DATA wight file.
    $bdata_file = $this->getFilepath();
    if (!($fdr = fopen($bdata_file, 'r'))) {
      return "Cannot open file ($bdata_file).";
    }

    // Searchs the weights of the marker.
    $arr_weights    = array();
    $arr_headers    = array();
    $header_flag    = FALSE;
    $marker_name_lc = strtolower($marker_name);
    while ($line = fgets($fdr)) {
      $line = trim($line);
      if ($line == '') {
        continue;
      }
      $tmp = explode("\t", $line);
      if (!$header_flag) {
        $header_flag = TRUE;
        $arr_headers = $tmp;
        continue;
      }

      // Searches for the marker.
      if ($marker_name_lc == strtolower($tmp[0])) {
        $arr_weights = $tmp;
        break;
      }
    }
    fclose($fdr);

    // Returns the weights.
    $results = '';
    $size = sizeof($arr_headers);
    if ($flag == BIMS_VERT) {
      for ($i = 0; $i < $size; $i++) {
        $results .= sprintf("%s\t%.6f\n", $arr_headers[$i], $arr_weights[$i]);
      }
    }
    else if ($flag == BIMS_HORT) {
      $results .= implode("\t", $arr_headers) . "\n";
      $results .= implode("\t", $arr_weights);
    }
    else {
      $results = array();
      for ($i = 0; $i < $size; $i++) {
        $results[$arr_headers[$i]] = $arr_weights[$i];
      }
    }
    return $results;
  }

  /**
   * Returns the format for the precision.
   */
  public function getFormat() {
    return '%.0' . $this->getPropByKey('precision'). 'f';
  }

  /**
   * Returns the working directory. If not exists, create it.
   *
   * @return string
   */
  public function getWorkingDir() {

    // Gets the working directory.
    $bims_user = BIMS_USER::byID($this->user_id);
    $dir = $bims_user->getPath('bdata') . '/' . $this->bdata_id;

    // Creates the path if not exists.
    if (!file_exists($dir)) {
      bims_create_dir($dir);
    }
    return $dir;
  }

  /**
   * Returns the filepath.
   *
   * @return string
   */
  public function getFilepath() {
    return $this->getWorkingDir() . '/' . $this->filename;
  }

  /**
   * Returns the status label.
   *
   * @return string
   */
  public function getStatusLabel() {
    if ($this->status == '0') {
      return 'waiting';
    }
    else if ($this->status < 0) {
      return 'failed';
    }
    else if ($this->status == 100) {
      return 'completed';
    }
    else {
      return 'processing';
    }
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the value of the given key in prop.
   *
   * @param string $key
   *
   * @return various
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * @param string $key
   * @param various $value
   */
  public function setPropByKey($key, $value, $flag_update = FALSE) {
    $this->prop_arr[$key] = $value;
    if ($flag_update) {
      $this->update();
    }
  }

  /**
   * Retrieves the prop array.
   *
   * @return array
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }
}