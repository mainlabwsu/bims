<?php
/**
 * The declaration of BIMS_JOB class.
 *
 */
class BIMS_JOB extends PUBLIC_BIMS_JOB {

  /**
   * Class data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_JOB::__construct()
   */
  public function __construct($details = array()) {

    // Updates the submit date.
    if (!array_key_exists('submit_date', $details)) {
      $details['submit_date'] = '';
    }
    if ($details['submit_date']) {
      $details['submit_date'] = date("Y-m-d H:i:s");
    }
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_JOB::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_JOB by job ID.
   *
   * @param integer $job_id
   *
   * @return BIMS_JOB|NULL
   */
  public static function byID($job_id) {
    if (!bims_is_int($job_id)) {
      return NULL;
    }
    return self::byKey(array('job_id' => $job_id));
  }

  /**
   * @see PUBLIC_BIMS_JOB::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_JOB::insert()
   */
  public function insert() {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    if (parent::insert()) {

      // Creates the working diretory.
      $dir = $this->getWorkingDir();
      if (file_exists($dir)) {
        bims_create_dir($dir);
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @see PUBLIC_BIMS_JOB::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * @see PUBLIC_BIMS_JOB::delete()
   */
  public function delete() {

    // Kills the running job.
    $exec_id = $this->getExecID();
    if ($exec_id) {

      // Kills if it is running.
      if (file_exists("/proc/$exec_id" )) {
        exec("kill -9 $exec_id", $output, $status);
      }
    }

    // Deletes the job directory.
    $dir = $this->getWorkingDir();
    if (file_exists($dir)) {
      mcl_remove_dir($dir);
    }

    // Delete this job.
    return parent::delete();
  }

  /**
   * Returns BIMS_JOB by job ID.
   *
   * @param integer $job_id
   *
   * @return BIMS_JOB|NULL
   */
  public function getWorkingDir() {

    // Gets the user directory.
    $bims_user = BIMS_USER::byID($this->user_id);
    $working_dir = $bims_user->getPath('job') . '/' . $this->job_id;

    // Creates the directory if not exists.
    if (!file_exists($working_dir)) {
      bims_create_dir($working_dir);
    }
    return $working_dir;
  }

  /**
   * Clears the temporary directory.
   *
   * @param string $name
   *
   * @return boolean
   */
  public function clearTmpDir($name = 'tmp') {

    // Gets the temporary directory.
    $tmp_dir = $this->getWorkingDir() . "/$name";
    if (file_exists($tmp_dir)) {
      bims_remove_dir($tmp_dir);
    }
    return bims_create_dir($tmp_dir);
  }

  /**
   * Returns the temporary directory.
   *
   * @param string $name
   *
   * @return string
   */
  public function getTmpDir($name = 'tmp') {
    return $this->getWorkingDir() . "/$name";
  }

  /**
   * Retrieves the progress of the job.
   *
   * @retrun string
   */
  public function getProgress() {

    // Gets the process file.
    $progress_file = $this->getWorkingDir() . '/progress';
    if (file_exists($progress_file)) {
      return file_get_contents($progress_file);
    }
    else {
      return 'No progress report.';
    }
  }

  /**
   * Resets the progress file.
   */
  public function resetProgress() {
    $progress_file = $this->getWorkingDir() . '/progress';
    if (file_exists($progress_file)) {
      unlink($progress_file);
    }

    // Creates a progress file.
    file_put_contents($progress_file, 'RESET PROGRESS');
  }

  /**
   * Sets the progress of the job.
   *
   * @param string $progress
   * @param boolean $append
   */
  public function setProgress($progress, $append = FALSE) {

    // Gets the process file.
    $progress_file = $this->getWorkingDir() . '/progress';

    // Appends the progress.
    if (file_exists($progress_file) && $append) {
      file_put_contents($progress_file, "\n$progress", FILE_APPEND);
    }

    // Initializes the progress.
    else {
      file_put_contents($progress_file, $progress);
    }
  }

  /**
   * Runs the job.
   *
   * @return boolean
   */
  public function run() {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Adds a job.
   *
   * @param array $details
   *
   * @return object of child class of BIMS_JOB
   */
  public static function getObject($job_id) {

    // Gets BIMS_JOB.
    $bims_job = BIMS_JOB::byID($job_id);
    if ($bims_job) {
      // Gets BIMS_TOOL.
      $bims_tool = BIMS_TOOL::byID($bims_job->getToolID());
      if ($bims_tool) {
        $class_name = strtoupper('BIMS_JOB_' . $bims_tool->getName());
        return $class_name::byID($job_id);
      }
    }
    return NULL;
  }

  /**
   * Adds a job.
   *
   * @param array $details
   *
   * @return BIMS_JOB
   */
  public static function addJob($details) {
    // To be overridden by Child class.
    return NULL;
  }

  /**
   * Returns the delimiter of the line.
   *
   * @param string $line
   *
   * @return string
   */
  public function getDelimiter($line) {
    if (preg_match("/\t/", $line)) {
      return "\t";
    }
    else if (preg_match("/,/", $line)) {
      return ",";
    }
    return '';
  }

  /**
   * Returns the status label.
   *
   * @return string
   */
  public function getStatusLabel() {
    if ($this->status == '0') {
      return 'waiting';
    }
    else if ($this->status < 0) {
      return 'failed';
    }
    else if ($this->status == 100) {
      return 'completed';
    }
    else {
      return 'processing';
    }
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the prop_arr.
   *
   * @return array
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   *
   * $param string $key
   *
   * @return string
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * $param string $key
   * $param string $value
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }

  /**
   * Updates the value of the given key in prop.
   *
   * $param string $key
   * $param string $value
   *
   * @return boolean
   */
  public function updatePropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
    return $this->update();
  }
}