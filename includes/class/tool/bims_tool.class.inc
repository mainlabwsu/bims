<?php
/**
 * The declaration of BIMS_TOOL class.
 *
 */
class BIMS_TOOL extends PUBLIC_BIMS_TOOL {

  /**
   * Class data members.
   */
  /**
   * @see PUBLIC_BIMS_TOOL::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see PUBLIC_BIMS_TOOL::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_TOOL by tool ID.
   *
   * @param integer $tool_id
   *
   * @return BIMS_TOOL|NULL
   */
  public static function byID($tool_id) {
    if (!bims_is_int($tool_id)) {
      return NULL;
    }
    return self::byKey(array('tool_id' => $tool_id));
  }

  /**
   * Returns BIMS_TOOL by tool name.
   *
   * @param string $name
   *
   * @return BIMS_TOOL|NULL
   */
  public static function byName($name) {
    return self::byKey(array('name' => $name));
  }


  /**
   * @see PUBLIC_BIMS_TOOL::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Add a tool.
   *
   * @param array $details
   *
   * @return BIMS_TOOL
   */
  public static function addTool($details) {
    $bims_tool = NULL;
    $transaction = db_transaction();
    try {

      // Adds a tool.
      $label = $details['label'];
      $bims_tool = new BIMS_TOOL($details);
      if (!$bims_tool->insert()) {
        throw new Exception("Fail to add a tool ($label)");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return $bims_tool;
  }
}