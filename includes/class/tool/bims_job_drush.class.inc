 <?php
/**
 * The declaration of BIMS_JOB_DRUSH class.
 *
 */
class BIMS_JOB_DRUSH extends BIMS_JOB {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_JOB::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_JOB::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_JOB_MVIEW by job ID.
   *
   * @param integer $job_id
   *
   * @return BIMS_JOB|NULL
   */
  public static function byID($job_id) {
    if (!bims_is_int($job_id)) {
      return NULL;
    }
    return self::byKey(array('job_id' => $job_id));
  }

  /**
   * @see BIMS_JOB::insert()
   */
  public function insert() {
    return parent::insert();
  }

  /**
   * @see BIMS_JOB::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Returns the filepath of the given types of result file.
   *
   * @return string
   */
  public function getResultsFile($type) {
    if (preg_match("/^(csv|table|log)$/", $type)) {
      return $this->getWorkingDir() . '/gpt_results.' . $type;
    }
    return '';
  }

  /**
   * Checks if the given types of result file exists.
   *
   * @return boolean
   */
  public function isResultsFile($type) {
    if (preg_match("/^(csv|table|log)$/", $type)) {
      return file_exists($this->getResultsFile($type));
    }
    return FALSE;
  }

  /**
   * Add a job.
   *
   * @param array $details
   *
   * @return BIMS_JOB_DRUSH
   */
  public static function addJob($details) {

    $transaction = db_transaction();
    try {

      // Updates the $details.
      $details['submit_date'] = date("Y-m-d G:i:s");

      // Adds a job.
      $bims_job_drush = new BIMS_JOB_DRUSH($details);
      if (!$bims_job_drush->insert()) {
        throw new Exception("Fail to add a job");
      }
      return $bims_job_drush;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
    return NULL;
  }

  /**
   * Returns the number of the jobs owned by the user.
   *
   * @param integer $user_id
   *
   * @return integer
   */
  public static function getNumJobs($user_id) {
    $sql = "
      SELECT COUNT(job_id) FROM {bims_job}
      WHERE user_id = :user_id
    ";
    $args = array('user_id' => $user_id);
    return db_query($sql, $args)->fetchField();
  }

  /**
   * Runs the job.
   *
   * @param boolean $verbose
   *
   * @return boolean
   */
  public function run($verbose = FALSE) {

    // Gets and runs the drush commands.
    $drush  = bims_get_config_setting('bims_drush_binary');
    $cmds = $this->getPropByKey('cmds');
    foreach ((array)$cmds as $cmd) {
      $run_cmd = "$drush $cmd > /dev/null 2>/dev/null  echo $!";
      if ($verbose) {
        bims_print("Call drush command  $run_cmd", 1, 1);
      }
      $pid = exec($run_cmd, $output, $return_var);
    }
    return TRUE;
  }
}