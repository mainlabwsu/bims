<?php
/**
 * The declaration of BIMS_RESOURCE_GE class.
 */
class BIMS_RESOURCE_GE extends BIMS_RESOURCE {

  /**
   * @see BIMS_RESOURCE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_RESOURC::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_RESOURCE_GE by ID.
   *
   * @param integer $resrouce_id
   *
   * @return BIMS_RESOURCE_GE|NULL
   */
  public static function byID($resrouce_id) {
    if (!bims_is_int($resrouce_id)) {
      return NULL;
    }
    return self::byKey(array('resrouce_id' => $resrouce_id));
  }

  /**
   * @see BIMS_RESOURCE::isJobRunning()
   */
  public function isJobRunning(GenSASJobExec $job) {
/*
    // Get JobExec ID. If it is empty return FALSE.
    $exec_id = $this->getJobExecID($job);
    if (!$exec_id) {
      return FALSE;
    }

    // Search the status of the submitted job in qstat table.
    // Make sure to add -u option.
    $username = $this->getUsername();
    $output = array();
    $return_var = '';
    exec("export SGE_ROOT=/ge;/ge/bin/linux-x64/qstat -u $username", $output, $return_var);
    foreach ($output as $line) {
      $line = trim($line);
      $cols = preg_split('/\s+/', $line);
      if ($cols[0] == $exec_id) {
        return TRUE;
      }
    }*/
    return FALSE;
  }

  /**
   * @see BIMS_RESOURCE::killJob()
   */
  public function killJob(GenSASJobExec $job) {
/*
    // Get the PID for this job.
    $pid = $this->getJobExecID($job);
    if ($pid) {
      exec("export SGE_ROOT=/ge;/ge/bin/linux-x64/qdel $pid", $output, $status);
      return ($status) ? FALSE : TRUE;
    }
    return TRUE;*/
  }

  /**
   * @see BIMS_RESOURCE::reportRunningJobs()
   */
  public function reportRunningJobs() {
/*
    // Gets the resource info.
    $name = $this->getName();
    $id   = $this->getResourceID();
    $type = strtoupper($this->getType());

    // Sets the prefix of the cluster job name.
    $prefix = (preg_match("/gensas-wd/", getcwd())) ? 'GSD' : 'GSP';

    // Writes the report header.
    $report  = "\n\t================================\n";
    $report .= "\tResource Name [ID] : $name [$id]\n";
    $report .= "\tResource Type      : $type\n";
    $report .= "\tPrefix of Job Name : $prefix\n";
    $report .= "\t--------------------------------\n";

    // Checks the running job.
    $username = $this->getUsername();
    $output = array();
    $return_var = '';
    exec("export SGE_ROOT=/ge;/ge/bin/linux-x64/qstat -u $username -r", $output, $return_var);
    $running_jobs = array();
    $cur_pid = '';
    $dup_jobs = array();
    $flag = FALSE;
    foreach ($output as $line) {
      $line = trim($line);
      $cols = preg_split('/\s+/', $line);
      $pid  = $cols[0];
      $name = $cols[2];

      // Gets the job info.
      if (preg_match("/^(\d+)$/", $pid)) {
        if ($cur_pid == '' || $cur_pid != $pid) {
          $cur_pid = $pid;
          $flag = TRUE;
        }

        if (!preg_match("/^$prefix\_/", $name)) {
          $flag = FALSE;
          continue;
        }
        $running_jobs[$cur_pid] = array(
          'user'    => $cols[3],
          'status'  => $cols[4],
          'date'    => $cols[5],
          'time'    => $cols[6],
          'node'    => $cols[7],
        );
      }
      else if ($flag && preg_match("/^full jobname:\s+$prefix\_(.*)$/i", $line, $matches)) {
        $job_id = trim($matches[1]);
        $running_jobs[$cur_pid]['job_id'] = $job_id;
        $dup_jobs[$job_id]++;
      }
    }

    // Returns no job running message.
    if (empty($running_jobs)) {
      $report .= "\tNo job is running\n\n";
      return $report;
    }

    // Checks each job on the resource.
    $report .= "\t>>List of processes on the $name\n";
    foreach ($running_jobs as $pid => $job) {
      $job_id = $job['job_id'];
      $status = $job['status'];
      $date   = $job['date'];
      $time   = $job['time'];

      if ($status == 'r') {

        // Checks for duplication.
        $report .= "\t[$pid] " . $job['job_id'] . " is running ($date $time)\n";
        if ($dup_jobs[$job_id] > 1) {
          $report .= "\tDuplicated job\n";
        }

        // Check if the job exists in gensas_job_exeid table.
        $sql = "
          SELECT COUNT(job_id) FROM {gensas_job_execid}
          WHERE job_id = :job_id AND resource_id = :resource_id AND exec_id = :exec_id
        ";
        $args = array(
          ':job_id' => $job_id,
          ':resource_id' => $this->resource_id,
          ':exec_id' => $pid,
        );
        $num = db_query($sql, $args)->fetchField();
        if ($num == 0) {
          $report .= "\t\t>This job dose not exists in gensas_job_execid\n";
        }

        // Check if the job exists in gensas_job table.
        $gensas_job = GenSASJob::byID($job_id);
        if (!$gensas_job) {
          $report .= "\t\t>This job dose not exists in gensas_job\n";
        }
      }
      else {
        $report .= "\t$pid "  . $job['job_id'] . " is not running\n";
      }
    }*/
    return $report;
  }

  /**
   * @see BIMS_RESOURCE::runCommand()
   */
  public function runCommand(GenSASJobExec $job, $command, $index = 0, $options = array()) {
/*
    // Set default options.
    $outfile    = $options['outfile']   ? $options['outfile']   : '';
    $no_stdout  = $options['no_stdout'] ? $options['no_stdout'] : FALSE;
    $env        = $options['env']       ? $options['env']       : array();

    // Prepare some commonly used variables.
    $job_id     = $job->getJobID();
    $job_dir    = $job->getWorkingDir();
    $prefix     = $job->getJobFilePrefix();

    // Build the STDERR and STDOUT files and add to the command.
    $stderr_f = $job->getSTDERRfile($index);
    $stdout_f = $job->getSTDOUTfile($index);

    // If the user wants to redirect into a specific outfile then do so,
    // but, create a blank STDOUT file so we don't break logfile parsing.
    $full_command = '';
    if ($outfile) {
      $full_command = "$command > $outfile 2> $outfile.stderr ";
    }
    else {
      $full_command = $command;
    }

    // Sets the name of the cluster job. Add 'GSP_' for projection and 'GSD_' for development sites.
    $job_name = (preg_match("/gensas-wd/", $job_dir)) ? 'GSD_' . $job_id : 'GSP_' . $job_id;

    // Create the submission file
    $script  = "#!/bin/bash\n";
    $script .= "#\n";
    $script .= "#\$ -N $job_name\n";
    $script .= "#\$ -o $stdout_f\n";
    $script .= "#\$ -e $stderr_f\n";

    // Adds the environmet values.
    if (!empty($env)) {
      foreach ($env as $key => $value) {
        $script .= "#\$ $key $value\n";
      }
    }
    $script .= $full_command;

    // Create the submission file in the local directory and then sync it so that
    // it can be launced on the cluster head node.
    $qsub_f = "$prefix.$index.qsub";
    file_put_contents($job->getWorkingDir() . '/' . $qsub_f, $script);

    // Execute the command.
    $cmd = "qsub " . $job->getWorkingDir() . '/' . $qsub_f;
    exec($cmd, $output, $return_var);

    // Check the return status of the submitted job.
    switch ($return_var) {

      // The job was successfully submitted.
      case 0:
        // Get the job ID
        $job_id = $output[0];
        $job_id = preg_replace('/Your job (\d+) \(.*?\) has been submitted/', '$1', $job_id);
        $this->addJobExecID($job, $job_id);
        return TRUE;
        break;
      // The job was not submitted because the limit was reached
      case 25:
        return FALSE;
        break;
      // Some other failure led to the job not being submitted.
      default:
        return FALSE;
        break;
    }*/
  }
}
