<?php
/**
 * The declaration of BIMS_RESOURCE_LOCAL class.
 */
class BIMS_RESOURCE_LOCAL extends BIMS_RESOURCE {

  /**
   * @see BIMS_RESOURCE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_RESOURC::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_RESOURCE_LOCAL by ID.
   *
   * @param integer $resrouce_id
   *
   * @return BIMS_RESOURCE_LOCAL|NULL
   */
  public static function byID($resrouce_id) {
    if (!bims_is_int($resrouce_id)) {
      return NULL;
    }
    return self::byKey(array('resrouce_id' => $resrouce_id));
  }

  /**
   * @see BIMS_RESOURCE::isJobRunning()
   */
  public function isJobRunning() {
/*
    // First check the job state. If it's in the 'running' state then we
    // will check if the PID is still on the system
    $status = $job->getStatus();
    if (in_array($status, array(25, 50, 60, 70, 75))) {

      // Get the PID for this job.
      $pid = $this->getJobExecID($job);

      // if the job still has running processes then just return
      if ($pid and file_exists( "/proc/$pid" )){
        $result = shell_exec(sprintf("ps -p %d", $pid));

        if (preg_match("/" . $pid . "/", $result)){
          return TRUE;
        }
        return FALSE;
      }
    }*/
    return FALSE;
  }

  /**
   * @see BIMS_RESOURCE::killJob()
   */
  public function killJob() {
/*
    // Get the PID for this job.
    $pid = $this->getJobExecID($job);
    exec("kill -9 $pid", $output, $status);
    return $status;
    */
  }

  /**
   * Return the working directory.
   *
   * @return string
   */
  private function getPIDDir() {
/*
    // Get working directory.
     $working_dir = gensas_get_config_setting('working_dir') . '/' . 'run';

    // Make sure the directoy exists.
    if (!file_exists($working_dir)) {
      mkdir($working_dir, 0775, TRUE);
    }
    return $working_dir;*/
  }

  /**
   * @see BIMS_RESOURCE::clean()
   */
  protected function clean() {
    /*
    $pid_dir = $this->getPIDDir();
    $job_id = $job->getJobID();

    // Iterate through all of the PID files in the pid directory
    // and check to see which ones are running.  Remove those
    // that are not running. These would be cases where this
    // script failed without terminating properly. Otherwise
    // this script should remove it's own PID when it's done
    if ($handle = opendir($pid_dir)) {
      while (false !== ($entry = readdir($handle))) {
        if (is_numeric($entry) and !file_exists( "/proc/$entry" )){
          gensas_remove_file("$pid_dir/$entry");
        }
      }
      closedir($handle);
    }

    // If the job still has running processes then just return.
    if (file_exists("/proc/$exec_id" )){
      // Sometimes its possible that the job could complete and another non
      // GenSAS process picks up the pid.  So, make sure that the job
      // a real GenSAS job
      $result = shell_exec(sprintf("ps www %d", $exec_id));
      if (preg_match("/$job_id/s", $result)) {
        //continue;
      }
    }
    */
  }

  /**
   * @see BIMS_RESOURCE::runCommand()
   */
  public function runCommand(GenSASJobExec $job, $command, $index = 0, $options = array()) {
/*
    // Set default options.
    $outfile   = $options['outfile']   ? $options['outfile']   : '';
    $no_stdout = $options['no_stdout'] ? $options['no_stdout'] : FALSE;
    $no_stderr = $options['no_stderr'] ? $options['no_stderr'] : FALSE;

    // Prepare some commonly used variables.
    $job_dir  = $job->getWorkingDir();
    $job_id   = $job->getJobID();
    $prefix   = $job->getJobFilePrefix();

    // Build the STDERR and STDOUT files and add to the command.
    $stderr_f = $job->getSTDERRfile($index);
    $stdout_f = $job->getSTDOUTfile($index);
    $cmd_f    = "$job_dir/$prefix.$index.cmd";

    // If the user wants to redirect into a specific outfile then do so,
    // but, create a blank STDOUT file so we don't break logfile parsing.
    if ($outfile) {
      $full_command = "$command > $outfile 2> $outfile.stderr ";
      touch($stdout_f);
    }
    else {
      if ($no_stdout && $no_stderr) {
        $full_command = "$command";
        touch($stderr_f);
        touch($stdout_f);
      }
      if (!$no_stdout && $no_stderr) {
        $full_command = "$command > $stdout_f";
        touch($stderr_f);
      }
      if ($no_stdout && !$no_stderr) {
        $full_command = "$command 2> $stderr_f";
        touch($stdout_f);
      }
      if (!$no_stdout && !$no_stderr) {
        $full_command = "$command > $stdout_f 2> $stderr_f";
      }
    }

    // Save the command for future debugging.
    file_put_contents($cmd_f, $full_command);

    // Execute the command.
    exec($full_command . "& echo $!", $op);
    $pid = (int) $op[0];
    $this->addJobPID($job, $pid);
    return TRUE;
    */
  }

  /**
   * Associates a job with a process ID for a running process.
   *
   * @param $job_id
   *   The Job ID.
   * @param string $pid
   *   The process ID.
   */
  private function addJobPID($job_id, $pid) {
/*
    // Create a PID file for this job.
    $job_id = $job->getJobID();
    $pid_dir = gensas_get_config_setting('working_dir') . '/' . 'run';
    //$pid = getmypid();
    if (!is_dir($pid_dir)) {
      gensas_create_dir($pid_dir);
    }
    $fh = fopen("$pid_dir/$pid", 'w');
    fclose($fh);

    $this->addJobExecID($job, $pid);*/
  }

  /**
   * Remove the exec_id and the PID file.
   *
   * @param $job_id
   * @param $pid
   *
   * @return boolean TRUE|FALSE
   */
  private function removeJobPID($job_id, $pid) {
/*
    // Remove the exec_id and the PID file.
    if (removeJobExecId($job, $pid)) {
      $pid_dir = gensas_get_config_setting('working_dir') . '/' . 'run';
      gensas_remove_file("$pid_dir/$pid");
      return TRUE;
    }
    return FALSE;*/
  }
}