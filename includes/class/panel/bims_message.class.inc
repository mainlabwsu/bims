<?php
/**
 * The declaration of BIMS_MESSAGE class.
 *
 */
class BIMS_MESSAGE {

  /**
   * Class data members.
   */
  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}


  /**
   * Adds the form element for the message.
   *
   * @param array &$form
   * @param BIMS_USER $bims_user
   * @param string $key
   */
  public static function init(&$form, BIMS_USER $bims_user, $key) {
    $msg = self::getMsg();
    if ($msg) {

      // Sets the local variables.
      $name     = 'panel_message_' . $key;
      $callback = 'bims_panel_' . $key . '_form_ajax_callback';
      $wrapper  = str_replace('_', '-', 'bims-panel-' . $key . '-form');

      // Shows the message for once.
      $form[$name] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => TRUE,
        '#title'        => 'Message',
        '#attributes'   => array(),
      );
      $form[$name]['msg'] = array('#markup' => $msg);

      // Adds the button to hide the message.
      $form[$name]['hide_msg_btn'] = array(
        '#type'       => 'button',
        '#name'       => 'hide_msg_btn',
        '#value'      => 'Close',
        '#prefix'     => '<br />',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => $callback,
          'wrapper'  => $wrapper,
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
  }

  /**
   * Shows the message on the panel.
   *
   * @param array $form
   * @param string $key
   *
   * @return string
   */
  public static function show(&$form, $key) {

    // Show the message.
    $layout = '';
    $name = 'panel_message_' . $key;
    if (array_key_exists($name, $form)) {
      $layout = '<div style="float:left;width:100%;">' . drupal_render($form[$name]) . '</div>';
    }
    return $layout;
  }

  /**
   * Handles ajax callback.
   *
   * @param array $form
   * @param array $form_state
   * @param string $key
   */
  public static function handleAjaxCallback(&$form, $form_state, $key) {

    // Hides the message.
    hide($form['panel_message_' . $key]);
    self::setMsg('');
  }


  /**
   * Sets the panel message.
   *
   * @param string $msg
   */
  public static function setMsg($msg) {
    bims_set_session('BIMS_PANEL_MESSAGE', $msg);
  }

  /**
   * Appends the panel message.
   *
   * @param string $msg
   */
  public static function addMsg($msg, $is_array = FALSE) {
    if ($is_array) {
      $msg = print_r($msg, TRUE);
    }
    $new_msg = bims_get_session('BIMS_PANEL_MESSAGE') . '<br />' . $msg;
    bims_set_session('BIMS_PANEL_MESSAGE', $new_msg);
  }

  /**
   * Gets the panel message.
   *
   * @param string $msg
   *
   * @return boolean
   */
  public static function getMsg() {
    $msg = bims_get_session('BIMS_PANEL_MESSAGE', '');
    if ($msg) {
      self::setMsg('');
    }
    return $msg;
  }

}