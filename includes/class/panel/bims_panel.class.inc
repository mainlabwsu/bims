<?php
/**
 * The declaration of BIMS_PANEL class.
 *
 */
class BIMS_PANEL {

  /**
   * Class data members.
   */
  private $bims_user  = NULL;
  private $key        = NULL;

  /**
   * Implements the class constructor.
   *
   * @param BIMS_USER $bims_user
   * @param string $key
   */
  public function __construct(BIMS_USER $bims_user, $key) {

    // Initializes data members.
    $this->bims_user  = $bims_user;
    $this->key        = $key;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Initializes the basic componets of a panel.
   * - message
   * - instrurction
   *
   * @param array $form
   * @param boolean $anonymous
   * @param boolean $edit_user
   *
   * @return boolean
   */
  public function init(&$form, $anonymous = FALSE, $edit_user = FALSE) {

    // Saves this object in the form.
    $form['bims_panel'] = array(
      '#type'   => 'value',
      '#value'  => $this,
    );

    // Adds BIMS_MESSAGE.
    BIMS_MESSAGE::init($form, $this->bims_user, $this->key);

    // Adds BIMS_INSTRUCTION.
    BIMS_INSTRUCTION::init($form, $this->bims_user, $this->key);

    // Adds the message for anonymous.
    if ($anonymous) {
      if ($this->bims_user->isAnonymous()) {
        $form['anonymous'] = array(
          '#type'         => 'fieldset',
          '#collapsed'    => FALSE,
          '#collapsible'  => FALSE,
          '#title'        => 'Anonymous user',
          '#attributes'   => array('style' => 'margin:0px;'),
        );
        $form['anonymous']['msg'] = array(
          '#markup' => "<div style='margin:40px 0px 15px 10px;'><em>You are required to have an account to view the page.</em></div>",
        );
        return FALSE;
      }
    }

    // Adds the message for Edit.
    if ($edit_user) {
      if (!$this->bims_user->canEdit()) {
        $form['edit'] = array(
          '#type'         => 'fieldset',
          '#collapsed'    => FALSE,
          '#collapsible'  => FALSE,
          '#title'        => 'Permission required',
          '#attributes'   => array('style' => 'margin:0px;'),
        );
        $form['edit']['msg'] = array(
          '#markup' => "<div style='margin:40px 0px 15px 10px;'><em>You are required to have 'EDIT' permission to view the page.</em></div>",
        );
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Shows the basic componets of a panel.
   *
   * @param array $form
   *
   * @return string
   */
  public function show(&$form) {

    // Shows BIMS_MESSAGE.
    $layout = BIMS_MESSAGE::show($form, $this->key);

    // Shows BIMS_INSTRUCTION.
    $layout .= BIMS_INSTRUCTION::show($form, $this->key);
    return $layout;
  }

  /**
   * Handles the ajax-callback of the basic componets of a panel.
   *
   * @param array $form
   * @param array $form_state
   *
   * @return string
   */
  public function handleAjaxCallback(&$form, $form_state) {

    // Gets the trigger element.
    $trigger_elem = $form_state['triggering_element']['#name'];

    // BIMS_INSTRUCTION.
    if (preg_match("/_inst_btn/", $trigger_elem)) {
      BIMS_INSTRUCTION::handleAjaxCallback($form, $form_state, $this->key);
    }

    // BIMS_MESSAGE.
    else if (preg_match("/hide_msg_btn/", $trigger_elem)) {
      BIMS_MESSAGE::handleAjaxCallback($form, $form_state, $this->key);
    }
  }

  /**
   * Updates theme ID.
   *
   * @param string $theme_id
   *
   * @return string theme ID.
   */
  public static function updateThemeID($theme_id) {

    // Returns 'bims_panel_main_crop' if user want to change a crop.
    if ($theme_id == 'bims_panel_main_crop') {
      return $theme_id;
    }

    // Gets BIMS_USER.
    $bims_user = getBIMS_USER();

    // Gets the crop_id.
    $crop_id = $bims_user->getCropID();
    if (!$crop_id) {
      $num_crops = BIMS_CROP::getNumCrops();
      if ($num_crops !=  1) {
        return 'bims_panel_main_crop';
      }

      // Assigns the first crop in the returning array since it has
      // only one crop.
      $crops = BIMS_CROP::getCrops(BIMS_CLASS);
      foreach ($crops as $crop) {
        $crop_id = $crop->getCropID();
        break;
      }
      $bims_user->setCropID($crop_id);
    }

    // Returns 'bims_panel_main_program' if
    // no program_id or a user wants to change a program.
    $program_id = $bims_user->getProgramID();
    if (!$program_id || $theme_id == 'bims_panel_main_program') {
      return 'bims_panel_main_program';
    }
    return $theme_id;
  }

  /**
   * Pre-check the panel. Make sure crop and program are selected.
   *
   * @param BIMS_USER $bims_user
   * @param boolean $anonymous
   *
   * @return array
   */
  public static function preCheckSidebar(BIMS_USER $bims_user, $anonymous = FALSE) {

    // Checks for anonymous.
    if ($anonymous) {
      if ($bims_user->isAnonymous()) {
        return array('items' => array('#markup' => ''));
      }
    }

    // Gets the current crop.
    $bims_crop = $bims_user->getCrop();
    if (!$bims_crop) {
      if (BIMS_CROP::getNumCrops() > 1) {
        return array('items' => array('#markup' => "<em>Please click 'Crop' button at the top and choose a crop.</em>"));
      }
    }

    // Gets the current program.
    $bims_program = $bims_user->getProgram();
    if (!$bims_program) {
      return array('items' => array('#markup' => "<em>Please choose a program or create a program.</em>"));
    }
    return array();
  }
}