<?php
/**
 * The declaration of BIMS_INSTRUCTION class.
 *
 */
class BIMS_INSTRUCTION extends PUBLIC_BIMS_INSTRUCTION {

  /**
   *  Data members.
   */
  /**
   * @see PUBLIC_BIMS_INSTRUCTION::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see PUBLIC_BIMS_INSTRUCTION::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see PUBLIC_BIMS_INSTRUCTION::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Adds the form element for the instruction.
   *
   * @param array &$form
   * @param BIMS_USER $bims_user
   * @param string $key
   */
  public static function init(&$form, BIMS_USER $bims_user, $key) {

    // Gets BIMS_USER.
    $bims_user = getBIMS_USER();

    // Local variables.
    $name     = 'instruction_' . $key;
    $id       = 'bims_panel_' . $key;
    $callback = 'bims_panel_' . $key . '_form_ajax_callback';
    $wrapper  = str_replace('_', '-', 'bims-panel-' . $key . '-form');

    // Gets the instruction.
    $instruction = BIMS_INSTRUCTION::getInstrunction($id, $bims_user);

    // Adds fieldset for an instruction.
    $form[$name] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => $bims_user->getInstructionOption(),
      '#collapsible'  => TRUE,
      '#title'        => 'Instructions',
    );
    $form[$name]['instructions'] = array(
      '#markup' => "<div>$instruction</div>",
    );

    // Shows textarea and edit button for admin people.
    if ($bims_user->isBIMSAdmin()) {
      $form[$name]['text'] = array(
        '#type'   => 'textarea',
        '#prefix' => '<br />',
        '#value'  => $instruction,
      );
      $form[$name]['edit_inst_btn'] = array(
        '#type'       => 'button',
        '#name'       => 'edit_inst_btn',
        '#value'      => 'Edit',
        '#attributes' => array('style' => 'width: 120px;margin-top:20px;'),
        '#ajax'       => array(
          'callback' => $callback,
          'wrapper'  => $wrapper,
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form[$name]['update_inst_btn'] = array(
        '#type'       => 'button',
        '#name'       => 'update_inst_btn',
        '#value'      => 'Update',
        '#prefix'     => '<br />',
        '#attributes' => array('style' => 'width: 120px;'),
        '#ajax'       => array(
          'callback' => $callback,
          'wrapper'  => $wrapper,
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form[$name]['cancel_inst_btn'] = array(
        '#type'       => 'button',
        '#name'       => 'cancel_inst_btn',
        '#value'      => 'Cancel',
        '#attributes' => array('style' => 'width: 120px;'),
        '#ajax'       => array(
          'callback' => $callback,
          'wrapper'  => $wrapper,
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      hide($form[$name]['text']);
      hide($form[$name]['update_inst_btn']);
      hide($form[$name]['cancel_inst_btn']);
    }

    // Hides the instruction if $bims_user is NULL or the user want to hide.
    if (!$bims_user || !$bims_user->showInstruction()) {
      unset($form[$name]);
    }
  }

  /**
   * Shows the message on the panel.
   *
   * @param array $form
   * @param string key
   *
   * @return string
   */
  public static function show(&$form, $key) {

    // Show the instruction.
    $name = 'instruction_' . $key;
    if (array_key_exists($name, $form)) {
      return '<div>' . drupal_render($form[$name]) . '</div>';
    }
    return '';
  }

  /**
   * Handles ajax callback.
   *
   * @param array $form
   * @param array $form_state
   * @param string $key
   */
  public static function handleAjaxCallback(&$form, $form_state, $key) {

    // Local variables.
    $name = 'instruction_' . $key;
    $id   = 'bims_panel_' . $key;

    // Gets the trigger element.
    $trigger_elem = $form_state['triggering_element']['#name'];

    // If 'Edit' button is clicked.
    if ($trigger_elem == 'edit_inst_btn') {
      show($form[$name]['text']);
      show($form[$name]['update_inst_btn']);
      show($form[$name]['cancel_inst_btn']);
      hide($form[$name]['edit_inst_btn']);
    }

    // If 'Update' button is clicked.
    else if ($trigger_elem == 'update_inst_btn') {
      hide($form[$name]['text']);
      hide($form[$name]['update_inst_btn']);
      hide($form[$name]['cancel_inst_btn']);
      show($form[$name]['edit_inst_btn']);

      // Gets the instruction.
      $instruction = trim($form_state['input'][$name]['text']);

      // Adds or updates the instruction.
      $err_msg = self::addInstrunction($id, $instruction);
      if ($err_msg) {
        drupal_set_message($err_msg, 'error');
      }
      else {
        drupal_set_message('The instruction updated');
        $form[$name]['instructions']['#markup'] = "<div>$instruction</div>";
      }
    }

    // If 'Cancel' button is clicked.
    else if ($trigger_elem == 'cancel_inst_btn') {
      hide($form[$name]['text']);
      hide($form[$name]['update_inst_btn']);
      hide($form[$name]['cancel_inst_btn']);
      show($form[$name]['edit_inst_btn']);
    }
  }

  /**
   * Returns the instrunctions of the given ID.
   *
   * @param integer $id
   * @param BIMS_USER $bims_user
   *
   * @return string instrunctions.
   */
  public static function getInstrunction($id, $bims_user) {

    // Gets BIMS_USER.
    $bims_user = getBIMS_USER();

    // Gets the instruction.
    $instruction = 'The instruction has not been provided yet';
    $i = PUBLIC_BIMS_INSTRUCTION::byKey(array('id' => $id));
    if ($i) {
      $inst = $i->getInstruction();
      if ($instruction) {
        $instruction = $inst;
      }
    }
    return $instruction;
  }

  /**
   * Returns the instrunctions of the given ID.
   *
   * @param integer $id
   * @param string $instruction
   *
   * @return string error message.
   */
  public static function addInstrunction($id, $instruction) {
    $err_msg = '';

    // Adds the instruction. If the instruction ID exists in the table,
    // update the instruction, otherwise add it.
    $i = PUBLIC_BIMS_INSTRUCTION::byKey(array('id' => $id));
    if ($i) {
      $i->setInstruction($instruction);
      if (!$i->update()) {
        $err_msg = 'Error : Fail to update the instruction';
      }
    }
    else {
      $i = new PUBLIC_BIMS_INSTRUCTION(array(
        'id' => $id,
        'instruction' => $instruction,
      ));
      if (!$i->insert()) {
        $err_msg = 'Error : Fail to add the instruction';
      }
    }
    return $err_msg;
  }

  /**
   * Exports the instrunctions.
   *
   * @param BIMS_USER $bims_user
   * @param string $prefix
   * @param string $dest_dir
   *
   * @return string
   */
  public static function export(BIMS_USER $bims_user, $prefix = 'bims_export_instruction', $dest_dir = 'exported') {

    // Gets the destination directory.
    $exported_dir = $bims_user->getPath($dest_dir);

    // Sets the filepath.
    $filename = $prefix . date("-Y-m-d-G-i-s") . '.csv';
    $filepath = "$exported_dir/$filename";

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return FALSE;
    }

    // Exports the instructions.
    $sql = "SELECT I.id, I.instruction FROM {bims_instruction} I ORDER BY I.id";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      fputcsv($fdw, array($obj->id, $obj->instruction));
    }
    fclose($fdw);
    return $filepath;
  }

  /**
   * Imports the instrunctions.
   *
   * @param BIMS_USER $bims_user
   * @param string $filename
   * @param boolean $flag_update
   *
   * @return boolean
   */
  public static function import(BIMS_USER $bims_user, $filename, $flag_update = FALSE) {

    // Checks the file.
    $filepath = $bims_user->getUserDir() . '/'. $filename;
    if (!file_exists($filepath)) {
      return FALSE;
    }

    // Creates a backup.
    if ($flag_update) {
      $backup = self::export($bims_user, 'bims_backup_instruction', 'backup');
      if (!file_exists($backup)) {
        return FALSE;
      }
    }

    // Imports the instructions.
    if (!($fdr = fopen($filepath, 'r'))) {
      return FALSE;
    }
    while (!feof($fdr)) {
      $line = fgetcsv($fdr);
      if (!empty($line)) {
        $id   = $line[0];
        $inst = $line[1];
        $bims_inst = BIMS_INSTRUCTION::byKey(array('id' =>$id));
        if ($bims_inst) {
          if ($flag_update) {
            $bims_inst->setInstruction($inst);
            if ($bims_inst->update()) {
              bims_print("'$id' has been updated", 1, 2);
            }
            else {
              bims_print("Error : Failed to update '$id'", 1, 2);
            }
          }
          else {
            bims_print("'$id' has already existed", 1, 2);
          }
        }
        else {
          $err_msg = self::addInstrunction($id, $inst);
          if ($err_msg) {
            bims_print($err_msg, 1, 2);
            return FALSE;
          }
          else {
            bims_print("'$id' has been added", 1, 2);
          }
        }
      }
    }
    fclose($fdr);
    return TRUE;
  }
}