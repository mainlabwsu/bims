<?php
/**
 * The declaration of BIMS_USER class.
 *
 */
class BIMS_USER extends PUBLIC_BIMS_USER {

  /**
   * Class data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_USER::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_USER::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return $parent;
  }

  /**
   * Returns BIMS_USER by user ID.
   *
   * @param integer $user_id
   *
   * @return BIMS_USER
   */
  public static function byID($user_id) {
    if (!bims_is_int($user_id)) {
      return NULL;
    }
    return BIMS_USER::byKey(array('user_id' => $user_id));
  }

  /**
   * Returns BIMS_USER by user name.
   *
   * @param integer $name
   *
   * @return BIMS_USER
   */
  public static function byName($name) {
    return BIMS_USER::byKey(array('name' => $name));
  }

  /**
   * @see PUBLIC_BIMS_USER::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_USER::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Returns all breeders.
   *
   * @return array of objects
   */
  public static function getBreeders() {
    $sql = "
      SELECT U.user_id
      FROM {bims_user} U
      WHERE U.breeder = :breeder
    ";
    $results = db_query($sql, array(':breeder' => 1));
    $breeders = array();
    while ($user_id = $results->fetchField()) {
      $breeders []= BIMS_USER::byKey(array('user_id' => $user_id));
    }
    return $breeders;
  }

  /**
   * Returns all the users.
   *
   * @param string $type
   *
   * @return array of objects
   */
  public static function getUserObjects($type = 'bims-user') {

    // Gets the users.
    $args = array();
    $sql = '';
    if ($type == 'bims-user') {
      $sql = "
        SELECT U.user_id, U.name
        FROM {users} U
          INNER JOIN {bims_user} BM on BM.user_id = U.uid
      ";
    }
    else if ($type == 'non-bims-user') {
      $sql = "
        SELECT U.user_id, U.name
        FROM {users} U
        WHERE U.uid NOT IN (SELECT user_id FROM {bims_user})
      ";
    }
    else if ($type == 'non-breeder') {
      $sql = "
        SELECT U.uid, U.name
        FROM {users} U
        WHERE U.uid NOT IN (
          SELECT user_id FROM {bims_user} WHERE breeder = :breeder
        )
      ";
      $args[':breeder'] = 1;
    }
    else if ($type == 'breeder') {
      $sql = "
        SELECT U.uid, U.name
        FROM {users} U
          INNER JOIN {bims_user} BU on BU.user_id = U.uid
        WHERE BU.breeder = :breeder
      ";
      $args[':breeder'] = 1;
    }
    $sql .= " ORDER BY U.name ";

    // Gets the user objects.
    $users = array();
    $results = db_query($sql, $args);
    while ($user_obj = $results->fetchObject()) {
      if (trim($user_obj->name)) {
        $users[] = $user_obj;
      }
    }
    return $users;
  }

  /**
   * Returns if the current user has admin priviledge.
   *
   * @return boolean TRUE|FALSE
   */
  public function isBIMSAdmin() {
    return user_access('admin_bims');
  }

  /**
   * Returns the drupal user.
   *
   * @param string $username
   *
   * @return object|NULL
   */
  public static function getDrupalUser($username) {
    $sql = "SELECT * FROM {users} WHERE LOWER(name) = LOWER(:name)";
    return db_query($sql, array(':name' => $username))->fetchObject();
  }

  /**
   * Returns the MCL user.
   *
   * @return MCL_USER|NULL
   */
  public function getMCL_USER() {

    // Gets the MCL_USER.
    $mcl_user = MCL_USER::byKey(array('user_id' => $this->getUserID()));
    if (!$mcl_user) {
      return MCL_USER::addUser($this->getUserID());
    }
    return $mcl_user;
  }

  /**
   * Adds a new user to bims_user table.
   *
   * @param integer $user_id
   *
   * @return BIMS_USER|NULL
   */
  public static function addUser($user_id) {

    // Checks if the user is alreay in BIMS system.
    $bims_user = BIMS_USER::byKey(array('user_id' => $user_id));
    if ($bims_user) {
      return $bims_user;
    }

    // Gets the user properties.
    $sql = "SELECT * FROM {users} WHERE uid = :id";
    $args = array(':id' => $user_id);
    $details = db_query($sql, $args)->fetch(PDO::FETCH_ASSOC);
    $details['user_id'] = $user_id;

    // Sets name as 'anonymous' if $user_id is 0.
    if (!$user_id) {
      $details['name'] = 'anonymous';
    }

    // Adds a new user.
    $new_user = new BIMS_USER($details);
    if ($new_user->insert()) {
      return BIMS_USER::byID($user_id);
    }
    return NULL;
  }

  /**
   * Returns the user directory of this user.
   *
   * @return string
   */
  public function getUserDir() {

    // Builds the user directory path.
    $user_dir = bims_get_config_setting('bims_working_dir') . '/user/' . $this->getName();

    // Creates the directory if not exists.
    bims_create_dir($user_dir);
    return $user_dir;
  }

  /**
   * Returns the URL of the user directory of this user.
   *
   * @return string
   */
  public function getUserDirURL() {

    // Gets the user directory.
    $user_dir = $this->getUserDir();

    // Converts into URL
    $user_url = 'public://bims/user/' . $this->getName();
    return $user_url;
  }

  /**
   * Returns the path of the provided type in the user space.
   *
   * @param string $type
   * @param boolean $url_flag
   *
   * @return string
   */
  public function getPath($type, $url_flag = FALSE) {

    // Gets the user directory.
    $dir = $this->getUserDir();
    $url = $this->getUserDirURL();

    // Sets the valid type array.
    $valid_types = array(
      'bdata', 'job', 'trait', 'field', 'archive',
      'download', 'upload', 'exported', 'tmp',
      'photo', 'backup', 'descriptor', 'stats',
    );

    // Gets the path of the provided type.
    if ($type == 'user') {}
    else if (in_array($type, $valid_types)) {
      $dir .= "/$type";
      $url .= "/$type";
    }
    else {
      return '';
    }

    // Creates the path if not exists.
    if (!file_exists($dir)) {
      bims_create_dir($dir);
    }
    return ($url_flag) ? $url : $dir;
  }

  /**
   * Returns the filepath of the provided type in the user space.
   *
   * @param string $type
   * @param array $args
   * @param boolean $time_flag
   *
   * @return string
   */
  public function getFilepath($args) {

    // Gets the variables.
    $type = $args['type'];
    $ext  = array_key_exists('ext', $args)  ? $args['ext'] : '';
    $keys = array_key_exists('keys', $args) ? $args['keys'] : NULL;
    $time = array_key_exists('time', $args) ? $args['time'] : FALSE;

    // Sets the filename.
    $filename = '';
    if (!empty($keys)) {
      $filename = implode('-', $keys);
    }

    // Uses the type as filename if the kyes is not provided.
    if ($filename == '') {
      $filename = $type;
    }

    // Adds the time.
    if ($time) {
      $filename .= '-' . time();
    }

    // Adds the extension.
    if ($ext) {
      $filename .= '.' . $ext;
    }
    return $this->getPath($type) . "/$filename";
  }

  /**
   * Returns the config array by the provided key. Returns an empty array
   * if not found in prop.
   *
   * @param string $key
   *
   * @return array
   */
  public function getConfig($key) {
    $data = $this->getPropByKey($key);
    if (!$data) {
      $data = array();
    }
    return $data;
  }

  /**
   * Saves and updates the value for the provided key.
   *
   * @param string $key
   * @param string $value
   *
   * @return boolean
   */
  public function updateConfig($key, $value) {
    $this->setPropByKey($key, $value);
    return $this->update();
  }

  /**
   * Returns if this user want to see instruction. If a key 'instruction'
   * exists in user prop array, return the value. if the key dose not exists,
   * add the key and save it as 'show instrunction'.
   *
   * @return boolean
   */
  public function showInstruction() {

    // Checks if the key 'instruction' exists in the prop array.
    if (array_key_exists('instruction', $this->prop_arr)) {
      return $this->getPropByKey('instruction');
    }

    // Adds the key and returns the value.
    $this->setPropByKey('instruction', TRUE);
    $this->update();
    return TRUE;
  }

  /**
   * Set the key 'instruction' in prop array to be FALSE.
   *
   * @return boolean
   */
  public function hideInstruction() {

    // Checks if the key 'instruction' exists in the prop array.
    if (array_key_exists('instruction', $this->prop_arr)) {
      $this->setPropByKey('instruction', FALSE);

    }
    // Adds the key and returns the value.
    else {
      $this->setPropByKey('instruction', FALSE);
    }
    return $this->update();
  }

  /**
   * Sets contact_id to NULL.
   *
   * @return boolean
   */
  public function nullContactID() {
    $sql = "UPDATE {bims_user} SET contact_id = NULL WHERE user_id = :user_id";
    $reulst = db_query($sql, array(':user_id' => $this->user_id));
  }

  /**
   * Returns if this user is the owner of the current program.
   *
   * @return boolean
   */
  public function isOwner() {

    // Gets the selected program.
    $prograrm = $this->getProgram();
    if ($prograrm && $prograrm->isOwner($this->user_id)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns if this user has 'EDIT' permission on the current program.
   *
   * @return boolean
   */
  public function isEditUser() {

    // Gets the selected program.
    $prograrm = $this->getProgram();
    if ($prograrm && $prograrm->isEditUser($this->user_id)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns if this user has 'READ' permission on the current program.
   *
   * @return boolean
   */
  public function isReadUser() {

    // Gets the selected program.
    $prograrm = $this->getProgram();
    if ($prograrm && $prograrm->isReadUser($this->user_id)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns if this user is anonymous.
   *
   * @return boolean
   */
  public function isAnonymous() {
    return ($this->user_id) ? FALSE : TRUE;
  }

  /**
   * Returns if this user has admin privilegde.
   *
   * @return boolean
   */
  public function isAdmin() {
    if ($this->isAnonymous()) {
      return FALSE;
    }
    $account = user_load($this->user_id);
    return user_access('admin_bims', $account);
  }

  /**
   * Adds BIMS anonymous user.
   *
   * @return boolean
   */
  public static function addBIMSAnonymous() {
    $transaction = db_transaction();
    try {
      $name = 'bims.anonymous';
      $type = 'person';
      $desc = 'BIMS anonymous user';
      if (!MCL_CHADO_CONTACT::addContact(NULL, $name, $type, $desc)) {
        throw new Exception("Error : Failed to add BIMS anonymous user");
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      throw new DrupalUpdateException($error);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns BIMS anonymous user.
   *
   * @return boolean
   */
  public function getBIMSAnonymous() {
    $sql = "SELECT * FROM {users} WHERE name = :name";
    return db_query($sql, array(':name' => 'bims.anonymous'))->fetchObject();
  }


  /**
   * Returns all the saved results.
   *
   * @param string $type
   * @param integer $flag
   *
   * @return various
   */
  public function getSavedData($type, $flag = BIMS_OBJECT) {
    $data = array();

    // Returns the saved results.
    if ($type == 'search_result') {
      $sql = "
        SELECT BL.*
        FROM {bims_list} BL
        WHERE BL.type = :type AND BL.program_id = :program_id
          AND BL.user_id = :user_id
      ";
      $args = array(
        ':type'        => 'sample',
        ':user_id'     => $this->user_id,
        ':program_id'  => $this->getProgramID(),
      );
      $resutls = db_query($sql, $args);
      while ($obj = $resutls->fetchObject()) {
        if ($flag == BIMS_OPTION) {
          $data[$obj->list_id] = $obj->name;
        }
        else {
          $data []= $obj;
        }
      }
    }
    return $data;
  }

  /**
   * Returns if the user can edit.
   *
   * @return boolean
   */
  public function canEdit() {
    return ($this->isOwner() || $this->isEditUser()) ? TRUE : FALSE;
  }

  /**
   * Returns if this user has 'BREEDER' permission.
   *
   * @return boolean
   */
  public function isBreeder() {
    return ($this->breeder) ? TRUE : FALSE;
  }


  /**
   * Makes the current user to be breeder.
   */
  public function makeBreeder() {
    $this->setBreeder(1);
    $this->update();
  }

  /**
   * Makes the current user to be non-breeder.
   */
  public function makeNonBreeder() {
    $this->setBreeder(0);
    $this->update();
  }

  /**
   * Generates and returns a session ID.
   *
   * @param string $type
   */
  public function getSessionID($type) {
    return sprintf("%d-%d-%s", $this->getUserID(), $this->getProgramID(), $type);
  }

  /**
   * Returns BIMS_CROP.
   *
   * @return BIMS_CROP|NULL
   */
  public function getCrop() {
    return BIMS_CROP::byID($this->getCropID());
  }

  /**
   * Returns crop ID.
   */
  public function getCropID() {
    if ($this->isAnonymous()) {
      return bims_get_session('crop_id', '');
    }
    return $this->getPropByKey('crop_id');
  }

  /**
   * Updates crop ID.
   */
  public function setCropID($crop_id) {
    if ($this->isAnonymous()) {
      $_SESSION['crop_id'] = $crop_id;
    }
    else {
      $this->setPropByKey('crop_id', $crop_id);
      $this->update();
    }
  }

  /**
   * Returns program ID.
   *
   * @return integer
   */
  public function getProgramID() {
    if ($this->isAnonymous()) {
      return bims_get_session('program_id', '');
    }
    return $this->getPropByKey('program_id');
  }

  /**
   * Updates program ID.
   */
  public function setProgramID($program_id) {
    if ($this->isAnonymous()) {
      $_SESSION['program_id'] = $program_id;
    }
    else {
      $this->setPropByKey('program_id', $program_id);
      $this->update();
    }
  }

  /**
   * Returns current program.
   *
   * @return BIMS_PROGRAM
   */
  public function getProgram() {
    return BIMS_PROGRAM::byID($this->getProgramID());
  }

  /**
   * Resets the current crop.
   *
   * @return boolean
   */
  public function resetCrop() {
    $this->setCropID('');
    $this->setProgramID('');
    return TRUE;
  }

  /**
   * Returns the email option of the provided type. If it is empty,
   * creates and assigns 'FALSE' as the default options.
   *
   * @param string $type
   *
   * @return boolean
   */
  public function getEmailOption($type) {
    $options = $this->getPropByKey('email_option');
    if (!is_array($options)) {
      $options = array();
    }
    if (!array_key_exists($type, $options)) {
      $options[$type] = FALSE;
      $this->updateEmailOption($type, FALSE);
    }
    return $options[$type];
  }

  /**
   * Updates the email option of the provided type.
   *
   * @param string $type
   * @param string $value
   *
   * @return boolean
   */
  public function updateEmailOption($type, $value) {

    // Gets the email options
    $options = $this->getPropByKey('email_option');
    if (!is_array($options)) {
      $options = array();
    }

    // Sets the option value and update the email options.
    $options[$type] = $value;
    $this->setPropByKey('email_option', $options);
    return $this->update();
  }

  /**
   * Returns the instruction option. If it is empty,
   * creates and assigns 'FALSE' as the default options.
   *
   * @return boolean
   */
  public function getInstructionOption() {
    $option = $this->getPropByKey('instruction_option');
    if ($option == '') {
      $this->updateInstructionOption(FALSE);
      $option = FALSE;
    }
    return $option;
  }

  /**
   * Updates the instruction option.
   *
   * @param boolean $bool
   */
  public function updateInstructionOption($bool) {
    $this->setPropByKey('instruction_option', $bool);
    return $this->update();
  }

  /**
   * Returns the lable of BIMS column.
   *
   * @param integer $flag
   *
   * @return string
   */
  public function getBIMSLabel($type, $flag = BIMS_LOWER) {
    $bims_program = $this->getProgram();
    if ($bims_program) {
      return $bims_program->getBIMSLabel($type, $flag);
    }
    return '';
  }

  /*
   * Defines getters and setters below.
   */
   /**
   * Returns the value of the given key in prop.
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}