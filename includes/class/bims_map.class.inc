<?php
/**
 * The declaration of BIMS_MAP class.
 *
 */
class BIMS_MAP {

  /**
   * Class data members.
   */
  protected $def_latitude   = NULL;
  protected $def_longitude  = NULL;
  protected $def_zoom       = NULL;
  protected $def_icon       = NULL;
  protected $map_img_path   = NULL;

 /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {

    // Initializes data members.
    $this->def_latitude   = bims_get_config_setting('bims_def_latitude');
    $this->def_longitude  = bims_get_config_setting('bims_def_longitude');
    $this->def_zoom       = bims_get_config_setting('bims_def_zoom');
    $this->def_icon       = bims_get_config_setting('bims_def_icon');
    $this->map_img_path   = bims_get_config_setting('bims_map_img_path');
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the default latitude.
   *
   * @retrun float
   */
  public function getDefLatitude() {
    return $this->def_latitude;
  }

  /**
   * Retrieves the default longitude.
   *
   * @retrun float
   */
  public function getDefLongitude() {
    return $this->def_longitude;
  }

  /**
   * Retrieves the default zoom.
   *
   * @retrun float
   */
  public function getDefZoom() {
    return $this->def_zoom;
  }

  /**
   * Retrieves the default icon.
   *
   * @retrun string
   */
  public function getDefIcon() {
    return $this->def_icon;
  }

  /**
   * Retrieves the map image path.
   *
   * @retrun string
   */
  public function getMapImgPath() {
    return $this->map_img_path;
  }
}