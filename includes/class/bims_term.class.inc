<?php
/**
 * The declaration of BIMS_TERM class.
 *
 */
class BIMS_TERM extends PUBLIC_BIMS_TERM {

  /**
   * Class data members.
   */
  /**
   * @see PUBLIC_BIMS_TERM::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see PUBLIC_BIMS_TERM::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_TERM by term ID.
   *
   * @param integer $term_id
   *
   * @return BIMS_TERM|NULL
   */
  public static function byID($term_id) {
    if (!bims_is_int($term_id)) {
      return NULL;
    }
    return self::byKey(array('term_id' => $term_id));
  }

  /**
   * Returns BIMS_TERM by name.
   *
   * @param string $name
   *
   * @return BIMS_TERM|NULL
   */
  public static function byName($name) {
    return self::byKey(array('name' => $name));
  }

  /**
   * @see PUBLIC_BIMS_TERM::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Returns the terms.
   *
   * @param integer $flag
   *
   * @return array
   */
  public static function getTerms($flag = BIMS_OBJECT) {

    // Gets the terms.
    $sql = "SELECT * FROM {bims_term} ORDER BY name";
    $results = db_query($sql);
    $terms = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $terms[$obj->term_id] = $obj->name;
      }
      else if ($flag == BIMS_CLASS) {
        $terms[] = BIMS_TERM::byID($obj->term_id);
      }
      else {
        $terms []= $obj;
      }
    }
    return $terms;
  }

  /**
   * Add a term.
   *
   * @param array $details
   *
   * @return BIMS_TERM
   */
  public static function addTerm($details) {

    $transaction = db_transaction();
    try {

      // Checks for duplication
      $bims_term = BIMS_TERM::byName($details['name']);
      if (!$bims_term) {

        // Adds a term.
        $bims_term = new BIMS_TERM($details);
        if (!$bims_term->insert()) {
          throw new Exception("Fail to add a term");
        }
      }
      return $bims_term;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
    return NULL;
  }
}