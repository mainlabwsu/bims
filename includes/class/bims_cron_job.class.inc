<?php
/**
 * The declaration of BIMS_CRON_JOB class.
 *
 */
class BIMS_CRON_JOB {

  /**
   * Class data members.
   */
  protected $module     = 'BIMS';
  protected $skip_keys  = array('year', 'mday', 'hours');

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {}

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Processes a cron job.
   *
   * @param string $type
   * @param boolean $force
   *
   * @return boolean TRUE|FLASE
   */
  public function processCronJob($type = '', $force = FALSE) {

    // Processes a cron job.
    try {

      // Gets the date info.
      $today_arr  = getdate();
      $today_str  = date("Y-m-d H:i:s");

      // Skips cron job.
      if ($force && $this->_skipCronJob($today_arr)) {
        return TRUE;
      }

      // Run the cron jobs.
      if ($this->_runCronJobs($today_arr, $type, $force)) {

        // Add a new cron log entry.
        $date_fields = array(
          'year'        => $today_arr['year'],
          'mon'         => $today_arr['mon'],
          'wday'        => $today_arr['wday'],
          'mday'        => $today_arr['mday'],
          'yday'        => $today_arr['yday'],
          'hours'       => $today_arr['hours'],
          'minutes'     => $today_arr['minutes'],
          'seconds'     => $today_arr['seconds'],
          'called_date' => $today_str,
        );
        db_insert('bims_cron_log')
          ->fields($date_fields)
          ->execute();
      }
      else {
        throw new Exception("Error : Failed to run cron jobs");
      }
    }
    catch (Exception $e) {
      watchdog($this->module, $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Checks if this cron job should be skipped.
   *
   * @param array $today_arr
   *
   * @return boolean
   */
  private function _skipCronJob($today_arr) {

    // Gets the key values.
    $keys = array();
    foreach ((array)$this->skip_keys as $key) {
      $keys[$key] = $today_arr[$key];
    }

    // Checks the keys in the table.
    $cron_log = PUBLIC_BIMS_CRON_LOG::byKey($keys);
    return $cron_log ? TRUE : FALSE;
  }

  /**
   * Runs the cron jobs.
   *
   * @param array $today_arr
   * @param string $type
   * @param boolean $force
   *
   * @return boolean
   */
  private function _runCronJobs($today_arr, $type = '', $force = FALSE) {

    // Runs the cron jobs.
    $cron_jobs = $this->getCronJobs($today_arr, $type, $force);
    foreach ($cron_jobs as $type) {
      $method = 'runCron' . ucfirst($type);
      if (method_exists($this, $method)) {
        if (!$this->{$method}()) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * Returns the all defined cron job.
   *
   * @return array
   */
  public function getCronTypes() {

    // These are all possible cron types.
    $all_cron_types = array('yearly', 'monthly', 'weekly', 'daily', 'hourly', 'minutely', 'secondly');

    // Returns only defined types.
    $defined_types = array();
    foreach ($all_cron_types as $type) {
      $method = 'runCron' . ucfirst($type);
      if (method_exists($this, $method)) {
        $defined_types []= $type;
      }
    }
    return $defined_types;
  }

  /**
   * Returns the all cron job to be run.
   *
   * @param array $today
   * @param string $type
   * @param boolean $force
   *
   * @return boolean TRUE|FLASE
   */
  public function getCronJobs($today, $type = '', $force = FALSE) {

    // Gets the cron job types.
    $all_types = array();
    $cron_types = $this->getCronTypes($type);
    foreach ($cron_types as $cron_type) {

      // Checks if it has already run.
      if (!$force && $this->hasRunCronJob($today, $cron_type)) {
        continue;
      }

      // Adds the cron type.
      $all_types []= $cron_type;
    }
    return $all_types;
  }

  /**
   * Checks the cron job. It returns the number of log entries. If zero was
   * returned, then it should call the cron job.
   *
   * @param array $today
   * @param string $type
   *
   * @return integer
   */
  public function hasRunCronJob($today, $type) {

    // Sets the day for the weekly cron.
    $target_day = 2;

    // Yearly cron job.
    $sql = "
      SELECT COUNT(cron_log_id) FROM {bims_cron_log}
      WHERE year = :year
    ";
    $args = array(':year' => $today['year']);
    if ($type == 'yearly') {
      return db_query($sql, $args)->fetchField();
    }

    // Monthly cron job.
    $sql .= " AND mon = :mon ";
    $args[':mon'] = $today['mon'];
    if ($type == 'monthly') {
      return db_query($sql, $args)->fetchField();
    }

    // Daily cron job.
    $sql .= " AND mday = :mday ";
    $args[':mday'] = $today['mday'];
    if ($type == 'daily') {
      return db_query($sql, $args)->fetchField();
    }

    // Weekly cron job.
    if ($type == 'weekly') {
      if ($today['wday'] != $target_day) {
        return 1;
      }
      $sql .= " AND wday = :wday ";
      $args[':wday'] = $target_day;
      return db_query($sql, $args)->fetchField();
    }

    // Hourly cron job.
    $sql .= " AND hours = :hours ";
    $args[':hours'] = $today['hours'];
    if ($type == 'hourly') {
      return db_query($sql, $args)->fetchField();
    }

    // Minutely cron job.
    $sql .= " AND minutes = :minutes ";
    $args[':minutes'] = $today['minutes'];
    if ($type == 'minutely') {
      return db_query($sql, $args)->fetchField();
    }
    return 1;
  }

  /**
   * The cron job runs monthly.
   *
   * @return boolean
   */
  public function runCronMonthly() {

    // Logs the cron job.
    $this->logCronJob('The monthly cron job was called');
    return TRUE;
  }

  /**
   * The cron job runs weekly.
   *
   * @return boolean
   */
  public function runCronWeekly() {

    // Logs the cron job.
    $this->logCronJob('The weekly cron job was called');
    return TRUE;
  }

  /**
   * The cron job runs daily.
   *
   * @return boolean
   */
  public function runCronDaily() {

    // Logs the cron job.
    $this->logCronJob('The daily cron job was called');
    return TRUE;
  }

  /**
   * Logs the cron job.
   *
   * @param string $msg
   *
   * @return boolean TRUE|FLASE
   */
  public function logCronJob($msg) {
    $args = array(
      '%module' => $this->module,
      '%msg'    => $msg,
      '%date'   => date("Y-m-d G:i:s"),
    );
    watchdog($this->module, "%module : %msg at %date.", $args, WATCHDOG_NOTICE);
  }
}