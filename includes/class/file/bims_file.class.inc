<?php
/**
 * The declaration of BIMS_FILE class.
 *
 */
class BIMS_FILE extends PUBLIC_BIMS_FILE {

 /**
  *  Class data members.
  */
  protected $prop_arr = NULL;
  protected static $file_types = array(
    'ARC' => 'Archive',
    'DL'  => 'Download',
    'FBT' => 'Input Trait',
    'FBF' => 'Input Field',
    'ULE' => 'Uploaded Excel',
    'ULT' => 'Uploaded Trait',
    'ULF' => 'Uploaded Field',
  );

  /**
   * @see PUBLIC_BIMS_FILE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates type by filename if not assigned.
    if (empty($this->type)) {
      $this->assignTypeByFilename();
    }

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_FILE::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_FILE by ID.
   *
   * @param integer $file_id
   *
   * @return BIMS_FILE|NULL
   */
  public static function byID($file_id) {
    if (!bims_is_int($file_id)) {
      return NULL;
    }
    return self::byKey(array('file_id' => $file_id));
  }

  /**
   * @see PUBLIC_BIMS_FILE::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_FILE::insert()
   */
  public function insert() {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new file.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_FILE::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * @see PUBLIC_BIMS_FILE::delete()
   */
  public function delete() {

    // Kills the process if exists and is running.
    $pid = $this->getPropByKey('pid');
    if ($pid) {
      if (file_exists("/proc/$pid" )) {
        exec("kill -9 $pid", $output, $status);
      }
    }

    // Deletes the MCL job if exists.
    $mcl_job_id = $this->getPropByKey('mcl_job_id');
    if ($mcl_job_id) {
      $mcl_job = MCL_JOB_UPLOAD::byID($mcl_job_id);
      if ($mcl_job) {
        if(!$mcl_job->delete()) {
          drupal_set_message("Failed to delete MCL job.", 'error');
          return FALSE;
        }
      }
    }

    // Deletes file_managed if exists.
    $fid = $this->getPropByKey('file_id');
    if ($fid) {
      db_delete('file_managed')
      ->condition('fid', $fid, '=')
      ->execute();
    }

    // Deletes the physical file.
    if (file_exists($this->filepath)) {
      unlink($this->filepath);
    }
    return parent::delete();
  }

  /**
   * Assigns the type by filename.
   *
   */
  private function assignTypeByFilename() {
    if (preg_match("/(xlsx|xls)$/", $this->filename)) {
      $this->type = 'excel';
    }
    else if (preg_match("/\.zip$/", $this->filename)) {
      $this->type = 'zip';
    }
    else if (preg_match("/\.csv$/", $this->filename)) {
      $this->type = 'csv';
    }
    else if (preg_match("/\.trt$/", $this->filename)) {
      $this->type = 'trt';
    }
    else {
      $this->type = 'unknown';
    }
  }

  /**
   * Returns the file types.
   *
   * @param integer $user_id
   * @param string $type
   * @param integer $program_id
   * @param boolean $options
   *
   * @return array
   */
  public static function getFileTypes() {
    return self::$file_types;
  }

  /**
   * Returns the owner of this file.
   *
   * @return BIMS_USER
   */
  public function getOwner() {
    return BIMS_USER::byID($this->user_id);
  }

  /**
   * Returns the valid extension of the provided type.
   *
   * @param string $type
   *
   * @return array
   */
  public static function getValidExt($type) {
    $valid_ext = array();
    if ($type == 'FBT') {
      $valid_ext = array('trt');
    }
    else if ($type == 'FBF') {
      $valid_ext = array('cvs');
    }
    else if ($type == 'ULE') {
      $valid_ext = array('xls xlsx csv gz zip');
    }
    else {
      $valid_ext = array('xls xlsx gz zip csv trt');
    }
    return $valid_ext;
  }

  /**
   * Returns the files.
   *
   * @param array $details
   * @param boolean $flag
   *
   * @return array
   */
  public static function getFiles($details, $flag = BIMS_OBJECT) {

    // Gets and sets the variables.
    $type         = array_key_exists('type', $details)         ? $details['type']         : '';
    $user_id      = array_key_exists('user_id', $details)      ? $details['user_id']      : '';
    $by_user_id   = array_key_exists('by_user_id', $details)   ? $details['by_user_id']   : '';
    $program_id   = array_key_exists('program_id', $details)   ? $details['program_id']   : '';
    $date         = array_key_exists('date', $details)         ? TRUE : FALSE;

    // Adds conditions.
    $where_str = 'F.program_id = :program_id';
    $args = array(':program_id' => $program_id);
    $case_str = '';
    $order_by_str = '';
    if ($by_user_id) {
      $case_str = "
        , CASE
            WHEN F.user_id = :by_user_id THEN 'By you' ELSE 'By group members'
          END AS bywho
      ";
      $args[':by_user_id'] = $by_user_id;
      $order_by_str = ' byWho DESC, ';
    }
    if ($user_id) {
      $where_str .= ' AND F.user_id = :user_id ';
      $args[':user_id'] = $user_id;
    }
    if ($type) {
      $where_str .= ' AND LOWER(F.type) = LOWER(:type) ';
      $args[':type'] = $type;
    }

    // Gets the files.
    $sql = "
      SELECT F.* $case_str
      FROM {bims_file} F
      WHERE $where_str
      ORDER BY $order_by_str F.submit_date DESC
    ";
    $files = array();
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $filename = $obj->filename;
        if ($date) {
          $tmp = explode(' ', $obj->submit_date);
          $submit_date = $tmp[0];
          $filename = "$submit_date : $filename";
        }
        if ($by_user_id) {
          $files[$obj->bywho][$obj->file_id] = $filename;
        }
        else {
          $files[$obj->file_id] = $filename;
        }
      }
      else if ($flag == BIMS_CLASS) {
        $files[] = BIMS_FILE::byID($obj->file_id);
      }
      else {
        $files[] = $obj;
      }
    }
    return $files;
  }

  /**
   * Moves the file to user space.
   *
   * @param BIMS_USER|MCL_USER $bims_user
   * @param object $drupal_file
   *
   * @return string
   */
  public static function moveFile($user, $drupal_file, $type) {

    // Moves the file to the user temp directory.
    $dest_filepath    = $user->getPath($type) . '/' . $drupal_file->filename;
    $target_filepath  = drupal_realpath($drupal_file->uri);
    exec("mv \"$target_filepath\" \"$dest_filepath\"");

    // Removes from file_managed.
    db_delete('file_managed')
      ->condition('fid', $drupal_file->fid, '=')
      ->execute();
    return $dest_filepath;
  }


  /**
   * Moves the file to the specific location.
   *
   * @param BIMS_USER|MCL_USER $bims_user
   * @param object $drupal_file
   * @param string $target_loc
   *
   * @return string
   */
  public static function moveFileLoc($user, $drupal_file, $target_loc) {

    // Moves the file to the user temp directory.
    $dest_filepath    = $target_loc . '/' . $drupal_file->filename;
    $target_filepath  = drupal_realpath($drupal_file->uri);
    exec("mv \"$target_filepath\" \"$dest_filepath\"");

    // Removes from file_managed.
    db_delete('file_managed')
    ->condition('fid', $drupal_file->fid, '=')
    ->execute();
    return $dest_filepath;
  }

  /**
   * Returns MLC file ID of the MLC job.
   *
   * @return integer
   */
  public static function getMCLFileID() {

    // Gets MCL job and returns the file ID.
    $mcl_job_id = $this->getPropByKey('mcl_job_id');
    if ($mcl_job_id) {
      $mcl_job = MCL_JOB_UPLOAD::byID($mcl_job_id);
      if ($mcl_job) {
        return $mcl_file_id = $mcl_job->getFileID();
      }
    }
    return NULL;
  }

  /**
   * Generate file information table.
   *
   * @param array $element
   * @param BIMS_FILE $bims_file
   *
   * @return string
   */
  public static function createInfoTable($element, BIMS_FILE $bims_file = NULL) {

    // Initializes the properties.
    $filename       = '--';
    $uploaded_file  = '';
    $filesize       = '--';
    $submit_date    = '--';
    $complete_date  = '';
    $job_id         = '--';
    $uploader       = '--';
    $desc           = '';
    $status         = '--';
    $actions        = '';
    $download_link  = '';

    // Updates the properites.
    if ($bims_file) {
      $filename     = $bims_file->getFilename();
      $filesize     = $bims_file->getFilesize(TRUE);
      $submit_date  = $bims_file->getSubmitDate();
      $desc         = $bims_file->getDescription();

      // Gets the uploader.
      $bims_user = BIMS_USER::byID($bims_file->getUserID());
      if($bims_user) {
        $uploader = $bims_user->getName();
      }

      // Gets the stutus if it has MCL job ID.
      $mcl_job_id = $bims_file->getPropByKey('mcl_job_id');
      if ($mcl_job_id) {
        $mcl_job = MCL_JOB_UPLOAD::byID($mcl_job_id);
        if ($mcl_job) {
          $status = ucfirst($mcl_job->getStatusLabel());
          if ($mcl_job->getStatus() == 100) {
            $complete_date = $mcl_job->getCompleteDate();
          }

          // Updates the download link.
          $mcl_file = $mcl_job->getMCLFile();
          $download_link = l('Download', 'mcl/download_file/' . $mcl_file->getFileID());
        }
      }
      $job_id = $mcl_job_id;
    }

    // Adds them in textarea.
    $desc = "<textarea class='bims-textarea-400' style='width:100%;' rows=3 READONLY>$desc</textarea>";

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('download_btn', $element)) {
      $actions .= drupal_render($element['download_btn']);
    }
    if (array_key_exists('view_btn', $element)) {
      $actions .= drupal_render($element['view_btn']);
    }
    if (!$actions) {
      $actions = '--';
    }

    // Creates the file information table.
    $rows = array();
    $rows []= array(array('data' => 'File Name', 'width' => '120'), $filename);
    if ($download_link) {
      $rows []= array('Uploaded File', $download_link);
    }
    $rows []= array('File Size', $filesize);
    $rows []= array('Submit&nbsp;Date', $submit_date);
    if ($complete_date) {
      $rows []= array('Complete&nbsp;Date', $complete_date);
    }
    $rows []= array('Job ID', $job_id);
    $rows []= array('Uploader', $uploader);
    $rows []= array('Status', $status);
    $rows []= array('Description', $desc);
    $rows []= array('Actions', $actions);
    $table_vars = array(
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%'),
    );
    $form['file_info'] = array(
      '#markup' => theme('table', $table_vars),
    );
    return theme('table', $table_vars);
  }

  /**
   * Inspects a Field Book field file.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param string $filepath
   *
   * @return array
   */
  public static function inspectFieldFile(BIMS_PROGRAM $bims_program, $filepath) {

    // Initializes the variables.
    $status           = TRUE;
    $req_cols         = '';
    $fb_known_cols    = array('trait', 'value', 'timestamp', 'person', 'location');
    $fb_unknown_cols  = array();

    // Gets the first line of the file.
    if (!file_exists($filepath)) {
      return array('status' => FALSE, 'error' => "$filepath does not exist.");
    }
    $line = fgets(fopen($filepath, 'r'));

    // Tokenizes the header line.
    $line = str_replace('"', '', $line);
    $arr = explode(',', $line);
    $headers = array();
    foreach ($arr as $item) {
      $item = strtolower(trim($item));
      if (array_key_exists($item, $headers)) {
        $headers[$item]++;
      }
      else {
        $headers[$item] = 1;
      }
    }

    // Finds the type of the field file from the header line.
    $template = array_key_exists('timestamp', $headers) ? 'fb_field_database' : 'fb_field_table';

    // Gets the BIMS columns.
    $bims_cols  = $bims_program->getBIMSCols();
    $accession  = $bims_cols['accession'];
    $unique_id  = $bims_cols['unique_id'];
    $primary    = $bims_cols['primary_order'];
    $secondary  = $bims_cols['secondary_order'];

    // Checks the BIMS required columns.
    $fb_known_cols[] = strtolower($accession);
    if (!array_key_exists(strtolower($accession), $headers)) {
      $req_cols .= "$accession (<em>Accession</em>) does not exist.<br />Please rename your <em>'Accession</em>' column to '$accession'<br /><br />";
      }
    $fb_known_cols[] = strtolower($unique_id);
    if (!array_key_exists(strtolower($unique_id), $headers)) {
      $req_cols .= "$unique_id (<em>Unique ID</em>) does not exist.<br />Please rename your '<em>Unique ID</em>' column to '$unique_id'<br /><br />";
    }
    $fb_known_cols[] = strtolower($primary);
    if (!array_key_exists(strtolower($primary), $headers)) {
      $req_cols .= "$primary (<em>Primary order</em>) does not exist.<br />Please rename your '<em>Primary order</em>' column  to '$primary'<br /><br />";
    }
    $fb_known_cols[] = strtolower($secondary);
    if (!array_key_exists(strtolower($secondary), $headers)) {
      $req_cols .= "$secondary (<em>Secondary order</em>) does not exist.<br />Please rename your '<em>Secondary order</em>' column to '$secondary'<br /><br />";
    }
    if ($req_cols == '') {
      $req_cols = 'All the required Field Book columns of a field file are found.<br />' .
      "<div style='margin-left:10px;'><b>accession :</b> $accession.</div>" .
      "<div style='margin-left:10px;margin-top:5px;'><b>Unique ID :</b> $unique_id</div>" .
      "<div style='margin-left:10px;'><b>Primary order :</b> $primary</div>" .
      "<div style='margin-left:10px;'><b>Secondary order :</b> $secondary</div>";
    }
    else {
      $status = FALSE;
    }

    // Gets all unknown columns.
    foreach ($headers as $header => $num) {
      if (!in_array($header, $fb_known_cols)) {
        $fb_unknown_cols []= $header;
      }
    }

    // returns the results of the inspection.
    $inspected = array(
      'status'          => $status,
      'template'        => $template,
      'program_id'      => $bims_program->getProgramID(),
      'cv_arr'          => $bims_program->getCvArr(),
      'filepath'        => $filepath,
      'req_cols'        => $req_cols,
      'fb_unknown_cols' => $fb_unknown_cols,
    );
    return $inspected;
  }

  /**
   * Uploads a file.
   *
   * @param array $args
   *
   * @return BIMS_FILE
   */
  public static function upload($args) {

    // Sets the local variables.
    $mcl_user     = $args['mcl_user'];
    $description  = $args['description'];
    $filepath     = $args['filepath'];
    $param        = $args['param'];
    $program_id   = $param['program_id'];
    $type         = $param['type'];

    // Adds a uploading job.
    $transaction = db_transaction();
    $bims_file = NULL;
    try {

      // Creates a MCL_JOB_UPLOAD and gets job ID.
      $details = array(
        'name'        => strtoupper('BIMS-UPLOAD-' . $type) . date("-Y-m-d-G-i-s"),
        'type'        => 'upload-bims',
        'class_name'  => 'MCL_JOB_UPLOAD',
        'status'      => 0,
        'user_id'     => $mcl_user->getUserID(),
        'description' => $description,
        'submit_date' => date("Y-m-d G:i:s"),
        'prop'        => json_encode(array('working_dir' => dirname($mcl_user->getUserDir()))),
        'param'       => json_encode($param),
      );
      $mcl_job = new MCL_JOB_UPLOAD($details);
      if (!$mcl_job->insert()) {
        throw new Exception("Error : Failed to add MCL a uploading job");
      }
      $job_id = $mcl_job->getJobID();

      // Adds callback function.
      $callbacks = "[\"bims-update-mview-callback $program_id $job_id\"]";
      $mcl_job->setParamByKey('callbacks', $callbacks);
      if (!$mcl_job->update()) {
        throw new Exception("Error : Failed to update job property");
      }

      // Creates a MCL_File.
      $details = array(
        'filename'    => basename($filepath),
        'filepath'    => $filepath,
        'uri'         => '',
        'filesize'    => filesize($filepath),
        'user_id'     => $mcl_user->getUserID(),
        'job_id'      => $job_id,
        'submit_date' => date("Y-m-d G:i:s"),
      );
      $mcl_file = new MCL_FILE($details);
      if (!$mcl_file->insert()) {
        throw new Exception("Error : Failed to add a MCL file");
      }

      // Updates the job properties.
      $mcl_job->setFileID($mcl_file->getFileID());
      if (!$mcl_job->update()) {
        throw new Exception("Error : Failed to update job properties");
      }

      // Creates BIMS_FILE.
      $prop = array('mcl_job_id' => $mcl_job->getJobID());
      $details = array(
        'type'        => $type,
        'filename'    => $mcl_file->getFilename(),
        'filepath'    => $mcl_file->getFilepath(),
        'filesize'    => $mcl_file->getFilesize(),
        'program_id'  => $program_id,
        'user_id'     => $mcl_user->getUserID(),
        'submit_date' => $mcl_file->getSubmitDate(),
        'description' => $description,
        'prop'        => json_encode($prop),
      );
      $bims_file = new BIMS_FILE($details);
      if (!$bims_file->insert()) {
        throw new Exception("Error : Failed to add BIMS_FILE");
      }

      // Sets stdout and stderr log file names.
      $stdout = $mcl_job->getStdFile('stdout');
      $stderr = $mcl_job->getStdFile('stderr');

      // Runs the job.
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd = "$drush mcl-rerun-job $job_id \"$filepath\" --transaction_on > $stdout 2>$stderr  & echo $!";
      $pid = exec($cmd, $output, $return_var);
      if ($return_var) {
        throw new Exception("Error : Failed to add a uploading job");
      }

      // Keeps the pid.
      $bims_file->setPropByKey('pid', $pid);
      $bims_file->update();
    }
    catch (\Exception $e) {
      $transaction->rollback();
      unlink($filepath);
      $error = $e->getMessage();
      return NULL;
    }
    return $bims_file;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the prop_arr.
   *
   * @return array
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   *
   * $param string $key
   *
   * @return string
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * $param string $key
   * $param string $value
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * $param string $key
   * $param string $value
   *
   * @return boolean
   */
  public function updatePropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
    return $this->update();
  }

  /**
   * Returns the file size.
   *
   * @param boolean $format
   */
  public function getFilesize($format = FALSE) {
    $filesize = parent::getFilesize();
    return ($format) ? bims_format_bytes($filesize) : $filesize;
  }
}