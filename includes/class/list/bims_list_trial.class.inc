<?php
/**
 * The declaration of BIMS_LIST_TRIAL class.
 *
 */
class BIMS_LIST_TRIAL extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Gets values from the form_state.
    $node_ids = $form_state['trial'];

    // Sets the params.
    $params = array(
      'trial' => $node_ids,
    );

    // Sets the information.
    $desc       = '';
    $where_stmt = '';
    if (!empty($node_ids)) {
      $tmp = array();
      foreach ($node_ids as $node_id) {
        $trial = BIMS_TRIAL::byID($node_id);
        $tmp []= $trial->getName();
      }
      $desc = implode(',', $tmp);
      $where_stmt = ' node_id IN (' . implode(',', $node_ids) . ')';
    }

    // Creates the filter and return it.
    return array(
      'name'        => 'trial',
      'type'        => 'LIST',
      'list_type'   => 'trial',
      'key'         => 'node_id',
      'desc'        => $desc,
      'params'      => $params,
      'column'      => array(),
      'where_stmt'  => $where_stmt,
      'args'        => array(),
    );
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars($bims_program, $filter) {

    // Sets the headers.
    $headers = array(
      array('data' => 'name','field' => 'name', 'header' => 'Trial Name', 'sort' => 'asc'),
    );

    // Sets the mview.
    $mview = 'bims_node';

    // Sets the conditions.
    $conditions = array(array(
      'key'   => 'node_id',
      'value' => $filter['items'],
      'op'    => 'IN',
    ));

    // Returns the table variables.
    return array('headers' => $headers, 'mview' => $mview, 'conditions' => $conditions);
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Adds the headers.
    $contents = '"Name"' . "\n";

    // Gets all the trials specified in the filter.
    $rows = array();
    foreach ((array)$filter['items'] as $trial_id) {
      $trial = BIMS_TRIAL::byID($trial_id);
      $rows []= array($trial->getName());
    }

    // Sort the rows by stock name.
    usort($rows, function($a, $b) {
      return strnatcmp($a[0], $b[0]);
    });

    // Writes data line.
    foreach ((array)$rows as $row) {
      $contents .= '"' . implode('","', str_replace('"', '\"', $row)) . "\"\n";
    }
    return $contents;
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
}