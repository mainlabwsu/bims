<?php
/**
 * The declaration of BIMS_LIST_CROSS class.
*
*/
class BIMS_LIST_CROSS extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Gets values from the form_state.
    $nd_experiment_ids = $form_state['cross'];

    // Sets the params.
    $params = array(
      'cross' => $nd_experiment_ids,
    );

    // Sets the information.
    $desc       = '';
    $where_stmt = '';
    if (!empty($nd_experiment_ids)) {
      $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $bims_program->getNodeID()));
      $tmp = array();
      foreach ($nd_experiment_ids as $nd_experiment_id) {
        $cross = $bm_cross->getCross($nd_experiment_id);
        $tmp []= $cross->cross_number;
      }
      $desc = implode(',', $tmp);
      $where_stmt = ' nd_experiment_id IN (' . implode(',', $nd_experiment_ids) . ')';
    }

    // Creates the filter and return it.
    return array(
      'name'        => 'cross',
      'type'        => 'LIST',
      'list_type'   => 'cross',
      'key'         => 'nd_experiment_id',
      'desc'        => $desc,
      'params'      => $params,
      'column'      => array(),
      'where_stmt'  => $where_stmt,
      'args'        => array(),
    );
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars($bims_program, $filter) {

    // Sets the headers.
    $headers = array(
      array('data' => 'cross_number','field' => 'cross_number', 'header' => 'Cross Number', 'sort' => 'asc'),
      array('data' => 'maternal', 'field' => 'maternal', 'header' => 'Maternal'),
      array('data' => 'paternal', 'field' => 'paternal', 'header' => 'Paternal'),
    );

    // Adds extra columns in the preference in the filter.
    $preference = $filter['preference'];
    foreach ((array)$preference['crossprop'] as $field => $info) {
      if ($info['value']) {
        $headers []= array('data' => $field, 'field' => $field, 'header' => $info['name'], 'sort' => 'asc');
      }
    }

    // Sets the mview.
    $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $bims_program->getNodeID()));
    $m_cross  = $bm_cross->getMView();

    // Sets the conditions.
    $conditions = array();
    if (array_key_exists('items', $filter)) {
      $conditions = array(array(
        'key'   => 'nd_experiment_id',
        'value' => $filter['items'],
        'op'    => 'IN',
      ));
    }

    // Returns the table variables.
    return array('headers' => $headers, 'mview' => $m_cross, 'conditions' => $conditions);
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return '';
    }

    // Gets the filter variables.
    $sql  = $filter['sql'];
    $args = $filter['args'];

    // Gets BIMS_MVIEW_CROSS.
    $program_id = $filter['program_id'];
    $bm_cross   = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
    $m_cross    = $bm_cross->getMView();

    // Sets the headers.
    $preference = $filter['preference'];
    $headers    = array('Cross Number', 'Maternal', 'Parternal');
    $prop_arr   = array('cross_number', 'maternal', 'paternal');
    foreach ((array)$preference['crossprop'] as $field => $info) {
      if ($info['value']) {
        $headers []= $info['name'];
        $prop_arr []= $field;
      }
    }

    // Adds the stats rows.
    if ($param['stats']) {
      $stats_param = array(
        'sql'   => $sql,
        'args'  => $args,
        'from'  => $m_cross,
      );
      self::addStatsRows($fdw, $stats_param, $prop_arr, $preference['crossprop']);
      fputcsv($fdw, array());
    }

    // Adds the headers.
    fputcsv($fdw, $headers);

    // Gets all the cross.
    $sql_final = "
      SELECT MV.*
      FROM $m_cross MV
      WHERE MV.nd_experiment_id IN (SELECT L.nd_experiment_id FROM ($sql) L )
      ORDER BY MV.cross_number
    ";
    $results = db_query($sql_final, $args);
    while ($obj = $results->fetchObject()) {
      $row = array();
      foreach ($prop_arr as $field) {
        $row []= $obj->{$field};
      }
      fputcsv($fdw, $row);
    }
    fclose($fdw);
    return file_get_contents($filepath);
  }


  /**
   * Adds the stats rows at the top for a wide form.
   *
   * @param file descriptor $fdw
   * @param array $param
   * @param array $prop_arr
   * @param array $properties
   *
   * @return string
   */
  private static function addStatsRows($fdw, $param, $prop_arr, $properties) {

    // Gets all stats.
    $bims_stats = new BIMS_STATS();
    $desc_stats = array();
    foreach ((array)$properties as $field => $info) {
      if ($info['value']) {
        if (preg_match("/^p(\d+)$/", $field, $matches)) {
          $cvterm_id = $matches[1];
          $property = MCL_CHADO_CVTERM::byID($cvterm_id);

          // Adds the stats if it can.
          if (BIMS_CVTERM::canStats($property)) {
            $param['name']      = $property->getName();
            $param['field']     = $field;
            $param['format']    = $property->getFormat();
            $desc_stats[$field] = $bims_stats->calcStats($param);
          }
        }
      }
    }
    if (empty($desc_stats)) {
      return;
    }

    // Initializes the stats rows.
    $stats_rows = array(
      array('# Data'),
      array('Maximum'),
      array('Minimum'),
      array('Mean'),
      array('STD'),
      array('Frequency'),
    );

    // Populates the stats rows.
    $idx = 0;
    foreach ($prop_arr as $field) {
      if (array_key_exists($field, $desc_stats)) {
        $stats_rows[0][$idx] = $desc_stats[$field]['num'];
        if (array_key_exists('max', $desc_stats[$field])) {
          $stats_rows[1][$idx] = $desc_stats[$field]['max'];
        }
        if (array_key_exists('min', $desc_stats[$field])) {
          $stats_rows[2][$idx] = $desc_stats[$field]['min'];
        }
        if (array_key_exists('mean', $desc_stats[$field])) {
          $stats_rows[3][$idx] = $desc_stats[$field]['mean'];
        }
        if (array_key_exists('std', $desc_stats[$field])) {
          $stats_rows[4][$idx] = $desc_stats[$field]['std'];
        }
        if (array_key_exists('freq', $desc_stats[$field])) {
          $freq = $desc_stats[$field]['freq'];
          $freq_arr = array();
          arsort($freq);
          foreach ((array)$freq as $key => $val) {
            $freq_arr []= "$key:$val";
          }
          $stats_rows[5][$idx] = implode(';', $freq_arr);
        }
        if (array_key_exists('true', $desc_stats[$field])) {
          $num_true   = $desc_stats[$field]['true'];
          $num_false  = $desc_stats[$field]['false'];
          $stats_rows[5][$idx] = "TRUE:$num_true;FALSE:$num_false";
        }
      }
      $idx++;
    }

    // Adds the stats rows.
    fputcsv($fdw, array());
    $num_cols = sizeof($prop_arr);
    for ($i = 0; $i < 6; $i++) {
      $row = array();
      for ($j = 0; $j < $num_cols; $j++) {
        if (isset($stats_rows[$i][$j])) {
          $row []= $stats_rows[$i][$j];
        }
        else {
          $row []= '';
        }
      }
      fputcsv($fdw, $row);
    }
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
}