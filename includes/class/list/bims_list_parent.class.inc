<?php
/**
 * The declaration of BIMS_LIST_PARENT class.
 *
 */
class BIMS_LIST_PARENT extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Gets values from the form_state.
    $stock_ids_m  = $form_state['maternal'];
    $parent_op    = $form_state['parent_op'];
    $stock_id_p   = $form_state['paternal'];


    // Sets the params.
    $params = array(
      'maternal'  => $stock_ids_m,
      'parent_op' => $parent_op,
      'paternal'  => $stock_id_p,
    );

    // Sets the information.
    $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));

    // Maternal.
    $m_cond =  $m_desc = '';
    if (!empty($stock_ids_m)) {
      $tmp = array();
      foreach ($stock_ids_m as $stock_id) {
        $stock = $mview_stock->getStock($stock_id, BIMS_OBJECT);
        $tmp []= $stock->name;
      }
      $m_desc = 'Maternal:' . implode(',', $tmp);
      $m_cond = ' stock_id_m IN (' . implode(',', $stock_ids_m) . ')';
    }

    // Paternal.
    $f_cond = $f_desc = '';
    if (!empty($stock_ids_p)) {
      $tmp = array();
      foreach ($stock_ids_p as $stock_id) {
        $stock = $mview_stock->getStock($stock_id, BIMS_OBJECT);
        $tmp []= $stock->name;
      }
      $f_desc = 'Paternal:' . implode(',', $tmp);
      $f_cond = ' stock_id_p IN (' . implode(',', $stock_ids_p) . ')';
    }

    // Updates the SQL and the description.
    $desc       = '';
    $where_stmt = '';
    if ($m_cond && $f_cond) {
      $where_stmt = "$m_cond $parent_op $f_cond";
      $desc = "$m_desc\n$f_desc";
    }
    else if ($m_cond) {
      $where_stmt = $m_cond;
      $desc = $m_desc;
    }
    else if ($f_cond) {
      $where_stmt = $f_cond;
      $desc = $f_desc;
    }

    // Creates the filter and return it.
    return array(
      'name'        => 'parent',
      'type'        => 'LIST',
      'list_type'   => 'parent',
      'key'         => 'stock_id',
      'desc'        => $desc,
      'params'      => $params,
      'column'      => array(),
      'where_stmt'  => $where_stmt,
      'args'        => array(),
    );
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars($bims_program, $filter) {

    // Sets the headers.
    $headers = array(
      array('data' => 'maternal','field' => 'maternal', 'header' => 'Maternal', 'sort' => 'asc'),
      array('data' => 'paternal','field' => 'paternal', 'header' => 'Paternal'),
      array('data' => 'cross_number','field' => 'cross_number', 'header' => 'Cross Number'),
    );

    // Sets the mview.
    $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $bims_program->getNodeID()));
    $mview = $mview_phenotype->getMView();

    // Sets the conditions.
    $conditions = array(array(
      'key'   => 'stock_id',
      'value' => $filter['items'],
      'op'    => 'IN',
    ));

    // Returns the table variables.
    return array('headers' => $headers, 'mview' => $mview, 'conditions' => $conditions);
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Gets the program ID.
    $program_id = $filter['program_id'];

    // Gets BIMS_MVIEW_CROSS and BIMS_MVIEW_STOCK.
    $bm_stock = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
    $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
    $m_cross  = $bm_cross->getMView();
    $m_stock  = $bm_stock->getMView();

    // Adds the headers.
    $contents = '"Maternal","Paternal","Progeny","Cross Number"' . "\n";

    // Gets all the locations specified in the filter.
    $stock_ids = implode(',', $filter['items']);
    $sql = "
      SELECT MVS.*, MVC.cross_number
      FROM $m_stock MVS
        INNER JOIN $m_cross MVC on MVC.stock_id_m = MVS.stock_id_m AND MVC.stock_id_p = MVS.stock_id_p
      WHERE MVS.stock_id IN ($stock_ids)
      ORDER BY MVS.maternal, MVS.paternal, MVC.cross_number
    ";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $progenies = '';
      if ($obj->progeny) {
        $arr = json_decode($obj->progeny, TRUE);
        $progenies = implode(',', $arr);
      }
      $row = array($obj->maternal, $obj->paternal, $obj->name, $obj->cross_number);
      $contents .= '"' . implode('","', str_replace('"', '\"', $row)) . "\"\n";
    }
    return $contents;
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {


    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
}