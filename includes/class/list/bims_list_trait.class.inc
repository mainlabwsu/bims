<?php
/**
 * The declaration of BIMS_LIST_TRAIT class.
 *
 */
class BIMS_LIST_TRAIT extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Gets values from the form_state.
    $cvterm_ids = $form_state['trait'];

    // Sets the params.
    $params = array(
      'trait' => $cvterm_ids,
    );

    // Sets the information.
    $desc       = '';
    $where_stmt = '';
    $column     = array();
    if (!empty($cvterm_ids)) {
      $tmp = array();
      $program_id = $bims_program->getProgramID();
      foreach ($cvterm_ids as $cvterm_id) {
        $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id);
        $column[$cvterm_id] = $descriptor->getName();
        $tmp []= $descriptor->getName();
      }
      $desc = implode(',', $tmp);
      $op = 'OR';
      $where_stmt = ' (t' . implode(" != '' $op t", $cvterm_ids) . " != '') ";
    }

    // Creates the filter and return it.
    return array(
      'name'        => 'trait',
      'type'        => 'LIST',
      'list_type'   => 'trait',
      'key'         => 'cvterm_id',
      'desc'        => $desc,
      'params'      => $params,
      'column'      => $column,
      'where_stmt'  => $where_stmt,
      'args'        => array(),
    );
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars($bims_program, $filter) {

    // Sets the headers.
    $headers = array(
      array('data' => 'name', 'field' => 'name', 'header' => 'Name', 'sort' => 'asc'),
      array('data' => 'format', 'field' => 'format', 'header' => 'Format'),
      array('data' => 'definition', 'field' => 'definition', 'header' => 'Definition'),
    );

    // Sets the mview.
    $mview = 'bims_mview_descriptor';

    // Sets the conditions.
    $conditions = array(array(
      'key'   => 'cvterm_id',
      'value' => array_keys($filter['items']),
      'op'    => 'IN',
    ));

    // Returns the table variables.
    return array('headers' => $headers, 'mview' => $mview, 'conditions' => $conditions);
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Adds the headers.
    $contents = '"Name","Format","Definition"' . "\n";

    // Gets all the locations specified in the filter.
    $rows = array();
    foreach ((array)$filter['items'] as $cvterm_id => $name) {
      $descriptor = MCL_CHADO_CVTERM::byID($cvterm_id);
      $rows []= array($name, $descriptor->getFormat(), $descriptor->getDefinition());
    }

    // Sort the rows by stock name.
    usort($rows, function($a, $b) {
      return strnatcmp($a[0], $b[0]);
    });

    // Writes data line.
    foreach ((array)$rows as $row) {
      $contents .= '"' . implode('","', str_replace('"', '\"', $row)) . "\"\n";
    }
    return $contents;
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {
    return TRUE;
  }

  /*
   * Defines getters and setters below.
   */
}