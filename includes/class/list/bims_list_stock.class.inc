<?php
/**
 * The declaration of BIMS_LIST_STOCK class.
 *
 */
class BIMS_LIST_STOCK extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Gets values from the form_state.
    $stock_ids = $form_state['stock'];

    // Sets the params.
    $params = array(
      'stock' => $stock_ids,
    );

    // Sets the information.
    $desc       = '';
    $where_stmt = '';
    if (!empty($stock_ids)) {
      $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));
      $tmp = array();
      foreach ($stock_ids as $stock_id) {
        $stock = $mview_stock->getStock($stock_id, BIMS_OBJECT);
        $tmp []= $stock->name;
      }
      $desc = implode(',', $tmp);
      $where_stmt = ' stock_id IN (' . implode(',', $stock_ids) . ')';
    }

    // Creates the filter and return it.
    return array(
      'name'        => 'stock',
      'type'        => 'LIST',
      'list_type'   => 'stock',
      'key'         => 'stock_id',
      'desc'        => $desc,
      'params'      => $params,
      'column'      => array(),
      'where_stmt'  => $where_stmt,
      'args'        => array(),
    );
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars(BIMS_PROGRAM $bims_program, $filter) {

    // Gets accession label.
    $accession = $bims_program->getBIMSLabel('accession');

    // Sets the headers.
    $headers = array(
      array('data' => 'name','field' => 'name', 'header' => $accession, 'sort' => 'asc'),
      array('data' => 'genus','field' => 'genus', 'header' => 'Genus'),
      array('data' => 'species','field' => 'species', 'header' => 'Species'),
      array('data' => 'maternal','field' => 'maternal', 'header' => 'Maternal'),
      array('data' => 'paternal','field' => 'paternal', 'header' => 'Paternal'),
    );

    // Sets the mview.
    $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getNodeID()));
    $mview = $mview_stock->getMView();

    // Sets the conditions.
    $conditions = array(array(
      'key'   => 'stock_id',
      'value' => $filter['items'],
      'op'    => 'IN',
    ));

    // Returns the table variables.
    return array('headers' => $headers, 'mview' => $mview, 'conditions' => $conditions);
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Gets the program ID.
    $program_id = $filter['program_id'];

    // Gets BIMS_MVIEW_CROSS.
    $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
    $mview = $mview_stock->getMView();

    // Adds the headers.
    $contents = '"Stock","Alias","Genus","Species","Type","cultivar","Maternal","Paternal"' . "\n";

    // Gets all the locations specified in the filter.
    $stock_ids = implode(',', $filter['items']);
    $sql = "
      SELECT MV.*
      FROM $mview MV
      WHERE MV.stock_id IN ($stock_ids)
      ORDER BY MV.name
    ";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $row = array($obj->name, $obj->alias, $obj->genus, $obj->species, $obj->type, $obj->cultivar, $obj->maternal, $obj->paternal);
      $contents .= '"' . implode('","', str_replace('"', '\"', $row)) . "\"\n";
    }
    return $contents;
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {


    return TRUE;
  }
}