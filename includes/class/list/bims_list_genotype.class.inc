<?php
/**
 * The declaration of BIMS_LIST_GENOTYPE class.
 *
 */
class BIMS_LIST_GENOTYPE extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Local variables.
    $type       = $details['type'];
    $key        = $details['key'];
    $list_type  = $details['list_type'];
    $property   = $details['property'];

    // Copies the preference from the previous one.
    $preference = $filters[sizeof($filters) - 1]['preference'];

    // Gets the values in the form_state.
    $where_stmt = '';
    $desc       = '';
    $args       = array();
    $params     = array();
    $traits     = array();

    // Marker.
    if ($property == 'marker') {
      $feature_ids      = $form_state['marker'];
      $params['marker'] = $feature_ids;

      // Sets the information.
      if (!empty($feature_ids)) {

        // Gets BIMS_MVIEW_MARKER.
        $bm_marker = new BIMS_MVIEW_MARKER(array('node_id' => $bims_program->getProgramID()));

        // Sets the description.
        $desc_arr = array();
        foreach ($feature_ids as $feature_id) {
          $marker = $bm_marker->getMarker($feature_id, BIMS_OBJECT);
          if ($marker) {
            $desc_arr []= $marker->name;
          }
        }
        $desc = implode(', ', $desc_arr);

        // Sets the condition.
        $where_stmt = " feature_id IN (" . implode(',', $feature_ids) . ')';
      }
    }

    // Accession.
    else if ($property == 'accession') {
      $stock_ids            = $form_state['accession'];
      $params['accession']  = $stock_ids;

      // Sets the information.
      if (!empty($stock_ids)) {

        // Gets BIMS_MVIEW_STOCK.
        $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));

        // Sets the description.
        $desc_arr = array();
        foreach ($stock_ids as $stock_id) {
          $stock = $mview_stock->getStock($stock_id, BIMS_OBJECT);
          if ($stock) {
            $desc_arr []= $stock->name;
          }
        }
        $desc = implode(', ', $desc_arr);

        // Sets the condition.
        $where_stmt = " stock_id IN (" . implode(',', $stock_ids) . ')';
      }
    }

    // Geneotype.
    else if ($property == 'genotype') {}

    // Updates the filters array.
    if ($where_stmt) {
      $filter = array(
        'bims_program'  => $bims_program,
        'name'          => $property,
        'type'          => $type,
        'list_type'     => $list_type,
        'key'           => $key,
        'desc'          => $desc,
        'params'        => $params,
        'where_stmt'    => $where_stmt,
        'args'          => $args,
        'traits'        => $traits,
        'preference'    => $preference,
      );
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars($bims_program, $filter) {

    // Gets the label for accession.
    $accession = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

    // Initializes the headers with accession column only.
    $headers = array(
      array('data' => 'accession', 'field' => 'accession', 'header' => $accession, 'sort' => 'asc'),
    );
    $header_sort = array('accession' => FALSE);

    // Adds extra columns in the preference in the filter.
    $preference = $filter['preference'];
    foreach ((array)$preference['gc_accessionprop'] as $field => $info) {
      if ($info['value']) {
        $header_sort[$field] = FALSE;
        $headers []= array('data' => $field, 'field' => $field, 'header' => $info['name'], 'sort' => 'asc');
      }
    }
    foreach ((array)$preference['markerprop'] as $field => $info) {
      if ($info['value']) {
        $header_sort[$field] = FALSE;
        $headers []= array('data' => $field, 'field' => $field, 'header' => $info['name'], 'sort' => 'asc');
      }
    }

    // Gets the mview.
    $mview_genotype = new BIMS_MVIEW_GENOTYPE(array('node_id' => $bims_program->getNodeID()));
    $mview = $mview_genotype->getMView();

    // Sets the conditions.
    $conditions = array(array(
      'key'   => 'stock_id',
      'value' => $filter['items'],
      'op'    => 'IN',
    ));

    // Returns the table variables.
    return array('headers' => $headers, 'header_sort' => $header_sort, 'mview' => $mview, 'conditions' => $conditions);
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Gets the program ID.
    $program_id = $filter['program_id'];

    // Gets the filepath.
    $bims_file = NULL;
    if (!is_string($filepath)) {
      $bims_file = $filepath;
      $filepath = $bims_file->getFilepath();
    }

    // Checks BIMS_MVIEW_GENOTYPE, BIMS_MVIEW_GENOTYPE_MC, BIMS_MVIEW_GENOTYPE_AC
    // and BIMS_MVIEW_PHENOTYPE.
    $mview_genotype = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
    if (!$mview_genotype->exists()) {
      return '';
    }
    $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    if (!$mview_phenotype->exists()) {
      return '';
    }

    // Sets the parameters.
    $param['filepath']          = $filepath;
    $param['filter']            = $filter;
    $param['bims_program']      = BIMS_PROGRAM::byID($program_id);
    $param['mview_genotype']    = $mview_genotype;
    $param['mview_phenotype']   = $mview_phenotype;
    $param['save_file']         = $save_file;
    $param['bims_file']         = $bims_file;

    // Gets the contents.
    if ($param['file_form'] == 'long') {
      return self::getGenotypeDataLong($param);
    }
    else if ($param['file_form'] == 'wide_marker' || $param['file_form'] == 'wide_accession') {
      return self::getGenotypeDataWide($param);
    }
    else if ($param['file_form'] == 'vcf') {
      return self::getGenotypeDataVCF($param);
    }
    return '';
  }

  /**
   * Returns the genotypic data in a long form
   *
   * @param array $param
   *
   * @return string
   */
  private static function getGenotypeDataLong($param = array()) {

    // Gets the parameters.
    $filepath           = $param['filepath'];
    $filter             = $param['filter'];
    $bims_program       = $param['bims_program'];
    $mview_phenotype    = $param['mview_phenotype'];
    $mview_genotype     = $param['mview_genotype'];
    $save_file          = $param['save_file'];
    $bims_file          = $param['bims_file'];

    // Local variables.
    $program_id     = $bims_program->getProgramID();
    $mview          = $mview_genotype->getMView();
    $markerprop     = FALSE;
    $accessionprop  = FALSE;
    $preference     = $filter['preference'];
    $accession      = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
    $headers        = array($accession, 'Marker', 'Genotype');
    $prop_arr       = array('accession', 'marker', 'genotype');

    // Sets SELECT statement.
    $select_str = ' TT.accession, TT.marker, TT.genotype';

    // Adds accession properties.
    foreach ((array)$preference['gc_accessionprop'] as $field => $info) {
      if ($info['value']) {
        $headers []= $info['name'];
        $prop_arr []= $field;
        $select_str .= ", GA.$field";
        $accessionprop = TRUE;
      }
    }

    // Adds marker properties.
    foreach ((array)$preference['markerprop'] as $field => $info) {
      if ($info['value']) {
        $headers []= $info['name'];
        $prop_arr []= $field;
        $select_str .= ", GM.$field";
        $markerprop = TRUE;
      }
    }

    // Adds genotype properties.
    foreach ((array)$preference['genotypeprop'] as $field => $info) {
      if ($info['value']) {
        $headers []= $info['name'];
        $prop_arr []= $field;
        $select_str .= ", TT.$field";
      }
    }

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return '';
    }

    // Adds the headers.
    fputcsv($fdw, $headers);

      // Adds property columns.
    $inner_join_prop = '';
    if ($accessionprop) {
      $bims_mview = new BIMS_MVIEW_GENOTYPE_ACCESSION(array('node_id' => $program_id));
      $inner_join_prop .= ' INNER JOIN ' . $bims_mview->getMView() . ' GA on GA.stock_id = TT.stock_id ';
    }
    if ($markerprop) {
      $bims_mview = new BIMS_MVIEW_GENOTYPE_MARKER(array('node_id' => $program_id));
      $inner_join_prop .= ' INNER JOIN ' . $bims_mview->getMView() . ' GM on GM.feature_id = TT.feature_id ';
    }

    // Populates the data rows.
    $sql = "
      SELECT DISTINCT $select_str
      FROM (" . $filter['sql'] . ") TT
        $inner_join_prop
      ORDER BY TT.accession
    ";
    if ($filter['type'] == 'SEARCH_PHENOTYPE') {
      $sql = "
        SELECT DISTINCT $select_str FROM $mview TT
          $inner_join_prop
          INNER JOIN (" . $filter['sql'] . ") K on K.stock_id = TT.stock_id
        ORDER BY TT.accession
      ";
    }
    else if ($filter['type'] == 'SEARCH_GENOTYPE_ACCESSION') {
      $sql = "
        SELECT DISTINCT $select_str FROM $mview TT
          $inner_join_prop
          INNER JOIN (" . $filter['sql'] . ") K on K.stock_id = TT.stock_id
        ORDER BY TT.accession
      ";
    }
    else if ($filter['type'] == 'SEARCH_GENOTYPE_MARKER') {
      $sql = "
        SELECT DISTINCT $select_str FROM $mview TT
          $inner_join_prop
          INNER JOIN (" . $filter['sql'] . ") K on K.feature_id = TT.feature_id
        ORDER BY TT.accession
      ";
    }

    // Gets the num;ber of record.
    $num_records = 0;
    if ($save_file) {
      $csql = "SELECT COUNT(C.*) FROM ($sql) C";
      $num_records = db_query($csql, $filter['args'])->fetchField();
    }

    // Writes data to file.
    $results = db_query($sql, $filter['args']);
    $row = NULL;
    $count = 0;
    while ($obj = $results->fetchObject()) {

      // Updates the progress.
      if ($bims_file) {
        if (++$count % 1000 == 0) {
          $status = sprintf ("%d / %d = %.03f\n", $count, $num_records, $count/$num_records*100.0);
          $bims_file->updatePropByKey('status', $status);
        }
      }

      // Adds the properties.
      $row = array();
      foreach ($prop_arr as $field) {
        $row []= $obj->{$field};
      }
      fputcsv($fdw, $row);
    }
    fclose($fdw);

    // Update the status.
    if ($bims_file) {
      $bims_file->setFilesize(filesize($filepath));
      $bims_file->updatePropByKey('status', 'completed');
    }

    // Returns the results.
    if ($save_file) {
      return '';
    }
    return file_get_contents($filepath);
  }

  /**
   * Returns the genotypic data in a wide form.
   *
   * @param array $param
   *
   * @return string filepath
   */
  private static function getGenotypeDataWide($param = array()) {

    // Gets the parameters.
    $filepath       = $param['filepath'];
    $filter         = $param['filter'];
    $bims_program   = $param['bims_program'];
    $mview_genotype = $param['mview_genotype'];
    $file_form      = $param['file_form'];
    $save_file      = $param['save_file'];
    $bims_file      = $param['bims_file'];

    // Gets the preference.
    $preference = $filter['preference'];

    // Local variables.
    $program_id     = $bims_program->getProgramID();
    $type           = $filter['type'];
    $mview          = $mview_genotype->getMView();
    $select_tt_str  = " SS.genotype_data ";

    $fixed_cols     = array();
    $dynamic_cols   = array();
    $f_sql          = $filter['sql'];
    $f_args         = $filter['args'];
    $markerprop     = FALSE;
    $accessionprop  = FALSE;

    // Sets the join statement.
    $join_str = '';
    if ($type == 'SEARCH_PHENOTYPE') {
      $join_str = "INNER JOIN ($f_sql) TJ ON TJ.stock_id = TS.stock_id";
    }
    else if ($type == 'SEARCH_GENOTYPE_ACCESSION') {
      $join_str = "INNER JOIN ($f_sql) TJ ON TJ.stock_id = TS.stock_id";
    }
    else if ($type == 'SEARCH_GENOTYPE_MARKER') {
      $join_str = "INNER JOIN ($f_sql) TJ ON TJ.feature_id = TS.feature_id";
    }

    // Wide form : accession columns.
    $select_str     = '';
    $order_by_str   = '';
    $group_by_str   = '';
    $join_cond_str  = '';
    if ($file_form == 'wide_accession') {

      // Sets select/order/group/join of the SQL statement.
      $select_str     = " TS.feature_id, TS.project_id, string_agg(DISTINCT TS.stock_id || ':' || TS.genotype, ',') AS genotype_data ";
      $group_by_str   = " GROUP BY TS.marker, TS.feature_id, TS.project_id ";
      $order_by_str   = " ORDER BY TT.marker ";
      $join_cond_str  = " SS on SS.feature_id = TT.feature_id AND SS.project_id = TT.project_id ";

      // Adds the marker properties.
      $fixed_cols['marker'] = 'marker';
      $select_tt_str .= " , TT.marker ";
      foreach ((array)$preference['markerprop'] as $field => $info) {
        if ($info['value']) {
          $select_tt_str .= " , GM.$field ";
          $fixed_cols[$field]= $info['name'];
          $markerprop = TRUE;
        }
      }

      // Adds the genotype properties.
      foreach ((array)$preference['genotypeprop'] as $field => $info) {
        if ($info['value'] && $info['def']) {
          $select_tt_str .= " , TT.$field ";
          $fixed_cols[$field]= $info['name'];
        }
      }

      // Adds the dynamic columns.
      $d_sql = "
        SELECT DISTINCT TS.stock_id, TS.accession FROM $mview TS
        $join_str
      ";
      $results = db_query($d_sql, $f_args);
      while ($obj = $results->fetchObject()) {
        $dynamic_cols[$obj->stock_id] = $obj->accession;
      }
    }

    // Wide form : marker columns.
    else if ($file_form == 'wide_marker') {

      // Sets select/order/group/join of the SQL statement.
      $select_str     = " TS.stock_id, TS.project_id, string_agg(DISTINCT TS.feature_id || ':' || TS.genotype, ',') AS genotype_data ";
      $group_by_str   = " GROUP BY TS.accession, TS.stock_id, TS.project_id ";
      $order_by_str   = " ORDER BY TT.accession ";
      $join_cond_str  = " SS on SS.stock_id = TT.stock_id AND SS.project_id = TT.project_id ";

      // Adds the accession and accession properties.
      $accession = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
      $fixed_cols['accession'] = $accession;
      $select_tt_str .= " , TT.accession ";
      foreach ((array)$preference['gc_accessionprop'] as $field => $info) {
        if ($info['value']) {
          $select_tt_str .= " , GA.$field ";
          $fixed_cols[$field]= $info['name'];
          $accessionprop = TRUE;
        }
      }

      // Adds the genotype properties.
      foreach ((array)$preference['genotypeprop'] as $field => $info) {
        if ($info['value'] && $info['def']) {
          $select_tt_str .= " , TT.$field ";
          $fixed_cols[$field]= $info['name'];
        }
      }

      // Adds the dynamic columns.
      $d_sql = "
        SELECT DISTINCT TS.feature_id, TS.marker FROM $mview TS
        $join_str
      ";
      $results = db_query($d_sql, $f_args);
      while ($obj = $results->fetchObject()) {
        $dynamic_cols[$obj->feature_id] = $obj->marker;
      }
    }

    // Adds property columns.
    $inner_join_prop = '';
    if ($accessionprop) {
      $bims_mview = new BIMS_MVIEW_GENOTYPE_ACCESSION(array('node_id' => $program_id));
      $inner_join_prop .= ' INNER JOIN ' . $bims_mview->getMView() . ' GA on GA.stock_id = TT.stock_id ';
    }
    if ($markerprop) {
      $bims_mview = new BIMS_MVIEW_GENOTYPE_MARKER(array('node_id' => $program_id));
      $inner_join_prop .= ' INNER JOIN ' . $bims_mview->getMView() . ' GM on GM.feature_id = TT.feature_id ';
    }

    // Gets the genotyping data.
    $sql = "
      SELECT DISTINCT $select_tt_str
      FROM $mview TT
        $inner_join_prop
        INNER JOIN (
          SELECT $select_str
          FROM $mview TS
            $join_str
            $group_by_str
        )
        $join_cond_str
      $order_by_str
    ";

    // Writes the data to the file.
    self::_writeData($filepath, $sql, $f_args, $fixed_cols, $dynamic_cols, $bims_file);

    // Updates the status.
    if ($bims_file) {
      $bims_file->setFilesize(filesize($filepath));
      $bims_file->updatePropByKey('status', 'completed');
    }

    // Returns the results.
    if ($save_file) {
      return '';
    }
    return file_get_contents($filepath);
  }

  /**
   * Helper : Writes the datat to the file.
   *
   * @param string $filepath
   * @param string $sql
   * @param array $args
   * @param array $fixed_cols
   * @param array $dynamic_cols
   * @param BIMS_FILE $bims_file
   * @param string $separator
   * @param string $meta_info
   * @param boolean $header_flag
   */
  private static function _writeData($filepath, $sql, $args = array(), $fixed_cols, $dynamic_cols, $bims_file = NULL, $separator = 'csv', $meta_info = '', $header_flag = TRUE) {

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return '';
    }

    // Adds the meta information.
    if ($meta_info) {
      fputs($fdw, $meta_info);
    }

    // Adds the headers.
    if ($header_flag) {
      $header_row = array();
      foreach ($fixed_cols as $field => $label) {
        $header_row []= $label;
      }
      foreach ($dynamic_cols as $field => $label) {
        $header_row []= $label;
      }
      if ($separator == 'tab') {
        fputs($fdw, implode("\t", $header_row) . "\n");
      }
      else {
        fputcsv($fdw, $header_row);
      }
    }

    // Gets the data.
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {

      // Adds the fixed columns.
      $row = array();
      foreach ($fixed_cols as $field => $label) {
        $row []= $obj->{$field};
      }

      // Adds genotype data.
      if (!$obj->genotype_data) {
        continue;
      }

      // Parses out genotype.
      $genotype_arr = array();
      $genotype_data = explode(',', $obj->genotype_data);
      foreach ($genotype_data as $genotype_datum) {
        $tmp = explode(':', $genotype_datum);
        $genotype_arr[$tmp[0]] = $tmp[1];
      }

      // Adds the dynamic columns.
      foreach ($dynamic_cols as $field => $label) {
        if (array_key_exists($field, $genotype_arr)) {
         $row []= $genotype_arr[$field];
        }
        else {
          $row []= '';
        }
      }

      // Adds the data.
      if ($separator == 'tab') {
        fputs($fdw, implode("\t", $row) . "\n");
      }
      else {
        fputcsv($fdw, $row);
      }
    }
    fclose($fdw);
  }

  /**
   * Returns the genotypic data in a VCF format.
   *
   * @param array $param
   *
   * @return string filepath
   */
  private static function getGenotypeDataVCF($param = array()) {

    // Gets the parameters.
    $filepath           = $param['filepath'];
    $filter             = $param['filter'];
    $bims_program       = $param['bims_program'];
    $mview_phenotype    = $param['mview_phenotype'];
    $mview_genotype     = $param['mview_genotype'];
    $save_file          = $param['save_file'];
    $bims_file          = $param['bims_file'];

    // Local varibale.
    $program_id     = $bims_program->getProgramID();
    $mview          = $mview_genotype->getMView();
    $fixed_cols     = array();
    $dynamic_cols   = array();
    $f_sql          = $filter['sql'];
    $f_args         = $filter['args'];

    // Adds Meta-information.
    $date_str = date("Ymd");
    $meta_info  = "##fileformat=VCFv4.0\n";
    $meta_info .= "##fileDate=$date_str\n";

    // Adds the fixed headers.
    $headers = array(
      'chrom'   => 'CHROM',
      'pos'     => 'POS',
      'id'      => 'ID',
    );
    if (FALSE) {
      $headers [] = 'REF';
      $headers [] = 'ALT';
      $headers [] = 'QUAL';
      $headers [] = 'FILTER';
      $headers [] = 'INFO';
    }

      // Sets the join statement.
    $join_str = '';
    if ($filter['type'] == 'SEARCH_PHENOTYPE') {
      $join_str = "INNER JOIN ($f_sql) TJ ON TJ.stock_id = TS.stock_id";
    }
    else if ($filter['type'] == 'SEARCH_GENOTYPE_ACCESSION') {
      $join_str = "INNER JOIN ($f_sql) TJ ON TJ.stock_id = TS.stock_id";
    }
    else if ($filter['type'] == 'SEARCH_GENOTYPE_MARKER') {
      $join_str = "INNER JOIN ($f_sql) TJ ON TJ.feature_id = TS.feature_id";
    }

    // Adds the accession columns.
    $sql = "
      SELECT DISTINCT M.stock_id, M.accession
      FROM ($f_sql) M
      ORDER BY M.accession
    ";
    $results = db_query($sql, $f_args);
    while ($obj = $results->fetchObject()) {
      $headers[$obj->stock_id] = $obj->accession;
    }

    // Adds the headers.
    $meta_info .= '#' . implode("\t", array_values($headers)) . "\n";

    // Adds the marker properties.
    $fixed_cols['chromosome'] = 'CHROM';
    $fixed_cols['loc_start']  = 'POS';
    $fixed_cols['marker']     = 'ID';
    $select_str = " GA.chromosome, GA.loc_start, TT.marker AS marker ";
    $flag_ss_id = TRUE;
    foreach ((array)$preference['markerprop'] as $field => $info) {
      if ($info['value']) {
        $select_str .= " , GM.$field ";
        $fixed_cols[$field]= $info['name'];
        $markerprop = TRUE;
      }
    }

    // Adds marker.
    $marker_str = ' TT.marker ';
    if ($flag_ss_id) {
      $marker_str = 'CASE WHEN GA.ss_id IS NOT NULL THEN GA.ss_id ELSE TT.marker END';
    }
    $select_str .= " , $marker_str AS marker ";

    // Gets the mview of BIMS_MVIEW_GENOTYPE_MARKER.
    $bims_mview_ga  = new BIMS_MVIEW_GENOTYPE_MARKER(array('node_id' => $program_id));
    $mview_ga       = $bims_mview_ga->getMView();

    // Adds the genotype properties.
    foreach ((array)$preference['genotypeprop'] as $field => $info) {
      if ($info['value'] && $info['def']) {
        $select_tt_str .= " , TT.$field ";
        $fixed_cols[$field]= $info['name'];
      }
    }

    // Adds the dynamic columns.
    $d_sql = "
      SELECT DISTINCT TS.stock_id, TS.accession FROM $mview TS
      $join_str
    ";
    $results = db_query($d_sql, $f_args);
    while ($obj = $results->fetchObject()) {
      $dynamic_cols[$obj->stock_id] = $obj->accession;
    }

    // Gets the genotyping data.
    $sql = "
      SELECT DISTINCT SS.genotype_data, $select_str
      FROM $mview TT
        INNER JOIN $mview_ga GA on GA.feature_id = TT.feature_id
        INNER JOIN (
          SELECT TS.feature_id, TS.project_id, string_agg(DISTINCT TS.stock_id || ':' || TS.genotype, ',') AS genotype_data
          FROM $mview TS
            $join_str
          GROUP BY TS.marker, TS.feature_id, TS.project_id
        ) SS on SS.feature_id = TT.feature_id AND SS.project_id = TT.project_id
      ORDER BY TT.marker
    ";

    // Writes the data to the file.
    self::_writeData($filepath, $sql, $f_args, $fixed_cols, $dynamic_cols, $bims_file, 'tab', $meta_info, FALSE);

      // Updates the status.
    if ($bims_file) {
      $bims_file->setFilesize(filesize($filepath));
      $bims_file->updatePropByKey('status', 'completed');
    }

    // Returns the results.
    if ($save_file) {
      return '';
    }
    return file_get_contents($filepath);
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {
    return TRUE;
  }
}
