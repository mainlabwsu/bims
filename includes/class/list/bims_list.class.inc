<?php
/**
 * The declaration of BIMS_LIST class.
 *
 */
class BIMS_LIST extends PUBLIC_BIMS_LIST {

  /**
   * Class data members.
   */
  protected static $list_types = array(
    'location'  => 'Location',
    'trial'     => 'Trial',
    'accession' => 'Accession',
    'cross'     => 'Cross',
    'parent'    => 'Parent',
    'trait'     => 'Trait',
  );
  protected static $max_num_filters = 8;
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_LIST::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Return BIMS_LIST by list ID.
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return BIMS_LIST::byKey(array('list_id' => $list_id));
  }

  /**
   * @see PUBLIC_BIMS_LIST::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_LIST::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * @see PUBLIC_BIMS_LIST::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Returns the all list types.
   *
   * @return array
   */
  public static function getListTypes() {
    return self::$list_types;
  }

  /**
   * Returns the instance of BIMS_LIST.
   *
   * @return BIMS_LIST|NULL
   */
  public static function getBIMS_LIST($list_id) {
    $bims_list = BIMS_LIST::byID($list_id);
    if ($bims_list) {
      $class_name = 'BIMS_LIST_' . strtoupper($bims_list->getType());
      if (class_exists($class_name)) {
        return $class_name::byID($list_id);
      }
    }
    return NULL;
  }

  /**
   * Returns the filepath of the downloading file.
   *
   * @param BIMS_USER $bims_user
   * @param string $list_type
   * @param string $file_type
   *
   * @return string
   */
  public static function getDownloadFilepath(BIMS_USER $bims_user, $list_type, $file_type) {

    // Updates the list type.
    if ($list_type == 'sample') {
      $list_type = 'phenotype';
    }

    // Creates the filename and append to the path.
    $filename = sprintf("%d-%s-%s.%s", $bims_user->getProgramID(), $list_type, date("Y-m-d-Gis"), $file_type);
    $download_path = $bims_user->getPath('download', TRUE);
    return "$download_path/$filename";
  }

  /**
   * Returns the options for the lists.
   *
   * @param array $args
   *
   * @return array
   */
  public static function getLists($args) {

    // Gets and sets the variables.
    $type       = array_key_exists('type', $args)       ? $args['type']       : '';
    $user_id    = array_key_exists('user_id', $args)    ? $args['user_id']    : '';
    $program_id = array_key_exists('program_id', $args) ? $args['program_id'] : '';
    $shared     = array_key_exists('shared', $args)     ? $args['shared']     : FALSE;
    $flag       = array_key_exists('flag', $args)       ? $args['flag']       : BIMS_OBJECT;

    // Gets the lists.
    $args = array(
      ':user_id'    => $user_id,
      ':program_id' => $program_id,
    );

    // Adds the conditions.
    $where_str = '';

    // Add user.
    if ($shared) {
      $args[':shared'] = 1;
      $where_str .= 'AND (L.user_id = :user_id OR L.shared = :shared) ';
    }
    else {
      $where_str .= ' AND L.user_id = :user_id ';
    }

    // Add type.
    if ($type) {
      $args[':type'] = $type;
      $where_str .= ' AND LOWER(L.type) = LOWER(:type) ';
    }
    else {

      // Returns all types except 'download' if the type is not specified.
      $args[':type'] = 'download';
      $where_str .= ' AND LOWER(L.type) != :type ';
    }

    // Gets the lists.
    $sql = "
      SELECT L.*,
        CASE
          WHEN L.user_id = :user_id THEN 'Your list' ELSE 'Shared list'
        END AS bywho
      FROM {bims_list} L
      WHERE L.program_id = :program_id $where_str
      ORDER BY bywho DESC, L.type, L.name
    ";
    $results = db_query($sql, $args);
    $lists = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $lists[$obj->list_id] = $obj->name;
      }
      else if ($flag == BIMS_CLASS) {
        $lists[] = BIMS_LIST::byID($obj->list_id);
      }
      else if ($flag == BIMS_OPTION_DEPTH) {

        $lists[$obj->bywho][$obj->list_id] = $obj->type . ' - ' . $obj->name;

      }
      else {
        $lists[] = $obj;
      }
    }
    return $lists;
  }

  /**
   * Returns the options for the filters.
   *
   * @param boolean $option
   *
   * @return array
   */
  public static function getFilters($option = FALSE) {
    $sql = "
      SELECT L.list_id, L.name
      FROM {bims_list} L
      WHERE L.user_id = :user_id
    ";
    $results = db_query($sql, array(':user_id' => $user_id));
    $options = array();
    while ($obj = $results->fetchObject()) {
      $options[$obj->list_id] = $obj->name;
    }
    $options = array(
        'year' => 'year',
        'accession' => 'accession',
        'location' => 'location',
    );
    return $options;
  }

  /**
   * Returns the filter type.
   *
   * @return string
   */
  public function getFilterType() {
    $filter = $this->getPropByKey('filter');
    return $filter['type'];
  }

  /**
   * Returns the filter label.
   *
   * @return string
   */
  public function getFilterLabel() {
    $filter_type = $this->getFilterType();
    if ($filter_type == 'SEARCH_GENOTYPE_MARKER') {
      return 'Search SNP Genotype by Marker Properties';
    }
    else if ($filter_type == 'SEARCH_GENOTYPE_ACCESSION') {
      return 'Search SNP Genotype by Accession Properties';
    }
    else if ($filter_type == 'SEARCH_PHENOTYPE') {
      return 'Search Phenotype';
    }
    return '';
  }

  /**
   * Creates the filter from the form_state and return it.
   *
   * @param array $form_state
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {
    // To be overridden by Child class.
    return array();
  }

  /**
   * Returns the variables for the sorted table of the list in array.
   * - headers
   * - mview
   * - conditions
   *
   * @param BIMS_PROGRAM $bims_program
   * @param array $filters
   *
   * @return array
   */
  public static function getTableVars($bims_program, $filter) {
    // To be overridden by Child class.
    return array();
  }

  /**
   * Summarize the list.
   *
   * @return boolean
   */
  public function summarize() {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Returns the contents of the downloading file.
   *
   * @param various $variable
   * @param array $filter
   * @param array $param
   * @param boolean $save_file
   *
   * @return string
   */
  public static function getFileContents($variable, $filter, $param = array(), $save_file = TRUE) {
    // To be overridden by Child class.
    return '';
  }

  /**
   * Saves the contents to the file.
   *
   * @param integer $file_id
   *
   * @return string
   */
  public static function saveContentToFile($file_id) {

    // Gets BIMS_FILE.
    $bims_file = BIMS_FILE::byID($file_id);
    if (!$bims_file) {
      return "File ID ($file_id) does not exist";
    }

    // Gets the child class.
    $filter = $bims_file->getPropByKey('filter');
    if (empty($filter)) {
      return 'Error : filter not found';
    }
    $list_type = $filter['list_type'];
    $class_name = 'BIMS_LIST_' . strtoupper($list_type);
    if (!class_exists($class_name)) {
      return 'Error : class not found';
    }

    // Sets the param.
    $param = array(
      'file_type' => $bims_file->getPropByKey('file_type'),
      'file_form' => $bims_file->getPropByKey('file_form'),
    );

    // Saves the contents to the file.
    return $class_name::getFileContents($bims_file, $filter, $param, TRUE);
  }

  /**
   * Returns the contents of the downloading file.
   *
   * @param BIMS_USER $bims_user
   * @param array $filter
   * @param array $param
   *
   * @return file object or NULL
   */
  public static function saveContents(BIMS_USER $bims_user, $filter, $param) {

    // Gets the child class.
    $list_type = $filter['list_type'];
    $class_name = 'BIMS_LIST_' . strtoupper($list_type);
    if (class_exists($class_name)) {

      // Gets filename and filepath.
      $filepath = self::getDownloadFilepath($bims_user, $list_type, $param['file_type']);
      $filename = basename($filepath);

      // Creates a field file in temp dir.
      $tmp_file = $bims_user->getPath('tmp') . "/$filename";
      $contents = $class_name::getFileContents($tmp_file, $filter, $param, FALSE);
      return file_save_data($contents, $filepath);
    }
    return NULL;
  }

  /**
   * Creates the downloading file of this list and saved in the user account.
   *
   * @param BIMS_USER $bims_user
   * @param string $file_type
   *
   * @return boolean
   */
  public function download(BIMS_USER $bims_user, $file_type) {

    // Gets the list type.
    $filter = $this->getPropByKey('filter');
    $list_type = $filter['list_type'];

    // Checks if the downloading file has been created.
    $filepath = $this->getPropByKey('filepath');
    if ($filepath && !file_exists($filepath)) {
      return FALSE;
    }
    else if ($filepath == '') {

      // Creates a new file and put the contents to it.
      $filepath = $this->getDownloadFilepath($bims_user, $list_type, $file_type);
      $contents = $this->getFileContentsByType($list_type);
      file_put_contents($filepath, $contents);
    }

    // Save the file for download.
    $details = array(
      'type'        => 'DL',
      'filename'    => basename($filepath),
      'filepath'    => $filepath,
      'filesize'    => filesize($filepath),
      'program_id'  => $this->getProgramID(),
      'user_id'     => $bims_user->getUserID(),
      'submit_date' => date("Y-m-d G:i:s"),
      'description' => ucfirst($list_type) . ' data for downloading',
      'prop'        => json_encode(array()),
    );
    $bims_file = new BIMS_FILE($details);
    if (!$bims_file->insert()) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Return TRUE if the stats for this list has been calculated.
   *
   * @return boolean
   */
  public function hasStats() {
    $stats = $this->getPropByKey('stats');
    return !empty($stats);
  }

  /**
   * Return TRUE if the list type is accession.
   *
   * @return boolean
   */
  public function canStats() {
    return in_array($this->getType(), array('accession', 'sample'));
  }

  /**
   * Computes the stats.
   *
   * @return boolean
   */
  public function computeStats() {

    // Computes the stats.
    if ($this->canStats()) {

      // Gets BIMS_STATS and BIMS_MVIEW_PHENOTYPE.
      $bims_stats = new BIMS_STATS();
      $bims_mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->getProgramID()));

      // Gets the filter.
      $filter = $this->getPropByKey('filter');

      // Calculates the stats for each active descriptor.
      $bims_program = BIMS_PROGRAM::byID($this->getProgramID());
      $params = array(
        'flag'    => BIMS_CLASS,
        'program' => 'program',
      );
      $descriptors  = $bims_program->getActiveDescriptors($params);
      $list_stats   = array();
      $key          = $filter['key'];
      foreach ((array)$descriptors as $descriptor) {

        // Calculates the stats for the descriptor.
        $item_list = implode(',', $filter['items']);
        $param = array(
          'condition' => " $key IN ($item_list)",
          'args'      => array(),
          'from'      => $filter['from'],
          'name'      => $descriptor->getName(),
          'field'     => 't' . $descriptor->getCvtermID(),
          'format'    => $descriptor->getFormat(),
        );
        $stats_arr = $bims_stats->calcStats($param);
        if (!empty($stats_arr) && $stats_arr['num']) {
          $list_stats[$descriptor->getCvtermID()] = $stats_arr;
        }
      }
      $this->setPropByKey('stats', $list_stats);
      $this->setPropByKey('stats_status', 'computed');
      return $this->update();
    }
    return FALSE;
  }

  /**
   * Shares the list.
   *
   *
   * @return boolean
   */
  public function share() {
    $this->setShared(1);
    return $this->update();
  }

  /**
   * Unshares the list.
   *
   * @return boolean
   */
  public function unshare() {
    $this->setShared(0);
    return $this->update();
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the max number of filters.
   */
  public static function getMaxNumFilters() {
    return self::$max_num_filters;
  }

  /**
   * Returns the value of the given key in prop.
   *
   * @return array
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   *
   * $param string $key
   *
   * $return string
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * $param string $key
   * $param string $value
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}