<?php
/**
 * The declaration of BIMS_LIST_SAMPLE class.
 *
 */
class BIMS_LIST_SAMPLE extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Gets values from the form_state.
    $stock_ids = $form_state['stock'];

    // Sets the params.
    $params = array(
      'stock' => $stock_ids,
    );

    // Sets the information.
    $desc       = '';
    $where_stmt = '';
    if (!empty($stock_ids)) {
      $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));
      $tmp = array();
      foreach ($stock_ids as $stock_id) {
        $stock = $mview_stock->getStock($stock_id, BIMS_OBJECT);
        $tmp []= $stock->name;
      }
      $desc = implode(',', $tmp);
      $where_stmt = ' stock_id IN (' . implode(',', $stock_ids) . ')';
    }

    // Creates the filter and return it.
    return array(
      'name'        => 'stock',
      'type'        => 'LIST',
      'list_type'   => 'stock',
      'key'         => 'stock_id',
      'desc'        => $desc,
      'params'      => $params,
      'column'      => array(),
      'where_stmt'  => $where_stmt,
      'args'        => array(),
    );
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars($bims_program, $filter) {

    // Gets the label for accession.
    $accession = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

    // Initializes the headers with accession column only.
    $headers = array(
      array('data' => 'accession', 'field' => 'accession', 'header' => $accession, 'sort' => 'asc'),
    );
    $header_sort = array('accession' => FALSE);

    // Adds extra columns in the preference in the filter.
    $preference = $filter['preference'];
    foreach ((array)$preference['pc_accessionprop'] as $field => $info) {
      if ($info['value']) {
        $header_sort[$field] = FALSE;
        $headers []= array('data' => $field, 'field' => $field, 'header' => $info['name'], 'sort' => 'asc');
      }
    }
    foreach ((array)$preference['sampleprop'] as $field => $info) {
      if ($info['value']) {
        $header_sort[$field] = FALSE;
        $headers []= array('data' => $field, 'field' => $field, 'header' => $info['name'], 'sort' => 'asc');
      }
    }
    foreach ((array)$preference['descriptor'] as $field => $info) {
      if ($info['value']) {
        $header_sort[$field] = array_key_exists('isNumeric', $info) ? $info['isNumeric'] : FALSE;
        $headers []= array('data' => $field, 'field' => $field, 'header' => $info['name'], 'sort' => 'asc');
      }
    }

    // Sets the mview.
    $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $bims_program->getNodeID()));
    $mview = $mview_phenotype->getMView();

    // Returns the table variables.
    return array('headers' => $headers, 'header_sort' => $header_sort, 'mview' => $mview);
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Gets the contents.
    if ($param['file_form'] == 'wide') {
      return self::getPhenotypeDataWide($filepath, $filter, $param);
    }
    else if ($param['file_form'] == 'long') {
      return self::getPhenotypeDataLong($filepath, $filter, $param);
    }
    return '';
  }

  /**
   * Returns the phenotypic data in a long form
   *
   * @param string $filepath
   * @param array $filter
   * @param array $param
   *
   * @return string
   */
  private static function getPhenotypeDataLong($filepath, $filter, $param = array()) {

    // Gets the program ID.
    $program_id = $filter['program_id'];

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return '';
    }

    // Gets BIMS_MVIEW_PHENOTYPE.
    $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    if (!$mview_phenotype->exists()) {
      return '';
    }
    $mview = $mview_phenotype->getMView();

    // Gets FB req columns.
    $bims_program = BIMS_PROGRAM::byID($program_id);
    $fb_cols = $bims_program->getBIMSCols();

    // Sets the headers.
    $preference = $filter['preference'];
    $headers    = array($fb_cols['accession']);
    $prop_arr   = array('accession');
    foreach ((array)$preference['pc_accessionprop'] as $field => $info) {
      if ($info['value']) {
        $headers []= $info['name'];
        $prop_arr []= $field;
      }
    }
    foreach ((array)$preference['sampleprop'] as $field => $info) {
      if ($info['value']) {
        if ($field == 'unique_id') {
          $headers []= $fb_cols['unique_id'];
        }
        else if ($field == 'primary_order'){
          $headers []= $fb_cols['primary_order'];
        }
        else if ($field == 'secondary_order'){
          $headers []= $fb_cols['secondary_order'];
        }
        else {
          $headers []= $info['name'];
        }
        $prop_arr []= $field;
      }
    }
    $headers []= 'trait';
    $headers []= 'value';

    // Gets the selected traits.
    $trait_arr = array();
    foreach ((array)$preference['descriptor'] as $field => $info) {
      if ($info['value']) {
        $trait_arr[$field]= $info;
      }
    }

    // Gets the filter variables.
    $type = $filter['type'];
    $key  = $filter['key'];
    $sql  = $filter['sql'];
    $args = $filter['args'];

    // Updates the properties of the filter.
    $condition = '';
    if ($type == 'SEARCH_GENOTYPE_MARKER') {

      // Gets BIMS_MVIEW_GENOTYPE.
      $mview_genotype = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
      if (!$mview_genotype->exists()) {
        $mview_g = $mview_genotype->getMView();
        $sql = "
          SELECT DISTINCT stock_id
          FROM $mview_g
          WHERE $key IN (SELECT DISTINCT $key FROM ($sql) LL
        ";
      }
      $key = 'stock_id';
    }

    // Adds the stats rows.
    if ($param['stats']) {
      $stats_param = array(
        'condition' => $condition,
        'args'      => $args,
        'from'      => $mview,
      );
      self::addStatsRowsLong($fdw, $stats_param, $prop_arr, $preference['descriptor']);
      fputcsv($fdw, array());
    }

    // Adds the headers.
    fputcsv($fdw, $headers);

    // Populates the data rows.
    $sql_final = "
      SELECT MV.*
      FROM $mview MV
      WHERE MV.$key IN (SELECT $key FROM ($sql) L )
      ORDER BY MV.accession
    ";
    $results = db_query($sql_final, $args);
    while ($obj = $results->fetchObject()) {

      // Adds the properties.
      $row_prop = array();
      foreach ($prop_arr as $field) {
        $row_prop []= $obj->{$field};
      }

      // Adds traits with data.
      foreach ((array)$trait_arr as $field => $info) {
        $value = $obj->{$field};
        if ($value != '') {
          fputcsv($fdw, array_merge($row_prop , array($info['name'], $value)));
        }
      }
    }
    fclose($fdw);
    return file_get_contents($filepath);
  }

  /**
   * Adds the stats rows at the top for a long form.
   *
   * @param file descriptor $fdw
   * @param array $param
   * @param array $prop_arr
   * @param array $descriptors
   *
   * @return string
   */
  private static function addStatsRowsLong($fdw, $param, $prop_arr, $descriptors) {

    // Gets all stats.
    $bims_stats = new BIMS_STATS();

    // Calculates the stats for the descriptors.
    $desc_stats = array();
    foreach ((array)$descriptors as $field => $info) {
      if ($info['value']) {
        $cvterm_id = substr($field, 1);
        $descriptor = MCL_CHADO_CVTERM::byID($cvterm_id);
        if ($descriptor) {
          $param['name']      = $descriptor->getName();
          $param['field']     = $field;
          $param['format']    = $descriptor->getFormat();
          $desc_stats[$field] = $bims_stats->calcStats($param);
        }
      }
    }

    // Calculates the stats for the properties.
    foreach ((array)$prop_arr as $field) {
      if (preg_match("/^p(\d+)$/", $field, $matches)) {
        $cvterm_id = substr($field, 1);
        $descriptor = MCL_CHADO_CVTERM::byID($cvterm_id);
        if ($descriptor) {
          $param['name']      = $descriptor->getName();
          $param['field']     = $field;
          $param['format']    = $descriptor->getFormat();
          $desc_stats[$field] = $bims_stats->calcStats($param);
        }
      }
    }

    // Returns if no stats.
    if (empty($desc_stats)) {
      return;
    }

    // Adds the stats header row.
    $headers = array('Trait/Property', '# Data', 'Minimum', 'Maximum', 'Mean', 'STD', 'Frequency(value:freq)');
    fputcsv($fdw, $headers);

    // Populates the stats rows.
    foreach ((array)$desc_stats as $field => $info) {
      $num  = ($desc_stats[$field]['num'])                  ? $desc_stats[$field]['num']  : '0';
      $min  = array_key_exists('min', $desc_stats[$field])  ? $desc_stats[$field]['min']  : '';
      $max  = array_key_exists('max', $desc_stats[$field])  ? $desc_stats[$field]['max']  : '';
      $mean = array_key_exists('mean', $desc_stats[$field]) ? $desc_stats[$field]['mean'] : '';
      $std  = array_key_exists('std', $desc_stats[$field])  ? $desc_stats[$field]['std']  : '';
      $freq = array_key_exists('freq', $desc_stats[$field]) ? $desc_stats[$field]['freq'] : '';
      $true = array_key_exists('true', $desc_stats[$field]) ? $desc_stats[$field]['true'] : '';

      // Parses out the frequency.
      if ($freq) {
        $freq_arr = array();
        arsort($freq);
        foreach ((array)$freq as $key => $val) {
          $freq_arr []= "$key:$val";
        }
        $freq = implode(';', $freq_arr);
      }
      else if ($true) {
        $num_true   = $desc_stats[$field]['true'];
        $num_false  = $desc_stats[$field]['false'];
        $freq       = "TRUE:$num_true;FALSE:$num_false";
      }

      // Adds the stats row.
      $row = array($info['name'], $num, $min, $max, $mean, $std, $freq);
      fputcsv($fdw, $row);
    }
  }

  /**
   * Returns the phenotypic data in a wide form.
   *
   * @param string $filepath
   * @param array $filter
   * @param array $param
   *
   * @return string filepath
   */
  private static function getPhenotypeDataWide($filepath, $filter, $param = array()) {

    // Gets the program ID.
    $program_id = $filter['program_id'];

    // Opens the file.
    if (!($fdw = fopen($filepath, 'w'))) {
      return '';
    }

    // Gets BIMS_MVIEW_PHENOTYPE.
    $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    if (!$mview_phenotype->exists()) {
      return '';
    }
    $mview_p = $mview_phenotype->getMView();

    // Gets FB req columns.
    $bims_program = BIMS_PROGRAM::byID($program_id);
    $fb_cols = $bims_program->getBIMSCols();

    // Sets the headers.
    $preference = $filter['preference'];
    $headers    = array($fb_cols['accession']);
    $prop_arr   = array('accession');
    foreach ((array)$preference['pc_accessionprop'] as $field => $info) {
      if ($info['value']) {
        $headers []= $info['name'];
        $prop_arr []= $field;
      }
    }
    foreach ((array)$preference['sampleprop'] as $field => $info) {
      if ($info['value']) {
        if ($field == 'unique_id') {
          $headers []= $fb_cols['unique_id'];
        }
        else if ($field == 'primary_order'){
          $headers []= $fb_cols['primary_order'];
        }
        else if ($field == 'secondary_order'){
          $headers []= $fb_cols['secondary_order'];
        }
        else {
          $headers []= $info['name'];
        }
        $prop_arr []= $field;
      }
    }
    $no_descriptor = TRUE;
    foreach ((array)$preference['descriptor'] as $field => $info) {
      if ($info['value']) {
        $headers []= $info['name'];
        $prop_arr []= $field;
        $no_descriptor = FALSE;
      }
    }

    // Gets the filter variables.
    $type = $filter['type'];
    $key  = $filter['key'];
    $sql  = $filter['sql'];
    $args = $filter['args'];

    // Updates the properties of the filter.
    $condition = '';
    if ($type == 'SEARCH_GENOTYPE_MARKER') {

      // Gets BIMS_MVIEW_GENOTYPE.
      $mview_genotype = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
      if ($mview_genotype->exists()) {
        $mview_g = $mview_genotype->getMView();
        $sql = "
          SELECT DISTINCT stock_id
          FROM $mview_g
          WHERE $key IN (SELECT DISTINCT $key FROM ($sql) LL)
        ";
      }
      $key = 'stock_id';
    }
    else if ($type == 'SEARCH_GENOTYPE_ACCESSION') {

      // Gets BIMS_MVIEW_GENOTYPE_ACCESSION.
      $mview_genotype = new BIMS_MVIEW_GENOTYPE_ACCESSION(array('node_id' => $program_id));
      if ($mview_genotype->exists()) {
        $sql = "SELECT DISTINCT LL.stock_id FROM ($sql) LL";
      }
      $key = 'stock_id';
      $condition = " stock_id IN ($sql) ";
    }

    // Adds the stats rows.
    if ($param['stats']) {
      $stats_param = array(
        'sql'       => $sql,
        'from'      => $mview_p,
        'args'      => $args,
        'condition' => $condition,
      );
      self::addStatsRowsWide($fdw, $stats_param, $prop_arr, $preference['descriptor']);
      fputcsv($fdw, array());
    }

    // Adds the headers.
    fputcsv($fdw, $headers);

    // Populates the data rows.
    $sql_final = "
      SELECT MV.*
      FROM $mview_p MV
      WHERE MV.$key IN (SELECT L.$key FROM ($sql) L )
      ORDER BY MV.accession
    ";
    $results = db_query($sql_final, $args);

    // Remove duplicated row if no phenotype data are in the headers.
    if ($no_descriptor) {
      $dup_rows = array();
      while ($obj = $results->fetchObject()) {
        $row = array();
        foreach ($prop_arr as $field) {
          $row []= $obj->{$field};
        }
        $dup_key = implode('', $row);
        if (array_key_exists($dup_key, $dup_rows)) {
          continue;
        }
        $dup_rows[$dup_key] = TRUE;
        fputcsv($fdw, $row);
      }
    }
    else {
      while ($obj = $results->fetchObject()) {
        $row = array();
        foreach ($prop_arr as $field) {
          $row []= $obj->{$field};
        }
        fputcsv($fdw, $row);
      }
    }
    fclose($fdw);
    return file_get_contents($filepath);
  }

  /**
   * Adds the stats rows at the top for a wide form.
   *
   * @param file descriptor $fdw
   * @param array $param
   * @param array $prop_arr
   * @param array $descriptors
   *
   * @return string
   */
  private static function addStatsRowsWide($fdw, $param, $prop_arr, $descriptors) {

    // Gets all stats.
    $bims_stats = new BIMS_STATS();

    // Calculates the stats for the descriptors.
    $desc_stats = array();
    foreach ((array)$descriptors as $field => $info) {
      if ($info['value']) {
        $cvterm_id = substr($field, 1);
        $descriptor = MCL_CHADO_CVTERM::byID($cvterm_id);
        if ($descriptor) {
          $param['name']      = $descriptor->getName();
          $param['field']     = $field;
          $param['format']    = $descriptor->getFormat();
          $desc_stats[$field] = $bims_stats->calcStats($param);
        }
      }
    }

    // Calculates the stats for the properties.
    foreach ((array)$prop_arr as $field) {
      if (preg_match("/^p(\d+)$/", $field, $matches)) {
        $cvterm_id = substr($field, 1);
        $descriptor = MCL_CHADO_CVTERM::byID($cvterm_id);
        if ($descriptor) {
          $param['name']      = $descriptor->getName();
          $param['field']     = $field;
          $param['format']    = $descriptor->getFormat();
          $desc_stats[$field] = $bims_stats->calcStats($param);
        }
      }
    }

    // Returns if no stats.
    if (empty($desc_stats)) {
      return;
    }

    // Initializes the stats rows.
    $stats_rows = array(
      array('# Data'),
      array('Maximum'),
      array('Minimum'),
      array('Mean'),
      array('STD'),
      array('Frequency'),
    );

    // Populates the stats rows.
    $idx = 0;
    foreach ($prop_arr as $field) {
      if (array_key_exists($field, $desc_stats)) {
        $stats_rows[0][$idx] = $desc_stats[$field]['num'];
        if (array_key_exists('max', $desc_stats[$field])) {
          $stats_rows[1][$idx] = $desc_stats[$field]['max'];
        }
        if (array_key_exists('min', $desc_stats[$field])) {
          $stats_rows[2][$idx] = $desc_stats[$field]['min'];
        }
        if (array_key_exists('mean', $desc_stats[$field])) {
          $stats_rows[3][$idx] = $desc_stats[$field]['mean'];
        }
        if (array_key_exists('std', $desc_stats[$field])) {
          $stats_rows[4][$idx] = $desc_stats[$field]['std'];
        }
        if (array_key_exists('freq', $desc_stats[$field])) {
          $freq = $desc_stats[$field]['freq'];
          $freq_arr = array();
          arsort($freq);
          foreach ((array)$freq as $key => $val) {
            $freq_arr []= "$key:$val";
          }
          $stats_rows[5][$idx] = implode(';', $freq_arr);
        }
        if (array_key_exists('true', $desc_stats[$field])) {
          $num_true   = $desc_stats[$field]['true'];
          $num_false  = $desc_stats[$field]['false'];
          $stats_rows[5][$idx] = "TRUE:$num_true;FALSE:$num_false";
        }
      }
      $idx++;
    }

    // Adds the stats rows.
    fputcsv($fdw, array());
    $num_cols = sizeof($prop_arr);
    for ($i = 0; $i < 6; $i++) {
      $row = array();
      for ($j = 0; $j < $num_cols; $j++) {
        if (isset($stats_rows[$i][$j])) {
          $row []= $stats_rows[$i][$j];
        }
        else {
          $row []= '';
        }
      }
      fputcsv($fdw, $row);
    }
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {
    return TRUE;
  }
}
