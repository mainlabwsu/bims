<?php
/**
 * The declaration of BIMS_LIST_LOCATION class.
 *
 */
class BIMS_LIST_LOCATION extends BIMS_LIST {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_LIST::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_LIST::byID()
   */
  public static function byID($list_id) {
    if (!bims_is_int($list_id)) {
      return NULL;
    }
    return self::byKey(array('list_id' => $list_id));
  }

  /**
   * @see BIMS_LIST::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_LIST::getFilterByForm()
   */
  public static function getFilterByForm($form_state, BIMS_PROGRAM $bims_program) {

    // Gets values from the form_state.
    $nd_geolocation_ids = $form_state['location'];

    // Sets the params.
    $params = array(
      'location' => $nd_geolocation_ids,
    );

    // Sets the information.
    $desc       = '';
    $where_stmt = '';
      if (!empty($nd_geolocation_ids)) {
      $mview_location = new BIMS_MVIEW_LOCATION(array('node_id' => $bims_program->getProgramID()));
      foreach ($nd_geolocation_ids as $nd_geolocation_id) {
        $loc = $mview_location->getLocation($nd_geolocation_id);
        $tmp []= $loc->name;
      }
      $desc = implode(',', $tmp);
      $where_stmt = ' nd_geolocation_id IN (' . implode(',', $nd_geolocation_ids) . ')';
    }

    // Creates the filter and return it.
    return array(
      'name'        => 'location',
      'type'        => 'LIST',
      'list_type'   => 'location',
      'key'         => 'nd_geolocation_id',
      'desc'        => $desc,
      'params'      => $params,
      'column'      => array(),
      'where_stmt'  => $where_stmt,
      'args'        => array(),
    );
  }

  /**
   * @see BIMS_LIST::getTableVars()
   */
  public static function getTableVars($bims_program, $filter) {

    // Sets the headers.
    $headers = array(
      array('data' => 'name', 'field' => 'name', 'header' => 'Location', 'sort' => 'asc'),
      array('data' => 'state', 'field' => 'state', 'header' => 'State'),
      array('data' => 'region', 'field' => 'region', 'header' => 'Region'),
      array('data' => 'country', 'field' => 'country', 'header' => 'Country'),
    );

    // Sets the mview.
    $mview_loc = new BIMS_MVIEW_LOCATION(array('node_id' => $bims_program->getNodeID()));
    $mview = $mview_loc->getMView();

    // Sets the conditions.
    $conditions = array(array(
      'key'   => 'nd_geolocation_id',
      'value' => $filter['items'],
      'op'    => 'IN',
    ));

    // Returns the table variables.
    return array('headers' => $headers, 'mview' => $mview, 'conditions' => $conditions);
  }

  /**
   * @see BIMS_LIST::summarize()
   */
  public function summarize() {


    return TRUE;
  }

  /**
   * @see BIMS_LIST::getFileContents()
   */
  public static function getFileContents($filepath, $filter, $param = array(), $save_file = TRUE) {

    // Gets the program ID.
    $program_id = $filter['program_id'];

    // Gets BIMS_MVIEW_LOCATION.
    $mview_location = new BIMS_MVIEW_LOCATION(array('node_id' => $program_id));
    $mview = $mview_location->getMView();

    // Adds the headers.
    $contents = '"Name","Long Name","Type","Latitude","Longitude","Altitude","Country","State","Region","Address"' . "\n";

    // Gets all the locations specified in the filter.
    $nd_geolocation_ids = implode(',', $filter['items']);
    $sql = "
      SELECT MV.*
      FROM $mview MV
      WHERE MV.nd_geolocation_id IN ($nd_geolocation_ids)
      ORDER BY MV.name
    ";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $row = array($obj->name, $obj->long_name, $obj->type, $obj->latitude, $obj->longitude, $obj->altitude, $obj->country, $obj->state, $obj->region, $obj->address);
      $contents .= '"' . implode('","', str_replace('"', '\"', $row)) . "\"\n";
    }
    return $contents;
  }

  /*
   * Defines getters and setters below.
   */
}