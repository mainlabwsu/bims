<?php
/**
 * The declaration of BIMS_CLASS_LOADER class.
 *
 */
class BIMS_CLASS_LOADER {

  /**
   *  Class data members.
   *
   */
  private $class_path_bims  = NULL;
  private $class_path_mcl   = NULL;

  /**
   * Implements the class constructor.
   */
  public function __construct() {
    $this->class_path_bims  = drupal_get_path('module', 'bims') . "/includes/class";
    $this->class_path_mcl   = drupal_get_path('module', 'mcl') . "/includes/class";
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Register autoloader.
   *
   * @param string $class_name
   *
   * @return boolean
   */
  public function register() {
    spl_autoload_register(array($this, 'loadClass'));
  }

  /**
   * Loads a class file.
   *
   * @param string $class_name
   *
   * @return boolean
   */
  public function loadClass($class_name) {

    // Sets the default class path.
    $class_path = $this->class_path_bims;

    // Gets the path to the class file.
    $path = '/';
    if ($class_name == 'PUBLIC_USERS') {
      $path .= 'public_table_class/';
      $class_path = $this->class_path_mcl;
    }
    else if (preg_match("/^(MCL_USER)$/", $class_name, $matches)) {
      $path .= '';
      $class_path = $this->class_path_mcl;
    }
    else if (preg_match("/^(MCL_SITE_VAR|MCL_DUMMY_VAR)$/", $class_name, $matches)) {
      $path .= 'variables/';
      $class_path = $this->class_path_mcl;
    }
    else if (preg_match("/^MCL_CHADO_/", $class_name, $matches)) {
      $path .= 'mcl_chado_class/';
      $class_path = $this->class_path_mcl;
    }
    else if (preg_match("/^MCL_CUSTOM_/", $class_name, $matches)) {
      $path .= 'mcl_custom_class/';
      $class_path = $this->class_path_mcl;
    }
    else if (preg_match("/^CHADO_/", $class_name, $matches)) {
      $path .= 'chado_table_class/';
      $class_path = $this->class_path_mcl;
    }
    else if (preg_match("/^PUBLIC_MCL_/", $class_name, $matches)) {
      $path .= 'public_table_class/';
      $class_path = $this->class_path_mcl;
    }
    else if (preg_match("/^PUBLIC_BIMS_/", $class_name, $matches)) {
      $path .= 'public_table_class/';
    }
    else if (preg_match("/^BIMS_CHADO_/", $class_name, $matches)) {
      $path .= 'bims_chado_class/';
    }
    else if (preg_match("/^BIMS_ARCHIVE/", $class_name, $matches)) {
      $path .= 'archive/';
    }
    else if (preg_match("/^BIMS_FILTER/", $class_name, $matches)) {
      $path .= 'filter/';
    }
    else if (preg_match("/^BIMS_EXCEL/", $class_name, $matches)) {
      $path .= 'excel/';
    }
    else if (preg_match("/^BIMS_FILE/", $class_name, $matches)) {
      $path .= 'file/';
    }
    else if (preg_match("/^BIMS_FIELD_BOOK/", $class_name, $matches)) {
      $path .= 'field_book/';
    }
    else if (preg_match("/^(BIMS_PANEL|BIMS_MESSAGE|BIMS_INSTRUCTION)$/", $class_name, $matches)) {
      $path .= 'panel/';
    }
    else if (preg_match("/^BIMS_LIST/", $class_name, $matches)) {
      $path .= 'list/';
    }
    else if (preg_match("/^BIMS_IMPORTED/", $class_name, $matches)) {
      $path .= 'imported/';
    }
    else if (preg_match("/^BIMS_MVIEW/", $class_name, $matches)) {
      $path .= 'mview/';
    }
    else if (preg_match("/^BIMS_RESOURCE/", $class_name, $matches)) {
      $path .= 'resource/';
    }
    else if (preg_match("/^BIMS_(NODE|PROGRAM|TRIAL)$/", $class_name, $matches)) {
      $path .= 'node/';
    }
    else if (preg_match("/^BIMS_(TOOL|JOB|BDATA|ENVIRONMENT)/", $class_name, $matches)) {
      $path .= 'tool/';
    }
    else if (preg_match("/^BIMS_([A-Z\_]+)/", $class_name, $matches)) {
      // Does nothing. The file should be in the root of class directory.
    }
    else {
      return FALSE;
    }

    // Includes the file.
    $class_filepath = $class_path . $path . strtolower($class_name) . '.class.inc';
    if (file_exists($class_filepath)) {
      require_once($class_filepath);
      return TRUE;
    }
    return FALSE;
  }
}