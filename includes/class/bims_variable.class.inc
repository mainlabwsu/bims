<?php
/**
 * The declaration of PUBLIC_BIMS_VARIABLE class.
 *
 */
class BIMS_VARIABLE extends PUBLIC_BIMS_VARIABLE {

  /**
   * Class data members.
   */
  private $val = NULL;

  /**
   * @see PUBLIC_BIMS_VARIABLE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates $this.val.
    if ($this->value == '') {
      $this->val = '';
    }
    else {
      $this->val = json_decode($this->value, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_VARIABLE::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_VARIABLE by name.
   *
   * @param string $name
   *
   * @return BIMS_VARIABLE|NULL
   */
  public static function byName($name) {
    return self::byKey(array('name' => $name));
  }

  /**
   * @see PUBLIC_BIMS_VARIABLE::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_VARIABLE::insert()
   */
  public function insert() {

    // Serializes the value.
    $this->value = json_encode($this->val);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_VARIABLE::update()
   */
  public function update($new_values = array()) {

    // Serializes the value.
    $this->value = json_encode($this->val);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Returns the vaule of the variable.
   *
   * @param string $name
   * @param string $default_value
   *
   * @return string
   */
  public static function getVariable($name, $default_value = '') {

    // Gets the varialbe.
    $bims_variable = self::byName($name);
    if ($bims_variable) {
      return $bims_variable->getVal();
    }
    return $default_value;
  }

  /**
   * Sets the value to the variable.
   *
   * @param string $name
   * @param string $value
   * @param integer $expire
   *
   * @return boolean
   */
  public static function setVariable($name, $value, $expire = 0) {

    // Gets the varialbe.
    $bims_variable = BIMS_VARIABLE::byName($name);

    // Updates the value.
    if ($bims_variable) {
      $bims_variable->setVal($value);
      return $bims_variable->update();
    }

    // Adds a new variable.
    $details = array(
      'name'        => $name,
      'value'       => json_encode($value),
      'expire'      => $expire,
      'create_date' => date("Y-m-d G:i:s"),
    );
    $bims_variable = new PUBLIC_BIMS_VARIABLE($details);
    return $bims_variable->insert();
  }

  /**
   * Removes the variable.
   *
   * @return boolean
   */
  public function delVariable($name) {
    $bims_variable = BIMS_VARIABLE::byName($name);
    if ($bims_variable) {
      return $bims_variable->delete();
    }
    return TRUE;
  }

  /**
   * Returns if the variable is expired.
   *
   * @return boolean
   */
  public function isExpired($name) {


    return FALSE;
  }

  /**
   * Cleans up bims_variable table.
   *
   * @return boolean
   */
  public function cleanup($name) {

    // Checks for the expire.
    $sql = "SELECT name FROM {bims_variable}";
    $results = db_query($sql);
    while ($name = $results->fetchField()) {
      $bims_variable = BIMS_VARIABLE::byName($name);
      if ($bims_variable && $bims_variable->isExpired()) {
        $bims_variable->delete();
      }
    }
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the val.
   *
   * @retrun various
   */
  public function getVal() {
    return $this->val;
  }

  /**
   * Sets the val.
   *
   * @param various $val
   */
  public function setVal($val) {
    $this->val = $val;
  }
}