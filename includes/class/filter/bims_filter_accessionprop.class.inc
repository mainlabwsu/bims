<?php
/**
 * The declaration of BIMS_FILTER_ACCESSIONPROP class.
*
*/
class BIMS_FILTER_ACCESSIONPROP extends BIMS_FILTER {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_FILTER::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_FILTER::getOptions()
   */
  public static function getOptions($filters) {
    $type = $filters[0]['type'];
    if ($type == 'SEARCH_PHENOTYPE') {
      return array(
        'name'      => 'Name',
        'trial'     => 'Trial',
        'location'  => 'Location',
        'cross'     => 'Cross',
        'parent'    => 'Parent',
        'data_year' => 'Data Year',
      );
    }
    else if ($type == 'SEARCH_GENOTYPE_ACCESSION') {
      return array(
        'name'    => 'Name',
        'trial'   => 'Trial',
        'parent'  => 'Parent',
        'cross'   => 'Cross',
      );
    }
    else if ($type == 'SEARCH_CROSS') {
      return array(
        //'trial'     => 'Trial',
        'cross_number'  => 'Cross Number',
        //'cross_group'   => 'Cross Group',
        'parent'        => 'Parent',
        //'data_year' => 'Data Year',
      );
    }
    return array();
  }

  /**
   * @see BIMS_FILTER::getFormFilter()
   */
  public static function getFormFilter(&$form, $details = array()) {

    // Local variables.
    $bims_user      = $details['bims_user'];
    $property       = $details['property'];
    $filters        = $details['filters'];
    $bims_program   = $bims_user->getProgram();
    $accession      = $bims_program->getBIMSLabel('accession');
    $details['max'] = 13;
    $width          = 300;

    // Adds the filters by format.
    $disabled = FALSE;
    if ($property == 'name' || $property == 'cross_number') {
      $options = array(
        'start'       => 'Start with',
        'exaxctly'    => 'Exactly',
        'contain'     => 'Contains',
        'not_start'   => 'Not start with',
        'not_exactly' => 'Not exaxctly',
        'not_contain' => 'Not contains',
      );

      if ($property == 'name') {
        $form['name_cond']= array(
          '#type'         => 'select',
          '#title'        => ucfirst($accession),
          '#options'      => $options,
          '#attributes'   => array('style' => 'width:140px;'),
        );
        $form['name'] = array(
          '#type'         => 'textarea',
          '#description'  => "Please add one $accession per line",
          '#attributes'   => array('style' => 'width:' . $width . 'px;'),
          '#rows'         => 4,
        );
        $form['search_alias']= array(
          '#type'           => 'checkbox',
          '#title'          => 'Check if you want to search Alias as well',
          '#default_value'  => FALSE,
        );
      }
      else if ($property == 'cross_number') {
        $form['cross_number_cond']= array(
          '#type'         => 'select',
          '#title'        => 'Cross Number',
          '#options'      => $options,
          '#attributes'   => array('style' => 'width:140px;'),
        );
        $form['cross_number'] = array(
          '#type'         => 'textarea',
          '#description'  => "Please add one cross number per line",
          '#attributes'   => array('style' => 'width:' . $width . 'px;'),
          '#rows'         => 4,
        );
      }
    }
    else if ($property == 'cross_group') {
      $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $bims_program->getProgramID()));
      $m_cross  = $bm_cross->getMView();
      $details['select']    = "SELECT DISTINCT T.nd_experiment_id AS id, T.cross_number AS label";
      $details['desc']      = 'Please choose cross groups';
      $details['where']     = "T.nd_experiment_id IN (SELECT nd_experiment_id FROM {$m_cross} WHERE num_cross != 0)";
      $details['order_by']  = 'T.cross_number';
      $details['max']       = 10;
      $details['title']     = 'Cross&nbsp;Groups';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($property == 'location') {
      $details['select']    = "SELECT DISTINCT T.nd_geolocation_id AS id, T.site_name AS label";
      $details['desc']      = 'Please choose locations';
      $details['order_by']  = 'T.site_name';
      $details['max']       = 10;
      $details['title']     = 'Locations';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($property == 'data_year') {
      $details['select']    = "SELECT DISTINCT T.data_year AS id, T.data_year AS label";
      $details['desc']      = 'Please choose data years';
      $details['order_by']  = 'T.data_year';
      $details['max']       = 10;
      $details['title']     = 'Data Years';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($property == 'stock') {
      $details['select']    = "SELECT DISTINCT T.stock_id AS id, T.accession AS label";
      $details['desc']      = 'Please choose stocks';
      $details['order_by']  = 'T.accession';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($property == 'trial') {
      $details['select']    = "SELECT DISTINCT T.node_id AS id, T.dataset_name AS label";
      $details['desc']      = 'Please choose trials';
      $details['order_by']  = 'T.dataset_name';
      $details['title']     = 'Trials';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($property == 'parent') {
      $max                  = ($details['max'] / 2) - 2;
      $details['select']    = "SELECT DISTINCT T.stock_id_m AS id, T.maternal AS label";
      $details['max']       = $max;
      $details['property']  = 'maternal';
      $details['title']     = 'Maternal parent';
      $details['desc']      = 'Please choose maternal parents';
      $details['order_by']  = 'T.maternal';
      $m_disabled           = self::_addFormSelectVert($form, $details);
      $form['parent_op'] = array(
        '#type'           => 'radios',
        '#options'        => array('AND' => 'AND', 'OR' => 'OR'),
        '#default_value'  => 'OR',
        '#suffix'         => '<br />',
        '#attributes'     => array('style' => 'width:20px;'),
      );
      $details['select']    = "SELECT DISTINCT T.stock_id_p AS id, T.paternal AS label";
      $details['max']       = $max;
      $details['property']  = 'paternal';
      $details['title']     = 'Paternal parent';
      $details['desc']      = 'Please choose paternal parents';
      $details['order_by']  = 'T.paternal';
      $f_disabled           = self::_addFormSelectVert($form, $details);
      $disabled             = ($m_disabled && $f_disabled);
      if ($disabled) {
        hide($form['parent_op']);
      }
    }
    else if ($property == 'cross') {
      $details['select']    = "SELECT DISTINCT T.nd_experiment_id AS id, T.cross_number AS label";
      $details['desc']      = 'Please choose crosses';
      $details['order_by']  = 'T.cross_number';
      $details['title']     = 'Crosses';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    return $disabled;
  }

  /**
   * @see BIMS_FILTER::checkFormFilter()
   */
  public static function checkFormFilter($form_state, $details = array()) {

    // Local variables.
    $property   = $details['property'];
    $base_path  = $details['base_path'];

    // Checks the values in the form_state.
    if ($property == 'name') {
      $name = trim($form_state['name']);
      if (!$name) {
        form_set_error("$base_path][name", 'Please type accession');
        return;
      }
    }
    else if ($property == 'parent') {
      $maternal = $form_state['maternal'];
      $paternal = $form_state['paternal'];
      if (empty($maternal) && empty($paternal)) {
        form_set_error("$base_path][maternal", "Please choose at least one parent");
        return;
      }
    }
    else if (preg_match("/(location|trial|cross|trait)/", $property)) {
      $selected = $form_state[$property];
      if (empty($selected)) {
        form_set_error("$base_path][$property", "Please choose at least one $property");
        return;
      }
    }
  }

  /**
   * @see BIMS_FILTER::addFormFilter()
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {

    // Local variables.
    $bims_program = $details['bims_program'];
    $property     = $details['property'];
    $program_id   = $bims_program->getProgramID();

    // Copies the preference from the previous one.
    $filter = $filters[sizeof($filters) - 1];
    //self::updatePreference($filter, $cvterm_id);

    $preference = $filters[sizeof($filters) - 1]['preference'];

    // Gets the parameters for filter.
    $ret_values = array();
    if ($property == 'name') {
      $ret_values = self::_propName($form_state, $program_id);
    }
    else if ($property == 'trial') {
      $ret_values = self::_propTrial($form_state);
    }
    else if ($property == 'location') {
      $ret_values = self::_propLocation($form_state, $program_id);
    }
    else if ($property == 'data_year') {
      $ret_values = self::_propDataYear($form_state);
    }
    else if ($property == 'trait') {
      $ret_values = self::_propTrait($form_state, $preference);
    }
    else if ($property == 'cross') {
      $ret_values = self::_propCross($form_state, $program_id);
    }
    else if ($property == 'parent') {
      $ret_values = self::_propParent($form_state, $program_id);
    }
    else if ($property == 'cross_number') {
      $ret_values = self::_propCrossNumber($form_state);
    }
    else if ($property == 'cross_group') {
      $ret_values = self::_propCrossGroup($form_state, $program_id);
    }

    // Updates the filters array.
    if (!empty($ret_values)) {
      $args = array();
      if (array_key_exists('args', $ret_values)) {
        $args = $ret_values['args'];
      }
      $traits = array();
      if (array_key_exists('trait', $ret_values)) {
        $traits = $ret_values['trait'];
      }

      $filter = array(
        'bims_program'  => $bims_program,
        'program_id'    => $program_id,
        'name'          => $property,
        'desc'          => $ret_values['desc'],
        'params'        => $ret_values['params'],
        'from'          => $filter['from'],
        'where_stmt'    => $ret_values['where_stmt'],
        'args'          => $args,
        'traits'        => $traits,
        'preference'    => $preference,
      );
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }

  /**
   * Returns the params for 'Name'.
   *
   * @param array $form_state
   * @param integer $program_id
   *
   * @return array
   */
  private function _propName($form_state, $program_id) {
    $cond         = $form_state['name_cond'];
    $search_alias = $form_state['search_alias'];
    $name         = trim($form_state['name']);

    // Sets the information.
    $tmp = explode("\n", $name);
    $names = array();
    $aliases = array();
    $desc = "Name(s) [$cond] :";
    $stmt = '';
    foreach ($tmp as $n) {
      $n = trim($n);
      $n_escaped = pg_escape_string($n);
      if ($n) {
        $desc .= " $n,";
        $op = '=';
        $t  = $n_escaped;
        if ($cond == 'start') {
          $op = 'LIKE';
          $t = $n_escaped . '%';
        }
        else if ($cond == 'contain') {
          $op = 'LIKE';
          $t = '%' . $n_escaped . '%';
        }
        else if ($cond == 'not_start') {
          $op = 'NOT LIKE';
          $t = $n_escaped . '%';
        }
        else if ($cond == 'not_exactly') {
          $op = '!=';
        }
        else if ($cond == 'not_contain') {
          $op = 'NOT LIKE';
          $t = '%' . $n_escaped . '%';
        }
        $names []= " LOWER(accession) $op LOWER('$t')";
        $aliases [] = " LOWER(alias) $op LOWER('$t')";
      }

      if ($search_alias) {
        $desc = '<em><b>Alias searched</b></em></br>' . trim($desc, ',');
        $where_accession = implode(' OR ', $names);

        // Adds search alias.
        $bm_alias = new BIMS_MVIEW_ALIAS(array('node_id' => $program_id));
        $m_alias  = $bm_alias->getMView();
        $list_alias = implode(' OR ', $aliases);
        $where_alias = "SELECT target_id FROM {$m_alias} WHERE type = 'accession' AND $list_alias";

        // Sets the final WHERE statement.
        $stmt = " ($where_accession) OR (stock_id IN ($where_alias)) ";
      }
      else {
        $desc = trim($desc, ',');
        $stmt = implode(' OR ', $names);
      }
    }

    // Returns the where and description.
    $params = array(
      'name_cond' => $cond,
      'name'      => $name,
    );
    $ret_values = array(
      'params'      => $params,
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }

  /**
   * Returns the params for 'Trait'.
   *
   * @param array $form_state
   * @param array $preference
   *
   * @return array
   */
  private function _propTrait($form_state, &$preference) {
    $cvterm_ids = $form_state['trait'];

    // Sets the information.
    $desc   = '';
    $stmt   = '';
    $traits = array();
    if (!empty($cvterm_ids)) {
      $stmts = array();
      foreach ($cvterm_ids as $cvterm_id) {
        $trait = BIMS_MVIEW_DESCRIPTOR::byKey(array('cvterm_id' => $cvterm_id));
        $stmts []= "  t$cvterm_id != '' ";
        $desc .= ($trait) ? ' ' . $trait->getName() . ',' : " $cvterm_id,";
        $traits[$cvterm_id] = $trait->getName();

        // Sets as default columns for the chosen descriptor.
        $preference['descriptor']["t$cvterm_id"]['value'] = TRUE;
      }
      if (!empty($stmts)) {
        $stmt = implode(' OR ', $stmts);
      }
    }

    // Returns the parameters.
    $ret_values = array(
      'traits'      => $traits,
      'params'      => array('trait' => $cvterm_ids),
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }


  /**
   * Returns the params for 'Trial'.
   *
   * @param array $form_state
   *
   * @return array
   */
  private function _propTrial($form_state) {
    $node_ids = $form_state['trial'];

    // Sets the information.
    $desc = '';
    $stmt = '';
    if (!empty($node_ids)) {

      // Sets the description.
      $desc_arr = array();
      foreach ($node_ids as $node_id) {
        $trial = BIMS_TRIAL::byID($node_id);
        if ($trial) {
          $desc_arr []= $trial->getName();
        }
      }
      $desc = implode(', ', $desc_arr);

      // Sets the condition.
      $stmt = " node_id IN (" . implode(',', $node_ids) . ')';
    }


    // Returns the parameters.
    $ret_values = array(
      'params'      => array('trial' => $node_ids),
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }

  /**
   * Returns the params for 'Cross'.
   *
   * @param array $form_state
   * @param integer $program_id
   *
   * @return array
   */
  private function _propCross($form_state, $program_id) {
    $nd_experiment_ids = $form_state['cross'];

    // Sets the information.
    $desc = '';
    $stmt = '';
    $all_nd_experiment_ids = array();
    if (!empty($nd_experiment_ids)) {

      // Gets BIMS_MVIEW_CROSS.
      $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
      $m_cross  = $bm_cross->getMView();

      // Sets the description.
      $desc_arr = array();
      foreach ($nd_experiment_ids as $nd_experiment_id) {
        $cross = $bm_cross->getCross($nd_experiment_id);
        if ($cross) {
          $desc_arr []= $cross->cross_number;
          $all_nd_experiment_ids []= $nd_experiment_id;

          // If this is cross group, adds all nd_experiment_id of children.
          if ($cross->num_cross) {
            $sql = "
              SELECT nd_experiment_id FROM {$m_cross}
              WHERE cross_group_id = :nd_experiment_id
            ";
            $results = db_query($sql, array(':nd_experiment_id' => $nd_experiment_id));
            while ($ne_id = $results->fetchField()) {
              $all_nd_experiment_ids []= $ne_id;
            }
          }
        }
      }
      $desc = implode(', ', $desc_arr);

      // Sets the condition.
      $stmt = " nd_experiment_id IN (" . implode(',', $all_nd_experiment_ids) . ')';
    }

    // Returns the parameters.
    $ret_values = array(
      'params'      => array('cross' => $nd_experiment_ids),
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }

  /**
   * Returns the params for 'data_year'.
   *
   * @param array $form_state
   *
   * @return array
   */
  private function _propDataYear($form_state) {
    $data_years = $form_state['data_year'];

    // Sets the information.
    $desc = '';
    $stmt = '';
    if (!empty($data_years)) {
      $desc = implode(',', $data_years);
      $stmt = " data_year IN ('" . implode("','", $data_years) . "')";
    }

    // Returns the parameters.
    $ret_values = array(
      'params'      => array('data_year' => $data_years),
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }

  /**
   * Returns the params for 'Location'.
   *
   * @param array $form_state
   * @param integer $program_id
   *
   * @return array
   */
  private function _propLocation($form_state, $program_id) {
    $nd_geolocation_ids = $form_state['location'];
    $params['location'] = $nd_geolocation_ids;

    // Sets the information.
    $desc = '';
    $stmt = '';
    if (!empty($nd_geolocation_ids)) {

      // Gets BIMS_MVIEW_LOCATION.
      $bm_location = new BIMS_MVIEW_LOCATION(array('node_id' => $program_id));

      // Sets the description.
      $desc_arr = array();
      foreach ($nd_geolocation_ids as $nd_geolocation_id) {
        $location = $bm_location->getLocation($nd_geolocation_id);
        if ($location) {
          $desc_arr []= $location->name;
        }
      }
      $desc = implode(', ', $desc_arr);

      // Sets the condition.
      $stmt = " nd_geolocation_id IN (" . implode(',', $nd_geolocation_ids) . ')';
    }

    // Returns the parameters.
    $ret_values = array(
      'params'      => array('location' => $nd_geolocation_ids),
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }

  /**
   * Returns the params for 'Parent'.
   *
   * @param array $form_state
   * @param integer $program_id
   *
   * @return array
   */
  private function _propParent($form_state, $program_id) {
    $stock_ids_m  = $form_state['maternal'];
    $parent_op    = $form_state['parent_op'];
    $stock_ids_p  = $form_state['paternal'];

    // Gets BIMS_MVIEW_STOCK.
    $bm_stock = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));

    // Sets the information.
    $m_cond = $f_cond = $m_desc = $f_desc = '';
    if (!empty($stock_ids_m)) {
      $m_desc = 'Maternal Parents :';
      foreach ($stock_ids_m as $stock_id_m) {
        $stock = $bm_stock->getStock($stock_id_m, BIMS_OBJECT);
        $m_desc .= ($stock) ? ' ' . $stock->name . ',' : " $stock_id_m,";
      }
      $m_desc = trim($m_desc, ',');
      $m_cond = ' stock_id_m IN (' . implode(',', $stock_ids_m) . ')';
    }
    if (!empty($stock_ids_p)) {
      $f_desc = 'Paternal Parents :';
      foreach ($stock_ids_p as $stock_id_p) {
        $stock = $bm_stock->getStock($stock_id_p, BIMS_OBJECT);
        $f_desc .= ($stock) ? ' ' . $stock->name . ',' : " $stock_id_p,";
      }
      $f_desc = trim($f_desc, ',');
      $f_cond = ' stock_id_p IN (' . implode(',', $stock_ids_p) . ')';
    }
    $where_str = $desc = '';
    if ($m_cond && $f_cond) {
      $stmt = "$m_cond $parent_op $f_cond";
      $desc = "$m_desc \\n $f_desc";
    }
    else if ($m_cond) {
      $stmt = $m_cond;
      $desc = $m_desc;
    }
    else if ($f_cond) {
      $stmt = $f_cond;
      $desc = $f_desc;
    }

    // Returns the parameters.
    $params = array(
      'maternal'  => $stock_ids_m,
      'parent_op' => $parent_op,
      'paternal'  => $stock_ids_p,
    );
    $ret_values = array(
      'params'      => $params,
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }

  /**
   * Returns the params for 'Cross Number'.
   *
   * @param array $form_state
   *
   * @return array
   */
  private function _propCrossNumber($form_state) {

    // Gets the parameters.
    $cond = $form_state['cross_number_cond'];
    $cn   = trim($form_state['cross_number']);

    // Sets the information.
    $tmp = explode("\n", $cn);
    $cns = array();
    $desc = "Cross Number(s) [$cond] :";
    foreach ($tmp as $n) {
      $n = trim($n);
      $n_escaped = pg_escape_string($n);
      if ($n) {
        $desc .= " $n,";
        $op = '=';
        $t  = $n_escaped;
        if ($cond == 'start') {
          $op = 'LIKE';
          $t = $n_escaped . '%';
        }
        else if ($cond == 'contain') {
          $op = 'LIKE';
          $t = '%' . $n_escaped . '%';
        }
        else if ($cond == 'not_start') {
          $op = 'NOT LIKE';
          $t = $n_escaped . '%';
        }
        else if ($cond == 'not_exactly') {
          $op = '!=';
        }
        else if ($cond == 'not_contain') {
          $op = 'NOT LIKE';
          $t = '%' . $n_escaped . '%';
        }
        $cns []= " LOWER(cross_number) $op LOWER('$t')";
      }
    }
    $desc = trim($desc, ',');
    $stmt = implode(' OR ', $cns);

    // Returns the parameters.
    $params = array(
      'cross_number_cond' => $cond,
      'cross_number'      => $cn,
    );
    $ret_values = array(
      'params'      => $params,
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }


  /**
   * Returns the params for 'Cross Group'.
   *
   * @param array $form_state
   * @param integer $program_id
   *
   * @return array
   */
  private function _propCrossGroup($form_state, $program_id) {
    $nd_experiment_ids = $form_state['cross_group'];
    $params['cross_group'] = $nd_experiment_ids;

    // Sets the information.
    $desc = '';
    $stmt = '';
    if (!empty($nd_experiment_ids)) {

      // Gets BIMS_MVIEW_CROSS.
      $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));

      // Sets the description.
      $desc_arr = array();
      foreach ($nd_experiment_ids as $nd_experiment_id) {
        $cross = $bm_cross->getCross($nd_experiment_id);
        if ($cross) {
          $desc_arr []= $cross->cross_number;
        }
      }
      $desc = implode(', ', $desc_arr);

      // Sets the condition.
      $stmt = " cross_group_id IN (" . implode(',', $nd_experiment_ids) . ')';
    }

    // Returns the parameters.
    $ret_values = array(
      'params'      => array('cross_group' => $nd_experiment_ids),
      'where_stmt'  => $stmt,
      'desc'        => $desc,
    );
    return $ret_values;
  }

  /**
   * Adds select form element for a vertical filter.
   *
   * @param array $form
   * @param array $details
   *
   * @return boolean
   */
  private static function _addFormSelectVert(&$form, $details) {

    // Local variables.
    $filters    = $details['filters'];
    $property   = $details['property'];
    $select     = $details['select'];
    $max        = $details['max'];
    $where      = array_key_exists('where', $details) ? $details['where'] : '';
    $order_by   = array_key_exists('order_by', $details) ? $details['order_by'] : '';
    $id         = array_key_exists('id', $details) ? $details['id'] : '';
    $title      = array_key_exists('title', $details) ? $details['title'] : '';
    $desc       = array_key_exists('desc', $details) ? $details['desc'] : '';

    // Generates the SQL.
    $last       = end($filters);
    $last_sql   = $last['sql'];
    $last_args  = $last['args'];
    $sql        = "$select FROM ($last_sql) T WHERE 1=1 ";

    if ($where) {
      $sql .= " AND $where ";
    }
    if ($order_by) {
      $order_by = "ORDER BY $order_by";
    }

    // Prints the SQL statement for debugging.
    if ($GLOBALS['BIMS_DEBUG_SEARCH_SQL']) {
      bims_print_sql("$sql $order_by", $last_args, 'filter_phenotype::_addFormSelectVert()');
    }

    // Gets the options for the filter.
    $options = array();
    $results = db_query("$sql $order_by", $last_args);
    while ($obj = $results->fetchObject()) {
      if ($obj->label) {
        $options[$obj->id] = $obj->label;
      }
    }
    if (empty($options)) {
      return TRUE;
    }
    else {
      $attrs =  array('style' => 'width:100%;');
      if ($id) {
        $attrs['id'] = $id;
      }
      $size = (sizeof($options) > $max) ? $max : sizeof($options);
      $form[$property] = array(
        '#type'         => 'select',
        '#options'      => $options,
        '#multiple'     => TRUE,
        '#size'         => $size,
        '#attributes'   => $attrs,
      );
      if ($title) {
        $form[$property]['#title'] = $title;
      }
      if ($desc) {
        $form[$property]['#description'] = $desc;
      }
    }
    return FALSE;
  }
}