<?php
/**
 * The declaration of BIMS_FILTER_GENOTYPEPROP class.
*
*/
class BIMS_FILTER_GENOTYPEPROP extends BIMS_FILTER {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_FILTER::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_FILTER::getOptions()
   */
  public static function getOptions($filters) {
    $filter = end($filters);
    if ($filter) {
      return array(
        'marker'    => 'Marker',
        'genotype'  => 'Genome Location',
      );
    }
    return array();
  }

  /**
   * @see BIMS_FILTER::getFormFilter()
   */
  public static function getFormFilter(&$form, $details = array()) {

    // Local variables.
    $bims_user      = $details['bims_user'];
    $property       = $details['property'];
    $filters        = $details['filters'];
    $analysis_id    = $details['analysis_id'];
    $chromosome_fid = $details['chromosome_fid'];
    $bims_program   = $bims_user->getProgram();
    $accession      = $bims_program->getBIMSLabel('accession');
    $max            = 13;
    $width          = 300;
    $no_data        = '';
    $div_no_data    = '<div style="margin:40px 0px 10px 5px;">';

    // Adds the filters by property.
    $disabled = FALSE;
    if ($property == 'marker') {
      $options = array(
        'start'       => 'Start with',
        'exaxctly'    => 'Exactly',
        'contain'     => 'Contains',
        'not_start'   => 'Not start with',
        'not_exactly' => 'Not exaxctly',
        'not_contain' => 'Not contains',
      );
      $form['name_cond']= array(
        '#type'         => 'select',
        '#title'        => 'Marker',
        '#options'      => $options,
        '#attributes'   => array('style' => 'width:140px;'),
      );
      $form['name'] = array(
        '#type'         => 'textarea',
        '#description'  => "Please add one marker per line",
        '#attributes'   => array('style' => 'width:' . $width . 'px;'),
        '#rows'          => 4,
      );
    }
    if ($property == 'genotype') {
      self::_getFormGenotype($form, $details);
    }
    return $disabled;
  }

  /**
   * Sets the form for gentoype filter.
   *
   * @param array $form
   * @param array $details
   */
  private static function _getFormGenotype(&$form, $details) {

    // Gets the variables.
    $bims_user      = $details['bims_user'];
    $filters        = $details['filters'];
    $analysis_id    = $details['analysis_id'];
    $chromosome_fid = $details['chromosome_fid'];

    // Gets BIMS_MVIEW_MARKER.
    $bm_marker  = new BIMS_MVIEW_MARKER(array('node_id' => $bims_user->getProgramID()));
    $m_marker   = $bm_marker->getMView();

    // Gets the options.
    $last       = end($filters);
    $last_sql   = $last['sql'];
    $last_args  = $last['args'];
    $sql        = "
      SELECT DISTINCT M.analysis_id, M.genome
      FROM ($last_sql) T
        INNER JOIN {$m_marker} M on M.feature_id = T.feature_id
      WHERE M.genome IS NOT NULL
      ORDER BY M.genome
    ";
    $results = db_query($sql, $last_args);
    $options = array();
    while ($obj = $results->fetchObject()) {
      $options[$obj->analysis_id] = $obj->genome;
      if (!$analysis_id) {
        $analysis_id = $obj->analysis_id;
      }
    }

    // Checks the genome selection.
    if (empty($options)) {
      $form['genome']['no_genome'] = array(
        '#markup' => "<div style='padding:10'>No genome is associated with markers in the current program.</div>",
      );
      return FALSE;
    }

    // Add genome selection.
    $form['genome']= array(
      '#type'       => 'select',
      '#title'      => 'Genome',
      '#options'    => $options,
      '#attributes' => array('style' => 'min-width:200px;max-width:380px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_sa_genotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sa-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Gets the chromosomes.
    $sql = "
      SELECT DISTINCT M.chromosome_fid, M.chromosome
      FROM ($last_sql) T
        INNER JOIN {$m_marker} M on M.feature_id = T.feature_id
      WHERE M.analysis_id = :analysis_id
      ORDER BY M.chromosome
    ";
    $last_args[':analysis_id'] = $analysis_id;
    $results = db_query($sql, $last_args);
    $options = array();
    while ($obj = $results->fetchObject()) {
      $options[$obj->chromosome_fid] = $obj->chromosome;
      if (!$chromosome_fid) {
        $chromosome_fid = $obj->chromosome_fid;
      }
    }
    if (empty($options)) {
      $form['genome']['no_chromosome'] = array(
        '#markup' => "<div style='padding:10'>No chromosome is associated with '$genome'.</div>",
      );
      return TRUE;
    }

    // Adds the chromosome selection.
    $form['chromosome']= array(
      '#type'       => 'select',
      '#title'      => t('Chr / Scaffold'),
      '#options'    => $options,
      '#attributes' => array('style' => 'min-width:200px;max-width:380px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_sa_genotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sa-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Gets the start and stop.
    $sql = "
      SELECT MIN(loc_start) AS loc_start, MAX(loc_stop) AS loc_stop
      FROM ($last_sql) T
        INNER JOIN {$m_marker} M on M.feature_id = T.feature_id
      WHERE M.analysis_id = :analysis_id AND M.chromosome_fid = :chromosome_fid
    ";
    $last_args[':analysis_id']    = $analysis_id;
    $last_args[':chromosome_fid'] = $chromosome_fid;
    $obj = db_query($sql, $last_args)->fetchObject();

    // Locations.
    $form['loc_start'] = array(
      '#type'       => 'textfield',
      '#title'      => t('Locations between'),
      '#value'      => $obj->loc_start,
      '#attributes' => array('style' => 'width:100px;'),
    );
    $form['loc_stop'] = array(
      '#type'       => 'textfield',
      '#title'      => '&nbsp;',
      '#value'      => $obj->loc_stop,
      '#attributes' => array('style' => 'width:100px;'),
    );
    $form['#theme'] = 'bims_panel_main_sa_genotype_form_genotype';
  }

  /**
   * @see BIMS_FILTER::checkFormFilter()
   */
  public static function checkFormFilter($form_state, $details = array()) {

    // Local variables.
    $property   = $details['property'];
    $base_path  = $details['base_path'];

    // Checks the values in the form_state.
    if ($property == 'marker') {
      $name = trim($form_state['name']);
      if (!$name) {
        form_set_error("$base_path][name", 'Please provide marker names');
        return;
      }
    }
    else if ($property == 'genotype') {

      // Checks the location.
      $start  = trim($form_state['loc_start']);
      $stop   = trim($form_state['loc_stop']);
      if ($start) {
        if (!is_numeric($start)) {
          form_set_error("$base_path][loc_start", 'Please enter numeric value for the start');
          return;
        }
      }
      if ($stop) {
        if (!is_numeric($stop)) {
          form_set_error("$base_path][loc_stop", 'Please enter numeric value for the stop');
          return;
        }
      }
      if (($start && $stop) && ($start > $stop)) {
        form_set_error("$base_path][loc_start", '');
        form_set_error("$base_path][loc_stop", "The stop must be is larger than the start.<br />You entered [start : $start > stop : $stop");
        return;
      }
    }
  }

  /**
   * @see BIMS_FILTER::addFormFilter()
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {

    // Local variables.
    $bims_user    = $details['bims_user'];
    $bims_program = $details['bims_program'];
    $type         = $details['type'];
    $key          = $details['key'];
    $list_type    = $details['list_type'];
    $property     = $details['property'];

    // Copies the preference from the previous one.
    $preference = $filters[sizeof($filters) - 1]['preference'];

    // Gets the values in the form_state.
    $where_stmt = '';
    $desc       = '';
    $args       = array();
    $params     = array();
    $traits     = array();

    // Marker.
    if ($property == 'marker') {
      $name_cond            = $form_state['name_cond'];
      $name                 = trim($form_state['name']);
      $params['name_cond']  = $name_cond;
      $params['name']       = $name;

      // Sets the information.
      $tmp = explode("\n", $name);
      $names = array();
      $desc = "Marker(s) [$name_cond] :";
      foreach ($tmp as $n) {
        $n = trim($n);
        $n_escaped = pg_escape_string($n);
        if ($n) {
          $desc .= " $n,";
          $op = '=';
          $t  = $n_escaped;
          if ($name_cond == 'start') {
            $op = 'LIKE';
            $t = $n_escaped . '%';
          }
          else if ($name_cond == 'contain') {
            $op = 'LIKE';
            $t = '%' . $n_escaped . '%';
          }
          else if ($name_cond == 'not_start') {
            $op = 'NOT LIKE';
            $t = $n_escaped . '%';
          }
          else if ($name_cond == 'not_exactly') {
            $op = '!=';
          }
          else if ($name_cond == 'not_contain') {
            $op = 'NOT LIKE';
            $t = '%' . $n_escaped . '%';
          }
          $names []= " LOWER(marker) $op LOWER('$t')";
        }
      }
      $desc = trim($desc, ',');
      $where_stmt = implode(' OR ', $names);
    }

    // Genotype.
    else if ($property == 'genotype') {
      $genome = $form_state['genome'];
      $chr    = $form_state['chr'];
      $start  = trim($form_state['loc_start']);
      $stop   = trim($form_state['loc_stop']);
      $start  = ($start) ? $start : 1;
      $stop   = ($stop) ? $stop : 'MAX';
      $chr    = ($chr) ? $chr : 'ANY';
      $desc = "Genome : $genome<br />";
      $desc .= "Location : $chr between $start and $stop";
      $where_stmt = ' 1=1 ';
    }

    // Updates the filters array.
    if ($where_stmt) {
      $filter = array(
        'bims_program'  => $bims_program,
        'name'          => $property,
        'type'          => $type,
        'list_type'     => $list_type,
        'key'           => 'stock_id',
        'desc'          => $desc,
        'params'        => $params,
        'where_stmt'    => $where_stmt,
        'args'          => $args,
        'traits'        => $traits,
        'preference'    => $preference,
      );
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }

  /**
   * Adds select form element for a vertical filter.
   *
   * @param array $form
   * @param array $details
   *
   * @return boolean
   */
  private static function _addFormSelectVert(&$form, $details) {

    // Local variables.
    $filters    = $details['filters'];
    $property   = $details['property'];
    $select     = $details['select'];
    $max        = $details['max'];
    $order_by   = array_key_exists('order_by', $details) ? $details['order_by'] : '';
    $id         = array_key_exists('id', $details) ? $details['id'] : '';
    $title      = array_key_exists('title', $details) ? $details['title'] : '';
    $desc       = array_key_exists('desc', $details) ? $details['desc'] : '';

    // Generates the SQL.
    $last       = end($filters);
    $last_sql   = $last['sql'];
    $last_args  = $last['args'];
    $sql        = "$select FROM ($last_sql) T";
    if ($order_by) {
      $order_by = "ORDER BY $order_by";
    }

    // Prints the SQL statement for debugging.
    if ($GLOBALS['BIMS_DEBUG_SEARCH_SQL']) {
      bims_print_sql("$sql $order_by", $last_args, 'filter_genotype::_addFormSelectVert()');
    }

    // Gets the options for the filter.
    $options = array();
    $results = db_query("$sql $order_by", $last_args);
    while ($obj = $results->fetchObject()) {
      if ($obj->label) {
        $options[$obj->id] = $obj->label;
      }
    }
    if (empty($options)) {
      return TRUE;
    }
    else {
      $attrs =  array('style' => 'width:100%;');
      if ($id) {
        $attrs['id'] = $id;
      }
      $size = (sizeof($options) > $max) ? $max : sizeof($options);
      $form[$property] = array(
        '#type'         => 'select',
        '#options'      => $options,
        '#multiple'     => TRUE,
        '#size'         => $size,
        '#attributes'   => $attrs,
      );
      if ($title) {
        $form[$property]['#title'] = $title;
      }
      if ($desc) {
        $form[$property]['#description'] = $desc;
      }
    }
    return FALSE;
  }
}