<?php
/**
 * The declaration of BIMS_FILTER_MARKERPROP class.
*
*/
class BIMS_FILTER_MARKERPROP extends BIMS_FILTER {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_FILTER::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_FILTER::getOptions()
   */
  public static function getOptions($filters) {
    $filter = end($filters);
    if ($filter) {
      return array(
        'marker'  => 'Marker',
        'ss_id'   => 'SS ID',
        'rs_id'   => 'RS ID',
        'genome'  => 'Genome Location',
      );
    }
    return array();
  }

  /**
   * @see BIMS_FILTER::getFormFilter()
   */
  public static function getFormFilter(&$form, $details = array()) {

    // Local variables.
    $bims_user      = $details['bims_user'];
    $property       = $details['property'];
    $filters        = $details['filters'];
    $analysis_id    = $details['analysis_id'];
    $chromosome_fid = $details['chromosome_fid'];
    $bims_program   = $bims_user->getProgram();
    $accession      = $bims_program->getBIMSLabel('accession');
    $max            = 13;
    $width          = 300;
    $no_data        = '';

    // Adds the filters by property.
    $disabled = FALSE;
    if ($property == 'marker') {
      $options = array(
        'start'       => 'Start with',
        'exaxctly'    => 'Exactly',
        'contain'     => 'Contains',
        'not_start'   => 'Not start with',
        'not_exactly' => 'Not exaxctly',
        'not_contain' => 'Not contains',
      );
      $form['name_cond']= array(
        '#type'         => 'select',
        '#title'        => 'Marker',
        '#options'      => $options,
        '#attributes'   => array('style' => 'width:140px;'),
      );
      $form['name'] = array(
        '#type'         => 'textarea',
        '#description'  => "Please add one marker per line",
        '#attributes'   => array('style' => 'width:' . $width . 'px;'),
        '#rows'          => 3,
      );
      $form['search_alias']= array(
        '#type'           => 'checkbox',
        '#title'          => 'Check if you want to search Alias as well',
        '#default_value'  => FALSE,
      );
    }
    else if (preg_match("/^(ss_id|rs_id)/i", $property)) {
      $form[$property] = array(
        '#type'         => 'textarea',
        '#title'        => strtoupper($property),
        '#description'  => "Please add one per line",
        '#attributes'   => array('style' => 'width:' . $width . 'px;'),
        '#rows'          => 3,
      );
    }
    else if ($property == 'genome') {
      self::_getFormGenome($form, $details);
    }
    return $disabled;
  }

  /**
   * Sets the form for gentoype filter.
   *
   * @param array $form
   * @param array $details
   */
  private static function _getFormGenome(&$form, $details) {

    // Gets the variables.
    $bims_user      = $details['bims_user'];
    $filters        = $details['filters'];
    $analysis_id    = $details['analysis_id'];
    $chromosome_fid = $details['chromosome_fid'];

    // Gets BIMS_MVIEW_MARKER.
    $bm_gm  = new BIMS_MVIEW_GENOTYPE_MARKER(array('node_id' => $bims_user->getProgramID()));
    $m_gm   = $bm_gm->getMView();

    // Gets the options.
    $last       = end($filters);
    $last_sql   = $last['sql'];
    $last_args  = $last['args'];
    $sql        = "
      SELECT DISTINCT GM.analysis_id, GM.genome
      FROM ($last_sql) T
        INNER JOIN {$m_gm} GM on GM.feature_id = T.feature_id
      WHERE GM.genome IS NOT NULL
      ORDER BY GM.genome
    ";
    $results = db_query($sql, $last_args);
    $options = array();
    while ($obj = $results->fetchObject()) {
      $options[$obj->analysis_id] = $obj->genome;
      if (!$analysis_id) {
        $analysis_id = $obj->analysis_id;
      }
    }

    // Checks the genome selection.
    if (empty($options)) {
      $form['genome']['no_genome'] = array(
        '#markup' => "<div style='padding:10'>Markers are not associated with genome locations.</div>",
      );
      return FALSE;
    }

    // Add genome selection.
    $form['genome']= array(
      '#id'             => 'sagm_mprop_genome',
      '#type'           => 'select',
      '#title'          => 'Genome',
      '#options'        => $options,
      '#default_value'  => $analysis_id,
      '#attributes'     => array('style' => 'min-width:200px;max-width:380px;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_sagm_genotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sagm-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Gets the chromosomes.
    $sql = "
      SELECT DISTINCT GM.chromosome_fid, GM.chromosome
      FROM ($last_sql) T
        INNER JOIN {$m_gm} GM on GM.feature_id = T.feature_id
      WHERE GM.analysis_id = :analysis_id
      ORDER BY GM.chromosome
    ";
    $last_args[':analysis_id'] = $analysis_id;
    $results = db_query($sql, $last_args);
    $options = array();
    while ($obj = $results->fetchObject()) {
      $options[$obj->chromosome_fid] = $obj->chromosome;
      if (!$chromosome_fid) {
        $chromosome_fid = $obj->chromosome_fid;
      }
    }
    if (empty($options)) {
      $form['genome']['no_chromosome'] = array(
        '#markup' => "<div style='padding:10'>No chromosome is associated with '$genome'.</div>",
      );
      return TRUE;
    }

    // Adds the chromosome selection.
    $form['chromosome']= array(
      '#id'             => 'sagm_mprop_chr',
      '#type'           => 'select',
      '#title'          => t('Chr / Scaffold'),
      '#options'        => $options,
      '#default_value'  => $chromosome_fid,
      '#attributes'     => array('style' => 'min-width:200px;max-width:380px;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_sagm_genotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sagm-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Gets the start and stop.
    $sql = "
      SELECT MIN(T.loc_start) AS loc_start, MAX(T.loc_stop) AS loc_stop
      FROM ($last_sql) T
      WHERE T.analysis_id = :analysis_id AND T.chromosome_fid = :chromosome_fid
    ";
    $last_args[':analysis_id']    = $analysis_id;
    $last_args[':chromosome_fid'] = $chromosome_fid;
    $obj = db_query($sql, $last_args)->fetchObject();
    $loc_start = $loc_stop = '';
    if ($obj) {
      $loc_start  = $obj->loc_start;
      $loc_stop   = $obj->loc_stop;
    }

    // Locations.
    $form['loc_start'] = array(
      '#type'       => 'textfield',
      '#title'      => t('Locations between'),
      '#value'      => $loc_start,
      '#attributes' => array('style' => 'width:100px;'),
    );
    $form['loc_stop'] = array(
      '#type'       => 'textfield',
      '#title'      => '&nbsp;',
      '#value'      => $loc_stop,
      '#attributes' => array('style' => 'width:100px;'),
    );
    $form['#theme'] = 'bims_panel_main_sagm_genotype_form_genome';
  }

  /**
   * @see BIMS_FILTER::checkFormFilter()
   */
  public static function checkFormFilter($form_state, $details = array()) {

    // Local variables.
    $property   = $details['property'];
    $base_path  = $details['base_path'];

    // Checks the values in the form_state.
    if ($property == 'marker') {
      $name = trim($form_state['name']);
      if (!$name) {
        form_set_error("$base_path][name", 'Please provide marker names');
        return;
      }
    }
    else if ($property == 'genome') {

      // Checks the location.
      $start  = trim($form_state['loc_start']);
      $stop   = trim($form_state['loc_stop']);
      if ($start) {
        if (!is_numeric($start)) {
          form_set_error("$base_path][loc_start", 'Please enter numeric value for the start');
          return;
        }
      }
      if ($stop) {
        if (!is_numeric($stop)) {
          form_set_error("$base_path][loc_stop", 'Please enter numeric value for the stop');
          return;
        }
      }
      if (($start && $stop) && ($start > $stop)) {
        form_set_error("$base_path][loc_start", '');
        form_set_error("$base_path][loc_stop", "The stop must be is larger than the start.<br />You entered [start : $start > stop : $stop");
        return;
      }
    }
  }

  /**
   * @see BIMS_FILTER::addFormFilter()
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {

    // Local variables.
    $bims_user    = $details['bims_user'];
    $bims_program = $details['bims_program'];
    $property     = $details['property'];

    // Copies the preference from the previous one.
    $preference = $filters[sizeof($filters) - 1]['preference'];

    // Gets the values in the form_state.
    $where_stmt = '';
    $desc       = '';
    $args       = array();
    $params     = array();

    // Marker.
    if ($property == 'marker') {
      $name_cond            = $form_state['name_cond'];
      $search_alias         = $form_state['search_alias'];
      $name                 = trim($form_state['name']);
      $params['name_cond']  = $name_cond;
      $params['name']       = $name;

      // Sets the information.
      $tmp = explode("\n", $name);
      $names = array();
      $aliases = array();
      $desc = "Marker(s) [$name_cond] :";
      foreach ($tmp as $n) {
        $n = trim($n);
        $n_escaped = pg_escape_string($n);
        if ($n) {
          $desc .= " $n,";
          $op = '=';
          $t  = $n_escaped;
          if ($name_cond == 'start') {
            $op = 'LIKE';
            $t = $n_escaped . '%';
          }
          else if ($name_cond == 'contain') {
            $op = 'LIKE';
            $t = '%' . $n_escaped . '%';
          }
          else if ($name_cond == 'not_start') {
            $op = 'NOT LIKE';
            $t = $n_escaped . '%';
          }
          else if ($name_cond == 'not_exactly') {
            $op = '!=';
          }
          else if ($name_cond == 'not_contain') {
            $op = 'NOT LIKE';
            $t = '%' . $n_escaped . '%';
          }
          $names []= " LOWER(marker) $op LOWER('$t')";
          $aliases []= " LOWER(alias) $op LOWER('$t')";
        }
      }
      if ($search_alias) {
        $desc = '<em><b>Alias searched</b></em></br>' . trim($desc, ',');
        $where_marker = implode(' OR ', $names);

        // Adds search alias.
        $bm_alias = new BIMS_MVIEW_ALIAS(array('node_id' => $bims_program->getProgramID()));
        $m_alias  = $bm_alias->getMView();
        $list_alias = implode(' OR ', $aliases);
        $where_alias = "SELECT target_id FROM {$m_alias} WHERE type = 'marker' AND $list_alias";

        // Sets the final WHERE statement.
        $where_stmt = " ($where_marker) OR (feature_id IN ($where_alias)) ";
      }
      else {
        $desc = trim($desc, ',');
        $where_stmt = implode(' OR ', $names);
      }
    }

    // Genome.
    else if ($property == 'genome') {
      $analysis_id    = $form_state['genome'];
      $chromosome_fid = $form_state['chromosome'];
      $start          = intval($form_state['loc_start']);
      $stop           = intval($form_state['loc_stop']);
      $start          = ($start) ? $start : 1;
      $stop           = ($stop) ? $stop : 'MAX';

      // Sets the description of the search.
      $analysis = MCL_CHADO_ANALYSIS::byID($analysis_id);
      $genome   = $analysis->getName();
      $feature  = MCL_CHADO_FEATURE::byID($chromosome_fid);
      $chr      = $feature->getName();
      $desc     = "Genome Location : [$genome] $chr between $start and $stop";
      $where_stmt = ' analysis_id = :analysis_id AND chromosome_fid = :chromosome_fid AND loc_start >= :loc_start ';
      $args[':analysis_id']     = $analysis_id;
      $args[':chromosome_fid']  = $chromosome_fid;
      $args[':loc_start']       = $start;
      if ($stop != 'MAX') {
        $where_stmt .= ' AND loc_stop <= :loc_stop ';
        $args[':loc_stop'] = $stop;
      }
    }

    // Updates the filters array.
    if ($where_stmt) {
      $filter = array(
        'bims_program'  => $bims_program,
        'name'          => $property,
        'desc'          => $desc,
        'params'        => $params,
        'where_stmt'    => $where_stmt,
        'args'          => $args,
        'preference'    => $preference,
      );
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }

  /**
   * Adds select form element for a vertical filter.
   *
   * @param array $form
   * @param array $details
   *
   * @return boolean
   */
  private static function _addFormSelectVert(&$form, $details) {

    // Local variables.
    $filters    = $details['filters'];
    $property   = $details['property'];
    $select     = $details['select'];
    $max        = $details['max'];
    $order_by   = array_key_exists('order_by', $details) ? $details['order_by'] : '';
    $id         = array_key_exists('id', $details) ? $details['id'] : '';
    $title      = array_key_exists('title', $details) ? $details['title'] : '';
    $desc       = array_key_exists('desc', $details) ? $details['desc'] : '';

    // Generates the SQL.
    $last       = end($filters);
    $last_sql   = $last['sql'];
    $last_args  = $last['args'];
    $sql        = "$select FROM ($last_sql) T";
    if ($order_by) {
      $order_by = "ORDER BY $order_by";
    }

    // Prints the SQL statement for debugging.
    if ($GLOBALS['BIMS_DEBUG_SEARCH_SQL']) {
      bims_print_sql("$sql $order_by", $last_args, 'filter_genotype::_addFormSelectVert()');
    }

    // Gets the options for the filter.
    $options = array();
    $results = db_query("$sql $order_by", $last_args);
    while ($obj = $results->fetchObject()) {
      if ($obj->label) {
        $options[$obj->id] = $obj->label;
      }
    }
    if (empty($options)) {
      return TRUE;
    }
    else {
      $attrs =  array('style' => 'width:100%;');
      if ($id) {
        $attrs['id'] = $id;
      }
      $size = (sizeof($options) > $max) ? $max : sizeof($options);
      $form[$property] = array(
        '#type'         => 'select',
        '#options'      => $options,
        '#multiple'     => TRUE,
        '#size'         => $size,
        '#attributes'   => $attrs,
      );
      if ($title) {
        $form[$property]['#title'] = $title;
      }
      if ($desc) {
        $form[$property]['#description'] = $desc;
      }
    }
    return FALSE;
  }
}