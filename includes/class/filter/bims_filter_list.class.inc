<?php
/**
 * The declaration of BIMS_FILTER_LIST class.
*
*/
class BIMS_FILTER_LIST extends BIMS_FILTER {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_FILTER::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_FILTER::getFormFilter()
   */
  public static function getFormFilter(&$form, $details = array()) {

    // Local variables.
    $bims_user      = $details['bims_user'];
    $list_type      = $details['list_type'];
    $filters        = $details['filters'];
    $bims_program   = $bims_user->getProgram();
    $details['max'] = 13;

    // Adds the filters by format.
    $disabled = FALSE;
    if ($list_type == 'location') {
      $details['select']    = "SELECT DISTINCT T.nd_geolocation_id AS id, T.site_name AS label";
      $details['desc']      = 'Please choose locations';
      $details['order_by']  = 'T.site_name';
      $details['max']       = 10;
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    if ($list_type == 'data_year') {
      $details['select']    = "SELECT DISTINCT T.data_year AS id, T.data_year AS label";
      $details['desc']      = 'Please choose data years';
      $details['order_by']  = 'T.data_year';
      $details['max']       = 10;
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($list_type == 'stock') {
      $details['select']    = "SELECT DISTINCT T.stock_id AS id, T.accession AS label";
      $details['desc']      = 'Please choose stocks';
      $details['order_by']  = 'T.accession';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($list_type == 'trial') {
      $details['select']    = "SELECT DISTINCT T.node_id AS id, T.dataset_name AS label";
      $details['desc']      = 'Please choose trials';
      $details['order_by']  = 'T.dataset_name';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($list_type == 'parent') {
      $max                = ($details['max'] / 2) - 2;
      $details['select']  = "SELECT DISTINCT T.stock_id_m AS id, T.maternal AS label";
      $details['max']       = $max;
      $details['list_type'] = 'maternal';
      $details['title']     = 'Maternal parent';
      $details['desc']      = 'Please choose maternal parents';
      $details['order_by']  = 'T.maternal';
      $m_disabled           = self::_addFormSelectVert($form, $details);
      $form['parent_op'] = array(
        '#type'           => 'radios',
        '#options'        => array('AND' => 'AND', 'OR' => 'OR'),
        '#default_value'  => 'OR',
        '#suffix'         => '<br />',
        '#attributes'     => array('style' => 'width:20px;'),
      );
      $details['select']    = "SELECT DISTINCT T.stock_id_p AS id, T.paternal AS label";
      $details['max']       = $max;
      $details['list_type'] = 'paternal';
      $details['title']     = 'Paternal parent';
      $details['desc']      = 'Please choose paternal parents';
      $details['order_by']  = 'T.paternal';
      $f_disabled           = self::_addFormSelectVert($form, $details);
      $disabled             = ($m_disabled && $f_disabled);
      if ($disabled) {
        hide($form['parent_op']);
      }
    }
    else if ($list_type == 'cross') {
      $details['select']    = "SELECT DISTINCT T.nd_experiment_id AS id, T.cross_number AS label";
      $details['desc']      = 'Please choose crosses';
      $details['order_by']  = 'T.cross_number';
      $disabled             = self::_addFormSelectVert($form, $details);
    }
    else if ($list_type == 'trait') {
      $details['bims_program']  = $bims_program;
      $details['desc']          = 'Please choose trait';
      $disabled                 = self::_addFormSelectTrait($form, $details);
    }
    return $disabled;
  }

  /**
   * Adds select form element for a horizontal filter.
   *
   * @param array $form
   * @param array $details
   *
   * @return boolean
   */
  private static function _addFormSelectTrait(&$form, $details) {

    // Local variables.
    $filters      = $details['filters'];
    $list_type    = $details['list_type'];
    $bims_program = $details['bims_program'];
    $max          = $details['max'];
    $id           = array_key_exists('id', $details) ? $details['id'] : '';
    $title        = array_key_exists('title', $details) ? $details['title'] : '';
    $desc         = array_key_exists('desc', $details) ? $details['desc'] : '';

    // Gets traits from the last element in the filters.
    $last = end($filters);
    $options = $last['traits'];

    // Adds the select form element.
    if (empty($options)) {
      return TRUE;
    }
    else {
      $size = (sizeof($options) > $max) ? $max : sizeof($options);
      $attrs =  array('style' => 'width:100%;');
      if ($id) {
        $attrs['id'] = $id;
      }
      $form[$list_type] = array(
        '#type'         => 'select',
        '#options'      => $options,
        '#multiple'     => TRUE,
        '#size'         => $size,
        '#attributes'   => $attrs,
      );
      if ($title) {
        $form[$list_type]['#title'] = $title;
      }
      if ($desc) {
        $form[$list_type]['#description'] = $desc;
      }
    }
    return FALSE;
  }


  /**
   * Adds select form element for a horizontal filter.
   *
   * @param array $form
   * @param array $details
   *
   * @return boolean
   */
  private static function _addFormSelectHoriz(&$form, $details) {

    // Local variables.
    $filters      = $details['filters'];
    $list_type    = $details['list_type'];
    $bims_program = $details['bims_program'];
    $max          = $details['max'];
    $id           = array_key_exists('id', $details) ? $details['id'] : '';
    $title        = array_key_exists('title', $details) ? $details['title'] : '';
    $desc         = array_key_exists('desc', $details) ? $details['desc'] : '';

    // Lists the all columns stored in the previous filter. If not exists,
    // initialze it.
    $last = end($filters);
    if (!array_key_exists($list_type, $last['column'])) {
      $options = self::_getOptionsByType($list_type, $bims_program);
    }
    else {
      $options = $last['column'][$list_type];
    }

    // Adds the select form element.
    if (empty($options)) {
      return TRUE;
    }
    else {
      $size = (sizeof($options) > $max) ? $max : sizeof($options);
      $attrs =  array('style' => 'width:100%;');
      if ($id) {
        $attrs['id'] = $id;
      }
      $form[$list_type] = array(
        '#type'         => 'select',
        '#options'      => $options,
        '#multiple'     => TRUE,
        '#size'         => $size,
        '#attributes'   => $attrs,
      );
      if ($title) {
        $form[$list_type]['#title'] = $title;
      }
      if ($desc) {
        $form[$list_type]['#description'] = $desc;
      }
    }
    return FALSE;
  }

  /**
   * Returns the options by list type.
   *
   * @param string $list_type
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  private static function _getOptionsByType($list_type, $bims_program) {
    $columns = array();
    if ($list_type == 'trait') {
      $params = array(
        'flag'    => BIMS_OPTION,
        'program' => 'program',
      );
      $columns = $bims_program->getActiveDescriptors($params);
    }
    return $columns;
  }

  /**
   * Adds select form element for a vertical filter.
   *
   * @param array $form
   * @param array $details
   *
   * @return boolean
   */
  private static function _addFormSelectVert(&$form, $details) {

    // Local variables.
    $filters    = $details['filters'];
    $list_type  = $details['list_type'];
    $select     = $details['select'];
    $max        = $details['max'];
    $order_by   = array_key_exists('order_by', $details) ? $details['order_by'] : '';
    $id         = array_key_exists('id', $details) ? $details['id'] : '';
    $title      = array_key_exists('title', $details) ? $details['title'] : '';
    $desc       = array_key_exists('desc', $details) ? $details['desc'] : '';

    // Generates the SQL.
    $last       = end($filters);
    $last_sql   = $last['sql'];
    $last_args  = $last['args'];
    $sql        = "$select FROM ($last_sql) T";
    if ($order_by) {
      $order_by = "ORDER BY $order_by";
    }

    // Prints the SQL statement for debugging.
    if ($GLOBALS['BIMS_DEBUG_SEARCH_SQL']) {
      bims_print_sql("$sql $order_by", $last_args, '_addFormSelectVert()');
    }

    // Gets the options for the filter.
    $options = array();
    $results = db_query("$sql $order_by", $last_args);
    while ($obj = $results->fetchObject()) {
      if ($obj->label) {
        $options[$obj->id] = $obj->label;
      }
    }
    if (empty($options)) {
      return TRUE;
    }
    else {
      $attrs =  array('style' => 'width:100%;');
      if ($id) {
        $attrs['id'] = $id;
      }
      $size = (sizeof($options) > $max) ? $max : sizeof($options);
      $form[$list_type] = array(
        '#type'         => 'select',
        '#options'      => $options,
        '#multiple'     => TRUE,
        '#size'         => $size,
        '#attributes'   => $attrs,
      );
      if ($title) {
        $form[$list_type]['#title'] = $title;
      }
      if ($desc) {
        $form[$list_type]['#description'] = $desc;
      }
    }
    return FALSE;
  }

  /**
   * @see BIMS_FILTER::checkFormFilter()
   */
  public static function checkFormFilter($form_state, $details = array()) {

    // Local variables.
    $list_type = $details['list_type'];

    // Checks the values in the form_state.
    if ($list_type == 'parent') {
      $maternal = $form_state['maternal'];
      $paternal = $form_state['paternal'];
      if (empty($maternal) && empty($paternal)) {
        form_set_error('', "Please choose at least one parent");
        return;
      }
    }
    else {
      $values = $form_state[$list_type];
      if (!$values) {
        form_set_error('', 'Please choose at least one');
        return;
      }
    }
  }

  /**
   * @see BIMS_FILTER::addFormFilter()
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {

    // Local variables.
    $type         = $details['type'];
    $bims_user    = $details['bims_user'];
    $list_type    = $details['list_type'];
    $bims_program = $bims_user->getProgram();

    // Gets the filter from the values in the form_state and add it to the
    // filters.
    $class_name = 'BIMS_LIST_' . strtoupper($list_type);
    $filter = $class_name::getFilterByForm($form_state, $bims_program);
    if (!empty($filter)) {
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }

  /*
   * Defines getters and setters below.
   */
}