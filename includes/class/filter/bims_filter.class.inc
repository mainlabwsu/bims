<?php
/**
 * The declaration of BIMS_FILTER class.
 *
 */
class BIMS_FILTER {

  /**
   * Class data members.
   *
   */
 /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {}

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Initializes the $filters array.
   *
   * @param BIMS_USER $bims_user
   * @param string $type
   *
   * @return array
   */
  public static function initFilters($bims_user, $type) {

    // Gets BIMS_PRROGRAM.
    $bims_program = $bims_user->getProgram();
    $progarm_id   = $bims_program->getProgramID();

    // Gets BIMS_MVIEW_PHENOTYPE or BIMS_MVIEW_SNP.
    $bims_mview = NULL;
    $key        = NULL;
    $list_type  = NULL;
    if ($type == 'SEARCH_PHENOTYPE') {
      $key        = 'sample_id';
      $list_type  = 'sample';
      $bims_mview = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $progarm_id));
    }
    else if ($type == 'SEARCH_SNP') {
      $key        = 'stock_id';
      $list_type  = 'stock';
      $bims_mview = new BIMS_MVIEW_SNP(array('node_id' => $progarm_id));
    }
    else if ($type == 'SEARCH_GENOTYPE_MARKER') {
      $key        = 'feature_id';
      $list_type  = 'marker';
      $bims_mview = new BIMS_MVIEW_GENOTYPE_MARKER(array('node_id' => $progarm_id));
    }
    else if ($type == 'SEARCH_GENOTYPE_ACCESSION') {
      $key        = 'stock_id';
      $list_type  = 'stock';
      $bims_mview = new BIMS_MVIEW_GENOTYPE_ACCESSION(array('node_id' => $progarm_id));
    }
    else if ($type == 'SEARCH_SSR_AM') {
      $key        = 'stock_id';
      $list_type  = 'stock';
      $bims_mview = new BIMS_MVIEW_SSR(array('node_id' => $progarm_id));
    }
    else if ($type == 'SEARCH_SSR_ALLELE') {
      $key        = 'stock_id';
      $list_type  = 'stock';
      $bims_mview = new BIMS_MVIEW_SSR(array('node_id' => $progarm_id));
    }
    if ($type == 'SEARCH_CROSS') {
      $key        = 'nd_experiment_id';
      $list_type  = 'cross';
      $bims_mview = new BIMS_MVIEW_CROSS(array('node_id' => $progarm_id));
    }
    $mview = $bims_mview->getMView();

    // Returns the filter.
    $filter = array(
      'user_id'       => $bims_user->getUserID(),
      'program_id'    => $progarm_id,
      'type'          => $type,
      'key'           => $key,
      'list_type'     => $list_type,
      'select'        => 'T.*',
      'from'          => $mview,
      'sql'           => "SELECT T.* FROM $mview T",
      'args'          => array(),
      'column'        => array(),
      'descs'         => '[]',
      'preference'    => self::getPreference($bims_program),
    );

    // Gets the searchable custom properties.
    $cvterm_id_searchable = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'searchable')->getCvtermID();
    $cprop = array();
    if ($type == 'SEARCH_PHENOTYPE') {
      $phenotype_props = $bims_program->getProperyByCV('sample', BIMS_CLASS);
      foreach ((array)$phenotype_props as $cvterm) {
        $searchable = $cvterm->getPropByTypeID($cvterm_id_searchable);
        if ($searchable) {
          $cprop[$cvterm->getCvtermID()] = $cvterm->getName();
        }
      }
    }
    else if ($type == 'SEARCH_SNP') {
      $genotype_props = $bims_program->getProperyByCV('genotype', BIMS_CLASS);
      foreach ((array)$genotype_props as $cvterm) {
        $searchable = $cvterm->getPropByTypeID($cvterm_id_searchable);
        if ($searchable) {
          $cprop[$cvterm->getCvtermID()] = $cvterm->getName();
        }
      }
    }
    else if ($type == 'SEARCH_GENOTYPE_MARKER') {
      $genotype_props = $bims_program->getProperyByCV('marker', BIMS_CLASS);
      foreach ((array)$genotype_props as $cvterm) {
        $searchable = $cvterm->getPropByTypeID($cvterm_id_searchable);
        if ($searchable) {
          $cprop[$cvterm->getCvtermID()] = $cvterm->getName();
        }
      }
    }
    else if ($type == 'SEARCH_GENOTYPE_ACCESSION') {
      $genotype_props = $bims_program->getProperyByCV('accession', BIMS_CLASS);
      foreach ((array)$genotype_props as $cvterm) {
        $searchable = $cvterm->getPropByTypeID($cvterm_id_searchable);
        if ($searchable) {
          $cprop[$cvterm->getCvtermID()] = $cvterm->getName();
        }
      }
    }
    if ($type == 'SEARCH_CROSS') {
      $cross_props = $bims_program->getProperyByCV('cross', BIMS_CLASS);
      foreach ((array)$cross_props as $cvterm) {
        $searchable = $cvterm->getPropByTypeID($cvterm_id_searchable);
        if ($searchable) {
          $cprop[$cvterm->getCvtermID()] = $cvterm->getName();
        }
      }
    }
    $filter['cprop'] = $cprop;

    // Adds the initialize the trait list.
    $params = array(
      'flag'    => BIMS_OPTION,
      'program' => 'program',
      'formats' => array('numeric', 'counter', 'percent', 'categorical', 'boolean', 'date'),
      'rank'    => TRUE,
      'star'    => TRUE,
    );
    $filter['traits'] = $bims_program->getActiveDescriptors($params);
    return array($filter);
  }

  /**
   * Initializes the $filters array.
   *
   * @param BIMS_PROGRAM $bims_program
   *
   * @return array
   */
  public static function getPreference($bims_program) {

    // Adds descriptors.
    $params = array(
      'flag'    => BIMS_CLASS,
      'program' => 'program',
    );
    $active_descriptors = $bims_program->getActiveDescriptors($params);
    $descriptors = array();
    foreach ((array)$active_descriptors as $desc) {
      $cvterm_id = $desc->getCvtermID();
      $descriptors["t$cvterm_id"] = array('name' => $desc->getName(), 'value' => FALSE, 'isNumeric' => $desc->isNumeric());
    }

    // Adds the accession properties.
    $items = $bims_program->getProperties('accession', 'preference');
    $accessionprop = array();
    foreach ((array)$items as $field => $info) {
      $accessionprop[$field] = array('name' => $info['name'], 'def' => $info['def'], 'value' => FALSE);
    }

    // Adds the sample properties.
    $items = $bims_program->getProperties('sample', 'preference');
    $sampleprop = array();
    foreach ((array)$items as $field => $info) {
       $sampleprop[$field] = array('name' => $info['name'], 'def' => $info['def'], 'value' => FALSE);
    }

    // Adds the marker/genotype properties.
    $items = $bims_program->getProperties('marker', 'preference');
    $markerprop = array();
    foreach ((array)$items as $field => $info) {
      $markerprop[$field] = array('name' => $info['name'], 'def' => $info['def'], 'value' => FALSE);
    }

    // Adds the genotype properties.
    $items = $bims_program->getProperties('genotype', 'preference');
    $genotypeprop = array();
    foreach ((array)$items as $field => $info) {
      $genotypeprop[$field] = array('name' => $info['name'], 'def' => $info['def'], 'value' => FALSE);
    }

    // Adds the cross properties.
    $items = $bims_program->getProperties('cross', 'preference');
    $crossprop = array();
    foreach ((array)$items as $field => $info) {
      $crossprop[$field] = array('name' => $info['name'], 'def' => $info['def'], 'value' => FALSE);
    }


    // Returns the preference.
    return array(
      'pc_accessionprop'  => $accessionprop,
      'gc_accessionprop'  => $accessionprop,
      'markerprop'        => $markerprop,
      'genotypeprop'      => $genotypeprop,
      'descriptor'        => $descriptors,
      'sampleprop'        => $sampleprop,
      'crossprop'         => $crossprop,
    );
  }

  /**
   * Returns the options.
   *
   * @param array $filters
   *
   * @return array
   */
  public static function getOptions($filters) {
    // To be overridden by Child class.
    return array();
  }

  /**
   * Adds the filter to the filters array.
   *
   * @param array $filters
   * @param array $filter
   *
   * @return boolean
   */
  public static function addFilter(&$filters, $filter) {

    // Appends the filter.
    $filters []= $filter;

    // Updates the filter.
    return self::updateFilters($filters, -1);
  }

  /**
   * Updates the filter.
   *
   * @param array $filters
   * @param integer $start
   *
   * @return boolean
   */
  public static function updateFilters(&$filters, $start = 1) {

    // Updates the start index. Sets the index to the last entry if '-1' is given.
    if ($start == -1){
      $start = sizeof($filters) - 1;
    }

    // Gets type, key and list_type in the bottom of the filter.
    $program_id = $filters[0]['program_id'];
    $type       = $filters[0]['type'];
    $key        = $filters[0]['key'];
    $from       = $filters[0]['from'];
    $list_type  = $filters[0]['list_type'];

    // Updates the filters from the start index.
    for ($idx = $start; $idx < sizeof($filters); $idx++) {

      // Gets the current filter.
      $name       = $filters[$idx]['name'];
      $args       = $filters[$idx]['args'];
      $desc       = $filters[$idx]['desc'];
      $where_stmt = $filters[$idx]['where_stmt'];

      // Gets the previous filter.
      $pre_filter     = $filters[$idx - 1];
      $pre_sql        = $pre_filter['sql'];
      $pre_descs      = $pre_filter['descs'];
      //$pre_preference = $pre_filter['preference'];
      $pre_args       = array_key_exists('args', $pre_filter) ? $pre_filter['args'] : array();

      // Updates the SQL and the arguments.
      $sql  = "SELECT T$idx.* FROM ($pre_sql) T$idx WHERE $where_stmt";
      if (empty($args)) {
        $args = array();
      }
      foreach ((array)$pre_args as $key => $value) {
        if (!array_key_exists($key, $args)) {
          $args[$key] = $value;
        }
      }

      // Updates the description.
      $desc = sprintf("{\"%s\":\"%s\"}", $name, str_replace('"', '\"', $desc));
      $descs = ($pre_descs == '[]') ? "[$desc]" : rtrim($pre_descs, ']') . ',' . $desc . ']';

      // Updates the traits.
      if ($type == 'SEARCH_PHENOTYPE') {
        $filters[$idx]['traits'] = self::updateTraits($filters[$idx], $pre_filter, $sql, $args);
      }

      // Updates the custom property.
      $filters[$idx]['cprop'] = self::updateCustomProp($filters[$idx], $pre_filter, $sql, $args);

      // Updates the number of data.
      $count_gd = $count_pd = $count_cd = 0;
      if ($type == 'SEARCH_PHENOTYPE') {
        $count_pd = self::_countBreeding($sql, $args);

        // Counts the number of genotypic data for the matched accesions.
        $bm_genotype = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
        $m_genotype  = $bm_genotype->getMView();
        $sql_sub = "
          SELECT COUNT(MVG.stock_id) FROM $m_genotype MVG
          WHERE MVG.stock_id IN (SELECT GT.stock_id FROM ($sql) GT)
        ";
        $count_gd = db_query($sql_sub, $args)->fetchField();
      }
      else if ($type == 'SEARCH_GENOTYPE_ACCESSION') {
        $count_gd = self::_countSNP($sql, $args);

        // Counts the number of phenotypic data for the matched accesions.
        $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
        $m_phenotype  = $bm_phenotype->getMView();
        $sql_sub = "
          SELECT COUNT(MVP.sample_id) FROM $m_phenotype MVP
          WHERE MVP.stock_id IN (SELECT GT.stock_id FROM ($sql) GT)
        ";
        $count_pd = db_query($sql_sub, $args)->fetchField();
      }
      else if ($type == 'SEARCH_GENOTYPE_MARKER') {
        $count_gd = self::_countSNP($sql, $args);

        // Counts the number of phenotypic data for the matched accesions.
        $bm_genotype  = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
        $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
        $m_phenotype  = $bm_phenotype->getMView();
        $m_genotype   = $bm_genotype->getMView();
        $sql_sub = "
          SELECT COUNT(MVP.sample_id) FROM $m_phenotype MVP
          WHERE MVP.stock_id IN (SELECT MVG.stock_id FROM $m_genotype MVG
            WHERE MVG.feature_id IN (SELECT GT.feature_id FROM ($sql) GT)
          )
        ";
        $count_pd = db_query($sql_sub, $args)->fetchField();
      }
      else if ($type == 'SEARCH_CROSS') {
        $count_cd = self::_countCross($sql, $args);
      }

      // Updates the info.
      $filters[$idx]['program_id']  = $program_id;
      $filters[$idx]['key']         = $key;
      $filters[$idx]['type']        = $type;
      $filters[$idx]['list_type']   = $list_type;
      $filters[$idx]['sql']         = $sql;
      $filters[$idx]['args']        = $args;
      $filters[$idx]['descs']       = $descs;
      $filters[$idx]['from']        = $from;
      $filters[$idx]['count_pd']    = $count_pd;
      $filters[$idx]['count_gd']    = $count_gd;
      $filters[$idx]['count_cd']    = $count_cd;
    //$filters[$idx]['preference']  = $pre_preference;

      // Prints the SQL statement for debugging.
      if ($GLOBALS['BIMS_DEBUG_SEARCH_SQL']) {
        bims_print_sql($sql, $args, 'updateFilters()');
        bims_dm($filters[$idx]);
      }
    }
    return TRUE;
  }

  /**
   * Returns the total number of the phenotype.
   *
   * @param string $sql
   * @param array $args
   *
   * @return integer
   */
  private static function _countBreeding($sql, $args) {
    $sql = "SELECT COUNT(C.*) FROM ($sql) C";
    return db_query($sql, $args)->fetchField();
  }

  /**
   * Returns the total number of the cross.
   *
   * @param string $sql
   * @param array $args
   *
   * @return integer
   */
  private static function _countCross($sql, $args) {
    $sql = "SELECT COUNT(C.*) FROM ($sql) C";
    return db_query($sql, $args)->fetchField();
  }

  /**
   * Returns the total number of the genotype.
   *
   * @param string $sql
   * @param array $args
   *
   * @return integer
  */
  private static function _countSNP($sql, $args) {
    $sql = "SELECT SUM(C.num_snp) FROM ($sql) C";
    return db_query($sql, $args)->fetchField();
  }

  /**
   * Returns the list of the custom properties.
   *
   * @param array $filter
   * @param array $pre_filter
   * @param array $sql
   * @param array $args
   *
   * @return array
   */
  private static function updateCustomProp($filter, $pre_filter, $sql, $args = array()) {

    // Gets the current the custom properties.
    $cur_cprop = array_key_exists('cprop', $filter) ? $filter['cprop'] : array();

    // If $cur_cprop is empty, it means a user did not choose the custom properties.
    // Then check for the previous traits.
    if (empty($cur_cprop)) {
      if (array_key_exists('cprop', $pre_filter)) {
        $cur_cprop = $pre_filter['cprop'];
      }
    }

    // Adds the custom properties.
    $select_arr = array();
    foreach ($cur_cprop as $cvterm_id => $name) {
      $select_arr []= " COUNT( NULLIF( p$cvterm_id, '' ))  AS c$cvterm_id ";
    }
    $cprops = array();
    if (!empty($select_arr)) {
      $sql = 'SELECT ' . implode(',', $select_arr) . " FROM ($sql) TMP";
      $results = db_query($sql, $args);
      $row = $results->fetchAssoc();
      foreach ((array)$row as $field => $num) {
        if ($num) {
          $cvterm_id = substr($field, 1);
          $cprops[$cvterm_id] = $cur_cprop[$cvterm_id];
        }
      }
    }
    return $cprops;
  }

  /**
   * Returns the list of the traits.
   *
   * @param array $filter
   * @param array $pre_filter
   * @param array $sql
   * @param array $args
   *
   * @return array
   */
  private static function updateTraits($filter, $pre_filter, $sql, $args = array()) {

    // Gets the current traits.
    $cur_traits = array_key_exists('traits', $filter) ? $filter['traits'] : array();

    // If the traits is empty, it means a user did not choose traits.
    // Then check for the previous traits.
    if (empty($cur_traits)) {
      if (array_key_exists('traits', $pre_filter)) {
        $cur_traits = $pre_filter['traits'];
      }
    }
    if (empty($cur_traits)) {
      $bims_program = BIMS_PROGRAM::byID($filter['program_id']);
      $params = array(
        'flag'    => BIMS_OPTION,
        'program' => 'program',
      );
      $cur_traits = $bims_program->getActiveDescriptors($params);
    }

    $select_arr = array();
    foreach ($cur_traits as $cvterm_id => $name) {
      $select_arr []= " COUNT( NULLIF( t$cvterm_id, '' ))  AS c$cvterm_id ";
    }
    $sql = 'SELECT ' . implode(',', $select_arr) . " FROM ($sql) TMP";
    $results = db_query($sql, $args);
    $traits = array();
    $row = $results->fetchAssoc();
    foreach ((array)$row as $field => $num) {
      if ($num) {
        $cvterm_id = substr($field, 1);
        $traits[$cvterm_id] = $cur_traits[$cvterm_id];
      }
    }
    return $traits;
  }

  /**
   * Adds form elements for filter.
   *
   * @param array $form
   * @param array $details
   */
  public static function getFormFilter(&$form, $details = array()) {
    // To be overridden by Child class.
  }

  /**
   * Checks the form elements for filter.
   *
   * @param array $form_state
   * @param array $details
   */
  public static function checkFormFilter($form_state, $details = array()) {
    // To be overridden by Child class.
  }

  /**
   * Adds the filter.
   *
   * @param array $filters
   * @param array $form_state
   * @param array $details
   *
   * @return boolean
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Displays the criteria of the filter in a form.
   *
   * @param array $form
   * @param array $filters
   * @param array $param
   *
   * @return string
   */
  public static function displayCriteria(&$form, $filters, $param = array()) {
    $size = sizeof($filters);
    if ($size > 1) {

      // Gets the parameters.
      $empty = array_key_exists('empty', $param) ? $param['empty'] : 'No description';
      $title = array_key_exists('title', $param) ? $param['title'] : 'You have chosen :';

      // Gets the all criteria.
      $desc_arr = json_decode($filters[$size-1]['descs'], TRUE);
      $rows = array();
      foreach ($desc_arr as $idx => $items) {
        foreach ($items as $name => $desc) {
          $str_length = strlen($desc);
          if ($str_length > 60) {
            $desc = "<textarea rows='2' style='width:100%;max-width:600px;'>$desc</textarea>";
          }
          $rows []= array(ucfirst($name), $desc);
        }
      }
      $table_vars = array(
        'header'      => array(array('data' => 'Filtered By', 'width' => '80'), 'Values'),
        'rows'        => $rows,
        'attributes'  => array('style' => 'width:100%'),
        'sticky'      => TRUE,
        'empty'       => $empty,
        'param'       => array(),
      );

      // Adds the criteria table to the form.
      $form['criteria'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => TRUE,
        '#title'        => $title,
        '#attributes'   => array('style' => 'width:100%'),
        '#description'  => bims_theme_table($table_vars),
      );
    }
  }

  /**
   * Shows the results of the filter.
   *
   * @param array $form
   * @param array $filter
   * @param BIMS_PROGRAM
   * @param string $id
   * @param integer $list_id
   *
   * @return string
   */
  public static function showResults(&$form, $filter, BIMS_PROGRAM $bims_program, $id, $list_id = NULL) {

    // Gets the filter variables.
    $key        = $filter['key'];
    $sql        = $filter['sql'];
    $args       = $filter['args'];
    $list_type  = strtoupper($filter['list_type']);

    // Gets the headers, mview and conditions.
    $class_name   = 'BIMS_LIST_' . $list_type;
    $table_vars   = $class_name::getTableVars($bims_program, $filter);
    $headers      = $table_vars['headers'];
    $header_sort  = array_key_exists('header_sort', $table_vars) ? $table_vars['header_sort'] : array();
    $mview        = $table_vars['mview'];

    // Prints the SQL statement for debugging.
    if ($GLOBALS['BIMS_DEBUG_SEARCH_SQL']) {
      bims_dm($table_vars);
    }

    // Shows the no data message.
    if (!self::hasData($filter)) {
      $table_vars = array(
        'header'      => $headers,
        'rows'        => array(),
        'attributes'  => array(),
        'sticky'      => TRUE,
        'empty'       => 'No matched data found',
      );
      $form['list_result'] = array(
        '#markup' => bims_theme_table($table_vars),
      );
      return;
    }

    // Adds the list item table with pager.
    $num_pages = 10;
    $element   = 0;

    // Gets list items.
    $fields = NULL;
    if (function_exists('array_column')) {
      $fields = array_column($headers, 'field');
    }
    else {
      $fields = array_map(function($element) { return $element['field'];}, $headers);
    }

    // Adds stock_id for sample.
    if ($list_type == 'SAMPLE') {
      $fields []= 'stock_id';
      $fields []= 'sample_id';
    }

    // Sets the query.
    $query = db_select($mview, 'MV')->fields('MV', $fields);

    // Adds the condition.
    $sql_cond = "SELECT $key FROM ($sql) C";
    $results = db_query($sql_cond, $args)->fetchCol();
    if (!empty($results)) {
      $query->condition($key, $results, 'IN');
    }

    // Sorts the results.
    $sort   = isset($_GET['sort']) ? strtoupper($_GET['sort']) : 'DESC';
    $order  = isset($_GET['order']) ? $_GET['order'] : '';
    if (!$order) {
      $order  = $headers[0]['field'];
      $sort   = strtoupper($headers[0]['sort']);
    }

    // Adds '::numeric' for the numeric descriptor.
    $numeric = '';
    if (array_key_exists($order, $header_sort) && $header_sort[$order]) {
      $numeric = '::numeric';
    }

    // Gets the results.
    $query = $query->extend('PagerDefault')->limit($num_pages)
      ->extend('TableSort')->orderBy($order.$numeric, $sort);
    $results = $query->execute();

    // Populates the table rows.
    $rows = array();
    while ($assoc = $results->fetchAssoc()) {
      $row = array();
      foreach ($headers as $header) {
        $field = $header['field'];
        if ($list_type == 'SAMPLE' && $field == 'accession') {
          $stock_id   = $assoc['stock_id'];
          $sample_id  = $assoc['sample_id'];
          $accession  = $assoc['accession'];
          $row []= "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_sample\",\"Sample\",\"bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id\"); return (false);'>$accession</a>";
        }
        else {
          $value = $assoc[$field];
          $row []= $value;
        }
      }
      $rows []= $row;
    }

    // Params for the sortable table and the pager.
    $url_str  = ($list_id) ? $list_id : 'SESSION_FILTER';
    $url      = "bims/load_main_panel/$id/$url_str" . '?';
    $param = array(
      'url' => $url,
      'id'  => $id,
      'tab' => ucfirst($filter['list_type']) . ' List',
    );

    // Creates the data table.
    $table_vars = array(
      'header'      => $headers,
      'rows'        => $rows,
      'attributes'  => array(),
      'sticky'      => TRUE,
      'empty'       => 'No matched data found',
      'param'       => $param,
    );
    $table = bims_theme_table($table_vars);

    // Creates the pager.
    $pager_vars = array(
      'tags'        => array(),
      'element'     => $element,
      'parameters'  => array(),
      'quantity'    => $num_pages,
      'param'       => $param,
    );
    $pager = bims_theme_pager($pager_vars);

    // Adds the result table.
    $form['list_result'] = array(
      '#markup' => $table . $pager,
    );
  }

  /**
   * Checks if this filter has matched data.
   *
   * @param array $filter
   *
   * @return boolean
   */
  public static function hasData($filter) {
    if ($filter['type'] == 'SEARCH_CROSS') {
      return $filter['count_cd'] ? TRUE : FALSE;
    }
    else if (preg_match("/^SEARCH_PHENOTYPE/", $filter['type'])) {
      return $filter['count_pd'] ? TRUE : FALSE;
    }
    else if (preg_match("/^SEARCH_SNP/", $filter['type'])) {
      return $filter['count_gd'] ? TRUE : FALSE;
    }
    return FALSE;
  }
}