<?php
/**
 * The declaration of BIMS_FILTER_CUSTOMPROP class.
*
*/
class BIMS_FILTER_CUSTOMPROP extends BIMS_FILTER {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_FILTER::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_FILTER::getOptions()
   */
  public static function getOptions($filters) {

    // Returns the 'cprop' in the last filter.
    $filter = end($filters);
    return $filter['cprop'];
  }

  /**
   * @see BIMS_FILTER::getFormFilter()
   */
  public static function getFormFilter(&$form, $details = array()) {

    // Local variables.
    $disabled         = FALSE;
    $max              = 10;
    $bims_user        = $details['bims_user'];
    $property         = $details['property'];
    $filters          = $details['filters'];
    $filter           = end($filters);
    $bims_program     = $bims_user->getProgram();
    $stats_arr        = array();

    // Gets the filter type from the cvterm.
    $cvterm = MCL_CHADO_CVTERM::byID($property);
    if ($cvterm) {
      $format     = $cvterm->getFormat();
      $cvterm_id  = $cvterm->getCvtermID();
      $name       = $cvterm->getName();
      $field      = "p$cvterm_id";

      // Gets the precision format.
      $pricison_format = bims_get_precision($format);

      // Adds the property info. table.
      self::addPropInfoTable($form, $name, $format);

      // TYPE : 'text'.
      if ($format == 'text') {
        $options = array(
          'start'       => 'Start with',
          'exaxctly'    => 'Exactly',
          'contain'     => 'Contains',
          'not_start'   => 'Not start with',
          'not_exactly' => 'Not exaxctly',
          'not_contain' => 'Not contains',
        );
        $form['name_cond']= array(
          '#type'       => 'select',
          '#options'    => $options,
          '#attributes' => array('style' => 'width:140px;'),
        );
        $form['name'] = array(
          '#type'         => 'textarea',
          '#description'  => "Please add one property per line",
          '#attributes'   => array('style' => 'width:80%;'),
          '#rows'          => 4,
        );
      }

      // TYPE : 'categorical'.
      else if ($format == 'categorical') {

        // Adds the stats table.
        $stats_table = BIMS_FILTER_CUSTOMPROP::getStatsTable($filter, $name, $field, $format, $stats_arr);
        $form['stats_table'] = array(
          '#markup' => $stats_table,
        );

        // Gets the options.
        $last       = $filter;
        $last_sql   = $last['sql'];
        $last_args  = $last['args'];
        $field      = "p$cvterm_id";
        $sql        = "
          SELECT DISTINCT $field
          FROM ($last_sql) T
          WHERE $field IS NOT NULL
          ORDER BY $field
        ";
        $results = db_query($sql, $last_args);
        $options = array();
        $size = 0;
        while ($obj = $results->fetchObject()) {
          if ($obj->{$field}) {
            $size++;
            $options[$obj->{$field}] = $obj->{$field};
          }
        }
        if ($size) {
          $size = ($size > $max) ? $max : $size;
          $form['cprop_ddl'] = array(
            '#type'       => 'select',
            '#options'    => $options,
            '#multiple'   => TRUE,
            '#size'       => $size,
            '#attributes' => array('style' => 'width:100%;'),
          );
        }
        else {
          $disabled = TRUE;
        }
      }

      // TYPE : numeric types.
      else if (preg_match("/^(numeric|counter|percent)/", $format)) {

        // Adds the stats table.
        $stats_table = BIMS_FILTER_CUSTOMPROP::getStatsTable($filter, $name, $field, $format, $stats_arr);
        $form['stats_table'] = array(
          '#markup' => $stats_table,
        );
        $form['cprop_min'] = array(
          '#title'          => 'Minimum',
          '#type'           => 'textfield',
          '#default_value'  => sprintf($pricison_format, $stats_arr['min']),
          '#attributes'     => array('style' => 'width:150px;'),
        );
        $form['cprop_max'] = array(
          '#title'          => 'Maximum',
          '#type'           => 'textfield',
          '#default_value'  => sprintf($pricison_format, $stats_arr['max']),
          '#attributes'     => array('style' => 'width:150px;'),
        );
      }
      else {
        $disabled = TRUE;
      }
    }
    return $disabled;
  }

  /**
   * Returns the statistical information in a table.
   *
   * @param array $filter
   * @param string $name
   * @param string $field
   * @param array $stats_arr
   * @param boolean $row_only
   *
   * @return string
   */
  public static function getStatsTable($filter, $name, $field, $format, &$stats_arr = array(), $row_only = FALSE) {

    // Sets the parameters for the stats computation.
    $program_id = $filter['program_id'];
    $key        = $filter['key'];
    $sql        = $filter['sql'];
    $args       = $filter['args'];
    $from       = $filter['from'];
    $condition  = " $key IN (SELECT Z.$key FROM ($sql) Z)";

    // Computes the stats.
    $bims_stats = new BIMS_STATS();
    $param = array(
      'condition' => $condition,
      'args'      => $args,
      'from'      => $from,
      'name'      => $name,
      'field'     => $field,
      'format'    => $format,
    );
    $stats_arr = $bims_stats->calcStats($param);

    // Generates the stats table.
    return BIMS_STATS::getStatsTable($program_id, $stats_arr, -1, $row_only);
  }

  /**
   * Adds the property info. table.
   *
   * @param array $form
   * @param string $name
   * @param string $format
   */
  public static function addPropInfoTable(&$form, $name, $format) {

    // Property info. table.
    if (preg_match("/^(text|selection|numeric)/", $format)) {
      $rows = array(
        array(array('data' => 'Name', 'size' => 20), $name),
        array('Type', ucfirst($format)),
      );
      $table_vars = array(
        'rows'        => $rows,
        'attributes'  => array('style' => 'width:100%;'),
      );
      $form['prop_info'] = array('#markup' => theme('table', $table_vars));
    }
  }

  /**
   * @see BIMS_FILTER::checkFormFilter()
   */
  public static function checkFormFilter($form_state, $details = array()) {

    // Local variables.
    $property   = $details['property'];
    $base_path  = $details['base_path'];

    // Gets the foramt.
    $cvterm = MCL_CHADO_CVTERM::byID($property);
    $format = $cvterm->getFormat();

    // Checks 'categorical'.
    if ($format == 'categorical') {
      if (!$form_state['cprop_ddl']) {
        form_set_error("$base_path][cprop_ddl", 'Please choose at least one property');
        return;
      }
    }

    // Checks 'text'.
    else if ($format == 'text') {
      $name = trim($form_state['name']);
      if (!$name) {
        form_set_error("$base_path][name", 'Please provide properties');
        return;
      }
    }

    // Checks 'numeric'.
    else if ($format == 'numeric') {
      $min = trim($form_state['cprop_min']);
      $max = trim($form_state['cprop_max']);
      if (!bims_is_numeric($min)) {
        form_set_error("$base_path][cprop_min", 'Please provide numerical value at minimum');
        return;
      }
      if (!bims_is_numeric($max)) {
        form_set_error("$base_path][cprop_max", 'Please provide numerical value at maximum');
        return;
      }
      if ($min == '' && $max == '') {
        form_set_error("$base_path][cprop_max", 'Please provide numeric valaue');
        form_set_error("$base_path][cprop_min", '');
        return;
      }
      if (($min && $max) && ($min > $max)) {
        form_set_error("$base_path][cprop_min", 'The minimum value cannot exceeds the maximum value');
        return;
      }
    }
  }

  /**
   * @see BIMS_FILTER::addFormFilter()
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {

    // Local variables.
    $bims_user    = $details['bims_user'];
    $bims_program = $details['bims_program'];
    $property     = $details['property'];

    // Gets the values in the form_state.
    $where_stmt = '';
    $desc       = '';
    $args       = array();
    $params     = array();
    $traits     = array();

    // Gets the format.
    $cvterm = MCL_CHADO_CVTERM::byID($property);
    if (!$cvterm) {
      return FALSE;
    }
    $cvterm_id  = $cvterm->getCvtermID();
    $format = $cvterm->getFormat();

    // Copies the preference from the previous one and update it.
    $filter = $filters[sizeof($filters) - 1];
    self::updatePreference($filter, $cvterm_id);

    // Adds 'text'.
    if ($format == 'text') {
      $name_cond            = $form_state['name_cond'];
      $name                 = trim($form_state['name']);
      $params['name_cond']  = $name_cond;
      $params['name']       = $name;

      // Sets the information.
      $tmp = explode("\n", $name);
      $names = array();
      $desc = "Properties [$name_cond] :";
      foreach ($tmp as $n) {
        $n = trim($n);
        $n_escaped = pg_escape_string($n);
        if ($n) {
          $desc .= " $n,";
          $op = '=';
          $t  = $n_escaped;
          if ($name_cond == 'start') {
            $op = 'LIKE';
            $t = $n_escaped . '%';
          }
          else if ($name_cond == 'contain') {
            $op = 'LIKE';
            $t = '%' . $n_escaped . '%';
          }
          else if ($name_cond == 'not_start') {
            $op = 'NOT LIKE';
            $t = $n_escaped . '%';
          }
          else if ($name_cond == 'not_exactly') {
            $op = '!=';
          }
          else if ($name_cond == 'not_contain') {
            $op = 'NOT LIKE';
            $t = '%' . $n_escaped . '%';
          }
          $names []= " LOWER(p$property) $op LOWER('$t')";
        }
      }
      $desc = trim($desc, ',');
      $where_stmt = implode(' OR ', $names);
    }

    // Adds 'categorical'.
    else if ($format == 'categorical') {
      $cprop_ddl = $form_state['cprop_ddl'];
      $params['cprop_ddl']  = $cprop_ddl;

      // Sets the information.
      if (!empty($cprop_ddl)) {
        $desc = implode(',', $cprop_ddl);
        $in_clause = bims_get_pg_in_stmt($cprop_ddl);
        $where_stmt = " p$property IN ($in_clause)";
      }
    }

    // Adds 'numeric'.
    else if (preg_match("/^(numeric|percent|counter)/", $format)) {
      $min      = $form_state['cprop_min'];
      $max      = $form_state['cprop_max'];
      $min_flag = bims_is_numeric($min, TRUE);
      $max_flag = bims_is_numeric($max, TRUE);

      // Sets the filed.
      $field = " CAST (p$property AS DOUBLE PRECISION) ";
      if ($format == 'counter') {
        $field = " CAST (p$property AS INTEGER) ";
      }
      if ($min_flag && $max_flag) {
        $desc = "[MIN] $min - $max [MAX]";
        $where_stmt = " ($field >= $min AND $field <= $max) ";
      }
      else if ($min_flag) {
        $desc = "[MIN] $min <";
        $where_stmt = " $field >= $min ";
      }
      else if ($max_flag) {
        $desc = "< $max [MAX]";
        $where_stmt = " $field <= $max ";
      }
    }

    // Updates the filters array.
    if ($where_stmt) {
      $filter = array(
        'bims_program'  => $bims_program,
        'name'          => $cvterm->getName(),
        'desc'          => $desc,
        'params'        => $params,
        'from'          => $filter['from'],
        'where_stmt'    => $where_stmt,
        'args'          => $args,
        'traits'        => $traits,
        'preference'    => $filter['preference'],
      );
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }

  /**
   * Updates the preference.
   *
   * @param array $filter
   * @param integer $cvterm_id
   */
  public static function updatePreference(&$filter, $cvterm_id) {

    // Gets the property type.
    $prop_type = strtolower(BIMS_PROGRAM::getPropType($cvterm_id));
    if ($prop_type) {

      // Update the prop type.
      if (preg_match("/phenotype/", $prop_type)) {
        $prop_type = 'pc_accession';
      }
      else if (preg_match("/genotype/i", $prop_type)) {
        $pref_idx = 'gc_accession';
      }

      // Updates the preference.
      $pref_idx = $prop_type . 'prop';
      if (array_key_exists($pref_idx, $filter['preference'])) {
        $filter['preference'][$pref_idx]["p$cvterm_id"]['value'] = TRUE;
      }
    }
  }

  /**
   * Adds select form element for a vertical filter.
   *
   * @param array $form
   * @param array $details
   *
   * @return boolean
   */
  private static function _addFormSelectVert(&$form, $details) {

    // Local variables.
    $filters    = $details['filters'];
    $property   = $details['property'];
    $select     = $details['select'];
    $max        = $details['max'];
    $order_by   = array_key_exists('order_by', $details) ? $details['order_by'] : '';
    $id         = array_key_exists('id', $details) ? $details['id'] : '';
    $title      = array_key_exists('title', $details) ? $details['title'] : '';
    $desc       = array_key_exists('desc', $details) ? $details['desc'] : '';

    // Generates the SQL.
    $last       = end($filters);
    $last_sql   = $last['sql'];
    $last_args  = $last['args'];
    $sql        = "$select FROM ($last_sql) T";
    if ($order_by) {
      $order_by = "ORDER BY $order_by";
    }

    // Prints the SQL statement for debugging.
    if ($GLOBALS['BIMS_DEBUG_SEARCH_SQL']) {
      bims_print_sql("$sql $order_by", $last_args, 'filter_genotype::_addFormSelectVert()');
    }

    // Gets the options for the filter.
    $options = array();
    $results = db_query("$sql $order_by", $last_args);
    while ($obj = $results->fetchObject()) {
      if ($obj->label) {
        $options[$obj->id] = $obj->label;
      }
    }
    if (empty($options)) {
      return TRUE;
    }
    else {
      $attrs =  array('style' => 'width:100%;');
      if ($id) {
        $attrs['id'] = $id;
      }
      $size = (sizeof($options) > $max) ? $max : sizeof($options);
      $form[$property] = array(
        '#type'         => 'select',
        '#options'      => $options,
        '#multiple'     => TRUE,
        '#size'         => $size,
        '#attributes'   => $attrs,
      );
      if ($title) {
        $form[$property]['#title'] = $title;
      }
      if ($desc) {
        $form[$property]['#description'] = $desc;
      }
    }
    return FALSE;
  }
}