<?php
/**
 * The declaration of BIMS_FILTER_TRAIT class.
*
*/
class BIMS_FILTER_TRAIT extends BIMS_FILTER {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_FILTER::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_FILTER::getOptions()
   */
  public static function getOptions($filters) {

    // Returns the 'traits' in the last filter.
    $filter = end($filters);
    return $filter['traits'];
  }

  /**
   * @see BIMS_FILTER::getFormFilter()
   */
  public static function getFormFilter(&$form, $details = array()) {

    // Local variables.
    $bims_user        = $details['bims_user'];
    $cvterm_id        = $details['cvterm_id'];
    $filters          = $details['filters'];
    $program_id       = $bims_user->getProgramID();
    $disabled         = FALSE;
    $pricison_format  = bims_get_config_setting('bims_precision_format');

    // Adds the description.
    $desc = "Below shows the properties or trait statistics for the filtered dataset (e.g. trait statistics of the phenotyping samples filtered so far).";
    $form['trait_stats_desc'] = array(
      '#markup' => $desc,
    );

    /// Shows the trait information.
    $trait  = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id);
    $name   = $trait->getName();
    $format = $trait->getFormat();

    // Adds the format info. table.
    $rows = array(
      array(array('data' => 'Name', 'size' => 20), $name),
      array('Format', $format),
    );
    $table_vars = array(
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%;'),
    );
    $form['trait_info'] = array(
      '#markup' => theme('table', $table_vars),
    );
    if ($disabled) {
      return $disabled;
    }

    // Gets and shows the stats in a table.
    $stats_table = '';
    $stats_arr = array();
    if (sizeof($filters) == 1) {
      $stats_table = BIMS_MVIEW_PHENOTYPE_STATS::getStatsTable($cvterm_id, $program_id, $stats_arr);
    }
    else {
      $filter = end($filters);
      $stats_table = BIMS_MVIEW_STOCK_STATS::getStatsTable($cvterm_id, $program_id, $filter, $stats_arr);
    }
    if (empty($stats_arr)) {
      $form['no_stats'] = array(
        '#markup' => '<div><em>No statistics for this trait</em></div>',
      );
      return TRUE;
    }
    if ($stats_arr['num'] == 0) {
      $form['no_data'] = array(
        '#markup' => '<div><em>No data point</em></div>',
      );
      return TRUE;
    }
    $form['trait_stats_table'] = array(
      '#markup' => $stats_table,
    );

    // Adds the filters by format.
    if (preg_match("/^(numeric|percent|counter)/", $format)) {

      // Gets the precision format.
      $pricison_format = bims_get_precision($format);

      // Adds min and max fields.
      $form['min'] = array(
        '#title'          => 'Minimum',
        '#type'           => 'textfield',
        '#value'          => sprintf($pricison_format, $stats_arr['min']),
        '#description'    => 'Please specify the minimum value of the trait.',
        '#attributes'     => array('style' => 'width:80%;'),
      );
      $form['max'] = array(
        '#title'          => 'Maximum',
        '#type'           => 'textfield',
        '#value'          => sprintf($pricison_format, $stats_arr['max']),
        '#description'    => 'Please specify the maximum value of the trait.',
        '#attributes'     => array('style' => 'width:80%;'),
      );
    }
    else if ($format == 'date') {
      $form['start_date'] = array(
        '#title'          => 'Start Date',
        '#type'           => 'textfield',
        '#value'          => $stats_arr['min'],
        '#description'    => 'Please specify the starting date of the trait. (e.g. 1/12).',
        '#attributes'     => array('style' => 'width:80%;'),
      );
      $form['end_date'] = array(
        '#title'          => 'End Date',
        '#type'           => 'textfield',
        '#value'          => $stats_arr['max'],
        '#description'    => 'Please specify the ending date of the trait. (e.g. 8/2).',
        '#attributes'     => array('style' => 'width:80%;'),
      );
    }
    else if ($format == 'categorical') {
      $codes = $trait->getPropByKey('codes');
      $code_flag = (is_array($codes)) ? TRUE : FALSE;
      $categories = $trait->getPropByKey('categories');
      $options = array();
      $freq = $stats_arr['freq'];
      if ($categories) {
        $tmp = explode('/', $categories);
        foreach ($tmp as $category) {
          $category_lc = strtolower($category);
          if (!array_key_exists($category_lc, $freq)) {
            continue;
          }
          if ($code_flag && array_key_exists($category_lc, $codes)) {
            $options[$codes[$category_lc]['key']] = $codes[$category_lc]['val'];
          }
          else {
            $options[$category] = $category;
          }
        }
      }
      if (empty($options)) {
        $form['categorical'] = array(
          '#markup' => "$name does not have categories.",
        );
        $disabled = TRUE;
      }
      else {
        $size = (sizeof($options) > 6) ? 6 : sizeof($options);
        $form['categorical'] = array(
          '#type'         => 'select',
          '#title'        => 'Categories',
          '#options'      => $options,
          '#multiple'     => TRUE,
          '#size'         => $size,
          '#description'  => 'Please choose categories',
          '#attributes'   => array('style' => 'width:200px;'),
        );
      }
    }
    else if ($format == 'boolean') {
      $form['boolean'] = array(
        '#type'           => 'radios',
        '#options'        => array('TRUE' => 'TRUE', 'FALSE' => 'FALSE'),
        '#default_value'  => 'TRUE',
        '#attributes'     => array('style' => 'width:20px;'),
      );
    }
    return $disabled;
  }

  /**
   * @see BIMS_FILTER::checkFormFilter()
   */
  public static function checkFormFilter($form_state, $details = array()) {

    // Local variables.
    $cvterm_id  = $details['property'];
    $base_path  = $details['base_path'];

    // Gets the trait.
    $trait  = BIMS_MVIEW_DESCRIPTOR::byKey(array('cvterm_id' => $cvterm_id));
    $name   = $trait->getName();
    $format = $trait->getFormat();

    // Checks the values in the form_state.
    if (preg_match("/(numeric|counter|percent)/", $format)) {
      $min = trim($form_state['min']);
      $max = trim($form_state['max']);
      if ($min) {
        if (!is_numeric($min)) {
          form_set_error("$base_path][min", 'Please enter numeric value for the minimum');
          return;
        }
      }
      if ($max) {
        if (!is_numeric($max)) {
          form_set_error("$base_path][max", 'Please enter numeric value for the maximum');
          return;
        }
      }
      if (($min && $max) && ($min > $max)) {
        form_set_error("$base_path][min", '');
        form_set_error("$base_path][max", "The minimum value is larger than the maximum. Please enter maximum > minimum values.<br />You entered [min : $min > max : $max]");
        return;
      }
      if ($min.$max == '') {
        form_set_error("$base_path][min", '');
        form_set_error("$base_path][max", 'Please enter maximum and/or minimum values');
        return;
      }
    }
    else if ($format == 'date') {
      $start_date = trim($form_state['start_date']);
      $end_date   = trim($form_state['end_date']);
      if (!$start_date && !$end_date) {
        form_set_error("$base_path][start_date", 'Please enter a starting or ending dates');
        return;
      }
      if ($start_date && !preg_match("/\d{1,2}\/\d{1,2}/", $start_date)) {
        form_set_error("$base_path][start_date", 'Invalid date format. The format is D/M. (e.g. 1/2).');
        return;
      }
      if ($end_date && !preg_match("/\d{1,2}\/\d{1,2}/", $end_date)) {
        form_set_error("$base_path][end_date", 'Invalid date format. The format is D/M. (e.g. 1/2).');
        return;
      }
      if ($start_date && $end_date) {
        $start_date_int = (bims_convert_date_str_to_int($start_date));
        $end_date_int   = (bims_convert_date_str_to_int($end_date));
        if ($start_date_int > $end_date_int) {
          form_set_error("$base_path][start_date", "The starting date must be prior to the ending date ($start_date > $end_date).");
          return;
        }
      }
    }
    else if ($format == 'categorical') {
      $categorical = $form_state['categorical'];
      if (!$categorical) {
        form_set_error("$base_path][categorical", 'Please choose at least one category');
        return;
      }
    }
  }

  /**
   * @see BIMS_FILTER::addFormFilter()
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {

    // Local variables.
    $bims_user        = $details['bims_user'];
    $bims_program     = $details['bims_program'];
    $cvterm_id        = $details['cvterm_id'];
    $pricison_format  = bims_get_config_setting('bims_precision_format');

    // Copies the preference from the previous one.
    $preference = $filters[sizeof($filters) - 1]['preference'];

    // Gets the trait.
    $trait  = BIMS_MVIEW_DESCRIPTOR::byKey(array('cvterm_id' => $cvterm_id));
    $name   = $trait->getName();
    $format = $trait->getFormat();

    // Gets the values in the form_state.
    $where_stmt = '';
    $args       = array();
    $params     = array('cvterm_id' => $cvterm_id);
    $desc       = "$format : ";
    if (preg_match("/(numeric|counter|percent)/", $format)) {
      $min            = $form_state['min'];
      $max            = $form_state['max'];
      $params['min']  = $min;
      $params['max']  = $max;
      if ($min && max) {
        $desc .= "[MIN] $min - $max [MAX]";
        $where_stmt = sprintf("((t%d != '' AND CAST(t%d as float) >= $pricison_format) AND (t%d != '' AND CAST(t%d as float) <= $pricison_format))", $cvterm_id, $cvterm_id, $min, $cvterm_id, $cvterm_id, $max);
      }
      else if ($min) {
        $desc .= "[MIN] $min <";
        $where_stmt = sprintf("(t%d != '' AND CAST(t%d as float) >= $pricison_format)", $cvterm_id, $cvterm_id, $min);
      }
      else if ($max) {
        $desc .= "< $max [MAX]";
        $where_stmt = sprintf("(t%d != '' AND CAST(t%d as float) <= $pricison_format)", $cvterm_id, $cvterm_id, $max);
      }

    }
    else if ($format == 'date') {
      $start_date     = $form_state['start_date'];
      $end_date       = $form_state['end_date'];
      $start_date_int = bims_convert_date_str_to_int($start_date);
      $end_date_int   = bims_convert_date_str_to_int($end_date);
      if ($start_date_int && $end_date_int) {
        $desc .= "[START] $start_date - $end_date [END]";
        $where_stmt = sprintf("((t%d != '' AND CAST(t%d_jd as int) >= %d) AND (t%d != '' AND CAST(t%d_jd as int) <= %d))", $cvterm_id, $cvterm_id, $start_date_int, $cvterm_id, $cvterm_id, $end_date_int);

      }
      else if ($start_date_int) {
        $desc .= "[START] $start_date < ";
        $where_stmt = sprintf("(t%d != '' AND CAST(t%d_jd as int) >= %d)", $cvterm_id, $cvterm_id, $start_date_int);
      }
      else if ($end_date_int) {
        $desc .= " < $end_date [END]";
        $where_stmt = sprintf("(t%d != '' AND CAST(t%d_jd as int) <= %d)", $cvterm_id, $cvterm_id, $end_date_int);
      }
    }
    else if ($format == 'categorical') {
      $categorical            = $form_state['categorical'];
      $params['categorical']  = $categorical;
      $where_stmt             = " LOWER(t$cvterm_id) IN ('" . strtolower(implode("','", $categorical)) . "')";
      $desc .= implode(', ', $categorical);
    }
    else if ($format == 'boolean') {
      $bool               = $form_state['boolean'];
      $params['boolean']  = $bool;
      $where_stmt         = "(t$cvterm_id = '$bool')";
      $desc .= $bool;
    }

    // Sets as default columns for the chosen descriptor.
    $preference['descriptor']["t$cvterm_id"]['value'] = TRUE;

    // Updates the filters array.
    if ($where_stmt) {
      $filter = array(
        'bims_program'  => $bims_program,
        'name'          => $name,
        'desc'          => $desc,
        'params'        => $params,
        'where_stmt'    => $where_stmt,
        'args'          => $args,
        'preference'    => $preference,
      );
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }
}