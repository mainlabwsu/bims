<?php
/**
 * The declaration of BIMS_FILTER_GENOTYPE_ALLELE class.
*
*/
class BIMS_FILTER_GENOTYPE_ALLELE extends BIMS_FILTER {

  /**
   * Class data members.
   */
  /**
   * @see BIMS_FILTER::__construct()
   *
   * @param $details
   */
  public function __construct($details) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_FILTER::getFormFilter()
   */
  public static function getFormFilter(&$form, $details = array()) {

    // Local variables.
    $bims_crop  = $details['bims_crop'];
    $alleles    = $details['alleles'];

    // Checks the alleles.
    if (empty($alleles)) {
      return TRUE;
    }
    $alleles = array_merge(array('any' => 'Any'), $alleles);

    // Adds the dropdwon for the alleles.
    $ploidy = $bims_crop->getPloidy();
    for ($i = 1; $i <= $ploidy; $i++) {
      $form['allele-' . $i]= array(
        '#type'         => 'select',
        '#title'        => 'Allele' . $i,
        '#options'      => $alleles,
        '#attributes'   => array('style' => 'width:140px;'),
      );
    }
    return FALSE;
  }

  /**
   * @see BIMS_FILTER::checkFormFilter()
   */
  public static function checkFormFilter($form_state, $details = array()) {
    $base_path  = $details['set_filter'];
    $marker     = $details['marker'];
    $alleles    = $details['alleles'];
    if (empty($alleles)) {
      form_set_error($base_path, "Please choose an allele");
    }
    return;
  }

  /**
   * @see BIMS_FILTER::addFormFilter()
   */
  public static function addFormFilter(&$filters, $form_state, $details = array()) {

    // Local variables.
    $bims_user    = $details['bims_user'];
    $bims_program = $details['bims_program'];
    $property     = $details['property'];
    $marker_name  = $details['marker'];
    $alleles      = $details['alleles'];

    // Copies the preference from the previous one.
    $preference = $filters[sizeof($filters) - 1]['preference'];

    // Gets the values in the form_state.
    $where_stmt = '';
    $desc       = '';
    $args       = array();
    $params     = array();

    // Adds the marker condition.
    $desc .= "Marker name : $marker_name";
    $where_stmt .= " LOWER(marker) = LOWER(:marker_name) ";
    $args[':marker_name'] = $marker_name;

    // Adds the allele condition.
    $desc .= 'Alleles : ' . implode('|', array_values($alleles));
    $where_stmt .= " AND allele IN ('" . implode("','", array_values($alleles)) . "') ";

    // Updates the filters array.
    if ($where_stmt) {
      $filter = array(
        'bims_program'  => $bims_program,
        'name'          => $property,
        'desc'          => $desc,
        'params'        => $params,
        'where_stmt'    => $where_stmt,
        'args'          => $args,
        'preference'    => $preference,
      );
      return self::addFilter($filters, $filter);
    }
    return FALSE;
  }
}