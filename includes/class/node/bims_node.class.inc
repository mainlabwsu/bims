<?php
/**
 * BIMS_NODE Class.
 *
 * This class handles tree node element.
 *
 */
class BIMS_NODE extends PUBLIC_BIMS_NODE {

  /**
   * Data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_NODE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_NODE::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return $parent;
  }

  /**
   * Generate BIMS_NODE by ID.
   *
   * @param integer $node_id
   */
  public static function byID($node_id) {
    if (!bims_is_int($node_id)) {
      return NULL;
    }
    $parent = parent::byKey(array('node_id' => $node_id));
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Generate BIMS_NODE by project ID.
   *
   * @param integer $project_id
   * @param integer $program_id
   *
   * @return BIMS_NODE|NULL
   */
  public static function byProjectID($project_id, $program_id) {
    $keys = array(
      'root_id'     => $program_id,
      'project_id'  => $project_id,
    );
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_PROGRAM that this node belongs to.
   *
   * @return BIMS_PROGRAM
   */
  public function getProgram() {
    return BIMS_PROGRAM::byID($this->root_id);
  }

  /**
   * Returns the program ID of the program that this node belongs to.
   *
   * @return integer
   */
  public function getProgramID() {
    $bims_program = BIMS_PROGRAM::byID($this->root_id);
    return $bims_program->getProgramID();
  }

  /**
   * Returns the appropriate BIMS_NDOE.
   *
   * @param integer $node_id
   *
   * @return various
   */
  public static function getNodebyID($node_id) {

    // Gets the node type.
    $bims_node = BIMS_NODE::byID($node_id);
    $node_type = $bims_node->getType();

    // Returns the desendence of BIMS_NODE.
    $bims_obj = NULL;
    switch ($node_type) {
      case 'PROGRAM'      : $bims_obj = BIMS_PROGRAM::byID($node_id); break;
      case 'SUB_PROGRAM'  : $bims_obj = BIMS_SUB_PROGRAM::byID($node_id); break;
      case 'TRIAL'        : $bims_obj = BIMS_TRIAL::byID($node_id); break;
    }
    return $bims_obj;
  }

  /**
   * Returns the all associated data.
   *
   * @return array
   */
  public function getAssocData() {
    // To be overridden by Child class.
    return array();
  }

  /**
   * Create a node.
   *
   * @param array $details
   *  node_id
   *  name
   *  parent
   */
  public static function createNode($details) {
    $node_id  = $details['node_id'];
    $type     = $details['type'];
    $name     = $details['name'];

    // Sets the icon.
    $icon = 'bims-' . str_replace('_', '-', strtolower($type));

    // Sets the attribute.
    $a_attr = array(
      'href'  => '#',
      'id'    => $node_id . '_anchor',
    );

    // Sets the state.
    $state = array(
      'loaded'    => true,
      'selected'  => false,
      'disabled'  => false,
    );

    // Creates a new node.
    $node = array(
      'id'        => $node_id,
      'type'      => $type,
      'text'      => $name,
      'icon'      => $icon,
      'li_attr'   => array('id' => $node_id),
      'a_attr'    => $a_attr,
      'state'     => $state,
      'children'  => array(),
    );
    return $node;
  }

  /**
   * @see PUBLIC_BIMS_NODE::insert()
   */
  public function insert() {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Insert data.
    if (parent::insert()) {

      // Initialize the trees if this node is PROGRAM.
      if ($this->type == 'PROGRAM') {
        $this->initTree();
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @see PUBLIC_BIMS_NODE::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop fields.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Removes all related nodes in the tables.
   *
   * @return boolean
   */
  public function removeRelatedNodes() {

    // Deletes from bims_node_relationship.
    db_delete('bims_node_relationship')
      ->condition('parent_id', $this->node_id, '=')
      ->execute();
    db_delete('bims_node_relationship')
      ->condition('child_id', $this->node_id, '=')
      ->execute();

    // Deletes child nodes in bims_node.
    $sql = "DELETE FROM {bims_node} WHERE root_id = :node_id AND root_id != node_id";
    db_query($sql, array(':node_id' => $this->node_id));
    return TRUE;
  }

  /**
   * @see PUBLIC_BIMS_NODE::delete()
   */
  public function delete() {

    // Removes related nodes.
    $this->removeRelatedNodes();
    return parent::delete();
  }

  /**
   * Updates the stored active descriptors from the mview.
   *
   * @param boolean $sort_flag
   *
   * @return boolan
   */
  public function updateStoredActiveDescriptorsByMView($sort_flag = TRUE) {

    // Gets BIMS_MVIEW_PHENOTYPE.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->node_id));
    $m_phenotype = $bm_phenotype->getMView();

    // Count the number rows.
    $sql      = "SELECT COUNT(*) FROM {$m_phenotype}";
    $num_rows = db_query($sql)->fetchField();

    // Gets the trait colums.
    $sql = "SELECT MV.* FROM {$m_phenotype} MV LIMIT 1";
    $assoc = db_query($sql)->fetchAssoc();
    $active_descriptors = array();
    foreach ((array)$assoc as $column => $value) {
      $program_id = $assoc['root_id'];
      if (preg_match("/^t(\d+)$/", $column, $matches)) {
        $cvterm_id = $matches[1];

        // Drops the descriptor column if they ara all null/empty.
        $sql = "
          SELECT COUNT(*) FROM {$m_phenotype}
          WHERE $column IS NULL OR $column = ''
        ";
        $num_empty = db_query($sql)->fetchField();
        if ($num_empty == $num_rows) {
          db_drop_field($m_phenotype, $column);
          continue;
        }

        // Adds it as an active descriptor.
        $desc = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id);
        if ($desc) {
          $active_descriptors[$cvterm_id] = array(
            'name'    => $desc->getName(),
            'format'  => $desc->getFormat(),
          );
        }
      }
    }

    // Updates the active descriptors.
    return $this->updateStoredActiveDescriptors($active_descriptors, $sort_flag);
  }

  /**
   * Updates the active descriptors stored in prop.
   *
   * @param array $active_descriptors
   * @param boolean $sort_flag
   *
   * @return boolan
   */
  public function updateStoredActiveDescriptors($active_descriptors, $sort_flag = TRUE) {

    // Sorts the descriptors.
    if ($sort_flag) {



    }

    // Stores the descriptors.
    $this->setPropByKey('active_descriptors', $active_descriptors);
    return $this->update();
  }

  /**
   * Returns the active descriptors stored in prop.
   *
   * @return array
   */
  public function getStoredActiveDescriptors() {
    $active_descriptors = $this->getPropByKey('active_descriptors');
    if (!$active_descriptors) {
      $active_descriptors = array();
    }
    return $active_descriptors;
  }

  /**
   * Adds the descriptor from the active descriptors.
   *
   * @param integer $cvterm_id
   * @param boolean $flag_all
   *
   * @return boolean
   */
  public function addActiveDescriptor($cvterm_id, $flag_all = TRUE) {

    // Gets MCL_CHADO_CVTERM.
    $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
    if (!$cvterm) {
      return FALSE;
    }

    // If this is a program and $all_flag is TRUE, then adds the descriptor
    // to the active descriptors of all the phenotype trials.
    $bims_nodes = array($this);
    if ($all_flag && $this->getType() == 'PROGRAM') {
      $bims_program = BIMS_PROGRAM::byID($this->node_id);
      $bims_nodes = $this->getTrialsByProjectType('phenotype', BIMS_CLASS);
      $bims_nodes []= $this;
    }

    // Adds the descriptor.
    foreach ((array)$bims_nodes as $bims_node) {
      $ad = $bims_node->getStoredActiveDescriptors();
      if (!array_key_exists($cvterm_id, $ad)) {
        $ad[$cvterm_id] = array(
          'name'    => $cvterm->getName(),
          'format'  => $cvterm->getFormat(),
        );
        if (!$bims_node->updateStoredActiveDescriptors($ad)) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

/**
   * Updates the descriptor in the active descriptors.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function updateActiveDescriptors($cvterm_id) {

    // Gets all nodes. If this is a program, then add trials.
    $bims_nodes = array($this);
    if ($this->getType() == 'PROGRAM') {
      $bims_program = BIMS_PROGRAM::byID($this->node_id);
      $bims_nodes = $this->getTrialsByProjectType('phenotype', BIMS_CLASS);
      $bims_nodes []= $this;
    }

    // Gets BIMS_MVIEW_PHENOTYPE.
    $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->getRootID()));
    $table = $mview_phenotype->getMView();

    // Updates the active descriptors.
    foreach ((array)$bims_nodes as $bims_node) {

      // Removes this descriptor if the mview does not have t-column.
      if (!db_field_exists($table, "t$cvterm_id")) {
        if (!$bims_node->removeActiveDescriptor($cvterm_id)) {
          return FALSE;
        }
        continue;
      }

      // Checks if this descriptor has an associated data.
      $field = ($bims_node->getType() == 'PROGRAM') ? 'root_id' : 'node_id';
      $sql = "
        SELECT node_id FROM {$table}
        WHERE $field = :node_id AND t$cvterm_id IS NOT NULL OR t$cvterm_id = ''
        LIMIT 1
      ";
      $node_id = db_query($sql, array(':node_id' => $bims_node->getNodeID()))->fetchField();

      // Adds this descriptor as it has data.
      if ($node_id) {
        if (!$bims_node->addActiveDescriptor($cvterm_id, FALSE)) {
          return FALSE;
        }
      }

      // Removes this descriptor as it does not have data.
      else {
        if (!$bims_node->removeActiveDescriptor($cvterm_id)) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * Removes the descriptor from the active descriptors.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function removeActiveDescriptor($cvterm_id) {

    // If this is a program, then remove the descriptor from the active
    // descriptors of all the phenotype trials.
    $bims_nodes = array($this);
    if ($this->getType() == 'PROGRAM') {
      $bims_program = BIMS_PROGRAM::byID($this->node_id);
      $bims_nodes = $this->getTrialsByProjectType('phenotype', BIMS_CLASS);
      $bims_nodes []= $this;
    }

    // Removes the descriptor.
    foreach ((array)$bims_nodes as $bims_node) {
      $ad = $bims_node->getStoredActiveDescriptors();
      if (array_key_exists($cvterm_id, $ad)) {
        unset($ad[$cvterm_id]);
        if (!$bims_node->updateStoredActiveDescriptors($ad)) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }

  /**
   * Returns the active descriptors of this program.
   *
   * @param array $params
   *
   * @return array
   */
  public function getActiveDescriptors($params) {

    //  Gets the parameters.
    $flag     = array_key_exists('flag', $params)     ? $params['flag'] : 'BIMS_OPTION';
    $type     = array_key_exists('type', $params)     ? $params['type'] : 'program';
    $formats  = array_key_exists('formats', $params)  ? $params['formats'] : array();
    $stats    = array_key_exists('stats', $params)    ? $params['stats'] : FALSE;
    $rank     = array_key_exists('rank', $params)     ? $params['rank'] : FALSE;
    $id       = array_key_exists('id', $params)       ? $params['id'] : '';
    $chado    = array_key_exists('chado', $params)    ? $params['chado'] : '';
    $star     = array_key_exists('star', $params)     ? $params['star'] : FALSE;

    // Gets the stored active descriptors.
    $descriptors = array();
    if ($type == 'program') {
      $descriptors = $this->getStoredActiveDescriptors();
    }
    else if ($type == 'trial') {
      $trial = BIMS_TRIAL::byID($id);
      $descriptors = $trial->getStoredActiveDescriptors();
    }
    else if (preg_match("/(accession|cross)/", $type)) {

      // Gets BIMS_MVIEW_PHENOTYPE.
      $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->root_id));
      if (!$mview_phenotype->exists()) {
        return array();
      }
      $mview = $mview_phenotype->getMView();

      // Sets the conditions.
      $args = array();
      $where_str = '';
      if ($chado != '') {
        $args[':chado'] = $chado;
        $where_str .= ' AND MV.chado = :chado ';
      }
      if ($type == 'accession') {
        $args[':stock_id'] = $id;
        $where_str .= ' AND MV.stock_id = :stock_id ';
      }
      else if ($type == 'cross') {
        $args[':nd_experiment_id'] = $id;
        $where_str .= ' AND MV.nd_experiment_id = :nd_experiment_id ';
      }

      // Gets the active descriptors.
      $stats_formats = BIMS_FIELD_BOOK::getStatsFormats();
      $sql = "SELECT MV.* FROM {$mview} MV WHERE 1=1 $where_str";
      $results = db_query($sql, $args);
      while ($assoc = $results->fetchAssoc()) {
        foreach ((array)$assoc as $column => $value) {
          if (preg_match("/^t(\d+)$/", $column, $matches)) {
            if ($value || $value == '0') {
              $cvterm_id = $matches[1];
              if (!array_key_exists($cvterm_id, $descriptors)) {
                $desc = BIMS_MVIEW_DESCRIPTOR::byID($this->root_id, $cvterm_id);
                if ($desc) {
                  $format = $desc->getFormat();
                  if ($stats && !in_array($format, $stats_formats)) {
                    continue;
                  }
                  $imported = ($assoc['chado']) ? TRUE : FALSE;
                  $descriptors[$cvterm_id] = array(
                    'name'      => $desc->getName(),
                    'foramt'    => $format,
                    'imported'  => $imported,
                  );
                }
              }
            }
          }
        }
      }
    }

    // Returns the updated active descrptors.
    $active_descriptors = array();
    if (!empty($descriptors)) {
      foreach($descriptors as $cvterm_id => $prop) {
        if (!$cvterm_id) {
          continue;
        }

        // Gets BIMS_MVIEW_DESCRIPTOR.
        $ad = BIMS_MVIEW_DESCRIPTOR::byID($this->root_id, $cvterm_id);
        if (!$ad) {
          continue;
        }

        // Checks the formats.
        if (!empty($formats)) {
          if (!in_array($prop['format'], $formats)) {
            continue;
          }
        }

        // Adds the descriptor.
        if ($flag == BIMS_OPTION) {
          $star_sym = ($star && $ad->isChado()) ? ' *' : '';
          $active_descriptors[$cvterm_id] = $prop['name'] . $star_sym ;
          asort($active_descriptors);
        }
        else if ($flag == BIMS_VARIOUS) {
          $active_descriptors[$cvterm_id] = $prop;
        }
        else {
          $active_descriptors []= $ad;
        }
      }
    }
    return $active_descriptors;
  }

  /**
   * Returns the ID of the trait descriptor set of this trial if this trial is
   * 'phenotype'.
   *
   * @return integer|NULL
   */
  public function getBreedingCvID() {

    if ($this->project_type == 'phenotype') {
      $sql = "
        SELECT project_id FROM {bims_imported_project}
        WHERE program_id = :program_id AND project_id = :project_id
      ";
      $args = array(
        ':program_id' => $this->root_id,
        ':project_id' => $this->project_id,
      );
      $project_id = db_query($sql, $args)->fetchField();
      if ($project_id) {
        $sql = "
          SELECT C.cv_id
          FROM {chado.cvterm} C
            INNER JOIN {chado.phenotype} P on P.attr_id = C.cvterm_id
            INNER JOIN {chado.nd_experiment_phenotype} NEP on NEP.phenotype_id = P.phenotype_id
            INNER JOIN {chado.nd_experiment_project} NEPRJ on NEPRJ.nd_experiment_id = NEP.nd_experiment_id
          WHERE NEPRJ.project_id = :project_id
          LIMIT 1;
        ";
        return db_query($sql, array(':project_id' => $this->project_id))->fetchField();
      }
      else {
        return $this->getCvID('descriptor');
      }
    }
    return NULL;
  }

  /**
   * Initializes the tree.
   *
   * @return boolean
   */
  public function initTree() {

    // Initializes the tree.
    $details = array(
      'node_id' => $this->node_id,
      'type'    => $this->type,
      'name'    => $this->name,
      'parent'  => NULL,
    );
    $node = BIMS_NODE::createNode($details);

    // Updates the trees (trial, breed_line and cross).
    $this->setTrialTree(json_encode($node));
    $this->setBreedLineTree(json_encode($node));
    $this->setCrossTree(json_encode($node));
    return $this->update();
  }

  /**
   * Checks if this node has child.
   *
   * @return boolean
   */
  public function hasChild() {
    $sql = "
      SELECT COUNT(NR.child_id)
      FROM {bims_node_relationship} NR
      WHERE NR.parent_id = :parent_id
    ";
    $num = db_query($sql, array('parent_id' => $this->node_id))->fetchField();
    return ($num) ? TRUE : FALSE;
  }

  /**
   * Returns parent ID in bims_node_relationship table.
   *
   * @return integer
   */
  public function getParentID() {
    $sql = "SELECT parent_id FROM {bims_node_relationship} WHERE child_id = :child_id";
    return db_query($sql, array(':child_id' => $this->node_id))->fetchField();
  }

  /**
   * Update the parent of this node.
   *
   * 1. updates parent in the tree properties.
   * 2. adds to node_relationship.
   *
   * @param integer $parent_id
   *
   * @return boolean TRUE|FALSE
   */
  public function updateParentID($parent_id) {

    // Gets the current parent ID.
    $cur_parent_id = $this->getParentID();

    // Inserts a new relationship (parent).
    if (!$cur_parent_id){
      $rel = new PUBLIC_BIMS_NODE_RELATIONSHIP(array(
        'parent_id' => $parent_id,
        'child_id'  => $this->node_id,
      ));
      if (!$rel->insert()) {
        return FALSE;
      }
    }

    // Updates the relationship.
    else if ($cur_parent_id != $parent_id) {
      $rel = PUBLIC_BIMS_NODE_RELATIONSHIP::byKey(array(
        'parent_id' => $cur_parent_id,
        'child_id'  => $this->node_id,
      ));
      $rel->setParentID($parent_id);
      if (!$rel->update()) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Returns all nodes in sub-tree.
   *
   * @param boolean $all_type
   *
   * $all_type is TRUE : this returns all nodes in sub-tree.
   * $all_type is FALSE :
   *   If node type is PROGRAM, it returns the following types fo nodes.
   *     SUB_PROGRAM/TRIAL
   *   If node type is SUB_PROGRAM, it returns the following types fo nodes.
   *     TRIAL
   *
   * @return array of BIMS_NODE object.
   */
  public function getSubNodes($regex = '') {

    // Gets BIMS_USER.
    $bims_user = getBIMS_USER();

    // Gets the nodes in sub-tree.
    $sub_nodes = array();
    $this->_getSubNodes($sub_nodes, $regex, $bims_user);
    return $sub_nodes;
  }

  /**
   * Helper function for getSubNodes().
   *
   * @param array $sub_nodes
   * @param string $regex
   * @param BIMS_USER $bims_user
   *
   * @param array $nodes
   */
  protected function _getSubNodes(&$sub_nodes, $regex, BIMS_USER $bims_user) {

    // Gets the properties of the node.
    $node_id   = $this->getNodeID();
    $node_type = $this->getType();

    // Gets the nodes in the sub-tree.
    if ($regex == '' || preg_match("/^($regex)$/i", $node_type)) {

      // Gets BIMS_NODE and adds it to the array.
      $bims_node = BIMS_NODE::getNodebyID($node_id);
      $sub_nodes[$node_id] = $bims_node;
    }

    // Adds node_id of children.
    $children = $this->getChildren('object');
    if ($children) {
      foreach ($children as $child) {
        $child->_getSubNodes($sub_nodes, $regex, $bims_user);
      }
    }
  }

  /**
   * Returns children node in the provided type.
   *
   * @param string $return_type
   *   'integer' : return node_id of all children.
   *   'object'  : return BIMS_NODE object of all children.
   *
   * @return arary of BIMS_NODE objects or integers.
   */
  public function getChildren($return_type = 'integer') {

    // Gets all childrens.
    $sql = "
      SELECT NR.child_id
      FROM {bims_node_relationship} NR
        INNER JOIN {bims_node} N on N.node_id = NR.child_id
      WHERE NR.parent_id = :node_id

    ";
    //ORDER BY N.rank ASC
    $results = db_query($sql, array(':node_id' => $this->node_id));
    $children = array();
    while ($child_id = $results->fetchField()) {
      if ($return_type == 'object') {
        $children []= BIMS_NODE::byID($child_id);
      }
      else {
        $children []= $child_id;
      }
    }
    return $children;
  }

  /**
   * Saves the trial tree.
   *
   * @param array $root_node
   *
   * @return boolean
   */
  public function saveTrialTree($root_node) {

    // DEBUG.
    bims_debug('Processing ' . $root_node['id'] . ':' . $root_node['type'] . ':' . $root_node['text'], 'SAVE_TREE');
    bims_debug($root_node, 'SAVE_TREE');

    $transaction = db_transaction();
    try {
      // Initializes $new_ndoes array.
      $new_nodes = array($root_node['id']);

      // Updates the name of the root node (PROGRAM).
      $this->setName($root_node['text']);

      // Hanldes the children if exists.
      if (!empty($root_node['children'])) {
        if ($GLOBALS['BIMS_DEBUG_SAVE_TREE']) {
          drupal_set_message('Processing child nodes');
        }
        foreach ($root_node['children'] as &$child) {
          $this->_saveTrialTree($child, $root_node, $new_nodes);
        }
      }

      // Removes orphant nodes.
      $this->_remove_orphant($new_nodes);

      // Updates the tree.
      $root_node_json = json_encode($root_node);
      $this->setTrialTree($root_node_json);

      // Finally updates the name and tree.
      $this->update();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Helper function to save the tree.
   *
   * @param array $node
   */
  private function _saveTrialTree(&$node, $parent, &$new_nodes) {

    // DEBUG.
    bims_debug('>' . $node['id'] . ':' . $node['type'] . ':' . $node['text'], 'SAVE_TREE');
    bims_debug($node, 'SAVE_TREE');

    // If the current node exists in the table, updates it.
    $bims_node = NULL;
    if (preg_match("/^(\d+)$/", $node['id'], $matches)) {

      // DEBUG.
      bims_debug('Updating the exisint node', 'SAVE_TREE');
      bims_debug($node, 'SAVE_TREE');

      // Removes the node from the tree if it does not exist in the table.
      $bims_node = BIMS_NODE::byID($node['id']);
      // TODO:
      if (!$bims_node) {
        return;
      }
      $bims_node->setName($node['text']);
      $bims_node->update();
    }
    else {
      $bims_node = $this->_add_new_node($node);

      // DEBUG.
      bims_debug('New node added', 'SAVE_TREE');
      bims_debug($node, 'SAVE_TREE');
    }

    // Updates the parent ID.
    $bims_node->updateParentID($parent['id']);

    // Adds it to the list of new nodes.
    $new_nodes []= $bims_node->getNodeID();

    // If the type of node is
    // 3. SUB_PROGRAM : traverse child nodes.
    // 5. TRIAL       : do nothing.

    if ($node['type'] == 'PROGRAM') {
      $bims_node->saveTrialTree($node);
    }
    else if (preg_match("/^(SUB_PROGRAM)$/", $node['type'])) {

      // DEBUG.
      if ($GLOBALS['BIMS_DEBUG_SAVE_TREE']) {
        drupal_set_message("process children");
      }
      // Hanldes the children if exists.
      if (!empty($node['children'])) {
        foreach ($node['children'] as &$child) {
          $bims_node->_saveTrialTree($child, $node, $new_nodes);
        }
      }
    }
  }

  /**
   * Helper function [savTree()] to remove orphant nodes.
   *
   * @param array $new_nodes
   *
   * @return boolean
   */
  private function _remove_orphant($new_nodes) {

    // Checks $new_nodes array.
    if (empty($new_nodes)) {
      throw new Exception('The array of the new nodes is empty. Something is wrong');
    }

    // Gets all the tree nodes in sub-tree.
    $old_nodes = $this->getSubNodes();

    // DEBUG.
    bims_debug('Removing orphant nodes for ' . $this->node_id . ':' . $this->name, 'SAVE_TREE');
    bims_debug($new_nodes, 'SAVE_TREE');
    bims_debug($old_nodes, 'SAVE_TREE');

    // Removes the node if it is an orphant.
    foreach ($old_nodes as $node_id => $bims_node) {

      // Delete an orphant node.
      if (!in_array($node_id, $new_nodes)) {

        // DEBUG.
        if ($GLOBALS['BIMS_DEBUG_SAVE_TREE']) {
          drupal_set_message("$node_id wii be deleted");
        }

        // Deletes the node.
        if (!$bims_node->delete()) {
          throw new Exception("Error : Failed to delete the node [$node_id]");
        }
      }
    }
    return TRUE;
  }

  /**
   * Helper function [savTree()] to update the node in the table.
   *
   * @param array &$node
   *
   * @return BIMS_NODE
   */
  private function _update_existing_node($node) {
    $bims_node = BIMS_NODE::byID($node['id']);
    if ($bims_node) {
      $bims_node->setName($node['text']);
      if ($bims_node->update()) {
        return $bims_node;
      }
    }
    return NULL;
  }

  /**
   * Helper function [savTree()] to add a new node to the table.
   *
   * @param array &$node
   *
   * @return BIMS_NODE
   */
  private function _add_new_node(&$node) {

    // Adds a new node.
    $bims_node = new BIMS_NODE(array(
      'name'        => $node['text'],
      'type'        => $node['type'],
      'crop_id'     => $this->crop_id,
      'owner_id'    => $this->owner_id,
      'root_id'     => $this->root_id,
      'project_id'  => $this->project_id,
    ));
    if ($bims_node->insert()) {

      // Updates 'id', 'li_attr' and '' fields.
      $node['id']            = $bims_node->getNodeID();
      $node['li_attr']['id'] = $bims_node->getNodeID();
      $node['a_attr']['id']  = $bims_node->getNodeID() . '_anchor';
    }
    else {
      throw new Exception("Error : Failed to add a new node");
    }

    // Unsets the highlight of the selected node.
    $node['state']['selected'] = false;
    return BIMS_NODE::byID($bims_node->getNodeID());
  }

  /**
   * Returns TRUE if this node is PROGRAM.
   *
   * @return boolean
   */
  public function isProgram() {
    return ($this->type == 'PROGRAM') ? TRUE : FALSE;
  }

  /**
   * Returns TRUE if this node is TRIAL.
   *
   * @return boolean
   */
  public function isTrial() {
    return ($this->type == 'TRIAL') ? TRUE : FALSE;
  }

  /**
   * Returns TRUE if this is imported project.
   *
   * @return boolean
   */
  public function isImported() {

    // Checks the type.
    if ($this->type == 'TRIAL') {
      $keys = array(
        'program_id' => $this->root_id,
        'project_id' => $this->project_id,
      );
      $imported_proj = BIMS_IMPORTED_PROJECT::byKey($keys);
      if ($imported_proj) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Returns the created date.
   *
   * @param boolean $date_only
   *
   * @return string
   */
  public function getCreateDate($date_only = FALSE) {
    $date_str = $this->getPropByKey('create_date');
    if ($date_only) {
      $tmp = explode(' ', $date_str);
      return $tmp[0];
    }
    return $date_str;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns BIMS_CROP.
   *
   * @return BIMS_CROP
   */
  public function getCrop() {
    return BIMS_CROP::byKey(array('crop_id' => $this->crop_id));
  }

  /**
   * Returns the value of the given key in prop.
   *
   * @param string $key
   *
   * @return various
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * @param string $key
   * @param string $value
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }

  /**
   * Updates the value of the given key in prop.
   *
   * @param string $key
   * @param string $value
   *
   * @return boolean
   */
  public function updatePropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
    return $this->update();
  }

  /**
   * Retrieves the prop array.
   *
   * @return array
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }
}