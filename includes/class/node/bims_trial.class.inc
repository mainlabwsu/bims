<?php
/**
 * The declaration of BIMS_TRIAL class.
 *
 */
class BIMS_TRIAL extends BIMS_NODE {

 /**
  *  Data members.
  */
  /**
   * @see BIMS_NODE::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);
  }

  /**
   * @see BIMS_NODE::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_NODE::byID()
   */
  public static function byID($id) {
    if (!bims_is_int($id)) {
      return NULL;
    }
    $parent = parent::byID($id);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see BIMS_NODE::byProjectID()
   */
  public static function byProjectID($project_id, $program_id) {
    $keys = array(
      'root_id'     => $program_id,
      'project_id'  => $project_id,
    );
    return self::byKey($keys);
  }

  /**
   * @see BIMS_NODE::insert()
   */
  public function insert() {
    if (parent::insert()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * @see BIMS_NODE::update()
   */
  public function update($new_values = array()) {

    // Updates the trial properties on BIMS_CHADO.
    $bc_project = new BIMS_CHADO_PROJECT($this->root_id);
    $fields = array(
      'name'        => BIMS_PROGRAM::getPrefixByID($this->root_id) . $this->name,
      'description' => $this->description,
    );
    db_update($bc_project->getTableName('project'))
      ->fields($fields)
      ->condition('project_id', $this->project_id, '=')
      ->execute();

    // Updates the program properties.
    if (!parent::update($new_values)) {
      throw new Exception("Error : Failed to update the tree");
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_NODE::delete()
   */
  public function delete() {
    return parent::delete();
  }

  /**
   * Deletes the trial.
   *
   * @return boolean
   */
  public function deleteTrial() {

    $transaction = db_transaction();
    try {

      // Gets the trial information.
      $program_id = $this->getProgramID();
      $type       = strtolower($this->getProjectType());
      $sub_type   = strtolower($this->getProjectSubType());

      // Gets the callback.
      $callbacks = $this->deleteCallbacks($type, $sub_type);

      // Deletes the data in BIMS_CHADO.
      if (!$this->deleteDataBIMS($type, $sub_type)) {
        throw new Exception("Fail to delete the data in BIMS_CHADO");
      }

      // Deletes the trial in BIMS_MVIEW.
      if (!$this->deleteDataMView($this->node_id)) {
        throw new Exception("Fail to delete the data (trial) in BIMS schema");
      }

      // Deletes the trial in public.
      if (!$this->deleteDataPublic($this->node_id)) {
        throw new Exception("Fail to delete the data (trial) in public schema");
      }

      // Deletes the trial in bims_node table.
      if (!$this->delete()) {
        throw new Exception("Fail to delete the trial node");
      }

      // Removes the tree node of the trial.
      $bims_program = BIMS_PROGRAM::byID($program_id);
      $bims_program->deleteTreeNode($this->node_id);

      // Submits the callbacks.
      if (!empty($callbacks)) {
        if (!bims_submit_drush_cmds($program_id, 'delete-trial', $callbacks)) {
          throw new Exception("Error : Failed to submit callbacks");
        }
      }
    }
    catch (\Exception $e) {
      $transaction->rollback();
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets the callbacks called after deletion.
   *
   * @param string $type
   * @param string $sub_type
   *
   * @return array
   */
  public function deleteCallbacks($type, $sub_type) {
    $callbacks = array();

    // Local variables.
    $program_id     = $this->getProgramID();
    $project_id     = $this->getProjectID();
    $stock_id_arr   = array();
    $cross_id_arr   = array();
    $feature_id_arr = array();

    // Phenotype.
    $args = array(':project_id' => $project_id);
    if ($type == 'phenotype') {
      $bims_mview = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
      $mview = $bims_mview->getMView();
      $sql = "
        SELECT DISTINCT stock_id, nd_experiment_id FROM {$mview}
        WHERE project_id = :project_id
      ";
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        $stock_id_arr[$obj->stock_id] = TRUE;
        if ($obj->nd_experiment_id) {
          $cross_id_arr[$obj->nd_experiment_id] = TRUE;
        }
      }
    }

    // SNP.
    else if ($sub_type == 'snp') {

      // BIMS_MVIEW_GENOTYPE.
      $bims_mview = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
      $mview = $bims_mview->getMView();
      $sql = "
        SELECT DISTINCT stock_id, feature_id
        FROM {$mview} WHERE project_id = :project_id
      ";
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        $stock_id_arr[$obj->stock_id]     = TRUE;
        $feature_id_arr[$obj->feature_id] = TRUE;
      }
    }

    // SSR.
    else if ($sub_type == 'ssr') {

      // BIMS_MVIEW_SSR.
      $bims_mview = new BIMS_MVIEW_SSR(array('node_id' => $program_id));
      $mview = $bims_mview->getMView();
      $sql = "
        SELECT DISTINCT stock_id, feature_id
        FROM {$mview} WHERE project_id = :project_id
      ";
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        $stock_id_arr[$obj->stock_id]     = TRUE;
        $feature_id_arr[$obj->feature_id] = TRUE;
      }
    }

    // Adds the callbacks.
    if (!empty($stock_id_arr)) {
      $id_list    = implode(':', array_keys($stock_id_arr));
      $options    = " --group=stock --group_ids=$id_list --mview=stats";
      $callbacks  []= "bims-update-mview-stock $program_id $options";
    }
    if (!empty($cross_id_arr)) {
      $id_list    = implode(':', array_keys($cross_id_arr));
      $options    = " --group=cross --group_ids=$id_list --mview=stats";
      $callbacks  []= "bims-update-mview-cross $program_id $options";
    }
    if (!empty($feature_id_arr)) {
      $id_list    = implode(':', array_keys($feature_id_arr));
      $options    = " --group=feature --group_ids=$id_list --mview=num";
      $callbacks  []= "bims-update-mview-marker $program_id $options";
    }
    return $callbacks;
  }

  /**
   * Deletes the data in BIMS_CHADO.
   *
   * @param string $type
   * @param string $sub_type
   *
   * @return boolean
   */
  public function deleteDataBIMS($type, $sub_type) {

    // Local variables.
    $program_id = $this->getProgramID();
    $project_id = $this->getProjectID();

    // Deletes the trial from BIMS_CHADO_PROJECT.
    $bc_project = new BIMS_CHADO_PROJECT($program_id);
    if ($bc_project) {
      db_delete($bc_project->getTableName('project'))
        ->condition('project_id', $project_id, '=')
        ->execute();
    }

    // Deletes the trial from other BIMS_CHADO tables.
    $args = array(':project_id' => $this->getProjectID());
    if ($this->isChado()) {
      db_delete('bims_imported_project')
        ->condition('project_id', $project_id, '=')
        ->execute();
    }
    else {

      // Deletes from phenotype related tables.
      if ($type == 'phenotype') {

        // Gets BIMS_CHADO.
        $bc_accession     = new BIMS_CHADO_ACCESSION($program_id);
        $bc_pc            = new BIMS_CHADO_PHENOTYPE_CALL($program_id);
        $table_accession  = $bc_accession->getTableName('accession');
        $table_pc         = $bc_pc->getTableName('phenotype_call');

        // Deletes the samples from BIMS_CHADO_ACCESSION.
        $sql = "
          DELETE FROM {$table_accession} WHERE stock_id IN
          (SELECT stock_id FROM {$table_pc} WHERE project_id = :project_id)
        ";
        db_query($sql, $args);

        // Deletes the phenotype from BIMS_CHADO_PHENOTYPE_CALL.
        $sql = "DELETE FROM {$table_pc} WHERE project_id = :project_id";
        db_query($sql, $args);
      }

      // Deletes from SSR related tables.
      else if ($sub_type == 'ssr') {
        $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
        $bc_ne        = new BIMS_CHADO_ND_EXPERIMENT($program_id);
        $table_a      = $bc_accession->getTableName('accession');
        $table_a_rel  = $bc_accession->getTableName('accessionrel');
        $table_ne     = $bc_ne->getTableName('nd_experiment');

        // Deletes the samples.
        $sql = "
          DELETE FROM {$table_a} WHERE stock_id IN (
            SELECT NE.stock_id
            FROM {$table_ne} NE
            WHERE NE.project_id = :project_id
          )
        ";
        db_query($sql, $args);

        // Deletes the ND experiments.
        $sql = "DELETE FROM {$table_ne} WHERE project_id = :project_id";
        db_query($sql, $args);
      }

      // Deletes from Haplotype related tables.
      else if ($sub_type == 'haplotype') {

        // Gets BIMS_CHADO.
        $bc_nde     = new BIMS_CHADO_ND_EXPERIMENT($program_id);
        $table_nde  = $bc_nde->getTableName('nd_experiment');

        // Deletes from BIMS_CHADO_ND_EXPERIMENT.
        $sql = "DELETE FROM {$table_nde} WHERE project_id = :project_id";
        db_query($sql, $args);
      }

      // Deletes from SNP related tables.
      else if ($sub_type == 'snp') {

        // Gets BIMS_CHADO.
        $bc_gc    = new BIMS_CHADO_GENOTYPE_CALL($program_id);
        $table_gc = $bc_gc->getTableName('genotype_call');

        // Deletes the SNP from BIMS_CHADO_GENOTYPE_CALL.
        $sql = "DELETE FROM {$table_gc} WHERE project_id = :project_id";
        db_query($sql, $args);
      }

      // Deletes from cross related tables.
      else if ($type == 'cross') {

        // Gets BIMS_CHADO.
        $bc_cross     = new BIMS_CHADO_CROSS($program_id);
        $table_cross  = $bc_gc->getTableName('cross');

        // Deletes the cross from BIMS_CHADO_CROSS.
        $sql = "DELETE FROM {$table_cross} WHERE project_id = :project_id";
        db_query($sql, $args);
      }
    }
    return TRUE;
  }

  /**
   * Deletes the data in mview.
   *
   * @param integer $node_id
   *
   * @return boolean
   */
  public function deleteDataMView($node_id) {

    // Deletes the data.
    $fkeys_delete = array(
      'BIMS_MVIEW_PHENOTYPE'          => array('node_id'),
      'BIMS_MVIEW_GENOTYPE'           => array('node_id'),
      'BIMS_MVIEW_GENOTYPE_MARKER'    => array('node_id'),
      'BIMS_MVIEW_GENOTYPE_ACCESSION' => array('node_id'),
      'BIMS_MVIEW_SSR'                => array('node_id'),
      'BIMS_MVIEW_HAPLOTYPE'          => array('node_id'),
    );
    return bims_delete_data_mview($this->root_id, array($node_id), $fkeys_delete);
  }

  /**
   * Deletes the data in public.
   *
   * @param integer $node_id
   *
   * @return boolean
   */
  public function deleteDataPublic($node_id) {

    // Gets conditions.
    $conds = array(
      'bims_node'                   => array('node_id' => $node_id),
      'bims_mview_phenotype_stats'  => array('node_id' => $node_id),
      'bims_mview_stock_stats'      => array('node_id' => $node_id),
      'bims_mview_cross_stats'      => array('node_id' => $node_id),
      'bims_node_relationship'      => array(
        array('parent_id' => $node_id),
        array('child_id' => $node_id),
      ),
    );
    return bims_delete_data_public($conds);
  }

  /**
   * @see BIMS_NODE::getAssocData()
   */
  public function getAssocData() {

    // Initializes the data counts.
    $data_counts = array();

    // Counts the data in BIMS_CHADO.
    $data_count = bims_count_data_bc('trial', $this->project_id, $this->root_id);
    if (!empty($data_count)) {
      $data_counts['bims_chado'] = $data_count;
    }

    // Counts the data in mviews.
    $data_count = bims_count_data_mview('trial', $this->node_id, $this->root_id);
    if (!empty($data_count)) {
      $data_counts['mview'] = $data_count;
    }

    // Counts the data in public schema.
    $data_count = bims_count_data_public('trial', $this->node_id);
    if (!empty($data_count)) {
      $data_counts['public'] = $data_count;
    }
    return $data_counts;
  }

  /**
   * Deletes TRIAL node and chado.project.
   *
   * @return boolean
   */
  public function deleteAll() {
    if ($this->delete()) {
      $project = CHADO_PROJECT::byKey(array('project_id' => $this->project_id));
      return $project->delete();
    }
    return FALSE;
  }

  /**
   * Returns the mview by the type.
   *
   * @param string $type
   *
   * @return BIMS_MVIEW_PHENOTYPE
   */
  public function getMView($type) {
    $details = array(
      'project_id'  => $this->project_id,
      'node_id'     => $this->node_id,
    );
    return new BIMS_MVIEW_PHENOTYPE($details);
  }

  /**
   * Returns if this is Chado project.
   *
   * @retrun boolean
   */
  public function isChado() {
    return BIMS_IMPORTED_PROJECT::isChado($this->project_id);
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the cv ID.
   *
   * @param string $type
   *
   * @return integer
   */
  public function getCvID($type) {
    $bims_program = BIMS_PROGRAM::byID($this->root_id);
    return $bims_program->getCvID($type);
  }
}