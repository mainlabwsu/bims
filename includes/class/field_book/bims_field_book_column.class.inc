<?php
/**
 * The declaration of BIMS_FIELD_BOOK_COLUMN class.
 *
 */
class BIMS_FIELD_BOOK_COLUMN {

 /**
  *  Class data members.
  */
  private static $column_types  = array('trait', 'accessionprop', 'sampleprop');

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {}

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Checks if the provided type is valid.
   *
   * @param string $type
   *
   * @return boolean
   */
  public static function isValidType($type) {
    return in_array($type, self::$column_types);
  }
}