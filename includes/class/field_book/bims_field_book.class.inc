<?php
/**
 * The declaration of BIMS_FIELD_BOOK class.
 *
 */
class BIMS_FIELD_BOOK {

  /**
   * Class data members.
   */
  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {}

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Returns cvterms of the provided cv name.
   *
   * @param string $options
   * @param boolean $options
   *
   * @return array
   */
  public static function getOntologies($cv_name, $options = FALSE) {
    $cv = MCL_CHADO_CV::getCv($cv_name);
    if ($cv) {
      $sql = "
        SELECT C.*
          FROM {chado.cvterm} C
            INNER JOIN {chado.cv} CV on CV.cv_id = C.cv_id
        WHERE CV.name = :cv_name
        ORDER BY C.name
      ";
      $results = db_query($sql, array(':cv_name' => $cv_name));
      $formats = array();
      while ($obj = $results->fetchObject()) {
        if ($options) {
          $formats [$obj->name]= $obj->name;
        }
        else {
          $formats []= $obj->name;
        }
      }
      return $formats;
    }
    return array();
  }

  /**
   * Returns all formats in array for options.
   *
   * @param boolean $options
   *
   * @return array
   */
  public static function getFormats($options = FALSE) {
    $sql = "
      SELECT C.*
        FROM {chado.cvterm} C
          INNER JOIN {chado.cv} CV on CV.cv_id = C.cv_id
      WHERE CV.name = :cv_name
      ORDER BY C.name
    ";
    $results = db_query($sql, array(':cv_name' => 'BIMS_FIELD_BOOK_FORMAT'));
    $formats = array();
    while ($obj = $results->fetchObject()) {
      if ($options) {
        $formats [$obj->name]= $obj->name;
      }
      else {
        $formats []= $obj->name;
      }
    }
    return $formats;
  }

  /**
   * Returns statisticalble formats in array.
   *
   * @return array
   */
  public static function getStatsFormats() {
    return array('numeric', 'percent', 'categorical', 'multicat', 'counter', 'boolean', 'date');
  }

  /**
   * Returns numeric formats in array.
   *
   * @return array
   */
  public static function getNumericFormats() {
    return array('numeric', 'counter', 'percent');
  }

  /**
   * Returns data point formats in array.
   *
   * @return array
   */
  public static function getDataPointFormats() {
    return array('audio', 'photo', 'text', 'location');
  }

  /**
   * Checks an input file for Field Book by cross.
   *
   * @param BIMS_USER $bims_user
   * @param object $drupal_file
   *
   * @return string
   */
  public static function checkFieldFileByCross(BIMS_USER $bims_user, $drupal_file, $prefix = '') {

    // Opens the the uploaded file.
    $filepath = drupal_realpath($drupal_file->uri);
    if (!($fdr = fopen($filepath, 'r'))) {
      return "Cannot open file ($filepath).";
    }

    // Checks crosses if they exist.
    $program_id = $bims_user->getProgramID();
    $bc_cross = new BIMS_CHADO_CROSS($program_id);
    $err_msg = '';
    while (!feof($fdr)) {
      $line = fgetcsv($fdr);

      // Gets the cross_number.
      $cross_number = trim($line[0]);

      // Checks the cross number.
      $cross = $bc_cross->byCrossNumber($cross_number);
      if (!$cross) {
        //$err_msg .= "$cross_number does not exist.";
      }
    }
    fclose($fdr);
    return $err_msg;
  }

  /**
   * Generates an input file for Field Book by a list (mew trial).
   *
   * @param BIMS_USER $bims_user
   * @param array $param
   *
   * @return string (error)
   */
  public static function generateFieldFileByListNew(BIMS_USER $bims_user, $param) {

    // Gets values from the parameters.
    $program_id       = $param['program_id'];
    $list_id          = $param['list_id'];
    $fb_req_cols      = $param['fb_req_cols'];
    $prop_cols        = $param['prop_cols'];
    $save_file        = $param['save_file'];

    // Gets BIMS_LIST.
    $bims_list = BIMS_LIST::byID($list_id);

    // Creates a field file in temp dir.
    $filename = 'field-' . date("Gis-Y-m-d") . '.csv';
    $tmp_file = bims_get_config_setting('bims_tmp_dir') . "/$filename";
    if (!($fdw = fopen($tmp_file, 'w'))) {
      return "Cannot open file ($filename) for writing.";
    }

    // Adds the headers.
    $headers = array($fb_req_cols['accession'], $fb_req_cols['unique_id'], $fb_req_cols['primary_order'], $fb_req_cols['secondary_order']);
    foreach ((array)$prop_cols as $field => $label) {
      $headers []= $label;
    }
    fputcsv($fdw, $headers);

    // Gets the stocks.
    $bm_stock  = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
    $m_stock   = $bm_stock->getMView();
    $filter    = $bims_list->getPropByKey('filter');

    // Adds the contents.
    $key = $filter['key'];
    $sql = '';
    if ($key == 'sample_id') {
      $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
      $m_phenotype  = $bm_phenotype->getMView();
      $sub_sql      = "SELECT MVP.stock_id FROM {$m_phenotype} MVP WHERE MVP.sample_id IN (" . implode(',', $filter['items']) . ')';
      $sql          = "SELECT MV.* FROM {$m_stock} MV WHERE MV.stock_id IN ($sub_sql)";
    }
    else {
      $sql = "SELECT MV.* FROM {$m_stock} MV WHERE MV.stock_id IN (" . implode(',', $filter['items']) . '';
    }
    $results = db_query("$sql ORDER BY MV.name");
    while ($obj = $results->fetchObject()) {
      $row = array($obj->name, '', '', '');
      foreach ((array)$prop_cols as $field => $label) {
        $row []= $obj->{$field};
      }
      fputcsv($fdw, $row);
    }
    fclose($fdw);

    // Creates and saves the field file in the user space.
    $filepath = $bims_user->getPath('field', TRUE) . "/$filename";
    $data     = file_get_contents($tmp_file);
    $file     = file_save_data($data, $filepath);

    // Saves the field file in BIMS.
    if ($save_file) {

      // Adds a new file.
      $details = array(
        'filename'    => $filename,
        'filepath'    => $filepath,
        'type'        => 'FBF',
        'filesize'    => filesize($filepath),
        'user_id'     => $bims_user->getUserID(),
        'program_id'  => $program_id,
        'description' => 'Field file for Field Book',
        'submit_date' => date("Y-m-d G:i:s"),
        'prop'        => json_encode(array()),
      );
      $bims_file = new BIMS_FILE($details);
      //if (!$bims_file->insert()) {
      //  return 'Error : Failed to add the field file to BIMS.';
      //}
    }

    // Redirects to the download link.
    $url = "bims/download_file/" . $file->fid;
    bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
    return '';
  }

  /**
   * Generates an input file for Field Book by a list (Existing trial).
   *
   * @param BIMS_USER $bims_user
   * @param array $param
   *
   * @return string (error)
   */
  public static function generateFieldFileByListExisting(BIMS_USER $bims_user, $param) {

    // Gets values from the parameters.
    $program_id   = $param['program_id'];
    $node_id      = $param['trial_id'];
    $prop_cols    = $param['prop_cols'];
    $save_file    = $param['save_file'];

    // Creates a field file in temp dir.
    $filename = 'field-' . date("Gis-Y-m-d") . '.csv';
    $tmp_file = bims_get_config_setting('bims_tmp_dir') . "/$filename";
    if (!($fdw = fopen($tmp_file, 'w'))) {
      return "Cannot open file ($filename) for writing.";
    }

    // Adds the headers.
    $headers = array();
    foreach ((array)$prop_cols as $field => $label) {
      $headers []= $label;
    }
    fputcsv($fdw, $headers);

    // Gets the samples.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' =>$program_id));
    $m_phenotype  = $bm_phenotype->getMView();
    $sql = "
      SELECT MV.* FROM {$m_phenotype} MV
      WHERE MV.node_id = :node_id
      ORDER BY MV.unique_id
    ";
    $results = db_query($sql, array(':node_id' => $node_id));
    while ($obj = $results->fetchObject()) {
      $row = array();
      foreach ((array)$prop_cols as $field => $label) {
        $row []= $obj->{$field};
      }
      fputcsv($fdw, $row);
    }
    fclose($fdw);

    // Creates and saves the field file in the user space.
    $filepath = $bims_user->getPath('field', TRUE) . "/$filename";
    $data     = file_get_contents($tmp_file);
    $file     = file_save_data($data, $filepath);

    // Saves the field file in BIMS.
    if ($save_file) {

      // Adds a new file.
      $details = array(
        'filename'    => $filename,
        'filepath'    => $filepath,
        'type'        => 'FBF',
        'filesize'    => filesize($filepath),
        'user_id'     => $bims_user->getUserID(),
        'program_id'  => $program_id,
        'description' => 'Field file for Field Book',
        'submit_date' => date("Y-m-d G:i:s"),
        'prop'        => json_encode(array()),
      );
      $bims_file = new BIMS_FILE($details);
      if (!$bims_file->insert()) {
        return 'Error : Failed to add the field file to BIMS.';
      }
    }

    // Redirects to the download link.
    $url = "bims/download_file/" . $file->fid;
    bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
    return '';
  }

  /**
   * Generates an input file for Field Book by cross.
   *
   * @param BIMS_USER $bims_user
   * @param array $param
   *
   * @return string (error)
   */
  public static function generateFieldFileByCross(BIMS_USER $bims_user, $param) {

    // Gets values from the parameters.
    $drupal_file      = $param['drupal_file'];
    $cross_project_id = $param['cross_project_id'];
    $prefix           = $param['prefix'];
    $add_progeny      = $param['add_progeny'];
    $fb_req_cols      = $param['fb_req_cols'];
    $save_file        = $param['save_file'];

    // Creates a field file in temp dir.
    $filename = 'field-' . date("Gis-Y-m-d") . '.csv';
    $tmp_file = bims_get_config_setting('bims_tmp_dir') . "/$filename";
    if (!($fdw = fopen($tmp_file, 'w'))) {
      return "Cannot open file ($filename) for writing.";
    }

    // Adds the headers.
    $headers = array($fb_req_cols['accession'], $fb_req_cols['unique_id'], $fb_req_cols['primary_order'], $fb_req_cols['secondary_order']);
    fputcsv($fdw, $headers);

    // Opens the the uploaded file.
    $filepath = drupal_realpath($drupal_file->uri);
    if (!($fdr = fopen($filepath, 'r'))) {
      return "Cannot open file ($filepath).";
    }
    $counter = array();
    while (!feof($fdr)) {
      $line = fgetcsv($fdr);

      // Read the line and parse it.
      $cross  = trim($line[0]);
      $row    = trim($line[1]);
      $tree   = trim($line[2]);

      // Initializes the counter.
      if (!array_key_exists($cross, $counter)) {
        $counter[$cross] = 1;
      }

      // Parses out the last column.
      $segments = explode(',', $tree);
      foreach ((array)$segments as $segment) {

        // Checks the segment.
        if (preg_match("/^(\d+)$/", $segment, $matches)) {
          $single = $matches[1];
          $acc = sprintf("%s-%03d", $cross, $counter[$cross]++);
          $uid = ($prefix) ? "$prefix-$acc" : $acc;
          fputcsv($fdw, array($acc, $uid, $row, $single));
        }
        else if (preg_match("/^(\d+)[-_](\d+)$/", $segment, $matches)) {
          $start = $matches[1];
          $end   = $matches[2];
          for ($i = $start; $i <= $end; $i++) {
            $acc = sprintf("%s-%03d", $cross, $counter[$cross]++);
            $uid = ($prefix) ? "$prefix-$acc" : $acc;
            fputcsv($fdw, array($acc, $uid, $row, $i));
          }
        }
      }
    }
    fclose($fdr);
    fclose($fdw);

    // Creates and saves the field file in the user space.
    $filepath = $bims_user->getPath('field', TRUE) . "/$filename";
    $data     = file_get_contents($tmp_file);
    $file     = file_save_data($data, $filepath);
    $file_id  = $file->fid;

    // Saves the field file in BIMS.
    if ($save_file) {

      // Adds a new file.
      $details = array(
        'filename'    => $filename,
        'filepath'    => $filepath,
        'type'        => 'FBF',
        'filesize'    => filesize($filepath),
        'user_id'     => $bims_user->getUserID(),
        'program_id'  => $bims_user->getProgramID(),
        'description' => 'Field file for Field Book generated by a Cross file',
        'submit_date' => date("Y-m-d G:i:s"),
        'prop'        => json_encode(array('cross_project_id' => $cross_project_id)),
      );
      $bims_file = new BIMS_FILE($details);
      if (!$bims_file->insert()) {
        return 'Error : Failed to add the field file to BIMS.';
      }
    }

    // Adds the progenies to the database.
    if ($add_progeny) {

      // Adds progenies by drush command.
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd = "$drush bims-add-progeny $file_id > /dev/null 2>/dev/null  & echo $!";
      $pid = exec($cmd, $output, $return_var);
      if ($return_var) {
        return "Error : Failed to submit a job for adding progenies";
      }
    }

    // Redirects to the download link.
    $url = "bims/download_file/$file_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
    return '';
  }

  /**
   * Generates an input file for Field Book.
   *
   * @param BIMS_FILE $bims_file
   *
   * @return boolean
   */
  public function createInput(BIMS_FILE $bims_file) {

    // Checks and gets BIMS_USER.
    $bims_user = BIMS_USER::byID($bims_file->getUserID());
    if (!$bims_user) {
      return FALSE;
    }

    // Creates an input file.
    $no_error_flag = FALSE;
    $type = $bims_file->getType();
    if ($type == 'FBT') {
      $no_error_flag = $this->_createInputTrait($bims_file, $bims_user);
    }
    else if ($type == 'FBF') {
      $no_error_flag = $this->_createInputField($bims_file, $bims_user);
    }

    // Updates the status of the file and filesize.
    $status = array(
      'label'   => 'failed',
      'numeric' => -200,
    );
    if ($no_error_flag) {
      $status['label']  = 'completed';
      $status['numeric'] = 100;
    }
    $bims_file->setPropByKey('status', $status);
    $bims_file->setFilesize(filesize($bims_file->getFilepath()));

    if (!$bims_file->update()) {
      return FALSE;
    }
    return $no_error_flag;
  }

  /**
   * Creates an input file for trait.
   *
   * @param BIMS_FILE $bims_file
   * @param BIMS_USER $bims_user
   *
   * @return boolean
   */
  private function _createInputTrait(BIMS_FILE $bims_file, BIMS_USER $bims_user) {}

  /**
   * Creates an input file for field.
   *
   * @param BIMS_FILE $bims_file
   * @param BIMS_USER $bims_user
   *
   * @return boolean
   */
  private function _createInputField(BIMS_FILE $bims_file, BIMS_USER $bims_user) {

    // Gets the properties.
    $prop_arr = $bims_file->getPropArr();

    // Gets the type of field file generated from.
    $type = $prop_arr['type'];

    // Genereates the input field file.
    $contents = '';
    if ($type == 'new') {
      $contents = $this->_createInputFieldNew($bims_file);
    }
    else if ($type == 'existing') {
      $contents = $this->_createInputFieldExisting($bims_file);
     }

    // Gets the filepath and add the contents.
    $filepath = $bims_user->getPath('field') . '/' . $bims_file->getFilename();
    file_put_contents($filepath, $contents);
    return TRUE;
  }

  /**
   * Creates an input file for field for the new trial.
   *
   * @param BIMS_FILE $bims_file
   *
   * @return boolean
   */
  private function _createInputFieldNew(BIMS_FILE $bims_file) {

    // Gets the properties.
    $prop_arr = $bims_file->getPropArr();

    // Gets BIMS_PROGRAM and BIMS_TRIAL.
    $trial_id      = $prop_arr['bims_cols']['trial_cols']['trial_list'];
    $bims_trial   = BIMS_TRIAL::byID($trial_id);
    $bims_program = $bims_trial->getProgram();
    $program_id   = $bims_program->getProgramID();

    // Checks and gets the list of stocks.
    $list_id = $prop_arr['bims_cols']['list_cols']['list_id'];
    $bims_list = BIMS_LIST::byID($list_id);
    if (!$bims_list) {
      return '';
    }

    // Initializes the headers.
    $headers  = array();

    // Adds the BIMS columns.
    $bims_cols                  = $bims_program->getBIMSCols();
    $headers['accession']       = $bims_cols['accession'];
    $headers['unique_id']       = $bims_cols['unique_id'];
    $headers['primary_order']   = $bims_cols['primary_order'];
    $headers['secondary_order'] = $bims_cols['secondary_order'];
    $headers['dataset_name']    = 'trial';

    // Adds location columns.
    $location = '';
    if ($prop_arr['bims_cols']['loc_list']) {
      $bm_location = new BIMS_MVIEW_LOCATION(array('node_id' => $bims_file->getProgramID()));
      $location = $bm_location->getLocation($prop_arr['bims_cols']['loc_list']);
      if ($location) {
        $headers []= 'location';
        $location = $location->name;
      }
    }

    // Add Stock columns.
    if (array_key_exists('stock', $prop_arr['custom_cols'])) {
      foreach ($prop_arr['custom_cols']['stock']['ids'] as $id => $dummy) {
        $headers[$id] = $id;
      }
    }

    // Writes the headers.
    $format = '%s' . str_repeat(',%s', sizeof(array_keys($headers)) - 1);
    $header_str = '"' . implode('","', array_values($headers)) . '"' . "\n";

    // Gest the BIMS columns variables.
    $unique_id  = $bims_cols['unique_id'];
    $primary    = $bims_cols['primary_order'];
    $secondary  = $bims_cols['secondary_order'];

    // Gets the list from the filter.
    $filter     = $bims_list->getPropByKey('filter');
    $list_type  = $filter['list_type'];

    // Sets the stocks.
    $stocks = array();
    if ($list_type == 'stock') {
      $stocks = $filter['items'];
    }
    else if ($list_type == 'cross') {
      $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
      foreach ((array)$filter['items'] as $nd_experiment_id) {
        $cross = $bm_cross->getCross($nd_experiment_id);
        $arr = json_decode($cross->progeny, TRUE);
        foreach ((array)$arr as $stock_id => $name) {
          $tmp[$stock_id] = $name;
        }
      }
      $stocks = array_keys($tmp);
    }

    // Writes the body of the field file.
    $rows = array();
    foreach ((array)$stocks as $stock_id) {

      // Populates the field file.
      $bims_mview_stock = new BIMS_MVIEW_STOCK(array('node_id' =>  $bims_file->getProgramID()));
      $stock_arr = $bims_mview_stock->getStock($stock_id, BIMS_ASSOC);
      $accession = $stock_arr['name'];

      // Gets the experimetal design.
      $primary_order = '';
      $secondary_order = '';

      // Sets the Field Book required columns.
      $unique = "$program_id-$trial_id-$stock_id";
      if ($primary_order) {
        $unique .= "-$primary_order";
      }
      if ($secondary_order) {
        $unique .= "-$secondary_order";
      }
      $row = array(
        $unique,
        $primary_order,
        $secondary_order,
        $accession,
        $bims_trial->getName(),
      );

      // Adds location columns.
      if ($location) {
        $row []= $location;
      }

      // Adds Stock columns.
      foreach ($prop_arr['custom_cols']['stock']['ids'] as $id => $dummy) {
        $row []= $stock_arr[$id];
      }
      $rows []= $row;
    }

    // Sort the rows by stock name.
    usort($rows, function($a, $b) {
      return strnatcmp($a[3], $b[3]);
    });

    // Writes all columns.
    $lines = '';
    foreach ($rows as $row) {
      $line = '"' . implode('","', $row) . '"';
      $lines .= $line . "\n";
    }
    return $header_str . $lines;
  }

  /**
   * Creates an input file for field for the existing trial.
   *
   * @param BIMS_FILE $bims_file
   *
   * @return boolean
   */
  private function _createInputFieldExisting(BIMS_FILE $bims_file) {

    // Gets the properties.
    $prop_arr = $bims_file->getPropArr();

    // Gets BIMS_PROGRAM and BIMS_TRIAL.
    $node_id      = $prop_arr['bims_cols']['trial_cols']['trial_list'];
    $bims_trial   = BIMS_TRIAL::byID($node_id);
    $bims_program = $bims_trial->getProgram();

    // Initializes the headers.
    $headers  = array();

    // Adds the BIMS columns.
    $bims_cols                  = $bims_program->getBIMSCols();
    $headers['accession']       = $bims_cols['accession'];
    $headers['unique_id']       = $bims_cols['unique_id'];
    $headers['primary_order']   = $bims_cols['primary_order'];
    $headers['secondary_order'] = $bims_cols['secondary_order'];

    // Adds BISM columns.
    $headers['sample']        = 'sample';
    $headers['dataset_name']  = 'dataset_name';

    // Add Stock columns.
    if (array_key_exists('stock', $prop_arr['custom_cols'])) {
      foreach ($prop_arr['custom_cols']['stock']['ids'] as $id => $dummy) {
        if (preg_match("/^\d+$/", $id)) {
          $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
          if ($cvterm) {
            $headers[$cvterm->getName()] = $cvterm->getName();
          }
        }
        else {
          $headers[$id] = $id;
        }
      }
    }

    // Add Sample columns.
    if (array_key_exists('sample', $prop_arr['custom_cols'])) {
      foreach ($prop_arr['custom_cols']['sample']['ids'] as $cvterm_id => $dummy) {
        $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
        if ($cvterm) {
          $headers["p$cvterm_id"] = $cvterm->getName();
        }
      }
    }

    // Add Trait columns.
    if (array_key_exists('trait', $prop_arr['custom_cols'])) {
      foreach ($prop_arr['custom_cols']['trait']['ids'] as $cvterm_id => $dummy) {
        $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
        if ($cvterm) {
          $headers["t$cvterm_id"] = $cvterm->getName();
        }
      }
    }

    // Gets BIMS_TRIAL and BIMS_MVIEW_PHENOTYPE.
    $node_id      = $prop_arr['bims_cols']['trial_cols']['trial_list'];
    $bims_trial   = BIMS_TRIAL::byID($node_id);
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $bims_trial->getRootID()));
    $m_phenotype  = $bm_phenotype->getMView();

    // Gets all samples.
    $sql = "SELECT MV.* FROM {$m_phenotype} MV ORDER BY MV.accession";
    $results = db_query($sql);
    $lines = '';
    while ($arr = $results->fetchAssoc()) {

      // Updates the headers and the format strings.
      if ($line == '') {
        if ($arr['unique_id'] == '') {
          unset($headers['unique_id']);
          unset($headers['primary_order']);
          unset($headers['secondary_order']);
        }
        else {
          unset($headers['sample']);
        }
        $format = '%s' . str_repeat(',%s', sizeof(array_keys($headers)) - 1);
        $header_str = '"' . implode('","', array_values($headers)) . '"' . "\n";
      }

      $vars = array();
      foreach ($headers as $header => $label) {
        $value = '';
        if (array_key_exists($header, $arr)) {
          $value = $arr[$header];
        }
        $vars []= $value;
      }
      $line = '"' . implode('","', $vars) . '"';
      $lines .= $line . "\n";
    }
    return $header_str . $lines;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the types.
   *
   * @return array
   */
  public function getTypes() {
    return $this->types;
  }
}