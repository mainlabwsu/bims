<?php
/**
 * The declaration of BIMS_EVENT class.
 *
 */
class BIMS_EVENT extends PUBLIC_BIMS_EVENT {

  /**
   *  Data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_EVENT::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_EVENT::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see PUBLIC_BIMS_EVENT::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_EVENT::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_EVENT::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }


  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the prop array.
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}