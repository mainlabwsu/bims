<?php
$lib_path = 'sites/all/libraries';
require_once("$lib_path/PHPExcel/PHPExcel.php");
require_once("$lib_path/PHPExcel/PHPExcel/Writer/Excel2007.php");
require_once("$lib_path/PHPExcel/PHPExcel/IOFactory.php");

/**
 * The declaration of BIMS_EXCEL class.
 *
 */
class BIMS_EXCEL {

  /**
   * Class data members.
   */
  private $excel          = NULL;
  private $creator        = NULL;
  private $filename       = NULL;
  private $filepath       = NULL;
  private $title          = NULL;
  private $subject        = NULL;
  private $keywords       = NULL;
  private $category       = NULL;
  private $verison        = NULL;
  private $max_rows       = NULL;
  private $max_cols       = NULL;
  private $description    = NULL;
  private $lastModifiedBy = NULL;

  /*
   * Version
   *
   * This version of class supports ('Excel2007' and 'Excel5')
   */
  private $version = NULL;

  /*
   *  Sheets = array(
   *   'name' => 'sheet name',
   *   'sql'  => 'SELECT ....',
   *   'headers' => array(
   *      'A' => array('heading' => 'Accession', 'field' => 'uniquename'),
   *      'B' => array('heading' => 'Location', 'field' => 'description'),
   *      'C' => array('heading' => 'Parent', 'field' => 'stock_id', 'type' => 'link', 'url' => 'bims/accession/'),
   *    ),
   *  )
   */
  private $sheets = NULL;

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    $this->creator        = array_key_exists('creator', $details)         ? $details['creator']         : 'BIMS';
    $this->filepath       = array_key_exists('filepath', $details)        ? $details['filepath']        : '';
    $this->filename       = array_key_exists('filename', $details)        ? $details['filename']        : '';
    $this->title          = array_key_exists('title', $details)           ? $details['title']           : 'NEW TITLE';
    $this->subject        = array_key_exists('subject ', $details)        ? $details['subject ']        : '';
    $this->keywords       = array_key_exists('keywords ', $details)       ? $details['keywords ']       : '';
    $this->category       = array_key_exists('category', $details)        ? $details['category']        : '';
    $this->sheets         = array_key_exists('sheets', $details)          ? $details['sheets']          : array();
    $this->lastModifiedBy = array_key_exists('lastModifiedBy', $details)  ? $details['lastModifiedBy']  : 'MAIN LAB';

    // Sets the properties of the Excel library.
    $this->version  = bims_get_config_setting('bims_excel_version');
    $this->max_rows = bims_get_config_setting('bims_excel_max_rows');
    $this->max_cols = bims_get_config_setting('bims_excel_max_cols');

    // Updates filename.
    if ($this->filepath && !$this->filename) {
      $this->filename = basename($this->filepath);
    }

    // Gets the default precision.
    $def_precision = ini_get('precision');

    // Creates new PHPExcel object.
    $this->excel = new PHPExcel();

    // Re-set the precision. It seems that PHPExcel change the precision.
    ini_set('precision', $def_precision);

    // Sets the properties of an Excel file.
    $this->setExcelProperties();
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Creates an Excel file from the sheets.
   *
   * @param string $type
   *
   * @return string filepath
   */
  public function create($type) {

    // Creates Excel sheets.
    $csv_files = array();
    $num_sheets = 0;
    for ($idx = 0; $idx < sizeof($this->sheets); $idx++) {
      $sheet    = $this->sheets[$idx];
      $name     = $sheet['name'];
      $headers  = $sheet['headers'];
      $data     = $sheet['data'];

      // Gets the number of the rows.
      $num_rows = sizeof($data);
      if ($num_rows > $this->max_rows) {
        $csv_files[strtolower($name)] = array('sheet' => $sheet);
        unset($this->sheets[$idx]);
        continue;
      }

      // Adds a new worksheet.
      if ($num_sheets) {
        $this->excel->createSheet();
      }

      // Creates an Excel sheet.
      $this->excel->setActiveSheetIndex($num_sheets);

      // Sets sheet name.
      $this->excel->getActiveSheet()->setTitle($sheet['name']);

      // Sets the styles of an Excel sheet.
      $this->setExcelStyle($headers);

      // Writes the headers.
      foreach ($headers as $col_chr => $arr) {
        $this->excel->getActiveSheet()->setCellValue($col_chr.'1', $arr['heading']);
      }

      // Writes the data.
      $row_no = 2;
      foreach ($data as $row) {

        // Writes the value on the cell.
        foreach ($headers as $col_chr => $prop) {
          $value = $row[$col_chr];
          $this->excel->getActiveSheet()->setCellValue($col_chr.$row_no, $value);
        }
        $row_no++;
      }
      $num_sheets++;
    }

    // If $csv_files is not empty, it needs to create CSV files and
    // zip them with the excel file if an excel file exist.
    $is_zip = FALSE;
    $tmp_dir = '';
    if (!empty($csv_files)) {
      $tmp_dir = bims_create_tmp_dir();
      foreach ($csv_files as $name => $info) {
        $sheet    = $info['sheet'];
        $headers  = $sheet['headers'];
        $data     = $sheet['data'];

        // Creates a CSV file.
        $csv_filepath = "$tmp_dir/$name.csv";
        $csv_files[$name]['filepath'] = $csv_filepath;
        $fdw = fopen($csv_filepath, 'w');

        // Adds the template name.
        fputcsv($fdw, array("#$name"));

        // Adds the headers.
        $headings = array();
        foreach ($headers as $col_chr => $prop) {
          $headings []= $prop['heading'];
        }
        fputcsv($fdw, $headings);

        // Adds the contents.
        foreach ($data as $row) {
          $data_line_arr = array();
          foreach ($headers as $col_chr => $prop) {
            $data_line_arr []= $row[$col_chr];
          }
          fputcsv($fdw, $data_line_arr);
        }
        fclose($fdw);
      }
      $is_zip = TRUE;
    }

    // Prepare for downloading the file.
    if ($type == 'ONLINE' && !$is_zip) {

      // Redirects output to a client web browser.
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="' . $this->filename . '"');
      header('Cache-Control: max-age=0');
      $this->excel->setActiveSheetIndex(0);
      $objWriter = PHPExcel_IOFactory::createWriter($this->excel, $this->version);
      ob_end_clean();
      $objWriter->save('php://output');
      exit;
    }
    else {

      // Zips the CSV and excel file.
      if ($is_zip) {
        if ($type == 'ONLINE') {

          // Updates the name and path of the file.
          $excel_filepath = "$tmp_dir/" . $this->filename;
          $zip_filepath   = bims_change_ext($excel_filepath, 'zip');
          $zip_filename   = basename($zip_filepath);

          // Creates a zip file.
          $zip  = new ZipArchive;
          $res  = $zip->open($zip_filepath, ZipArchive::OVERWRITE|ZipArchive::CREATE);

          // Adds CSV files.
          foreach ($csv_files as $name => $info) {
            $filepath = $info['filepath'];
            $zip->addFile($filepath, basename($filepath));
          }

          // Adds the Excel file.
          if ($num_sheets) {
            $this->excel->setActiveSheetIndex(0);
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, $this->version);
            $objWriter->save($excel_filepath);
            $zip->addFile($excel_filepath, basename($excel_filepath));
          }
          $zip->close();

          // Attaches the zip file.
          header("Content-Type: application/zip");
          header("Content-Disposition: attachment; filename=$zip_filename");
          readfile($zip_filepath);
          exit;
        }
        else {

          // Updates the name and path of the file.
          $excel_filepath = "$tmp_dir/" . $this->filename;
          $this->filepath = bims_change_ext($this->filepath, 'zip');
          $this->filename = basename($this->filepath);

          // Creates a zip file.
          $zip  = new ZipArchive;
          $res  = $zip->open($this->filepath, ZipArchive::OVERWRITE|ZipArchive::CREATE);

          // Adds CSV files.
          foreach ($csv_files as $name => $info) {
            $filepath = $info['filepath'];
            $zip->addFile($filepath, basename($filepath));
          }

          // Adds the Excel file.
          if ($num_sheets) {
            $this->excel->setActiveSheetIndex(0);
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, $this->version);
            $objWriter->save($excel_filepath);
            $zip->addFile($excel_filepath, basename($excel_filepath));
          }
          $zip->close();
        }

        // Removes the temporary folder.
        bims_remove_dir($tmp_dir);
      }
      else {

        // Saves the excel file.
        $this->excel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, $this->version);
        $objWriter->save($this->filepath);
      }
      return $this->filepath;
    }
  }

  /**
   * Creates an Excel file by SQL.
   *
   * @param string $type
   *
   * @return string filepath
   */
  public function createBySQL($type) {

    // Creates Excel sheets.
    for ($idx = 0; $idx < sizeof($this->sheets); $idx++) {

      // Gets the SQL.
      $sheet  = $this->sheets[$idx];
      $sql    = $sheet['sql'];
      $args   = $sheet['args'];

      // Populates the rows.
      $results = db_query($sql, $args);
      $data = array();
      while ($row = $results->fetch(PDO::FETCH_ASSOC)) {
        $data []= $row;
      }
      $sheet['data'] = $data;
    }

    // Creates an Excel file.
    $this->create($type);
  }

  /**
   * Sets the properties of the Excel file.
   */
  public function setExcelProperties() {

    // Sets Excel File Properties.
    if ($this->creator) {
      $this->excel->getProperties()->setCreator($this->creator);
    }
    if ($this->lastModifiedBy) {
      $this->excel->getProperties()->setLastModifiedBy($this->lastModifiedBy);
    }
    if ($this->title) {
      $this->excel->getProperties()->setTitle($this->title);
    }
    if ($this->subject) {
      $this->excel->getProperties()->setSubject($this->subject);
    }
    if ($this->keywords) {
      $this->excel->getProperties()->setKeywords($this->keywords);
    }
    if ($this->category) {
      $this->excel->getProperties()->setCategory($this->category);
    }
    if ($this->description) {
      $this->excel->getProperties()->setDescription($this->description);
    }
  }

  /**
   * Sets the style of the Excel sheet.
   *
   * @param array $headers
   */
  public function setExcelStyle($headers) {

    // Sets the default styles.
    $this->excel->getDefaultStyle()->getFont()->setName('Times New Roman');
    $this->excel->getDefaultStyle()->getFont()->setSize(11);

    // Sets the style for the headers.
    foreach ($headers as $col_chr => $arr) {
      if (array_key_exists('width', $arr)) {
        $this->excel->getActiveSheet()->getColumnDimension($col_chr)->setWidth($arr['width']);
      }
      $this->excel->getActiveSheet()->getStyle($col_chr.'1')->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle($col_chr.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $this->excel->getActiveSheet()->getStyle($col_chr.'1')->getFill()->getStartColor()->setARGB('FF808080');
    }
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the excel.
   *
   * @retrun array
   */
  public function getExcel() {
    return $this->excel;
  }

  /**
   * Sets the excel.
   *
   * @param PHPExcel $excel
   */
  public function setExcel($excel) {
    $this->excel = $excel;
  }

  /**
   * Retrieves the sheets.
   *
   * @retrun array
   */
  public function getSheets() {
    return $this->sheets;
  }

  /**
   * Sets the sheets.
   *
   * @param array $sheets
   */
  public function setSheets($sheets) {
    $this->sheets = $sheets;
  }

  /**
   * Retrieves the creator.
   *
   * @retrun string
   */
  public function getCreator() {
    return $this->creator;
  }

  /**
   * Sets the creator.
   *
   * @param string $creator
   */
  public function setCreator($creator) {
    $this->creator = $creator;
  }

  /**
   * Retrieves the filename.
   *
   * @retrun string
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * Sets the filename.
   *
   * @param string $filename
   */
  public function setFilename($filename) {
    $this->filename = $filename;
  }

  /**
   * Retrieves the filepath.
   *
   * @retrun string
   */
  public function getFilepath() {
    return $this->filepath;
  }

  /**
   * Sets the filepath.
   *
   * @param string $filepath
   */
  public function setFilepath($filepath) {
    $this->filepath = $filepath;
  }

  /**
   * Retrieves the version.
   *
   * @retrun string
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * Sets the version.
   *
   * @param string $version
   */
  public function setVersion($version) {
    $this->version = $version;
  }

  /**
   * Retrieves the maximum rows.
   *
   * @retrun string
   */
  public function getMaxRows() {
    return $this->max_rows;
  }

  /**
   * Retrieves the maximum columns.
   *
   * @retrun string
   */
  public function getMaxCols() {
    return $this->max_cols;
  }

  /**
   * Retrieves the title.
   *
   * @retrun string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the title.
   *
   * @param string $title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Retrieves the subject.
   *
   * @retrun string
   */
  public function getSubject() {
    return $this->subject;
  }

  /**
   * Sets the subject.
   *
   * @param string $subject
   */
  public function setSubject($subject) {
    $this->subject = $subject;
  }

  /**
   * Retrieves the keywords.
   *
   * @retrun string
   */
  public function getKeywords() {
    return $this->keywords;
  }

  /**
   * Sets the keywords.
   *
   * @param string $keywords
   */
  public function setKeywords($keywords) {
    $this->keywords = $keywords;
  }

  /**
   * Retrieves the category.
   *
   * @retrun string
   */
  public function getCategory() {
    return $this->category;
  }

  /**
   * Sets the category.
   *
   * @param string $category
   */
  public function setCategory($category) {
    $this->category = $category;
  }
}