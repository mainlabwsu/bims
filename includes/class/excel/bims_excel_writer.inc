<?php
$lib_path = 'sites/all/libraries';
require_once("$lib_path/PHPExcel/PHPExcel.php");
require_once("$lib_path/PHPExcel/PHPExcel/Writer/Excel2007.php");
require_once("$lib_path/PHPExcel/PHPExcel/IOFactory.php");

/**
 * The declaration of BIMS_EXCEL_WRITER class.
 *
 */
class BIMS_EXCEL_WRITER {

  /**
   * Class data members.
   */
  private $excel          = NULL;
  private $creator        = NULL;
  private $filename       = NULL;
  private $filepath       = NULL;
  private $title          = NULL;
  private $subject        = NULL;
  private $keywords       = NULL;
  private $category       = NULL;
  private $description    = NULL;
  private $lastModifiedBy = NULL;

  /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($details = array()) {
    $this->creator        = (array_key_exists('creator', $details))         ? $details['creator']         : 'BIMS';
    $this->filepath       = (array_key_exists('filepath', $details))        ? $details['filepath']        : '';
    $this->filename       = (array_key_exists('filename', $details))        ? $details['filename']        : '';
    $this->title          = (array_key_exists('title', $details))           ? $details['title']           : 'NEW TITLE';
    $this->subject        = (array_key_exists('subject ', $details))        ? $details['subject ']        : '';
    $this->keywords       = (array_key_exists('keywords ', $details))       ? $details['keywords ']       : '';
    $this->category       = (array_key_exists('category', $details))        ? $details['category']        : '';
    $this->version        = (array_key_exists('version', $details))         ? $details['version']         : '';
    $this->sheets         = array_key_exists('sheets', $details)            ? $details['sheets']          : array();
    $this->lastModifiedBy = (array_key_exists('lastModifiedBy', $details))  ? $details['lastModifiedBy']  : 'MAIN LAB';

    // Updates filename.
    if ($this->filepath && !$this->filename) {
      $this->filename = basename($this->filepath);
    }

    // Sets the default version.
    if (!$this->version) {
      $this->version = 'Excel2007';
    }

    // Creates new PHPExcel object.
    $this->excel = new PHPExcel();

    // Sets the properties of an Excel file.
    $this->setExcelProperties($param);
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Saves CSV files to the Excel file.
   *
   * @param array $csv_files
   */
  public function saveCSVtoExcel($csv_files) {

    // Creates reader for reading CSV file.
    $reader = PHPExcel_IOFactory::createReader('CSV');

    // Creates Excel sheets.
    $idx = 0;
    foreach ($csv_files as $bims_file_id => $filename) {
      $bims_file = BIMS_FILE::byKey(array('file_id' => $bims_file_id));

      // Gets the file.
      $filepath = $bims_file->getFilepath();
      if (file_exists($filepath)) {

        // Gets the module name at the top of the line.
        if (!($fdr = fopen($filepath, 'r'))) {
          return FALSE;
        }
        $tmp = dirname($filepath) . '/tmp';
        if (!($fdw = fopen($tmp, 'w'))) {
          return FALSE;
        }

        // Copies the file except the first line.
        $line_no = 1;
        $module = '';
        while (!feof($fdr)) {
          $line = fgets($fdr);

          if ($line_no == 1) {
            $module = substr(trim($line), 1);
          }
          else {
            fputs($fdw, $line);
          }
          $line_no++;
        }
        fclose($fdr);
        fclose($fdw);

        // Loads CSV file.
        if ($idx == 0) {
          $this->excel = $reader->load($tmp);
        }
        else {
          $reader->setSheetIndex($idx);
          $reader->LoadIntoExisting($tmp, $this->excel);
        }

        // Sets the sheet name.
        $this->excel->getActiveSheet()->setTitle($module);
        $idx++;
      }
      else {
        return FALSE;
      }
    }

    // Saves them to the Excel file.
    $writer = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
    $writer->save($this->filepath);
    return TRUE;
  }

  /**
   * Write Excel sheets to the Excel file.
   *
   * @param array $sheets
   */
  public function writeExcelSheets($sheets) {

    // Creates Excel sheets.
    for ($idx = 0; $idx < sizeof($sheets); $idx++) {
      $sheet = $sheets[$idx];

      // Creates an Excel sheet.
      $this->excel->setActiveSheetIndex($idx);

      // Sets sheet name.
      $this->excel->getActiveSheet()->setTitle($sheet['name']);

      // Sets the styles of an Excel sheet.
      $this->setExcelStyle($sheet['headers']);

      // Writes headers.
      $headers = $sheets[$idx]['headers'];
      foreach ($headers as $col_chr => $arr) {
        $this->excel->getActiveSheet()->setCellValue($col_chr.'1', $arr['heading']);
      }

      // Writes data.
      $sql  = $sheet['sql'];
      $args = $sheet['args'];
      $results = db_query($sql, $args);
      $row_no = 2;
      while ($row = $results->fetch(PDO::FETCH_ASSOC)) {
        foreach ($headers as $col_chr => $prop) {

          // Writes the value on the cell.
          $value = $row[$prop['field']];
          $this->excel->getActiveSheet()->setCellValue($col_chr.$row_no, $value);

          // Sets the properties on the cell
          if ($prop['type'] == 'link' && strtolower($value) != 'na') {
            if (!isset($prop['url'])) {
              echo "\n\tError: check header. 'url' value is missing\n\n";
              exit;
            }
            $this->excel->getActiveSheet()->getStyle($col_chr.$row_no)->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_DARKBLUE ) );
            $this->excel->getActiveSheet()->getCell($col_chr.$row_no)->getHyperlink()->setUrl($prop['url'].$value);
            if (isset($prop['tooltip'])) {
              $this->excel->getActiveSheet()->getCell($col_chr.$row_no)->getHyperlink()->setTooltip($prop['tooltip']);
            }
          }
        }
        $row_no++;
      }
    }

    // Saves data as Excel 2007 file.
    $writer = new PHPExcel_Writer_Excel2007($this->excel);
    $writer->save($this->filepath);
    $filename = $this->filename;
    mcl_print("Excel file ($filename) created", 1, 1.2);

    // Displays memory peak usage.
    bims_display_memory_usage();
  }

  /**
   * Sets the properties of the Excel file.
   *
   * @param array $params
   */
  public function setExcelProperties($param) {

    // Sets Excel File Properties.
    if ($this->creator) {
      $this->excel->getProperties()->setCreator($this->creator);
    }
    if ($this->lastModifiedBy) {
      $this->excel->getProperties()->setLastModifiedBy($this->lastModifiedBy);
    }
    if ($this->title) {
      $this->excel->getProperties()->setTitle($this->title);
    }
    if ($this->subject) {
      $this->excel->getProperties()->setSubject($this->subject);
    }
    if ($this->keywords) {
      $this->excel->getProperties()->setKeywords($this->keywords);
    }
    if ($this->category) {
      $this->excel->getProperties()->setCategory($this->category);
    }
    if ($this->description) {
      $this->excel->getProperties()->setDescription($this->description);
    }
  }

  /**
   * Sets the style of the Excel sheet.
   *
   * @param array $headers
   */
  public function setExcelStyle($headers) {

    // Sets the default styles.
    $this->excel->getDefaultStyle()->getFont()->setName('Times New Roman');
    $this->excel->getDefaultStyle()->getFont()->setSize(11);

    // Sets the style for the headers.
    foreach ($headers as $col_chr => $arr) {
      $this->excel->getActiveSheet()->getColumnDimension($col_chr)->setWidth($arr['width']);
      $this->excel->getActiveSheet()->getStyle($col_chr.'1')->getFont()->setBold(true);
      $this->excel->getActiveSheet()->getStyle($col_chr.'1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
      $this->excel->getActiveSheet()->getStyle($col_chr.'1')->getFill()->getStartColor()->setARGB('FF808080');
    }
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the filepath.
   *
   * @retrun string
   */
  public function getFilepath() {
    return $this->filepath;
  }

  /**
   * Sets the filepath.
   *
   * @param string $filepath
   */
  public function setFilepath($filepath) {
    $this->filepath = $filepath;
  }

  /**
   * Defines getters and setters below.
   */
  /**
   * Retrieves the filename.
   *
   * @retrun string
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * Sets the filename.
   *
   * @param string $filename
   */
  public function setFilename($filename) {
    $this->filename = $filename;
  }

  /**
   * Retrieves the excel.
   *
   * @retrun array
   */
  public function getExcel() {
    return $this->excel;
  }

  /**
   * Sets the excel.
   *
   * @param PHPExcel $excel
   */
  public function setExcel($excel) {
    $this->excel = $excel;
  }
}