<?php
/**
 * The declaration of BIMS_CHART class.
 *
 */
class BIMS_CHART {

  /**
   * Class data members.
   */
  protected $node_id = NULL;

 /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct($node_id) {

    // Initializes data members.
    $this->node_id = $node_id;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Returns the array of colors.
   *
   * @param integer $num
   *
   * @return array
   */
  public function getColors($num) {
    $colors = array('#B0C4DE','#D2B48C','#9ACD32','#6A5ACD','#5F9EA0','#BDB76B','#000080','#F5DEB3','#36A2EB','#FFCE56','#33CE56','#008000','#2F4F4F','#D8BFD8','#8B0000');
    if (sizeof($colors) < $num) {
      $num = sizeof($colors) - 1;
    }
    return array_slice($colors, 0, $num);
  }

  /**
   * Returns the specified color.
   *
   * @param integer $num
   *
   * @return array
   */
  public function getColor($num) {
    $colors = array('#B0C4DE','#D2B48C','#9ACD32','#6A5ACD','#5F9EA0','#BDB76B','#000080','#F5DEB3','#36A2EB','#FFCE56','#33CE56','#008000','#2F4F4F','#D8BFD8','#8B0000');
    if (sizeof($colors) < $num) {
      $num = sizeof($colors) - 1;
    }
    return $colors[$num];
  }

  /**
   * Returns the array of gray colors.
   *
   * @param integer $num
   *
   * @return array
   */
  public function getGraycolors($num) {
    $graycolors = array('#DCDCDC','#C0C0C0','#A9A9A9','#808080','#696969','#505050','#333333');
    if (sizeof($graycolors) < $num) {
      $num = sizeof($graycolors) - 1;
    }
    return array_slice($graycolors, 0, $num);
  }

  /**
   * Returns the specified gray color.
   *
   * @param integer $idx
   *
   * @return array
   */
  public function getGraycolor($idx) {
    $graycolors = array('#DCDCDC','#C0C0C0','#A9A9A9','#808080','#696969','#505050','#333333');
    if (sizeof($graycolors) < $idx) {
      $idx = sizeof($graycolors) - 1;
    }
    return $graycolors[$idx];
  }

  /**
   * Returns the parameters for a single chart.
   *
   * @param string $type
   * @param string $chart_type
   * @param integer $cvterm_id
   *
   * @return array
   */
  public function getParamsChartSingle($type, $chart_type, $cvterm_id) {

    // Gets the stats.
    $stats = NULL;
    if ($type == 'chart_trait') {
      $stats = BIMS_MVIEW_PHENOTYPE_STATS::getStatsByID($this->node_id, $cvterm_id);
    }
    else if ($type == 'chart_accession') {
      $tmp = explode(':', $cvterm_id);
      $stock_id   = $tmp[0];
      $cvterm_id  = $tmp[1];
      $stats = BIMS_MVIEW_STOCK_STATS::getStatsByID($this->node_id, $stock_id, $cvterm_id);
    }
    else if ($type == 'chart_trial') {
      $tmp = explode(':', $cvterm_id);
      $trial_id   = $tmp[0];
      $cvterm_id  = $tmp[1];
      $stats = BIMS_MVIEW_PHENOTYPE_STATS::getStatsByID($trial_id, $cvterm_id);
    }
    else if ($type == 'chart_cross') {
      $tmp = explode(':', $cvterm_id);
      $nd_experiment_id = $tmp[0];
      $cvterm_id        = $tmp[1];
      $stats = BIMS_MVIEW_CROSS_STATS::getStatsByID($this->node_id, $nd_experiment_id, $cvterm_id);
    }
    if ($stats) {

      // Gets BIMS_MVIEW_DESCRIPTOR.
      $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($this->node_id, $cvterm_id);

      // Gets the information for the chart paramaters.
      $chart_params = array();
      if ($descriptor->isNumeric()) {
        $chart_params = $this->_getChartParamsNumeric($chart_type, $stats, $cvterm_id);
      }
      else if ($stats['format'] == 'date') {
        $chart_params = $this->_getChartParamsDate($chart_type, $stats, $cvterm_id);
      }
      else if ($stats['format'] == 'boolean') {
        $chart_params = $this->_getChartParamsBoolean($chart_type, $stats);
      }
      else if (preg_match("/(categorical|multicat)/", $stats['format'])) {
        $chart_params = $this->_getChartParamsCategorical($chart_type, $stats, $cvterm_id);
      }

      // Returns the parameters.
      return array('settings' => array('bims_chart_params' => json_encode($chart_params)));
    }
    return array();
  }

  /**
   * Returns the parameters for numeric.
   *
   * @param string $chart_type
   * @param array $stats
   * @param integer $cvterm_id
   *
   * @retrun array
   */
  private function _getChartParamsNumeric($chart_type, $stats_arr, $cvterm_id) {

    // Gets the global and format precisions.
    $g_prec = bims_get_config_setting('bims_precision_format');
    $prec   = bims_get_precision($stats_arr['format']);

    // Sets the dataset.
    $dataset = array(
      'label'           => $stats_arr['name'],
      'backgroundColor' => BIMS_COLOR::getCodeByIdx('color', 0),
      'data'            => explode(':', $stats_arr['hist']),
    );

    // Sets the labels.
    $labels = array();
    $base = $stats_arr['min'];
    for ($i = 0; $i <= BIMS_HISTOGRAM_INT; $i++) {
      $label = '';
      if (($i % 10) == 0) {
        $label = sprintf($prec,  $base + ($i * $stats_arr['g_int']));
      }
      $labels []= $label;
    }

    // Sets the data.
    $data = array(
      'labels'    => $labels,
      'datasets'  => array($dataset),
    );

    // Populates the stats table row.
    $rows = array();
    if ($stats_arr['num']) {
      $rows []= array(
        $stats_arr['num'],
        sprintf($prec, $stats_arr['min']),
        sprintf($prec, $stats_arr['max']),
        sprintf($g_prec, $stats_arr['mean']),
        sprintf($g_prec, $stats_arr['std']),
      );
    }
    else {
      $rows []= array('0', '---', '---', '---', '---');
    }

    // Gets the summary table.
    $table_vars = array(
      'header'      => array('#&nbsp;Data', 'Min', 'Max', 'Mean', 'STD'),
      'rows'        => $rows,
      'attributes'  => array(),
    );
    $table = theme('table', $table_vars);

    // Sets the options.
    $options = array('legend' => array ('onClick' => null));

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
      'table'   => $table,
    );
  }

  /**
   * Returns the parameters for date.
   *
   * @param string $chart_type
   * @param array $stats
   * @param integer $cvterm_id
   *
   * @retrun array
   */
  private function _getChartParamsDate($chart_type, $stats_arr, $cvterm_id) {

    // Sets the dataset.
    $dataset = array(
      'label'           => $stats_arr['name'],
      'backgroundColor' => BIMS_COLOR::getCodeByIdx('color'),
      'data'            => explode(':', $stats_arr['hist']),
    );

    // Sets the labels.
    $labels = array();
    $base = bims_convert_date_str_to_int($stats_arr['min']);
    for ($i = 0; $i <= BIMS_HISTOGRAM_INT; $i++) {
      $label = '';
      if (($i % 10) == 0) {
        $label = bims_convert_date_int_to_str($base + ($i * $stats_arr['g_int']), TRUE);
      }
      $labels []= $label;
    }

    // Sets the data.
    $data = array(
      'labels'    => $labels,
      'datasets'  => array($dataset),
    );

    // Populates the stats table row.
    $rows = array();
    if ($stats_arr['num']) {
      $rows []= array(
        $stats_arr['num'],
        $stats_arr['min'],
        $stats_arr['max'],
        $stats_arr['mean'],
        $stats_arr['std'],
      );
    }
    else {
      $rows []= array('0', '---', '---', '---', '---');
    }

    // Gets the summary table.
    $table_vars = array(
      'header'      => array('#&nbsp;Data', 'Min', 'Max', 'Mean', 'STD'),
      'rows'        => $rows,
      'attributes'  => array(),
    );
    $table = theme('table', $table_vars);

    // Sets the options.
    $options = array('legend' => array ('onClick' => null));

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
      'table'   => $table,
    );
  }

  /**
   * Returns the parameters for boolean.
   *
   * @param string $chart_type
   * @param array $stats
   *
   * @retrun array
   */
  private function _getChartParamsBoolean($chart_type, $stats) {
    $datasets = array(
      'label'           => 'Frequency',
      'backgroundColor' => BIMS_COLOR::getCodes('color', 2),
      'data'            => array($stats['true'], $stats['false']),
    );
    $data = array(
      'labels'    => array('TRUE', 'FALSE'),
      'datasets'  => array($datasets),
    );

    // Sets the options.
    $options = array('legend' => array ('onClick' => null));

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
    );
  }

  /**
   * Returns the parameters for categorical.
   *
   * @param string $chart_type
   * @param array $stats
   * @param integer $cvterm_id
   *
   * @retrun array
   */
  private function _getChartParamsCategorical($chart_type, $stats, $cvterm_id) {

    // Gets the descriptor information.
    $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($this->node_id, $cvterm_id);
    $categories = $descriptor->getCategories();

    // Gets the labels.
    $labels = array_values($categories);

    // Gets the freq.
    $freq = array();
    foreach ($categories as $name => $label) {
      $name_lc = strtolower($name);
      if (array_key_exists($name_lc, $stats['freq'])) {
        $freq []= $stats['freq'][$name_lc];
      }
      else {
        $freq []= '0';
      }
    }

    // Sets datasets.
    $datasets = array();
    if ($chart_type == 'bar') {
      $datasets = array(
        'label'           => 'Frequency',
        'backgroundColor' => BIMS_COLOR::getCodes('color', sizeof($labels)),
        'data'            => $freq,
      );
    }
    else if ($chart_type == 'doughnut') {
      $datasets = array(
        'label'           => 'Frequency',
        'backgroundColor' => BIMS_COLOR::getCodes('color', sizeof($labels)),
        'data'            => $freq,
      );
    }

    // Sets the data.
    $data = array(
      'labels'    => $labels,
      'datasets'  => array($datasets),
    );

    // Sets the options.
    $options = array('legend' => array ('onClick' => null));

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
    );
  }

  /**
   * Returns the parameters for the grouped chart.
   *
   * @param string $chart_type
   * @param string $stats_id
   *
   * @return array
   */
  public function getParamsChartGrouped($chart_type, $stats_id) {

    // Gets the stats_arr.
    $stats_arr = BIMS_VARIABLE::getVariable($stats_id);
    if (empty($stats_arr)) {
      return array();
    }

    // Gets BIMS_MVIEW_DESCRIPTOR.
    $cvterm_id  = $stats_arr['params']['cvterm_id'];
    $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($this->node_id, $cvterm_id);
    $format = $descriptor->getFormat();

    // Gets the information for the chart paramaters.
    $chart_params = array();
    if ($descriptor->isDataPointOnly()) {
      $chart_params = $this->_getChartParamsDataPointGrouped($chart_type, $stats_arr);
    }
    else if ($descriptor->isNumeric()) {
      $chart_params = $this->_getChartParamsNumericGrouped($chart_type, $stats_arr);
    }
    else if ($format == 'date') {
      $chart_params = $this->_getChartParamsDateGrouped($chart_type, $stats_arr);
    }
    else if ($format == 'boolean') {
      $chart_params = $this->_getChartParamsBooleanGrouped($chart_type, $stats_arr);
    }
    else if (preg_match("/(categorical|multicat)/", $format)) {
      $chart_params = $this->_getChartParamsCategoricalGrouped($chart_type, $stats_arr);
    }

    // Returns the parameters.
    return array('settings' => array('bims_chart_params' => json_encode($chart_params)));
  }

  /**
   * Returns the parameters for numeric (Grouped).
   *
   * @param string $chart_type
   * @param array $stats_arr
   *
   * @retrun array
   */
  private function _getChartParamsNumericGrouped($chart_type, $stats_arr) {

    // Gets the precision format.
    $pricison_format = bims_get_config_setting('bims_precision_format');

    // Gets the target descriptor.
    $cvterm_id = $stats_arr['params']['cvterm_id'];

    // Gets the data from stats array.
    $g_min = '';
    $g_max = '';
    $g_int = '';
    $datasets = array();
    $count = 0;
    $global_flag = FALSE;
    $rows = array();
    foreach ($stats_arr['stats'] as $selection) {
      $cvterms = $selection['CVTERMS'];

      // Gets global max, min and interval from the first element.
      if (!$global_flag && isset($cvterms[$cvterm_id]['g_min'])) {
        $g_min = $cvterms[$cvterm_id]['g_min'];
        $g_max = $cvterms[$cvterm_id]['g_max'];
        $g_int = $cvterms[$cvterm_id]['g_int'];
        $global_flag = TRUE;
      }

      // Sets the dataset.
      $dataset_data = array_fill(0, BIMS_HISTOGRAM_INT, 0);
      if (isset($cvterms[$cvterm_id]['hist'])) {
        $hist = $cvterms[$cvterm_id]['hist'];
        $dataset_data = explode(':', $hist);
      }
      $dataset = array(
        'label'           => $selection['LABEL'],
        'backgroundColor' => BIMS_COLOR::getCodeByIdx('color', $count++),
        'data'            => $dataset_data,
      );
      $datasets []= $dataset;
      if (isset($cvterms[$cvterm_id]['num']) && $cvterms[$cvterm_id]['num']) {
        $rows []= array(
          $selection['LABEL'],
          $cvterms[$cvterm_id]['num'],
          sprintf($pricison_format, $cvterms[$cvterm_id]['min']),
          sprintf($pricison_format, $cvterms[$cvterm_id]['max']),
          sprintf($pricison_format, $cvterms[$cvterm_id]['mean']),
          sprintf($pricison_format, $cvterms[$cvterm_id]['std']),
        );
      }
      else {
        $rows []= array($selection['LABEL'], '0', '---', '---', '---', '---');
      }
    }

    // Sets the labels.
    $labels = array();
    $interval = (real)($g_max - $g_min) / (BIMS_HISTOGRAM_INT - 1);
    for ($i = 0; $i <= BIMS_HISTOGRAM_INT; $i++) {
      $label = '';
      if (($i % 10) == 0) {
        $label = sprintf($pricison_format,  $g_min + ($i * $interval));
      }
      $labels []= $label;
    }

    // Sets the data.
    $data = array(
      'labels'    => $labels,
      'datasets'  => $datasets,
    );

    // Gets the summary table.
    $table_vars = array(
      'header'      => array(array('data' => 'Selection', 'width' => '10%'), '#&nbsp;Data', 'Min', 'Max', 'Mean', 'STD'),
      'rows'        => $rows,
      'attributes'  => array(),
    );
    $table = theme('table', $table_vars);

    // Adds the options.
    $y_axes = array(
      'type'  => 'linear',
      'ticks' => array(
        'beginAtZero' => true,
        'callback'    => "return ((value % 1) == 0) ? value : '';",
      ),
    );
    $options = array(
      'scales' => array(
        'yAxes' => array($y_axes),
      ),
    );

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
      'table'   => $table,
    );
  }

  /**
   * Returns the parameters for date (Grouped).
   *
   * @param string $chart_type
   * @param array $stats_arr
   *
   * @retrun array
   */
  private function _getChartParamsDateGrouped($chart_type, $stats_arr) {

    // Gets the precision format.
    $pricison_format = bims_get_config_setting('bims_precision_format');

    // Gets the target descriptor.
    $cvterm_id = $stats_arr['params']['cvterm_id'];

    // Gets the data from stats array.
    $g_min = '';
    $g_max = '';
    $g_int = '';
    $datasets = array();
    $count = 0;
    $rows = array();
    foreach ($stats_arr['stats'] as $selection) {
      $cvterms = $selection['CVTERMS'];

      // Gets global max, min and interval from the first element.
      if (!$count) {
        $g_min = $cvterms[$cvterm_id]['g_min'];
        $g_max = $cvterms[$cvterm_id]['g_max'];
        $g_int = $cvterms[$cvterm_id]['g_int'];
      }

      // Sets the dataset.
      $dataset_data = array_fill(0, BIMS_HISTOGRAM_INT, 0);
      if (array_key_exists($cvterm_id, $cvterms)) {
        $hist = $cvterms[$cvterm_id]['hist'];
        $dataset_data = explode(':', $hist);
      }
      $dataset = array(
        'label'           => $selection['LABEL'],
        'backgroundColor' => BIMS_COLOR::getCodeByIdx('color', $count++),
        'data'            => $dataset_data,
      );
      $datasets []= $dataset;
      if ($cvterms[$cvterm_id]['num']) {
        $rows []= array(
          $selection['LABEL'],
          $cvterms[$cvterm_id]['num'],
          $cvterms[$cvterm_id]['min'],
          $cvterms[$cvterm_id]['max'],
          $cvterms[$cvterm_id]['mean'],
          $cvterms[$cvterm_id]['std'],
        );
      }
      else {
        $rows []= array($selection['LABEL'], '0', '---', '---', '---', '---');
      }
    }

    // Sets the labels.
    $labels = array();
    $base = bims_convert_date_str_to_int($g_min);
    for ($i = 0; $i <= BIMS_HISTOGRAM_INT; $i++) {
      $label = '';
      if (($i % 10) == 0) {
        $label = bims_convert_date_int_to_str($base + ($i * $g_int), TRUE);
      }
      $labels []= $label;
    }

    // Sets the data.
    $data = array(
      'labels'    => $labels,
      'datasets'  => $datasets,
    );

    // Gets the summary table.
    $table_vars = array(
      'header'      => array(array('data' => 'Selection', 'width' => '10%'), '#&nbsp;Data', 'Min', 'Max', 'Mean', 'STD'),
      'rows'        => $rows,
      'attributes'  => array(),
    );
    $table = theme('table', $table_vars);

    // Adds the options.
    $y_axes = array(
      'type'  => 'linear',
      'ticks' => array(
        'beginAtZero' => true,
        'callback'    => "return ((value % 1) == 0) ? value : '';",
      ),
    );
    $options = array(
      'scales' => array(
        'yAxes' => array($y_axes),
      ),
    );

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
      'table'   => $table,
    );
  }

  /**
   * Returns the parameters for data point (Grouped).
   *
   * @param string $chart_type
   * @param array $stats_arr
   *
   * @retrun array
   */
  private function _getChartParamsDataPointGrouped($chart_type, $stats_arr) {

    // Gets the target descriptor.
    $cvterm_id = $stats_arr['params']['cvterm_id'];

    // Gets the data from stats array.
    $datasets = array();
    $count = 0;
    foreach ($stats_arr['stats'] as $selection) {

      // Sets the dataset.
      $cvterms = $selection['CVTERMS'];
      $dataset_data = array(0);
      if (array_key_exists($cvterm_id, $cvterms)) {
        $dataset_data = array($cvterms[$cvterm_id]['num']);
      }
      $dataset = array(
        'label'           => $selection['LABEL'],
        'backgroundColor' => BIMS_COLOR::getCodeByIdx('color', $count++),
        'data'            => $dataset_data,
      );
      $datasets []= $dataset;
    }
    $data = array(
      'labels'    => array('Data Points'),
      'datasets'  => $datasets,
    );

    // Adds the options.
    $y_axes = array(
      'type'  => 'linear',
      'ticks' => array(
        'beginAtZero' => true,
        'callback'    => "return ((value % 1) == 0) ? value : '';",
      ),
    );
    $options = array(
      'scales' => array(
        'yAxes' => array($y_axes),
      ),
    );

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
    );
  }

  /**
   * Returns the parameters for boolean (Grouped).
   *
   * @param string $chart_type
   * @param array $stats_arr
   *
   * @retrun array
   */
  private function _getChartParamsBooleanGrouped($chart_type, $stats_arr) {

    // Gets the target descriptor.
    $cvterm_id = $stats_arr['params']['cvterm_id'];

    // Gets the data from stats array.
    $datasets = array();
    $count = 0;
    foreach ($stats_arr['stats'] as $selection) {

      // Sets the dataset.
      $cvterms = $selection['CVTERMS'];
      $dataset_data = array(0, 0);
      if (array_key_exists($cvterm_id, $cvterms)) {
        $dataset_data = array($cvterms[$cvterm_id]['true'], $cvterms[$cvterm_id]['false']);
      }
      $dataset = array(
        'label'           => $selection['LABEL'],
        'backgroundColor' => BIMS_COLOR::getCodeByIdx('color', $count++),
        'data'            => $dataset_data,
       );
      $datasets []= $dataset;
    }
    $data = array(
      'labels'    => array('TRUE', 'FALSE'),
      'datasets'  => $datasets,
    );

    // Adds the options.
    $y_axes = array(
      'type'  => 'linear',
      'ticks' => array(
        'beginAtZero' => true,
        'callback'    => "return ((value % 1) == 0) ? value : '';",
      ),
    );
    $options = array(
      'scales' => array(
        'yAxes' => array($y_axes),
      ),
    );

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
    );
  }

  /**
   * Returns the parameters for categorical (Grouped).
   *
   * @param string $chart_type
   * @param array $stats_arr
   *
   * @retrun array
   */
  private function _getChartParamsCategoricalGrouped($chart_type, $stats_arr) {

    // Gets the descriptor information.
    $cvterm_id = $stats_arr['params']['cvterm_id'];
    $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($this->node_id, $cvterm_id);
    $categories = $descriptor->getCategories();

    // Gets the labels.
    $labels = array_values($categories);

    // Gets the data from stats array.
    $datasets = array();
    $count = 0;
    foreach ($stats_arr['stats'] as $selection) {

      // Gets the freq.
      $cvterms = $selection['CVTERMS'];
      $freq = array();
      foreach ($categories as $name => $label) {
        $name_lc = strtolower($name);
        if (array_key_exists($cvterm_id, $cvterms) && array_key_exists($name_lc, $cvterms[$cvterm_id]['freq'])) {
          $freq []= $cvterms[$cvterm_id]['freq'][$name_lc];
        }
        else {
          $freq []= '0';
        }
      }

      // Sets the dataset.
      $dataset = array(
        'label'           => $selection['LABEL'],
        'backgroundColor' => BIMS_COLOR::getCodeByIdx('color', $count++),
        'data'            => $freq,
      );
      $datasets []= $dataset;
    }

    // Sets the data.
    $data = array(
      'labels'    => $labels,
      'datasets'  => $datasets,
    );

    // Adds the options.
    $y_axes = array(
      'type'  => 'linear',
      'ticks' => array(
        'beginAtZero' => true,
        'callback'    => "return ((value % 1) == 0) ? value : '';",
      ),
    );
    $options = array(
      'scales' => array(
        'yAxes' => array($y_axes),
      ),
    );

    // Adds the parameters for the chart.
    return array(
      'type'    => $chart_type,
      'data'    => json_encode($data),
      'options' => json_encode($options),
    );
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the node ID.
   *
   * @retrun integer
   */
  public function getNodeID() {
    return $this->node_id;
  }

  /**
   * Sets the node ID.
   *
   * @param integer $node_id
   */
  public function setNodeID($node_id) {
    $this->node_id = $node_id;
  }
}