<?php
/**
 * The declaration of BIMS_IMPORTED_PROJECT class.
 *
 */
class BIMS_IMPORTED_PROJECT extends PUBLIC_BIMS_IMPORTED_PROJECT {

  /**
   *  Data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_IMPORTED_PROJECT::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_PROJECT::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_PROJECT::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_PROJECT::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_PROJECT::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Checks if the given project can be removed from BIMS. Return TRUE if
   * no BIMS program imported the project.
   *
   * @param integer $project_id
   *
   * @return boolean
   */
  public static function canDeleteProject($project_id) {
    $sql = "
      SELECT COUNT(IP.project_id)
      FROM {bims_imported_project} IP
      WHERE IP.project_id = :project_id
    ";
    $num = db_query($sql, array(':project_id' => $project_id))->fetchField();
    return $num ? FALSE : TRUE;
  }

  /**
   * Adds a project to bims_chado_project.
   *
   * @param array $details
   *
   * @return boolean
   */
  public static function addProject($details) {

    // Checks for duplication.
    $keys = array('project_id' => $details['project_id']);
    $chado_proj = PUBLIC_BIMS_CHADO_PROJECT::byKey($keys);

    // Adds chado-project.
    if (!$chado_proj) {

      // Updates the crop ID.
      if (!array_key_exists('crop_id', $details)) {
        if (array_key_exists('cv_id', $details)) {
          $keys = array('cv_id' => $details['cv_id']);
          $bims_chado_cv = PUBLIC_BIMS_CHADO_CV::byKey($keys);
          if ($bims_chado_cv) {
            $details['crop_id'] = $bims_chado_cv->getCropID();
          }
        }
      }

      // Adds the project.
      $chado_proj = new PUBLIC_BIMS_CHADO_PROJECT($details);
      return $chado_proj->insert();
    }
    return TRUE;
  }

  /**
   * Returns if this is Chado project.
   *
   * @param integer $project_id
   *
   * @return boolean
   */
  public static function isChado($project_id) {
    $sql = "
      SELECT COUNT(project_id) FROM {bims_chado_project}
      WHERE project_id = :project_id
    ";
    $num = db_query($sql, array(':project_id' => $project_id))->fetchField();
    return $num ? TRUE : FALSE;
  }


  /**
   * Checks the imported trials.
   *
   * @param boolean $flag_invalid
   *
   * @return string
   */
  public static function checkImportedTrials($flag_invalid = FALSE) {

    // Checks the trials in bims_chado_project.
    $msg  = "\n\n ----- Checking the trials in bims_chado_project table -----\n";
    $msgi = $msg;

    // Gets the trials.
    $sql = 'SELECT CP.* FROM {bims_chado_project} CP ORDER BY CP.type, CP.project_name';
    $project_existing = array();
    $project_missing  = array();
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $dataset = MCL_CHADO_DATASET::byID($obj->project_id);
      if ($dataset) {
        $project_existing []= sprintf("%d\t%s\t%s", $obj->project_id, $obj->type, $obj->project_name);
      }
      else {
        $project_missing []= sprintf("%d\t%s\t%s",$obj->project_id, $obj->type, $obj->project_name);
      }
    }

    // Writes the message.
    if (!empty($project_existing)) {
      $msg .= "\nChado Projects (Valid projects)\nProject ID\tType\tName\n";
      foreach ($project_existing as $project_info) {
        $msg .= "$project_info\n";
      }
    }
    if (empty($project_missing)) {
      $msg .= "\nNo project needed to be removed from bims_chado_project\n";
      $msgi .= "\nNo project needed to be removed from bims_chado_project\n";
    }
    else {
      $msg  .= "\nChado Projects (Invalid projects)\nProject ID\tType\tName\n";
      $msgi .= "\nChado Projects (Invalid projects)\nProject ID\tType\tName\n";
      foreach ($project_missing as $project_info) {
        $msg  .= "$project_info\n";
        $msgi .= "$project_info\n";
      }
    }

    // Checks the trials in bims_imported_project.
    $msg  .= "\n\n ----- Checking the trials in bims_imported_project table -----\n";
    $msgi .= "\n\n ----- Checking the trials in bims_imported_project table -----\n";

    // Gets the trials.
    $sql = "
      SELECT * FROM {bims_imported_project}
      ORDER BY program_id, type, sub_type
    ";
    $project_existing = array();
    $project_missing  = array();
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $dataset = MCL_CHADO_DATASET::byID($obj->project_id);
      if ($dataset) {
        $project_existing []= sprintf("%d\t%d\t%s\t%s", $obj->project_id,  $obj->program_id, $obj->type, $obj->project_name);
      }
      else {
        $project_missing []= sprintf("%d\t%d\t%s\t%s", $obj->project_id, $obj->program_id, $obj->type, $obj->project_name);
      }
    }

    // Writes the message.
    if (!empty($project_existing)) {
      $msg .= "\nImported Trails (Valid projects)\nProject ID\tProgram ID\tType\tLabel\n";
      foreach ($project_existing as $project_info) {
        $msg .= "$project_info\n";
      }
    }
    if (empty($project_missing)) {
      $msg  .= "\nNo trials needed to be removed from bims_imported_project\n";
      $msgi .= "\nNo trials needed to be removed from bims_imported_project\n";
    }
    else {
      $msg  .= "\nImported Trails (Invalid projects)\nProject ID\tProgram ID\tType\tName\n";
      $msgi .= "\nImported Trails (Invalid projects)\nProject ID\tProgram ID\tType\tName\n";
      foreach ($project_missing as $project_info) {
        $msg  .= "$project_info\n";
        $msgi .= "$project_info\n";
      }
    }
    return $flag_invalid ? $msgi : $msg;
  }

  /**
   * Cleans bims_imported_project table.
   *
   * @param integer $program_id
   * @param boolean $delete_flag
   *
   * @return boolean
   */
  public static function cleanImportedProject($program_id, $delete_flag = FALSE) {

    // Gets all the project.
    $sql = "
      SELECT project_id, project_name FROM {bims_imported_project}
      WHERE program_id = :program_id
        AND project_id NOT IN (SELECT project_id FROM {chado.project})
    ";
    $results = db_query($sql, array(':program_id' => $program_id));
    $delete_projs = array();
    while ($obj = $results->fetchObject()) {
      $delete_projs[$obj->project_id] = $obj->project_name;
    }

    // Shows the results.
    if (empty($delete_projs)) {
      bims_print("No project needed to be removed", 1, 3);
      return TRUE;
    }
    bims_print("The following projects will be removed from BIMS", 1, 1.2);
    foreach ($delete_projs as $project_id => $project_name) {
      bims_print("[$project_id] $project_name", 1, 1, FALSE);
    }

    // Deletes the projects.
    if ($delete_flag) {
      foreach ((array)$delete_projs as $project_id => $project_name) {
        db_delete('bims_imported_project')
          ->condition('program_id', $program_id, '=')
          ->condition('project_id', $project_id, '=')
          ->execute();
        db_delete('bims_chado_project')
          ->condition('project_id', $project_id, '=')
          ->execute();
      }
    }
    return TRUE;
  }

  /**
   * Cleans bims_chado_project table.
   *
   * @param boolean $delete_flag
   *
   * @return boolean
   */
  public static function cleanBimsChadoProject($delete_flag = FALSE) {

    // Checks the projects in chado.project.
    $sql = "
      SELECT BCP.project_id, BCP.project_name
      FROM {bims_chado_project} BCP
      WHERE (BCP.project_id NOT IN (SELECT project_id FROM {chado.project})) OR (
        BCP.cv_name != '' AND BCP.cv_id NOT IN (SELECT cv_id FROM {bims_chado_cv})
      )
      ORDER BY BCP.project_name
    ";
    $results = db_query($sql);
    $delete_projs = array();
    while ($obj = $results->fetchObject()) {
      $delete_projs[$obj->project_id] = $obj->project_name;
    }

    // Shows the results.
    if (empty($delete_projs)) {
      bims_print("No project needed to be removed", 1, 1.1);
    }
    else {
      bims_print("The following projects will be removed from BIMS", 1, 1.2);
      foreach ($delete_projs as $project_id => $project_name) {
        bims_print("[$project_id] $project_name", 1, 1, FALSE);
      }

      // Deletes the projects.
      if ($delete_flag) {
        foreach ((array)$delete_projs as $project_id => $project_name) {
          db_delete('bims_chado_project')
            ->condition('project_id', $project_id, '=')
            ->execute();
        }
      }
    }
    return TRUE;
  }

  /**
   * Returns the ID of all the features of the projects belong to the given program.
   *
   * @param integer $program_id
   * @param integer $cv_name
   * @param integer $cvterm_name
   *
   * @return array
   */
  public static function getFeatureIDs($program_id, $cv_name, $cvterm_name) {

    // Gets all the imported projects.
    $sql = "
      SELECT IP.* FROM {bims_imported_project} IP
      WHERE IP.program_id = :program_id
    ";
    $args = array(':program_id' => $program_id);
    $results = db_query($sql, array(':program_id' => $program_id));

    // Gets all the feature IDs.
    $feature_ids = array();
    while ($obj = $results->fetchObject()) {
      $type     = strtolower($obj->type);
      $sub_type = strtolower($obj->sub_type);

      // Gets the feature IDs.
      $sql = '';
      if (preg_match("/^(ssr|haplotype)$/", $sub_type)) {
        $sql = "
          SELECT DISTINCT FG.feature_id
          FROM {chado.feature_genotype} FG
            INNER JOIN {chado.nd_experiment_genotype} NEG ON NEG.genotype_id = FG.genotype_id
            INNER JOIN {chado.nd_experiment_project} NEP ON NEP.nd_experiment_id = NEG.nd_experiment_id
            INNER JOIN {chado.feature} F on F.feature_id = FG.feature_id
            INNER JOIN {chado.cvterm} C on C.cvterm_id = F.type_id
            INNER JOIN {chado.cv} CV on CV.cv_id = C.cv_id
          WHERE NEP.project_id = :project_id AND LOWER(CV.name) = LOWER(:cv_name)
            AND LOWER(C.name) = LOWER(:cvterm_name)
        ";
      }
      else if ($sub_type == 'snp') {
        $sql = "
          SELECT DISTINCT GC.feature_id
          FROM {chado.genotype_call} GC
            INNER JOIN {chado.feature} F on F.feature_id = GC.feature_id
            INNER JOIN {chado.cvterm} C on C.cvterm_id = F.type_id
            INNER JOIN {chado.cv} CV on CV.cv_id = C.cv_id
          WHERE GC.project_id = :project_id AND LOWER(CV.name) = LOWER(:cv_name)
            AND LOWER(C.name) = LOWER(:cvterm_name)
        ";
      }

      // Adds the feature IDs.
      if ($sql) {
        $args = array(
          ':project_id'   => $obj->project_id,
          ':cv_name'      => $cv_name,
          ':cvterm_name'  => $cvterm_name,
        );
        $sub_results = db_query($sql, $args);
        while ($feature_id = $sub_results->fetchField()) {
          $feature_ids[$feature_id] = TRUE;
        }
      }
    }
    return empty($feature_ids) ? array() : array_keys($feature_ids);
  }

  /**
   * Returns the ID of all the stock of the projects belong to the given program.
   *
   * @param integer $program_id
   *
   * @return array
   */
  public static function getStockIDs($program_id) {

    // Gets all the imported projects.
    $sql = "
      SELECT IP.* FROM {bims_imported_project} IP
      WHERE IP.program_id = :program_id
    ";
    $results = db_query($sql, array(':program_id' => $program_id));

    // Gets all the stock IDs.
    $stock_ids = array();
    while ($obj = $results->fetchObject()) {
      $type     = strtolower($obj->type);
      $sub_type = strtolower($obj->sub_type);

      // Gets the stock IDs.
      $sql = '';
      if ($type == 'phenotype') {
        $sql = "
          SELECT DISTINCT SR.object_id AS stock_id
          FROM {chado.stock_relationship} SR
            INNER JOIN {chado.nd_experiment_stock} NES ON NES.stock_id = SR.subject_id
            INNER JOIN {chado.nd_experiment_project} NEP ON NEP.nd_experiment_id = NES.nd_experiment_id
          WHERE NEP.project_id = :project_id
        ";
      }
      else if (preg_match("/^(ssr|haplotype)$/", $sub_type)) {
        $sql = "
          SELECT DISTINCT SR.object_id AS stock_id
          FROM {chado.stock_relationship} SR
            INNER JOIN {chado.nd_experiment_stock} NES ON NES.stock_id = SR.subject_id
            INNER JOIN {chado.nd_experiment_project} NEP ON NEP.nd_experiment_id = NES.nd_experiment_id
          WHERE NEP.project_id = :project_id
        ";
      }
      else if ($sub_type == 'snp') {
        $sql = "
          SELECT DISTINCT GC.stock_id
          FROM {chado.genotype_call} GC
          WHERE GC.project_id = :project_id
        ";
      }

      // Adds the stock IDs.
      if ($sql) {
        $sub_results = db_query($sql, array(':project_id' => $obj->project_id));
        while ($stock_id = $sub_results->fetchField()) {
          $stock_ids[$stock_id] = TRUE;
        }
      }
    }
    return empty($stock_ids) ? array() : array_keys($stock_ids);
  }

  /**
   * Returns all phenotypic trials that belong to the given cv ID.
   * If $program_id is provided, it returns all unimported trial from Chado.
   *
   * @param integer $cv_id
   * @param integer $flag
   * @param integer $program_id
   *
   * @return array
   */
  public static function getPhenotypeTrial($cv_id, $flag = BIMS_OBJECT, $program_id = NULL) {

    // Checks cv ID.
    if (!$cv_id) {
      return array();
    }

    // Gets the trials.
    $sql = "
      SELECT * FROM {bims_chado_project}
      WHERE cv_id = :cv_id AND LOWER(type) = :type
    ";
    $args = array(
      ':cv_id'  => $cv_id,
      ':type'   => 'phenotype',
    );
    if ($program_id) {
      $sql .= " AND project_id NOT IN (
        SELECT project_id FROM {bims_imported_project}
        WHERE program_id = :program_id)
      ";
      $args[':program_id'] = $program_id;
    }
    $sql .= ' ORDER BY project_name';
    $results = db_query($sql, $args);
    $trials = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $trials[$obj->project_id] = $obj->project_name;
      }
      else {
        $trials []= $obj;
      }
    }
    return $trials;
  }

  /**
   * Returns all genotypic trials.
   * If $program_id is provided, it returns all unimported trial from Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param integer $flag
   * @param integer $program_id
   *
   * @return array
   */
  public static function getGenotypeTrial(BIMS_PROGRAM $bims_program, $flag = BIMS_OBJECT) {

    // Gets the trials.
    $sql  = "
      SELECT CP.* FROM {bims_chado_project} CP
      WHERE CP.crop_id = :crop_id AND LOWER(CP.type) = :type
        AND CP.project_id NOT IN (
          SELECT project_id FROM {bims_imported_project}
          WHERE program_id = :program_id
        )
      ORDER BY CP.project_name
    ";
    $args = array(
      ':type'       => 'genotype',
      ':program_id' => $bims_program->getProgramID(),
      ':crop_id'    => $bims_program->getCropID(),
    );
    $results = db_query($sql, $args);
    $trials = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $trials[$obj->project_id] = $obj->project_name;
      }
      else {
        $trials []= $obj;
      }
    }
    return $trials;
  }

  /**
   * Returns all cross trials.
   * If $program_id is provided, it returns all unimported trial from Chado.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param integer $flag
   *
   * @return array
   */
  public static function getCrossTrial(BIMS_PROGRAM $bims_program, $flag = BIMS_OBJECT) {

    // Gets the trials.
    $sql  = "
      SELECT * FROM {bims_chado_project}
      WHERE crop_id = :crop_id AND LOWER(type) = :type
       AND project_id NOT IN (
          SELECT project_id FROM {bims_imported_project}
          WHERE program_id = :program_id
        )
    ";
    $args = array(
      ':type'       => 'cross',
      ':program_id' => $bims_program->getProgramID(),
      ':crop_id'    => $bims_program->getCropID(),
    );
    $results = db_query($sql, $args);
    $trials = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $trials[$obj->project_id] = $obj->project_name;
      }
      else {
        $trials []= $obj;
      }
    }
    return $trials;
  }

  /**
   * Returns the imported projects by the given type and program ID.
   *
   * @param integer $program_id
   * @param string $type
   * @param integer $flag
   *
   * @return array
   */
  public static function getImportedProjByType($program_id, $type = '', $flag = BIMS_OBJECT) {

    // Gets the imported projects.
    $sql = "
      SELECT IP.*, CP.cv_id, CP.cv_name
      FROM {bims_imported_project} IP
        LEFT JOIN {bims_chado_project} CP on CP.project_id = IP.project_id
      WHERE IP.program_id = :program_id
    ";
    $args = array(':program_id' => $program_id);
    if ($type) {
      $sql .= " AND LOWER(IP.type) = LOWER(:type) ";
      $args[':type' ] = $type;
    }
    $sql .= " ORDER BY CP.cv_name, IP.project_name, IP.type, IP.sub_type ";
    $results = db_query($sql, $args);
    $imported = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_CLASS) {
        $keys = array(
          'program_id'  => $obj->program_id,
          'project_id'  => $obj->project_id,
        );
        $imported []= BIMS_IMPORTED_PROJECT::byKey($keys);
      }
      else if ($flag == BIMS_OPTION) {
        $imported[$obj->project_id] = $obj->project_name;
      }
      else {
        $imported []= $obj;
      }
    }
    return $imported;
  }

  /**
   * Updates the stauts.
   *
   * @param string $status
   *
   * @return boolean
   */
  public function updateStatus($status) {
    $this->setStatus($status);
    return $this->update();
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the prop array.
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   *
   * @param string $key
   *
   * @return string
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   *
   * @param string $key
   * @param string $value
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}