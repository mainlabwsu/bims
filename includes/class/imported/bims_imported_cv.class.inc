<?php
/**
 * The declaration of BIMS_IMPORTED_CV class.
 *
 */
class BIMS_IMPORTED_CV extends PUBLIC_BIMS_IMPORTED_CV {

  /**
   *  Data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_IMPORTED_CV::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_CV::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
      return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_IMPORTED_CV by cv ID.
   *
   * @param integer $cv_id
   *
   * @return BIMS_IMPORTED_CV|NULL
   */
  public static function byID($cv_id) {
    if (!bims_is_int($cv_id)) {
      return NULL;
    }
    return self::byKey(array('cv_id' => $cv_id));
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_CV::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_CV::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_IMPORTED_CV::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * Deletes the CV from BIMS.
   *
   * @param string $cv_id
   *
   * @return boolean
   */
  public static function deleteCV($cv_id) {

    $transaction = db_transaction();
    try {

      // Lists all tables.
      $table_names = array(
        'bims_chado_cv',
        'bims_chado_project',
        'bims_mview_descriptor'
      );

      // Deletes cv from the tables.
      foreach ($table_names as $table_name) {
        db_delete($table_names)
          ->condition('cv_id', $cv_id, '=')
          ->execute();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a CV to BIMS.
   *
   * @param integer $crop_id
   * @param integer $cv_id
   * @param string $cv_name
   * @param string $type
   * @param boolean $flag_clean
   *
   * @return boolean
   */
  public static function addCV($crop_id, $cv_id, $cv_name, $type, $flag_clean = FALSE) {

    $transaction = db_transaction();
    try {

      // Cleans cv form the tables.
      if ($flag_clean) {
        db_delete('bims_chado_cv')
          ->condition('cv_id', $cv_id, '=')
          ->execute();
        db_delete('bims_chado_project')
          ->condition('cv_id', $cv_id, '=')
          ->execute();
        db_delete('bims_mview_descriptor')
          ->condition('cv_id', $cv_id, '=')
          ->execute();
      }

      // Checks for duplication.
      $bims_chado_cv = PUBLIC_BIMS_CHADO_CV::byKey(array('cv_id' => $cv_id));

      // Adds the CV to bims_chado_cv table.
      if (!$bims_chado_cv) {
        $details = array(
          'crop_id' => $crop_id,
          'cv_id'   => $cv_id,
          'cv_name' => $cv_name,
          'type'    => $type,
        );
        $bims_chado_cv = new PUBLIC_BIMS_CHADO_CV($details);
        if (!$bims_chado_cv->insert()) {
          throw new Exception("Error : Failed to add cv to bims_chado_cv.");
        }
      }

      // Populates bims_chado_project for the cv_id.
      $sql = "
        SELECT DISTINCT PR.project_id, PR.name AS project_name, CV.cv_id, CV.name AS cv_name
        FROM {chado.project} PR
          INNER JOIN {chado.projectprop} PRP on PRP.project_id = PR.project_id
          LEFT JOIN {chado.nd_experiment_project} NEPR on NEPR.project_id = PR.project_id
          LEFT JOIN {chado.nd_experiment_phenotype} NEP on NEP.nd_experiment_id = NEPR.nd_experiment_id
          LEFT JOIN {chado.phenotype} P on P.phenotype_id = NEP.phenotype_id
          LEFT JOIN {chado.cvterm} C on C.cvterm_id = P.attr_id
          LEFT JOIN {chado.cv} CV on CV.cv_id = C.cv_id
        WHERE C.cv_id = :cv_id AND C.name is not NULL
        ORDER BY PR.name
      ";
      $args = array(':cv_id' => $cv_id);
      $results = db_query($sql, $args);
      while ($obj = $results->fetchObject()) {
        $cv_name      = $obj->cv_name;
        $project_name = $obj->project_name;

        // Adds the project to to bims_chado-project table.
        $details = array(
          'type'          => 'phenotype',
          'sub_type'      => '',
          'cv_id'         => $obj->cv_id,
          'cv_name'       => $cv_name,
          'project_id'    => $obj->project_id,
          'project_name'  => $project_name,
        );
        if (!BIMS_IMPORTED_PROJECT::addProject($details)) {
          throw new Exception("Error : Failed to add bims_chado_project ($cv_name, $project_name)");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Cleans bims_imported_cv table.
   *
   * @param integer $program_id
   * @param boolean $delete_flag
   *
   * @return boolean
   */
  public static function cleanImportedCV($program_id, $delete_flag = FALSE) {

    // Gets all the CVs of the program.
    $sql = "
      SELECT cv_id, cv_name FROM {bims_imported_cv}
      WHERE program_id = :program_id
        AND cv_id NOT IN (SELECT cv_id FROM {chado.cv})
    ";
    $results = db_query($sql, array(':program' => $program_id));
    $delete_cvs = array();
    while ($obj = $results->fetchObject()) {
      $delete_cvs[$obj->cv_id] = $obj->cv_name;
    }

    // Shows the results.
    if (empty($delete_cvs)) {
      bims_print("No cv needed to be removed", 1, 3);
      return TRUE;
    }
    bims_print("The following CVs will be removed from BIMS", 1, 1.2);
    foreach ($delete_cvs as $cv_id => $cv_name) {
      bims_print("[$cv_id] $cv_name", 1, 1, FALSE);
    }

    // Deletes the CVs.
    if ($delete_flag) {
      foreach ((array)$delete_cvs as $cv_id => $cv_name) {
        db_delete('bims_imported_cv')
          ->condition('program_id', $program_id, '=')
          ->condition('cv_id', $cv_id, '=')
          ->execute();
        db_delete('bims_chado_cv')
          ->condition('cv_id', $cv_id, '=')
          ->execute();
      }
    }
    return TRUE;
  }

  /**
   * Cleans bims_chado_cv table.
   *
   * @param boolean $delete_flag
   *
   * @return boolean
   */
  public static function cleanBimsChadoCV($delete_flag = FALSE) {

    // Gets all the CVs.
    $sql = "
      SELECT * FROM {bims_chado_cv}
      WHERE cv_id NOT IN (SELECT cv_id FROM {chado.cv})
    ";
    $results = db_query($sql);
    $delete_cvs = array();
    while ($obj = $results->fetchObject()) {
      $delete_cvs[$obj->cv_id] = $obj->cv_name;
    }

    // Shows the results.
    if (empty($delete_cvs)) {
      bims_print("No CV needed to be removed", 1, 1.1);
      return TRUE;
    }
    bims_print("The following CVs will be removed from BIMS", 1, 1.2);
    foreach ($delete_cvs as $cv_id => $cv_name) {
      bims_print("[$cv_id] $cv_name", 1, 1, FALSE);
    }

    // Deletes the CVs.
    if ($delete_flag) {
      foreach ((array)$delete_cvs as $cv_id => $cv_name) {
        db_delete('bims_chado_cv')
          ->condition('cv_id', $cv_id, '=')
          ->execute();
      }
    }
    return TRUE;
  }

  /**
   * Returns the publically available CVs.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param string $type
   * @param integer $flag
   *
   * @return array
   */
  public static function getChadoCV(BIMS_PROGRAM $bims_program, $type = '', $flag = BIMS_OBJECT) {
    $sql = "SELECT * FROM {bims_chado_cv} WHERE crop_id = :crop_id";
    $args = array(':crop_id' => $bims_program->getCropID());
    if ($type) {
      $sql .= " AND LOWER(type) = LOWER(:type)";
      $args[':type'] = $type;
    }
    $results = db_query($sql, $args);
    $cv_arr = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $cv_arr [$obj->cv_id]= $obj->cv_name;
      }
      else {
        $cv_arr []= $obj;
      }
    }
    return $cv_arr;
  }

  /**
   * Returns all imported CVs.
   *
   * @param integer $program_id
   * @param integer $flag
   *
   * @return array
   */
  public static function getImportedCV($program_id, $flag = BIMS_OPTION) {
    $sql = "
      SELECT DISTINCT cv_id, cv_name
      FROM {bims_imported_cv}
      WHERE program_id = :program_id
      ORDER BY cv_name
    ";
    $results = db_query($sql, array(':program_id' => $program_id));
    $cv_arr = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OBJECT) {
        $cv_arr []= $obj;
      }
      else {
        $cv_arr [$obj->cv_id]= $obj->cv_name;
      }
    }
    return $cv_arr;
  }

  /**
   * Returns all unimported CVs.
   *
   * @param BIMS_PROGRAM $bims_program
   * @param integer $flag
   *
   * @return array
   */
  public static function getUnimportedCV(BIMS_PROGRAM $bims_program) {
    $sql = "
      SELECT DISTINCT cv_id, cv_name
      FROM {bims_chado_cv}
      WHERE crop_id = :crop_id AND cv_id NOT IN (
        SELECT cv_id FROM {bims_imported_cv}
        WHERE program_id = :program_id
      )
      ORDER BY cv_name
    ";
    $args = array(
      ':program_id' => $bims_program->getProgramID(),
      ':crop_id'    => $bims_program->getCropID(),
    );
    $results = db_query($sql, $args);
    $cv_arr = array();
    while ($obj = $results->fetchObject()) {
      $cv_arr [$obj->cv_id]= $obj->cv_name;
    }
    return $cv_arr;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Returns the prop array.
   */
  public function getPropArr() {
    return $this->prop_arr;
  }

  /**
   * Sets the prop array.
   *
   * @param array $prop_arr
   */
  public function setPropArr($prop_arr) {
    $this->prop_arr = $prop_arr;
  }

  /**
   * Returns the value of the given key in prop.
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}