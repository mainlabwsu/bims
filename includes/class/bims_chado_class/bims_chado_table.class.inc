<?php
/**
 * The declaration of BIMS_CHADO_TABLE class.
 *
 */
class BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $program_id = NULL;

  /**
   * Implements the class constructor.
   */
  public function __construct($progarm_id) {
    $this->program_id = $progarm_id;
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Returns the table name by type.
   *
   * @param string $type
   * @param boolean $schema
   *
   * @return object
   */
  public function getTableName($type, $schema = TRUE) {
    if ($schema) {
      return 'bims.bims_' . $this->program_id . '_bims_chado_' . $type;
    }
    return 'bims_' . $this->program_id . '_bims_chado_' . $type;
  }

  /**
   * Returns TRUE if the given ID of the object is chado (public).
   *
   * @param integer $target_id
   *
   * @return boolean
   */
  public function isChado($target_id) {
    // To be overridden by Child class.
    return FALSE;
  }

  /**
   * Returns the object.
   *
   * @param string $type
   *
   * @return object
   */
  public function getObject($type) {
    // To be overridden by Child class.
    return NULL;
  }

  /**
   * Returns the table schema by type.
   *
   * @param array $type
   *
   * @return array
   */
  public function getTableSchema($type) {
    // To be overridden by Child class.
    return array();
  }

  /**
   * Returns the all tables.
   *
   * @param integer $flag
   *
   * @return array
   */
  public function getTables($flag = BIMS_CREATE) {
    // To be overridden by Child class.
    return array();
  }

  /**
   * Returns the all associated data.
   *
   * @param integer $target_id
   *
   * @return array
   */
  public function getAssocData($target_id) {
    // To be overridden by Child class.
    return array();
  }

  /**
   * Creates tables.
   *
   * @param array $tables
   * @param boolean $recreate
   *
   * @return boolean
   */
  public function createTable($tables, $recreate = FALSE) {
    foreach ((array)$tables as $table) {

      // Gets the schema.
      $t_name    = $this->getTableName($table);
      $t_schema  = $this->getTableSchema($table);

      // Drops the table if exists and recreate flag is on.
      if ($recreate && db_table_exists($t_name)) {
        db_drop_table($t_name);
      }

      // Create a table if not exist.
      if (!db_table_exists($t_name)) {

        // Create a table.
        bims_print("Creating $t_name", 1, 2);
        db_create_table($t_name, $t_schema);

        // Adds foreign keys.
        if (array_key_exists('foreign keys', $t_schema)) {
          foreach ((array)$t_schema['foreign keys'] as $fkey => $info) {

            // Gets the table name.
            $ref_table = '';
            if (array_key_exists('ref_table', $info)) {
              $ref_table = $info['ref_table'];
            }
            else {
              $ref_class  = $info['ref_class'];
              $ref_key    = $info['ref_key'];
              $bims_chado = new $ref_class($this->program_id);
              $ref_table  = $bims_chado->getTableName($ref_key);
            }

            // Sets the SQL.
            $field_id = $info['field_id'];
            $ref_id   = $info['ref_id'];
            $on       = array_key_exists('on', $info) ? ' ON ' . $info['on'] : '';
            $sql      = "ALTER TABLE ONLY $t_name ADD CONSTRAINT $fkey FOREIGN KEY ($field_id) REFERENCES $ref_table($ref_id) $on";
            db_query($sql);
          }
        }
      }
    }
    return TRUE;
  }

  /**
   * Adds the message.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $key
   * @param string $table
   * @param array $args
   *
   * @return boolean
   */
  public function addMsg(MCL_TEMPLATE $mcl_tmpl, $key, $table, $args = array()) {
    if ($mcl_tmpl) {
      $mcl_tmpl->addMsg($key, $table, $args);
    }
  }

  /**
   * Updates the message.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $key
   * @param string $msg
   *
   * @return boolean
   */
  public function updateMsg(MCL_TEMPLATE $mcl_tmpl, $key, $msg) {
    if ($mcl_tmpl) {
      $mcl_tmpl->updateMsg($key, $msg);
    }
  }

  /**
   * Adds the default values.
   *
   * @return boolean
   */
  public function addDefValues() {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Drops tables.
   *
   * @param array $tables
   *
   * @return boolean
   */
  public function dropTable($tables) {
    foreach ((array)$tables as $table) {
      $table_name = $this->getTableName($table);
      if (db_table_exists($table_name)) {
        db_drop_table($table_name);
      }
    }
    return TRUE;
  }

  /**
   * Checks if the materialized view exists.
   *
   * @return boolean
   */
  public function exists($type = '') {
    // To be overridden by Child class.
    return TRUE;
  }

  /**
   * Returns TRUE if the given table has data.
   *
   * @return boolean
   */
  public function dataExists($table_name) {
    $sql = "SELECT COUNT(*) FROM {$table_name} LIMIT 1";
    $num = db_query($sql)->fetchField();
    return $num ? TRUE : FALSE;
  }

  /**
   * Returns the schema of BIMS_CHADO.
   *
   * @param integer $program_id
   * @param string $module
   * @param string $table
   * @param string $type
   *
   * @return array
   */
  public static function getSchema($program_id, $module = NULL, $table = NULL, $type = NULL) {

    // Sets the modules.
    $modules = array(
      'accession', 'cross', 'feature', 'genotype_call',
      'genotype', 'image', 'nd_experiment', 'phenotype_call', 'project',
      'site', 'synonym', 'ssr',
    );
    if ($module) {
      $modules = array($module);
    }

    // Sets the types.
    $types = array(
      'foreign keys', 'primary key', 'foreign keys', 'unique keys',
    );
    if ($type) {
      $types = array($type);
    }

    // Gets the schema.
    $schema = array();
    foreach ($modules as $module) {
      $class = 'BIMS_CHADO_'  . strtoupper($module);
      $bims_chado = new $class($program_id);
      $schema[$module] = array();
      $tables = $bims_chado->getTables(BIMS_CREATE);
      foreach ((array)$tables as $table) {
        $schema[$module][$table] = $bims_chado->getTableSchema($table);
      }
    }
    return $schema;
  }

  /**
   * Returns the schema of BIMS_CHADO.
   *
   * @param integer $program_id
   * @param array $params
   *
   * @return array
   */
  public static function getRelTables($program_id, $params) {

    // Gets all foreign keys.
    $rel_tables = array();
    $schema = self::getSchema($program_id);
    foreach ($schema as $module => $tables) {
      foreach ($tables as $table => $details) {
        if (array_key_exists('foreign keys', $details)) {
          foreach ((array)$details['foreign keys'] as $id => $details) {

            // Gets the table name.
            $table_name = '';
            if (array_key_exists('class', $details)) {
              $obj = new $details['class']($program_id);
              $table_name = $obj->getTableName($details['key']);
            }
            else if (array_key_exists('table', $details)) {
              $table_name = $details['table'];
            }
            else {
              continue;
            }

            // Adds the table.
            $add_flag = FALSE;
            if (array_key_exists('ref_table', $details)) {
              if ($details['ref_table'] == $params['ref_table']
                  && $details['ref_id'] == $params['ref_id']) {
                $add_flag = TRUE;
              }
            }
            else if (array_key_exists('ref_class', $details)) {
              if ($details['ref_class'] == $params['ref_class']
                  && $details['ref_key'] == $params['ref_key']
                  && $details['ref_id'] == $params['ref_id']) {
                $add_flag = TRUE;
              }
            }
            if ($add_flag) {
              if (!array_key_exists($table_name, $rel_tables)) {
                $rel_tables[$table_name][]= $details;
              }
            }
          }
        }
      }
    }
    return $rel_tables;
  }

  /**
   * Counts data in the associated tables.
   *
   * @param array $data_counts
   * @param integer $program_id
   * @param array $params
   */
  public static function getAssocDataCounts(&$data_counts, $program_id, $params) {

    // Adds no data count.
    $no_data = array_key_exists('no_data', $params) ? $params['no_data'] : FALSE;

    // Gets the related tables.
    $rel_tables = BIMS_CHADO_TABLE::getRelTables($program_id, $params);
    foreach ($rel_tables as $rel_table => $fkeys) {
      foreach ($fkeys as $fkey) {
        if (array_key_exists('class', $fkey)) {
          $class    = $fkey['class'];
          $key      = $fkey['key'];
          $field_id = $fkey['field_id'];

          // Counts the number of the data.
          $bims_chado = new $class($program_id);
          $table = $bims_chado->getTableName($key);
          $sql = "SELECT COUNT(*) FROM {$table} WHERE $field_id = :target_id";
          $num = db_query($sql, array(':target_id' => $target_id))->fetchField();
       //   if ($no_data && $num) {
            $data_counts[$table] = $num;
        //  }
        }
      }
    }
    return ;
  }

  /**
   * Counts data in the target tables.
   *
   * @param array $data_counts
   * @param integer $program_id
   * @param string $target_type
   * @param integer $target_id
   * @param boolean $no_data
   */
  public static function getTargetDataCounts(&$data_counts, $program_id, $target_type, $target_id, $no_data = FALSE) {

    // Gets BIMS_CHADO_SYNONYM.
    $bims_classes = array(
      'BIMS_CHADO_IMAGE'    => 'eimage_link',
      'BIMS_CHADO_SYNONYM'  => 'synonym',
    );
    $msg = '';
    foreach ($bims_classes as $bims_class => $key) {
      $bims_chado = new $bims_class($program_id);
      $table = $bims_chado->getTableName($key);
      $args = array(
        ':target_type'  => $target_type,
        ':target_id'    => $target_id,
      );
      $sql = "
        SELECT COUNT(*) FROM {$table}
        WHERE LOWER(target_type) = LOWER(:target_type)
        AND target_id = :target_id
      ";
      $num = db_query($sql, $args)->fetchField();
      //   if ($no_data && $num) {
        $data_counts[$table] = $num;
      //}
    }
  }

  /**
   * The next value of the sequence.
   *
   * @param string $table_name
   *
   * @return integer
   */
  public function getNextval($table_name) {
    // To be overridden by Child class.
    return 0;
  }

  /**
   * Returns the object by keys.
   *
   * @param array $table_name
   * @param array $keys
   *
   * @return object
   */
  public function byKey($table_name, $keys) {

    // Adds the conditions.
    $conditions = '';
    $args = array();
    foreach ($keys as $key => $value) {
      $conditions .= " AND LOWER($key) = LOWER(:$key) ";
      $args[":$key"] = $value;
    }

    // Gets the data.
    $sql = "
      SELECT T.*
      FROM {$table_name} T
      WHERE 1=1 $conditions
    ";
    return db_query($sql, $args)->fetchObject();
  }

/**
   * Returns the object by keys.
   *
   * @param string $table_key
   * @param array $keys
   *
   * @return object
   */
  public function byTKey($table_key, $keys) {

    // Gets the table name and schema.
    $t_name   = $this->getTableName($table_key);
    $t_schema = $this->getTableSchema($table_key);

    // Adds the conditions.
    $conditions = '';
    $args = array();
    foreach ($keys as $key => $value) {
      $type = 'non_char';
      if (isset($t_schema['fields'][$key])) {
        $type = $t_schema['fields'][$key]['type'];
      }
      if (preg_match("/^(varchar|text)/", $type)) {
        $conditions .= " AND LOWER($key) = LOWER(:$key) ";
      }
      else {
        $conditions .= " AND $key = :$key ";
      }
      $args[":$key"] = $value;
    }

    // Gets the data.
    $sql = "
      SELECT T.*
      FROM {$t_name} T
      WHERE 1=1 $conditions
    ";
    return db_query($sql, $args)->fetchObject();
  }

  /**
   * Returns the regular expression of the separator.
   *
   * @param string
   */
  public static function getSepRegex($separator) {
    return ($separator) ? "/$separator/" : '/@@@@@/';
  }

  /**
   * Gets the property.
   *
   * @param string $table_name
   * @param string $target_field
   * @param integer $target_id
   * @param integer $cv_name
   * @param integer $cvterm_name
   * @param string $separator
   */
  public function getProperty($table_name, $target_field, $target_id, $cv_name, $cvterm_name, $separator = '') {

    // Checks the type.
    $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
    if ($cvterm) {
      $type_id = $cvterm->getCvtermID();

      // Gets the properties.
      $sql = "
        SELECT P.value
        FROM {$table_name} P
        WHERE P.type_id = :type_id AND P.$target_field = :$target_field
          AND P.value != '' AND P.value IS NOT NULL
        ORDER BY P.rank
      ";
      $args = array(
        ":$target_field"  => $target_id,
        ':type_id'        => $type_id,
      );
      $results = db_query($sql, $args);

      // Returns the lowest ranked one.
      if ($separator) {
        $props = array();
        while ($value = $results->fetchField()) {
          if ($value) {
            $props []= $value;
          }
        }
        return implode($separator, $props);
      }
      else {
        $value = $results->fetchField();
        return ($value) ? $value : '';
      }
    }
    return '';
  }

  /**
   * Adds one or more properties.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $target_table
   * @param string $primary_key
   * @param string $table_key
   * @param string $target_field
   * @param integer $target_id
   * @param integer $type_id
   * @param string $value
   * @param string $separator
   *
   * @return boolean
   */
  public function addProperty(MCL_TEMPLATE $mcl_tmpl = NULL, $target_table, $primary_key, $table_key, $target_field, $target_id, $type_id, $value, $separator = '') {
    if ($value || $value == '0') {

      // Gets the table name.
      $table_name = $this->getTableName($table_key);

      // Adds new properties.
      if ($separator) {
        $value_arr = preg_split($this->getSepRegex($separator), $value, -1, PREG_SPLIT_NO_EMPTY);

        // Checks duplication before adding new properties.
        // We add multiple valuse with different ranks. So check duplication
        // for type_id andvalue.
        $err_flag  = FALSE;
        $args      = NULL;
        foreach ($value_arr as $val) {
          $val = trim($val);
          $args = array(
            $target_field => $target_id,
            'type_id'     => $type_id,
            'value'       => $val,
          );
          $obj = $this->byTKey($table_key, $args);
          if ($obj) {
            $this->addMsg($mcl_tmpl, 'D', $table_name, $args);
          }
          else {
            $rank_keys = array(
              $target_field => $target_id,
              'type_id'     => $type_id,
            );
            $args['rank']       = $this->getNextRank($table_name, $rank_keys);
            $args[$primary_key] = $this->getNextval($target_table);

            // Inserts the record.
            try {
              db_insert($table_name)
                ->fields($args)
                ->execute();
              $this->addMsg($mcl_tmpl, 'N', $table_name, $args);
            }
            catch (Exception $e) {
              $this->addMsg($mcl_tmpl, 'E', $table_name, $args);
              $err_flag = FALSE;
            }
          }
        }
        if (!$err_flag) {
          return TRUE;
        }
      }
      else {

        // Checks duplication before adding a new property.
        //   We assume that the property holds one value. So check duplication
        //   for only type_id.
        $args = array(
          $target_field => $target_id,
          'type_id'     => $type_id,
        );
        $obj = $this->byTKey($table_key, $args);
        if ($obj) {
          $this->addMsg($mcl_tmpl, 'D', $table_name, $args);
        }
        else {
          $args['value']      = $value;
          $args[$primary_key] = $this->getNextval($target_table);

          // Inserts the record.
          try {
            db_insert($table_name)
              ->fields($args)
              ->execute();
            $this->addMsg($mcl_tmpl, 'N', $table_name, $args);
            return TRUE;
          }
          catch (Exception $e) {
            $msg = $e->getMessage();
            $this->addMsg($mcl_tmpl, 'E', $table_name, $args);
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * Adds a property with the provide rank.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $target_table
   * @param string $primary_key
   * @param string $table_key
   * @param string $target_field
   * @param integer $target_id
   * @param integer $type_id
   * @param string $value
   * @param string $rank
   *
   * @return boolean
   */
  public function addPropertyRank(MCL_TEMPLATE $mcl_tmpl = NULL, $target_table, $primary_key, $table_key, $target_field, $target_id, $type_id, $value, $rank = 0) {
    if ($value || $value == '0') {

      // Gets the table name.
      $table_name = $this->getTableName($table_key);

      // Checks duplication before adding a new property.
      $args = array(
        $target_field => $target_id,
        'type_id'     => $type_id,
        'rank'        => $rank,
      );
      $obj = $this->byTKey($table_key, $args);
      if ($obj) {
        $this->addMsg($mcl_tmpl, 'D', $table_name, $args);
      }
      else {
        $args['value']      = $value;
        $args[$primary_key] = $this->getNextval($target_table);

        // Inserts the record.
        try {
          db_insert($table_name)
            ->fields($args)
            ->execute();
          $this->addMsg($mcl_tmpl, 'N', $table_name, $args);
          return TRUE;
        }
        catch (Exception $e) {
          $this->addMsg($mcl_tmpl, 'E', $table_name, $args);
        }
      }
    }
    return FALSE;
  }

  /**
   * Adds one or more properties
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $target_table
   * @param string $primary_key
   * @param string $table_key
   * @param string $subject_key
   * @param integer $subject_value
   * @param string $object_key
   * @param integer $object_value
   * @param integer $type_id_relationship
   *
   * @return boolean
   */
  public function addRelationship(MCL_TEMPLATE $mcl_tmpl = NULL, $target_table, $primary_key, $table_key, $subject_key, $subject_value, $object_key, $object_value, $type_id_relationship) {

    // Gets the table name.
    $table_name = $this->getTableName($table_key);

    // Checks duplication before adding new relationship.
    $args = array(
      $subject_key  => $subject_value,
      $object_key   => $object_value,
      'type_id'     => $type_id_relationship,
    );
    $obj = $this->byTKey($table_key, $args);
    if ($obj) {
      $this->addMsg($mcl_tmpl, 'D', $table_name, $args);
    }
    else {
      $args[$primary_key] = $this->getNextval($target_table);

      // Inserts the record.
      try {
        db_insert($table_name)
          ->fields($args)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', $table_name, $args);
        return TRUE;
      }
      catch (Exception $e) {
        $this->addMsg($mcl_tmpl, 'E', $table_name, $args);
      }
    }
    return FALSE;
  }

  /**
   * Returns the next empty rank.
   *
   * @param string $table_name
   * @param array $rank_keys
   *
   * @return integer
   */
  public function getNextRank($table_name, $rank_keys) {
    $sql = "SELECT COUNT(rank) FROM $table_name WHERE 1=1";
    $args = array();
    foreach ($rank_keys as $target_field => $value) {
      $sql .= " AND $target_field = :$target_field ";
      $args[":$target_field"] = $value;
    }
    $count = db_query($sql, $args)->fetchField();
    if (!$count) {
      return 0;
    }
    $sql = "SELECT MAX(rank) FROM $table_name WHERE 1=1";
    $args = array();
    foreach ($rank_keys as $target_field => $value) {
      $sql .= " AND $target_field = :$target_field ";
      $args[":$target_field"] = $value;
    }
    $rank = db_query($sql, $args)->fetchField();
    return $rank + 1;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the program ID.
   *
   * @retrun various
   */
  public function getProgramID() {
    return $this->program_id;
  }

  /**
   * Retrieves BIMS_PROGRAM.
   *
   * @retrun BIMS_PROGRAM
   */
  public function getProgram() {
    return BIMS_PROGRAM::byID($this->program_id);
  }
}