<?php
/**
 * The declaration of BIMS_CHADO_SITE class.
 *
 */
class BIMS_CHADO_SITE extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $nd_geolocation_id      = NULL;
  protected $nd_geolocationprop_id  = NULL;
  protected $nd_geolocation         = NULL;
  protected $nd_geolocationprop     = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->nd_geolocation     = $this->getTableName('site');
    $this->nd_geolocationprop = $this->getTableName('siteprop');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'nd_geolocation') {
      $sql = "select nextval('chado.nd_geolocation_nd_geolocation_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_geolocationprop') {
      $sql = "select nextval('chado.nd_geolocationprop_nd_geolocationprop_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getAssocData()
   */
  public function getAssocData($nd_geolocation_id) {

    // Initializes the data counts.
    $data_counts = array();

    // Counts the data in BIMS_CHADO.
    $data_count = bims_count_data_bc('site', $nd_geolocation_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['bims_chado'] = $data_count;
    }

    // Counts the data in mviews.
    $data_count = bims_count_data_mview('site', $nd_geolocation_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['mview'] = $data_count;
    }

    // Counts the data in public schema.
    $data_count = bims_count_data_public('site', $nd_geolocation_id);
    if (!empty($data_count)) {
      $data_counts['public'] = $data_count;
    }
    return $data_counts;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('siteprop', 'site');
    }
    return array('site', 'siteprop');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Returns the site object of 'Not Avalilable'. If not exists, add it.
   */
  public function getNotAvailable() {

    // Gets the site_name of 'Not Available'.
    $prefix     = BIMS_PROGRAM::getPrefixByID($this->program_id);
    $site_name  =  $prefix . 'Not Available';

    // Gets site.
    $site = $this->byTKey('site', array('description' => $site_name));
    if (!$site) {

      // Adds 'Not Available'.
      $nd_geolocation_id = $this->addSite(NULL, array('description' => $site_name));
      $site = $this->byTKey('site', array('nd_geolocation_id' => $nd_geolocation_id));
    }
    return $site;
  }

  /**
   * Returns nd_geolocaiton_id of 'Not Avalilable'.
   *
   * @return integer
   */
  public function getNotAvailableID() {
    $site_obj = $this->getNotAvailable();
    return $site_obj->nd_geolocation_id;
  }

  /**
   * @see BIMS_CHADO_TABLE::addDefValues()
   */
  public function addDefValues($tables = array()) {

    // Gets the default nd_geolocation.
    $nd_geolocation_id = MCL_DUMMY_VAR::getIdByName('ND_GEOLOCATION_ID');

    // Checks for a duplication.
    $obj = $this->byTKey('site', array('nd_geolocation_id' => $nd_geolocation_id));
    if ($obj) {
      return TRUE;
    }

    // Adds the default nd_geolocation.
    $mcl_location = MCL_CHADO_LOCATION::byID($nd_geolocation_id);
    if ($mcl_location) {
      $desc = $mcl_location->getDescription();
      $table = $this->getTableName('site');
      $sql = "INSERT INTO $table (nd_geolocation_id, description) VALUES ($nd_geolocation_id, '$desc')";
      db_query($sql);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Checks the existence of site. If not, write the error messasge to the log.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $site_name
   *
   * @return boolean
   */
  public function checkSite(MCL_TEMPLATE $mcl_tmpl = NULL, $site_name) {
    $flag = TRUE;
    if ($site_name) {

      // Sets the arguments.
      $args = array('description' => $site_name);
      $obj = $this->byTKey('site', $args);
      if (!$obj) {
        $this->addMsg($mcl_tmpl, 'E', "$site_name not found in site");
        $flag = FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Deletes the data in BIMS_CHADO.
   *
   * @param array $nd_geolocation_ids
   *
   * @return boolean
   */
  public function deleteDataBIMS($nd_geolocation_ids) {

    // Skips if this program is a Public Chado program.
    $bims_program = BIMS_PROGRAM::byID($this->program_id);
    if ($bims_program->isPublicChado()) {
      return TRUE;
    }
    return TRUE;
  }

  /**
   * Deletes the data in mview.
   *
   * @param array $nd_geolocation_ids
   *
   * @return boolean
   */
  public function deleteDataMView($nd_geolocation_ids) {

    // Prepare for deletion.
    $fkeys_delete = array(
      'BIMS_MVIEW_LOCATION'   => array('nd_geolocation_id'),
      'BIMS_MVIEW_PHENOTYPE'  => array('nd_geolocation_id'),
      'BIMS_MVIEW_STOCK_LOC'  => array('nd_geolocation_id'),
    );

    // Deletes the data.
    return bims_delete_data_mview($this->program_id, $nd_geolocation_ids, $fkeys_delete);
  }

  /**
   * Deletes the sites.
   *
   * @param array $nd_geolocation_ids
   *
   * @return boolean
   */
  public function deleteSites($nd_geolocation_ids) {

    $transaction = db_transaction();
    try {

      // Gets the callbacks.
      $callbacks = $this->deleteCallbacks($nd_geolocation_ids);

      // Deletes the data in BIMS_CHADO.
      if (!$this->deleteDataBIMS($nd_geolocation_ids)) {
        throw new Exception("Fail to delete the data in BIMS_CHADO");
      }

      // Deletes the data in BIMS_MVIEW.
      if (!$this->deleteDataMView($nd_geolocation_ids)) {
        throw new Exception("Fail to delete the data in BIMS schema");
      }

      // Deletes the sites.
      $table = $this->getTableName('site');
      if (db_table_exists($table)) {
        $id_list = implode(',', $nd_geolocation_ids);
        $sql = "DELETE FROM {$table} WHERE nd_geolocation_id IN ($id_list)";
        db_query($sql);
      }

      // Submit the callbacks.
      if (!empty($callbacks)) {
        if (!bims_submit_drush_cmds($this->program_id, 'delete-location', $callbacks)) {
          throw new Exception("Error : Failed to submit callbacks");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets the callbacks called after deletion.
   *
   * @param array $nd_geolocation_ids
   *
   * @return string
   */
  public function deleteCallbacks($nd_geolocation_ids) {
    $callbacks = array();

    // Gets the related projects, stocks and crosses.
    $program_id     = $this->program_id;
    $project_id_arr = array();
    $stock_id_arr   = array();
    $cross_id_arr   = array();

    // BIMS_MVIEW_PHENOTYPE.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    $m_phenotype  = $bm_phenotype->getMView();
    $id_list      = implode(",", $nd_geolocation_ids);
    $sql = "
      SELECT DISTINCT project_id, stock_id, nd_experiment_id
      FROM {$m_phenotype} WHERE nd_geolocation_id IN ($id_list)
    ";
    $results = db_query($sql);
    while ($obj = $results->fetchObject()) {
      $project_id_arr[$obj->project_id] = TRUE;
      $stock_id_arr[$obj->stock_id] = TRUE;
      if ($obj->nd_experiment_id) {
        $cross_id_arr[$obj->nd_experiment_id] = TRUE;
      }
    }

    // Adds the callbacks.
    if (!empty($project_id_arr)) {
      $id_list    = implode(':', array_keys($project_id_arr));
      $options     = " --group=project --group_ids=$id_list --mview=stats";
      $callbacks  []= "bims-update-mview-phenotype $program_id $options;";
    }
    if (!empty($stock_id_arr)) {
      $id_list    = implode(':', array_keys($stock_id_arr));
      $options    = " --group=stock --group_ids=$id_list --mview=stats-loc";
      $callbacks  []= "bims-update-mview-stock $program_id $options;";
    }
    if (!empty($cross_id_arr)) {
      $id_list    = implode(':', array_keys($cross_id_arr));
      $options    = " --group=cross --group_ids=$id_list --mview=stats";
      $callbacks  []= "bims-update-mview-cross $program_id $options;";
    }
    return $callbacks;
  }

  /**
   * Adds a new site.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addSite(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array('description' => $details['description']);
    $site = $this->byTKey('site', $keys);
    if ($site) {
      if ($mcl_tmpl) {
        $this->addMsg($mcl_tmpl, 'D', 'bc_stie', $keys);
      }
      return $site->nd_geolocation_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets the next value.
        $nd_geolocation_id = $this->getNextval('nd_geolocation');

        // Adds the data.
        $fields = array(
          'nd_geolocation_id' => $nd_geolocation_id,
          'description'       => $details['description'],
        );
        if ($details['altitude'] || $details['altitude'] == '0') {
          $fields['altitude'] = $details['altitude'];
        }
        if ($details['latitude'] || $details['latitude'] == '0') {
          $fields['latitude'] = $details['latitude'];
        }
        if ($details['longitude'] || $details['longitude'] == '0') {
          $fields['longitude'] = $details['longitude'];
        }
        if ($details['geodetic_datum'] || $details['geodetic_datum'] == '0') {
          $fields['geodetic_datum'] = $details['geodetic_datum'];
        }

        // Inserts the record.
        db_insert($this->nd_geolocation)
          ->fields($fields)
          ->execute();
        if ($mcl_tmpl) {
          $this->addMsg($mcl_tmpl, 'N', 'bc_stie', $fields);
        }
        return $nd_geolocation_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return NULL;
  }

  /**
   * Updates the site.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateSite($details) {

    // Updates the feature.
    $transaction = db_transaction();
    try {

      // Gets the variables.
      $nd_geolocation_id  = $details['nd_geolocation_id'];
      $description        = $details['description'];
      $latitude           = $details['latitude'];
      $longitude          = $details['longitude'];
      $altitude           = $details['altitude'];
      $type               = $details['type'];
      $country            = $details['country'];
      $state              = $details['state'];
      $region             = $details['region'];
      $address            = $details['address'];

      // --------------------------------- //
      // Updates the properties of the feature.
      $fields = array(
        'nd_geolocation_id' => $nd_geolocation_id,
        'description'       => $description,
        'latitude'          => $latitude,
        'longitude'         => $longitude,
        'altitude'          => $altitude,
      );

      // Updates the site.
      db_update($this->getTableName('site'))
        ->fields($fields)
        ->condition('nd_geolocation_id', $nd_geolocation_id, '=')
        ->execute();

      // Updates the site type.
      $cvterm_id_type = MCL_CHADO_CVTERM::getCvterm(SITE_CV, 'type')->getCvtermID();
      db_update($this->getTableName('siteprop'))
        ->fields(array('value' => $type))
        ->condition('nd_geolocation_id', $nd_geolocation_id, '=')
        ->condition('type_id', $cvterm_id_type, '=')
        ->execute();

      // --------------------------------- //
      // Updates site properties.


      }
      catch (Exception $e) {
        $transaction->rollback();
        drupal_set_message($e->getMessage(), 'error');
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return FALSE;
      }
    return TRUE;
  }

  /**
   * Retrieves the property. It returns all properties if the separator is
   * provided.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $separator
   *
   * @return string
   */
  public function getProp($cv_name, $cvterm_name, $separator = '') {
    return $this->getProperty($this->nd_geolocationprop, 'nd_geolocation_id', $this->nd_geolocation_id, $cv_name, $cvterm_name, $separator);
  }

  /**
   * Adds a property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $value
   * @param string $separator
   *
   * @return boolean
   */
  public function addProp(MCL_TEMPLATE $mcl_tmpl = NULL, $cv_name, $cvterm_name, $value, $separator = '') {
    if ($value != '') {
      $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
      if ($cvterm) {
        $type_id = $cvterm->getCvtermID();
        return $this->addProperty($mcl_tmpl, 'nd_geolocationprop', 'nd_geolocationprop_id', 'siteprop', 'nd_geolocation_id', $this->nd_geolocation_id, $type_id, $value, $separator);
      }
    }
    return TRUE;
  }

  /**
   * Adds a property with the provided rank.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $value
   * @param integer $rank
   *
   * @return boolean
   */
  public function addRankProp(MCL_TEMPLATE $mcl_tmpl = NULL, $cv_name, $cvterm_name, $value, $rank = 0) {
    if ($value != '') {
      $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
      if ($cvterm) {
        $type_id = $cvterm->getCvtermID();
        return $this->addPropertyRank($mcl_tmpl, 'nd_geolocationprop', 'nd_geolocationprop_id', 'siteprop', 'nd_geolocation_id', $this->nd_geolocation_id, $type_id, $value, $rank);
      }
    }
    return TRUE;
  }

  /**
   * Adds a property by ID.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $type_id
   * @param string $value
   *
   * @return boolean
   */
  public function addPropByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type_id, $value) {
    return $this->addProperty($mcl_tmpl, 'nd_geolocationprop', 'nd_geolocationprop_id', 'siteprop', 'nd_geolocation_id', $this->nd_geolocation_id, $type_id, $value, $separator);
  }

  /**
   * Adds a property by ID.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $type_id
   * @param string $value
   * @param integer $rank
   *
   * @return boolean
   */
  public function addPropRankByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type_id, $rank = 0) {
    return $this->addProperty($mcl_tmpl, 'nd_geolocationprop', 'nd_geolocationprop_id', 'siteprop', 'nd_geolocation_id', $this->nd_geolocation_id, $type_id, $value, $rank);
  }

  /**
   * Returns nd_geolocation_id of the provided site name. If site name
   * is empty, return the default nd_geolocation_id.
   *
   * @param string $site_name
   *
   * @return integer nd_geolocation_id
   */
  public function getLocationIDBySite($site_name) {

    // Sets the default nd_geolocation_id.
    $nd_geolocation_id = $this->getNotAvailableID();

    // Gets the nd_geolocation_id.
    if ($site_name) {

      // Gets site.
      $site = $this->byTKey('site', array('description' => $site_name));
      if ($site) {
        $nd_geolocation_id = $site->nd_geolocation_id;
      }
      else {
        return NULL;
      }
    }
    return $nd_geolocation_id;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'site') {
      return self::_getSchemaSite();
    }
    else if ($type == 'siteprop') {
      return self::_getSchemaSiteprop();
    }
    return array();
  }

  /**
   * Returns the schema for site.
   *
   * @return array
   */
  private function _getSchemaSite() {
    return array(
      'description' => 'The custom table for site.',
      'fields' => array(
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'description' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'altitude' => array(
          'type' => 'text',
        ),
        'latitude' => array(
          'type' => 'text',
        ),
        'longitude' => array(
          'type' => 'text',
        ),
        'geodetic_datum' => array(
          'type' => 'text',
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_geolocation_id'),
      'unique keys' => array(
        'ukey_001' => array('description')
      ),
    );
  }

  /**
   * Returns the schema for site propeprty.
   *
   * @return array
   */
  private function _getSchemaSiteprop() {
    return array(
      'description' => 'The custom table for site property.',
      'fields' => array(
        'nd_geolocationprop_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_geolocationprop_id'),
      'unique keys' => array(
        'ukey_001' => array('nd_geolocation_id', 'type_id', 'rank')
      ),
      'foreign keys' => array(
        'fkey_bims_siteprop_001' => array(
          'class'     => 'BIMS_CHADO_SITE',
          'key'       => 'siteprop',
          'field_id'  => 'nd_geolocation_id',
          'ref_class' => 'BIMS_CHADO_SITE',
          'ref_key'   => 'site',
          'ref_id'    => 'nd_geolocation_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the nd_geolocation  ID.
   *
   * @retrun integer
   */
  public function getNdGeolocationID() {
    return $this->nd_geolocation_id;
  }

  /**
   * Sets the nd_geolocation  ID.
   *
   * @param integer $nd_geolocation_id
   */
  public function setNdGeolocationID($nd_geolocation_id) {
    $this->nd_geolocation_id = $nd_geolocation_id;
  }

  /**
   * Retrieves the nd_geolocationprop ID.
   *
   * @retrun integer
   */
  public function getNdGeolocationpropID() {
    return $this->nd_geolocationprop_id;
  }

  /**
   * Sets the nd_geolocationprop ID.
   *
   * @param integer $nd_geolocationprop_id
   */
  public function setNdGeolocationpropID($nd_geolocationprop_id) {
    $this->nd_geolocationprop_id = $nd_geolocationprop_id;
  }
}
