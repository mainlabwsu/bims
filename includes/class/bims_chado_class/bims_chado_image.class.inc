<?php
/**
 * The declaration of BIMS_CHADO_IMAGE class.
 *
 */
class BIMS_CHADO_IMAGE extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $eimage_id        = NULL;
  protected $eimageprop_id    = NULL;
  protected $stock_image_id   = NULL;
  protected $cvterm_image_id  = NULL;
  protected $eimage           = NULL;
  protected $eimageprop       = NULL;
  protected $stock_image      = NULL;
  protected $cvterm_image     = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->eimage       = $this->getTableName('eimage');
    $this->eimageprop   = $this->getTableName('eimageprop');
    $this->eimage_link  = $this->getTableName('eimage_link');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'eimage') {
      $sql = "select nextval('chado.eimage_eimage_id_seq'::regclass)";
    }
    else if ($table_name == 'eimageprop') {
      $sql = "select nextval('chado.eimageprop_eimageprop_id_seq'::regclass)";
    }
    else if ($table_name == 'stock_image') {
      $sql = "select nextval('chado.stock_image_stock_image_id_seq'::regclass)";
    }
    else if ($table_name == 'cvterm_image') {
      $sql = "select nextval('chado.cvterm_image_cvterm_image_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getAssocData()
   */
  public function getAssocData($eimage_id) {

    // Initializes the data counts.
    $data_counts = array();

    // Counts the data in bims_chado.
    $data_count = bims_count_data_bc('image', $eimage_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['bims_chado'] = $data_count;
    }

    // Counts the data in mviews.
    $data_count = bims_count_data_mview('image', $eimage_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['mview'] = $data_count;
    }

    // Counts the data in public schema.
    $data_count = bims_count_data_public('image', $eimage_id);
    if (!empty($data_count)) {
      $data_counts['public'] = $data_count;
    }
    return $data_counts;
  }

  /**
   * Deletes the data in mview.
   *
   * @param array $eimage_id
   *
   * @return boolean
   */
  public function deleteDataMView($eimage_ids) {

    // Gets foreign keys.
    $fkeys_delete = array(
      'BIMS_MVIEW_IMAGE'      => array('eimage_id'),
      'BIMS_MVIEW_IMAGE_TAG'  => array('eimage_id'),
    );

    // Deletes the data.
    return bims_delete_data_mview($this->program_id, $eimage_ids, $fkeys_delete);
  }

  /**
   * Deletes the image.
   *
   * @param array $eimage_ids
   *
   * @return boolean
   */
  public function deleteImage($eimage_ids) {

    $transaction = db_transaction();
    try {

      // Gets the callbacks.
      $callbacks = $this->deleteCallbacks($eimage_ids);

      // Deletes the data in BIMS_MVIEW.
      if (!$this->deleteDataMView($eimage_ids)) {
        throw new Exception("Fail to delete the data in BIMS schema");
      }

      // Deletes the images.
      $table = $this->getTableName('eimage');
      if (db_table_exists($table)) {
        $id_list = implode(',', $nd_geolocation_ids);
        $sql = "DELETE FROM {$table} WHERE nd_geolocation_id IN ($id_list)";
        db_query($sql);
      }

      // Submit the callbacks.
      if (!empty($callbacks)) {
        if (!bims_submit_drush_cmds($this->program_id, 'delete-accession', $callbacks)) {
          throw new Exception("Error : Failed to submit callbacks");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets the callbacks called after deletion.
   *
   * @param array $eimage_ids
   *
   * @return string
   */
  public function deleteCallbacks($eimage_ids) {
   return array();
  }

  /**
   * Deletes the images associated with the cvterm.
   *
   * @param integer $target_type
   * @param integer $target_id
   *
   * @return boolean
   */
  public function deleteImageByTarget($target_type, $target_id) {

    $transaction = db_transaction();
    try {

      // Deletes the images by target.
      $tbl_eimage       = $this->getTableName('eimage');
      $tbl_eimage_link  = $this->getTableName('eimage_link');
      $sql = "
        DELETE FROM {$tbl_eimage} WHERE eimage_id IN (
          SELECT eimage_id FROM {$tbl_eimage_link}
          WHERE target_id = :target_id AND LOWER(target_type) = LOWER(:target_type)
        )
      ";
      $args = array(
        ':target_type' => $target_type,
        ':target_id'  => $target_id,
      );
      db_query($sql, $args);
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('eimage_link', 'eimageprop', 'eimage');
    }
    return array('eimage', 'eimageprop', 'eimage_link');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Adds a new image.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $type
   * @param string $target
   *
   * @return boolean
   */
  public function checkTarget(MCL_TEMPLATE $mcl_tmpl = NULL, $type, $target) {

    // Checks the accession.
    if ($type == 'stock') {
      $bc_accession = new BIMS_CHADO_ACCESSION($this->getProgramID());
      $accession = $bc_accession->getAccessionByName($target);
      if (!$accession) {
        $this->addMsg($mcl_tmpl, 'E', "The accession '$target' not found in stock table");
        return FALSE;
      }
    }

    // Checks the descriptor.
    else if ($type == 'trait') {
      $bims_program = BIMS_PROGRAM::byID($this->getProgramID());
      $cv_id = $bims_program->getCvID('descriptor');
      $descriptor = MCL_CHADO_CVTERM::getCvtermByCvID($cv_id, $target);
      if (!$descriptor) {
        $this->addMsg($mcl_tmpl, 'E', "The descriptor '$target' not found in cvterm table");
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Links the image with the target.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $type
   * @param string $target
   *
   * @return boolean
   */
  public function linkTarget(MCL_TEMPLATE $mcl_tmpl = NULL, $type, $target) {

    // Links with the stock.
    if ($type == 'stock' || $type == 'sample') {
      $bc_accession = new BIMS_CHADO_ACCESSION($this->getProgramID());
      $accession = $bc_accession->getAccessionByName($target);
      if ($accession) {
        return $this->linkTargetByID($mcl_tmpl, $type, $accession->stock_id);
      }
    }

    // Links with the descriptor.
    else if ($type == 'trait') {
      $bims_program = BIMS_PROGRAM::byID($this->getProgramID());
      $cv_id = $bims_program->getCvID('descriptor');
      $descriptor = MCL_CHADO_CVTERM::getCvtermByCvID($cv_id, $target);
      if ($descriptor) {
        return $this->linkTargetByID($mcl_tmpl, $type, $descriptor->getCvtermID());
      }
    }
    return FALSE;
  }

  /**
   * Links the image with the target by ID of the target.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $type
   * @param integer $target_id
   *
   * @return boolean
   */
  public function linkTargetByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type, $target_id) {

    // Sets target type.
    $target_type = '';
    if ($type == 'stock' || $type == 'sample') {
      $target_type = 'accession';
    }
    else if ($type == 'trait') {
      $target_type = 'cvterm';
    }
    if (!($target_id && $target_type)) {
      return FALSE;
    }

    // Checks for duplication.
    $keys = array('eimage_id' => $this->eimage_id);
    $eimage_link = $this->byTKey('eimage_link', $keys);
    if ($eimage_link) {
      $this->addMsg($mcl_tmpl, 'D', 'eimage_link', $keys);
    }
    else {
      $transaction = db_transaction();
      try {

        // Links the image with the target..
        $fields = array(
          'eimage_id'     => $this->eimage_id,
          'target_id'     => $target_id,
          'target_type'  => $target_type,
        );

        // Inserts the record.
        db_insert($this->getTableName('eimage_link'))
          ->fields($fields)
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Returns the preficx of the target.
   *
   * @param string $type
   * @param integer $target_id
   *
   * @return string
   */
  public function getPrefixByID($type, $target_id) {
    if ($type == 'stock' || $type == 'sample') {
      return 's-' . $target_id . '-';
    }
    else if ($type == 'trait') {
      return 'c-' . $target_id . '-';
    }
    return '';
  }

  /**
   * Returns the preficx of the target.
   *
   * @param string $type
   * @param string $target
   *
   * @return string
   */
  public function getPrefix($type, $target) {

    // Checks the accession.
    $target_id = NULL;
    if ($type == 'stock') {
      $bc_accession = new BIMS_CHADO_ACCESSION($this->getProgramID());
      $accession = $bc_accession->getAccessionByName($target);
      if ($accession) {
        $target_id = $accession->stock_id;
      }
    }

    // Checks the descriptor.
    else if ($type == 'trait') {
      $bims_program = BIMS_PROGRAM::byID($this->getProgramID());
      $cv_id = $bims_program->getCvID('descriptor');
      $descriptor = MCL_CHADO_CVTERM::getCvtermByCvID($cv_id, $target);
      if ($descriptor) {
        $target_id = $descriptor->getCvtermID();
      }
    }
    if ($target_id) {
      return $this->getPrefixByID($type, $target_id);
    }
    return '';
  }

  /**
   * Adds new images for sample.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $stock_id
   * @param array $cvterm_id
   * @param array $filenames
   * @param array $separator
   *
   * @return boolean
   */
  public function addSampleImage(MCL_TEMPLATE $mcl_tmpl = NULL, $stock_id, $cvterm_id, $filenames, $separator = '') {
    $no_error = TRUE;
    if ($filenames) {
      $image_uris = preg_split($this->getSepRegex($separator), $filenames, -1, PREG_SPLIT_NO_EMPTY);
      foreach ($image_uris as $image_uri) {
        $image_uri = trim($image_uri);

        // Updates the filename. BIMS needs to remove the path and adds stock ID
        // as a prefix.
        $filename = $this->getPrefixByID('sample', $stock_id) . basename($image_uri);

        // Adds an image.
        $details = array(
          'image_uri' => $filename,
          'type'      => 'sample',
        );
        $eimage_id = $this->addImage($mcl_tmpl, $details);
        if ($eimage_id) {
          $this->setEimageID($eimage_id);

          // Adds properties.
          $this->addProp($mcl_tmpl, 'SITE_CV', 'photo_descriptor', $cvterm_id);

          // Links the image with the sample.
          $this->linkTargetByID($mcl_tmpl, 'sample', $stock_id);
        }
        else {
          $this->addMsg($mcl_tmpl, 'E', "Failed to add an image [$image_url]");
          $no_error = FALSE;
        }
      }
    }
    return $no_error;
  }

  /**
   * Adds a photo.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   *
   * @param string $filepath
   * @param string $overwrite
   *
   * @return boolean
   */
  public function addPhoto(MCL_TEMPLATE $mcl_tmpl = NULL, $filepath, $overwrite = FALSE) {

    // Checks filename for a duplication.
    $filename = basename($filepath);
    if (!$overwrite) {
      $eimage = $this->byTKey('eimage', array('filename' => $filename));
      if (!$eimage) {



        // Creates thumbnail.



      }
    }
    return TRUE;
  }

  /**
   * Adds a new image.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addImage(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array('image_uri' => $details['image_uri']);
    $eimage = $this->byTKey('eimage', $keys);
    if ($eimage) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_eimage', $keys);
      return $eimage->eimage_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets the next value.
        $eimage_id = $this->getNextval('eimage');

        // Adds the data.
        $fields = array(
          'eimage_id'   => $eimage_id,
          'eimage_type' => $details['type'],
          'image_uri'   => $details['image_uri'],
        );

        // Inserts the record.
        db_insert($this->getTableName('eimage'))
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'bc_eimage', $fields);
        return $eimage_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('MCL', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return NULL;
  }

  /**
   * Retrieves the property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $separator
   *
   * @return string
   */
  public function getProp($cv_name, $cvterm_name, $separator = '') {
    return $this->getProperty($this->eimageprop, 'eimage_id', $this->eimage_id, $cv_name, $cvterm_name, $separator);
  }

  /**
   * Adds a property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $value
   * @param string $separator
   *
   * @return boolean
   */
  public function addProp(MCL_TEMPLATE $mcl_tmpl = NULL, $cv_name, $cvterm_name, $value, $separator = '') {
    if ($value != '') {
      $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
      if ($cvterm) {
        $type_id = $cvterm->getCvtermID();
        return $this->addProperty($mcl_tmpl, 'eimageprop', 'eimageprop_id', 'eimageprop', 'eimage_id', $this->eimage_id, $type_id, $value, $separator);
      }
    }
    return TRUE;
  }

  /**
   * Adds a property by ID.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $type_id
   * @param string $value
   *
   * @return boolean
   */
  public function addPropByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type_id, $value) {
    return $this->addProperty($mcl_tmpl, 'eimageprop', 'eimageprop_id', 'eimageprop', 'eimage_id', $this->eimage_id, $type_id, $value, $separator);
  }

  /**
   * Returns the eimage.
   *
   * @param integer $eimage_id
   *
   * @return various
   */
  public function getImageByID($eimage_id) {
    return $this->byTKey('eimage', array('eimage_id' => $eimage_id));
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {

    if ($type == 'eimage') {
      return self::_getSchemaEimage();
    }
    else if ($type == 'eimageprop') {
      return self::_getSchemaEimageprop();
    }
    else if ($type == 'eimage_link') {
      return self::_getSchemaImageLink();
    }
    return array();
  }

  /**
   * Returns the schema for eimage.
   *
   * @return array
   */
  private function _getSchemaEimage() {
    return array(
      'description' => 'The custom table for eimage.',
      'fields' => array(
        'eimage_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'eimage_data' => array(
          'type' => 'text',
        ),
        'eimage_type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'image_uri' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('eimage_id'),
      'unique keys' => array(
        'ukey_001' => array('image_uri')
      ),
    );
  }

  /**
   * Returns the schema for image property.
   *
   * @return array
   */
  private function _getSchemaEimageprop() {
    return array(
      'description' => 'The custom table for image property.',
      'fields' => array(
        'eimageprop_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'eimage_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('eimageprop_id'),
      'unique keys' => array(
        'ukey_001' => array('eimage_id', 'type_id', 'rank')
      ),
      'foreign keys' => array(
        'fkey_bims_eimageprop_001' => array(
          'class'     => 'BIMS_CHADO_IMAGE',
          'key'       => 'eimageprop',
          'field_id'  => 'eimage_id',
          'ref_class' => 'BIMS_CHADO_IMAGE',
          'ref_key'   => 'eimage',
          'ref_id'    => 'eimage_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

  /**
   * Returns the schema for image linker table.
   *
   * @return array
   */
  private function _getSchemaImageLink() {
    return array(
      'description' => 'The custom table for linking the eimage with other tables.',
      'fields' => array(
        'eimage_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'target_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'target_type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('eimage_id', 'target_id', 'target_type'),
      ),
      'foreign keys' => array(
        'fkey_bims_eimage_link_001' => array(
          'class'     => 'BIMS_CHADO_IMAGE',
          'key'       => 'eimage_link',
          'field_id'  => 'eimage_id',
          'ref_class' => 'BIMS_CHADO_IMAGE',
          'ref_key'   => 'eimage',
          'ref_id'    => 'eimage_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the eimage ID.
   *
   * @retrun integer
   */
  public function getEimageID() {
    return $this->eimage_id;
  }

  /**
   * Sets the eimage ID.
   *
   * @param integer $eimage_id
   */
  public function setEimageID($eimage_id) {
    $this->eimage_id = $eimage_id;
  }

  /**
   * Retrieves the eimageprop ID.
   *
   * @retrun integer
   */
  public function getEimagepropID() {
    return $this->eimageprop_id;
  }

  /**
   * Sets the eimageprop ID.
   *
   * @param integer $eimageprop_id
   */
  public function setEimagepropID($eimageprop_id) {
    $this->eimageprop_id = $eimageprop_id;
  }
}
