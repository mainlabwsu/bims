<?php
/**
 * The declaration of BIMS_CHADO_GENOTYPE class.
 *
 */
class BIMS_CHADO_GENOTYPE extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $genotype = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->genotype     = $this->getTableName('genotype');
    $this->genotypeprop = $this->getTableName('genotypeprop');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'genotype') {
      $sql = "select nextval('chado.genotype_genotype_id_seq'::regclass)";
    }
    else if ($table_name == 'genotypeprop') {
      $sql = "select nextval('chado.genotypeprop_genotypeprop_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getAssocData()
   */
  public function getAssocData($genotype_id) {



    return array();
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('genotypeprop', 'genotype');
    }
    return array('genotype', 'genotypeprop');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Adds a genotype.synonym
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addGenotype(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array('uniquename' => $details['uniquename']);
    $genotype = $this->byTKey('genotype', $keys);
    if ($genotype) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_genotype', $keys);
      return $genotype->genotype_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets a new genotype ID.
        $genotype_id = $this->getNextval('genotype');

        // Adds the synonym.
        $fields = array(
          'genotype_id' => $genotype_id,
          'uniquename'  => $details['uniquename'],
          'name'        => $details['name'],
          'type_id'     => $details['type_id'],
          'description' => $details['description'],
        );

        // Inserts the record.
        db_insert($this->genotype)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'bc_genotype', $fields);
        return $genotype_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return NULL;
      }
    }
    return NULL;
  }

  /**
   * Deletes the genotype.
   *
   * @param integer $genotype_id
   *
   * @return array
   */
  public function deleteGenotype($genotype_id) {
    db_delete($this->getTableName('genotype'))
      ->condition('genotype_id', $genotype_id, '=')
      ->execute();
    return TRUE;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'genotype') {
      return self::_getSchemaGenotype();
    }
    if ($type == 'genotypeprop') {
      return self::_getSchemaGenotypeprop();
    }
    return array();
  }

  /**
   * Returns the schema for genotype.
   *
   * @return array
   */
  private function _getSchemaGenotype() {
    $schema = array(
      'description' => 'The custom table for genotype.',
      'fields' => array(
        'genotype_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'uniquename' => array(
          'type' => 'text',
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'text',
        ),
        'description' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('genotype_id'),
      'unique keys' => array(
        'ukey_001' => array('uniquename')
      ),
      'indexes' => array(
        'uniquename' => array('uniquename'),
      ),
      'fkey_bims_genotype_001' => array(
        'class'     => 'BIMS_CHADO_GENOTYPE',
        'key'       => 'genotype',
        'field_id'  => 'type_id',
        'field_id'  => 'cvterm_id',
        'ref_table' => 'chado.cvterm',
        'ref_id'    => 'cvterm_id',
        'on'        => 'DELETE CASCADE',
      ),
    );

    // Removes the foreign to chado tables.
    unset($schema['foreign keys']['fkey_bims_genotype_001']);
    return $schema;
  }


    /**
   * Returns the schema for genotypeprop.
   *
   * @return array
   */
  private function _getSchemaGenotypeprop() {
    return array(
      'description' => 'The custom table for genotype property.',
      'fields' => array(
        'genotypeprop_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'genotype_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('genotypeprop_id'),
      'unique keys' => array(
        'ukey_001' => array('genotype_id', 'type_id', 'rank')
      ),
      'foreign keys' => array(
        'fkey_bims_genotypeprop_001' => array(
          'class'     => 'BIMS_CHADO_GENOTYPE',
          'key'       => 'genotypeprop',
          'field_id'  => 'genotype_id',
          'ref_class' => 'BIMS_CHADO_GENOTYPE',
          'ref_key'   => 'genotype',
          'ref_id'    => 'genotype_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

}