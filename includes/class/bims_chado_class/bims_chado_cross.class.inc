<?php
/**
 * The declaration of BIMS_CHADO_CROSS class.
 *
 */
class BIMS_CHADO_CROSS extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $nd_experiment_id     = NULL;
  protected $nd_experimentprop_id = NULL;
  protected $nd_experiment        = NULL;
  protected $nd_experimentprop    = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->nd_experiment      = $this->getTableName('cross');
    $this->nd_experimentprop  = $this->getTableName('crossprop');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'nd_experiment') {
      $sql = "select nextval('chado.nd_experiment_nd_experiment_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experimentprop') {
      $sql = "select nextval('chado.nd_experimentprop_nd_experimentprop_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_project') {
      $sql = "select nextval('chado.nd_experiment_project_nd_experiment_project_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_stock') {
      $sql = "select nextval('chado.nd_experiment_stock_nd_experiment_stock_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getAssocData()
   */
  public function getAssocData($nd_experiment_id) {

    // Initializes the data counts.
    $data_counts = array();

    // Counts the data in BIMS_CHADO.
    $data_count = bims_count_data_bc('cross', $nd_experiment_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['bims_chado'] = $data_count;
    }

    // Counts the data in mviews.
    $data_count = bims_count_data_mview('cross', $nd_experiment_id, $this->program_id);
    if (!empty($data_count)) {
      $data_counts['mview'] = $data_count;
    }

    // Counts the data in public schema.
    $data_count = bims_count_data_public('cross', $nd_experiment_id);
    if (!empty($data_count)) {
      $data_counts['public'] = $data_count;
    }
    return $data_counts;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('crossprop', 'cross');
    }
    return array('cross', 'crossprop');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Returns the cross by cross number.
   *
   * @param string $cross_number
   * @param integer $project_id_cross
   *
   * @return object
   */
  public function byCrossNumber($cross_number, $project_id_cross = NULL) {

    // Gets project ID of the cross if not provided.
    if (!$project_id_cross) {
      $bims_program = BIMS_PROGRAM::byID($this->getProgramID());
      $project_id_cross = $bims_program->getCrossProjectID();
    }

    // Returns the cross.
    $keys = array(
      'project_id_cross' => $project_id_cross,
      'cross_number'     => $cross_number,
    );
    return $this->byTKey('cross', $keys);
  }

  /**
   * Returns the cross.
   *
   * @param integer $nd_experiment_id
   *
   * @return object
   */
  public function getCross($nd_experiment_id) {
    return $this->byTKey('cross', array('nd_experiment_id' => $nd_experiment_id));
  }

  /**
   * Deletes the data in mview.
   *
   * @param array $nd_experiment_ids
   *
   * @return boolean
   */
  public function deleteDataMView($nd_experiment_ids) {

      // Updates the data in BIMS_MVIEW_PHENOTYPE.
      $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $this->program_id));
      $m_phenotype  = $bm_phenotype->getMView();
      $id_list = implode(',', $nd_experiment_ids);
      $sql = "
        UPDATE $m_phenotype SET nd_experiment_id = NULL, cross_number = NULL
        WHERE nd_experiment_id IN ($id_list)
      ";
      db_query($sql);

      // Updates the data in BIMS_MVIEW_STOCK.
      $bm_stock = new BIMS_MVIEW_STOCK(array('node_id' => $this->program_id));
      $m_stock  = $bm_stock->getMView();
      $id_list = implode(',', $nd_experiment_ids);
      $sql = "
        UPDATE $m_stock SET nd_experiment_id = NULL, cross_number = NULL
        WHERE nd_experiment_id IN ($id_list)
      ";
      db_query($sql);

      // Prepare for deletion.
      $fkeys_delete = array(
        'BIMS_MVIEW_CROSS'          => array('nd_experiment_id'),
        'BIMS_MVIEW_CROSS_PROGENY'  => array('nd_experiment_id'),
      );

      // Deletes the data.
      return bims_delete_data_mview($this->program_id, $nd_experiment_ids, $fkeys_delete);
    }

  /**
   * Deletes the data in public.
   *
   * @param array $nd_experiment_ids
   *
   * @return boolean
   */
  public function deleteDataPublic($nd_experiment_ids) {

    foreach ($nd_experiment_ids as $nd_experiment_id) {

      // Gets conditions.
      $conds = array(
        'bims_mview_cross_stats' => array(
          'node_id'           => $this->program_id,
          'nd_experiment_id'  => $nd_experiment_id,
        ),
      );

      // Deletes the data.
      if (!bims_delete_data_public($conds)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Deletes the crosses.
   *
   * @param array $nd_experiment_id
   *
   * @return boolean
   */
  public function deleteCrosses($nd_experiment_ids) {

    $transaction = db_transaction();
    try {

      // Gets the callbacks.
      $callbacks = $this->deleteCallbacks($nd_experiment_ids);

      // Deletes the data in BIMS_MVIEW.
      if (!$this->deleteDataMView($nd_experiment_ids)) {
        throw new Exception("Fail to delete the data in BIMS schema");
      }

      // Deletes the data in public.
      if (!$this->deleteDataPublic($nd_experiment_ids)) {
        throw new Exception("Fail to delete the data in public schema");
      }

      // Deletes the cross.
      $table = $this->getTableName('cross');
      if (db_table_exists($table)) {
        $id_list = implode(',', $nd_experiment_ids);
        $sql = "DELETE FROM {$table} WHERE nd_experiment_id IN ($id_list)";
        db_query($sql);
      }

      // Submit the callbacks.
      if (!empty($callbacks)) {
        if (!bims_submit_drush_cmds($this->program_id, 'delete-cross', $callbacks)) {
          throw new Exception("Error : Failed to submit callbacks");
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets the callbacks called after deletion.
   *
   * @param array $nd_experiment_ids
   *
   * @return array
   */
  public function deleteCallbacks($nd_experiment_ids) {
    return array();
  }

  /**
   * Updates the cross.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateCross($details) {

    // Gets the variables.
    $nd_experiment_id = $details['nd_experiment_id'];
    $cross_number     = $details['cross_number'];
    $dup_key          = $details['dup_key'];
    $maternal         = $details['maternal'];
    $paternal         = $details['paternal'];

    // Updates the cross.
    $transaction = db_transaction();
    try {

      // --------------------------------- //
      // Updates the properties of the cross.
      $fields_cross = array(
        'cross_number'  => $cross_number,
        'dup_key'       => $dup_key,
      );

      // Updates the accession.
      db_update($this->getTableName('cross'))
        ->fields($fields_cross)
        ->condition('nd_experiment_id', $nd_experiment_id, '=')
        ->execute();

      // --------------------------------- //
      // Updates the parents of the accession.
      $this->setNdExperimentID($nd_experiment_id);
      if (!$this->updateParents(NULL, $maternal, $paternal)) {
        throw new Exception("Error : Failed to update parents");
      }

      // --------------------------------- //
      // Updates the cross properties.


    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new cross.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addCross(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array('dup_key' => $details['dup_key']);
    $cross = $this->byTKey('cross', $keys);
    if ($cross) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_cross', $keys);
      return $cross->nd_experiment_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets the cvterms.
        $cvterm_id_cross_experiment = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'cross_experiment')->getCvtermID();
        $cvterm_id_cross_number     = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'cross_number')->getCvtermID();

        // Gets the next value.
        $nd_experiment_id         = $this->getNextval('nd_experiment');
        $nep_id_dup_key           = $this->getNextval('nd_experimentprop');
        $nep_id_cross_number      = $this->getNextval('nd_experimentprop');
        $nd_experiment_project_id = $this->getNextval('nd_experiment_project');

        // Updates nd_geolocation_id.
        $nd_geolocation_id = array_key_exists('nd_geolocation_id', $details) ? $details['nd_geolocation_id'] : '';
        if (!$nd_geolocation_id) {
          $nd_geolocation_id = MCL_DUMMY_VAR::getIdByName('ND_GEOLOCATION_ID');
        }

        // Adds the data.
        $fields = array(
          'nd_experiment_id'          => $nd_experiment_id,
          'nep_id_dup_key'            => $nep_id_dup_key,
          'nep_id_cross_number'       => $nep_id_cross_number,
          'nd_experiment_project_id'  => $nd_experiment_project_id,
          'ne_type_id'                => $cvterm_id_cross_experiment,
          'nep_type_id_cross_number'  => $cvterm_id_cross_experiment,
          'nep_type_id_dup_key'       => $cvterm_id_cross_number,
          'dup_key'                   => $details['dup_key'],
          'cross_number'              => $details['cross_number'],
          'project_id_cross'          => $details['project_id_cross'],
          'nd_geolocation_id'         => $nd_geolocation_id,
        );

        // Inserts the record.
        db_insert($this->nd_experiment)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'bc_cross', $fields);
        return $nd_experiment_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('MCL', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return NULL;
  }

  /**
   * Retrieves the property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $separator
   *
   * @return string
   */
  public function getProp($cv_name, $cvterm_name, $separator = '') {
    return $this->getProperty($this->nd_experimentprop, 'nd_experiment_id', $this->nd_experiment_id, $cv_name, $cvterm_name, $separator);
  }

  /**
   * Adds a property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $value
   * @param string $separator
   *
   * @return boolean
   */
  public function addProp(MCL_TEMPLATE $mcl_tmpl = NULL, $cv_name, $cvterm_name, $value, $separator = '') {
    if ($value != '') {
      $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
      if ($cvterm) {
        $type_id = $cvterm->getCvtermID();
        return $this->addProperty($mcl_tmpl, 'nd_experimentprop', 'nd_experimentprop_id', 'crossprop', 'nd_experiment_id', $this->nd_experiment_id, $type_id, $value, $separator);
      }
    }
    return TRUE;
  }

  /**
   * Adds a property by ID.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $type_id
   * @param string $value
   *
   * @return boolean
   */
  public function addPropByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type_id, $value) {
    return $this->addProperty($mcl_tmpl, 'nd_experimentprop', 'nd_experimentprop_id', 'crossprop', 'nd_experiment_id', $this->nd_experiment_id, $type_id, $value, $separator);
  }

  /**
   * Updates a parent.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $maternal
   * @param string $paternal
   *
   * @return boolean
   */
  public function updateParents(MCL_TEMPLATE $mcl_tmpl = NULL, $maternal = NULL, $paternal = NULL) {

    // Gets BIMS_CHADO_ACCESSION.
    $bc_accession = new BIMS_CHADO_ACCESSION($this->getProgramID());

    // Gets the cvterms.
    $cvterms = array(
      'mp' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'maternal_parent')->getCvtermID(),
      'pp' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'paternal_parent')->getCvtermID(),
    );

    // Updates the current maternal.
    if (!$maternal) {
      $this->deleteParent('maternal');
    }
    else {
      $cur_maternal = $this->getParent('maternal');
      $new_maternal = $bc_accession->getAccessionByName($maternal);
      if (!$cur_maternal) {
        $this->addParent(NULL, 'm', $new_maternal, $cvterms['mp']);
      }
      else if ($cur_maternal != $maternal) {
        db_update($this->getTableName('cross'))
          ->fields(array('stock_id_m' => $new_maternal->stock_id))
          ->condition('nd_experiment_id', $this->nd_experiment_id, '=')
          ->execute();
      }
    }

    // Updates the current paternal.
    if (!$paternal) {
      $this->deleteParent('paternal');
    }
    else {
      $cur_paternal = $this->getParent('paternal');
      $new_paternal = $bc_accession->getAccessionByName($paternal);
      if (!$cur_paternal) {
        $this->addParent(NULL, 'p', $new_paternal, $cvterms['pp']);
      }
      else if ($cur_paternal != $paternal) {
        db_update($this->getTableName('cross'))
          ->fields(array('stock_id_p' => $new_paternal->stock_id))
          ->condition('nd_experiment_id', $this->nd_experiment_id, '=')
          ->execute();
      }
    }
    return TRUE;
  }

  /**
   * Returns the parent of the provided type.
   *
   * @param string $type
   *
   * @return string
   */
  public function getParent($type) {

    // Gets BIMS_CHADO_ACCESSION.
    $bc_accession = new BIMS_CHADO_ACCESSION($this->getProgramID());

    // Gets the parent and returns the name.
    $cross = $this->getCross($this->nd_experiment_id);
    $stock_id = ($type == 'maternal') ? $cross->stock_id_m : $cross->stock_id_p;
    if ($stock_id) {
      $parent = $bc_accession->getAccession($stock_id);
      if ($parent) {
        return $parent->name;
      }
    }
    return '';
  }

  /**
   * Deletes the parent of the provided type.
   *
   * @param string $type
   *
   * @return boolean
   */
  public function deleteParent($type) {
    $fields = array();
    if ($type == 'maternal') {
      $fields = array(
        'stock_id_m'  => NULL,
        'nes_id_m'    => NULL,
      );
    }
    else if ($type == 'paternal') {
      $fields = array(
        'stock_id_p'  => NULL,
        'nes_id_p'    => NULL,
      );
    }
    if (!empty($fields)) {
      db_update($this->getTableName('cross'))
        ->fields($fields)
        ->condition('nd_experiment_id', $this->nd_experiment_id, '=')
        ->execute();
    }
    return TRUE;
  }

  /**
   * Adds the accession to the cross.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $type
   * @param object $accession
   * @param integer $type_id
   *
   * @return boolean
   */
  public function addParent(MCL_TEMPLATE $mcl_tmpl = NULL, $type, $accession, $type_id) {
    if ($accession && $type_id) {

      // Checks for duplication.
      $has_parent = $this->obj->{'nes_id_' . $type};
      if ($has_parent == '') {

        // Updates the parant info.
        $fields = array(
          'stock_id_' . $type     => $accession->stock_id,
          'nes_id_' . $type       => $this->getNextval('nd_experiment_stock'),
          'nes_type_id_' . $type  => $type_id,
        );
        db_update($this->nd_experiment)
          ->fields($fields)
          ->condition('nd_experiment_id', $this->nd_experiment_id, '=')
          ->execute();
      }
    }
    return TRUE;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'cross') {
      return self::_getSchemaCross();
    }
    else if ($type == 'crossprop') {
      return self::_getSchemaCrossprop();
    }
    return array();
  }

  /**
   * Returns the schema for cross.
   *
   * @return array
   */
  private function _getSchemaCross() {
    return array(
      'description' => 'The custom table for cross.',
      'fields' => array(
        'nd_experiment_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'ne_type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'dup_key' => array(
          'type' => 'text',
          'not null' => TRUE,
        ),
        'nep_id_dup_key' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nep_type_id_dup_key' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'cross_number' => array(
          'type' => 'text',
          'not null' => TRUE,
        ),
        'nep_id_cross_number' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nep_type_id_cross_number' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'project_id_cross' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nes_id_m' => array(
          'type' => 'int',
        ),
        'nes_type_id_m' => array(
          'type' => 'int',
        ),
        'stock_id_m' => array(
          'type' => 'int',
        ),
        'nes_id_p' => array(
          'type' => 'int',
        ),
        'nes_type_id_p' => array(
          'type' => 'int',
        ),
        'stock_id_p' => array(
          'type' => 'int',
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_experiment_id'),
      'unique keys' => array(
        'ukey_001' => array('cross_number')
      ),
    );
  }

  /**
   * Returns the schema for cross property.
   *
   * @return array
   */
  private function _getSchemaCrossprop() {
    return array(
      'description' => 'The custom table for cross property.',
      'fields' => array(
        'nd_experimentprop_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'nd_experiment_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_experimentprop_id'),
      'unique keys' => array(
        'ukey_001' => array('nd_experiment_id', 'type_id', 'rank')
      ),
      'foreign keys' => array(
        'fkey_bims_crossprop_001' => array(
          'class'     => 'BIMS_CHADO_CROSS',
          'key'       => 'crossprop',
          'field_id'  => 'nd_experiment_id',
          'ref_class' => 'BIMS_CHADO_CROSS',
          'ref_key'   => 'cross',
          'ref_id'    => 'nd_experiment_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the nd_experiment ID.
   *
   * @retrun integer
   */
  public function getNdExperimentID() {
    return $this->nd_experiment_id;
  }

  /**
   * Sets the nd_experiment ID.
   *
   * @param integer $nd_experiment_id
   */
  public function setNdExperimentID($nd_experiment_id) {
    $this->nd_experiment_id = $nd_experiment_id;
  }

  /**
   * Retrieves the nd_experimentprop ID.
   *
   * @retrun integer
   */
  public function getNdExperimentpropID() {
    return $this->nd_experimentprop_id;
  }

  /**
   * Sets the nd_experimentprop ID.
   *
   * @param integer $nd_experimentprop_id
   */
  public function setNdExperimentpropID($nd_experimentprop_id) {
    $this->nd_experimentprop_id = $nd_experimentprop_id;
  }
}
