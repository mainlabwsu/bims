<?php
/**
 * The declaration of BIMS_CHADO_PROJECT class.
 *
 */
class BIMS_CHADO_PROJECT extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $project_id         = NULL;
  protected $projectprop_id     = NULL;
  protected $project_contact_id = NULL;
  protected $project            = NULL;
  protected $projectprop        = NULL;
  protected $project_contact    = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->project          = $this->getTableName('project');
    $this->projectprop      = $this->getTableName('projectprop');
    $this->project_contact  = $this->getTableName('project_contact');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'project') {
      $sql = "select nextval('chado.project_project_id_seq'::regclass)";
    }
    else if ($table_name == 'projectprop') {
      $sql = "select nextval('chado.projectprop_projectprop_id_seq'::regclass)";
    }
    else if ($table_name == 'project_contact') {
      $sql = "select nextval('chado.project_contact_project_contact_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getAssocData()
   */
  public function getAssocData($project_id) {



    return array();
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('project_contact', 'projectprop', 'project');
    }
    return array('project', 'projectprop', 'project_contact');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }
  /**
   * Checks the existence of dataset. If not, write the error messasge
   * to the log.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $name
   *
   * @return boolean
   */
  public function checkProject(MCL_TEMPLATE $mcl_tmpl = NULL, $name) {
    if ($name) {

      // Gets the dataset.
      $project = $this->byTKey('project', array('name' => $name));
      if (!$project) {
        $this->updateMsg($mcl_tmpl, 'E', "(name) = ($name) not found in project");
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Adds a new project.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addProject(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array('name' => $details['name']);
    $project = $this->byTKey('project', $keys);
    if ($project) {
      if ($mcl_tmpl) {
        $this->addMsg($mcl_tmpl, 'D', 'bc_project', $keys);
      }
      return $project->project_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets the next value.
        $project_id = $this->getNextval('project');

        // Adds the data.
        $fields = array(
          'project_id'  => $project_id,
          'name'        => $details['name'],
          'type'        => $details['type'],
          'description' => $details['description'],
        );

        // Inserts the record.
        db_insert($this->getTableName('project'))
          ->fields($fields)
          ->execute();
        if ($mcl_tmpl) {
          $this->addMsg($mcl_tmpl, 'N', 'bc_project', $fields);
        }
        return $project_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return NULL;
  }

  /**
   * Adds contacts.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $contact_name
   * @param string $separator
   *
   * @return boolean
   */
  public function addContact(MCL_TEMPLATE $mcl_tmpl = NULL, $contact_name, $separator = '') {

    // Adds contacts.
    if ($contact_name) {
      $value_arr = preg_split($this->getSepRegex($separator), $contact_name, -1, PREG_SPLIT_NO_EMPTY);

      // Checks duplication before adding new contacts.
      $err_flag = FALSE;
      foreach ($value_arr as $val) {
        $val = trim($val);

        $contact = MCL_CHADO_CONTACT::byName($val);
        if (!$contact) {
          $this->updateMsg($mcl_tmpl, 'E', "Error : $val does not exist in chado.contact table");
          continue;
        }
        $args = array(
          'project_id' => $this->project_id,
          'contact_id' => $contact->getContactID(),
        );
        $obj = $this->byTKey('project_contact', $args);
        if ($obj) {
          $this->addMsg($mcl_tmpl, 'D', $table_name, $args);
        }
        else {

          // Gets the next value.
          $project_contact_id = $this->getNextval('project_contact');

          // Adds the data.
          $table_name = $this->getTableName('project_contact');
          $transaction = db_transaction();
          try {
            $args['project_contact_id'] = $project_contact_id;
            db_insert($table_name)
              ->fields($args)
              ->execute();
            $this->addMsg($mcl_tmpl, 'N', $table_name, $args);
            return $project_contact_id;
          }
          catch (Exception $e) {
            $transaction->rollback();
            watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
          }
        }
      }
    }
  }

  /**
   * Retrieves the property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $separator
   *
   * @return string
   */
  public function getProp($cv_name, $cvterm_name, $separator = '') {
    return $this->getProperty($this->projectprop, 'project_id', $this->project_id, $cv_name, $cvterm_name, $separator);
  }

  /**
   * Adds a property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $value
   * @param string $separator
   *
   * @return boolean
   */
  public function addProp(MCL_TEMPLATE $mcl_tmpl = NULL, $cv_name, $cvterm_name, $value, $separator = '') {
    if ($value != '') {
      $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
      if ($cvterm) {
        $type_id = $cvterm->getCvtermID();
        return $this->addProperty($mcl_tmpl, 'projectprop', 'projectprop_id', 'projectprop', 'project_id', $this->project_id, $type_id, $value, $separator);
      }
    }
    return TRUE;
  }

  /**
   * Adds a property by ID.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $type_id
   * @param string $value
   *
   * @return boolean
   */
  public function addPropByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type_id, $value) {
    return $this->addProperty($mcl_tmpl, 'projectprop', 'projectprop_id', 'projectprop', 'project_id', $this->project_id, $type_id, $value, $separator);
  }

  /**
   * Returns the projects.
   *
   * @param string $type
   * @param integer $flag
   *
   * @return various
   */
  public function getProjects($type_arr = array(), $flag = MCL_OPTION) {

    // Gets the cvterm.
    $cvterm_id_sub_type = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'sub_type')->getCvtermID();

    // Sets the SQL statement.
    $table_project      = $this->getTableName('project');
    $table_projectprop  = $this->getTableName('projectprop');
    $sql = "
      SELECT P.*, PP.value AS sub_type
      FROM $table_project P
        LEFT JOIN $table_projectprop PP on PP.project_id = P.project_id
      WHERE 1=1 AND PP.type_id = :type_id
    ";

    // Gets the projects.
    if (!empty($type_arr)) {
      $sql .= " AND LOWER(P.type) IN ('" . implode("','", $type_arr) . "')";
    }
    $results = db_query($sql, array(':type_id' => $cvterm_id_sub_type));
    $projects = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == MCL_OPTION) {
        $projects[$obj->project_id] = $obj->name;
      }
      else if ($flag == MCL_OBJECT) {
        $projects []= $obj;
      }
    }
    return $projects;
  }

  /**
   * Returns the project by project ID.
   *
   * @param integer $project_id
   * @param integer $flag
   *
   * @return various
   */
  public function getProjectByID($project_id) {
    return $this->byTKey('project', array('project_id' => $project_id));
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'project') {
      return self::_getSchemaProject();
    }
    else if ($type == 'projectprop') {
      return self::_getSchemaProjectprop();
    }
    else if ($type == 'project_contact') {
      return self::_getSchemaProjectContact();
    }
    return array();
  }

  /**
   * Returns the schema for project.
   *
   * @return array
   */
  private function _getSchemaProject() {
    return array(
      'description' => 'The custom table for project.',
      'fields' => array(
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'type' => array(
          'type' => 'varchar',
          'length' => '255',
        ),
        'description' => array(
          'type' => 'text',
        ),
      ),
      'primary key' => array('project_id'),
      'unique keys' => array(
        'ukey_001' => array('name')
      ),
    );
  }

  /**
   * Returns the schema for project property.
   *
   * @return array
   */
  private function _getSchemaProjectprop() {
    return array(
      'description' => 'The custom table for project property.',
      'fields' => array(
        'projectprop_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('projectprop_id'),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'type_id', 'rank')
      ),
      'foreign keys' => array(
        'fkey_bims_projectprop_001' => array(
          'class'     => 'BIMS_CHADO_PROJECT',
          'key'       => 'projectprop',
          'field_id'  => 'project_id',
          'ref_class' => 'BIMS_CHADO_PROJECT',
          'ref_key'   => 'project',
          'ref_id'    => 'project_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

  /**
   * Returns the schema for project contact.
   *
   * @return array
   */
  private function _getSchemaProjectContact() {
    $schema = array(
      'description' => 'The custom table for project contact.',
      'fields' => array(
        'project_contact_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'contact_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'contact_id')
      ),
      'foreign keys' => array(
        'fkey_bims_project_contact_001' => array(
          'class'     => 'BIMS_CHADO_PROJECT',
          'key'       => 'project_contact',
          'field_id'  => 'project_id',
          'ref_class' => 'BIMS_CHADO_PROJECT',
          'ref_key'   => 'project',
          'ref_id'    => 'project_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_project_contact_002' => array(
          'class'     => 'BIMS_CHADO_PROJECT',
          'key'       => 'project_contact',
          'field_id'  => 'contact_id',
          'ref_table' => 'chado.contact',
          'ref_id'    => 'contact_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );

    // Removes the foreign to chado tables.
    unset($schema['foreign keys']['fkey_bims_project_contact_002']);
    return $schema;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the project ID.
   *
   * @retrun integer
   */
  public function getProjectID() {
    return $this->project_id;
  }

  /**
   * Sets the project ID.
   *
   * @param integer $project_id
   */
  public function setProjectID($project_id) {
    $this->project_id = $project_id;
  }

  /**
   * Retrieves the projectprop ID.
   *
   * @retrun integer
   */
  public function getProjectpropID() {
    return $this->projectprop_id;
  }

  /**
   * Sets the projectprop ID.
   *
   * @param integer $projectprop_id
   */
  public function setProjectpropID($projectprop_id) {
    $this->projectprop_id = $projectprop_id;
  }

  /**
   * Retrieves the project_contact ID.
   *
   * @retrun integer
   */
  public function getProjectContactID() {
    return $this->project_contact_id;
  }

  /**
   * Sets the project_contact ID.
   *
   * @param integer $project_contact_id
   */
  public function setProjectContactID($project_contact_id) {
    $this->project_contact_id = $project_contact_id;
  }
}
