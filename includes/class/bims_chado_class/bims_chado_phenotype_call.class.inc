<?php
/**
 * The declaration of BIMS_CHADO_PHENOTYPE_CALL class.
 *
 */
class BIMS_CHADO_PHENOTYPE_CALL extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $phenotype_call_id          = NULL;
  protected $phenotype_call_contact_id  = NULL;
  protected $phenotype_call             = NULL;
  protected $phenotype_call_contact     = NULL;


  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->phenotype_call         = $this->getTableName('phenotype_call');
    $this->phenotype_call_contact = $this->getTableName('phenotype_call_contact');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'phenotype_call') {
      $sql = "select nextval('chado.phenotype_call_phenotype_call_id_seq'::regclass)";
    }
    else if ($table_name == 'phenotype_call_contact') {
      $sql = "select nextval('chado.phenotype_call_contact_phenotype_call_contact_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * Deletes the trait.
   *
   * @param integer $cvterm_id
   *
   * @return boolean
   */
  public function deleteTrait($cvterm_id) {
    $table = $this->getTableName('phenotype_call');
    db_delete($table)
      ->condition('cvterm_id', $cvterm_id, '=')
      ->execute();
    return TRUE;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('phenotype_call_contact', 'phenotype_call');
    }
    return array('phenotype_call', 'phenotype_call_contact');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Adds a new data point.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addData(MCL_TEMPLATE $mcl_tmpl, $details) {

    // Checks for duplication.
    $keys = array(
      'project_id'        => $details['project_id'],
      'nd_geolocation_id' => $details['nd_geolocation_id'],
      'stock_id'          => $details['stock_id'],
      'cvterm_id'         => $details['cvterm_id'],
    );
    $data = $this->byTKey('phenotype_call', $keys);
    if ($data) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_phenotype_call', $keys);
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets the next value.
        $phenotype_call_id = $this->getNextval('phenotype_call');

        // Adds the data.
        $fields = array(
          'phenotype_call_id' => $phenotype_call_id,
          'project_id'        => $details['project_id'],
          'nd_geolocation_id' => $details['nd_geolocation_id'],
          'stock_id'          => $details['stock_id'],
          'cvterm_id'         => $details['cvterm_id'],
          'value'             => $details['value'],
          'time'              => $details['time'],
        );

        // Inserts the record.
        db_insert($this->phenotype_call)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'bc_phenotype_call', $fields);
        $GLOBALS['META_DATA']['phenotype_bims'][$details['project_id']] = TRUE;

        // Adds contacts.
        if (array_key_exists('contact', $details) && $details['contact']) {
          $this->setPhenotypeCallID($phenotype_call_id);
          $this->addContact($mcl_tmpl, $details['contact']);
        }
        return $phenotype_call_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('MCL', $e->getMessage(), array(), WATCHDOG_ERROR);
        return NULL;
      }
    }
    return $data->phenotype_call_id;
  }

    /**
   * Adds the contacts to phenotype_call_contact table.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $contact_id
   *
   * @return boolean
   */
  public function addContactByID(MCL_TEMPLATE $mcl_tmpl, $contact_id) {

    // Checks for duplication.
    $fields = array(
      'phenotype_call_id' => $this->phenotype_call_id,
      'contact_id'        => $contact_id,
    );
    $phenotype_call_contact = $this->byTKey('phenotype_call_contact', $fields);
    if ($phenotype_call_contact) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_phenotype_call_contact', $fields);
    }
    else {

      // Gets the next value.
      $fields['phenotype_call_contact_id'] = $this->getNextval('phenotype_call_contact');

      // Inserts the record.
      db_insert($this->phenotype_call_contact)
        ->fields($fields)
        ->execute();
      $this->addMsg($mcl_tmpl, 'N', 'bc_phenotype_call_contact', $fields);
    }
    return TRUE;
  }

  /**
   * Adds the contacts to phenotype_call_contact table.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $contact
   * @param string $separator
   *
   * @return boolean
   */
  public function addContact(MCL_TEMPLATE $mcl_tmpl, $contact, $separator = '[,;]') {

    // Parses out the contacts and add them to phenotype_call_contact table.
    if ($contact) {
      $contacts = preg_split(BIMS_CHADO_TABLE::getSepRegex($separator), $contact, -1, PREG_SPLIT_NO_EMPTY);
      $fields       = NULL;
      $bims_contact = NULL;
      foreach ($contacts as $contact_name) {
        $bims_contact = MCL_CHADO_CONTACT::byName($contact_name);

        // Checks for duplication.
        $fields = array(
          'phenotype_call_id' => $this->phenotype_call_id,
          'contact_id'        => $bims_contact->getContactID(),
        );
        $phenotype_call_contact = $this->byTKey('phenotype_call_contact', $fields);
        if ($phenotype_call_contact) {
          $this->addMsg($mcl_tmpl, 'D', 'bc_phenotype_call_contact', $fields);
        }
        else {

          // Gets the next value.
          $fields['phenotype_call_contact_id'] = $this->getNextval('phenotype_call_contact');

          // Inserts the record.
          db_insert($this->phenotype_call_contact)
            ->fields($fields)
            ->execute();
          $this->addMsg($mcl_tmpl, 'N', 'bc_phenotype_call_contact', $fields);
        }
      }
    }
    return TRUE;
  }

  /**
   * Returns the phenotype_call.
   *
   * @param array $keys
   *
   * @return object
   */
  public function getPhenotypeCall($keys) {

    // Gets the sample.
    $args = array(
      ':project_id'       => $keys['project_id'],
      ':unique_id'        => $keys['unique_id'],
      ':primary_order'    => $keys['primary_order'],
      ':secondary_order'  => $keys['secondary_order'],
    );
    $table_name = $this->getTableName('phenotype_call');
    $sql = "
      SELECT T.*
      FROM {$table_name} T
      WHERE T.project_id = :project_id AND T.unique_id = :unique_id
        AND T.primary_order = :primary_order
        AND T.secondary_order = :secondary_order
    ";
    return db_query($sql, $args)->fetchObject();
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'phenotype_call') {
      return self::_getSchemaPhenotypeCall();
    }
    else if ($type == 'phenotype_call_contact') {
      return self::_getSchemaPhenotypeCallContact();
    }
    return array();
  }

  /**
   * Returns the schema for phenotype_call.
   *
   * @return array
   */
  private function _getSchemaPhenotypeCall() {
    $schema = array(
      'description' => 'The custom table for phenotype_call.',
      'fields' => array(
        'phenotype_call_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'cvterm_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'phenotype_id' => array(
          'type' => 'int',
        ),
        'value' => array(
          'type' => 'varchar',
          'length' => '500',
        ),
        'contact_id' => array(
          'type' => 'int',
        ),
        'time' => array(
          'type' => 'datetime',
          'mysql_type' => 'datetime',
          'pgsql_type' => 'timestamp without time zone',
          'not null' => TRUE,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('phenotype_call_id'),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'nd_geolocation_id', 'stock_id', 'cvterm_id', 'time')
      ),
      'indexes' => array(
        'pc_project_id' => array('project_id'),
        'pc_nd_geolocation_id' => array('nd_geolocation_id'),
        'pc_stock_id' => array('stock_id'),
        'pc_cvterm_id' => array('cvterm_id'),
      ),
      'foreign keys' => array(
        'fkey_bims_phenotype_call_001' => array(
          'class'     => 'BIMS_CHADO_PHENOTYPE_CALL',
          'key'       => 'phenotype_call',
          'field_id'  => 'project_id',
          'ref_class' => 'BIMS_CHADO_PROJECT',
          'ref_key'   => 'project',
          'ref_id'    => 'project_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_phenotype_call_003' => array(
          'class'     => 'BIMS_CHADO_PHENOTYPE_CALL',
          'key'       => 'phenotype_call',
          'field_id'  => 'stock_id',
          'ref_class' => 'BIMS_CHADO_ACCESSION',
          'ref_key'   => 'accession',
          'ref_id'    => 'stock_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_phenotype_call_004' => array(
          'class'     => 'BIMS_CHADO_PHENOTYPE_CALL',
          'key'       => 'phenotype_call',
          'field_id'  => 'nd_geolocation_id',
          'ref_class' => 'BIMS_CHADO_SITE',
          'ref_key'   => 'site',
          'ref_id'    => 'nd_geolocation_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_phenotype_call_005' => array(
          'class'     => 'BIMS_CHADO_PHENOTYPE_CALL',
          'key'       => 'phenotype_call',
          'field_id'  => 'cvterm_id',
          'ref_table' => 'chado.cvterm',
          'ref_id'    => 'cvterm_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_phenotype_call_006' => array(
          'class'     => 'BIMS_CHADO_PHENOTYPE_CALL',
          'key'       => 'phenotype_call',
          'field_id'  => 'contact_id',
          'ref_table' => 'chado.contact',
          'ref_id'    => 'contact_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );

    // Removes the foreign to chado tables.
    unset($schema['foreign keys']['fkey_bims_phenotype_call_005']);
    unset($schema['foreign keys']['fkey_bims_phenotype_call_006']);
    return $schema;
  }


  /**
   * Returns the schema for phenotype_call_contact.
   *
   * @return array
   */
  private function _getSchemaPhenotypeCallContact() {
    $schema = array(
      'description' => 'The custom table for phenotype_call_contact.',
        'fields' => array(
          'phenotype_call_contact_id' => array(
            'type' => 'int',
            'not null'  => TRUE,
          ),
          'phenotype_call_id' => array(
            'type' => 'int',
            'not null'  => TRUE,
          ),
          'contact_id' => array(
            'type' => 'int',
            'not null'  => TRUE,
          ),
          'flag' => array(
            'type' => 'int',
            'not null' => TRUE,
            'default' => 0,
          ),
        ),
        'primary key' => array('phenotype_call_contact_id'),
        'unique keys' => array(
          'ukey_001' => array('phenotype_call_id', 'contact_id')
        ),
        'foreign keys' => array(
          'fkey_bims_phenotype_call_contact_001' => array(
            'class'     => 'BIMS_CHADO_PHENOTYPE_CALL_CONTACT',
            'key'       => 'phenotype_call_contact',
            'field_id'  => 'phenotype_call_id',
            'ref_class' => 'BIMS_CHADO_PHENOTYPE_CALL',
            'ref_key'   => 'phenotype_call',
            'ref_id'    => 'phenotype_call_id',
            'on'        => 'DELETE CASCADE',
          ),
          'fkey_bims_phenotype_call_contact_002' => array(
            'class'     => 'BIMS_CHADO_PHENOTYPE_CALL',
            'key'       => 'phenotype_call',
            'field_id'  => 'contact_id',
            'ref_table' => 'chado.contact',
            'ref_id'    => 'contact_id',
            'on'        => 'DELETE CASCADE',
          ),
        ),
    );
    // Removes the foreign to chado tables.
    unset($schema['foreign keys']['fkey_bims_phenotype_call_contact_002']);
    return $schema;
  }

  /**
   * Returns data object.
   *
   * @param integer $phenotype_call_id
   *
   * @return object
   */
  public function getDataPoint($phenotype_call_id) {
    if ($phenotype_call_id) {
      $table = $this->getTableName('phenotype_call');
      $sql = "SELECT MV.* FROM $table MV WHERE MV.phenotype_call_id = :phenotype_call_id";
      $results = db_query($sql, array(':phenotype_call_id' => $phenotype_call_id));
      return $results->fetchObject();
    }
    return NULL;
  }

  /**
   * Returns the number of the samples.
   *
   * @param integer $program_id
   * @param string $type
   * @param integer $id
   *
   * @return integer
   */
  public static function getNumData($program_id, $type, $id) {

    // Local varibales.
    $bc_pc        = new BIMS_CHADO_PHENOTYPE_CALL($program_id);
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    $m_phenotype  = $bm_phenotype->getMView();

    // Generates the SQL statement.
    $where_str  = '';
    $join_str   = '';
    $args       = array();
    if ($type == 'program') {}
    else if ($type == 'accession') {
      $args[':stock_id'] = $id;
      $join_str  = " INNER JOIN {$m_phenotype} MV on MV.sample_id = PC.stock_id ";
      $where_str = ' AND MV.stock_id = :stock_id ';
    }
    else if ($type == 'trial') {
      $args[':trial_id'] = $id;
      $where_str = ' MV.node_id = :trial_id ';
    }
    else if ($type == 'cross') {
      $args[':nd_experiment_id'] = $id;
      $join_str  = " INNER JOIN {$m_phenotype} MV on MV.sample_id = PC.stock_id ";
      $where_str = ' AND MV.nd_experiment_id = :nd_experiment_id ';
    }
    else {
      return 0;
    }

    // Returns the number of the data.
    $table_pc = $bc_pc->getTableName('phenotype_call');
    $sql = "
      SELECT COUNT(PC.phenotype_call_id)
      FROM {$table_pc} PC
        $join_str
      WHERE 1=1 $where_str
    ";
    $num = db_query($sql, $args)->fetchField();
    return ($num) ? $num : 0;
  }

  /**
   * Returns the number of the samples.
   *
   * @param integer $program_id
   * @param string $type
   * @param integer $id
   *
   * @return integer
   */
  public static function getNumSample($program_id, $type, $id) {

    // Gets BIMS_MVIEW_PHENOTYPE.
    $bm_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
    $m_phenotype  = $bm_phenotype->getMView();

    // Generates the SQL statement.
    $where_str  = '';
    $args       = array();
    if ($type == 'program') {}
    else if ($type == 'accession') {
      $args[':stock_id'] = $id;
      $where_str = ' AND MV.stock_id = :stock_id ';
    }
    else if ($type == 'trial') {
      $args[':trial_id'] = $id;
      $join_str  = " INNER JOIN {$m_phenotype} MV on MV.project_id = PC.project_id ";
      $where_str = ' AND MV.node_id = :trial_id ';
    }
    else if ($type == 'cross') {
      $args[':nd_experiment_id'] = $id;
      $join_str  = " INNER JOIN {$m_phenotype} MV on MV.sample_id = PC.stock_id ";
      $where_str = ' AND MV.nd_experiment_id = :nd_experiment_id ';
    }
    else {
      return 0;
    }

    // Returns the number of the samples.
    $sql = "SELECT COUNT(MV.stock_id) FROM {$m_phenotype} MV WHERE 1=1 $where_str";
    $num = db_query($sql, $args)->fetchField();
    return ($num) ? $num : 0;
  }

  /**
   * Gets the active descriptors.
   *
   * @param integer $project_id
   *
   * @retrun array
   */
  public function getActiveDescriptors($project_id = NULL) {
    $table = $this->getTableName('phenotype_call');
    $args = array();
    $sql = "SELECT DISTINCT cvterm_id FROM {$table} WHERE 1=1 ";
    if ($project_id) {
      $sql .= " AND project_id = :project_id ";
      $args[':project_id'] = $project_id;
    }
    $results = db_query($sql, $args);
    $descriptors = array();
    while ($cvterm_id = $results->fetchField()) {
      $desc = BIMS_MVIEW_DESCRIPTOR::byID($this->program_id, $cvterm_id);
      if ($desc) {
        $descriptors []= $desc;
      }
    }
    return $descriptors;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the phenotype_call ID.
   *
   * @retrun integer
   */
  public function getPhenotypeCallID() {
    return $this->phenotype_call_id;
  }

  /**
   * Sets the phenotype_call ID.
   *
   * @param integer $phenotype_call_id
   */
  public function setPhenotypeCallID($phenotype_call_id) {
    $this->phenotype_call_id = $phenotype_call_id;
  }
}