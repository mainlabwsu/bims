<?php
/**
 * The declaration of BIMS_CHADO_GENOTYPE_CALL class.
 *
 */
class BIMS_CHADO_GENOTYPE_CALL extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $genotype_call_id   = NULL;
  protected $genotype_call      = NULL;
  protected $genotype_callprop  = NULL;
  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->genotype_call      = $this->getTableName('genotype_call');
    $this->genotype_callprop  = $this->getTableName('genotype_callprop');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'genotype_call') {
      $sql = "select nextval('chado.genotype_call_genotype_call_id_seq'::regclass)";
    }
    else if ($table_name == 'genotype_callprop') {
      $sql = "select nextval('chado.genotype_callprop_genotype_callprop_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getAssocData()
   */
  public function getAssocData($genotype_call_id) {
    return array();
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('genotype_callprop', 'genotype_call');
    }
    return array('genotype_call', 'genotype_callprop');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * @see BIMS_CHADO_TABLE::getObject()
   *
   * @return object
   */
  public function getObject($type) {
    if ($type == 'genotype_call') {
      return $this->byTKey($type, array('genotype_call_id' => $this->genotype_call_id));
    }
    else if ($type == 'genotype_callprop') {
      return $this->byTKey($type, array('genotype_callprop_id' => $this->genotype_callprop_id));
    }
    return NULL;
  }

  /**
   * Checks genotype format.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $genotype
   *
   * @return boolean
   */
  public function checkGenotype(MCL_TEMPLATE $mcl_tmpl = NULL, $genotype) {

    //    if (preg_match("/^([a-z\-])(a-z\-)$/i", $description, $matches)) {
    //      $uniquename = $description = $matches[1] . '|' . $matches[2];
    //    }


    return TRUE;
  }

  /**
   * Adds a new data point.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addData(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array(
      'project_id'  => $details['project_id'],
      'feature_id'  => $details['feature_id'],
      'stock_id'    => $details['stock_id'],
    );
    $data = $this->byTKey('genotype_call', $keys);
    if ($data) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_genotype_call', $keys);
      return $data->genotype_call_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets the next value.
        $genotype_call_id = $this->getNextval('genotype_call');

        // Adds the data.
        $fields = array(
          'genotype_call_id'  => $genotype_call_id,
          'project_id'        => $details['project_id'],
          'feature_id'        => $details['feature_id'],
          'stock_id'          => $details['stock_id'],
          'genotype_id'       => $details['genotype_id'],
          'value'             => $details['value'],
          'contact_id'        => $details['contact_id'],
        );

        // Inserts the record.
        db_insert($this->genotype_call)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'bc_genotype_call', $fields);
        return $genotype_call_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return NULL;
      }
    }
    return $data;
  }

  /**
   * Returns the genotype_call.
   *
   * @param array $keys
   *
   * @return object
   */
  public function getGenotypeCall($keys) {

    // Gets the sample.
    $args = array(
      ':project_id' => $keys['project_id'],
      ':feature_id' => $keys['feature_id'],
      ':stock_id'   => $keys['stock_id'],
    );
    $table_name = $this->getTableName('genotype_call');
    $sql = "
      SELECT T.* FROM {$table_name} T
      WHERE T.project_id = :project_id AND T.feature_id = :feature_id AND T.stock_id = :stock_id
    ";
    return db_query($sql, $args)->fetchObject();
  }

  /**
   * Returns the genotype_callprop.
   *
   * @param array $keys
   *
   * @return object
   */
  public function getGenotypeCallprop($keys) {

    // Gets the sample.
    $args = array(
      ':genotype_call_id' => $keys['genotype_call_id'],
      ':type_id'          => $keys['type_id'],
    );
    $table_name = $this->getTableName('genotype_callprop');
    $sql = "
      SELECT T.* FROM {$table_name} T
      WHERE T.genotype_call_id = :genotype_call_id AND T.type_id = :type_id
    ";
    return db_query($sql, $args)->fetchObject();
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'genotype_call') {
      return self::_getSchemaGenotypeCall();
    }
    else if ($type == 'genotype_callprop') {
      return self::_getSchemaGenotypeCallProp();
    }
    return array();
  }

  /**
   * Returns the schema for genotype_call.
   *
   * @return array
   */
  private function _getSchemaGenotypeCall() {
    $schema = array(
      'description' => 'The custom table for genotype_call.',
      'fields' => array(
        'genotype_call_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'feature_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'feature_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'genotype_id' => array(
          'type' => 'int',
        ),
        'value' => array(
          'type' => 'varchar',
          'length' => '500',
        ),
        'contact_id' => array(
          'type' => 'int',
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('genotype_call_id'),
      'unique keys' => array(
        'ukey_001' => array('project_id', 'feature_id', 'stock_id')
      ),
      'indexes' => array(
        'gc_project_id' => array('project_id'),
        'gc_feature_id' => array('feature_id'),
        'gc_stock_id'   => array('stock_id'),
      ),
      'foreign keys' => array(
        'fkey_bims_genotype_call_001' => array(
          'class'     => 'BIMS_CHADO_GENOTYPE_CALL',
          'key'       => 'genotype_call',
          'field_id'  => 'project_id',
          'ref_class' => 'BIMS_CHADO_PROJECT',
          'ref_key'   => 'project',
          'ref_id'    => 'project_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_genotype_call_002' => array(
          'class'     => 'BIMS_CHADO_GENOTYPE_CALL',
          'key'       => 'genotype_call',
          'field_id'  => 'feature_id',
          'ref_class' => 'BIMS_CHADO_FEATURE',
          'ref_key'   => 'feature',
          'ref_id'    => 'feature_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_genotype_call_003' => array(
          'class'     => 'BIMS_CHADO_GENOTYPE_CALL',
          'key'       => 'genotype_call',
          'field_id'  => 'stock_id',
          'ref_class' => 'BIMS_CHADO_ACCESSION',
          'ref_key'   => 'accession',
          'ref_id'    => 'stock_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_genotype_call_004' => array(
          'class'     => 'BIMS_CHADO_GENOTYPE_CALL',
          'key'       => 'genotype_call',
          'field_id'  => 'genotype_id',
          'ref_class' => 'BIMS_CHADO_GENOTYPE',
          'ref_key'   => 'genotype',
          'ref_id'    => 'genotype_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_genotype_call_005' => array(
          'class'     => 'BIMS_CHADO_GENOTYPE_CALL',
          'key'       => 'genotype_call',
          'field_id'  => 'contact_id',
          'ref_table' => 'chado.contact',
          'ref_id'    => 'contact_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );

    // Removes the foreign to chado tables.
    unset($schema['foreign keys']['fkey_bims_genotype_call_005']);
    return $schema;
  }

  /**
   * Returns the schema for genotype_callprop.
   *
   * @return array
   */
  private function _getSchemaGenotypeCallProp() {
    $schema = array(
      'description' => 'The custom table for genotype_callprop.',
      'fields' => array(
        'genotype_callprop_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'genotype_call_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('genotype_call_id', 'type_id', 'rank')
      ),
      'indexes' => array(
        'gcp_genotype_call_id' => array('genotype_call_id'),
      ),
      'foreign keys' => array(
        'fkey_bims_genotype_callprop_001' => array(
          'class'     => 'BIMS_CHADO_GENOTYPE_CALL',
          'key'       => 'genotype_callprop',
          'field_id'  => 'genotype_call_id',
          'ref_class' => 'BIMS_CHADO_GENOTYPE_CALL',
          'ref_key'   => 'genotype_call',
          'ref_id'    => 'genotype_call_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
    return $schema;
  }

  /**
   * Returns distinct markers.
   *
   * @param integer $flag
   *
   * @return array
   */
  public function getMarkers($flag = BIMS_OPTION) {
    $bc_feature     = new BIMS_CHADO_FEATURE($this->program_id);
    $table_feature  = $bc_feature->getTableName('feature');
    $table_gc       = $this->getTableName('genotype_call');

    // Gets the distinct markers.
    $sql = "
      SELECT F.* FROM {$table_feature} F WHERE F.feature_id IN (
        SELECT DISTINCT GC.feature_id FROM {$table_gc} GC
      ) ORDER BY F.name
    ";
    $results = db_query($sql);
    $markers = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OJBECT) {
        $markers []= $obj;
      }
      else {
        $markers[$obj->feature_id] = $obj->name;
      }
    }
    return $markers;
  }


  /**
   * Returns distinct accession.
   *
   * @param integer $flag
   *
   * @return array
   */
  public function getAccessions($flag = BIMS_OPTION) {
    $bc_accession     = new BIMS_CHADO_ACCESSION($this->program_id);
    $table_accession  = $bc_accession->getTableName('accession');
    $table_gc         = $this->getTableName('genotype_call');

    // Gets the distinct markers.
    $sql = "
      SELECT S.* FROM {$table_accession} S WHERE S.stock_id IN (
        SELECT DISTINCT GC.stock_id FROM {$table_gc} GC
      ) ORDER BY S.name
    ";
    $results = db_query($sql);
    $accessions = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OJBECT) {
        $accessions []= $obj;
      }
      else {
        $accessions[$obj->stock_id] = $obj->name;
      }
    }
    return $accessions;
  }

  /**
   * Returns data object.
   *
   * @param integer $genotype_call_id
   *
   * @return object
   */
  public function getDataPoint($genotype_call_id) {
    if ($genotype_call_id) {
      $table = $this->getTableName('genotype_call');
      $sql = "SELECT MV.* FROM $table MV WHERE MV.genotype_call_id = :genotype_call_id";
      $results = db_query($sql, array(':genotype_call_id' => $genotype_call_id));
      return $results->fetchObject();
    }
    return NULL;
  }

  /**
   * Returns the number of the samples.
   *
   * @param integer $program_id
   * @param string $type
   * @param integer $id
   *
   * @return integer
   */
  public static function getNumData($program_id, $type, $id) {

    // Local variables.
    $bc_gc        = new BIMS_CHADO_GENOTYPE_CALL($program_id);
    $bm_genotype  = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
    $m_genotype   = $bm_genotype->getMView();

    // Generates the SQL statement.
    $where_str  = '';
    $join_str   = '';
    $args       = array();
    if ($type == 'program') {}
    else if ($type == 'accession') {
      $args[':stock_id'] = $id;
      $join_str  = " INNER JOIN {$m_genotype} MV on MV.sample_id = GC.stock_id ";
      $where_str = ' AND MV.stock_id = :stock_id ';
    }
    else if ($type == 'trial') {
      $args[':trial_id'] = $id;
      $where_str = ' MV.node_id = :trial_id ';
    }
    else if ($type == 'marker') {
      $args[':feature_id'] = $id;
      $join_str  = " INNER JOIN {$m_genotype} MV on MV.feature_id = GC.feature_id ";
      $where_str = ' AND MV.feature_id = :feature_id ';
    }
    else {
      return 0;
    }

    // Returns the number of the data.
    $table_gc = $bc_gc->getTableName('genotype_call');
    $sql = "
      SELECT COUNT(GC.genotype_call_id)
      FROM {$table_gc} GC
        $join_strF
      WHERE 1=1 $where_str
    ";
    $num = db_query($sql, $args)->fetchField();
    return ($num) ? $num : 0;
  }

  /**
   * Returns the number of the samples.
   *
   * @param integer $program_id
   * @param string $type
   * @param integer $id
   *
   * @return integer
   */
  public static function getNumSample($program_id, $type, $id) {

    // Local variables.
    $bm_genotype  = new BIMS_MVIEW_GENOTYPE(array('node_id' => $program_id));
    $m_genotype   = $bm_genotype->getMView();

    // Generates the SQL statement.
    $where_str  = '';
    $args       = array();
    if ($type == 'program') {}
    else if ($type == 'accession') {
      $args[':stock_id'] = $id;
      $where_str = ' AND MV.stock_id = :stock_id ';
    }
    else if ($type == 'trial') {
      $args[':trial_id'] = $id;
      $join_str  = " INNER JOIN {$m_genotype} MV on MV.project_id = GC.project_id ";
      $where_str = ' AND MV.node_id = :trial_id ';
    }
    else if ($type == 'marker') {
      $args[':feature_id'] = $id;
      $join_str  = " INNER JOIN {$m_genotype} MV on MV.feature_id = GC.feature_id ";
      $where_str = ' AND MV.feature_id = :feature_id ';
    }
    else {
      return 0;
    }

    // Returns the number of the samples.
    $sql = "SELECT COUNT(MV.stock_id) FROM {$m_genotype} MV WHERE 1=1 $where_str";
    $num = db_query($sql, $args)->fetchField();
    return ($num) ? $num : 0;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the genotype_call ID.
   *
   * @retrun integer
   */
  public function getGenotypeCallID() {
    return $this->genotype_call_id;
  }

  /**
   * Sets the genotype_call ID.
   *
   * @param integer $genotype_call_id
   */
  public function setGenotypeCallID($genotype_call_id) {
    $this->genotype_call_id = $genotype_call_id;
  }
}