<?php
/**
 * The declaration of BIMS_CHADO_SYNONYML class.
 *
 */
class BIMS_CHADO_SYNONYM extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $synonym = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->synonym = $this->getTableName('synonym');
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('synonym');
    }
    return array('synonym');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Adds a synonym.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addSynonym(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array(
      'target_type' => $details['target_type'],
      'target_id'   => $details['target_id'],
      'type_id'     => $details['type_id'],
      'name'        => $details['name'],
    );
    $synonym = $this->byTKey('synonym', $keys);
    if ($synonym) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_synonym', $keys);
      return $synonym->target_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Adds the synonym.
        $fields = array(
          'target_type' => $details['target_type'],
          'target_id'   => $details['target_id'],
          'type_id'     => $details['type_id'],
          'name'        => $details['name'],
        );

        // Inserts the record.
        db_insert($this->synonym)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'bc_synonym', $fields);
        return $details['target_id'];
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
        return NULL;
      }
    }
    return $data;
  }

  /**
   * Returns the synonyms.
   *
   * @param string $target_type
   * @param integer $target_id
   * @param integer $type_id
   * @param integer $flag
   * @param string $glue
   *
   * @return array
   */
  public function getSynonyms($target_type, $target_id, $type_id, $flag = BIMS_STRING, $glue = ', ') {

    // Gets the sample.
    $args = array(
      ':target_type'  => $target_type,
      ':target_id'    => $target_id,
      ':type_id'      => $type_id,
    );
    $table_name = $this->getTableName('synonym');
    $sql = "
      SELECT T.*
      FROM {$table_name} T
      WHERE LOWER(T.target_type) = LOWER(:target_type)
        AND T.target_id = :target_id AND T.type_id = :type_id
    ";
    $results = db_query($sql, $args);
    $synonyms = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OBJECT) {
        $synonyms []= $obj;
      }
      else {
        $synonyms []= $obj->name;
      }
    }
    if ($flag == BIMS_STRING) {
      return implode($glue, $synonyms);
    }
    return $synonyms;
  }

  /**
   * Returns the synonym.
   *
   * @param array $keys
   *
   * @return object
   */
  public function getSynonym($keys) {

    // Gets the sample.
    $args = array(
      ':target_type'  => $keys['target_type'],
      ':target_id'    => $keys['target_id'],
      ':type_id'      => $keys['type_id'],
      ':name'         => $keys['name'],
    );
    $table_name = $this->getTableName('synonym');
    $sql = "
      SELECT T.*
      FROM {$table_name} T
      WHERE T.target_id = :target_id AND T.type_id = :type_id
        AND LOWER(T.target_type) = LOWER(:target_type)
        AND LOWER(T.name) = LOWER(:name)
    ";
    return db_query($sql, $args)->fetchObject();
  }

  /**
   * Deletes the synonyms.
   *
   * @param string $target_type
   * @param integer $target_id
   * @param string $cvterm_name
   * @param string $cv_name
   * @param string $name
   *
   * @return array
   */
  public function deleteSynonym($target_type, $target_id, $cv_name = 'SITE_CV', $cvterm_name, $name = '') {

    // Gets the type_id.
    $type_id = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name)->getCvtermID();

    // Deletes the sysnonyms.
    if ($name) {
      db_delete($this->getTableName('synonym'))
        ->condition('target_type', $target_type, '=')
        ->condition('target_id', $target_id, '=')
        ->condition('type_id', $type_id, '=')
        ->condition('name', $name, '=')
        ->execute();
    }
    else {
      db_delete($this->getTableName('synonym'))
        ->condition('target_type', $target_type, '=')
        ->condition('target_id', $target_id, '=')
        ->condition('type_id', $type_id, '=')
        ->execute();
    }
    return TRUE;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'synonym') {
      return self::_getSchemaSynonym();
    }
    return array();
  }

  /**
   * Returns the schema for synonym.
   *
   * @return array
   */
  private function _getSchemaSynonym() {
    $schema = array(
      'description' => 'The custom table for synonym.',
      'fields' => array(
        'target_type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'target_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('target_type', 'target_id', 'type_id', 'name')
      ),
    );
    return $schema;
  }
}