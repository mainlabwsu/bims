<?php
/**
 * The declaration of BIMS_CHADO_SSR class.
 *
 */
class BIMS_CHADO_SSR extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $nd_experiment_id     = NULL;
  protected $nd_experimentprop_id = NULL;
  protected $nd_experiment        = NULL;
  protected $nd_experimentprop    = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->nd_experiment      = $this->getTableName('ssr');
    $this->nd_experimentprop  = $this->getTableName('ssrprop');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'nd_experiment') {
      $sql = "select nextval('chado.nd_experiment_nd_experiment_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experimentprop') {
      $sql = "select nextval('chado.nd_experimentprop_nd_experimentprop_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_project') {
      $sql = "select nextval('chado.nd_experiment_project_nd_experiment_project_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_stock') {
      $sql = "select nextval('chado.nd_experiment_stock_nd_experiment_stock_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_contact') {
      $sql = "select nextval('chado.nd_experiment_contact_nd_experiment_contact_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_genotype') {
      $sql = "select nextval('chado.nd_experiment_genotype_nd_experiment_genotype_id_seq'::regclass)";
    }
    else if ($table_name == 'stock_relationship') {
      $sql = "select nextval('chado.stock_relationship_stock_relationship_id_seq'::regclass)";
    }
    else if ($table_name == 'feature_genotype') {
      $sql = "select nextval('chado.feature_genotype_feature_genotype_id_seq'::regclass)";
    }
    else if ($table_name == 'stock') {
      $sql = "select nextval('chado.stock_stock_id_seq'::regclass)";
    }
    else if ($table_name == 'genotype') {
      $sql = "select nextval('chado.genotype_genotype_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getAssocData()
   */
  public function getAssocData($nd_experiment_id) {
    return array();
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('ssrprop', 'ssr');
    }
    return array('ssr', 'ssrprop');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Returns the SSR.
   *
   * @param integer $nd_experiment_id
   *
   * @return object
   */
  public function getSSR($nd_experiment_id) {
    return $this->byTKey('ssr', array('nd_experiment_id' => $nd_experiment_id));
  }

  /**
   * Updates the SSR.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateSSR($details) {

    // Gets the variables.
    $nd_experiment_id = $details['nd_experiment_id'];

    // Updates the SSR.
    $transaction = db_transaction();
    try {

      // --------------------------------- //
      // Updates the properties of the SSR.
      $fields = array();

      // Updates the accession.
      db_update($this->getTableName('ssr'))
        ->fields()
        ->condition('nd_experiment_id', $nd_experiment_id, '=')
        ->execute();

      // --------------------------------- //
      // Updates the SSR properties.


    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new SSR.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addSSR(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Gets the variables.
    $program_id = $details['program_id'];
    $dup_key    = $details['dup_key'];
    $project_id = $details['project_id'];
    $stock_id   = $details['stock_id'];
    $feature_id = $details['feature_id'];
    $contact_id = $details['contact_id'];
    $allele     = $details['allele'];

    // Checks for duplication.
    $keys = array('dup_key' => $dup_key);
    $ssr = $this->byTKey('ssr', $keys);
    if ($ssr) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_ssr', $keys);
      return $ssr->nd_experiment_id;
    }
    else {

      // Gets BIMS_CHADO.
      $bc_accession = new BIMS_CHADO_SITE($program_id);
      $bc_site      = new BIMS_CHADO_SITE($program_id);

      // Adds a SSR.
      $transaction = db_transaction();
      try {

        // Sets the default values.
        $nd_geolocation_id = $bc_site->getLocationIDBySite('');

        // Gets the cvterms.
        $cvterm_id_sample           = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'sample')->getCvtermID();
        $cvterm_id_genotyping       = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'genotyping')->getCvtermID();
        $cvterm_id_genotyping_date  = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'genotyping_date')->getCvtermID();
        $cvterm_id_sample_of        = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'sample_of')->getCvtermID();

        // Gets the stock.
        $stock = $bc_accession->getAccession($stock_id);
        if (!$stock) {
          throw new Exception("Error : The stock ($stock_id) could not found");
        }

        // Gets the sample.
        $sample_name        = sprintf("%d-%d-%d", $project_id, $stock_id, $feature_id);
        $sample_uniquename  = $program_id . "-$sample_name";
        $sample             = $bc_accession->getAccessionByUniquename($sample_uniquename);
        if (!$sample) {

          // Adds the sample.
          $details = array(
            'uniquename'  => $sample_uniquename,
            'name'        => $sample_name,
            'type_id'     => $cvterm_id_sample,
            'organism_id' => $stock->organism_id,
          );
          $sample_id = $bc_accession->addAccession(NULL, $details);
          $sample = $bc_accession->getAccession($sample_id);
        }

        // Adds stock-sample relationship.
        $bc_accession->setStockID($sample_id);
        $bc_accession->addRelatedAccession(NULL, $sample, $cvterm_id_sample_of);

        // Gets the next value.
        $nd_experiment_id           = $this->getNextval('nd_experiment');
        $nd_experimentprop_id       = $this->getNextval('nd_experimentprop');
        $nd_experiment_project_id   = $this->getNextval('nd_experiment_project');
        $nd_experiment_stock_id     = $this->getNextval('nd_experiment_stock');
        $nd_experiment_contact_id   = $this->getNextval('nd_experiment_contact');
        $nd_experiment_genotype_id  = $this->getNextval('nd_experiment_genotype');
        $feature_genotype_id        = $this->getNextval('feature_genotype');
        $genotype_id                = $this->getNextval('genotype');

        // Sets the uniquename of genotype.
        $genotype_uniquename = $feature_id . '_' . $allele;

        // Adds the data.
        $fields = array(
          'nd_experiment_id'          => $nd_experiment_id,
          'nd_geolocation_id'         => $nd_geolocation_id,
          'ne_type_id'                => $cvterm_id_genotyping,
          'nep_id_dup_key'            => $nd_experimentprop_id,
          'nd_experiment_project_id'  => $nd_experiment_project_id,
          'nd_experiment_stock_id'    => $nd_experiment_stock_id,
          'nd_experiment_genotype_id' => $nd_experiment_genotype_id,
          'feature_genotype_id'       => $feature_genotype_id,
          'genotype_id'               => $genotype_id,
          'stock_id'                  => $stock_id,
          'sample_id'                 => $sample_id,
          'dup_key'                   => $sample->getUniquename(),
          'project_id'                => $project_id,
          'feature_id'                => $feature_id,
          'contact_id'                => $contact_id,
          'genotype_uniquename'       => genotype_uniquename,
          'allele'                    => $allele,
        );

        // Inserts the record.
        db_insert($this->nd_experiment)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'bc_ssr', $fields);

        // Gets the properties.
        $props = array();
        if (array_key_exists('genotyping_date', $details)) {
          $props[$cvterm_id_genotyping_date] = $details['genotyping_date'];
        }

        // Adds the properties.
        foreach ((array)$props as $type_id => $value) {

          // Checks for duplication.
          $keys = array(
            'nd_experiment_id'  => $nd_experiment_id,
            'type_id'           => $type_id,
          );
          $ssrprop = NULL;
          if ($ssrprop) {
            $this->addMsg($mcl_tmpl, 'D', 'bc_ssrprop', $keys);
          }
          else {

            // Gets the next value.
            $nd_experimentprop_id = $this->getNextval('nd_experimentprop');

            // Adds a property.
            $fields = array(
              'nd_experimentprop_id' => $nd_experimentprop_id,
              'nd_experiment_id'     => $nd_experiment_id,
              'type_id'              => $type_id,
              'value'                => $value,
            );
            db_insert($this->nd_experimentprop)
              ->fields($fields)
              ->execute();
          }
        }
        return $nd_experiment_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return NULL;
  }

  /**
   * Retrieves the property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $separator
   *
   * @return string
   */
  public function getProp($cv_name, $cvterm_name, $separator = '') {
    return $this->getProperty($this->nd_experimentprop, 'nd_experiment_id', $this->nd_experiment_id, $cv_name, $cvterm_name, $separator);
  }

  /**
   * Adds a property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $value
   * @param string $separator
   *
   * @return boolean
   */
  public function addProp(MCL_TEMPLATE $mcl_tmpl = NULL, $cv_name, $cvterm_name, $value, $separator = '') {
    if ($value != '') {
      $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
      if ($cvterm) {
        $type_id = $cvterm->getCvtermID();
        return $this->addProperty($mcl_tmpl, 'nd_experimentprop', 'nd_experimentprop_id', 'ssrprop', 'nd_experiment_id', $this->nd_experiment_id, $type_id, $value, $separator);
      }
    }
    return TRUE;
  }

  /**
   * Adds a property by ID.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $type_id
   * @param string $value
   *
   * @return boolean
   */
  public function addPropByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type_id, $value) {
    return $this->addProperty($mcl_tmpl, 'nd_experimentprop', 'nd_experimentprop_id', 'ssrprop', 'nd_experiment_id', $this->nd_experiment_id, $type_id, $value, $separator);
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'ssr') {
      return self::_getSchemaSSR();
    }
    else if ($type == 'ssrprop') {
      return self::_getSchemaSSRprop();
    }
    return array();
  }

  /**
   * Returns the schema for SSR.
   *
   * @return array
   */
  private function _getSchemaSSR() {
    $schema = array(
      'description' => 'The custom table for SSR.',
      'fields' => array(
        'nd_experiment_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'ne_type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'dup_key' => array(
          'type' => 'text',
          'not null' => TRUE,
        ),
        'nep_id_dup_key' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_genotype_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'stock_relationship' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'feature_genotype_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'genotype_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'sample_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'feature_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'contact_id' => array(
          'type' => 'int',
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_experiment_id'),
      'unique keys' => array(
        'ukey_001' => array('stock_id', 'feature_id', 'genotype_id')
      ),
      'indexes' => array(
        'ssr_project_id'  => array('project_id'),
        'ssr_stock_id'    => array('stock_id'),
        'ssr_feature_id'  => array('feature_id'),
      ),
      'foreign keys' => array(
        'fkey_bims_ssr_001' => array(
          'class'     => 'BIMS_CHADO_SSR',
          'key'       => 'ssr',
          'field_id'  => 'project_id',
          'ref_class' => 'BIMS_CHADO_PROJECT',
          'ref_key'   => 'project',
          'ref_id'    => 'project_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_ssr_002' => array(
          'class'     => 'BIMS_CHADO_SSR',
          'key'       => 'ssr',
          'field_id'  => 'sample_id',
          'ref_class' => 'BIMS_CHADO_ACCESSION',
          'ref_key'   => 'accession',
          'ref_id'    => 'stock_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_ssr_003' => array(
          'class'     => 'BIMS_CHADO_SSR',
          'key'       => 'ssr',
          'field_id'  => 'feature_id',
          'ref_class' => 'BIMS_CHADO_FEATURE',
          'ref_key'   => 'feature',
          'ref_id'    => 'feature_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_ssr_004' => array(
          'class'     => 'BIMS_CHADO_SSR',
          'key'       => 'ssr',
          'field_id'  => 'nd_geolocation_id',
          'ref_class' => 'BIMS_CHADO_SITE',
          'ref_key'   => 'site',
          'ref_id'    => 'nd_geolocation_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_ssr_005' => array(
          'class'     => 'BIMS_CHADO_SSR',
          'key'       => 'ssr',
          'field_id'  => 'contact_id',
          'ref_table' => 'chado.contact',
          'ref_id'    => 'contact_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );

    // Removes the foreign to chado tables.
    unset($schema['foreign keys']['fkey_bims_ssr_005']);
    return $schema;
  }

  /**
   * Returns the schema for SSR property.
   *
   * @return array
   */
  private function _getSchemaSSRprop() {
    return array(
      'description' => 'The custom table for SSR property.',
      'fields' => array(
        'nd_experimentprop_id' => array(
          'type' => 'int',
          'not null'  => TRUE,
        ),
        'nd_experiment_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_experimentprop_id'),
      'unique keys' => array(
        'ukey_001' => array('nd_experiment_id', 'type_id', 'rank')
      ),
      'foreign keys' => array(
        'fkey_bims_ssrprop_001' => array(
          'class'     => 'BIMS_CHADO_SSR',
          'key'       => 'ssrprop',
          'field_id'  => 'nd_experiment_id',
          'ref_class' => 'BIMS_CHADO_SSR',
          'ref_key'   => 'ssr',
          'ref_id'    => 'nd_experiment_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the nd_experiment ID.
   *
   * @retrun integer
   */
  public function getNdExperimentID() {
    return $this->nd_experiment_id;
  }

  /**
   * Sets the nd_experiment ID.
   *
   * @param integer $nd_experiment_id
   */
  public function setNdExperimentID($nd_experiment_id) {
    $this->nd_experiment_id = $nd_experiment_id;
  }

  /**
   * Retrieves the nd_experimentprop ID.
   *
   * @retrun integer
   */
  public function getNdExperimentpropID() {
    return $this->nd_experimentprop_id;
  }

  /**
   * Sets the nd_experimentprop ID.
   *
   * @param integer $nd_experimentprop_id
   */
  public function setNdExperimentpropID($nd_experimentprop_id) {
    $this->nd_experimentprop_id = $nd_experimentprop_id;
  }
}
