<?php
/**
 * The declaration of BIMS_CHADO_ND_EXPERIMENT class.
*
*/
class BIMS_CHADO_ND_EXPERIMENT extends BIMS_CHADO_TABLE {

  /**
   *  Class data members.
   */
  protected $nd_experiment_id       = NULL;
  protected $nd_experimentprop_id   = NULL;
  protected $nd_experiment          = NULL;
  protected $nd_experimentprop      = NULL;
  protected $nd_experiment_contact  = NULL;

  /**
   * @see BIMS_CHADO_TABLE::__construct()
   */
  public function __construct($program_id) {
    parent::__construct($program_id);

    // Initializes the data members.
    $this->nd_experiment          = $this->getTableName('nd_experiment');
    $this->nd_experimentprop      = $this->getTableName('nd_experimentprop');
    $this->nd_experiment_contact  = $this->getTableName('nd_experiment_contact');
  }

  /**
   * The next value of the sequence.
   *
   * @return integer
   */
  public function getNextval($table_name) {
    $sql = '';
    if ($table_name == 'nd_experiment') {
      $sql = "select nextval('chado.nd_experiment_nd_experiment_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experimentprop') {
      $sql = "select nextval('chado.nd_experimentprop_nd_experimentprop_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_project') {
      $sql = "select nextval('chado.nd_experiment_project_nd_experiment_project_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_stock') {
      $sql = "select nextval('chado.nd_experiment_stock_nd_experiment_stock_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_contact') {
      $sql = "select nextval('chado.nd_experiment_contact_nd_experiment_contact_id_seq'::regclass)";
    }
    else if ($table_name == 'nd_experiment_genotype') {
      $sql = "select nextval('chado.nd_experiment_genotype_nd_experiment_genotype_id_seq'::regclass)";
    }
    else if ($table_name == 'genotype') {
      $sql = "select nextval('chado.genotype_genotype_id_seq'::regclass)";
    }
    if ($sql) {
      return db_query($sql)->fetchField();
    }
    return NULL;
  }

  /**
   * @see BIMS_CHADO_TABLE::getTables()
   */
  public function getTables($flag = BIMS_CREATE) {
    if ($flag == BIMS_DELETE) {
      return array('nd_experiment_contact', 'nd_experimentprop', 'nd_experiment');
    }
    return array('nd_experiment', 'nd_experimentprop', 'nd_experiment_contact');
  }

  /**
   * @see BIMS_CHADO_TABLE::createTable()
   */
  public function createTable($tables = array(), $recreate = FALSE) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_CREATE);
    }
    return parent::createTable($tables, $recreate);
  }

  /**
   * @see BIMS_CHADO_TABLE::dropTable()
   */
  public function dropTable($tables = array()) {
    if (empty($tables)) {
      $tables = $this->getTables(BIMS_DELETE);
    }
    return parent::dropTable($tables);
  }

  /**
   * Returns the nd_experiment.
   *
   * @param integer $nd_experiment_id
   *
   * @return object
   */
  public function getNdExperiment($nd_experiment_id) {
    return $this->byTKey('nd_experiment', array('nd_experiment_id' => $nd_experiment_id));
  }

  /**
   * Updates the nd_experiment.
   *
   * @param array $details
   *
   * @return boolean
   */
  public function updateNdExperiment($details) {

    // Gets the variables.
    $nd_experiment_id   = $details['nd_experiment_id'];
    $dup_key            = $details['dup_key'];

   // Updates the nd_experiment.
    $transaction = db_transaction();
    try {

      // --------------------------------- //
      // Updates the properties of the nd_experiment.
      $fields = array(
        'dup_key' => $dup_key,
      );

      // Updates the accession.
      db_update($this->getTableName('nd_experiment'))
        ->fields($fields)
        ->condition('nd_experiment_id', $nd_experiment_id, '=')
        ->execute();

      // --------------------------------- //
      // Updates the nd_experiment properties.


    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Adds a new nd_experiment.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param array $details
   *
   * @return integer
   */
  public function addNdExperiment(MCL_TEMPLATE $mcl_tmpl = NULL, $details) {

    // Checks for duplication.
    $keys = array('dup_key' => $details['dup_key']);
    $nd_experiment = $this->byTKey('nd_experiment', $keys);
    if ($nd_experiment) {
      $this->addMsg($mcl_tmpl, 'D', 'bc_nd_experiment', $keys);
      return $nd_experiment->nd_experiment_id;
    }
    else {
      $transaction = db_transaction();
      try {

        // Gets BIMS_CHADO tables.
        $bc_site        = new BIMS_CHADO_SITE($this->program_id);
        $table_site     = $bc_site->getTableName('site');

        // Gets the cvterms.
        $cvterm_id_genotyping = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'genotyping')->getCvtermID();
        $cvterm_id_sample_id  = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'sample_id')->getCvtermID();

        // Gets the next value.
        $nd_experiment_id           = $this->getNextval('nd_experiment');
        $nd_experimentprop_id       = $this->getNextval('nd_experimentprop');
        $nd_experiment_project_id   = $this->getNextval('nd_experiment_project');
        $nd_experiment_stock_id     = $this->getNextval('nd_experiment_stock');
        $nd_experiment_genotype_id  = $this->getNextval('nd_experiment_genotype');

        // Gets nd_geolocation_id.
        $site = array_key_exists('site', $details) ? $details['site'] : '';
        $nd_geolocation_id = $bc_site->getLocationIDBySite($site);

        // Adds the data.
        $fields = array(
          'nd_experiment_id'          => $nd_experiment_id,
          'type_id'                   => $details['type_id'],
          'nd_geolocation_id'         => $nd_geolocation_id,
          'dup_key'                   => $details['dup_key'],
          'dup_key_type_id'           => $details['dup_key_type_id'],
          'dup_key_prop_id'           => $nd_experimentprop_id,
          'nd_experiment_project_id'  => $nd_experiment_project_id,
          'project_id'                => $details['project_id'],
          'nd_experiment_stock_id'    => $nd_experiment_stock_id,
          'nes_type_id'               => $cvterm_id_genotyping,
          'stock_id'                  => $details['stock_id'],
          'nd_experiment_genotype_id' => $nd_experiment_genotype_id,
          'genotype_id'               => $details['genotype_id'],
        );

        // Adds a contact if exist.
        if (array_key_exists('contact_id', $details)) {
          $fields['nd_experiment_contact_id'] = $this->getNextval('nd_experiment_contact');
          $fields['contact_id']               = $details['contact_id'];
        }

        // Adds an nd_experiment.
        db_insert($this->nd_experiment)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'nd_experiment', $fields);

        // inserts the nd_experimentprop.
        // Adds the data.
        $fields = array(
          'nd_experimentprop_id'  => $nd_experimentprop_id,
          'nd_experiment_id'      => $nd_experiment_id,
          'type_id'               => $cvterm_id_sample_id,
          'value'                 => $details['dup_key'],
        );
        db_insert($this->nd_experimentprop)
          ->fields($fields)
          ->execute();
        $this->addMsg($mcl_tmpl, 'N', 'nd_experimentprop', $fields);
        return $nd_experiment_id;
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog('MCL', $e->getMessage(), array(), WATCHDOG_ERROR);
      }
    }
    return NULL;
  }

  /**
   * Adds a contact(s).
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $contact_name
   * @param string $separator
   *
   * @return boolean
   */
  public function addContact(MCL_TEMPLATE $mcl_tmpl = NULL, $contact_name, $separator = '') {

    // Adds contacts.
    if ($contact_name) {
      $value_arr = preg_split($this->getSepRegex($separator), $contact_name, -1, PREG_SPLIT_NO_EMPTY);

      // Checks duplication before adding new contacts.
      $err_flag = FALSE;
      foreach ($value_arr as $val) {
        $val = trim($val);

        $contact = MCL_CHADO_CONTACT::byName($val);
        if (!$contact) {
          $this->updateMsg($mcl_tmpl, 'E', "Error : $val does not exist in chado.contact table");
          continue;
        }
        $args = array(
          'nd_experiment_id'  => $this->nd_experiment_id,
          'contact_id'        => $contact->getContactID(),
        );
        $obj = $this->byTKey('nd_experiment_contact', $args);
        if ($obj) {
          $this->addMsg($mcl_tmpl, 'D', $table_name, $args);
        }
        else {

          // Gets the next value.
          $nd_experiment_contact_id = $this->getNextval('nd_experiment_contact');

          // Adds the data.
          $table_name = $this->getTableName('nd_experiment_contact');
          $transaction = db_transaction();
          try {
            $args['nd_experiment_contact_id'] = $nd_experiment_contact_id;
            db_insert($table_name)
              ->fields($args)
              ->execute();
            $this->addMsg($mcl_tmpl, 'N', $table_name, $args);
            return $nd_experiment_contact_id;
          }
          catch (Exception $e) {
            $transaction->rollback();
            watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
          }
        }
      }
    }
  }

  /**
   * Gets the contacts.
   *
   * @param string $separator
   *
   * @return string
   */
  public function getContacts($separator = '; ') {

    // Gets the all contacts associated with this nd_experiment.
    $table_name = $this->getTableName('nd_experiment_contact');
    $sql = "
      SELECT DISTINCT C.name FROM {chado.contact} C
        INNER JOIN {$table_name} NEC on NEC.contact_id = C.contact_id
      WHERE NEC.nd_experiment_id = :nd_experiment_id
    ";
    $contacts = array();
    $results = db_query($sql, array(':nd_experiment_id' => $this->nd_experiment_id));
    while ($name = $results->fetchField()) {
      $contacts []= $name;
    }
    return implode($separator, $contacts);
  }

  /**
   * Retrieves the property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $separator
   *
   * @return string
   */
  public function getProp($cv_name, $cvterm_name, $separator = '') {
    return $this->getProperty($this->nd_experimentprop, 'nd_experiment_id', $this->nd_experiment_id, $cv_name, $cvterm_name, $separator);
  }

  /**
   * Adds a property.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param string $cv_name
   * @param string $cvterm_name
   * @param string $value
   * @param string $separator
   *
   * @return boolean
   */
  public function addProp(MCL_TEMPLATE $mcl_tmpl = NULL, $cv_name, $cvterm_name, $value, $separator = '') {
    if ($value != '') {
      $cvterm = MCL_CHADO_CVTERM::getCvterm($cv_name, $cvterm_name);
      if ($cvterm) {
        $type_id = $cvterm->getCvtermID();
        return $this->addProperty($mcl_tmpl, 'nd_experimentprop', 'nd_experimentprop_id', 'nd_experimentprop', 'nd_experiment_id', $this->nd_experiment_id, $type_id, $value, $separator);
      }
    }
    return TRUE;
  }

  /**
   * Adds a property by ID.
   *
   * @param MCL_TEMPLATE $mcl_tmpl
   * @param integer $type_id
   * @param string $value
   *
   * @return boolean
   */
  public function addPropByID(MCL_TEMPLATE $mcl_tmpl = NULL, $type_id, $value) {
    return $this->addProperty($mcl_tmpl, 'nd_experimentprop', 'nd_experimentprop_id', 'nd_experimentprop', 'nd_experiment_id', $this->nd_experiment_id, $type_id, $value, $separator);
  }

  /**
   * @see BIMS_CHADO_TABLE::getTableSchema()
   */
  public function getTableSchema($type) {
    if ($type == 'nd_experiment') {
      return self::_getSchemaNdExperiment();
    }
    else if ($type == 'nd_experimentprop') {
      return self::_getSchemaNdExperimentprop();
    }
    else if ($type == 'nd_experiment_contact') {
      return self::_getSchemaNdExperimentContact();
    }
    return array();
  }

  /**
   * Returns the schema for nd_experiment.
   *
   * @return array
   */
  private function _getSchemaNdExperiment() {
    return array(
      'description' => 'The custom table for nd_experiment.',
      'fields' => array(
        'nd_experiment_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_geolocation_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'dup_key' => array(
          'type' => 'text',
          'not null' => TRUE,
        ),
        'dup_key_type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'dup_key_prop_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'project_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nes_type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'stock_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_genotype_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'genotype_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_experiment_id'),
      'fkey_bims_nde_001' => array(
        'field_id'  => 'project_id',
        'ref_class' => 'BIMS_CHADO_PROJECT',
        'ref_key'   => 'project',
        'ref_id'    => 'project_id',
        'on'        => 'DELETE CASCADE',
      ),
      'fkey_bims_nde_002' => array(
        'field_id'  => 'stock_id',
        'ref_class' => 'BIMS_CHADO_ACCESSION',
        'ref_key'   => 'accession',
        'ref_id'    => 'stock_id',
        'on'        => 'DELETE CASCADE',
      ),
      'fkey_bims_nde_003' => array(
        'field_id'  => 'genotype_id',
        'ref_class' => 'BIMS_CHADO_GENOTYPE',
        'ref_key'   => 'genotype',
        'ref_id'    => 'genotype_id',
        'on'        => 'DELETE CASCADE',
      ),
    );
  }

  /**
   * Returns the schema for nd_experiment property.
   *
   * @return array
   */
  private function _getSchemaNdExperimentprop() {
    return array(
      'description' => 'The custom table for nd_experiment property.',
      'fields' => array(
        'nd_experimentprop_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'type_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'value' => array(
          'type' => 'text',
        ),
        'rank' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
        'flag' => array(
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nd_experimentprop_id'),
      'unique keys' => array(
        'ukey_001' => array('nd_experiment_id', 'type_id', 'rank')
      ),
      'foreign keys' => array(
        'fkey_bims_ndep_001' => array(
          'class'     => 'BIMS_CHADO_ND_EXPERIMENT',
          'key'       => 'nd_experimentprop',
          'field_id'  => 'nd_experiment_id',
          'ref_class' => 'BIMS_CHADO_ND_EXPERIMENT',
          'ref_key'   => 'nd_experiment',
          'ref_id'    => 'nd_experiment_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );
  }

  /**
   * Returns the schema for nd_experiment and contact.
   *
   * @return array
   */
  private function _getSchemaNdExperimentContact() {
    $schema = array(
      'description' => 'The custom table for nd_experiment and contact.',
      'fields' => array(
        'nd_experiment_contact_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'nd_experiment_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
        'contact_id' => array(
          'type' => 'int',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('nd_experiment_contact_id'),
      'unique keys' => array(
        'ukey_001' => array('nd_experiment_id', 'contact_id')
      ),
      'foreign keys' => array(
        'fkey_bims_ndec_001' => array(
          'class'     => 'BIMS_CHADO_ND_EXPERIMENT',
          'key'       => 'nd_experiment_contact',
          'field_id'  => 'nd_experiment_id',
          'ref_class' => 'BIMS_CHADO_ND_EXPERIMENT',
          'ref_key'   => 'nd_experiment',
          'ref_id'    => 'nd_experiment_id',
          'on'        => 'DELETE CASCADE',
        ),
        'fkey_bims_ndec_002' => array(
          'class'     => 'BIMS_CHADO_ND_EXPERIMENT',
          'key'       => 'accession_contact',
          'field_id'  => 'contact_id',
          'ref_table' => 'chado.contact',
          'ref_id'    => 'contact_id',
          'on'        => 'DELETE CASCADE',
        ),
      ),
    );

    // Removes the foreign to chado table.
    unset($schema['foreign keys']['fkey_bims_ndec_002']);
    return $schema;
  }

  /*
   * Defines getters and setters below.
   */
  /**
   * Retrieves the nd_experiment ID.
   *
   * @retrun integer
   */
  public function getNdExperimentID() {
    return $this->nd_experiment_id;
  }

  /**
   * Sets the nd_experiment ID.
   *
   * @param integer $nd_experiment_id
   */
  public function setNdExperimentID($nd_experiment_id) {
    $this->nd_experiment_id = $nd_experiment_id;
  }

  /**
   * Retrieves the nd_experimentprop ID.
   *
   * @retrun integer
   */
  public function getNdExperimentpropID() {
    return $this->nd_experimentprop_id;
  }

  /**
   * Sets the nd_experimentprop ID.
   *
   * @param integer $nd_experimentprop_id
   */
  public function setNdExperimentpropID($nd_experimentprop_id) {
    $this->nd_experimentprop_id = $nd_experimentprop_id;
  }
}
