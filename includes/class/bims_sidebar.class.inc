<?php
/**
 * The declaration of BIMS_SIDEBAR class.
 *
 */
class BIMS_SIDEBAR {

  /**
   * Class data members.
   */
 /**
   * Implements the class constructor.
   *
   * @param $details
   */
  public function __construct() {
    // Initializes data members.
  }

  /**
   * Implements the class destructor.
   */
  public function __destruct() {}

  /**
   * Returns the all sidebars in an array.
   *
   * @return array
   */
  public static function getSidebars() {
    $bims_sidebar = new BIMS_SIDEBAR();
    return $bims_sidebar->_getSidebars();
  }

  /**
   * Returns the contens of sidebars.
   *
   * @return array
   */
  private function _getSidebars() {

    // Gets BIMS_USER and BIMS_PROGRAM.
    $bims_user = getBIMS_USER();

    // Gets the contents of sidebars.
    return array(
      'about'           => $this->_getSidebarAbout($bims_user),
      'help'            => $this->_getSidebarHelp($bims_user),
      'archive'         => $this->_getSidebarArchive($bims_user),
      'breeding'        => $this->_getSidebarBreeding($bims_user),
      'data_import'     => $this->_getSidebarDataImport($bims_user),
      'search'          => $this->_getSidebarSearch($bims_user),
      'fieldbook'       => $this->_getSidebarFieldbook($bims_user),
      'data_analysis'   => $this->_getSidebarDataAnalysis($bims_user),
      'breeding_tools'  => '',//$this->_getSidebarBreedingTools($bims_user),
    );
  }

  /**
   * SIDEBAR : About BIMS.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarAbout(BIMS_USER $bims_user) {

    // Lists of links for 'About BIMS'.
    $lists = array(
      'what_is_bims'    => 'What is BIMS?',
      'bims_team'       => 'The BIMS team',
      'work_completed'  => 'Work Completed',
      'work_progress'   => 'Work in Progress',
      'who_is_using'    => 'Locations of BIMS Breeders',
    );
    $items = array();
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.add_main_panel('bims_panel_add_on_about', 'About BIMS', 'bims/load_main_panel/bims_panel_add_on_about/";
    foreach ($lists as $name => $label) {
      $items[] = $prefix . "$name'); return (false);\">$label</a>";
    }
    $items[] = "<a href='/mailing_lists' target='_blank'>Join Our Mailing List</a>";
    $items[] = "<a href='/contact' target='_blank'>Contact Us</a>";
    return theme('item_list', array('items' => $items));
  }

  /**
   * SIDEBAR : Help
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarHelp(BIMS_USER $bims_user) {

    // Lists of links for 'Help'.
    $items = array();
    $items[] = "<a href='/BIMS_manual' target='_blank'>User's Guide</a>";
    $items[] = "<a href='/bimsFAQ' target='_blank'>FAQ and Troubleshooting</a>";
    $items[] = "<a href='/mailing_list' target='_blank'>Join Our Mailing List</a>";
    $items[] = "<a href='/contact' target='_blank'>Contact Us</a>";
    return theme('item_list', array('items' => $items));;
  }

  /**
   * SIDEBAR : Archive.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarArchive(BIMS_USER $bims_user) {

    // Checks for errors.
    $error = BIMS_PANEL::preCheckSidebar($bims_user, TRUE);
    if (!empty($error)) {
      return $error['items']['#markup'];
    }

    // List of links for managing Archives.
    $lists = array(
      'bims_panel_main_archive' => 'Manage Archive',
    );
    $items = array();
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";
    foreach ($lists as $name => $label) {
      $items[] = $prefix . "('$name')\">$label</a>";
    }
    return theme('item_list', array('items' => $items));
  }

  /**
   * SIDEBAR : Manage Breeding.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarBreeding(BIMS_USER $bims_user) {

    // Checks for errors.
    $error = BIMS_PANEL::preCheckSidebar($bims_user);
    if (!empty($error)) {
      return $error['items']['#markup'];
    }

    // Gets BIMS_PROGRAM and the accession label.
    $bims_program = $bims_user->getProgram();
    $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

    // Lists of links for 'Manage Breeding'.
    $lists = array(
      'bims_panel_main_program'         => 'Program',
      'bims_panel_main_trait'           => 'Trait',
      'bims_panel_main_location'        => 'Location',
      'bims_panel_main_cross'           => 'Cross',
      'bims_panel_main_accession'       => $accession,
      'bims_panel_main_marker'          => 'Marker',
      'bims_panel_main_haplotype_block' => 'Haplotype Block',
      'bims_panel_main_trial'           => 'Trial',
      'bims_panel_main_property'        => 'Property',

//    'bims_panel_main_tree_breed_line' => "$accession / Trial",
//    'bims_panel_main_data_summary'    => 'Data Summary',
    );

    // Hides 'Marker', 'Haplotype Block' if data do not exist.
    if (!$bims_program->hasData('BIMS_MVIEW', 'marker')) {
      unset($lists['bims_panel_main_marker']);
    }

    if ($bims_user->getName() == 'ltaein') {
      if (!$bims_program->hasData('BIMS_MVIEW', 'haplotype_block')) {
        unset($lists['bims_panel_main_haplotype_block']);
      }
    }

    if ($bims_user->getName() != 'ltaein') {
      unset($lists['bims_panel_main_property']);
    }
    $items = array();
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";
    foreach ($lists as $name => $label) {
      $items[] = $prefix . "('$name')\">$label</a>";
    }
    return  theme('item_list', array('items' => $items));
  }

  /**
   * SIDEBAR : Data Import.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarDataImport(BIMS_USER $bims_user) {

    // Checks for errors.
    $error = BIMS_PANEL::preCheckSidebar($bims_user, TRUE);
    if (!empty($error)) {
      return $error['items']['#markup'];
    }

    // Gets BIMS_PROGRAM.
    $bims_program = $bims_user->getProgram();

    // Sets the prefix.
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";
    $markup = '';

    // Adds upload data section.
    if (!$bims_program->isPublicChado()) {
      $markup .= '<div><em>Excel Data Templates</em></div>';
      $items = array();
      $items[] = $prefix . "('bims_panel_main_di_template_list')\">Template list</a>";
      $items[] = $prefix . "('bims_panel_main_di_file')\">Upload data</a>";
      $markup .=  theme('item_list', array('items' => $items));
    }

    // Adds import public data section.
    if ($bims_user->canEdit()) {
      $markup .= '<div><em>Public Data</em></div>';
      $items = array();
      $items[] = $prefix . "('bims_panel_main_di_trait')\">Traits</a>";
      $items[] = $prefix . "('bims_panel_main_di_phenotype')\">Phenotypic data</a>";
      $items[] = $prefix . "('bims_panel_main_di_genotype')\">Genotypic data</a>";
      $markup .=  theme('item_list', array('items' => $items));
    }

    // Adds lookup data section.
    if ($bims_user->canEdit()) {
      $markup .= '<div><em>Data Lookup</em></div>';
      $items = array();
      $items[] = $prefix . "('bims_panel_main_di_lookup/organism')\">Organisms</a>";
      $items[] = $prefix . "('bims_panel_main_di_lookup/contact')\">Contacts</a>";
      $markup .=  theme('item_list', array('items' => $items));
    }

    // Adds photo file section.
    if (FALSE) {
      $markup .= '<div><em>Photo files</em></div>';
      $items = array();
      $items[] = $prefix . "('bims_panel_main_di_image')\">Upload photo files</a>";
      $markup .=  theme('item_list', array('items' => $items));
    }
    return $markup;
  }

  /**
   * SIDEBAR : Search.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarSearch(BIMS_USER $bims_user) {

    $flag_debug = FALSE;
    if ($bims_user->getName() == 'ltaein') {
      $flag_debug = TRUE;
    }

    // Checks for errors.
    $error = BIMS_PANEL::preCheckSidebar($bims_user);
    if (!empty($error)) {
      return $error['items']['#markup'];
    }

    // Gets BIMS_PROGRAM and the accession label.
    $bims_program = $bims_user->getProgram();
    $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

    // List of links for Search.
    $lists = array();
    $lists['bims_panel_main_sa_phenotype'] = array(
      'label' => 'Phenotype',
      'type' => 'PHENOTYPE'
    );
    $lists['bims_panel_main_s_cross'] = array(
      'label' => 'Cross',
      'type'  => 'CROSS'
    );
    $lists['bims_panel_main_sagm_genotype'] = array(
      'label' => 'SNP Genotype',
      'type'  => 'GENOTYPE'
    );
    $lists['bims_panel_main_sa_ssr'] = array(
      'label' => 'SSR Genotype',
      'type'  => 'GENOTYPE_SSR'
    );
    $lists['bims_panel_main_sa_haplotype'] = array(
      'label' => 'Haplotype',
      'type'  => 'HAPLOTYPE'
    );

    // Generates the links.
    $items = array();
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";
    foreach ($lists as $name => $prop) {
      $label  = $prop['label'];
      $type   = $prop['type'];

      // Checks if the data exists.
      $flag_data = $bims_program->hasData('BIMS_MVIEW', $type) ? TRUE :FALSE;

      // Sets the link.
      if ($name == 'bims_panel_main_sa_phenotype') {
        $items[] = $flag_data ? $prefix . "('$name')\">$label</a>" : $label;
      }
      else if ($name == 'bims_panel_main_s_cross') {
        $items[] = $flag_data ? $prefix . "('$name')\">$label</a>" : $label;
      }
      else if (preg_match("/bims_panel_main_sag(m|a)_genotype/", $name)) {
        $sub_itmes = array();
        if ($flag_data) {
          $sub_itmes = array(
            $prefix . "('bims_panel_main_saga_genotype')\">By $accession Properties</a>",
            $prefix . "('bims_panel_main_sagm_genotype')\">By Marker Properties</a>",
          );
        }
        $items[] = $label . '<br />' . theme('item_list', array('items' => $sub_itmes));
      }
      else if ($name == 'bims_panel_main_sa_ssr') {
        $sub_itmes = array();
        if ($flag_data && !$flag_debug) {
          $sub_itmes = array(
            $prefix . "('bims_panel_main_sa_ssr_am')\">By $accession / Marker</a>",
            $prefix . "('bims_panel_main_sa_ssr_a')\">By Allele</a>",
          );
          $items[] = $label . '<br />' . theme('item_list', array('items' => $sub_itmes));
        }
        else {
          $items[] = $label;
        }
      }
      else if ($name == 'bims_panel_main_sa_haplotype') {
        $items[] = $flag_data ? $label : $label;
      }
      else {
        $items[] = $label;
      }
    }
    $markup = theme('item_list', array('items' => $items));

    // List of links for Search Results.
    if (!$bims_user->isAnonymous()) {
      $lists = array(
        'bims_panel_main_s_result' => 'Saved Searches',
      );
      $items = array();
      foreach ($lists as $name => $label) {
        $items[] = $prefix . "('$name')\">$label</a>";
      }
      $markup .= '<div><em><b>Search Results</b></em></div>' . theme('item_list', array('items' => $items));
    }
    return $markup;
  }

  /**
   * SIDEBAR : Field Book Management.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarFieldbook(BIMS_USER $bims_user) {

    // Checks for errors.
    $error = BIMS_PANEL::preCheckSidebar($bims_user);
    if (!empty($error)) {
      return $error['items']['#markup'];
    }

    // Configuration page.
    $markup = '<div><em><a href="https://excellenceinbreeding.org/toolbox/tools/field-book" target="_blank">Field Book</a></em></div>';
    $items = array();
    $items[] = '<div><a href="https://excellenceinbreeding.org/sites/default/files/manual/field_book_manual_v3.pdf" target="_blank">Manual</a></div>';
    $markup .=  theme('item_list', array('items' => $items));

    $markup .= '<div><em>Configure Field Book</em></div>';
    $items = array();
    $items[] = '<a href="javascript:void(0);" title="Open configuration panel" onclick="bims.add_main_panel(\'bims_panel_add_on_config\', \'Field Book Conf\', \'bims/load_main_panel/bims_panel_add_on_config/field_book\'); ; return (false);">Configuration</a>';
    $markup .=  theme('item_list', array('items' => $items));

    // Sets the prefix.
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";

    // Input files.
    $markup .= '<div><em>Generate input file</em></div>';
    $lists = array(
      'bims_panel_main_fb_trait' => 'Trait',
      'bims_panel_main_fb_field' => 'Field',
    );
    $items = array();
    foreach ($lists as $name => $label) {
      $items[] = $prefix . "('$name')\">$label</a>";
    }
    $markup .= theme('item_list', array('items' => $items));

    // Output files.
    $markup .= '<div><em>Upload exported file</em></div>';
    $items = array();
    $items[] = $prefix . "('bims_panel_main_fb_upload_trait')\">Upload trait file</a>";
    $items[] = $prefix . "('bims_panel_main_fb_upload_field')\">Upload field file</a>";
    $markup .=  theme('item_list', array('items' => $items));
    $markup .= '<div style="margin-top:15px;margin-left:4px;font-style:italic;">' .
        '* The format of an exported field<br />&nbsp;&nbsp;file can be table or database</em></div>';
    return $markup;
  }

  /**
   * SIDEBAR : Data Analysis Tools.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarDataAnalysis(BIMS_USER $bims_user) {

    // Checks for errors.
    $error = BIMS_PANEL::preCheckSidebar($bims_user);
    if (!empty($error)) {
      return $error['items']['#markup'];
    }

    // List of links for Data Analysis Tools.
    $markup = '<div><em>Phenotypic Data</em></div>';
    $lists = array(
      'bims_panel_main_da_statistical_analysis' => 'Statistical Analysis',
    );
    $items = array();
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";
    foreach ($lists as $name => $label) {
      $items[] = $prefix . "('$name')\">$label</a>";
    }
    $markup .= theme('item_list', array('items' => $items));
    return $markup;
  }

  /**
   * SIDEBAR : Breeding Tools.
   *
   * @param BIMS_USER $bims_user
   *
   * @return string
   */
  private function _getSidebarBreedingTools(BIMS_USER $bims_user) {

    // Checks for errors.
    $error = BIMS_PANEL::preCheckSidebar($bims_user);
    if (!empty($error)) {
      return $error['items']['#markup'];
    }

    $markup = '';

if ($bims_user->getProgram()->getOwnerName() == 'craig' || $bims_user->getName() == 'ltaein') {

    // List of the Breeding Predictoin Tools.
    $markup = '<div><em>Breeding Prediction Tools</em></div>';
    $lists = array(
      'bims_panel_main_tool_gpt'  => 'Global Prediction Tool',
    );
   // if ($bims_user->isBIMSAdmin()) {
      $lists['bims_panel_main_tool_gpt_config'] = 'GPT - Configuration';
  //  }



    $items = array();
    if ($bims_user->getCrop()->getName() == 'peach' || $bims_user->isBIMSAdmin()) {
      $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";
      foreach ($lists as $name => $label) {
        $items[] = $prefix . "('$name')\">$label</a>";
      }
    }
    else {
      $items[] = "<span>Global Prediction Tool<br /><em>(Currently available for peach)</em></span>";
    }
    $markup .= theme('item_list', array('items' => $items));
}


    // List of Breeding Decision Tools.
    $markup .= '<div style="margin-top:3px;"><em>Breeding Decision Tools</em></div>';
    $lists = array(
      'bims_panel_main_tool_ca'  => 'Cross Assist',
      'bims_panel_main_tool_ss'  => 'Seedling Select',
    );
    $items = array();
    $prefix = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel";
    foreach ($lists as $name => $label) {
      $items[] = $prefix . "('$name')\">$label</a>";
    }
    $markup .= theme('item_list', array('items' => $items));
    return $markup;
  }
}