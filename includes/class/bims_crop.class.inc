<?php
/**
 * The declaration of BIMS_CROP class.
 *
 */
class BIMS_CROP extends PUBLIC_BIMS_CROP {

  /**
   * Class data members.
   */
  protected $prop_arr = NULL;

  /**
   * @see PUBLIC_BIMS_CROP::__construct()
   */
  public function __construct($details = array()) {
    parent::__construct($details);

    // Updates property array ($this->prop_arr).
    if ($this->prop == '') {
      $this->prop_arr = array();
    }
    else {
      $this->prop_arr = json_decode($this->prop, TRUE);
    }
  }

  /**
   * @see PUBLIC_BIMS_CROP::byKey()
   */
  public static function byKey($keys) {
    $parent = parent::byKey($keys);
    if ($parent) {
       return new self($parent->getMemberArr());
    }
    return NULL;
  }

  /**
   * Returns BIMS_CROP by crop ID.
   *
   * @param integer $crop_id
   *
   * @return BIMS_CROP|NULL
   */
  public static function byID($crop_id) {
    if (!bims_is_int($crop_id)) {
      return NULL;
    }
    return self::byKey(array('crop_id' => $crop_id));
  }

  /**
   * Returns BIMS_CROP by crop name.
   *
   * @param string $name
   *
   * @return BIMS_CROP|NULL
   */
  public static function byName($name) {
    return self::byKey(array('name' => $name));
  }

  /**
   * @see PUBLIC_BIMS_CROP::insert()
   */
  public function insert() {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Insert a new user.
    return parent::insert();
  }

  /**
   * @see PUBLIC_BIMS_CROP::update()
   */
  public function update($new_values = array()) {

    // Updates the parent:$prop field.
    $this->prop = json_encode($this->prop_arr);

    // Updates the user properties.
    return parent::update($new_values);
  }

  /**
   * @see PUBLIC_BIMS_CROP::__destruct()
   */
  public function __destruct() {
    parent::__destruct();
  }

  /**
   * Cleans the label.
   *
   * @param string $label
   *
   * @return string
   */
  public static function cleanLabel($label) {
    return strtolower(str_replace(' ', '_', $label));
  }

  /**
   * Returns the all associated data.
   *
   * @return array
   */
  public function getAssocData() {
    return array();
  }

  /**
   * Add a crop.
   *
   * @param string $label
   * @param integer $ploidy
   *
   * @return BIMS_CROP
   */
  public static function addCrop($label, $ploidy = BIMS_DEF_PLOIDY, $cv_id = '') {

    $transaction = db_transaction();
    try {
      $name = BIMS_CROP::cleanLabel($label);

      // Adds a crop.
      $bims_crop = new BIMS_CROP(array(
        'name'    => $name,
        'label'   => $label,
        'ploidy'  => $ploidy,
      ));
      if (!$bims_crop->insert()) {
        throw new Exception("Fail to add a crop ($label)");
      }

      // Sets the public trait descriptor set.
      if ($cv_id) {
        $bims_crop->setTraitDescriptorSet($cv_id, TRUE);
      }

      // Adds a dictonary for the crop.
      /*
      if ($bims_crop->addDict()) {
        throw new Exception("Fail to add a dictionary table for the crop ($label)");
      }*/
      return $bims_crop;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
    return NULL;
  }

  /**
   * Returns the list of organism of the current crop.
   *
   * @param integer $flag
   *
   * @return array
   */
  public function getOrganisms($flag = BIMS_OPTION) {
    $sql = "
      SELECT CP.*, O.genus, O.species
      FROM {bims_crop_organism} CP
        INNER JOIN {chado.organism} O on O.organism_id = CP.organism_id
      WHERE CP.crop_id = :crop_id
      ORDER BY O.genus, O.species
    ";
    $organisms = array();
    $results = db_query($sql, array('crop_id' => $this->crop_id));
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_OPTION) {
        $organisms[$obj->organism_id] = $obj->genus . ' ' . $obj->species;
      }
      else if ($flag == BIMS_OBJECT) {
        $organisms []= $obj;
      }
      else {
        $organisms []= $obj->organism_id;
      }
    }
    return $organisms;
  }

  /**
   * Returns chado organism.
   *
   * @return array
   */
  public function getChadoOrganisms() {

    // Gets organisms.
    $sql = "
      SELECT O.genus, O.species, O.organism_id
      FROM {chado.organism} O
      WHERE O.organism_id NOT IN (
        SELECT BCO.organism_id
        FROM {bims_crop_organism} BCO
        WHERE BCO.crop_id = :crop_id
      )
      ORDER BY O.genus, O.species
    ";
    $args = array(':crop_id' => $this->crop_id);
    $results = db_query($sql, $args);
    $organisms = array();
    while ($obj = $results->fetchObject()) {
      $organisms[$obj->organism_id] = $obj->genus . ' ' . $obj->species;
    }
    return $organisms;
  }

  /**
   * Returns the cv ID of the public trait descriptor set.
   *
   * @return integer
   */
  public function getTraitDescriptorSet() {
    return $this->getPropByKey('trait_descriptor_set');
  }

  /**
   * Sets the public trait descriptor set.
   *
   * @param integer $cv_id
   * @param boolean $update
   *
   * @return boolean
   */
  public function setTraitDescriptorSet($cv_id, $update = FALSE) {
    $this->setPropByKey('trait_descriptor_set', $cv_id);
    if ($update) {
      return $this->update();
    }
    return TRUE;
  }

  /**
   * Returns the number of the crops.
   *
   * @return integer number of crops.
   */
  public static function getNumCrops() {
    $sql = "SELECT COUNT(crop_id) FROM {bims_crop}";
    return db_query($sql)->fetchField();
  }


  /**
   * Returns the number of the programs for this crop.
   *
   * @param string $type
   *
   * @return integer number of programs.
   */
  public function getNumPrograms($type = '') {
    $sql = "
      SELECT COUNT(node_id)
      FROM {bims_node}
      WHERE crop_id = :crop_id AND type = :type
    ";
    $args = array(
      ':crop_id'  => $this->crop_id,
      ':type'     => 'PROGRAM',
    );
    if ($type == 'public') {
      $access = sprintf("%d,%d", BIMS_ACCESS_PUBLIC, BIMS_ACCESS_PUBLIC_CHADO);
      $sql .= " AND access IN ($access) ";
    }
    else if ($type == 'private') {
      $sql .= " AND access = :access";
      $args[':access'] = BIMS_ACCESS_PRIVATE;
    }
    return db_query($sql, $args)->fetchField();
  }

  /**
   * Returns all crops.
   *
   * @param integer $flag
   *
   * @return array.
   */
  public static function getCrops($flag = BIMS_OPTION) {

    // Gets all crops.
    $sql = "SELECT C.* FROM {bims_crop} C ORDER BY C.name";
    $results = db_query($sql);
    $crops = array();
    while ($obj = $results->fetchObject()) {
      if ($flag == BIMS_CLASS) {
        $crops []= BIMS_CROP::byID($obj->crop_id);
      }
      else if ($flag == BIMS_OBJECT) {
        $crops []= $obj;
      }
      else {
        $crops[$obj->crop_id] = $obj->label;
      }
    }
    return $crops;
  }

  /**
   * Adds a dictonary.
   *
   * @param boolean $recreate
   *
   * @return boolean
   */
  public function addDict($recreate = FALSE) {

    $transaction = db_transaction();
    try {

      // Gets the name of the dictionary table.
      $table_name = sprintf("bims.dict_%s", $this->name);

      // Drops the table if exists and re-create is TURE.
      if ($recreate && db_table_exists($table_name)) {
        db_drop_table($table_name);
      }

      // Adds the dictionary table in BIMS schema.
      if (!db_table_exists($table_name)) {
        db_create_table($table_name, $this->getDictSchema());
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      watchdog('BIMS', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Returns the schema for the dictionary table.
   *
   * @return array
   */
  private function getDictSchema() {
    return array(
      'fields' => array(
        'GID' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'pid_ID' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'id.Plain' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'id.Original' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'raw.Plain' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'id.Raw' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'data_type' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'contrib' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'conf' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
        'file_name' => array(
          'type' => 'varchar',
          'length' => '255',
          'not null' => TRUE,
        ),
      ),
      'unique keys' => array(
        'ukey_001' => array('GID')
      ),
    );
  }

  /**
   * Adds a new organism.
   *
   * @return boolean
   */
  public function addOrganism($organism_id) {
    $crop_organism = new PUBLIC_BIMS_CROP_ORGANISM(array(
      'crop_id'     => $this->crop_id,
      'organism_id' => $organism_id,
    ));
    return $crop_organism->insert();
  }

  /**
   * Removes the organism.
   *
   * @return boolean
   */
  public function removeOrganism($organism_id) {
    $keys = array(
      'crop_id'     => $this->crop_id,
      'organism_id' => $organism_id,
    );
    $crop_organism = PUBLIC_BIMS_CROP_ORGANISM::byKey($keys);
    return $crop_organism->delete();
  }

  /*
   * Defines getters and setters below.
   */
   /**
   * Returns the value of the given key in prop.
   */
  public function getPropByKey($key) {
    if (array_key_exists($key, $this->prop_arr)) {
      return $this->prop_arr[$key];
    }
    return NULL;
  }

  /**
   * Sets the value of the given key in prop.
   */
  public function setPropByKey($key, $value) {
    $this->prop_arr[$key] = $value;
  }
}