<?php
/**
 * @file
 */
/**
 * Marker match panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_marker_match_form($form, &$form_state) {

  // Gets BIMS_USER, BIMS_PROGRAM and BIMS_CROP.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $bims_crop    = $bims_user->getCrop();
  $program_id   = $bims_program->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_marker_match');
  $bims_panel->init($form);

  // Property for an marker / progreny.
  /*
  $form['property'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Add a new property",
    '#collapsed'   => TRUE,
    '#collapsible' => TRUE,
  );
  $form['property']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Property Name'),
    '#description'  => t("Please provide an name of a property."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['property']['desc'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please provide a description of a property."),
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['property']['add_property_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_property_btn',
    '#value'      => "Add Property",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_marker_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-marker-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Properties of an marker.
  $form['marker_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Add a new marker',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['marker_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => 'Marker',
    '#description'  => t("Please provide a marker. Please avoid using any special characters excepts dash, slash and underscore"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['marker_prop']['organism_id'] = array(
    '#type'         => 'select',
    '#title'        => t('Organism'),
    '#options'      => $bims_crop->getOrganisms(BIMS_OPTION),
    '#description'  => t("Please choose an organism of the marker"),
    '#attributes' => array('style' => 'width:250px;'),
  );
  $form['marker_prop']['add_marker_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_marker_btn',
    '#value'      => "Add Marker",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_marker_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-marker-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['marker_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_marker_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-marker-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );*/

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-marker-match-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_marker_match_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_marker_match_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_marker_match_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add Property" was clicked.
  /*
  if ($trigger_elem == 'add_property_btn') {

    // Gets the name of the property.
    $name = trim($form_state['values']['property']['name']);

    // Checks the marker name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-\/\\][a-zA-z0-9_\-\/\\ ]*$/", $name)) {
      form_set_error('property][name', "Invalid property name. Please use alpha-numeric, dash and underscore.");
      return;
    }

    // Checks for duplication.
    if ($bims_program->checkProperty('marker', $name)) {
      form_set_error('property][name', "Propey nanme ($name) has aleady existed.");
      return;
    }
  }

  // If "Add Marker" was clicked.
  else if ($trigger_elem == 'add_marker_btn') {

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO table.
    $bc_feature = new BIMS_CHADO_FEATURE($program_id);

    // Gets the properties of the marker.
    $name         = trim($form_state['values']['marker_prop']['name']);
    $organism_id  = $form_state['values']['marker_prop']['organism_id'];

    // Checks the marker name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-]+$/", $name)) {
      form_set_error('marker_prop][name', "Invalid marker. Please use alpha-numeric, dash and underscore.");
      return;
    }

    // Checks the marker name for duplication.
    $uniquename = $prefix . $name;
    $marker = $bc_feature->byTKey('feature', array('uniquename' => $uniquename, 'organism_id' => $organism_id));
    if ($marker) {
      form_set_error('marker_prop][name', "The marker ($name, $organism_id) has been taken. Please choose other name.");
    }
  }*/
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_marker_match_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_marker';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add Property" was clicked.
  /*
  if ($trigger_elem == 'add_property_btn') {

    // Gets BIMS_USER and BIMS_PROGRAM.
    $bims_user    = getBIMS_USER();
    $bims_program = $bims_user->getProgram();

    // Gets the name of the property.
    $name = trim($form_state['values']['property']['name']);
    $desc = trim($form_state['values']['property']['desc']);
    $type = $form_state['values']['property']['type'];

    $transaction = db_transaction();
    try {

      // Adds the cvterm.
      $cv = $bims_program->getCv($type);
      if (MCL_CHADO_CVTERM::addCvterm(NULL, 'SITE_DB', $cv->getName(), $name, $desc)) {
        drupal_set_message("New property has been added");
      }
      else {
        throw new Exception("Error : Failed to add a new property");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // If "Add Marker" was clicked.
  else if ($trigger_elem == 'add_marker_btn') {

    $transaction = db_transaction();
    try {

      // Adds a new marker.
      $feature_id = _bims_panel_add_on_marker_add_marker($form_state);
      if (!$feature_id) {
        throw new Exception("Error : Failed to add a new marker");
      }
      $load_primary_panel = "$load_primary_panel/$feature_id";
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }
*/
  // Updates the panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_marker_match')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}