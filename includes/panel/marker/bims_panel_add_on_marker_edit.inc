<?php
/**
 * @file
 */
/**
 * Marker edit panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param intger $feature_id
 */
function bims_panel_add_on_marker_edit_form($form, &$form_state, $feature_id) {

  // Gets BIMS_USER and BIMS_CROP.
  $bims_user    = getBIMS_USER();
  $bims_crop    = $bims_user->getCrop();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets BIMS_MVIEW_MARKER.
  $bm_marker  = new BIMS_MVIEW_MARKER(array('node_id' => $bims_program->getProgramID()));
  $marker_obj = $bm_marker->getMarker($feature_id, BIMS_OBJECT);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves the marker object.
  $form['marker_obj'] = array(
    '#type'   => 'value',
    '#value'  => $marker_obj,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_marker_edit');
  $bims_panel->init($form);

  // Properties of the marker.
  $form['marker_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Marker Properties",
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['marker_prop']['name'] = array(
    '#type'           => 'textfield',
    '#title'          => 'Marker',
    '#description'    => t("Please provide a name of a marker. Please avoid using any special characters excepts dash, slash and underscore"),
    '#default_value'  => $marker_obj->name,
    '#attributes'     => array('style' => 'width:400px;'),
  );

  // Gets the alias.
  $mview_alias = new BIMS_MVIEW_ALIAS(array('node_id' => $program_id));
  $aliases = $mview_alias->getAlias('feature', $marker_obj->feature_id, BIMS_STRING, ' / ');
  $alias = ($aliases) ? $aliases : '';
  $form['marker_prop']['alias'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Alias'),
    '#description'    => t("Please provide aliases"),
    '#default_value'  => $alias,
    '#description'    => t("Please provide aliases. Please separates by columns for multple aliases."),
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $opt_organism = $bims_crop->getOrganisms(BIMS_OPTION);
  if (!array_key_exists($marker_obj->organism_id, $opt_organism)) {
    $organism = MCL_CHADO_ORGANISM::byID($marker_obj->organism_id);
    $opt_organism[$marker_obj->organism_id] = $organism->getGenus() . ' ' . $organism->getSpecies();
  }
  $form['marker_prop']['organism_id'] = array(
    '#type'           => 'select',
    '#title'          => t('Organism'),
    '#options'        => $opt_organism,
    '#default_value'  => $marker_obj->organism_id,
    '#description'    => t("Please choose an organism of the marker"),
    '#attributes'     => array('style' => 'width:250px;'),
  );
  $form['marker_prop']['edit_marker_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'edit_marker_btn',
    '#value'      => 'Update',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_marker_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-marker-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['marker_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_marker_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-marker-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-marker-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_marker_edit_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_marker_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_marker_edit_form_validate($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_marker_btn') {

    // Gets the value from the form.
    $marker_obj   = $form_state['values']['marker_obj'];
    $name         = trim($form_state['values']['marker_prop']['name']);
    $organism_id  = $form_state['values']['marker_prop']['organism_id'];

    // Checks marker name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-\/\\][a-zA-z0-9_\-\/\\ ]*$/", $name)) {
      form_set_error('marker_prop][name', "Invalid marker name");
      return;
    }

    // Gets BIMS_CHADO_FEATURE.
    $bc_feature= new BIMS_CHADO_FEATURE($program_id);

    // Checks the marker name for duplication.
    if ($marker_obj->name != $name) {
      $marker = $bc_feature->getFeatureByName($name);
      if ($marker) {
        form_set_error('marker_prop][name', "The name ($name) has been taken. Please choose other name.");
      }
    }
  }
}

/**
 * Updates the marker.
 *
 * @param array $form_state
 * @param object $marker_obj
 *
 * @return boolean
 */
function _bims_panel_add_on_marker_edit_form_edit_marker($form_state, $marker_obj) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $feature_id   = $marker_obj->feature_id;

  // Gets the properties of the marker.
  $name         = trim($form_state['values']['marker_prop']['name']);
  $alias        = trim($form_state['values']['marker_prop']['alias']);
  $organism_id  = $form_state['values']['marker_prop']['organism_id'];

  // Gets BIMS_CHADO_FEATURE.
  $bc_feature = new BIMS_CHADO_FEATURE($program_id);
  $bc_feature->setFeatureID($feature_id);

  // Prepares for updating the marker.
  $organism = MCL_CHADO_ORGANISM::byID($organism_id);

  // Adds the properties.
  $details = array(
    'feature_id'  => $feature_id,
    'uniquename'  => $bims_program->getPrefix() . $name,
    'name'        => $name,
    'organism_id' => $organism_id,
    'genus'       => $organism->getGenus(),
    'species'     => $organism->getSpecies(),
    'type_id'     => $marker_obj->type_id,
    'alias'       => $alias,
  );

  // Updates the marker in BIMS.
  if (!$bc_feature->updateFeature($details)) {
    drupal_set_message("Error : Failed to update the BIMS_CHADO_FEATURE", 'error');
    return FALSE;
  }

  // Updates the marker in the mview.
  $bm_marker = new BIMS_MVIEW_MARKER(array('node_id' => $program_id));
  if (!$bm_marker->updateMView(FALSE, 'feature', array($feature_id), TRUE)) {
    drupal_set_message("Error : Failed to update the mviews", 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_marker_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_marker';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the marker object.
  $marker_obj = $form_state['values']['marker_obj'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_marker_btn') {

    $transaction = db_transaction();
    try {

      // Edits the marker.
      if (!_bims_panel_add_on_marker_edit_form_edit_marker($form_state, $marker_obj)) {
        throw new Exception("Error : Failed to update the marker");
      }
      $load_primary_panel .= '/' . $marker_obj->feature_id;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Removes the panel.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_marker_edit')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}