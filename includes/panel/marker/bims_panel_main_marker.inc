<?php
/**
 * Marker panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_marker_form($form, &$form_state, $feature_id = NULL, $filter_list = '') {

  // Local variables for form properties.
  $min_height = 380;

  // Gets BIMS_MVIEW_MARKER and BIMS_MVIEW_IMAGE.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $bm_marker    = new BIMS_MVIEW_MARKER(array('node_id' => $program_id));
  $bm_image     = new BIMS_MVIEW_IMAGE(array('node_id' => $program_id));

  // Gets the marker object.
  $marker_obj = NULL;
  if (isset($form_state['values']['marker_list']['selection'])) {
    $feature_id = $form_state['values']['marker_list']['selection'];
  }
  if ($feature_id) {
    $marker_obj = $bm_marker->getMarker($feature_id, BIMS_OBJECT);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_marker');
  $bims_panel->init($form);

  // Adds the admin menu.
  $form['marker_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Marker',
    '#attributes'   => array(),
  );

  // Adds the "Add" button.
  $url = "bims/load_main_panel/bims_panel_add_on_marker_add";
  $form['marker_admin']['menu']['add_btn'] = array(
    '#name'       => 'add_btn',
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_marker_add', 'Add Marker', '$url'); return (false);",
    ),
  );
  $form['marker_admin']['menu']['#theme'] = 'bims_panel_main_marker_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['marker_admin']);

  // Lists the all marker.
  $form['marker_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Marker',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  _bims_panel_main_marker_form_add_marker_list($form['marker_list'], $bims_program, $bm_marker, $feature_id, $filter_list);
  $form['marker_list']['#theme'] = 'bims_panel_main_marker_form_marker_list';

  // Marker details.
  $form['marker_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => "Marker Details",
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['marker_details']['table']['marker_obj'] = array(
    '#type'   => 'value',
    '#value'  => $marker_obj,
  );
  $form['marker_details']['table']['num_image'] = array(
    '#type'   => 'value',
    '#value'  => 0,
  );
  $form['marker_details']['table']['program_id'] = array(
    '#type'   => 'value',
    '#value'  => $program_id,
  );

  // Adds action buttons.
  if ($marker_obj && bims_can_admin_action($bims_user)) {

    // Adds 'Edit' button.
    if (!$marker_obj->chado) {
      $form['marker_details']['table']['edit_btn'] = array(
        '#name'       => 'edit_btn',
        '#type'       => 'button',
        '#value'      => 'Edit',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_marker_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-marker-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds 'Delete' button.
    $form['marker_details']['table']['delete_btn'] = array(
      '#name'       => 'delete_btn',
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#disabled'   => !$bims_user->canEdit(),
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_marker_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-marker-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  if ($marker_obj) {
    $form['marker_details']['table']['prop'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Properties',
      '#attributes'   => array('style' => 'width:90%;'),
    );
    $form['marker_details']['table']['prop']['table'] = array(
      '#markup' => '<em>No propery found</em>',
    );
  }
  $form['marker_details']['table']['#theme'] = 'bims_panel_main_marker_form_marker_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-marker-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_marker_form_submit';
  return $form;
}

/**
 * Adds the list of markers.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_MVIEW_MARKER $bm_marker
 * @param integer $feature_id
 * @param string $filter_str
 */
function _bims_panel_main_marker_form_add_marker_list(&$form, BIMS_PROGRAM $bims_program, BIMS_MVIEW_MARKER $bm_marker, $feature_id, $filter_str) {

  // Checks the marker data in the mview.
  if (!$bm_marker->hasData()) {
    $form['no_marker'] = array(
      '#markup' => "No marker is associated with the current program.",
    );
    return;
  }

  // Adds the filter options.
  /*
  $form['filter'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Filter',
    '#attributes'   => array('style' => 'width:100%;'),
  );

  // Sets the flter list.
  $filter_list = array();
  if ($filter_str) {
    $filter_list = explode(':', $filter_str);
  }

  // Adds the filter options if needs.
  if ($bims_program->needFilter('marker')) {
    $filter_opts = $bims_program->getFilterOpts('marker');

    // Sets the default marker group.
    if (empty($filter_list)) {
      $filter_list []= bims_get_first_elem($filter_opts, BIMS_ARRAY_KEY);
    }

    // Adds the filter options.
    $form['filter']['filter_list'] = array(
      '#title'          => 'Marker Groups',
      '#type'           => 'checkboxes',
      '#options'        => $filter_opts,
      '#multiple'       => TRUE,
      '#description'    => 'Please choose marker group(s)',
      '#default_value'  => $filter_list,
      '#prefix'         => '<div style="padding:5px;border:1px solid lightgray;">',
      '#attributes'     => array('style' => 'backgroud-color:#EEEEEE;'),
    );
    $form['filter']['update_btn'] = array(
      '#type'       => 'submit',
      '#value'      => 'Update',
      '#name'       => 'update_btn',
      '#attributes' => array('style' => 'width:180px;'),
      '#suffix'     => '</div>',
      '#ajax'       => array(
        'callback' => "bims_panel_main_marker_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-marker-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  */

  // Gets the filtered markers.
  $filter_list  = array();
  $opt_markes   = $bims_program->getFilteredData('marker', $filter_list);
  $num_markers  = sizeof($opt_markes);

  // No marker found after filterd.
  if ($num_markers == 0) {
    $form['no_filtered_marker'] = array(
      '#markup' => "No marker found with the filtered conditions.",
    );
    return;
  }

  // Adds 'Find' textfield.
  if ($num_markers > BIMS_DDL_MAX_LIST) {
    $program_id = $bims_program->getProgramID();
    $form['marker_auto'] = array(
      '#type'               => 'textfield',
      '#attributes'         => array('style' => 'width:250px;'),
      '#autocomplete_path'  => "bims/bims_autocomplete_filtered_marker/$program_id/$filter_str",
    );
    $form['marker_details_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'marker_details_btn',
      '#value'      => 'Details',
      '#attributes' => array('style' => 'width:100px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_marker_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-marker-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds the marker list.
  if ($num_markers > BIMS_DDL_MAX_LIST) {
    $msg = "You have <b>$num_markers</b> markers.<br />" .
      "The maximum  number of markers listed in a dropdown list is <b>" . BIMS_DDL_MAX_LIST . "</b>.<br />
      Please use the auto-complete textbox above to find marker.";
    $form['selection'] = array(
      '#markup' => "<div>$msg</div>",
    );
  }
  else {
    $form['selection'] = array(
      '#type'           => 'select',
      '#options'        => $opt_markes,
      '#size'           => 14,
      '#default_value'  => $feature_id,
      '#prefix'         => "You have <b>$num_markers</b> markers",
      '#description'    => "Please choose a marker to view the details.",
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_marker_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-marker-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_marker_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Update" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets the chosen descriptor groups.
    $filter_list = $form_state['values']['marker_list']['filter']['filter_list'];
    $filter_arr = array();
    foreach ((array)$filter_list as $filter => $bool) {
      if ($bool) {
        $filter_arr []= $filter;
      }
    }

    // Updates panels.
    $filter_str = implode(':', $filter_arr);
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_marker//$filter_str")));
  }
  else {

    // Gets feature_id from the dropdownlist.
    $feature_id = $form_state['values']['marker_list']['selection'];

    // Gets the feature_id from the form.
    if (!$feature_id) {
      $marker_obj = $form_state['values']['marker_details']['table']['marker_obj'];
      $feature_id = $marker_obj->feature_id;
    }
    if ($feature_id) {

      // If "Edit" was clicked.
      if ($trigger_elem == 'edit_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_marker_edit/$feature_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_marker_edit', "Edit Marker", $url)));
      }

      // If "Delete" was clicked.
      else if ($trigger_elem == 'delete_btn') {

        // Deletes the marker.
        if(_bims_panel_main_marker_form_delete_marker($program_id, $feature_id)) {
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_marker')));
        }
      }
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Deletes the marker.
 *
 * @param integer $program_id
 * @param integer $feature_id
 *
 * @return boolean
 */
function _bims_panel_main_marker_form_delete_marker($program_id, $feature_id) {

  $transaction = db_transaction();
  try {

    // Deletes the marker.
    $bc_feature = new BIMS_CHADO_FEATURE($program_id);
    if (!$bc_feature->deleteFeatures(array($feature_id))) {
      throw new Exception("Error : Failed to delete marker");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_marker_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_marker_form_submit($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Details" was clicked.
  if ($trigger_elem == 'marker_details_btn') {

    // Gets the marker.
    $name       = trim($form_state['values']['marker_list']['marker_auto']);
    $bm_marker  = new BIMS_MVIEW_MARKER(array('node_id' => $program_id));
    $feature_id = $bm_marker->getFeatureIDByName($name);
    if ($feature_id) {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_marker/$feature_id")));
    }
    else {
      drupal_set_message("Marker '$name' does not exists.");
    }
  }
}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_marker_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), "Add a new marker / property.");
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the marker list.
 *
 * @param $variables
 */
function theme_bims_panel_main_marker_form_marker_list($variables) {
  $element = $variables['element'];

  // Displays 'No marker' message.
  if (array_key_exists('no_marker', $element)) {
    return '<div>' . drupal_render($element['no_marker']) . '</div>';
  }

  // Adds the filter.
  $layout = "<div>" . drupal_render($element['filter']) . "</div>";

  // Displays 'No filtered marker' message.
  if (array_key_exists('no_filtered_marker', $element)) {
    $layout .= "<div>" . drupal_render($element['filter']) . "</div>";
  }
  else {

    // Adds autocompute.
    if (array_key_exists('marker_auto', $element)) {
      $layout .= '<div style="float:left;width:40px;margin-top:8px;"><em><b>Find</b></em></div>' .
      ' <div style="float:left;">' . drupal_render($element['marker_auto']) .
      '</div><div style="float:left;margin-left:10px;">' . drupal_render($element['marker_details_btn']) . '</div><br clear="all" />';
    }

    // Adds the marker list.
    $layout .= '<div>' . drupal_render($element['selection']) . '</div>';
  }
  return $layout;
}

/**
 * Theme function for the marker information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_marker_form_marker_table($variables) {
  $element = $variables['element'];

  // Gets marker and image objects.
  $program_id = $element['program_id']['#value'];
  $marker_obj = $element['marker_obj']['#value'];
  $num_image  = $element['num_image']['#value'];

  // Initializes the properties.
  $name       = '--';
  $alias      = '--';
  $genus      = '--';
  $species    = '';
  $image_row  = '--';
  $dp_row     = '--';
  $data_row   = '--';
  $loc_row    = '--';
  $ss_id      = '';
  $rs_id      = '';
  $comments   = '';
  $properties = '';
  $actions    = '';

  // Updates the properites.
  $data_point_arr = array();
  if ($marker_obj) {
    $name     = $marker_obj->name;
    $genus    = $marker_obj->genus;
    $species  = $marker_obj->species;

    // Updates the alias.
    $mview_alias = new BIMS_MVIEW_ALIAS(array('node_id' => $program_id));
    $aliases = $mview_alias->getAlias('feature', $marker_obj->feature_id, BIMS_STRING, ' / ');
    if ($aliases) {
      $alias = $aliases;
    }

    // Adds 'Image' link.
    $image_row = '';
    if ($num_image) {
      $url =  'bims/load_main_panel/bims_panel_add_on_marker_image/' . $marker_obj->feature_id;
      $image_row = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_marker_image\",\"Photos\",\"$url\"); return (false);'>[View $num_image photos]</em></a>";
    }

    // Adds 'View' buttons.
    $data_row = '';
    if (array_key_exists('view_btn', $element)) {
      $data_row .= drupal_render($element['view_btn']);
    }

    // Adds the properties.
    $rows = array();
    $bims_program = BIMS_PROGRAM::byID($marker_obj->node_id);
    $props = $bims_program->getProperties('marker', 'details', BIMS_FIELD);
    if (!empty($props)) {
      foreach ($props as $field => $cvterm_name) {
        if (property_exists($marker_obj, $field)) {
          $value = $marker_obj->{$field};
          if ($value) {
            $rows []= array($cvterm_name, $value);
          }
        }
      }
    }
    if (!empty($rows)) {
      $table_vars = array(
        'header'      => array(array('data' => 'name', 'width' => 100), 'value'),
        'rows'        => $rows,
        'attributes'  => array(),
      );
      $element['prop']['table']['#markup'] = theme('table', $table_vars);
      $properties = drupal_render($element['prop']);
    }

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }

    // Adds the comments.
    if ($marker_obj->comments) {
      $comments = '<textarea style="width:100%;" rows=2 READONLY>' . $marker_obj->comments . '</textarea>';
    }

    // Adds SS and RS ID.
    if ($marker_obj->ss_id) {
      $ss_id = $marker_obj->ss_id;
    }
    if ($marker_obj->rs_id) {
      $rs_id = $marker_obj->rs_id;
    }

    // Updates the location.
    $loc_row = '';
    if (($marker_obj->loc_start || $marker_obj->loc_start == '0') &&
        ($marker_obj->loc_stop || $marker_obj->loc_stop == '0')) {
      $genome = $marker_obj->genome;
      $chr    = $marker_obj->chromosome;
      $start  = $marker_obj->loc_start;
      $stop   = $marker_obj->loc_stop;
      $loc_row = "<textarea style='padding:3px;height:58px;width:100%;font-family: Monospace;font-size:10.5px;'>Genome     : $genome\nChormosome : $chr\nPositions  : $start - $stop</textarea>";
    }

    // Gets all data points.
    if ($marker_obj->num_ssr) {
      $data_point_arr []= 'SSR : ' . $marker_obj->num_ssr;
    }
    if ($marker_obj->num_snp) {
      $data_point_arr []= 'SNP : ' . $marker_obj->num_snp;
    }
  }

  // Updates the data point row.
  if (empty($data_point_arr)) {
    $dp_row = '<em>No associated data</em>';
  }
  else {
    $dp_row = '<textarea style="width:100%;" rows=2 READONLY>' . implode("\n", $data_point_arr) . '</textarea>';
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'width' => '100'), $name);
  $rows[] = array('Alias', $alias);
  if ($ss_id) {
    $tag = "<span title='The submitted SNPs (SS) number'>SS ID</span>";
    $rows[] = array($tag, $ss_id);
  }
  if ($rs_id) {
    $tag = "<span title='The refSNP (RS) number'>RS ID</span>";
    $rows[] = array($tag, $rs_id);
  }
  $rows[] = array('Organism', "$genus $species");
  $rows[] = array('# data points', $dp_row);
  if ($image_row) {
    $rows[] = array('Photos', $image_row);
  }
  if ($comments) {
    $rows[] = array('Comments', $comments);
  }
  if ($properties) {
    $rows[] = array('Properties', $properties);
  }
  if ($data_row) {
    $rows[] = array('Genotyic Data', $data_row);
  }
  if ($loc_row) {
    $rows[] = array('Locations', $loc_row);
  }
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_marker_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('marker_admin', $form)) {
    $layout .= "<div>" . drupal_render($form['marker_admin']) . "</div>";
  }

  // Adds "marker".
  $layout .= "<div style='float:left;width:44%;'>" . drupal_render($form['marker_list']) . "</div>";

  // Adds 'Marker Details'.
  $layout .= "<div style='float:left;width:55%;margin-left:5px;'>" . drupal_render($form['marker_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
