<?php
/**
 * Trait panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trait_form($form, &$form_state, $cvterm_id = NULL, $cv_ids = NULL) {

  // Local variables for form properties.
  $min_height = 330;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets BIMS_MVIEW_DESCRIPTOR.
  $mview_descriptor = NULL;
  if (isset($form_state['values']['trait_list']['selection'])) {
    $cvterm_id = $form_state['values']['trait_list']['selection'];
  }
  if (preg_match("/^\d+$/", $cvterm_id)) {
    $mview_descriptor = BIMS_MVIEW_DESCRIPTOR::byID($bims_program->getProgramID(), $cvterm_id);
  }

  // Gets the chosen cv IDs.
  $cv_list = array();
  if ($cv_ids) {
    $cv_list = explode(':', $cv_ids);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_trait');
  $bims_panel->init($form);

  // Adds the admin menu.
  $form['trait_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Trait',
    '#attributes'   => array('style' => ''),
  );

  // Adds admin menu.
  $url = "bims/load_main_panel/bims_panel_add_on_trait_add";
  $form['trait_admin']['menu']['add_btn'] = array(
    '#name'       => 'add_btn',
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style' => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_trait_add', 'Add Trait', '$url'); return (false);",
    ),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_trait_create";
  $form['trait_admin']['menu']['create_btn'] = array(
    '#name'       => 'create_btn',
    '#type'       => 'button',
    '#value'      => 'Create',
    '#attributes' => array(
      'style' => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_trait_create', 'Create Trait', '$url'); return (false);",
    ),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_config/trait_order";
  $form['trait_admin']['menu']['order_btn'] = array(
    '#name'       => 'order_btn',
    '#type'       => 'button',
    '#value'      => 'Order',
    '#attributes' => array(
      'style' => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_config', 'Trait Order Config...', '$url'); return (false);",
    ),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_trait_match";
  $form['trait_admin']['menu']['match_btn'] = array(
    '#name'       => 'match_btn',
    '#type'       => 'button',
    '#value'      => 'Match',
    '#disabled'   => !$bims_user->canEdit(),
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_trait_match', 'Match Traits', '$url'); return (false);",
    ),
  );
  $form['trait_admin']['menu']['#theme'] = 'bims_panel_main_trait_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['trait_admin']);

  // Adds the list of traits.
  $form['trait_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Traits'),
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  _bims_panel_main_trait_form_add_trait_list($form['trait_list'], $bims_program, $cvterm_id, $cv_list);

  // Trait details.
  $desc = "<div>Click <a href='javascript:void(0);' onclick='bims.load_primary_panel(\"bims_panel_main_da_statistical_analysis\");'><b>here</b></a> to go <em>Data Analysis</em> &rarr; <em>Statistical Analysis</em> to view stats from multiple datasets.</div>";
  $form['trait_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Trait Details'),
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    '#prefix'       => '<div id="bims-panel-main-trait-form-trait-details">',
    '#suffix'       => '</div>',
    '#description'  => $desc,
  );
  $form['trait_details']['table']['mview_descriptor'] = array(
    '#type'   => 'value',
    '#value'  => $mview_descriptor,
  );
  $form['trait_details']['table']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );

  // Adds action buttons.
  if ($mview_descriptor && bims_can_admin_action($bims_user)) {

    // Adds 'Edit' button.
    if (!$mview_descriptor->isChado()) {
      $form['trait_details']['table']['edit_btn'] = array(
        '#name'       => 'edit_btn',
        '#type'       => 'button',
        '#value'      => 'Edit',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => 'bims_panel_main_trait_form_ajax_callback',
          'wrapper'  => 'bims-panel-main-trait-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds 'Delete' button.
    $form['trait_details']['table']['delete_btn'] = array(
      '#name'       => 'delete_btn',
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#disabled'   => !$bims_user->canEdit(),
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_trait_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-trait-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['trait_details']['table']['#theme'] = 'bims_panel_main_trait_form_trait_table';

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-trait-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_trait_form_submit';
  $form['#theme'] = 'bims_panel_main_trait_form';
  return $form;
}

/**
 * Adds the list of traits.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param integer $cvterm_id
 * @param array $cv_list
 *
 * @return array
 */
function _bims_panel_main_trait_form_add_trait_list(&$form, BIMS_PROGRAM $bims_program, $cvterm_id, $cv_list = array()) {

  // Gets the all CVs.
  $opt_cv = $bims_program->getAllCV(BIMS_OPTION);

  // Sets the default descriptor group.
  $flag_no_cv = FALSE;
  if (empty($cv_list)) {
    if(empty($opt_cv)) {
      $flag_no_cv = TRUE;
    }
    else {
      $cv_list []= bims_get_first_elem($opt_cv, BIMS_ARRAY_KEY);
    }
  }

  // Shows 'No CV' message.
  if ($flag_no_cv) {
    $msg = "No descriptor group has been chosen for this program.<br />Please go 'Data Import' section to add a descriptor group.";
    $form['no_cv'] = array(
      '#markup' => $msg,
    );
    return;
  }

  // Shows the list all available CVs.
  if (sizeof($opt_cv) > 1) {
    $form['cv'] = array(
      '#title'          => 'Descriptor Groups',
      '#type'           => 'checkboxes',
      '#options'        => $opt_cv,
      '#multiple'       => TRUE,
      '#description'    => 'Please choose descriptor group(s)',
      '#default_value'  => $cv_list,
      '#prefix'         => '<div style="padding:5px;border:1px solid lightgray;">',
      '#attributes'     => array('style' => 'backgroud-color:#EEEEEE;'),
    );
    $form['update_btn'] = array(
      '#type'       => 'submit',
      '#value'      => 'Update',
      '#name'       => 'update_btn',
      '#attributes' => array('style' => 'width:180px;'),
      '#suffix'     => '</div>',
      '#ajax'       => array(
        'callback' => 'bims_panel_main_trait_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-trait-form-trait-details',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds the list of all the traits.
  $traits = BIMS_MVIEW_DESCRIPTOR::getDescriptorsByCVs($bims_program->getProgramID(), $cv_list, NULL, BIMS_CLASS);
  if (empty($traits)) {
    $form['no_descriptor'] = array(
      '#markup' => '<div>No trait is associated with the current program.</div>',
    );
  }
  else {
    $flag = FALSE;
    $opt_trait = array();
    foreach ((array)$traits as $trait) {

      // Don't show the matched descriptor.
      if ($trait->getBaseDescriptor()) {
        continue;
      }

      // Marks the descriptor.
      $star = '';
      if (!$bims_program->isPublicChado()) {
        if ($trait->isChado()) {
          $flag = TRUE;
          $star = ' *';
        }
        if ($trait->getMatchedDescriptor()) {
          $flag = TRUE;
          $star = ' **';
        }
      }
      $opt_trait[$trait->getCvtermID()] = $trait->getName() . $star;
    }
    $desc = "Click a trait to see the details.";
    if ($flag) {
      $desc .= "<br />(* indicates imported traits  and ** matched tratis)";
    }
    $form['selection'] = array(
      '#type'           => 'select',
      '#options'        => $opt_trait,
      '#size'           => 14,
      '#default_value'  => $cvterm_id,
      '#description'    => $desc,
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => 'bims_panel_main_trait_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-trait-form-trait-details',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trait_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Update" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets the chosen descriptor groups.
    $cv_ids = $form_state['values']['trait_list']['cv'];
    $cv_id_arr = array();
    foreach ((array)$cv_ids as $cv_id => $bool) {
      if ($bool) {
        $cv_id_arr []= $cv_id;
      }
    }

    // Updates panels.
    $cv_id_str = implode(':', $cv_id_arr);
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_trait/NC/$cv_id_str")));
  }

  // If "Edit" or "Delete" was clicked.
  else if ($trigger_elem == 'edit_btn' || $trigger_elem == 'delete_btn') {

    // Gets cvterm_id.
    $cvterm_id = $form_state['values']['trait_list']['selection'];
    if ($cvterm_id) {

      // If "Edit" was clicked.
      if ($trigger_elem == 'edit_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_trait_edit/$cvterm_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_trait_edit', 'Edit Trait', $url)));
      }

      // If the 'delete' was clicked.
      if ($trigger_elem == 'delete_btn') {

        // Deletes the trait.
        if (_bims_panel_main_trait_form_delete_trait($program_id, $cvterm_id)) {

          // Updates panels.
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_trait')));
        }
      }
    }
    else {
      drupal_set_message("Please select a trait");
    }
  }

  // 'trait' was clicked.
  else if ($trigger_elem == 'trait_list[selection]') {

    // Gets cvterm_id.
    $cvterm_id = $form_state['values']['trait_list']['selection'];
    if ($cvterm_id) {
      $mview_descriptor = BIMS_MVIEW_DESCRIPTOR::byKey(array('cvterm_id' => $cvterm_id));
      $format = $mview_descriptor->getFormat();

      // Loads the chart.
      if (preg_match("/(boolean|categorical|multicat)/", $format)) {
        $params = "chart_trait:$program_id:doughnut:$cvterm_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "load_chart", array('bims_chart_trait', $params)));
      }
      else if ($mview_descriptor->isNumeric() || $format == 'date') {
        $params = "chart_trait:$program_id:bar:$cvterm_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "load_chart", array('bims_chart_trait', $params)));
      }
    }

    // Refresh the details section.
    return $form['trait_details'];
  }
  else {
    return $form;
  }
}

/**
 * Deletes the trait.
 *
 * @param integer $program_id
 * @param integer $cvterm_id
 *
 * @return boolean
 */
function _bims_panel_main_trait_form_delete_trait($program_id, $cvterm_id) {

  $transaction = db_transaction();
  try {

    // Deletes the cvterm.
    $bims_cvterm = new BIMS_CVTERM($cvterm_id, $program_id);
    if (!$bims_cvterm->deleteCvterm()) {
      throw new Exception("Error : Failed to delete the trait");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trait_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trait_form_submit($form, &$form_state) {}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_trait_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), 'Add a new trait.');


  if ($bims_user->getName() == 'ltaein') {
    $rows[] = array(drupal_render($element['create_btn']), 'Create a new trait from the existing traits.');
  }


  if (!$bims_program->isPublicChado()) {
    $rows[] = array(drupal_render($element['order_btn']), 'Customerize the order of the descriptors.');
    $rows[] = array(drupal_render($element['match_btn']), 'Match / Unmatch the descriptors.');
  }
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the trait details table.
 *
 * @param $variables
 */
function theme_bims_panel_main_trait_form_trait_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_MVIEW_DESCIPTOR.
  $mview_descriptor = $element['mview_descriptor']['#value'];
  $bims_program     = $element['bims_program']['#value'];

  // Initializes the properties.
  $name       = '--';
  $alias      = '--';
  $format     = '--';
  $def        = '--';
  $stats      = NULL;
  $stats_rows = NULL;
  $data_point = '';
  $prop       = '';
  $actions    = '';
  $d_group    = '';
  $d_matched  = '';

  // Shows the current available formats for a trait.
  $format_types =  BIMS_FIELD_BOOK::getFormats(TRUE);
  $format = '<SELECT style="width:250px;">';
  foreach ((array)$format_types as $cvterm_id => $format_type) {
    $format .= "<OPTION>$format_type</OPTION>";
  }
  $format .= '</SELECT>';

  // Updates the properites.
  if ($mview_descriptor) {
    $name     = $mview_descriptor->getName();
    $alias    = $mview_descriptor->getAlias();
    $format   = $mview_descriptor->getFormat();
    $def      = $mview_descriptor->getDefinition();
    $prop_arr = $mview_descriptor->getPropArr();
    $group    = $mview_descriptor->getDescriptorGroup();
    $matched  = $mview_descriptor->getMatchedDescriptor(BIMS_CLASS);

    // Updates properties.
    if ($matched) {
      $d_matched = $matched->getName(). '<br />[' . $matched->getDescriptorGroup() . ']';
    }
    $d_group  = ($group != 'program') ? $group : '';
    $alias    = ($alias) ? $alias : '<em>N/A</em>';
    $def      = ($def) ? "<textarea style='width:100%;' rows=3 READONLY>$def</textarea>" : '<em>N/A</em>';

    // Adds the rest of the properties.
    $prop = '';
    if (!empty($prop_arr)) {
      $props = '';
      foreach ($prop_arr as $key => $val) {
        if ($key == 'codes') {
          $tmp = array();
          foreach ($val as $item) {
            $tmp []= $item['key'] . ' : ' . $item['val'];
          }
          $props .= 'codes = ' . implode(', ', $tmp);
          continue;
        }
        if (is_array($val)) {
          $val = json_encode($val);
        }
        $props .= "$key = $val\n";
      }
      $prop = "<textarea style='width:100%;' rows=3 READONLY>$props</textarea>";
    }

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
      if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }

    // Adds the statistics for the trait.
    $args = array(
      'node_id'   => $bims_program->getNodeID(),
      'cvterm_id' => $mview_descriptor->getCvtermID(),
    );
    $bims_stats = BIMS_MVIEW_PHENOTYPE_STATS::byKey($args);
    if ($bims_stats) {

      // Gets the stats.
      $stats_arr = $bims_stats->getStatsArr();
      if ($stats_arr['num']) {
        if ($mview_descriptor->isDataPointOnly()) {
          $data_point = $stats_arr['num'];
        }
        else {
          if (preg_match("/(boolean|categorical|multicat)/", $format)) {
            $data_point = $stats_arr['num'];
            $chart = '<canvas id="bims_chart_trait" style="width:100%;"></canvas>';
            $stats = array(array('data' => $chart, 'colspan' => 2));
          }
          else {
            $chart = '<canvas id="bims_chart_trait" style="width:100%;"></canvas><div id="bims_chart_trait_table">&nbsp</div>';
            $stats = array(array('data' => $chart, 'colspan' => 2));
          }
        }
      }
      else {
        $stats = array('Statistics', '<em>There is no data associated with this trait.</em>');
      }
    }
    else {
      $stats = array('Statistics', '<em>There is no data associated with this trait.</em>');
    }
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'width' => '100'), $name);
  if ($d_group) {
    $rows[] = array('Descriptor Group', $d_group);
  }
  $rows[] = array('Alias', $alias);
  $rows[] = array('Format', $format);
  if ($d_matched) {
    $rows[] = array('Matched<br />Descriptors', $d_matched);
  }
  $rows[] = array('Definition', $def);
  if ($prop) {
    $rows[] = array('Properties', $prop);
  }
  if ($data_point) {
    $rows[] = array('# data points', $data_point);
  }
  if (!empty($stats)) {
    $rows[] = $stats;
  }
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the form.
 */
 function theme_bims_panel_main_trait_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('trait_admin', $form)) {
    $layout.= "<div style='float:left;width:100%;'>" . drupal_render($form['trait_admin']) . '</div>';
  }

  // Adds the traits list by group.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['trait_list']) . "</div>";

  // Adds the trait details
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['trait_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}