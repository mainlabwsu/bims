<?php
/**
 * @file
 */
/**
 * Add trait panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_edit_form($form, &$form_state, $cvterm_id) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_trait_edit');
  $bims_panel->init($form);

  // Gets the trait.
  $mview_descriptor = BIMS_MVIEW_DESCRIPTOR::byKey(array('cvterm_id' => $cvterm_id));
  $form['cvterm_id'] = array(
    '#type'   => 'value',
    '#value'  => $cvterm_id,
  );

  // Properties of the trait.
  $form['trait_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Edit Trait',
    '#attributes'   => array(),
  );
  _bims_panel_trait_edit_form_elems($form, $mview_descriptor);
  $form['trait_prop']['edit_trait_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#name'       => 'edit_trait_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_trait_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['trait_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_trait_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-trait-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_trait_edit_form_submit';
  return $form;
}

/**
 * Adds form elements based on the provided format.
 *
 * @param array &$form
 * @param BIMS_MVIEW_DESCRIPTOR $mview_descriptor
 */
function _bims_panel_trait_edit_form_elems(&$form, BIMS_MVIEW_DESCRIPTOR $mview_descriptor) {

  // Gets the format.
  $format = $mview_descriptor->getFormat();

  // Adds common fields.
  $form['trait_prop']['name'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Trait Name'),
    '#description'    => t("Please provide a trait name."),
    '#default_value'  => $mview_descriptor->getName(),
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['alias'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Alias'),
    '#default_value'  => $mview_descriptor->getAlias(),
    '#description'    => t("Please provide an alias of the trait"),
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['format'] = array(
    '#type'           => 'textfield',
    '#title'          => 'Format',
    '#default_value'  => $format,
    '#description'    => '(<em>The format of the trait cannot be changed</em>)',
    '#disabled'       => TRUE,
    '#attributes'     => array('style' => 'width:250px;'),
  );

  // Format : numeric.
  if ($format == 'numeric') {
    $form['trait_prop']['data_unit'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Data Unit'),
      '#default_value'  => $mview_descriptor->getPropByKey('data_unit'),
      '#description'    => t("Please provide data unit (e.g. cm, lb)"),
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['trait_prop']['minimum'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Minimum'),
      '#default_value'  => $mview_descriptor->getPropByKey('minimum'),
      '#description'    => t("Please provide minimum value"),
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['trait_prop']['maximum'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Maximum'),
      '#default_value'  => $mview_descriptor->getPropByKey('maximum'),
      '#description'    => t("Please provide maximum value"),
      '#attributes'     => array('style' => 'width:400px;'),
    );
    $form['trait_prop']['defaultValue'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Default Value'),
      '#default_value'  => $mview_descriptor->getPropByKey('defaultValue'),
      '#description'    => t("Please provide default value"),
      '#attributes'     => array('style' => 'width:400px;'),
    );
  }
  else if (preg_match("/(categorical|multicat)/", $format)) {
    $form['trait_prop']['categories'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Categories'),
      '#default_value'  => $mview_descriptor->getPropByKey('categories'),
      '#description'    => t("Please provide categories, separted by '/'"),
      '#attributes'     => array('style' => 'width:400px;'),
    );
  }
  else if ($format == 'boolean') {
    $bool = array(0 => 'FALSE', 1 => 'TRUE');
    $form['trait_prop']['defaultValue'] = array(
      '#type'           => 'radios',
      '#title'          => t('Default Value'),
      '#default_value'  => $mview_descriptor->ggetPropByKey('defaultValue'),
      '#options'        => $bool,
      '#description'    => t("Please choose default value"),
    );
  }
  else if ($format == 'text') {
    $form['trait_prop']['defaultValue'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Default Value'),
      '#default_value'  => $mview_descriptor->getPropByKey('defaultValue'),
      '#description'    => t("Please provide default value"),
      '#attributes'     => array('style' => 'width:400px;'),
    );
  }
  $form['trait_prop']['definition'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Definition'),
    '#rows'           => 4,
    '#default_value'  => $mview_descriptor->getDefinition(),
    '#description'    => t("Please provide a definition of the trait"),
    '#attributes'     => array('style' => 'width:400px;'),
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_edit_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Edit' was clicked.
  if ($trigger_elem == 'edit_trait_btn') {

    // Gets the properties of the trait.
    $cvterm_id  = $form_state['values']['cvterm_id'];
    $cvterm     = MCL_CHADO_CVTERM::byID($cvterm_id);
    $cur_name   = $cvterm->getName();
    $new_name   = trim($form_state['values']['trait_prop']['name']);
    $format     = trim($form_state['values']['trait_prop']['format']);

    // Validates the trait name.
    $error = bims_validate_name($new_name);
    if ($error) {
      form_set_error('trait_prop][name', "Invalid trait name : $error");
      return;
    }

    // Checks the trait name for duplication if the current name and new name
    // are different.
    if ($cur_name != $new_name) {
      $descriptor = MCL_CHADO_CVTERM::getCvtermByCvID($bims_program->getCvID('descriptor'), $new_name);
      if ($descriptor) {
        form_set_error('trait_prop][name', t("'$new_name' has already existed. Please change the name."));
        return;
      }
    }

    // Checks values for categorical and multicat.
    if (preg_match("/(categorical|multicat)/", $format)) {

      // Cleans up categories.
      $categories = trim($form_state['values']['trait_prop']['categories']);
      $categories = bims_clean_separated_items($categories, '/', '\/');
      $tmp = explode('/', $categories);
      if (sizeof($tmp) < 2) {
        form_set_error('trait_prop][categories', t("Please add at least 2 categories separted by '/'."));
        return FALSE;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_edit_form_submit($form, &$form_state) {
  $form_state['rebuild']  = TRUE;
  $load_primary_panel     = 'bims_panel_main_trait';

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Edit' was clicked.
  if ($trigger_elem == 'edit_trait_btn') {

    $transaction = db_transaction();
    try {

      // Gets the properties of the trait.
      $cvterm_id    = $form_state['values']['cvterm_id'];
      $name         = trim($form_state['values']['trait_prop']['name']);
      $alias        = trim($form_state['values']['trait_prop']['alias']);
      $format       = trim($form_state['values']['trait_prop']['format']);
      $definition   = trim($form_state['values']['trait_prop']['definition']);
      $defaultValue = array_key_exists('defaultValue', $form_state['values']['trait_prop']) ? $form_state['values']['trait_prop']['defaultValue'] : '';
      $data_unit    = array_key_exists('data_unit', $form_state['values']['trait_prop'])    ? $form_state['values']['trait_prop']['data_unit']    : '';
      $minimum      = array_key_exists('minimum', $form_state['values']['trait_prop'])      ? $form_state['values']['trait_prop']['minimum']      : '';
      $maximum      = array_key_exists('maximum', $form_state['values']['trait_prop'])      ? $form_state['values']['trait_prop']['maximum']      : '';
      $categories   = array_key_exists('categories', $form_state['values']['trait_prop'])   ? $form_state['values']['trait_prop']['categories']   : '';

      // Edit the trait.
      $details = array(
        'cvterm_id'     => $cvterm_id,
        'name'          => $name,
        'alias'         => $alias,
        'format'        => $format,
        'definition'    => $definition,
        'defaultValue'  => $defaultValue,
        'data_unit'     => $data_unit,
        'minimum'       => $minimum,
        'maximum'       => $maximum,
        'categories'    => $categories,
      );
      if (!$bims_program->updateTrait($details)) {
        throw new Exception("Error : Failed to update the trait ($cvterm_id)");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_trait_edit')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_trait_edit_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "trait".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['trait_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
