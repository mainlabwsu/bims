<?php
/**
 * @file
 */
/**
 * Match trait panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_match_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 340;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_trait_match');
  $bims_panel->init($form);

  // 'Match' trait.
  $disabled_base = TRUE;
  $form['trait_match'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Match Traits',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Trait [Base].
  $opt_trait_base = $bims_program->getUnmatchedDescriptors(TRUE, BIMS_VARIOUS);
  if (empty($opt_trait_base)) {
    $form['trait_match']['trait_base'] = array(
      '#markup' => '<em>No trait selection.</em>',
    );
  }
  else {
    $disabled_base = FALSE;
    $form['trait_match']['trait_base'] = array(
      '#type'       => 'select',
      '#title'      => t('Your Traits'),
      '#options'    => $opt_trait_base,
      '#attributes' => array('style' => 'width:260px;'),
    );
  }

  // Trait [Matching].
  $disabled_matching = TRUE;
  $opt_trait_matching = $bims_program->getUnmatchedDescriptors(FALSE, BIMS_VARIOUS);
  if (empty($opt_trait_matching)) {
    $form['trait_match']['trait_matching'] = array(
      '#markup' => '<div style="margin:10px;"><em>No trait found for matching.</em></div>',
    );
  }
  else {
    $disabled_matching = FALSE;
    $form['trait_match']['trait_matching'] = array(
      '#type'         => 'select',
      '#title'        => t('Trait [Matching]'),
      '#options'      => $opt_trait_matching,
      '#attributes'   => array('style' => 'width:260px;'),
    );
  }
  $form['trait_match']['match_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Match traits',
    '#name'       => 'match_btn',
    '#disabled'   => !(!$disabled_base && !$disabled_matching),
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_trait_match_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-match-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // 'unmatch' trait.
  $opt_traits = $bims_program->getMatchedBaseDescriptors(BIMS_OPTION);
  $max_size = 10;
  $num_traits = sizeof($opt_traits);
  $size = ($max_size < $num_traits) ? $max_size : $num_traits;
  $disabled_unmatch = TRUE;
  $form['trait_unmatch'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Unmatch Traits',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  if ($num_traits == 0) {
    $form['trait_unmatch']['matched_traits'] = array(
      '#markup' => '<div style="margin:10px;"><em>No matched traits.</em></div>',
    );
  }
  else {
    $disabled_unmatch = FALSE;
    $form['trait_unmatch']['matched_traits'] = array(
      '#type'         => 'select',
      '#title'        => t('Matched Traits'),
      '#options'      => $opt_traits,
      '#size'         => $size,
      '#multiple'     => TRUE,
      '#attributes'   => array('style' => 'width:260px;'),
    );
  }
  $form['trait_unmatch']['unmatch_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Unmatch traits',
    '#name'       => 'unmatch_btn',
    '#disabled'   => $disabled_unmatch,
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_trait_match_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-match-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-trait-match-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_trait_match_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_match_form_ajax_callback(&$form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_match_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();


  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Unmatch traits' was clicked.
  if ($trigger_elem == 'unmatch_btn') {

    // Checks the selected traits.
    $cvterm_ids = $form_state['values']['trait_unmatch']['matched_traits'];
    if (empty($cvterm_ids)) {
      form_set_error('trait_unmatch][matched_traits', 'Please choose at least one trait');
      return;
    }
  }
  // If 'Match traits' was clicked.
  else if ($trigger_elem == 'match_btn') {

    // Gets the cvterm ID of the selected descriptors.
    $cvterm_id_base     = $form_state['values']['trait_match']['trait_base'];
    $cvterm_id_matching = $form_state['values']['trait_match']['trait_matching'];

    // Checks the format of the descriptors.
    $cvterm_base      = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id_base);
    $cvterm_matching  = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id_matching);
    $format_base      = $cvterm_base->getFormat();
    $format_matching  = $cvterm_matching->getFormat();
    if ($format_base != $format_matching) {
      form_set_error('trait_match][trait_base', "Please choose at descriptor with the same format [$format_base : $format_matching]");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_match_form_submit($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Match traits' was clicked.
  if (preg_match("/match_btn$/", $trigger_elem)) {
    $transaction = db_transaction();
    $update_panel = TRUE;
    try {

      // If 'Match traits' was clicked.
      if ($trigger_elem == 'match_btn') {

        // Gets the selected traits.
        $cvterm_id_base     = $form_state['values']['trait_match']['trait_base'];
        $cvterm_id_matching = $form_state['values']['trait_match']['trait_matching'];

        // Matches the traits.
        if (!$bims_program->matchDescriptors('match', $cvterm_id_base, $cvterm_id_matching)) {
          $update_panel = FALSE;
          drupal_set_message("Error : Failed to match traits.", 'error');
        }
      }

      // If 'Unmatch traits' was clicked.
      else if ($trigger_elem == 'unmatch_btn') {

        // Gets the selected traits.
        $cvterm_ids = $form_state['values']['trait_unmatch']['matched_traits'];

        // Unmatches the traits.
        foreach ((array)$cvterm_ids as $cvterm_id_base) {
          if (!$bims_program->matchDescriptors('unmatch', $cvterm_id_base)) {
            $update_panel = FALSE;
            drupal_set_message("Error : Failed to unmatch traits from the base trait [$cvterm_id_base].", 'error');
          }
        }
        /*

        $drush  = bims_get_config_setting('bims_drush_binary');
        $cmd   = "bims-match-descriptors $program_id unmatch --base=" . implode(':', $cvterm_ids);
        $drush .= " $cmd > /dev/null 2>/dev/null  & echo $!";

        $pid = exec($drush, $output, $return_var);
        dpm($pid);dpm($return_var);
        dpm($drush);dpm($output);

        if (!$return_var) {
          $update_panel = FALSE;
          drupal_set_message("Error : Failed to unmatch traits.", 'error');
        }*/
      }

      // Updates panels.
      if ($update_panel) {
        $url = 'bims/load_main_panel/bims_panel_add_on_trait_match';
        bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_trait')));
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_trait_match', 'Match Traits', $url)));
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }
}

/**
 * Theme function for the form.
 *
 * @param array $variables
 */
function theme_bims_panel_add_on_trait_match_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // 'Match' trait.
  $layout .= "<div style='float:left;width:49%;'>" . drupal_render($form['trait_match']) . "</div>";

  // 'unmatch' trait.
  $layout .= "<div style='float:left;width:49%;margin-left:5px;'>" . drupal_render($form['trait_unmatch']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
