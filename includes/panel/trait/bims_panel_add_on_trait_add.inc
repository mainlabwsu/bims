<?php
/**
 * @file
 */
/**
 * Add trait panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_add_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Gets all the formats of a descriptor.
  $def_format = 'numeric';
  if (!empty($form_state['values']['trait_prop']['format_list'])) {
    $def_format = $form_state['values']['trait_prop']['format_list'];
  }

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_trait_add');
  $bims_panel->init($form);

  // Properties of a trait.
  $form['trait_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Add Trait',
    '#attributes'   => array(),
  );
  $form['trait_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Trait Name'),
    '#description'  => t("Please provide a trait name."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['alias'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Alias'),
    '#description'  => t("Please provide an alias of the trait"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['format_list'] = array(
    '#type'           => 'select',
    '#title'          => t('Data Format'),
    '#options'        => BIMS_FIELD_BOOK::getFormats(TRUE),
    '#default_value'  => $def_format,
    '#description'    => t("Please choose a format of the trait"),
    '#attributes'     => array('style' => 'width:250px;'),
    '#ajax'           => array(
      'callback' => 'bims_panel_add_on_trait_add_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-trait-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  _bims_panel_trait_add_form_elems($form, $def_format);
  $form['trait_prop']['definition'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Definition'),
    '#rows'         => 3,
    '#description'  => t("Please provide a definition of the trait"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['add_trait_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Add a new trait',
    '#name'       => 'add_trait_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_trait_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['trait_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_trait_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-trait-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_trait_add_form_submit';
  return $form;
}

/**
 * Adds form elements based on the provided format.
 *
 * @param array &$form
 * @param string $format
 */
function _bims_panel_trait_add_form_elems(&$form , $format = 'numeric') {

  // Format : numeric.
  if ($format == 'numeric') {
    $form['trait_prop']['data_unit'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Data Unit'),
      '#description'  => t("Please provide data unit (e.g. cm, lb)"),
      '#attributes'   => array('style' => 'width:400px;'),
    );
    $form['trait_prop']['minimum'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Minimum'),
      '#description'  => t("Please provide minimum value"),
      '#attributes'   => array('style' => 'width:400px;'),
    );
    $form['trait_prop']['maximum'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Maximum'),
      '#description'  => t("Please provide maximum value"),
      '#attributes'   => array('style' => 'width:400px;'),
    );
    $form['trait_prop']['defaultValue'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Default Value'),
      '#description'  => t("Please provide default value"),
      '#attributes'   => array('style' => 'width:400px;'),
    );
  }
  else if (preg_match("/(categorical|multicat)/", $format)) {
    $form['trait_prop']['categories'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Categories'),
      '#description'  => '<em>Please provide categories, separted by /</em>',
      '#attributes'   => array('style' => 'width:400px;'),
    );
  }
  else if ($format == 'boolean') {
    $bool = array('FALSE' => 'FALSE', 'TRUE' => 'TRUE');
    $form['trait_prop']['defaultValue'] = array(
      '#type'           => 'radios',
      '#title'          => t('Default Value'),
      '#default_value'  => 'FALSE',
      '#options'        => $bool,
      '#description'    => t("Please choose default value"),
    );
  }
  else if ($format == 'text') {
    $form['trait_prop']['defaultValue'] = array(
      '#type'         => 'textfield',
      '#title'        => t('Default Value'),
      '#description'  => t("Please provide default value"),
      '#attributes'   => array('style' => 'width:400px;'),
    );
  }
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_add_form_ajax_callback(&$form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // 'Data Format' was changed
  if ($trigger_elem == 'trait_prop[data_format]') {

    $format = $form_state['values']['trait_prop']['format_list'];
    _bims_panel_trait_add_form_elems($form, $format);
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_add_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add" was clicked.
  if ($trigger_elem == 'add_trait_btn') {

    // Gets the properties of the trait.
    $name   = trim($form_state['values']['trait_prop']['name']);
    $format = trim($form_state['values']['trait_prop']['format_list']);

    // Validates the trait name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('trait_prop][name', "Invalid trait name : $error");
      return;
    }

    // Checks the trait name for duplication.
    $descriptor = MCL_CHADO_CVTERM::getCvtermByCvID($bims_program->getCvID('descriptor'), $name);
    if ($descriptor) {
      form_set_error('trait_prop][name', t("'$name' has already existed. Please change the name."));
      return;
    }

    // Checks values for categorical and multicat.
    if (preg_match("/(categorical|multicat)/", $format)) {
      $categories = trim($form_state['values']['trait_prop']['categories']);
      $tmp = explode('/', $categories);
      if (sizeof($tmp) < 2) {
        form_set_error('trait_prop][categories', t("Please add at least 2 categories separted by '/'."));
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_add_form_submit($form, &$form_state) {
  $form_state['rebuild']  = TRUE;
  $load_primary_panel     = 'bims_panel_main_trait';

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add" was clicked.
  if ($trigger_elem == 'add_trait_btn') {

    $transaction = db_transaction();
    try {

      // Gets the properties of the trait.
      $name         = trim($form_state['values']['trait_prop']['name']);
      $alias        = trim($form_state['values']['trait_prop']['alias']);
      $format       = trim($form_state['values']['trait_prop']['format_list']);
      $definition   = trim($form_state['values']['trait_prop']['definition']);
      $defaultValue = array_key_exists('defaultValue', $form_state['values']['trait_prop']) ? $form_state['values']['trait_prop']['defaultValue'] : '';
      $data_unit    = array_key_exists('data_unit', $form_state['values']['trait_prop'])    ? $form_state['values']['trait_prop']['data_unit']    : '';
      $minimum      = array_key_exists('minimum', $form_state['values']['trait_prop'])      ? $form_state['values']['trait_prop']['minimum']      : '';
      $maximum      = array_key_exists('maximum', $form_state['values']['trait_prop'])      ? $form_state['values']['trait_prop']['maximum']      : '';
      $categories   = array_key_exists('categories', $form_state['values']['trait_prop'])   ? $form_state['values']['trait_prop']['categories']   : '';

      // Adds a new trait.
      $details = array(
        'cv_id'         => $bims_program->getCvID('descriptor'),
        'name'          => $name,
        'alias'         => $alias,
        'format'        => $format,
        'definition'    => $definition,
        'defaultValue'  => $defaultValue,
        'data_unit'     => $data_unit,
        'minimum'       => $minimum,
        'maximum'       => $maximum,
        'categories'    => $categories,
      );
      if (!$bims_program->addTrait($details)) {
        throw new Exception("Error : Failed to add a new trait");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_trait_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}

/**
 * Theme function for the form.
 *
 * @param array $variables
 */
function theme_bims_panel_add_on_trait_add_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "trait".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['trait_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
