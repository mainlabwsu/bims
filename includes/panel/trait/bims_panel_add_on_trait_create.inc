<?php
/**
 * @file
 */
/**
 * Create trait panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_create_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_trait_create');
  $bims_panel->init($form);

  // Properties of a trait.
  $form['trait_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Add Trait',
    '#attributes'   => array(),
  );
  $form['trait_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Trait Name'),
    '#description'  => t("Please provide a trait name."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['alias'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Alias'),
    '#description'  => t("Please provide an alias of the trait"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['definition'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Definition'),
    '#rows'         => 3,
    '#description'  => t("Please provide a definition of the trait"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['trait_prop']['create_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Create a new trait',
    '#name'       => 'create_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_trait_create_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-create-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['trait_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_trait_create_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trait-create-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-trait-create-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_trait_create_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_create_form_ajax_callback(&$form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];




  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_create_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add" was clicked.
  if ($trigger_elem == 'create_btn') {

    // Gets the trait name.
    $name = trim($form_state['values']['trait_prop']['name']);

    // Validates the trait name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('trait_prop][name', "Invalid trait name : $error");
      return;
    }

    // Checks the trait name for duplication.
    $descriptor = MCL_CHADO_CVTERM::getCvtermByCvID($bims_program->getCvID('descriptor'), $name);
    if ($descriptor) {
      form_set_error('trait_prop][name', t("'$name' has already existed. Please change the name."));
      return;
    }

    // Checks values for categorical and multicat.
    if (preg_match("/(categorical|multicat)/", $format)) {
      $categories = trim($form_state['values']['trait_prop']['categories']);
      $tmp = explode('/', $categories);
      if (sizeof($tmp) < 2) {
        form_set_error('trait_prop][categories', t("Please add at least 2 categories separted by '/'."));
      }
    }
  }
}

/**
 * Creates a trait.
 *
 * @param array $form_state
 *
 * @return MCL_CHADO_CVTERM
 */
function _bims_panel_add_on_trait_create_form_create_trait($form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the properties of the trait.
  $name         = trim($form_state['values']['trait_prop']['name']);
  $alias        = trim($form_state['values']['trait_prop']['alias']);
  $format       = trim($form_state['values']['trait_prop']['format_list']);
  $definition   = trim($form_state['values']['trait_prop']['definition']);
  $defaultValue = array_key_exists('defaultValue', $form_state['values']['trait_prop']) ? $form_state['values']['trait_prop']['defaultValue'] : '';
  $data_unit    = array_key_exists('data_unit', $form_state['values']['trait_prop'])    ? $form_state['values']['trait_prop']['data_unit']    : '';
  $minimum      = array_key_exists('minimum', $form_state['values']['trait_prop'])      ? $form_state['values']['trait_prop']['minimum']      : '';
  $maximum      = array_key_exists('maximum', $form_state['values']['trait_prop'])      ? $form_state['values']['trait_prop']['maximum']      : '';
  $categories   = array_key_exists('categories', $form_state['values']['trait_prop'])   ? $form_state['values']['trait_prop']['categories']   : '';

  // Adds a descriptor.
  $cv = $bims_program->getCv('descriptor');
  $descriptor = MCL_CHADO_CVTERM::addCvterm(NULL, 'SITE_DB', $cv->getName(), $name, $definition);
  if ($descriptor) {

    // Cleans up aliases and add them to the database.
    if ($alias) {
      $alias = bims_clean_separated_items($alias, ' ,', '[,;]');

      // Adds aliases.
      $descriptor->addAlias(NULL, 'SITE_CV', 'alias', $alias, '[,;]');
    }

    // Cleans up categories.
    $categories = bims_clean_separated_items($categories, '/', '\/');

    // Adds common properties.
    $descriptor->addProp(NULL, 'SITE_CV', 'format', $format);

    // Adds properties for this format.
    $prop = array();
    if ($format == 'numeric') {
      $descriptor->addProp(NULL, 'BIMS_FIELD_BOOK_FORMAT_PROP', 'defaultValue', $defaultValue);
      $descriptor->addProp(NULL, 'BIMS_FIELD_BOOK_FORMAT_PROP', 'data_unit', $data_unit);
      $descriptor->addProp(NULL, 'BIMS_FIELD_BOOK_FORMAT_PROP', 'minimum', $minimum);
      $descriptor->addProp(NULL, 'BIMS_FIELD_BOOK_FORMAT_PROP', 'maximum', $maximum);
    }
    else if (preg_match("/(text|boolean)/", $format)) {
      $descriptor->addProp(NULL, 'BIMS_FIELD_BOOK_FORMAT_PROP', 'defaultValue', $defaultValue);
    }
    else if (preg_match("/(categorical|multicat)/", $format)) {
      $descriptor->addProp(NULL, 'BIMS_FIELD_BOOK_FORMAT_PROP', 'categories', $categories);
    }

    // Adds it to bims_mview_descriptor.
    $details = array(
      'program_id'    => $program_id,
      'cv_id'         => $cv->getCvID(),
      'name'          => $name,
      'cvterm_id'     => $descriptor->getCvtermID(),
      'alias'         => $alias,
      'format'        => $format,
      'definition'    => $definition,
    );
    $mview_descriptor = new BIMS_MVIEW_DESCRIPTOR($details);
    if (!$mview_descriptor->insert()) {
      drupal_set_message('Error : Failed to insert into the mview', 'error');
      return NULL;
    }
    $mview_descriptor = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $descriptor->getCvtermID());
    if (!$mview_descriptor->updateProp()) {
      drupal_set_message('Error : Failed to update the properties in the mview', 'error');
      return NULL;
    }
  }
  else {
    drupal_set_message("Error : Failed to add a new trait", 'error');
    return NULL;
  }
  return $descriptor;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trait_create_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Create" was clicked.
  if ($trigger_elem == 'create_btn') {

    $transaction = db_transaction();
    try {

      // Adds a new trait.
      $trait = _bims_panel_add_on_trait_create_form_create_trait($form_state);
      if (!$trait) {
        throw new Exception("Error : Failed to add a new trait");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_trait_create')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_trait')));
}

/**
 * Theme function for the form.
 *
 * @param array $variables
 */
function theme_bims_panel_add_on_trait_create_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "trait".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['trait_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
