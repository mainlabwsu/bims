<?php
/**
 * @file
 */
/**
 * List edit panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_edit_form($form, &$form_state, $list_id) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();

  // Creates a form.
  $form = array();

  // Gets and save BIMS_LIST.
  $bims_list = BIMS_LIST::byID($list_id);
  $form['bims_list'] = array(
    '#type' => 'value',
    '#value' => $bims_list,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_list_edit');
  $bims_panel->init($form);

  // Properties of a list.
  $form['list_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Edit List',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['list_prop']['list_name'] = array(
    '#type'           => 'textfield',
    '#name'           => 'list_name',
    '#title'          => 'List name',
    '#default_value'  =>$bims_list->getName(),
    '#description'    => t("Please provide a list name. Please use alphabets, dash and underscore"),
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['list_prop']['list_desc'] = array(
    '#type'           => 'textarea',
    '#name'           => 'list_desc',
    '#default_value'  => $bims_list->getDescription(),
    '#title'          => 'Description of the list',
    '#rows'           => 3,
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['list_prop']['edit_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'edit_btn',
    '#value'      => 'Edit',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_list_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['list_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_list_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-list-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_list_edit_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_edit_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_btn') {

    // Gets BIMS_LIST.
    $bims_list = $form_state['values']['bims_list'];

    // Gets the name of the list.
    $list_name = trim($form_state['values']['list_name']);

    // Checks the list name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-]+$/", $list_name)) {
      form_set_error('list_prop][name', "Invalid list name");
      return;
    }

    // Checks for a duplication if list names are different.
    $cur_list_name  = $bims_list->getName();
    if ($cur_list_name != $list_name) {
      $keys = array(
        'type'        => $bims_list->getType(),
        'program_id'  => $bims_list->getProgramID(),
        'user_id'     => $bims_list->getUserID(),
        'name'        => $list_name,
      );
      $bims_list = BIMS_LIST::byKey($keys);
      if ($bims_list) {
        form_set_error('list_prop][list_name', "The list name ($list_name) has been used. Please choose other name.");
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_list';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_btn') {

    $transaction = db_transaction();
    try {

      // Gets BIMS_LIST.
      $bims_list = $form_state['values']['bims_list'];

      // Gets the properties of the list.
      $list_name  = trim($form_state['values']['list_name']);
      $list_desc  = trim($form_state['values']['list_desc']);

      // Sets the list properites.
      $bims_list->setName($list_name);
      $bims_list->setDescription($list_desc);
      if (!$bims_list->update()) {
        throw new Exception("Error : Failed to edit the list");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Updates the panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_list_edit')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}