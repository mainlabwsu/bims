<?php
/**
 * @file
 */
/**
 * Add list panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_add_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 480;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Gets and sets the chosen list type.
  $list_type = '';
  if (array_key_exists('values', $form_state)) {
    $list_type = trim($form_state['values']['choose_list_type']['list_types']);
  }
  $form['list_type'] = array(
    '#type'   => 'value',
    '#value'  => $list_type,
  );

  // Gets and saves the filters.
  $filters     = NULL;
  $no_filters  = TRUE;
  if (array_key_exists('values', $form_state)) {
    $filters = $form_state['values']['filters'];
    if (sizeof($filters) > 1) {
      $no_filters = FALSE;
    }
  }
  else {
    $filters = BIMS_FILTER::initFilters($bims_user, 'phenotype');
  }
  $form['filters'] = array(
    '#type'   => 'value',
    '#value'  => $filters,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_list_add');
  $bims_panel->init($form);

  // "Choose list type".
  $form['choose_list_type'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Choose a catetory',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $options = $bims_program->getActiveListTypes();
  $size = (sizeof($options) > 10) ? 10 : sizeof($options);
  $form['choose_list_type']['list_types'] = array(
    '#type'         => 'select',
    '#options'      => $options,
    '#size'         => $size,
    '#description'  => t("Please choose a list type"),
    '#attributes'   => array('style' => 'width:100%;margin-top:10px;'),
    '#ajax'         => array(
      'callback' => "bims_panel_add_on_list_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['choose_list_type']['reset_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'reset_btn',
    '#value'      => 'Reset',
    '#attributes' => array('style' => 'margin-top:180px;width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_list_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['choose_list_type']['close_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'close_btn',
    '#value'      => 'Close',
    '#attributes' => array('style' => 'margin-top:5px;width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_list_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // "Choose list items".
  $filter_list_title = 'Choose ' . ucfirst($list_type);
  if (!$list_type) {
    $filter_list_title = 'Choose data';
  }
  $form['filter_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => $filter_list_title,
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets data status.
  if (!$list_type) {
    $form['filter_list']['selected_list_title'] = array(
      '#markup' => '<p>Please choose a category on the left sidebar</p>',
    );
  }
  else if (!$bims_program->hasData($list_type)) {
    $form['filter_list']['selected_list_title'] = array(
      '#markup' => "<p>Please add $list_type data to your program</p>",
    );
  }
  else {

    // Lists all items for the chosen list type.
    $details = array(
      'bims_user' => $bims_user,
      'list_type' => $list_type,
      'filters'   => $filters,
      'id'        => 'bims_list_filter',
    );
    if (!BIMS_FILTER_LIST::getFormFilter($form['filter_list'], $details)) {

      if (0) {
      $form['filter_list']['checkall'] = array(
        '#type' => 'checkbox',
        '#title' => t('Select / Unselect all'),
        '#attributes' => array('onclick' => 'bims.checkUncheckAll(this, "bims_list_filter");'),
        '#weight' => -1,
      );
      }

      $suffix = "<br /><br /><div>Please choose all or some data before adding them to a list.</div>";
      $form['filter_list']['add_filter_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'add_filter_btn',
        '#value'      => 'Add to list',
        '#attributes' => array('style' => 'width:150px;'),
        '#suffix'     => $suffix,
        '#ajax'       => array(
          'callback' => "bims_panel_add_on_list_add_form_ajax_callback",
          'wrapper'  => 'bims-panel-add-on-list-add-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );

      // Disable 'Add' button if it rearches the max number of filters.
      $max_num_filters = BIMS_LIST::getMaxNumFilters();
      if (sizeof($filters)  > $max_num_filters) {
        $msg = "<br /><br /><b>The maximum number of filters : $max_num_filters" .
          "</b><br /><br />The maximum number of the filters are rearched. " .
          "Please remove some of the filters or click 'Reset' button to start over";
        $form['filter_list']['add_filter_btn']['#suffix'] = $msg;
        $form['filter_list']['add_filter_btn']['#disabled'] = TRUE;
      }
    }
    else {
      $msg = "<p>No results. Please remove the last chosen filter or " .
        "click 'Reset' button to start over.</p>";
      $form['filter_list']['no_results'] = array(
        '#markup' => $msg,
      );
    }
    if (0) {
      $form['filter_list']['apply_filter_op'] = array(
        '#type'           => 'radios',
        '#name'           => 'apply_lister_op',
        '#options'        => array('AND' => 'AND', 'OR' => 'OR'),
        '#default_value'  => 'AND',
        '#prefix'         => '<div style="height:10px;">&nbsp;</div>',
        '#attributes'     => array('style' => 'width:20px;'),
      );
    }
  }

  // "Lists".
  $form['lists'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Lists',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Shows the current lists in a table.
  foreach ($filters as $idx => $filter) {
    if ($idx) {
      $form['lists']['filter_table'][$idx]['remove'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_remove_' . $idx,
        '#value'      => 'Remove',
        '#attributes' => array('style' => 'float:left;width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_add_on_list_add_form_ajax_callback",
          'wrapper'  => 'bims-panel-add-on-list-add-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['lists']['filter_table'][$idx]['view'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_view_' . $idx,
        '#value'      => 'View',
        '#attributes' => array('style' => 'float:left;width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_add_on_list_add_form_ajax_callback",
          'wrapper'  => 'bims-panel-add-on-list-add-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['lists']['filter_table'][$idx]['filter'] = array(
        '#type'   => 'value',
        '#value'  => $filter,
      );
    }
  }
  $form['lists']['filter_table']['#theme'] = 'bims_panel_add_on_list_add_form_filter_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-list-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_list_add_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_add_form_ajax_callback(&$form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_add_form_validate($form, $form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Apply' was clicked.
  if ($trigger_elem == 'add_filter_btn') {

    // Checks the values in the form_state.
    $args = array('list_type' => $form_state['values']['list_type']);
    BIMS_FILTER_LIST::checkFormFilter($form_state['values']['filter_list'], $args);
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_list';

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'add_filter_btn') {

    // Gets the values from the form.
    $list_type  = $form_state['values']['list_type'];
    $filters    = $form_state['values']['filters'];
    //$op         = (sizeof($filters) == 1) ? '' : $form_state['values']['filter_list']['apply_filter_op'];

    // Applies the filter.
    $args = array(
      'type'      => 'LIST',
      'bims_user' => $bims_user,
      'list_type' => $list_type,
    );
    if (BIMS_FILTER_LIST::addFormFilter($filters, $form_state['values']['filter_list'], $args)) {
      $form_state['values']['filters'] = $filters;
//      $form_state['values']['choose_list_type']['list_types'] = '';
    }
    else {
      drupal_set_message("Error : Failed to add filter", 'error');
    }
  }

  // If 'view' was clicked.
  else if (preg_match("/^filter_view_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Gets the filters and stores it in the SESSION.
    $filters = $form_state['values']['filters'];
    $_SESSION['SESSION_FILTER'] = array('filter' => $filters[$idx]);

    // View the list.
    $tab = ucfirst($filters[$idx]['list_type']) . ' List';
    $url = "bims/load_main_panel/bims_panel_add_on_list_view/SESSION_FILTER";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_list_view', $tab, $url)));
  }

  // If 'Remove' was clicked.
  else if (preg_match("/^filter_remove_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Updates the filters.
    $filters  = $form_state['values']['filters'];
    array_splice($filters, $idx, 1);
    BIMS_FILTER::updateFilters($filters);
    $form_state['values']['filters'] = $filters;
  }

  // If 'Reset' was clicked.
  else if ($trigger_elem == 'reset_btn') {

    // Resets list types.
    $form_state['values']['choose_list_type']['list_types'] = '';

    // Updates panels.
    $url = 'bims/load_main_panel/bims_panel_add_on_list_add';
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_list_add', 'Add List', $url)));
  }
  else if ($trigger_elem == 'close_btn') {

    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_list_add')));
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
  }
}

/**
 * Theme function for the filters table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_list_add_form_filter_table($variables) {
  $element = $variables['element'];

  // Lists the filters.
  $rows = array();
  $idx = 1;
  while (array_key_exists($idx, $element)) {
    $filter = $element[$idx]['filter']['#value'];

    // Adds a row.
    $rows []= array(
      ucfirst($filter['list_type']),
      $filter['count'],
      drupal_render($element[$idx]['remove']) .
      drupal_render($element[$idx]['view']),
    );
    $idx++;
  }

  // Adds the headers.
  $headers = array(
    array('data' => 'Category', 'width' => 200),
    array('data' => '#', 'width' => 30),
    array('data' => 'Actions', 'width' => 140),
  );

  // Creates the table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'empty'       => 'Please add a list',
    'param'       => array(),
  );
  return bims_theme_table($table_vars);;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_list_add_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "choose_filter".
  $layout .= "<div style='float:left;width:20%;'>" . drupal_render($form['choose_list_type']) . "</div>";

  // Adds "filter_list".
  $layout .= "<div style='float:left;width:34%;margin-left:5px;'>" . drupal_render($form['filter_list']) . "</div>";

  // Adds "filter_result".
  $layout .= "<div style='float:left;width:45%;margin-left:5px;'>" . drupal_render($form['lists']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}
