<?php
/**
 * @file
 */
/**
 * Manage List panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_list_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 420;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets BIMS_LIST.
  $bims_list = NULL;
  if (isset($form_state['values']['list_list']['selection'])) {
    $list_id = $form_state['values']['list_list']['selection'];
    $bims_list = BIMS_LIST::byID($list_id);
  }

  // Gets list_type.
  // Gets the selected list type.
  $list_type = '';
  $list_user = '';
  if (array_key_exists('values', $form_state)) {
    if (isset($form_state['values']['list_list']['list_types'])) {
      $list_type = $form_state['values']['list_list']['list_types'];
    }
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_list');
  if (!$bims_panel->init($form, TRUE)) {
    return $form;
  }

  // Adds the admin menu.
  $form['list_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'List Admin Menu',
    '#attributes'   => array(),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_list_add";
  $form['list_admin']['menu']['add_btn'] = array(
    '#type'       => 'button',
    '#name'       => 'add_btn',
    '#value'      => 'Create',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_list_add', 'Add List', '$url'); return (false);",
    )
  );
  $url = "bims/load_main_panel/bims_panel_add_on_list_merge";
  $form['list_admin']['menu']['merge_btn'] = array(
    '#type'       => 'button',
    '#name'       => 'merge_btn',
    '#value'      => 'Merge',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_list_merge', 'Merge List', '$url'); return (false);",
    )
  );
  $form['list_admin']['menu']['#theme'] = 'bims_panel_main_list_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['list_admin']);
  hide($form['list_admin']);

  // Lists the all lists.
  $form['list_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Lists',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Filters lists by types.
  $options = BIMS_LIST::getListTypes();
  $options = array_merge(array('' => 'all types of List'), $options);
  $form['list_list']['list_types']= array(
    '#type'           => 'select',
    '#options'        => $options,
    '#default_value'  => $list_type,
    '#attributes'     => array('style' => 'width:150px;background-color:#FFFFFF;'),
    '#ajax'           => array(
      'callback' => "bims_panel_main_list_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-list-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  hide($form['list_list']['list_types']);

  // List selection.
  $args = array(
    'type'        => $list_type,
    'user_id'     => $bims_user->getUserID(),
    'program_id'  => $bims_program->getProgramID(),
    'shared'      => TRUE,
    'flag'        => BIMS_OPTION_DEPTH,
  );
  $options = BIMS_LIST::getLists($args);
  if (empty($options)) {
    $form['list_list']['no_list'] = array(
      '#markup' => "No $list_type list is found in this program.",
    );
  }
  else {
    $form['list_list']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#validated'      => TRUE,
      '#size'           => 12,
      '#default_value'  => '',
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_list_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-list-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // List details.
  $form['list_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'List Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  if ($bims_list) {
    $form['list_details']['table']['delete_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'delete_btn',
      '#value'      => 'Delete',
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_list_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-list-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['list_details']['table']['edit_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'edit_btn',
      '#value'      => 'Edit',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_list_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-list-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['list_details']['table']['view_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'view_btn',
      '#value'      => 'View',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_list_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-list-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $value = $bims_list->getShared() ? 'Unshare' : 'Share';
    $form['list_details']['table']['share_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'share_btn',
      '#value'      => $value,
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_list_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-list-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['list_details']['table']['download_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'download_btn',
      '#value'      => 'Download',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_list_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-list-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    if ($bims_list->canStats()) {
      $status = $bims_list->getPropByKey('stats_status');
      if ($status == 'computed') {
        $form['list_details']['table']['stats_btn'] = array(
          '#markup' => '<em>The statistics has been calculated</em>',
        );
      }
      else if ($status == 'computing') {
        $form['list_details']['table']['stats_btn'] = array(
          '#markup' => '<em>The statistics is being computed ...</em>',
        );
      }
      else {
        $form['list_details']['table']['stats_btn'] = array(
          '#type'         => 'submit',
          '#name'         => 'stats_btn',
          '#value'        => 'Calc Stats',
          '#description'  => 'Calculate the statistics fo this list',
          '#attributes'   => array('style' => 'width:120px;'),
          '#ajax'         => array(
            'callback' => "bims_panel_main_list_form_ajax_callback",
            'wrapper'  => 'bims-panel-main-list-form',
            'effect'   => 'fade',
            'method'   => 'replace',
          ),
        );
      }
    }

    // Hides some buttons.
    if ($bims_list->getUserID() != $bims_user->getUserID()) {
      hide($form['list_details']['table']['delete_btn']);
      hide($form['list_details']['table']['edit_btn']);
      hide($form['list_details']['table']['share_btn']);
      hide($form['list_details']['table']['stats_btn']);
    }
  }
  $form['list_details']['table']['bims_list'] = array(
    '#type'   => 'value',
    '#value'  => $bims_list,
  );
  $form['list_details']['table']['#theme'] = 'bims_panel_main_list_form_list_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-list-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_list_form_submit';
  $form['#theme'] = 'bims_panel_main_list_form';
  return $form;
}

/**
 * Deletes the list.
 *
 * @param integer $list_id
 *
 * @return boolean
 */
function _bims_panel_main_list_form_delete_list($list_id) {

  $transaction = db_transaction();
  try {
    $bims_list = BIMS_LIST::byKey(array('list_id' => $list_id));
    if (!$bims_list->delete()) {
      throw new Exception("Error : Failed to delete the list");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_list_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets BIMS_LIST.
  $bims_list = $form['list_details']['table']['bims_list']['#value'];
  if ($bims_list) {
    $list_id = $bims_list->getListID();

    // If "Delete" was clicked.
    if ($trigger_elem == 'delete_btn') {

      // Deletes the list.
      _bims_panel_main_list_form_delete_list($list_id);

      // Updates panels.
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_list')));
    }

    // If 'Edit' was clicked.
    else if ($trigger_elem == 'edit_btn') {

      // Edit the list.
      $url = "bims/load_main_panel/bims_panel_add_on_list_edit/$list_id";
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_list_edit', 'Edit List', $url)));
    }

    // If 'Delete' was clicked.
    if ($trigger_elem == 'delete_btn') {

      // Deletes the current list.
      if (!$bims_list->delete()) {
        drupal_set_message("Error : Failed to delete the list", 'error');
      }
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_list')));
    }

    // If "View" was clicked.
    else if ($trigger_elem == 'view_btn') {


      // Adds view list panel.
      $type = ucfirst($bims_list->getType());
      $url = "bims/load_main_panel/bims_panel_add_on_list_view/$list_id";
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_list_view', "$type List", $url)));
    }

    // If "Share" was clicked.
    else if ($trigger_elem == 'share_btn') {

      // Gets the share status.
      $share_value  = $form['list_details']['table']['share_btn']['#value'];
      if ($share_value == 'Share') {
        $bims_list->share();
        $form['list_details']['table']['share_btn']['#value'] = 'Unshare';
      }
      else {
        $bims_list->unshare();
        $form['list_details']['table']['share_btn']['#value'] = 'Share';
      }
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_list_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_list_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets values from the form_state.
  $bims_list = $form_state['values']['list_details']['table']['bims_list'];

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Download' was clicked.
  if ($trigger_elem == 'download_btn') {

    // Creates a downloading file.
    $filter = $bims_list->getPropByKey('filter');
    $file = BIMS_LIST::saveContents($bims_user, $filter, array('file_type' => 'csv'));
    if ($file) {

      // Redirects to the download link.
      $url = 'bims/download_file/' . $file->fid;
      bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
    }
    else {
      BIMS_MESSAGE::addMsg("Error : Failed to create a downloading file", 'error');
    }
  }

  // If "Add stats" was clicked.
  else if ($trigger_elem == 'stats_btn') {

    // Gets list ID.
    $list_id = $form_state['values']['list_list']['selection'];
    if ($list_id) {

      // Gets BIMS_LIST.
      $bims_list = BIMS_LIST::byID($list_id);
      $bims_list->setPropByKey('stats_status', 'computing');
      if ($bims_list->update()) {

        // Submits the drush command to compute the stats of the list.
        $drush = bims_get_config_setting('bims_drush_binary');
        $cmd = "$drush bims-compute-list-stats $list_id > /dev/null 2>/dev/null & echo $!";
        $pid = exec($cmd, $output, $return_var);
        if ($return_var) {
          drupal_set_message("Error : Failed to submit to compute the stats", 'error');
        }
      }
    }
  }
}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_list_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), 'Create a new list.');
  $rows[] = array(drupal_render($element['merge_btn']), 'Merge lists.');
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the list information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_list_form_list_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_LIST.
  $bims_list = $element['bims_list']['#value'];

  // Initializes the properties.
  $name         = '--';
  $type         = '--';
  $create_date  = '--';
  $owner        = '--';
//  $shared       = '--';
  $desc         = '--';
  $list_row     = '--';
  $actions      = '';
  $count_label  = '';
  $num          = '';
  $extra_rows   = '';
  $stats        = '';


  // Updates the properites.
  if ($bims_list) {
    $name         = $bims_list->getName();
    $type         = $bims_list->getType();
    $desc         = "<textarea cols=50 rows=3 READONLY>" . $bims_list->getDescription() . "</textarea>";
//    $shared       = $bims_list->getShared() ? '<em>Yes</em>' : '<em>No</em>';
    $create_date  = $bims_list->getCreateDate();

    // Gets the owner.
    $bims_owner = BIMS_USER::byID($bims_list->getUserID());
    if ($bims_owner) {
      $owner = $bims_owner->getName();
    }

    // Adds buttons to the list row.
    $list_row = '';
    if (array_key_exists('download_btn', $element)) {
      $list_row .= drupal_render($element['download_btn']);
    }
    if (array_key_exists('view_btn', $element)) {
      $list_row .= drupal_render($element['view_btn']);
    }

    // Adds buttons to the action row.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }
    if (array_key_exists('share_btn', $element)) {
//      $actions .= drupal_render($element['share_btn']);
    }

    // Adds stats row.
    if (array_key_exists('stats_btn', $element)) {
      $stats = drupal_render($element['stats_btn']);
    }

    // Gets the number of the list items.
    $filter       = $bims_list->getPropByKey('filter');
    $count_label  = '# ' . ucfirst($filter['list_type']);
    $num          = $filter['count'];

    // Adds the list specific extra rows.
    $extra_rows = $bims_list->getPropByKey('extra_rows');
    // TODO:
  }

  // Adds the list info table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'style' => 'width:100px;'), $name);
  $rows[] = array('Type', ucfirst($type));
  if ($count_label) {
    $rows[] = array($count_label, $num);
  }
  if ($extra_rows) {
    $rows[] = $extra_rows;
  }
  $rows[] = array('Owner', $owner);
  $rows[] = array('Created Date', $create_date);
  $rows[] = array('Shared', $shared);
  if ($stats) {
    $rows[] = array('statistics', $stats);
  }
  $rows[] = array('Description', $desc);
  $rows[] = array('List', $list_row);
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }

  // Creates the details table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the list form.
 *
 * @param $variables
 */
function theme_bims_panel_main_list_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  $layout.= "<div style='float:left;width:100%;'>" . drupal_render($form['list_admin']) . '</div>';

  // Adds "Lists" list.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['list_list']) . "</div>";

  // Adds "Manage Location".
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['list_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}