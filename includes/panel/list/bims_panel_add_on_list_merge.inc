<?php
/**
 * @file
 */
/**
 * List merge panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_merge_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_CROP.
  $bims_user  = getBIMS_USER();
  $bims_crop  = $bims_user->getCrop();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_list_merge');
  $bims_panel->init($form);

  // Lists the all lists.
  $form['list_list'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Lists',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  /*
  $form['list_list']['list_types'] = array(
    '#type'           => 'textfield',
    '#name'           => 'list_name',
    '#title'          => 'List name',
    '#default_value'  => $bims_list->getName() ,
    '#description'    => t("Please provide a list name. Please use alphabets, dash and underscore"),
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['list_list']['lists'] = array(
    '#type'       => 'select',
    '#name'       => 'list_desc',
    '#value'      => $bims_list->getDescription(),
    '#title'      => 'Description of the list',
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:250px;'),
  );
    $form['list_list']['list_name'] = array(
    '#type'           => 'textfield',
    '#name'           => 'list_name',
    '#title'          => 'List name',
    '#default_value'  => $bims_list->getName() ,
    '#description'    => t("Please provide a list name. Please use alphabets, dash and underscore"),
    '#attributes'     => array('style' => 'width:400px;'),
  );
  */
  $form['list_list']['merge_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'merge_btn',
    '#value'      => 'Merge',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_list_merge_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-merge-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['list_list']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_list_merge_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-merge-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-list-merge-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_list_merge_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_merge_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_merge_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Merge" was clicked.
  if ($trigger_elem == 'merge_btn') {


    // Checks the list name for empty or alpha-numeric, dash and underscore.
//    if (!preg_match("/^[a-zA-z0-9_\-]+$/", $list_name)) {
//      form_set_error('list_prop][name', "Invalid list name");
 //     return;
//    }


  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_merge_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_list';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Merge" was clicked.
  if ($trigger_elem == 'merge_btn') {

    $transaction = db_transaction();
    try {



   //   if (!$bims_list->update()) {
   //     throw new Exception("Error : Failed to merge the list");
   //   }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Updates the panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_list_merge')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}