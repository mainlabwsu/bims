<?php
/**
 * @file
 */
/**
 * View list panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_view_form($form, &$form_state, $list_id) {

  // Local variables for form properties.
  $min_height = 500;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Gets the selected cvterm ID.
  $cvterm_id = NULL;
  if (isset($form_state['values']['list_list']['list_stats']['trait_list'])) {
    $cvterm_id = $form_state['values']['list_list']['list_stats']['trait_list'];
  }

  // Gets the filter using the $list_id
  $filter     = NULL;
  $bims_list  = NULL;
  if ($list_id == 'SESSION_FILTER') {
    $session_obj = bims_get_session($list_id);
    $filter = $session_obj['filter'];
  }
  else {
    $bims_list = BIMS_LIST::byKey(array('list_id' => $list_id));
    if ($bims_list) {
      $bims_list_id = $list_id;
      $filter = $bims_list->getPropByKey('filter');
    }
  }

  // Sets the list owner.
  $list_owner = FALSE;
  if ($bims_list && ($bims_list->getUserID() == $bims_user->getUserID())) {
    $list_owner = TRUE;
  }

  // Saves BIMS_LIST and $filter.
  $form['bims_list'] = array(
    '#type'   => 'value',
    '#value'  => $bims_list,
  );
  $form['filter'] = array(
    '#type'   => 'value',
    '#value'  => $filter,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_list_view');
  $bims_panel->init($form);

  // Properties of the list.
  $form['list_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => ucfirst($filter['list_type']) . ' List&nbsp;&nbsp;(' . $filter['count'] . ')',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Check if 'filter' exists.
  if (!$filter) {
    $form['list_prop']['no_filter'] = array(
      '#markup' => '<em>Filter can not be found</em>',
    );
    $form['list_list'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    );
    return $form;
  }
  $list_name = ($bims_list) ? $bims_list->getName() : '';
  $form['list_prop']['list_name'] = array(
    '#type'           => 'textfield',
    '#name'           => 'list_name',
    '#title'          => 'List name',
    '#default_value'  => $list_name,
    '#description'    => t('Please type the name of the list'),
    '#attributes'     => array('style' => 'width:250px;'),
  );
  $list_desc = ($bims_list) ? $bims_list->getDescription() : '';
  $form['list_prop']['list_desc'] = array(
    '#type'       => 'textarea',
    '#name'       => 'list_desc',
    '#value'      => $list_desc,
    '#title'      => 'Description of the list',
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:250px;'),
  );
  $form['list_prop']['list_desc'] = array(
    '#type'       => 'textarea',
    '#name'       => 'list_desc',
    '#value'      => $list_desc,
    '#title'      => 'Description of the list',
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:250px;'),
  );

  // Sets the button disabled if no data.
  $no_data = ($filter['count']) ? FALSE : TRUE;

  // Adds 'Save' button.
  if (!$bims_list) {
    $form['list_prop']['save_list_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'save_list_btn',
      '#value'      => 'Save',
      '#disabled'   => $no_data,
      '#attributes' => array('style' => 'width:180px;margin-bottom:20px'),
      '#prefix'     => "<div style='margin-bottom:5px;font-size:13px;'><em>Click 'Save' to save the list</em></div>",
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_list_view_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-list-view-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds 'Download' button.
  $form['list_prop']['download_list_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'download_list_btn',
    '#value'      => 'Download',
    '#disabled'   => $no_data,
    '#attributes' => array('style' => 'width:180px;margin-bottom:20px'),
    '#prefix'     => "<div style='margin-bottom:5px;font-size:13px;'><em>Click 'Download' to download data</em></div>",
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_list_view_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-view-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['list_prop']['close_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'close_btn',
    '#value'      => 'Close',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_list_view_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-list-view-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Lists the items.
  $form['list_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'      => 'Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Adds the description.
  _bims_panel_add_on_list_view_form_get_list_desc($form['list_list'], $filter);

  // Adds the stats.
  _bims_panel_add_on_list_view_form_get_list_stats($form['list_list'], $filter, $list_id, $cvterm_id);

  // Adds the summary.
  _bims_panel_add_on_list_view_form_get_list_summary($form['list_list'], $filter, $list_id);

  // Adds the data tables.
  BIMS_FILTER::showResults($form['list_list'], $filter, $bims_program, 'bims_panel_add_on_list_view', $list_id);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-list-view-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_list_view_form_submit';
  $form['#theme'] = 'bims_panel_add_on_list_view_form';
  return $form;
}

/**
 * Adds the stats.
 *
 * @param array $form
 * @param array $filter
 * @param integer $list_id
 * @param integer $cvterm_id
 */
function _bims_panel_add_on_list_view_form_get_list_stats(&$form, $filter, $list_id, $def_cvterm_id = NULL) {

  // Adds the stats.
  if ($list_id && $list_id != 'SESSION_FILTER') {
    $bims_list = BIMS_LIST::byID($list_id);
    if ($bims_list->hasStats()) {
      $collapsed = ($def_cvterm_id) ? FALSE : TRUE;
      $form['list_stats'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => $collapsed,
        '#collapsible'  => TRUE,
        '#title'        => 'Statistics',
        '#attributes'   => array('style' => 'width:100%;'),
      );

      // Gets the stats.
      $stats = $bims_list->getPropByKey('stats');
      $options = array();
      $def_stats_arr = array();
      foreach ($stats as $cvterm_id => $stats_arr) {
        $options[$cvterm_id] = $stats_arr['name'];
        if (!$def_cvterm_id) {
          $def_cvterm_id = $cvterm_id;
          $def_stats_arr = $stats_arr;
        }
      }
      $form['list_stats']['trait_list'] = array(
        '#type'           => 'select',
        '#options'        => $options,
        '#default_value'  => $def_cvterm_id,
        '#size'           => 9,
        '#empty_options'  => 'No statistical trait found',
        '#attributes'     => array('style' => 'width:100%;'),
        '#ajax'           => array(
          'callback' => "bims_panel_add_on_list_view_form_ajax_callback",
          'wrapper'  => 'bims-panel-add-on-list-view-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['list_stats']['trait_stats'] = array(
        '#markup' => BIMS_STATS::getStatsTable($filter['program_id'], $stats[$def_cvterm_id], $def_cvterm_id),
      );
      $form['list_stats']['#theme'] = 'bims_panel_add_on_list_view_form_list_stats';
    }
  }
}

/**
 * Adds the summary.
 *
 * @param array $form
 * @param array $filter
 * @param integer $list_id
 */
function _bims_panel_add_on_list_view_form_get_list_summary(&$form, $filter, $list_id) {

  // Adds the summary.
  $summary = '';
  if ($summary) {
    $form['list_summary'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Summary',
      '#attributes'   => array('style' => 'width:100%;'),
    );
    $form['list_summary']['summary'] = array(
      '#markup' => $summary,
    );
  }
}

/**
 * Adds the description.
 *
 * @param array $form
 * @param array $filter
 */
function _bims_panel_add_on_list_view_form_get_list_desc(&$form, $filter) {

  // Adds the description.
  $form['list_desc'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Descriptions',
    '#attributes'   => array('style' => 'width:100%;'),
  );

  // Parses the description.
  $desc_arr = json_decode($filter['descs'], TRUE);
  $rows = array();
  foreach ($desc_arr as $idx => $items) {
    foreach ($items as $name => $desc) {
      $str_length = strlen($desc);
      if ($str_length > 60) {
        $desc = "<textarea rows='2' style='width:100%;max-width:600px;'>$desc</textarea>";
      }
      $rows []= array(ucfirst($name), $desc);
    }
  }
  $table_vars = array(
    'header'      => array(array('data' => 'Type', 'width' => '120'), 'Values'),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%'),
    'sticky'      => TRUE,
    'empty'       => 'No description',
    'param'       => array(),
  );
  $form['list_desc']['desc'] = array(
    '#markup' => bims_theme_table($table_vars),
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_view_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_view_form_validate($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' was clicked.
  if ($trigger_elem == 'save_list_btn') {

    // Gets the form values.
    $form_state['values']['list_prop']['list_name'] = trim($form_state['input']['list_name']);
    $form_state['values']['list_prop']['list_desc'] = trim($form_state['input']['list_desc']);
    $list_name = $form_state['values']['list_prop']['list_name'];
    $filter    = $form_state['values']['filter'];
    $bims_list = $form_state['values']['bims_list'];

    // Checks list name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-]+$/", $list_name)) {
      $msg = 'Invalid list name. Please type valid name. ' .
             'Valid name : non-empty, alpha-numeric, dash and underscore.';
      form_set_error('list_prop][list_name', $msg);
      return;
    }

    // Gets the current list name and list type.
    $cur_list_name = '';
    $list_type = $filter['list_type'];
    if ($bims_list) {
      $cur_list_name  = $bims_list->getName();
      $list_type      = $bims_list->getType();
    }

    // Checks for a duplication if list names are different.
    if ($cur_list_name != $list_name) {
      $keys = array(
        'type'        => $list_type,
        'program_id'  => $bims_user->getProgramID(),
        'user_id'     => $bims_user->getUserID(),
        'name'        => $list_name,
      );
      $bims_list = BIMS_LIST::byKey($keys);
      if ($bims_list) {
        form_set_error('list_prop][list_name', "The list name ($list_name) has been used. Please choose other name.");
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_list_view_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_list';

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the form values.
  $list_name  = trim($form_state['values']['list_prop']['list_name']);
  $list_desc  = trim($form_state['values']['list_prop']['list_desc']);
  $filter     = $form_state['values']['filter'];
  $bims_list  = $form_state['values']['bims_list'];
  $list_id    = 'SESSION_FILTER';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' was clicked.
  if ($trigger_elem == 'save_list_btn') {

    // Sets the properties of the list.
    $details = array(
      'name'        => $list_name,
      'type'        => $filter['list_type'],
      'program_id'  => $bims_user->getProgramID(),
      'user_id'     => $bims_user->getUserID(),
      'create_date' => date("Y-m-d G:i:s"),
      'description' => $list_desc,
      'prop'        => json_encode(array('filter' => $filter)),
    );

    // Saves the list.
    $bims_list = new BIMS_LIST($details);
    if ($bims_list->insert()) {
      $list_id = $bims_list->getListID();
      drupal_set_message("The search results have been saved in your account");
      bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_list_view')));
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_list')));
    }
    else {
      drupal_set_message("Error : Failed to save the list", 'error');
    }
  }

  // If 'Download' was clicked.
  else if ($trigger_elem == 'download_list_btn') {

    // Creates a downloading file.
    $file = BIMS_LIST::saveContents($bims_user, $filter, array('file_type' => 'csv'));
    if ($file) {

      // Redirects to the download link.
      $url = 'bims/download_file/' . $file->fid;
      bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
    }
    else {
      drupal_set_message("Error : Failed to create a downloading file", 'error');
    }
  }

  // If any button except 'Close' was clicked, refreshes this view page.
  if ($trigger_elem == 'close_btn') {
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_list_view')));
  }
  else {

    // Refreshes this panel.
    $url = "bims/load_main_panel/bims_panel_add_on_list_view/$list_id";
    $title = ucfirst($filter['list_type']) . " List";
    //bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_list_view', $title, $url)));
  }
}

/**
 * Theme function for the list stats.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_list_view_form_list_stats($variables) {
  $element = $variables['element'];

   // Adds the trait list.
  $layout = "<div style='float:left;width:40%;'>" . drupal_render($element['trait_list']) . "</div>";

  // Adds the trait stats.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;margin-top:-8px;'>" . drupal_render($element['trait_stats']) . "</div>";
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_list_view_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "list_prop".
  $layout .= "<div style='float:left;width:27%;'>" . drupal_render($form['list_prop']) . "</div>";

  // Adds "list_list".
  $layout .= "<div style='float:left;width:72%;margin-left:5px;'>" . drupal_render($form['list_list']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
