<?php
/**
 * @file
 */
/**
 * About BIMS form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_about_form($form, &$form_state, $type) {

  // Checks if this user is an anonymous.
  $bims_user = getBIMS_USER();
  $is_admin = FALSE;
  if ($bims_user) {
    $is_admin = $bims_user->isBIMSAdmin();
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Check if it is in the edit mode.
  $edit_mode = FALSE;
  if (preg_match("/-edit$/", $type)) {
    $type = str_replace('-edit', '', $type);
    $edit_mode = TRUE;
  }

  // Gets BIMS_INFO.
  $bims_info = BIMS_INFO::byName($type);
  if (!$bims_info) {
    $form['about'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'BIMS About',
      '#attributes'   => array('style' => 'min-height:340px;padding:0px;'),
      '#description'  => "Error : Unknown type ($type)",
    );
    return $form;
  }

  // Saves the variables.
  $form['bims_info'] = array(
    '#type'   => 'value',
    '#value'  => $bims_info,
  );
  $form['type'] = array(
    '#type'   => 'value',
    '#value'  => $type,
  );

  // BIMS about page.
  $form['about'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => $bims_info->getLabel(),
    '#attributes'   => array('style' => 'min-height:340px;padding:0px;'),
  );
  if ($type == 'who_is_using') {
    _bims_panel_add_on_about_form_who_is_using_page($form['about']);
  }
  else {
    _bims_panel_add_on_about_form_general_page($form['about'], $bims_info, $is_admin, $edit_mode);
  }
  $btn_label = ($edit_mode) ? 'Cancel' : 'Close';
  $btn_name  = ($edit_mode) ? 'cancel_btn' : 'close_btn';
  $form['about']['close_btn'] = array(
    '#type'       => 'submit',
    '#name'       => $btn_name,
    '#value'      => $btn_label,
    '#attributes' => array('style' => 'width: 120px;margin-top:20px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_about_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-about-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-about-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_about_form_submit';
  $form['#theme'] = 'bims_panel_add_on_about_form';
  return $form;
}

/**
 * Creates 'Who is using BIMS' page.
 *
 * @param array $form
 */
function _bims_panel_add_on_about_form_who_is_using_page(&$form) {

  // Gets and sets the default google map variables.
  $bims_map       = new BIMS_MAP();
  $def_latitude   = $bims_map->getDefLatitude();
  $def_longitude  = $bims_map->getDefLongitude();
  $def_zoom       = $bims_map->getDefZoom();
  $def_icon       = $bims_map->getDefIcon();
  $map_img_path   = $bims_map->getMapImgPath();

  // Adds all sites as markers.
  $markers  = array();
  $rows     = array();
  $sites    = BIMS_SITE::getSites(BIMS_OBJECT);
  foreach ($sites as $site) {
    $marker_id  = $site->site_id;
    $label      = $site->label;
    $latitude   = $site->latitude;
    $longitude  = $site->longitude;
    $type       = $site->type;
    $notes      = $site->notes;

    // Adds the site to the table.
    $rows []= array(
      $label,
      $type,
      $latitude,
      $longitude,
      "<a href='javascript:void(0);' onclick='bims.map_focus(\"bims_map_site\", $marker_id, 11); return (false);'>Locate</a>"
    );

    // Adds the site to the map.
    $img_filepath = "$map_img_path/" . $site->icon;
    if (!$site->icon || !file_exists($img_filepath)) {
      $img_filepath = "$map_img_path/$def_icon";
    }
    $markers []= array(
      'marker_id' => $marker_id,
      'latitude'  => $latitude,
      'longitude' => $longitude,
      'icon'      => $img_filepath,
      'content'   => "<b>$label</b><br/><em>$type</em><br/>$notes",
    );
  }

  // Adds the parameters for the map.
  $params = array(
    'latitude'  => $def_latitude,
    'longitude' => $def_longitude,
    'zoom'      => $def_zoom,
    'markers'   => $markers,
  );
  drupal_add_js(array('bims_map_params' => json_encode($params)), 'setting');

  // Adds google map.
  $form['map'] = array(
    '#markup' => '<div id="bims_map_site" style="width:100%;max-height:400px;min-height:400px;"></div>',
  );

  // Lists all users in the table.
  $header = array('Label', 'Type', 'Latitude', 'Longitude', 'Action');
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  $form['user_table'] = array(
    '#markup' => $table = bims_theme_table($table_vars),
  );
}

/**
 * Creates a general BIMS about page.
 *
 * @param array $form
 * @param BIMS_INFO $bims_info
 * @param boolean $is_admin
 * @param boolean $edit_mode
 */
function _bims_panel_add_on_about_form_general_page(&$form, $bims_info, $is_admin, $edit_mode) {

  // Adds the page contents.
  $description = $bims_info->getPropByKey('description');
  $desc = ($description) ? $description : 'Description coming soon...';
  $form['description'] = array(
    '#markup' => "<div style='margin:30px 0px 10px 5px;'>$desc</div>",
  );

  // Adds the editor.
  if ($is_admin) {
    if ($edit_mode) {
/*
    '#type' => 'text_format',
    '#format' => 'full_html',
*/
      $form['description'] = array(
        '#type'           => 'textarea',
        '#title'          => 'Please type description',
        '#rows'           => 5,
        '#default_value'  => $description,
      );
      $form['update_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'update_btn',
        '#value'      => 'Update',
        '#attributes' => array('style' => 'width: 120px;margin-top:20px;'),
        '#ajax'       => array(
          'callback' => 'bims_panel_add_on_about_form_ajax_callback',
          'wrapper'  => 'bims-panel-add-on-about-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
    else {
      $form['edit_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'edit_btn',
        '#value'      => 'Edit',
        '#attributes' => array('style' => 'width: 120px;margin-top:20px;'),
        '#ajax'       => array(
          'callback' => 'bims_panel_add_on_about_form_ajax_callback',
          'wrapper'  => 'bims-panel-add-on-about-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
  }
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_about_form_ajax_callback($form, &$form_state) {}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_about_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_about_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the variables.
  $bims_info  = $form_state['values']['bims_info'];
  $type       = $form_state['values']['type'];

  // Set the edit mode.
  $edit_mode = FALSE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_about/$type" . '-edit';
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_about', 'BIMS About', $url)));
  }

  // If the 'Update' was clicked.
  else if ($trigger_elem == 'update_btn') {

    // Updates the description.
    $desc = $form_state['values']['about']['description'];
    $bims_info->setPropByKey('description', $desc);
    if (!$bims_info->update()) {
      drupal_set_message("Error : Failed to update the description", 'error');
    }
    $url = "bims/load_main_panel/bims_panel_add_on_about/$type";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_about', 'BIMS About', $url)));
  }

  // If the 'Cancel' was clicked.
  else if ($trigger_elem == 'cancel_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_about/$type";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_about', 'BIMS About', $url)));

  }
  else {
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_about')));
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_about_form($variables) {
  $form = $variables['form'];

  // Adds "breed_line_tree".
  $layout = "<div style='width:100%;'>" . drupal_render($form['about']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
