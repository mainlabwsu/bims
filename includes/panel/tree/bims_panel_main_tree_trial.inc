<?php
/**
 * @file
 */
/**
 * Trial tree panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_trial_form($form, &$form_state, $type = NULL) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_tree_trial');
  $bims_panel->init($form);

  // Adds "Trials".
  $form['trial_tree'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Trials',
    '#attributes'   => array('style' => 'min-height:340px;padding:0px;'),
  );

  // Displaty the trial tree.
  $form['trial_tree']['tree'] = array(
    '#markup' => "<div id='BIMS-TREE-RW' data-code='$program_id:RO' style='margin-top:0px;margin-left:0px;'></div>",
  );

  if (0) {
    $form['trial_tree']['update_trial_btn'] = array(
      '#id'         => 'bims-panel-update-trial-btn',
      '#type'       => 'submit',
      '#name'       => 'update_trial_btn',
      '#value'      => 'Update',
      '#attributes' => array('style' => 'width:130px;margin-top:18px'),
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_tree_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-tree-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds action buttons.
  if ($type == 'DOWNLOAD') {
    $form['trial_tree']['download_btn']= array(
      '#id'         => 'bims-panel-main-tree-trial-download-btn',
      '#type'       => 'submit',
      '#name'       => 'download_btn',
      '#value'      => t('Download'),
      '#prefix'     => '<br />',
      '#attributes' => array('style' => 'width:180px;', 'onclick' => 'return (false);'),
    );
    $form['trial_tree']['reset_btn']= array(
      '#id'         => 'bims-panel-tree-main-trial-reset-btn',
      '#type'       => 'button',
      '#value'      => t('Reset'),
      '#attributes' => array('style' => 'width:180px;', 'onclick' => 'return (false);'),
    );
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-tree-trial-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_tree_trial_form_submit';
  $form['#theme'] = 'bims_panel_main_tree_trial_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_trial_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_trial_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_trial_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_tree_trial_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "trial_tree".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['trial_tree']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
