<?php
/**
 * @file
 */
/**
 * Cross tree panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_cross_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_tree_cross');
  $bims_panel->init($form);

  // Adds cross tree.
  $form['cross_tree'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Cross',
    '#attributes'   => array('style' => 'min-height:340px;padding:0px;'),
  );

  // Displaty the cross tree.
  $form['cross_tree']['tree'] = array(
    '#markup' => "<div id='BIMS-CT' data-code='$program_id' style='margin-top:0px;margin-left:0px;'></div>",
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-tree-cross-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_tree_cross_form_submit';
  $form['#theme'] = 'bims_panel_main_tree_cross_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_cross_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_cross_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tree_cross_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_tree_cross_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "cross_tree".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['cross_tree']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
