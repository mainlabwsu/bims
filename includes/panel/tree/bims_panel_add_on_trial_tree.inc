<?php
/**
 * @file
 */
/**
 * Trial Tree panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_tree_form($form, &$form_state, $program_id) {

  // Gets BIMS_USER. and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_trial_tree');
  $bims_panel->init($form);

  // Property form elements.
  $form['tree'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Trial Tree',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );

  // Trial tree.
  $style = 'border:1px solid lightgray;min-height:300px;max-height:500px;overflow-y:auto;margin:0px 0px 10px 5px;;padding:10px 0px 10px 0px;';
  $form['tree']['tree'] = array(
    '#markup' => "<div id='BIMS-TREE-RO' data-code='$program_id:RO' style='$style'></div>",
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-trial-tree-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_trial_tree_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_tree_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_tree_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_tree_form_submit($form, &$form_state) {}