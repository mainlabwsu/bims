<?php
/**
 * @file
 */
/**
 * GPT : Adds a B-Data panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_add_bdata_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'tool_gpt_add_bdata');
  $bims_panel->init($form);

  // Properties of B-DATA.
  $form['bdata_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Add B-DATA',
    '#attributes'   => array(),
  );
  $form['bdata_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Name'),
    '#description'  => t("Please provide a name for B-DATA."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $options = $bims_program->getDescriptors('STATABLE', BIMS_OPTION);
  if (empty($options)) {
    $form['bdata_prop']['error'] = array(
     '#markup' => '<div><em>There is no trait associated with this program.</div>',
    );
    return $form;
  }
  $form['bdata_prop']['upload']['browse'] = array(
    '#type'               => 'managed_file',
    '#title'              => t('B-DATA file'),
    '#description'        => t('Please upload your B-DATA file'),
    '#upload_location'    => $bims_user->getUserDirURL(),
    '#upload_validators'  => array(
      //'file_validate_extensions' => array('xls xlsx csv gz zip'),
          // Pass the maximum file size in bytes
          //'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
    ),
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_tool_gpt_add_bdata_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-add-bdata-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['bdata_prop']['trait'] = array(
    '#type'         => 'select',
    '#title'        => t('Traits'),
    '#options'      => $options,
    '#description'  => t("Please choose a trait"),
    '#attributes'   => array('style' => 'width:250px;'),
  );
  $form['bdata_prop']['description'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please add a description of B-DATA"),
    '#rows'         => 3,
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['bdata_prop']['add_bdata_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Add B-DATA',
    '#name'       => 'add_bdata_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_tool_gpt_add_bdata_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-add-bdata-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['bdata_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_tool_gpt_add_bdata_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-add-bdata-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-tool-gpt-add-bdata-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_tool_gpt_add_bdata_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_add_bdata_form_ajax_callback(&$form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_add_bdata_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add B-DATA" was clicked.
  if ($trigger_elem == 'add_bdata_btn') {

    // Gets the properties of the B-DATA.
    $name = trim($form_state['values']['bdata_prop']['name']);

    // Checks name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\- ]+$/", $name)) {
      form_set_error('bdata_prop][name', "Invalid name for B-DATA. Please use for alpha-numeric, dash and underscore.");
      return;
    }

    // Checks the name for duplication.
    $keys = array(
      'crop_id' => $bims_user->getCropID(),
      'name'    => $name,
    );
    $bdata = BIMS_BDATA::byKey($keys);
    if ($bdata) {
      form_set_error('bdata_prop][name', t("'$name' has already existed. Please change the name."));
      return;
    }

    // Validates the file.
    $drupal_file  = file_load($form_state['values']['bdata_prop']['upload']['browse']);
    $err = BIMS_BDATA::validateFile($drupal_file);
    if ($err) {
      form_set_error('bdata_prop][upload][browse', t("Error : $err."));
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_add_bdata_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_id = 'bims_panel_main_tool_gpt_config';

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add" was clicked.
  if ($trigger_elem == 'add_bdata_btn') {

    $transaction = db_transaction();
    try {

      // Gets the properties of a new B-DATA.
      $name         = trim($form_state['values']['bdata_prop']['name']);
      $cvterm_id    = $form_state['values']['bdata_prop']['trait'];
      $desc         = trim($form_state['values']['bdata_prop']['description']);
      $drupal_file  = file_load($form_state['values']['bdata_prop']['upload']['browse']);

      // Upload the file.
      if ($drupal_file) {

        // Adds a new B-DATA.
        $details = array(
          'crop_id'     => $bims_user->getCropID(),
          'user_id'     => $bims_user->getUserID(),
          'name'        => $name,
          'cvterm_id'   => $cvterm_id,
          'filename'    => $drupal_file->filename,
          'description' => $desc,
          'status'      => 0,
        );
        $bims_bdata = BIMS_BDATA::addBDATA($details);
        if (!$bims_bdata) {
          throw new Exception("Error : Failed to add a new B-DATA");
        }
        $bdata_id = $bims_bdata->getBdataID();
        $form_id .= "/$bdata_id";

        // Moves the file to 'upload' directory.
        $filepath = BIMS_FILE::moveFileLoc($bims_user, $drupal_file, $bims_bdata->getWorkingDir());

        // Processes the B-DATA.
        $drush = bims_get_config_setting('bims_drush_binary');
        $cmd = "$drush bims-process-bdata $bdata_id > /dev/null 2>/dev/null  & echo $!";
        $pid = exec($cmd, $output, $return_var);
        if ($return_var) {
          throw new Exception("Error : Failed to process the B-DATA");
        }

        // Saves the pid.
        $bims_bdata->setPropByKey('pid', $pid);
        $bims_bdata->update();
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_tool_gpt_add_bdata')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($form_id)));
}

/**
 * Theme function for the form.
 *
 * @param array $variables
 */
function theme_bims_panel_add_on_tool_gpt_add_bdata_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "bdata_prop".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['bdata_prop']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}
