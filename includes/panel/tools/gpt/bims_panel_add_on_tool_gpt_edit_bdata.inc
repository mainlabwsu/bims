<?php
/**
 * @file
 */
/**
 * GPT : Edits a B-Data panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_bdata_form($form, &$form_state, $bdata_id) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'tool_gpt_edit_bdata');
  $bims_panel->init($form);

  // Re-upload the B-DATA.
  $form['bdata_file'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Re-upload B-DATA file',
    '#attributes'   => array(),
    '#description'  => 'Re-upload the B-DATA file.'
  );
  $form['bdata_file']['upload']['browse'] = array(
    '#type'               => 'managed_file',
    '#title'              => t('B-DATA file'),
    '#description'        => t('Please re-upload your B-DATA file'),
    '#upload_location'    => $bims_user->getUserDirURL(),
    '#upload_validators'  => array(
        //'file_validate_extensions' => array('xls xlsx csv gz zip'),
        // Pass the maximum file size in bytes
        //'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
    ),
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_tool_gpt_edit_bdata_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-edit-bdata-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['bdata_file']['reupload_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Re-upload',
    '#name'       => 'reupload_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_tool_gpt_edit_bdata_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-edit-bdata-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  hide($form['bdata_file']);

  // Properties of B-DATA.
  $form['bdata_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Edit B-DATA properties',
    '#attributes'   => array(),
  );

  // Gets BIMS_BDATA.
  $bims_bdata = BIMS_BDATA::byID($bdata_id);
  if (!$bims_bdata) {
    $form['bdata_prop']['error'] = array(
      '#markup' => "<div><em>B-DATA (ID = $bdata_id) could not found.</em></div>",
    );
    return $form;
  }
  $form['bims_bdata'] = array(
    '#type'   => 'value',
    '#value'  => $bims_bdata,
  );
  $form['bdata_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Name'),
    '#default_value'  => $bims_bdata->getName(),
    '#description'  => t("Please provide a name for B-DATA."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $options = $bims_program->getDescriptors('STATABLE', BIMS_OPTION);
  $form['bdata_prop']['trait'] = array(
    '#type'           => 'select',
    '#title'          => t('Traits'),
    '#options'        => $options,
    '#default_value'  => $bims_bdata->getCvtermID(),
    '#description'    => t("Please choose a trait"),
    '#attributes'     => array('style' => 'width:250px;'),
  );
  $form['bdata_prop']['description'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Description'),
    '#description'    => t("Please add a description of B-DATA"),
    '#default_value'  => $bims_bdata->getDescription(),
    '#rows'           => 3,
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['bdata_prop']['update_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#name'       => 'update_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_tool_gpt_edit_bdata_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-edit-bdata-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['bdata_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_tool_gpt_edit_bdata_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-edit-bdata-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-tool-gpt-edit-bdata-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_tool_gpt_edit_bdata_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_bdata_form_ajax_callback(&$form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_bdata_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Update" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets the properties.
    $name = trim($form_state['values']['bdata_prop']['name']);

    // Gets the old name.
    $bims_bdata = $form_state['values']['bims_bdata'];
    $old_name = $bims_bdata->getName();

    // Checks the name only if they are different.
    if ($old_name != $name) {

      // Checks name for empty or alpha-numeric, dash and underscore.
      if (!preg_match("/^[a-zA-z0-9_\- ]+$/", $name)) {
        form_set_error('bdata_prop][name', "Invalid name for B-DATA. Please use for alpha-numeric, dash and underscore.");
        return;
      }

      // Checks the name for duplication.
      $keys = array(
        'crop_id' => $bims_user->getCropID(),
        'name'    => $name,
      );
      $bdata = BIMS_BDATA::byKey($keys);
      if ($bdata) {
        form_set_error('bdata_prop][name', t("'$name' has already existed. Please change the name."));
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_bdata_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_BDATA.
  $bims_bdata = $form_state['values']['bims_bdata'];
  $bdata_id   = $bims_bdata->getBdataID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Update" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Updates the B-DATA.
    $transaction = db_transaction();
    try {

      // Gets the properties of a new B-DATA.
      $name       = trim($form_state['values']['bdata_prop']['name']);
      $cvterm_id  = $form_state['values']['bdata_prop']['trait'];
      $desc       = trim($form_state['values']['bdata_prop']['description']);

      // Sets the properties.
      $bims_bdata->setName($name);
      $bims_bdata->setCvtermID($cvterm_id);
      $bims_bdata->setDescription($desc);
      if (!$bims_bdata->update()) {
        throw new Exception("Error : Failed to update the B-DATA");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_tool_gpt_edit_bdata')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_tool_gpt_config/$bdata_id")));
}

/**
 * Theme function for the form.
 *
 * @param array $variables
 */
function theme_bims_panel_add_on_tool_gpt_edit_bdata_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "bdata_file".
  //$layout .= "<div style='width:100%;'>" . drupal_render($form['bdata_file']) . "</div>";

  // Adds "bdata_prop".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['bdata_prop']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}
