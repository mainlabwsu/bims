<?php
/**
 * @file
 */
/**
 * Tool - Global Prediction Tool form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_form($form, &$form_state, $bdata_id = NULL) {

  // Local variables for form properties.
  $min_height = 370;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // BIMS_BDATA.
  $bims_bdata = NULL;
  if (isset($form_state['values']['bdata_list']['selection'])) {
    $bdata_id = $form_state['values']['bdata_list']['selection'];
    $bims_bdata = BIMS_BDATA::byID($bdata_id);
  }
  if (!$bims_bdata && $bdata_id) {
    $bims_bdata = BIMS_BDATA::byID($bdata_id);
  }

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_tool_gpt');
  $bims_panel->init($form);

  // Environment Weight list.
  $form['bdata_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Environment Weight',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // B-DATA selection.
  $options = BIMS_BDATA::getBData($bims_user->getCropID(), BIMS_OPTION, 100);
  if (empty($options)) {
    $form['bdata_list']['no_bdata'] = array(
      '#markup' => 'No environment weight is associated with the current program.',
    );
  }
  else {
    $form['bdata_list']['selection']= array(
      '#type'           => 'select',
      '#options'        => $options,
      '#default_value'  => $bdata_id,
      '#size'           => 6,
      '#description'    => t("Click an environment weight to see the details"),
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => 'bims_panel_main_tool_gpt_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-tool-gpt-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Shows the EW details table.
    if ($bims_bdata) {
      $form['bdata_list']['table']['env'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => TRUE,
        '#collapsible'  => TRUE,
        '#title'        => 'Environments',
        '#attributes'   => array('style' => 'width:100%;'),
      );
      $form['bdata_list']['table']['env']['list'] = array(
        '#markup' => _bims_panel_main_tool_gpt_form_env_table($bims_bdata),
      );
    }
    $form['bdata_list']['table']['bims_bdata'] = array(
      '#type'   => 'value',
      '#value'  => $bims_bdata,
    );
    $form['bdata_list']['table']['#theme'] = 'bims_panel_main_tool_gpt_form_bdata_table';
  }

  // GPT results.
  $form['bdata_results'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Results of GPT (Global Prediction Tool)',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['bdata_results']['submit_file'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Submit Genotype File',
    '#attributes'   => array('style' => 'min-height:100px;'),
  );
  if ($bims_bdata) {
    $form['bdata_results']['submit_file']['target'] = array(
      '#markup' => _bims_panel_main_tool_gpt_form_target($bims_bdata),
    );
    $form['bdata_results']['submit_file']['upload']['browse'] = array(
      '#type'               => 'managed_file',
      '#title'              => t('Genotype file'),
      '#description'        => t('Please upload your genotype file'),
      '#upload_location'    => $bims_user->getUserDirURL(),
      '#progress_indicator' => 'bar',
      '#upload_validators'  => array(
        'file_validate_extensions' => array('txt csv tab'),
        // Pass the maximum file size in bytes
        //'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
      ),
      '#attributes' => array('style' => 'width:180px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_tool_gpt_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-tool-gpt-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['bdata_results']['submit_file']['submit_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'submit_btn',
      '#value'      => "Run GPT",
      '#attributes' => array('style' => 'width:180px;'),
      '#ajax'   => array(
        'callback' => 'bims_panel_main_tool_gpt_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-tool-gpt-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  else {
    $form['bdata_results']['submit_file']['msg'] = array(
      '#markup' => '<em>Please choose an environment weight.</em>',
    );
  }
  $form['bdata_results']['your_results'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Your Results',
    '#attributes'   => array('style' => 'min-height:110px;max-height:300px;'),
  );
  $form['bdata_results']['your_results']['job_table']= array(
    '#markup' => _bims_panel_main_tool_gpt_form_job_table($bims_user),
  );
  $form['bdata_results']['refresh_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Refresh',
    '#attributes' => array('style' => 'width:140px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_main_tool_gpt_form_ajax_callback',
      'wrapper'  => 'bims-panel-main-tool-gpt-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-tool-gpt-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_tool_gpt_form_submit';
  $form['#theme'] = 'bims_panel_main_tool_gpt_form';
  return $form;
}

/**
 * Returns the target.
 *
 * @param BIMS_BDATA $bims_bdata
 *
 * @return string
 */
function _bims_panel_main_tool_gpt_form_target($bims_bdata) {

  // Gets the target info.
  $name   = $bims_bdata->getName();
  $trait  = MCL_CHADO_CVTERM::byID($bims_bdata->getCvtermID());
  $trait_name = $trait->getName();

   //"<div><em><b>Environment Weight :</b> $name</em></div>",
   $rows = array(array(
     array('data' => '<em><b>Chosen Environment Weight</b></em>', 'width' => 170),
     " $name ($trait_name)",
   ));
   $table_vars = array(
     'header'      => array(),
     'rows'        => $rows,
     'attributes'  => array('style' => 'width:80%;'),
   );
   return theme('table', $table_vars);
}
/**
 * Returns the envirnoment table.
 *
 * @param BIMS_USER $bims_user
 *
 * @return string
 */
function _bims_panel_main_tool_gpt_form_job_table(BIMS_USER $bims_user) {

  // Creates the job table.
  $headers = array(
    array('data' => 'Name', 'style' => 'width:30%;'), 'Trait',
    'Status', 'Submit&nbsp;Date', 'Actions',
  );

  // Gets the info. of the jobs.
  $rows = array();
  $map = array();
  $gpt_jobs = BIMS_JOB_GPT::getJobs($bims_user->getUserID(), BIMS_CLASS);
  foreach ($gpt_jobs as $gpt_job) {

    // Gets the trait name.
    $bims_bdata = BIMS_BDATA::byID($gpt_job->getPropByKey('bdata_id'));
    if (!$bims_bdata) {
      continue;
    }
    $bdata_id   = $bims_bdata->getBdataID();
    $trait_name = '';
    if (array_key_exists($bdata_id, $map)) {
      $trait_name = $map[$bdata_id];
    }
    else {
      $trait = MCL_CHADO_CVTERM::byID($bims_bdata->getCvtermID());
      $trait_name = $trait->getName();
      $map[$bdata_id] = $trait_name;
    }

    // Adds the view link.
    $job_id     = $gpt_job->getJobID();
    $job_name   = $gpt_job->getName();
    $view_url   = "bims/load_main_panel/bims_panel_add_on_tool_gpt_result/$job_id";
    $view_link  = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_tool_gpt_result\",\"GPT Result\",\"$view_url\"); return (false);'>$job_name</a>";

    // Creates 'Delete' link.
    $confirm_attr = array(
      'attributes' => array(
        'id' => 'bims_delete_job_' . $job_id,
        'class' => array('use-ajax', 'bims-confirm'),
      )
    );
    $key = "$job_id-bims_bims_panel_main_tool_gpt";
    $delete_link = l('delete', "bims/ajax_delete_bims_job/$key", $confirm_attr);
    $rows []= array(
      $view_link,
      $trait_name,
      $gpt_job->getStatusLabel(),
      $gpt_job->getSubmitDate(),
      $delete_link,
    );
  }
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'empty'       => '<em>No GPT results found</em>',
    'description' => '',
    'attributes'  => array(),
  );
  $surffix = '<div>Click a name to open the result page.</div>';
  return theme('table', $table_vars) . $surffix;
}

/**
 * Returns the envirnoment table.
 *
 * @param BIMS_BDATA $bims_bdata
 *
 * @return string
 */
function _bims_panel_main_tool_gpt_form_env_table(BIMS_BDATA $bims_bdata) {

  // Creates the environment table.
  $headers = array(
    array('data' => 'Environment', 'style' => 'width:30%;'),
    'Description',
  );
  $env_list = $bims_bdata->getEnvironments(BIMS_OBJECT);
  $rows = array();
  foreach ((array)$env_list as $env) {
    $rows []= array($env->name, $env->description);
  }
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'emtpy'       => '<em>No environment associated with the B-DATA</em>',
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_form_ajax_callback($form, $form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Run" was clicked.
  if ($trigger_elem == 'bdata_list[selection]') {

    // Gets the B-DATA ID.
    $bdata_id = $form['bdata_list']['selection']['#value'];
    $url = "bims_panel_main_tool_gpt/$bdata_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($url)));
  }

  // If "Refresh" was clicked.
  else if ($trigger_elem == 'refresh_btn') {
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_tool_gpt/$bdata_id")));
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Local variables.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Run" was clicked.
  if ($trigger_elem == 'submit_btn') {

    // Gets the form_stat values.
    $bdata_id     = $form_state['values']['bdata_list']['selection'];
    $drupal_file  = file_load($form_state['values']['bdata_results']['submit_file']['upload']['browse']);

    // Adds a GPT job.
    $transaction = db_transaction();
    try {

      // Upload the file.
      if ($drupal_file) {

        // Adds a GPT Job.
        $details = array(
          'name'        => 'GPT-' . date("Y-m-d-Gis"),
          'user_id'     => $bims_user->getUserID(),
          'program_id'  => $bims_user->getProgramID(),
        );
        $bims_job_gpt = BIMS_JOB_GPT::addJob($details);
        if ($bims_job_gpt) {

          // Moves the file to 'upload' directory.
          $filepath = BIMS_FILE::moveFileLoc($bims_user, $drupal_file, $bims_job_gpt->getWorkingDir());
          $bims_job_gpt->setPropBykey('filepath', $filepath);
          $bims_job_gpt->update();

          // Runs the job.
          $drush = bims_get_config_setting('bims_drush_binary');
          $job_id = $bims_job_gpt->getJobID();
          $cmd = "$drush bims-process-job $job_id > /dev/null 2>/dev/null  & echo $!";
          $output = array();
          $pid = exec($cmd, $output, $return_var);
          if ($return_var) {
            throw new Exception("Error : Failed to run the GPT job");
          }

          // Saves the properties and the pid.
          $bims_job_gpt->setPropBykey('bdata_id', $bdata_id);
          $bims_job_gpt->setPropBykey('uploaded', $filepath);
          $bims_job_gpt->setExecID($pid);
          $bims_job_gpt->update();
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_tool_gpt')));
        }
        else {
          drupal_set_message("Error : Failed to submit a GPT job", 'error');
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }
}

/**
 * Theme function for the B-DATA information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_tool_gpt_form_bdata_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_BDATA.
  $bims_bdata= $element['bims_bdata']['#value'];

  // Initializes the properties.
  $name       = '--';
  $owner      = '--';
  $trait_name = '--';
  $file_info  = '--';
  $env_table  = '';
  $actions    = '';
  $desc       = '';

  // Updates the properites.
  if ($bims_bdata) {
    $name = $bims_bdata->getName();
    $desc = $bims_bdata->getDescription();

    // Sets the trait.
    $trait = MCL_CHADO_CVTERM::byID($bims_bdata->getCvtermID());
    $trait_name = $trait->getName();

    // Adds the environments.
    $env_list = implode("\n", array_values($bims_bdata->getEnvironments(BIMS_OPTION)));
    if (!$env_list) {
      $env_list = '<em>No environment associated with this program</em>';
    }

    // Sets the owner.
    $u = BIMS_USER::byID($bims_bdata->getUserID());
    $owner = $u->getName();

    // Sets the file info.
    $file_info = $bims_bdata->getFileInfo('stats');
  }

  // Adds the description of the program.
  $description_ta = "<textarea rows='3' readonly class='bims-description' style='width:100%;'>$desc</textarea>";

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'style' => 'width:90px;'), $name);
  $rows[] = array('Owner', $owner);
  $rows[] = array('Trait', $trait_name);
  $rows[] = array('File Info.', $file_info);
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }
  $rows[] = array('Description', $description_ta);
  if ($env_table) {
    $rows[] = array(array('data' => $env_table, 'colspan' => 2));
  }

  // Adds the environment list.
  $layout = '';
  if (array_key_exists('env', $element)) {
    $layout .= drupal_render($element['env']);
  }
  // Creates the details table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout .= theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_main_tool_gpt_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the list of environment weights.
  $layout .= "<div style='float:left;width:44%;'>";
  $layout .= "<div style='width:100%;'>" . drupal_render($form['bdata_list']) . "</div>";
  $layout .= "</div>";

  // Adds the results table.
  $layout .= "<div style='float:left;width:55%;margin-left:5px;'>" . drupal_render($form['bdata_results']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
 }