<?php
/**
 * @file
 */
/**
 * Tool - Global Prediction Tool Configuration form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_config_form($form, &$form_state, $bdata_id = NULL) {

  // Local variables for form properties.
  $min_height = 300;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // BIMS_BDATA.
  $bims_bdata = NULL;
  if (isset($form_state['values']['bdata_list']['selection'])) {
    $bdata_id = $form_state['values']['bdata_list']['selection'];
  }
  if ($bdata_id != '') {
    $bims_bdata = BIMS_BDATA::byID($bdata_id);
  }

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_tool_gpt_config');
  $bims_panel->init($form);

  // Adds admin menu.
  $form['tool_gpt_config_admin']['menu'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Global Prediction Tool',
    '#attributes'   => array(),
  );
  $url = 'bims/load_main_panel/bims_panel_add_on_tool_gpt_add_bdata';
  $form['tool_gpt_config_admin']['menu']['add_bdata_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Add B-DATA',
    '#attributes' => array(
      'style'   => 'width:140px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_tool_gpt_add_bdata', 'Add B-Data', '$url'); return (false);",
    )
  );
  $form['tool_gpt_config_admin']['menu']['#theme'] = 'bims_panel_main_tool_gpt_config_form_admin_menu_table';

  // B-DATA list.
  $form['bdata_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'B-DATA',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // B-DATA selection.
  $options = BIMS_BDATA::getBData($bims_user->getCropID(), BIMS_OPTION);
  if (empty($options)) {
    $form['bdata_list']['no_bdata'] = array(
      '#markup' => 'No B-DATA is associated with the current program.',
    );
  }
  else {
    $form['bdata_list']['selection']= array(
      '#type'           => 'select',
      '#options'        => $options,
      '#default_value'  => $bdata_id,
      '#size'           => 10,
      '#description'    => t("Click a B-DATA to see the details"),
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => 'bims_panel_main_tool_gpt_config_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-tool-gpt-config-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['bdata_list']['refresh_btn'] = array(
      '#type'       => 'button',
      '#value'      => 'Refresh',
      '#attributes' => array('style' => 'width:140px;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_tool_gpt_config_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-tool-gpt-config-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // B-DATA details.
  $form['bdata_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'B-DATA details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  if ($bims_bdata) {
    $form['bdata_details']['table']['env'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Environments',
      '#attributes'   => array('style' => 'width:100%;'),
    );
    $form['bdata_details']['table']['env']['list'] = array(
      '#markup' => _bims_panel_main_tool_gpt_config_form_env_table($bims_bdata),
    );
  }

  // Adds the action buttons.
  if ($bims_bdata && bims_can_admin_action($bims_user)) {
    $form['bdata_details']['table']['edit_btn'] = array(
      '#type'       => 'button',
      '#value'      => 'Edit',
      '#name'       => 'edit_btn',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_tool_gpt_config_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-tool-gpt-config-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['bdata_details']['table']['delete_btn'] = array(
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#name'       => 'delete_btn',
      '#disabled'   => !$bims_user->canEdit(),
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_tool_gpt_config_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-tool-gpt-config-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Stores $bims_bdata.
  $form['bdata_details']['table']['bims_bdata'] = array(
    '#type'   => 'value',
    '#value'  => $bims_bdata,
  );
  $form['bdata_details']['table']['#theme'] = 'bims_panel_main_tool_gpt_config_form_bdata_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-tool-gpt-config-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_tool_gpt_config_form_submit';
  $form['#theme'] = 'bims_panel_main_tool_gpt_config_form';
  return $form;
}

/**
 * Returns the envirnoment table.
 *
 * @param BIMS_BDATA $bims_bdata
 *
 * @return string
 */
function _bims_panel_main_tool_gpt_config_form_env_table(BIMS_BDATA $bims_bdata) {

  // Creates the environment table.
  $headers = array(
    array('data' => 'Environment', 'style' => 'width:30%;'),
    'Mean', 'Description', 'Action',
  );

  // Popluates the table.
  $format   = $bims_bdata->getFormat();
  $means    = $bims_bdata->getPropByKey('means');
  $env_list = $bims_bdata->getEnvironments(BIMS_OBJECT);
  $rows = array();
  foreach ((array)$env_list as $env) {
    $environment_id = $env->environment_id;
    $env_lc = strtolower($env->name);
    $mean = '--';
    if (array_key_exists($env_lc, $means)) {
      $mean = sprintf($format, $means[$env_lc]);
    }
    $edit_link = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_tool_gpt_edit_environment\",\"Edit Environment\",\"bims/load_main_panel/bims_panel_add_on_tool_gpt_edit_environment/$environment_id\"); return (false);'>Edit</a>";
    $rows []= array($env->name, $mean, $env->description, $edit_link);
  }
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'emtpy'       => '<em>No environment associated with the B-DATA</em>',
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_config_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets bdata ID.
  $bdata_id = $form_state['values']['bdata_list']['selection'];

  // If "Refresh" was clicked.
  if ($trigger_elem == 'refresh_btn') {
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_add_on_tool_gpt_config/$bdata_id")));
  }

  // If "Edit" was clicked.
  else if ($trigger_elem == 'edit_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_tool_gpt_edit_bdata/$bdata_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_main_tool_gpt_config_edit_bdata', 'Edit B-DATA', $url)));
  }

  // If "Delete" was clicked.
  else if ($trigger_elem == 'delete_btn') {

    // Deletes the B-DATA.
    $bims_bdata = BIMS_BDATA::byID($bdata_id);
    if (!$bims_bdata->delete()) {
      drupal_set_message("Error : Failed to delete B-DATA (ID = $bdata_id)", 'error');
    }

    // Updates panels.
    else {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_tool_gpt_config')));
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_config_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_gpt_config_form_submit($form, &$form_state) {}

/**
 * Theme function for the program admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_tool_gpt_config_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  if (array_key_exists('add_bdata_btn', $element)) {
    $rows[] = array(drupal_render($element['add_bdata_btn']), 'Add a new B-Data.');
  }
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
    'empty'       => '<em>No menu</em>',
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the B-DATA information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_tool_gpt_config_form_bdata_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_BDATA.
  $bims_bdata = $element['bims_bdata']['#value'];

  // Initializes the properties.
  $name       = '--';
  $owner      = '--';
  $trait_name = '--';
  $file_link  = '--';
  $status     = '--';
  $file_info  = '--';
  $actions    = '';
  $desc       = '';
  $env_table  = '';

  // Updates the properites.
  if ($bims_bdata) {
    $bdata_id = $bims_bdata->getBdataID();
    $name     = $bims_bdata->getName();
    $status   = $bims_bdata->getStatus();
    $desc     = $bims_bdata->getDescription();

    // Sets the trait.
    $trait = MCL_CHADO_CVTERM::byID($bims_bdata->getCvtermID());
    $trait_name = $trait->getName();

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }

    // Updates the file link.
    $file_link = l('Download', "bims/download_bdata_file/$bdata_id", array('attributes' => array('target' => '_blank')));

    // Sets the owner.
    $u = BIMS_USER::byID($bims_bdata->getUserID());
    $owner = $u->getName();

    // Sets the file info.
    if ($status == 100) {
      $file_info = $bims_bdata->getFileInfo('stats');
    }

    // Sets the status.
    $status = $bims_bdata->getStatusLabel();
  }

  // Adds the description of the program.
  $description_ta = "<textarea rows='2' readonly class='bims-description' style='width:100%;'>$desc</textarea>";

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => '<b>Name</b>', 'style' => 'width:90px;'), $name);
  $rows[] = array('<b>Owner</b>', $owner);
  $rows[] = array('<b>Status</b>', $status);
  $rows[] = array('<b>Uploaded&nbsp;File</b>', $file_link);
  $rows[] = array('<b>Trait</b>', $trait_name);
  $rows[] = array('<b>File Info.</b>', $file_info);
  if ($actions) {
    $rows[] = array('<b>Actions</b>', $actions);
  }
  $rows[] = array('<b>Description</b>', $description_ta);
  if ($env_table) {
    $rows[] = array(array('data' => $env_table, 'colspan' => 2));
  }

  // Creates the details table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);

  // Adds the environment list.
  if (array_key_exists('env', $element)) {
    $layout .= drupal_render($element['env']);
  }
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_main_tool_gpt_config_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('tool_gpt_config_admin', $form)) {
    $layout.= "<div style='float:left;width:100%;'>" . drupal_render($form['tool_gpt_config_admin']) . '</div>';
  }

  // Adds the B-DATA and the environment list.
  $layout .= "<div style='float:left;width:40%;'>";
  $layout .= "<div style='width:100%;'>" . drupal_render($form['bdata_list']) . "</div>";
  $layout .= "<div style='width:100%;'>" . drupal_render($form['env_list']) . "</div>";
  $layout .= "</div>";

  // Adds the  B-DATA info. table.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['bdata_details']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
 }