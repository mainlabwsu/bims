<?php
/**
 * @file
 */
/**
 * Tool - Global Prediction Tool Result form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_result_form($form, &$form_state, $job_id) {

  // Local variables for form properties.
  $min_height = 260;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_tool_gpt_result');
  $bims_panel->init($form);

  // The result details.
  $form['result_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'GPT Result details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets BIMS_JOB.
  $bims_job = BIMS_JOB_GPT::byID($job_id);
  if (!$bims_job) {
    $form['result_details']['error'] = array(
      '#markup' => "<div><em>There is no GPT with ID ($job_id).</em></div>",
    );
    return $form;
  }

  // Gets BIMS_BDATA.
  $bdata_id = $bims_job->getPropByKey('bdata_id');
  $bims_bdata = BIMS_BDATA::byID($bdata_id);
  if (!$bims_bdata) {
    $form['result_details']['error'] = array(
      '#markup' => "<div><em>The environment weight (ID = $bdata_id) does not exist.</em></div>",
    );
    return $form;
  }
  $form['result_details']['table']['delete_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Delete',
    '#name'       => 'delete_btn',
    '#attributes' => array(
      'style' => 'width:120px;',
      'class' => array('use-ajax', 'bims-confirm'),
    ),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_tool_gpt_result_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-result-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['result_details']['table']['bims_job'] = array(
    '#type'   => 'value',
    '#value'  => $bims_job,
  );
  $form['result_details']['table']['#theme'] = 'bims_panel_add_on_tool_gpt_result_form_details_table';
  $form['result_details']['result_table'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Results table',
    '#attributes'   => array(),
  );
  $form['result_details']['result_table']['table'] = array(
    '#markup' => _bims_panel_add_on_tool_gpt_result_form_result_table($bims_job),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-tool-gpt-result-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_tool_gpt_result_form_submit';
  $form['#theme'] = 'bims_panel_add_on_tool_gpt_result_form';
  return $form;
}

/**
 * Returns the result table.
 *
 * @param BIMS_JOB_GPT $bims_job
 *
 * @return string
 */
function _bims_panel_add_on_tool_gpt_result_form_result_table(BIMS_JOB_GPT $bims_job) {

  // Gets the table file.
  $table_file = $bims_job->getResultsFile('table');
  if ($table_file) {
    $table = '<table id="bims_gpt_result" class="display" style="width:100%;">';
    $table .= file_get_contents($table_file);
    $table .= '</table>';
    return "<div style='overflow-y:auto;;max-height:270px;'>$table</div>";
  }
  return "<div><em>The result table file could not be found.</em></div>";
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_result_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the job ID.
  $bims_job = $form['result_details']['table']['bims_job']['#value'];

  // If "Delete" was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Deletes the job.
    if (!$bims_job->delete()) {
      drupal_set_message("Error : Failed to delete the job", 'error');
    }

    // Updates panels.
    else {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_tool_gpt')));
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_result_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_result_form_submit($form, &$form_state) {}

/**
 * Theme function for the B-DATA information table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_tool_gpt_result_form_details_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_JOB.
  $bims_job = $element['bims_job']['#value'];

  // Initializes the properties.
  $name             = '--';
  $status_label     = '--';
  $progress         = '';
  $actions          = '';
  $uploaded_file    = '--';
  $submit_date      = '--';
  $complete_date    = '';
  $generated_files  = '';
  $completed        = FALSE;

  // Updates the properites.
  if ($bims_job) {
    $job_id         = $bims_job->getJobID();
    $name           = $bims_job->getName();
    $submit_date    = $bims_job->getSubmitDate();
    $complete_date  = $bims_job->getCompleteDate();
    $status         = $bims_job->getStatus();
    $status_label   = $bims_job->getStatusLabel();

    // Updates $completed
    if ($status == 100) {
      $completed = TRUE;
    }

    // Updates the progress.
    if ($status < 100) {
      $progress = $bims_job->getProgress();
    }

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }

    // Sets the status.
    $status = $bims_job->getStatusLabel();

    // Updates the file link.
    $uploaded_file = l('Download', "bims/download_job_file/$job_id-uploaded", array('attributes' => array('target' => '_blank')));

    // Adds the list of the generated files.
    $items = array();
    $items[] = l('Results', "bims/download_job_file/$job_id-gpt_results", array('attributes' => array('target' => '_blank')));
    $items[] = l('Log', "bims/download_job_file/$job_id-log_file", array('attributes' => array('target' => '_blank')));
    $generated_files = theme('item_list', array('items' => $items));
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => '<b>Name</b>', 'style' => 'width:90px;'), $name);
  $rows[] = array('<b>Status</b>', $status_label);
  if ($progress) {
    $rows []= array('<b>Progress</b>', $progress);
  }
  $rows[] = array('<b>Uploaded&nbsp;File</b>', $uploaded_file);
  $rows[] = array('<b>Submit&nbsp;Date</b>', $submit_date);
  if ($completed) {
    $rows[] = array('<b>Generated&nbsp;Files</b>', $generated_files);
    $rows[] = array('<b>Completed&nbsp;Date</b>', $complete_date);
  }
  if ($actions) {
    $rows[] = array('<b>Actions</b>', $actions);
  }

  // Creates the details table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:40%;'),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_add_on_tool_gpt_result_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the program list.
  $layout .= "<div style='float:left;width:100%;'>" . drupal_render($form['result_details']) . "</div>";

  // Adds the program info. table.
//  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['result_details']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
 }