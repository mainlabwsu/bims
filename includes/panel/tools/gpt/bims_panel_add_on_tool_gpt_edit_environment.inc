<?php
/**
 * @file
 */
/**
 * GPT : Edit an environment panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_environment_form($form, &$form_state, $environment_id) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'tool_gpt_edit_environment');
  $bims_panel->init($form);

  // Properties of an environment.
  $form['environment_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Edit Environment',
    '#attributes'   => array(),
  );

  // Gets BIMS_ENVIRONMENT.
  $bims_environment = BIMS_ENVIRONMENT::byID($environment_id);
  if (!$bims_environment) {
    $form['environment_prop']['error'] = array(
      '#markup' => "<div><em>The environment (ID = $environment_id) could not found.</em></div>",
    );
    return $form;
  }
  $form['bims_environment'] = array(
    '#type'   => 'value',
    '#value'  => $bims_environment,
  );
  $name = $bims_environment->getName();
  $form['environment_prop']['name'] = array(
    '#markup' => "<b>Name : <em>$name</em></b>",
  );
  $form['environment_prop']['description'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Description'),
    '#default_value'  => $bims_environment->getDescription(),
    '#description'    => t("Please type a description of the environment."),
    '#rows'           => 3,
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['environment_prop']['update_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#name'       => 'update_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_tool_gpt_edit_environment_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-edit-environment-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['environment_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_tool_gpt_edit_environment_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-tool-gpt-edit-environment-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-tool-gpt-edit-environment-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_tool_gpt_edit_environment_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_environment_form_ajax_callback(&$form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_environment_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit Environment" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets the BIMS_ENVIRONMENT.
    $bims_environment = $form_state['values']['bims_environment'];

    // Gets the properties of the environment.
    if (FALSE) {
      $name = trim($form_state['values']['environment_prop']['name']);

      // Checks if the name is different.
      if (strtolower($name) != strtolower($bims_environment->getName())) {

        // Checks name for empty or alpha-numeric, dash and underscore.
        if (!preg_match("/^[a-zA-z0-9_\- ]+$/", $name)) {
          form_set_error('environment_prop][name', "Invalid name for an environment. Please use for alpha-numeric, dash and underscore.");
          return;
        }

        // Checks the name for duplication.
        $keys = array(
          'bdata_id'  => $bims_environment->getBdataID(),
          'name'      => $name,
        );
        $environment = BIMS_ENVIRONMENT::byKey($keys);
        if ($environment) {
          form_set_error('environment_prop][name', t("'$name' has already existed. Please change the name."));
          return;
        }
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_tool_gpt_edit_environment_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the BIMS_ENVIRONMENT.
  $bims_environment = $form_state['values']['bims_environment'];
  $bdata_id = $bims_environment->getBdataID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Update" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Updates the environment.
    $transaction = db_transaction();
    try {

      // Gets the description of the environment.
      $desc = trim($form_state['values']['environment_prop']['description']);

      // Updates the description.
      $bims_environment->setDescription($desc);
      if (!$bims_environment->update()) {
        throw new Exception("Error : Failed to update the environment");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_tool_gpt_edit_environment')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_tool_gpt_config/$bdata_id")));
}

/**
 * Theme function for the form.
 *
 * @param array $variables
 */
function theme_bims_panel_add_on_tool_gpt_edit_environment_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "environment_prop".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['environment_prop']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}
