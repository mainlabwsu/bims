<?php
/**
 * @file
 */
/**
 * Tool - Seedling Select form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_ss_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_tool_ss');
  $bims_panel->init($form);

  // Adds admin menu.
  /*
  $form['tool_ss_admin']['menu'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => !empty($program_options),
    '#collapsible'  => TRUE,
    '#title'        => 'Seeding Select Admin Menu',
    '#attributes'   => array(),
  );*/

  // Seedling Select.
  $form['seedling_select'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Seedling Select',
  );
  $form['seedling_select']['desc'] = array(
    '#markup' => 'Not currently available.',
  );

  $form['#prefix'] = '<div id="bims-panel-main-tool-ss-form-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_tool_ss_form_submit';
  $form['#theme'] = 'bims_panel_main_tool_ss_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_ss_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_ss_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];



}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_tool_ss_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_main_tool_ss_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);



  $layout .= drupal_render_children($form);
  return $layout;
 }