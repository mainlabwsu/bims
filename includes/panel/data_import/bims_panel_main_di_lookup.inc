<?php
/**
 * @file
 */
/**
 * lookup panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_lookup_form($form, &$form_state, $data_type = 'organism') {

  // Local variables for form properties.
  $min_height = 340;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the data type.
  if (array_key_exists('values', $form_state)) {
    $data_type = $form_state['values']['data_type']['selection'];
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_di_lookup');
  $bims_panel->init($form);

  // Sets the data types.
  $options = array(
    'organism'  => 'Organism',
    'contact'   => 'Contact',
  );

  // Shows data types that can be looked up.
  $form['data_type'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Data type - ' . $options[$data_type],
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['data_type']['selection'] = array(
    '#type'           => 'select',
    '#title'          => 'Data Types',
    '#options'        => $options,
    '#default_value'  => $data_type,
    '#attributes'     => array('style' => 'width:180px;margin-bottom:10px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_main_di_lookup_form_ajax_callback',
      'wrapper'  => 'bims-panel-main-di-lookup-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['data_type']['searchbox'] = array(
    '#type'               => 'textfield',
    '#attributes'         => array('style' => 'width:250px;'),
    '#prefix'             => "<div><em>Please type first characters of $data_type</em></div>",
    '#autocomplete_path'  => 'bims/bims_autocomplete_' . $data_type,
  );
  $form['data_type']['details_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'details_btn',
    '#value'      => 'Details',
    '#attributes' => array('style' => 'width:100px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_main_di_lookup_form_ajax_callback',
      'wrapper'  => 'bims-panel-main-di-lookup-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

/*
  $headers = array(
    array('data' => 'Name', 'align' => 'center'),
    array('data' => 'A', 'align' => 'center'),
    array('data' => 'B', 'style' => 'text-align:center'),
    array('data' => 'C', 'style' => 'text-align:center'),
  );


  $options = array();
  $options['1'] = array('WA','0.5','','0.5');
  $options['2'] = array('NY','','1','');
  $options['3'] = array('CA','0.33','0.33','0.33');

  $form['group_table'] = array(
    '#type'           => 'tableselect',
    '#header'         => $headers,
    '#options'        => $options,
    '#empty'          => t('There is no group. Please add a group'),
  );
*/



  // Shows data types that can be looked up.
  $form['data_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['data_details']['table']['#theme'] = 'bims_panel_main_di_lookup_form_detail_table';
  hide($form['data_type']['details_btn']);
  hide($form['data_details']);
  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-di-lookup-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_di_lookup_form_submit';
  $form['#theme'] = 'bims_panel_main_di_lookup_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_lookup_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_lookup_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_lookup_form_submit($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Details" was clicked.
  if ($trigger_elem == 'details_btn') {

    // Gets the data.
    $data_type  = trim($form_state['values']['data_type']['selection']);
    $name       = trim($form_state['values']['data_type']['searchbox']);

    /*
    if ($stock_id) {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_accession/$stock_id")));
    }
    else {
      drupal_set_message("$accession_l '$name' does not exists.");
    }*/
  }
}

/**
 * Theme function for the details table.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_lookup_form_detail_table($variables) {
  $element = $variables['element'];


  $rows = array();

  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_main_di_lookup_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the data type.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['data_type']) . "</div>";

  // Adds the data details.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['data_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
 }