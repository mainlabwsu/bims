<?php
/**
 * @file
 */
/**
 * Data Import (import public data) panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $cv_id
 */
function bims_panel_main_di_data_form($form, &$form_state, $cv_id = NULL) {

  // Local variables for form properties.
  $min_height = 250;

  // Gets BIMS_USER and BIMS__PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_di_data');
  if (!$bims_panel->init($form, TRUE, TRUE)) {
    return $form;
  }

  // Adds import section.
  $form['import'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Import public data',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets the options for chado_cv and bims_imported_cv.
  $opt_chado_cv     = BIMS_IMPORTED_CV::getChadoCV($bims_program, '', BIMS_OPTION);
  $opt_avail_cv     = BIMS_IMPORTED_CV::getUnimportedCV($bims_program, BIMS_OPTION);
  $opt_imported_cv  = BIMS_IMPORTED_CV::getImportedCV($bims_program->getProgramID(), BIMS_OPTION);

  // Adds 'Descriptor' selection. Lists all CVs for the imported phenotyping trials.
  _bims_panel_main_di_data_form_descriptor($form['import'], $bims_program, $opt_chado_cv, $opt_avail_cv, $cv_id);

  // Adds 'Phenotype Trial' selection.
  _bims_panel_main_di_data_form_import_phenotype_trial($form['import'], $bims_program, $opt_imported_cv, $cv_id);

  // Adds 'Genotype Trial' selection.
  _bims_panel_main_di_data_form_import_genotype_trial($form['import'], $bims_program);

  // Adds 'Cross Trial' selection.
  _bims_panel_main_di_data_form_import_cross_trial($form['import'], $bims_program);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-di-data-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_di_data_form_submit';
  $form['#theme'] = 'bims_panel_main_di_data_form';
  return $form;
}

/**
 * Adds 'Descriptor' selection.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param array $opt_chado_cv
 * @param array $opt_imported_cv
 * @param intger $cv_id
 */
function _bims_panel_main_di_data_form_descriptor(&$form, BIMS_PROGRAM $bims_program, $opt_chado_cv, $opt_avail_cv, &$cv_id) {

  $form['descriptor'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Descriptor Groups',
    '#attributes'   => array('style' => 'width:80%'),
  );
  $form['descriptor']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['descriptor']['#theme'] = 'bims_panel_main_di_data_form_descriptor';


  // Gets the imported projects in a table.
  $imported_projects = $bims_program->countImportedProjectsByCV();
  $imported_cvs = array();
  foreach ((array)$imported_projects as $cv_id => $prop) {

    if ($prop['num_proj'] == 0) {
      $form['descriptor']['delete_cv'][$cv_id] = array(
        '#id'    => 'delete_cv_btn_' . $cv_id,
        '#type'  => 'submit',
        '#value' => 'Remove',
        '#name'  => 'delete_cv_' . $cv_id,
        '#ajax'  => array(
          'callback' => 'bims_panel_main_di_data_form_ajax_callback',
          'wrapper'  => 'bims-panel-main-di-data-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
        '#attributes' => array(
          'style' => 'margin-left: 70px',
          'class' => array('bims-confirm'),
        ),
      );
    }
    else {
      $form['descriptor']['delete_cv'][$cv_id] = array(
        '#markup' => '<em>N/A</em>',
      );
    }
    $imported_cvs[$cv_id] = array(
      'cv_name'   => $prop['cv_name'],
      'num_proj'  => $prop['num_proj'],
      'status'    => $prop['status']
    );
  }

  // Descriptor group selection.
  $form['descriptor']['imported_cvs'] = array(
    '#type'   => 'value',
    '#value'  => $imported_cvs,
  );

  // No chado CV found in bims_chado_cv table.
  if (empty($opt_chado_cv)) {
    $form['descriptor']['no_chado_cv'] = array(
      '#markup' => '<div>There is no publically available CV. Please ask the site administrator to add it.</div>',
    );
    return;
  }

  // No available CV to add to a program.
  if (empty($opt_avail_cv)) {
    $form['descriptor']['no_avail_cv'] = array(
      '#markup' => '<div>There is no available descriptor group to add to your program.</div>',
    );
    return;
  }

  // Shows all the available descriptor groups.
  $prefix = "<div style='margin-top:8px;margin-bottom:8px;'>You need to choose a descriptor group from the list below before you import public phenotypic data associated with the chosen descriptor group from Chado.</div>";
  $form['descriptor']['select'] = array(
    '#type'           => 'select',
    '#options'        => $opt_avail_cv,
    '#prefix'         => $prefix,
    '#attributes'     => array('style' => 'width:280px'),
  );
  $form['descriptor']['descriptor_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'descriptor_btn',
    '#value'      => 'Add Descriptor Group',
    '#attributes' => array('style' => 'width:200px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_main_di_data_form_ajax_callback',
      'wrapper'  => 'bims-panel-main-di-data-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
}

/**
 * Adds 'Import Phenotype Trial Data' selection.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param array $opt_imported_cv
 * @param integer $cv_id
 */
function _bims_panel_main_di_data_form_import_phenotype_trial(&$form, BIMS_PROGRAM $bims_program, $opt_imported_cv, $cv_id) {

  // Phenotype Trial selection.
  $form['phenotype_trial'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Import Phenotype Trial Data',
    '#attributes'   => array('style' => 'width:80%'),
  );
  $form['phenotype_trial']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['phenotype_trial']['#theme'] = 'bims_panel_main_di_data_form_phenotype';

  // No imported CV.
  if (empty($opt_imported_cv)) {
    $form['phenotype_trial']['no_imported_cv'] = array(
      '#markup' => '<div>No imported descriptor group found in your program. Please add descriptor groups.</div>',
    );
    return;
  }

  // Lists all the imported CVs.
  $form['phenotype_trial']['imported_cv'] = array(
    '#type'       => 'select',
    '#options'    => $opt_imported_cv,
    '#title'      => 'Imported Descriptor Groups',
    '#attributes' => array('style' => 'width:280px'),
  );

  // No chosen CV.
  if (!$cv_id) {
    $cv_id = bims_get_first_elem($opt_imported_cv, BIMS_ARRAY_KEY);
  }

  // Gets all available trials for the given cv ID.
  $options = BIMS_IMPORTED_PROJECT::getPhenotypeTrial($cv_id, BIMS_OPTION, $bims_program->getProgramID());
  if (empty($options)) {
    $form['phenotype_trial']['no_trial'] = array(
      '#markup' => "<div style='margin-top:20px;'><em>There is no phenotypic trial to import.</em></div>",
    );
  }
  else {

    // Lists the phenotype trials.
    $cv_name = bims_get_first_elem($opt_imported_cv, BIMS_ARRAY_VAL);
    $size = sizeof($options);
    $size = ($size > 10) ? 10 : $size;
    $form['phenotype_trial']['select'] = array(
      '#type'         => 'select',
      '#options'      => $options,
      '#description'  => 'Use Ctl or Shift key to select multiple trials.',
      '#size'         => $size,
      '#title'        => "Phenotypic trials for '$cv_name'",
      '#multiple'     => TRUE,
      '#attributes'   => array('style' => 'width:400px'),
    );
    $form['phenotype_trial']['phenotype_trial_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'phenotype_trial_btn',
      '#value'      => 'Import Trials',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_di_data_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-di-data-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Imported phenotype trial table.
  $trials = $bims_program->getImportedProjByType('phenotype', BIMS_OBJECT);
  $imported_projs = array();
  foreach ((array)$trials as $trial_obj) {

    // Adds 'Remove' button.
    $project_id = $trial_obj->project_id;
    $imported_projs[$project_id] = array(
      'project_name'  => $trial_obj->project_name,
      'cv_name'       => $trial_obj->cv_name,
      'import_date'   => $trial_obj->import_date,
      'status'        => $trial_obj->status,
    );
    if ($trial_obj->status == 'completed') {
      $form['phenotype_trial']['delete_proj_btn'][$project_id] = array(
        '#id'    => 'delete_proj_btn_' . $project_id,
        '#type'  => 'submit',
        '#value' => 'Remove',
        '#name'  => 'delete_proj_' . $project_id,
        '#ajax'  => array(
          'callback' => 'bims_panel_main_di_data_form_ajax_callback',
          'wrapper'  => 'bims-panel-main-di-data-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
        '#attributes' => array(
          'style' => 'margin-left: 70px',
          'class' => array('bims-confirm'),
        ),
      );
    }
    else {
      $form['phenotype_trial']['delete_proj_btn'][$project_id] = array(
        '#markup' => '<em>N/A</em>',
      );
    }
  }
  $form['phenotype_trial']['imported_projs'] = array(
    '#type'   => 'value',
    '#value'  => $imported_projs,
  );
}

/**
 * Adds 'Import Genotype Trial Data' selection.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_main_di_data_form_import_genotype_trial(&$form, BIMS_PROGRAM $bims_program) {

  // Genotype trial selection.
  $form['genotype_trial'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Import Genotype Trial Data',
    '#attributes'   => array('style' => 'width:80%'),
  );
  $form['genotype_trial']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['genotype_trial']['#theme'] = 'bims_panel_main_di_data_form_genotype';

  // Gets all genotype trials.
  $options = BIMS_IMPORTED_PROJECT::getGenotypeTrial($bims_program, BIMS_OPTION);
  if (empty($options)) {
    $form['genotype_trial']['no_trial'] = array(
      '#markup' => "<div style='margin-top:20px;'><em>There is no genotypic trial to import.</em></div>",
    );
  }
  else {
    // Lists the genotype trials.
    $size = sizeof($options);
    $size = ($size > 10) ? 10 : $size;
    $form['genotype_trial']['select'] = array(
      '#type'         => 'select',
      '#options'      => $options,
      '#description'  => 'Use Ctl or Shift key to select multiple trials.',
      '#size'         => $size,
      '#multiple'     => TRUE,
      '#attributes'   => array('style' => 'width:280px'),
    );
    $form['genotype_trial']['genotype_trial_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'genotype_trial_btn',
      '#value'      => 'Import Trials',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_di_data_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-di-data-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Imported genotype trial table.
  $trials = $bims_program->getImportedProjByType('genotype', BIMS_OBJECT);
  $imported_projs = array();
  foreach ((array)$trials as $trial_obj) {

    // Adds 'Remove' button.
    $project_id = $trial_obj->project_id;
    $imported_projs[$project_id] = array(
      'project_name'  => $trial_obj->project_name,
      'sub_type'      => $trial_obj->sub_type,
      'import_date'   => $trial_obj->import_date,
      'status'        => $trial_obj->status,
    );
    if ($trial_obj->status == 'completed') {
      $form['genotype_trial']['delete_proj_btn'][$project_id] = array(
        '#id'    => 'delete_proj_btn_' . $project_id,
        '#type'  => 'submit',
        '#value' => 'Remove',
        '#name'  => 'delete_proj_' . $project_id,
        '#ajax'  => array(
          'callback' => 'bims_panel_main_di_data_form_ajax_callback',
          'wrapper'  => 'bims-panel-main-di-data-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
        '#attributes' => array(
          'style' => 'margin-left: 70px',
          'class' => array('bims-confirm'),
        ),
      );
    }
    else {
      $form['genotype_trial']['delete_proj_btn'][$project_id] = array(
        '#markup' => '<em>N/A</em>',
      );
    }
  }
  $form['genotype_trial']['imported_projs'] = array(
    '#type'   => 'value',
    '#value'  => $imported_projs,
  );
}

/**
 * Adds 'Import Cross Trial' selection.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_main_di_data_form_import_cross_trial(&$form, BIMS_PROGRAM $bims_program) {

  // Cross trial selection.
  $form['cross_trial'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Import Cross Trial Data',
    '#attributes'   => array('style' => 'width:80%'),
  );
  $form['cross_trial']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['cross_trial']['#theme'] = 'bims_panel_main_di_data_form_cross';

  // Gets all cross trials.
  $options = BIMS_IMPORTED_PROJECT::getCrossTrial($bims_program, BIMS_OPTION);
  if (empty($options)) {
    $form['cross_trial']['no_trial'] = array(
      '#markup' => "<div style='margin-top:20px;'><em>There is no cross trial to import.</em></div>",
    );
  }
  else {
    // Lists the genotype trials.
    $size = sizeof($options);
    $size = ($size > 10) ? 10 : $size;
    $form['cross_trial']['select'] = array(
      '#type'         => 'select',
      '#options'      => $options,
      '#description'  => 'Use Ctl or Shift key to select multiple trials.',
      '#size'         => $size,
      '#multiple'     => TRUE,
      '#attributes'   => array('style' => 'width:280px'),
    );
    $form['cross_trial']['cross_trial_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'cross_trial_btn',
      '#value'      => 'Import Trials',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_di_data_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-di-data-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Imported genotype trial table.
  $trials = $bims_program->getImportedProjByType('cross', BIMS_OBJECT);
  $imported_projs = array();
  foreach ((array)$trials as $trial_obj) {

    // Adds 'Remove' button.
    $project_id = $trial_obj->project_id;
    $imported_projs[$project_id] = array(
      'project_name'  => $trial_obj->project_name,
      'import_date'   => $trial_obj->import_date,
      'status'        => $trial_obj->status,
    );
    if ($trial_obj->status == 'completed') {
      $form['cross_trial']['delete_proj_btn'][$project_id] = array(
        '#id'    => 'delete_proj_btn_' . $project_id,
        '#type'  => 'submit',
        '#value' => 'Remove',
        '#name'  => 'delete_proj_' . $project_id,
        '#ajax'  => array(
          'callback' => 'bims_panel_main_di_data_form_ajax_callback',
          'wrapper'  => 'bims-panel-main-di-data-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
        '#attributes' => array(
          'style' => 'margin-left: 70px',
          'class' => array('bims-confirm'),
        ),
      );
    }
    else {
      $form['cross_trial']['delete_proj_btn'][$project_id] = array(
        '#markup' => '<em>N/A</em>',
      );
    }
  }
  $form['cross_trial']['imported_projs'] = array(
    '#type'   => 'value',
    '#value'  => $imported_projs,
  );
  hide($form['cross_trial']);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_data_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // 'Delete' CV was clicked.
  if (preg_match("/^delete_cv_(\d+)$/", $trigger_elem, $matches)) {

    // Removes the CV from the program.
    $bims_program->deleteImprotedData('cv', $matches[1]);
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_di_data')));
  }

  // 'Delete' project was clicked.
  else if (preg_match("/^delete_proj_(\d+)$/", $trigger_elem, $matches)) {

    // Removes the project from the program.
    $bims_program->deleteImprotedData('project', $matches[1]);
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_di_data')));
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_data_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Set Descriptor Group' was clicked.
  if ($trigger_elem == 'descriptor_btn') {

    // Check the selected cv ID.
    $cv_id = $form_state['values']['import']['descriptor']['select'];
    if (!$cv_id) {
      form_set_error('import][descriptor][select', "Please choose a descriptor group");
      return;
    }
  }

  // If 'Import' for 'Phenotype Trial' was clicked.
  else if ($trigger_elem == 'phenotype_trial_btn') {
    $project_ids = $form_state['values']['import']['phenotype_trial']['select'];
    if (empty($project_ids)) {
      form_set_error('import][phenotype_trial][select', "Please choose at least on trial");
      return;
    }
  }

  // If 'Import' for 'Genotype Trial' was clicked.
  else if ($trigger_elem == 'genotype_trial_btn') {
    $project_ids = $form_state['values']['import']['genotype_trial']['select'];
    if (empty($project_ids)) {
      form_set_error('import][genotype_trial][select', "Please choose at least on trial");
      return;
    }
  }

  // If 'Import' for 'Cross Trial' was clicked.
  else if ($trigger_elem == 'cross_trial_btn') {
    $project_ids = $form_state['values']['import']['cross_trial']['select'];
    if (empty($project_ids)) {
      form_set_error('import][cross_trial_btn][select', "Please choose at least on trial");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_data_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $crop         = $bims_program->getCrop();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  $transaction = db_transaction();
  try {

    // If 'Set Descriptor Group' was clicked.
    if ($trigger_elem == 'descriptor_btn') {

      // Gets cv ID.
      $cv_id = $form_state['values']['import']['descriptor']['select'];
      if (!$bims_program->importCV($cv_id)) {
        throw new Exception("Error : Failed to import CV");
      }
    }

    // If 'Import' for 'Phenotype Trial' was clicked.
    else if ($trigger_elem == 'phenotype_trial_btn') {

      // Gets the selected trials.
      $project_ids = $form_state['values']['import']['phenotype_trial']['select'];
      foreach ($project_ids as $project_id) {

        // Gets MCL_CHADO_DATASET.
        $dataset = MCL_CHADO_DATASET::byID($project_id);
        if (!$dataset) {
          throw new Exception("Error : Could not find the project (ID = $project_id)");
        }

        // Gets the project properties.
        $project_name = $dataset->getName();
        $type         = $dataset->getType();
        $sub_type     = $dataset->getSubType();

        // Adds the phenotype trial to bims_import.
        $details = array(
          'program_id'    => $program_id,
          'type'          => $type,
          'sub_type'      => $sub_type,
          'project_id'    => $project_id,
          'project_name'  => $project_name,
          'import_date'   => date("Y-m-d G:i:s"),
          'status'        => 'importing',
        );
        $bims_import = new BIMS_IMPORTED_PROJECT($details);
        if (!$bims_import->insert()) {
          throw new Exception("Error : Failed to add a phenotype trial to bims_import");
        }

        // Adds the phenotype trial to bims_node.
        $details = array(
          'project_id'        => $project_id,
          'name'              => $project_name,
          'type'              => 'TRIAL',
          'crop_id'           => $crop->getCropID(),
          'crop'              => $crop->getName(),
          'root_id'           => $program_id,
          'description'       => $dataset->getDescription(),
          'owner_id'          => $bims_user->getUserID(),
          'contact'           => $bims_user->getName(),
          'access'            => $bims_program->getAccess(),
          'project_type'      => $type,
          'project_sub_type'  => $sub_type,
        );
        if (!$bims_program->addTrial($details, FALSE)) {
          throw new Exception("Error : Failed to add a phenotype trial to bims_node");
        }
      }

      // Updates the mview for the imported phenotype trial data.
      $drush = bims_get_config_setting('bims_drush_binary');
      $project_ids_str = implode(':', $project_ids);
      $cmd = "bims-import-projects $program_id --project_id=$project_ids_str";
      exec("$drush $cmd > /dev/null 2>/dev/null &");
      drupal_set_message("Importing job has been started.");
    }

    // If 'Import' for 'Genotype Trial' was clicked.
    else if (preg_match("/^(genotype|cross)_trial_btn/", $trigger_elem)) {

      // Gets the selected trials.
      $elem_name = str_replace('_btn', '', $trigger_elem);
      $project_ids = $form_state['values']['import'][$elem_name]['select'];
      foreach ($project_ids as $project_id) {

        // Gets MCL_CHADO_DATASET.
        $dataset = MCL_CHADO_DATASET::byID($project_id);
        if (!$dataset) {
          throw new Exception("Error : Could not find the project (ID = $project_id)");
        }

        // Gets the project properties.
        $project_name = $dataset->getName();
        $type         = $dataset->getType();
        $sub_type     = $dataset->getSubType();

        // Adds the phenotype trial to bims_import.
        $details = array(
          'program_id'    => $program_id,
          'type'          => $type,
          'sub_type'      => $sub_type,
          'project_id'    => $project_id,
          'project_name'  => $project_name,
          'import_date'   => date("Y-m-d G:i:s"),
          'status'        => 'importing',
        );
        $bims_import = new BIMS_IMPORTED_PROJECT($details);
        if (!$bims_import->insert()) {
          throw new Exception("Error : Failed to add a trial to bims_import");
        }

        // Adds the phenotype trial to bims_node.
        $details = array(
          'project_id'        => $project_id,
          'name'              => $project_name,
          'type'              => 'TRIAL',
          'crop_id'           => $crop->getCropID(),
          'crop'              => $crop->getName(),
          'root_id'           => $program_id,
          'description'       => $dataset->getDescription(),
          'owner_id'          => $bims_user->getUserID(),
          'contact'           => $bims_user->getName(),
          'access'            => $bims_program->getAccess(),
          'project_type'      => $type,
          'project_sub_type'  => $sub_type,
        );
        if (!$bims_program->addTrial($details, FALSE)) {
          throw new Exception("Error : Failed to add a trial to bims_node");
        }
      }

      // Updates the mview for the imported genotype trial data.
      $drush = bims_get_config_setting('bims_drush_binary');
      $project_ids_str = implode(':', $project_ids);
      $cmd = "bims-import-projects $program_id --project_id=$project_ids_str";
      exec("$drush $cmd > /dev/null 2>/dev/null &");
      drupal_set_message("Importing job has been started.");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_data_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "import".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['import']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
 }

 /**
 * Theme function for the descriptor section.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_data_form_descriptor($variables) {
  $element = $variables['element'];

  // Gets the passed values.
  $bims_program = $element['bims_program']['#value'];
  $imported_cvs = $element['imported_cvs']['#value'];

  // Lists the imported CVs in the table.
  $layout = '';
  if (array_key_exists('no_chado_cv', $element)) {
    return drupal_render($form['no_chado_cv']);
  }
  $rows = array();
  if (array_key_exists('delete_cv', $element)) {
    foreach ((array)$element['delete_cv'] as $cv_id => $elem) {
      if (!preg_match("/^\d+$/", $cv_id)) {
        continue;
      }

      // Adds a cv row.
      $rows []= array(
        $imported_cvs[$cv_id]['cv_name'],
        $imported_cvs[$cv_id]['num_proj'],
        $imported_cvs[$cv_id]['status'],
        drupal_render($elem),
      );
    }
  }
  $header = array(
    'Descriptor Group Name',
    '# Imported Projects',
    'Status',
    'Action',
  );
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:90%;'),
    'sticky'      => TRUE,
    'empty'       => '<em>No descriptor group has been imported to this program</em>',
    'param'       => array(),
  );
  $layout .= bims_theme_table($table_vars);

  // Lists available CVs.
  if (array_key_exists('no_avail_cv', $element)) {
    $layout .= drupal_render($element['no_avail_cv']);
  }
  else {
    $layout .= drupal_render($element['select']);
    $layout .= drupal_render($element['descriptor_btn']);
  }
  return $layout;
}

 /**
  * Theme function for the phenotype section.
  *
  * @param $variables
  */
function theme_bims_panel_main_di_data_form_phenotype($variables) {
  $element = $variables['element'];

  // Gets BIMS_PROGRAM.
  $bims_program = $element['bims_program']['#value'];

  // Lists the imported CVs in the table.
  $layout = '';
  if (array_key_exists('no_chado_cv', $element)) {
    return drupal_render($form['no_chado_cv']);
  }

  // Lists all the imported CVs.
  $layout .= drupal_render($form['imported_cv']);

  // Lists available trials to be imported.
  if (array_key_exists('no_trial', $element)) {
    $layout .= drupal_render($element['no_trial']);
  }
  else {
    $layout .= drupal_render($element['select']);
    $layout .= drupal_render($element['phenotype_trial_btn']);
  }

  // Lists all the imported trials.
  $rows = array();
  if (array_key_exists('delete_proj_btn', $element)) {
    $imported_projs = $element['imported_projs']['#value'];
    foreach ((array)$element['delete_proj_btn'] as $project_id => $elem) {
      if (!preg_match("/^\d+$/", $project_id)) {
        continue;
      }

      // Adds a row.
      $rows []= array(
        $imported_projs[$project_id]['project_name'],
        $imported_projs[$project_id]['cv_name'],
        $imported_projs[$project_id]['import_date'],
        '<em>' . $imported_projs[$project_id]['status'] . '</em>',
        drupal_render($elem),
      );
    }
  }
  $header = array(
    array('data' => 'Project&nbsp;Name', 'width' => 250),
    array('data' => 'Descriptor&nbsp;Group', 'width' => 250),
    array('data' => 'Imported&nbsp;date', 'width' => 120),
    array('data' => 'Status', 'width' => 40),
    array('data' => 'Action', 'width' => 40),
  );
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:90%;'),
    'sticky'      => TRUE,
    'empty'       => '<em>No trial has been imported to this program</em>',
    'param'       => array(),
  );
  $layout .= "<div style='margin-top:15px;'><b>Imported phenotypic trials</b></div>";
  $layout .= bims_theme_table($table_vars);
  return $layout;
}

 /**
  * Theme function for the genotype section.
  *
  * @param $variables
  */
function theme_bims_panel_main_di_data_form_genotype($variables) {
  $element = $variables['element'];

  // Gets BIMS_PROGRAM.
  $bims_program = $element['bims_program']['#value'];

  // Lists available trials to be imported.
  $layout = '';
  if (array_key_exists('no_trial', $element)) {
    $layout .= drupal_render($element['no_trial']);
  }
  else {
    $layout .= drupal_render($element['select']);
    $layout .= drupal_render($element['genotype_trial_btn']);
  }

  // Lists all the imported trials.
  $rows = array();
  if (array_key_exists('delete_proj_btn', $element)) {
    $imported_projs = $element['imported_projs']['#value'];
    foreach ((array)$element['delete_proj_btn'] as $project_id => $elem) {
      if (!preg_match("/^\d+$/", $project_id)) {
        continue;
      }

      // Adds a row.
      $rows []= array(
        $imported_projs[$project_id]['project_name'],
        $imported_projs[$project_id]['sub_type'],
        $imported_projs[$project_id]['import_date'],
        '<em>' . $imported_projs[$project_id]['status'] . '</em>',
        drupal_render($elem),
      );
    }
  }
  $header = array(
    array('data' => 'Project&nbsp;Name', 'width' => 200),
    array('data' => 'Sub&nbsp;Type', 'width' => 70),
    array('data' => 'Imported&nbsp;Date', 'width' => 120),
    array('data' => 'Status', 'width' => 40),
    array('data' => 'Action', 'width' => 40),
  );
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:90%;'),
    'sticky'      => TRUE,
    'empty'       => '<em>No trial has been imported to this program</em>',
    'param'       => array(),
  );
  $layout .= "<div style='margin-top:15px;'><b>Imported genotypic trials</b></div>";
  $layout .= bims_theme_table($table_vars);
  return $layout;
}

 /**
  * Theme function for the cross section.
  *
  * @param $variables
  */
function theme_bims_panel_main_di_data_form_cross($variables) {
  $element = $variables['element'];

  // Gets BIMS_PROGRAM.
  $bims_program = $element['bims_program']['#value'];

  // Lists available trials to be imported.
  $layout = '';
  if (array_key_exists('no_trial', $element)) {
    $layout .= drupal_render($element['no_trial']);
  }
  else {
    $layout .= drupal_render($element['select']);
    $layout .= drupal_render($element['cross_trial_btn']);
  }

  // Lists all the imported trials.
  $rows = array();
  if (array_key_exists('delete_proj_btn', $element)) {
    $imported_projs = $element['imported_projs']['#value'];
    foreach ((array)$element['delete_proj_btn'] as $project_id => $elem) {
      if (!preg_match("/^\d+$/", $project_id)) {
        continue;
      }

      // Adds a row.
      $rows []= array(
        $imported_projs[$project_id]['project_name'],
        $imported_projs[$project_id]['import_date'],
        '<em>' . $imported_projs[$project_id]['status'] . '</em>',
        drupal_render($elem),
      );
    }
  }
  $header = array(
    array('data' => 'Project&nbsp;Name', 'width' => 250),
    array('data' => 'Imported&nbsp;date', 'width' => 120),
    array('data' => 'Status', 'width' => 40),
    array('data' => 'Action', 'width' => 40),
  );
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:90%;'),
    'sticky'      => TRUE,
    'empty'       => '<em>No trial has been imported to this program</em>',
    'param'       => array(),
  );
  $layout .= "<div style='margin-top:15px;'><b>Imported cross trials</b></div>";
  $layout .= bims_theme_table($table_vars);
  return $layout;
}
