<?php
/**
 * @file
 */
/**
 * template list panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_template_list_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_di_template_list');
  $bims_panel->init($form);

  // Shows templates in a table.
  $form['data_template'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Data templates',
  );
  $desc = 'The table below lists the templates used in BIMS. The template name corresponds to the name of Excel worksheet and it is not case-sensitive. You can download an indivual template in an Excel format form a "download" link in the table. The templates can be combined into one Excel file before loading to the database.';
  $form['data_template']['desc'] = array(
    '#markup' => "<div style='margin-bottom:20px;'>$desc</div>",
  );
  $form['data_template']['table'] = array(
    '#markup' => _get_template_table(),
  );
  $form['#prefix'] = '<div id="bims-panel-main-di-template-list-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_di_template_list_form_submit';
  $form['#theme'] = 'bims_panel_main_di_template_list_form';
  return $form;
}

/**
 * Returns template table.
 *
 * @return string
 */
function _get_template_table() {

  // Headers.
  $headers = array(
    array('data' => 'Template name', 'style' => 'width:200px;'),
    array('data' => 'Details', 'style' => 'width:80px;'),
    array('data' => 'Download', 'style' => 'width:80px;'),
    'Description',
  );

  // BIMS templates.
  $bims_templates = array(
    'contact_bims'                => array('name' => 'Contact', 'desc' => 'Contact information'),
    'descriptor_bims'             => array('name' => 'Descriptor', 'desc' => 'Traits used in phenotyping data of your program'),
    'dataset_bims'                => array('name' => 'Dataset', 'desc' => 'Trials of your program'),
    'site_bims'                   => array('name' => 'Site', 'desc' => 'Locations of the phenotyping data collected'),
    'accession_bims'              => array('name' => 'Accession', 'desc' => 'Accessions of your program'),
    'marker_bims'                 => array('name' => 'Marker', 'desc' => 'Markers of your program'),
    'cross_bims'                  => array('name' => 'Cross', 'desc' => 'Crosses of your program'),
    'progeny_bims'                => array('name' => 'Progeny', 'desc' => 'Progeny'),
    'property_bims'               => array('name' => 'Property', 'desc' => 'Properties'),
    'phenotype_bims'              => array('name' => 'Phenotype', 'desc' => 'Phenotypic data'),
    'phenotype_long_form_bims'    => array('name' => 'Phenotype (Long Form)', 'desc' => 'Phenotypic data in a long form'),
    'genotype_snp_long_form_bims' => array('name' => 'Genotype SNP (Long Form)', 'desc' => 'Genotypic SNP data in a long form'),
    'genotype_bims'               => array('name' => 'Genotype SSR', 'desc' => 'Genotypic SSR data'),
    'haplotype_bims'              => array('name' => 'Haplotype', 'desc' => 'Haplotype'),
    'haplotype_block_bims'        => array('name' => 'Haplotype Block', 'desc' => 'Haplotype block'),
    'image_bims'                  => array('name' => 'Image', 'desc' => 'Images for trait, accession and samples'),
  );

  // Data templates.
  $rows = array();
  $mcl_templates = MCL_TEMPLATE::getTemplates();
  $template_ids = array();
  foreach ($mcl_templates as $mcl_template) {
    $template = $mcl_template->getTemplate();

    // Filter out non-BIMS templates.
    if (!array_key_exists($template, $bims_templates)) {
      continue;
    }
    $template_type  = $mcl_template->getTemplateType();
    $template_id    = $mcl_template->getTemplateID();
    $tmpl           = MCL_TEMPLATE::getTemplateByID($template_id);

    // Checks if class has been defined.
    $link_details  = '<em>Not Defined</em>';
    $link_download = '<em>N/A</em>';
    if ($tmpl) {
      $onclick_details  = "onclick=\"bims.add_main_panel('bims_panel_add_on_template', '$template', 'bims/load_main_panel/bims_panel_add_on_template/$template_id'); return (false);\"";
      $link_details     = "<a href='javascript:void(0)' $onclick_details>view</a>";
      $link_download    = "<a href='/mcl/template/download/$template_id'>download</a>";
      $template_ids []= $template_id;
    }

    // Populates rows.
    $row = array(
      $bims_templates[$template]['name'],
      $link_details,
      $link_download,
      $bims_templates[$template]['desc'],
    );
    $rows []= $row;
  }

  // Adds the last row for download all template.
  $template_id_str = implode(':', $template_ids);
  $rows []= array(
    array('data' => "Click <a href='/mcl/template/download/$template_id_str'><b>here</b></a> to download all template in one file.",'height' => '40', 'colspan' => 4),
  );

  // Table variables.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'empty'       => 'No Data Template',
    'attributes'  => array('style' => 'width:100%'),
  );

  // Table Footer.
  return theme('table', $table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_template_list_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_template_list_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_template_list_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_main_di_template_list_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the data template.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['data_template']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
 }