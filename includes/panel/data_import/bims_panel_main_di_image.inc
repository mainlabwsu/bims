<?php
/**
 * @file
 */
/**
 * image upload panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_image_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_di_image');
  $bims_panel->init($form);

  // Shows templates in a table.
  $form['upload_photo_file'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Upload Photo files',
  );
  $form['upload_photo_file']['dnd'] = array(
    '#markup' => '<div id="bims_dnd_photo"><em>Please drop your files</em></div>',
  );

  $form['#prefix'] = '<div id="bims-panel-main-di-image-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_di_image_form_submit';
  $form['#theme'] = 'bims_panel_main_di_image_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_image_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_image_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_image_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_main_di_image_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the program list.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['upload_photo_file']) . "</div>";

  // Adds the program info table.
 // $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['program_details']) . "</div>";


  $layout .= drupal_render_children($form);
  return $layout;
 }