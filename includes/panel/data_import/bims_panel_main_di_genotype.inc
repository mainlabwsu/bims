<?php
/**
 * @file
 */
/**
 * Genotypic data import panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $cv_id
 */
function bims_panel_main_di_genotype_form($form, &$form_state, $cv_id = NULL) {

  // Gets BIMS_USER and BIMS__PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_di_genotype');
  if (!$bims_panel->init($form, TRUE, TRUE)) {
    return $form;
  }

  // Import data section.
  $form['import'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Import public genotypic data',
  );

  // Adds the import genotype trials selection.
  _bims_panel_main_di_genotype_form_import_trial($form['import'], $bims_program);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-di-genotype-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_di_genotype_form_submit';
  $form['#theme'] = 'bims_panel_main_di_genotype_form';
  return $form;
}

/**
 * Import genotype trial data.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_main_di_genotype_form_import_trial(&$form, BIMS_PROGRAM $bims_program) {

  // Genotype trial selection.
  $form['genotype_trial']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['genotype_trial']['#theme'] = 'bims_panel_main_di_genotype_form_genotype';

  // Gets all genotype trials.
  $options = BIMS_IMPORTED_PROJECT::getGenotypeTrial($bims_program, BIMS_OPTION);
  if (empty($options)) {
    $form['genotype_trial']['no_trial'] = array(
      '#markup' => "<div style='margin-top:20px;'><em>There is no genotypic trial to import.</em></div>",
    );
  }
  else {
    // Lists the genotype trials.
    $size = sizeof($options);
    $size = ($size > 10) ? 10 : $size;
    $prefix = "<div><b>Pulically available genotypic trials</b></div>";
    $form['genotype_trial']['select'] = array(
      '#type'         => 'select',
      '#options'      => $options,
      '#description'  => 'Use Ctl or Shift key to select multiple trials.',
      '#size'         => $size,
      '#multiple'     => TRUE,
      '#prefix'       => $prefix,
      '#attributes'   => array('style' => 'width:280px'),
    );
    $form['genotype_trial']['import_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'import_btn',
      '#value'      => 'Import Trials',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_di_genotype_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-di-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Imported genotype trial table.
  $trials = $bims_program->getImportedProjByType('genotype', BIMS_OBJECT);
  $imported_projs = array();
  foreach ((array)$trials as $trial_obj) {

    // Adds 'Remove' button.
    $project_id = $trial_obj->project_id;
    $imported_projs[$project_id] = array(
      'project_name'  => $trial_obj->project_name,
      'sub_type'      => $trial_obj->sub_type,
      'import_date'   => $trial_obj->import_date,
      'status'        => $trial_obj->status,
    );
    if (preg_match("/(mviews|completed)/", $trial_obj->status)) {
      $form['genotype_trial']['delete_proj_btn'][$project_id] = array(
        '#id'    => 'delete_proj_btn_' . $project_id,
        '#type'  => 'submit',
        '#value' => 'Remove',
        '#name'  => 'delete_proj_' . $project_id,
        '#ajax'  => array(
          'callback' => 'bims_panel_main_di_genotype_form_ajax_callback',
          'wrapper'  => 'bims-panel-main-di-genotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
        '#attributes' => array(
          'style' => 'margin-left: 70px',
          'class' => array('bims-confirm'),
        ),
      );
    }
    else {
      $form['genotype_trial']['delete_proj_btn'][$project_id] = array(
        '#markup' => '<em>N/A</em>',
      );
    }
  }
  $form['genotype_trial']['imported_projs'] = array(
    '#type'   => 'value',
    '#value'  => $imported_projs,
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_genotype_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // 'Delete' project was clicked.
  if (preg_match("/^delete_proj_(\d+)$/", $trigger_elem, $matches)) {

    // Removes the project from the program.
    $bims_program->deleteImprotedData('project', $matches[1]);
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_di_genotype')));
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_genotype_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Import' was clicked.
  if ($trigger_elem == 'import_btn') {
    $project_ids = $form_state['values']['import']['genotype_trial']['select'];
    if (empty($project_ids)) {
      form_set_error('import][genotype_trial][select', "Please choose at least on trial");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_genotype_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $crop         = $bims_program->getCrop();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  $transaction = db_transaction();
  try {

    // If 'Import' was clicked.
    if ($trigger_elem == 'import_btn') {

      // Gets the selected trials.
      $project_ids = $form_state['values']['import']['genotype_trial']['select'];
      foreach ($project_ids as $project_id) {

        // Gets MCL_CHADO_DATASET.
        $dataset = MCL_CHADO_DATASET::byID($project_id);
        if (!$dataset) {
          throw new Exception("Error : Could not find the project (ID = $project_id)");
        }

        // Gets the project properties.
        $project_name = $dataset->getName();
        $type         = $dataset->getType();
        $sub_type     = $dataset->getSubType();

        // Adds the genotype trial to bims_import.
        $details = array(
          'program_id'    => $program_id,
          'type'          => $type,
          'sub_type'      => $sub_type,
          'project_id'    => $project_id,
          'project_name'  => $project_name,
          'import_date'   => date("Y-m-d G:i:s"),
          'status'        => 'importing',
        );
        $bims_import = new BIMS_IMPORTED_PROJECT($details);
        if (!$bims_import->insert()) {
          throw new Exception("Error : Failed to add a trial to bims_import");
        }

        // Adds the genotype trial to bims_node.
        $details = array(
          'project_id'        => $project_id,
          'name'              => $project_name,
          'type'              => 'TRIAL',
          'crop_id'           => $crop->getCropID(),
          'crop'              => $crop->getName(),
          'root_id'           => $program_id,
          'description'       => $dataset->getDescription(),
          'owner_id'          => $bims_user->getUserID(),
          'contact'           => $bims_user->getName(),
          'access'            => $bims_program->getAccess(),
          'project_type'      => $type,
          'project_sub_type'  => $sub_type,
        );
        if (!$bims_program->addTrial($details, FALSE)) {
          throw new Exception("Error : Failed to add a trial to bims_node");
        }
      }

      // Updates the mview for the imported genotype trial data.
      $drush = bims_get_config_setting('bims_drush_binary');
      $project_ids_str = implode(':', $project_ids);
      $cmd = "bims-import-projects $program_id --project_ids=$project_ids_str";
      exec("$drush $cmd > /dev/null 2>/dev/null &");
      drupal_set_message("Importing job has been started.");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_genotype_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "import".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['import']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
 }

 /**
  * Theme function for the genotype section.
  *
  * @param $variables
  */
function theme_bims_panel_main_di_genotype_form_genotype($variables) {
  $element = $variables['element'];

  // Gets BIMS_PROGRAM.
  $bims_program = $element['bims_program']['#value'];

  // Lists available trials to be imported.
  $layout = '';
  if (array_key_exists('no_trial', $element)) {
    $layout .= drupal_render($element['no_trial']);
  }
  else {
    $layout .= drupal_render($element['select']);
    $layout .= drupal_render($element['import_btn']);
  }

  // Lists all the imported trials.
  $rows = array();
  if (array_key_exists('delete_proj_btn', $element)) {
    $imported_projs = $element['imported_projs']['#value'];
    foreach ((array)$element['delete_proj_btn'] as $project_id => $elem) {
      if (!preg_match("/^\d+$/", $project_id)) {
        continue;
      }

      // Adds a row.
      $rows []= array(
        $imported_projs[$project_id]['project_name'],
        $imported_projs[$project_id]['sub_type'],
        $imported_projs[$project_id]['import_date'],
        '<em>' . $imported_projs[$project_id]['status'] . '</em>',
        drupal_render($elem),
      );
    }
  }
  $header = array(
    array('data' => 'Project&nbsp;Name', 'width' => 200),
    array('data' => 'Sub&nbsp;Type', 'width' => 70),
    array('data' => 'Imported&nbsp;Date', 'width' => 120),
    array('data' => 'Status', 'width' => 40),
    array('data' => 'Action', 'width' => 40),
  );
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:90%;'),
    'sticky'      => TRUE,
    'empty'       => '<em>No trial has been imported to this program</em>',
    'param'       => array(),
  );
  $layout .= "<div style='margin-top:15px;'><b>Imported genotypic trials</b></div>";
  $layout .= bims_theme_table($table_vars);
  return $layout;
}
