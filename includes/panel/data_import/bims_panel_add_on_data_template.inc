<?php
/**
 * @file
 */
/**
 * Template panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $template_id
 */
function bims_panel_add_on_template_form($form, &$form_state, $template_id) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_template');
  $bims_panel->init($form);

  // Gets MCL_TEMPLATE.
  $mcl_template = MCL_TEMPLATE::getTemplateByID($template_id);

  // Shows the details of the template.
  $form['template_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Template : ' . $mcl_template->getTemplate(),
  );

  // The header columns of the template.
  $form['template_details']['headers'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => "Headers",
  );

  // Overwrite the accession and FB columns if the tempalte is
  // accession, phenotype and image_phenotype.
  $overwrite_cols = array();
  if (preg_match("/^(accession|phenotype|image_phenotype|genotype)/", $mcl_template->getTemplate())) {
    $overwrite_cols = $bims_program->getBIMSCols();
  }

  // Lists the headers in a table.
  $form['template_details']['headers']['table'] = array(
    '#markup' => $mcl_template->getHeaderTable($overwrite_cols),
  );

  // List the cvterms in a table.
  $form['template_details']['cvterms'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => "cvterms",
  );
  $form['template_details']['cvterms']['table'] = array(
    '#markup' => $mcl_template->getCvtermTable(),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-template-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_template_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_template_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_template_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_template_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_template_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);
  $layout .= drupal_render_children($form);
  return $layout;
}
