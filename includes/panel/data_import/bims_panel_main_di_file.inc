<?php
/**
 * @file
 */
/**
 * Data Import (upload data template file) panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_file_form($form, &$form_state, $file_id = '') {

  // Local variables for form properties.
  $min_height = 470;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets BIMS_FILE.
  $bims_file = NULL;
  if (isset($form_state['values']['uploads']['upload_list'])) {
    $file_id = $form_state['values']['uploads']['upload_list'];
    $bims_file = BIMS_FILE::byID($file_id);
  }
  if (!$bims_file && $file_id) {
    $bims_file = BIMS_FILE::byID($file_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_di_file');
  if (!$bims_panel->init($form, TRUE, TRUE)) {
    return $form;
  }

  // Adds the uploads.
  $form['uploads'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Uploaded files',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $args = array(
    'type'          => 'ULE',
    'by_user_id'    => $bims_user->getUserID(),
    'program_id'    => $bims_user->getProgramID(),
    'date'          => TRUE,
  );
  $options = BIMS_FILE::getFiles($args, BIMS_OPTION);
  if (empty($options)) {
    $options = array('' => 'No uploaded file found');
  }
  $form['uploads']['upload_list'] = array(
    '#type'           => 'select',
    '#options'        => $options,
    '#size'           => 7,
    '#attributes'     => array('style' => 'width:98%;'),
    '#default_value'  => $file_id,
    '#ajax'           => array(
      'callback' => 'bims_panel_main_di_file_form_ajax_callback',
      'wrapper'  => 'bims-panel-main-di-file-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Saves BIMS_FILE.
  $form['uploads']['info_table']['bims_file'] = array(
    '#type' => 'value',
    '#value' => $bims_file,
  );

  // Adds action buttons.
  if ($bims_file) {
    $form['uploads']['info_table']['view_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'view_btn',
      '#value'      => 'View',
      '#attributes' => array('style' => 'width:85px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_di_file_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-di-file-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['uploads']['info_table']['delete_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'delete_btn',
      '#value'      => 'Delete',
      '#attributes' => array(
        'style' => 'width:85px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_di_file_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-di-file-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['uploads']['info_table']['#theme'] = 'bims_panel_main_di_file_form_upload_table';

  // Uploads a file.
  $form['upload'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Upload data by Excel template',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['upload']['desc'] = array(
    '#markup' => "<div style='margin:5px 0px 18px 0px;'>Please browse your Excel template file and hit '<b>Upload</b>' button. Then add a description of your data file (<em>optional</em>) and click '<b>Submit</b>' button to start uploading your template file.</div>",
  );
  $form['upload']['browse'] = array(
    '#type'               => 'managed_file',
    '#title'              => t('Excel / CSV file'),
    '#description'        => t('Please upload your Excel, CSV or zipped file ( (.xlsx .xls .csv .gz .zip)'),
    '#upload_location'    => $bims_user->getUserDirURL(),
    '#upload_validators'  => array(
      'file_validate_extensions' => array('xls xlsx csv gz zip'),
      // Pass the maximum file size in bytes
      //'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
    ),
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_main_di_file_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-di-file-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['upload']['file_desc'] = array(
    '#type'       => 'textarea',
    '#title'      => 'File Description',
    '#rows'       => 3,
    '#suffix'     => 'Please add a description of your file<br /><br />',
    '#attributes' => array('style' => 'max-width:450px;'),
  );
  $form['upload']['start_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'start_btn',
    '#value'      => 'Submit',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_di_file_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-di-file-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['#prefix'] = '<div id="bims-panel-main-di-file-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_di_file_form_submit';
  $form['#theme'] = 'bims_panel_main_di_file_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_file_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_file_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Start' was clicked.
  if ($trigger_elem == 'start_btn') {
    $fid = $form_state['values']['upload']['browse'];
    if (!$fid) {
      form_set_error('bims', t("No file selected"));
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_file_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'View' was clicked.
  if ($trigger_elem == 'view_btn') {

    // Opens the job view panel.
    $bims_file = $form_state['values']['uploads']['info_table']['bims_file'];
    if ($bims_file) {
      $url = 'bims/load_main_panel/bims_panel_add_on_job_upload/' .  $bims_file->getFileID();
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_job_upload', 'Uploading Job', $url)));
    }
  }

  // If 'Delete' was clicked.
  else if ($trigger_elem == 'delete_btn') {

    // Gets file.
    $bims_file = $form_state['values']['uploads']['info_table']['bims_file'];
    if ($bims_file) {

      // Deletes the BIMS file.
      if (!$bims_file->delete()) {
        drupal_set_message('Error : Failed to delete the file', 'error');
      }

      // Removes the job view tab.
      bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_job_upload')));
    }
    else {
      drupal_set_message('Warn : Please choose the file', 'error');
    }
  }

  // If 'Start' was clicked.
  else if ($trigger_elem == 'start_btn') {

    // Gets BIMS_USER and MCL_USER.
    $bims_user    = getBIMS_USER();
    $bims_program = $bims_user->getProgram();
    $bims_crop    = $bims_user->getCrop();
    $mcl_user     = $bims_user->getMCL_USER();
    $program_id   = $bims_program->getProgramID();
    $bims_file_id = '';

    // Gets the values from the form_state.
    $description = trim($form_state['values']['upload']['file_desc']);
    $drupal_file = file_load($form_state['values']['upload']['browse']);

    // Upload the file.
    if ($drupal_file) {

      // Moves the file to 'upload' directory.
      $filepath = BIMS_FILE::moveFile($bims_user, $drupal_file, 'upload');

      // Sets email option.
      $send_email = $bims_user->getEmailOption('upload');

      // Gets BIMS columns.
      $bims_cols = $bims_program->getBIMSCols(TRUE);

      // Sets job parameters.
      $param = array(
        'type'              => 'ULE',
        'program_id'        => $program_id,
        'cv_arr'            => $bims_program->getCvArr(),
        'crop'              => $bims_crop->getName(),
        'PI'                => '',
        'project_id_cross'  => $bims_program->getCrossProjectID(),
        'valid_formats'     => BIMS_FIELD_BOOK::getFormats(),
        'send_email'        => $send_email,
      );
      $args = array(
        'mcl_user'    => $mcl_user,
        'filepath'    => $filepath,
        'description' => $description,
        'param'       => $param,
      );
      $bims_file = BIMS_FILE::upload($args);
      if (!$bims_file) {
        drupal_set_message("Failed to upload file.", 'error');
      }
      else {
        $bims_file_id = '/' . $bims_file->getFileID();
      }
    }
    $url = 'bims_panel_main_di_file' . $bims_file_id;
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($url)));
  }
}

/**
 * Theme function for the upload table.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_file_form_upload_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_FILE.
  $bims_file = $element['bims_file']['#value'];

  // Creates a table for uploaded file.
  return BIMS_FILE::createInfoTable($element, $bims_file);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_main_di_file_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "uploads".
  $layout .= "<div style='float:left;width:49%;'>" . drupal_render($form['uploads']) . "</div>";

  // Adds 'upload'.
  $layout .= "<div style='float:left;width:50%;margin-left:5px;'>" . drupal_render($form['upload']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
 }