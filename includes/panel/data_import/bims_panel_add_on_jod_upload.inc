<?php
/**
 * @file
 */
/**
 * Uploading job form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $file_id
 */
function bims_panel_add_on_job_upload_form($form, &$form_state, $file_id) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_job_upload');
  $bims_panel->init($form);

  // Show the job details.
  $form['job_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Job Details',
  );

  // Gets and saves BIMS_FILE and MCL_JOB_UPLOAD.
  $bims_file = BIMS_FILE::byID($file_id);
  if (!$bims_file) {
    $form['job_details']['error'] = array(
      '#markup' => "<em>The file ID ($file_id) of BIMS_FILE could not be found.</em>",
    );
    return $form;
  }
  $mcl_job_id = $bims_file->getPropByKey('mcl_job_id');
  $mcl_job = MCL_JOB_UPLOAD::byID($mcl_job_id);
  if (!$mcl_job) {
    $form['job_details']['error'] = array(
        '#markup' => "<em>The Job ID ($mcl_job_id) could not be found.</em>",
    );
    return $form;
  }
  $form['bims_file'] = array(
    '#type'   => 'value',
    '#value'  => $bims_file,
  );
  $form['mcl_job'] = array(
    '#type'   => 'value',
    '#value'  => $mcl_job,
  );

  // Displays an warning message that the MCL job has already been deleted.
  if (!$mcl_job) {
    $form['job_details']['no_mcl_job'] = array(
      '#markup' => '<div><em><b>MCL job has alerady been deleted.</b></em></div>',
    );
    return $form;
  }

  // Adds the job table.
  $form['job_details']['table'] = array(
    '#markup' => _bims_get_job_detail_table($mcl_job),
  );

  // Adds the refresh button.
  $url = "bims/load_main_panel/bims_panel_add_on_job_upload/$file_id";
  $button = "<button style=\"width:100px;\" onclick=\"bims.add_main_panel('bims_panel_add_on_job_upload', 'Upload Job' ,'$url'); return (false);\">Refresh</button>";
  $form['job_details']['refresh_page'] = array(
    '#markup' => $button,
  );

  // Adds re-run.
  $form['job_rerun'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Re-Run Job',
  );
  $valid_ext = BIMS_FILE::getValidExt($mcl_job->getParamByKey('type'));
  $form['job_rerun']['upload_file'] = array(
    '#type'               => 'managed_file',
    '#title'              => 'Data template file',
    '#description'        => "Please provide a data template file.",
    '#upload_location'    => $bims_user->getUserDirURL(),
    '#progress_indicator' => 'bar',
    '#upload_validators'  => array(
      'file_validate_extensions' => $valid_ext,
    ),
  );
  $form['job_rerun']['advanced'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Advanced options',
  );
  $form['job_rerun']['advanced']['force_cb'] = array(
    '#type'           => 'checkbox',
    '#title'          => 'Force to upload data',
    '#description'    => "Check if new data were added to the template which has already been uploaded.",
    '#default_value'  => FALSE,
  );
  /*
  $default = ($mcl_job->getPropByKey('transaction')) ? FALSE : TRUE;
  $form['job_rerun']['advanced']['no_transaction_cb'] = array(
    '#type'           => 'checkbox',
    '#title'          => 'No transaction',
    '#description'    => "Check if the size of your uploading file are large.",
    '#default_value'  => $default,
  );
  */
  $form['job_rerun']['rerun_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'rerun_btn',
    '#value'      => 'Re-Run the Job',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_job_upload_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-job-upload-form',
      'effect'   => 'fade',
      'method'   => 'replace',
     ),
  );
  $form['job_rerun']['delete_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'delete_btn',
    '#value'      => 'Delete the Job',
    '#attributes' => array(
      'style' => 'width:180px;',
      'class' => array('use-ajax', 'bims-confirm'),
    ),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_job_upload_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-job-upload-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Displays all log files of this job. It creates a fieldset
  // for each type of a log.
  $all_logs = $mcl_job->getLogs();
  //$log_types = array('E', 'N', 'W');
  $log_types = array('E', 'N', 'W', 'D');
  foreach ($log_types as $log_type) {
    if (array_key_exists($log_type, $all_logs)) {
      _bims_add_logs($form['logs'], $mcl_job, $log_type, $all_logs[$log_type]);
    }
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-job-upload-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_job_upload_form_submit';
  $form['#theme'] = 'bims_panel_add_on_job_upload_form';
  return $form;
}

/**
 * Returns the job information in table.
 *
 * @param MCL_JOB_UPLOAD $mcl_job
 */
function _bims_get_job_detail_table(MCL_JOB_UPLOAD $mcl_job) {

  // Adds the link to the uploaded file.
  $mcl_file = $mcl_job->getMCLFile();
  $link_template =  l('Download',  'mcl/download_file/' . $mcl_file->getFileID());

  // Adds the link to download all log files in zip.
  $link_log = l('download all', 'mcl/download_log/' . $mcl_job->getJobID());

  // Gets the compete date.
  $complete_date = $mcl_job->getCompleteDate();
  if (!$complete_date) {
    $complete_date = '<em>N/A</em>';
  }

  // Sets the status.
  $status = $mcl_job->getStatusLabel();
  $run_lock = $mcl_job->getJobDir() . '/run.lock';
  if (file_exists($run_lock)) {
    $status = "Running";

    // Checks process ID to make sure it is actually running.
    $pid = $mcl_job->getPID();
    $proc_file = "/proc/$pid";
    if (file_exists($proc_file)) {
      $status .= " [$pid]";
    }
    else {
      $status = "Failed";
      unlink($run_lock);
    }
  }

  // Sets the last run date.
  $last_run = $mcl_job->getPropByKey('last_run');
  $last_run_date = ($last_run) ? $last_run : '<em>N/A</em>';

  $rows = array();
  $rows []= array(array('data' => '<b>Job ID</b>', 'style' => 'width:160px;'), $mcl_job->getJobID());
  $rows []= array('<b>Filename</b>', $mcl_job->getFilename());
  $rows []= array('<b>Uploaded Template File</b>', $link_template);
  $rows []= array('<b>Status</b>', $status);

  // Adds progress row if not empty.
  $progress = $mcl_job->getProgress();
  if ($progress) {
    $rows []= array('<b>Progress</b>', $progress);
  }
  $rows []= array('<b>Last Run</b>', $last_run_date);
  $rows []= array('<b>Submit Date</b>', $mcl_job->getSubmitDate());
  $rows []= array('<b>Complete Date</b>', $complete_date);
  $rows []= array('<b>Log Files</b>', $link_log);

  // Table variables.
  $table_vars = array(
    'header'      => NULL,
    'rows'        => $rows,
    'attributes'  => array('style' => 'max-width:500px;'),
  );
  return theme('table', $table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_job_upload_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_job_upload_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Re-run' was clicked.
  if ($trigger_elem == 'rerun_btn') {
    $fid = $form_state['values']['job_rerun']['upload_file'];
    if (!$fid) {
      form_set_error('bims', t("No file selected"));
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_job_upload_form_submit($form, &$form_state) {

  // Gets BIMS_USER and MCL_USER.
  $bims_user  = getBIMS_USER();
  $mcl_user   = $bims_user->getMCL_USER();
  $user_dir   = $mcl_user->getUserDir();

  // Gets BIMS_FILE.
  $bims_file = $form_state['values']['bims_file'];

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Delete' was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Deletes the BIMS file.
    if (!$bims_file->delete()) {
      drupal_set_message('Error : Failed to delete the file', 'error');
    }
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_job_upload')));
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_di_file")));
  }

  // If 'Re-run' was clicked.
  else if ($trigger_elem == 'rerun_btn') {

    // Gets the Drupal file object.
    $drupal_file = file_load($form_state['values']['job_rerun']['upload_file']);
    if ($drupal_file) {

      // Gets the filepath.
      $filepath = drupal_realpath($drupal_file->uri);

      // Moves the file to the user temp directory.
      $dest_filepath    = $mcl_user->getUserDir('tmp') . '/' . $drupal_file->filename;
      $target_filepath  = drupal_realpath($drupal_file->uri);
      $cmd              = "mv \"$target_filepath\" \"$dest_filepath\"";
      exec($cmd);

      // Removes from file_managed.
      db_delete('file_managed')
        ->condition('fid', $drupal_file->fid, '=')
        ->execute();

      // Gets 'force' option.
      $opt_force = '';
      $force_cb = trim($form_state['values']['job_rerun']['advanced']['force_cb']);
      if ($force_cb) {
        $opt_force = '--force';
      }

      // Gets 'no_transaction' option.
      //$opt_no_transaction = '';
      //$trans = TRUE;
      //$no_transaction_cb = trim($form_state['values']['job_rerun']['advanced']['no_transaction_cb']);
      //if ($no_transaction_cb) {
        $opt_transaction = '--transaction_on';
        $trans = TRUE;
      //}

      // Gets MCL_JOB_UPLOAD and updates the properties.
      $mcl_job = $form_state['values']['mcl_job'];
      $mcl_job->resetProgress();
      $mcl_job->setPropByKey('transaction', $trans);
      $mcl_job->resetProgress();
      $mcl_job->resetCompleteDate();
      $mcl_job->setStatus(0);
      $mcl_job->update();

      // Sets stdout and stderr log file names.
      $stdout = $mcl_job->getStdFile('stdout');
      $stderr = $mcl_job->getStdFile('stderr');

      // Runs the job.
      $mcl_job_id = $mcl_job->getJobID();
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd = "$drush mcl-rerun-job $mcl_job_id \"$dest_filepath\" $opt_force $opt_transaction > $stdout 2>$stderr  & echo $!";
      $pid = exec($cmd, $output, $return_var);
      if ($return_var) {
        drupal_set_message("Error : Failed to add a uploading job", 'error');
      }

      // Updates the pid.
      $bims_file->setPropByKey('pid', $pid);
      $bims_file->update();
    }

    // Updates the panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_job_upload')));
    $url = 'bims/load_main_panel/bims_panel_add_on_job_upload/' . $bims_file->getFileID();
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_job_upload', 'Uploading Job', $url)));
  }
  else {

    // Updates the panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_job_upload')));
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_job_upload_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the job information table.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['job_details']) . "</div>";

  // Adds the rerun job.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['job_rerun']) . "</div>";

  // Adds the logs.
  $layout .= "<div style='float:left;width:100%;'>" . drupal_render($form['logs']) . "</div>";


  $layout .= drupal_render_children($form);
  return $layout;
}

/**
 * Adds the error logs to the form.
 *
 * @param array $form
 * @param MCL_UPLOAD_JOB $mcl_job
 * @param string $type
 * @param array $logs
 */
function _bims_add_logs(&$form, MCL_JOB_UPLOAD $mcl_job, $log_type, $logs) {
  $job_id     = $mcl_job->getJobID();
  $title      = $mcl_job->getMessageType($log_type) . ' Logs';
  $id_form    = 'bims_log_' . $log_type;
  $id_viewer  = 'bims_id_log_viewer_' . $log_type;
  $id_table   = 'bims_id_table_' . $log_type;
  $id_anchor  = 'bims_anchor_' . $log_type;

  // Adds the anchor.
  $form[$id_anchor] = array(
    '#markup' => "<a name='$id_anchor' style='display:block;position:relative;top:-40px;'></a>",
  );

  // Adds the log form.
  $form[$id_form] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => ($log_type != 'E'),
    '#collapsible'  => TRUE,
    '#title'        => $title,
  );

  // Creates a table for each status.
  $table = '';
  $total_row = 0;
  foreach ($logs as $status_int => $log_files) {
    $total_row += 2;

    // Populates the rows.
    $rows = array();
    foreach ($log_files as $log_file) {
      $total_row++;
      $url = "/mcl/ajax_view_log/$job_id/" . $log_file['log_file'];

      // Adds the link to view if the size of the log file is less than
      // 15 MB (15000000 B).
      $filepath = $log_file['filepath'];
      $link_view = '<em>N/A</em>';
      if (file_exists($filepath) && filesize($filepath) < 15000000) {
        $tmp_name = $log_file['key'];
        $link_view = "<a href='#$id_anchor' class='mcl_log' viewer-id='$id_viewer' tmp-name='[$tmp_name]' log-file='$url'>view</a>";
      }
      $row = array(
        $log_file['key'],
        $link_view,
      );
      if ($log_type == 'N') {
        $row []= $log_file['date'];
      }
      $rows []= $row;
    }

    // Table variables.
    $column_name = 'Template Name';
    if ($status_int == 20) {
      $column_name = 'Type';
    }

    // Adds headers.
    $headers = array(
      $column_name,
      array('data' => 'View', 'style' => 'width:28px;'),
    );
    if ($log_type == 'N') {
      $headers []= array('data' => 'Uploaded Date', 'style' => 'width:175px;');
    }

    // Sets table properties.
    $table_width = 'width:330px;';
    if ($log_type == 'N') {
      $table_width = 'width:470px;';
    }

    // Creates a table.
    $table_vars = array(
      'header'      => $headers,
      'rows'        => $rows,
      'empty'       => 'No logs',
      'attributes'  => array('style' => $table_width),
    );

    // Adds the caption.
    $caption = ucwords($mcl_job->trStatusIntToLabel($status_int));
    if (!preg_match("/[ND]/", $log_type)) {
      $table_vars['caption'] = "<b>$caption</b>";
    }
    $table .= theme('table', $table_vars);
  }

  // Creates two horizontal div and show log tale on the left and log viewer on the right.
  $div = "
    <div id='$id_viewer' style='overflow-y:scroll;height:150px;width:98%;padding:0px;border:1px solid lightgray;'>
    <div style='margin:0px 0px 0px 5px;'><em>Please click <b>view</b> on the table</em>.<br /></div>
    </div>
    <div id='$id_table' style='margin-top:15px;width:98%;'>$table</div>
    <br clear='all'>
  ";

  // Adds to the error logs.
  $form[$id_form]['table'] = array(
  '#markup' => $div,
  );
}
