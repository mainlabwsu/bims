<?php
/**
 * @file
 */
/**
 * Trait data import panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_trait_form($form, &$form_state, $cvterm_id = NULL) {

  // Gets BIMS_USER and BIMS__PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_di_trait');
  if (!$bims_panel->init($form, TRUE, TRUE)) {
    return $form;
  }

  // Adds the list of traits.
  $form['trait_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Import Public Traits'),
    '#attributes'   => array(),
  );
  _bims_panel_main_di_trait_form_get_trait_list($form['trait_list'], $bims_program);

  // Adds the trait table.
  $form['trait_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Trait Details'),
    '#attributes'   => array(),
    '#prefix'       => '<div id="bims-panel-main-di-trait-form-trait-details">',
    '#suffix'       => '</div>',
  );
  $form['trait_details']['cvterm_id'] = array(
    '#type'   => 'value',
    '#value'  => $cvterm_id,
  );
  $form['trait_details']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['trait_details']['#theme'] = 'bims_panel_main_di_trait_form_trait_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-di-trait-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_di_trait_form_submit';
  $form['#theme'] = 'bims_panel_main_di_trait_form';
  return $form;
}

/**
 * Creates the trait list.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_main_di_trait_form_get_trait_list(&$form, BIMS_PROGRAM $bims_program) {

  // Gets the crop.
  $bims_crop = $bims_program->getCrop();
  $crop_label = $bims_crop->getLabel();

  // Gets the trait descriptor set of this crop.
  $cv_id = $bims_crop->getTraitDescriptorSet();

  if (!$cv_id) {
    $form['no_cv_id'] = array(
      '#markup' => "<div><em>No public trait descriptor set has been assigned to <b>'$crop_label'.</b></em></div>",
    );
    return;
  }

  // Gets all traits of this program.
  $p_descriptors  = BIMS_MVIEW_DESCRIPTOR::getDescriptors($bims_program->getProgramID(), $bims_program->getCvID('descriptor'));
  $map            = array();
  foreach ($p_descriptors as $p_descriptor) {
    $map[strtolower($p_descriptor->name)] = $p_descriptor->cvterm_id;
  }

  // Gets all the public traits that belongs to.
  $format = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'format')->getCvtermID();
  $sql = "
    SELECT C.cvterm_id, C.name, CP.value AS format, LOWER(C.name) AS name_lc
    FROM chado.cvterm C
      INNER JOIN chado.cvtermprop CP on CP.cvterm_id = C.cvterm_id
    WHERE C.cv_id = :cv_id AND CP.type_id = :format
    ORDER BY C.name
  ";
  $args = array(
    ':cv_id'  => $cv_id,
    ':format' => $format,
  );
  $traits = array();
  $results = db_query($sql, $args);
  $count = 0;
  while ($obj = $results->fetchObject()) {
    $status = 'N/A';
    if (!array_key_exists($obj->name_lc, $map)) {
      $count++;
      $form['trait_cb'][$obj->cvterm_id] = array(
        '#title'          => '',
        '#type'           => 'checkbox',
        '#default_value'  => 0,
        '#attributes'     => array('style' => 'margin-left:10px;'),
      );
      $status = 'available';
    }
    $traits []= array(
      'cvterm_id' => $obj->cvterm_id,
      'name'      => $obj->name,
      'format'    => $obj->format,
      'status'    => $status,
    );
  }

  // No trait found.
  if (empty($traits)) {
    $form['no_traits'] = array(
      '#markup' => "<div><em><b>'$crop_label'</b> does not have public trait.</em></div>",
    );
    return;
  }

  // Saves some variables for theming.
  $form['traits'] = array(
    '#type'   => 'value',
    '#value'  => $traits,
  );
  $prefix   = 'N/A : name conflict or has alreday been imported.';
  $disabled = FALSE;
  if (!$count) {
    $disabled = TRUE;
    $prefix .= '<br />No trait is availble to import.';
  }
  $form['import_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'import_btn',
    '#value'      => 'Import trait(s)',
    '#disabled'   => $disabled,
    '#prefix'     => "<div><em>$prefix</em></div>",
    '#attributes' => array(
      'style' => 'width:180px;',
      'class' => array('use-ajax'),
    ),
    '#ajax'       => array(
      'callback' => "bims_panel_main_di_trait_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-di-trait-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['#theme'] = 'bims_panel_main_di_trait_form_trait_list';
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_trait_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // 'Import trait(s)' was clicked.
  if ($trigger_elem == 'import_btn') {

    // Imports the traits.
    $transaction = db_transaction();
    try {

      // Imports the traits.
      foreach ((array)$form_state['values']['trait_list']['trait_cb'] as $cvterm_id => $bool) {
        if ($bool) {
          if (!$bims_program->importTrait($cvterm_id)) {
            throw new Exception("Error : Failed to import trait ($cvterm_id)");
          }
        }
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
    }
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_di_trait')));
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_trait_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // 'Import trait(s)' was clicked.
  if ($trigger_elem == 'import_btn') {

      // Checks the checkbox.
      $num_checked = 0;
      foreach ((array)$form_state['values']['trait_list']['trait_cb'] as $cvterm_id => $bool) {
        if ($bool) {
          $num_checked++;
        }
      }
      if (!$num_checked) {
        form_set_error('trait_list][trait_cb', "Please choose at least one descriptor.");
      }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_di_trait_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_trait_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the trait list.
  $layout .= "<div style='float:left;width:54%;'>" . drupal_render($form['trait_list']) . "</div>";

  // Adds the trait details.
  $layout .= "<div style='float:left;width:45%;margin-left:5px;'>" . drupal_render($form['trait_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
 }

  /**
 * Theme function for the trait list section.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_trait_form_trait_list($variables) {
  $element = $variables['element'];

  // Gets the passed values.
  $traits     = $element['traits']['#value'];
  $trait_elem = $element['trait_cb'];
  $import_btn = $element['import_btn'];

  // Lists the traits.
  $total = 0;
  foreach ($traits as $trait) {
    $total++;
    $cvterm_id = $trait['cvterm_id'];
    $view_link = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel('bims_panel_main_di_trait/$cvterm_id')\">view</a>";;
    $checkbox = '';
    if ($trait['status'] == 'available') {
      $checkbox = drupal_render($trait_elem[$cvterm_id]);
    }
    $rows []= array(
      $checkbox,
      $trait['name'],
      $trait['format'],
      '<em>' . $trait['status'] . '</em>',
      $view_link,
    );
  }
  $layout = "<div>There are <b>$total</b> public traits.</div>";

  // Adds the descriptor table.
  $headers = array(
    array('data' => '', 'with' => 1),
    array('data' => 'Descriptor&nbsp;Name', 'with' => 40),
    array('data' => 'Format',  'with' => 20),
    array('data' => 'Status',  'with' => 20),
    array('data' => 'details', 'with' => 10),
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $table = theme('table', $table_vars);
  $layout .= "<div style='max-height:400px;overflow-y:auto;'>$table</div>";
  $layout .= '<div>' . drupal_render($import_btn) . '</div>';
  return $layout;
}

 /**
 * Theme function for the trait table section.
 *
 * @param $variables
 */
function theme_bims_panel_main_di_trait_form_trait_table($variables) {
  $element = $variables['element'];

  // Gets the passed values.
  $bims_program = $element['bims_program']['#value'];
  $cvterm_id    = $element['cvterm_id']['#value'];

  // Sets the initial trait information.
  $name           = '--';
  $alias          = '--';
  $format         = '--';
  $def            = '--';
  $data_unit      = '--';
  $categories     = '';
  $default        = '';
  $minimum        = '';
  $maximum        = '';
  $trait_category = '';

  // Updates the trait information.
  if ($cvterm_id) {

    // Gets the cvterm ID.
    $alias      = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'alias')->getCvtermID();
    $format     = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'format')->getCvtermID();
    $categories = MCL_CHADO_CVTERM::getCvterm('BIMS_FIELD_BOOK_FORMAT_PROP', 'categories')->getCvtermID();
    $default    = MCL_CHADO_CVTERM::getCvterm('BIMS_FIELD_BOOK_FORMAT_PROP', 'defaultValue')->getCvtermID();
    $minimum    = MCL_CHADO_CVTERM::getCvterm('BIMS_FIELD_BOOK_FORMAT_PROP', 'minimum')->getCvtermID();
    $maximum    = MCL_CHADO_CVTERM::getCvterm('BIMS_FIELD_BOOK_FORMAT_PROP', 'maximum')->getCvtermID();
    $data_unit  = MCL_CHADO_CVTERM::getCvterm('BIMS_FIELD_BOOK_FORMAT_PROP', 'data_unit')->getCvtermID();
    $is_a       = MCL_CHADO_CVTERM::getCvterm('relationship', 'is_a')->getCvtermID();

    // Gets the trait inforamtion.
    $sql = "
      SELECT C.name, C.definition, FORMAT.value AS format, ALIAS.aliases,
        CAT.categories, DEF.default, MIN.minimum, MAX.maximum, DU.data_unit,
        TRAIT_CAT.trait_category
      FROM chado.cvterm C
        INNER JOIN chado.cvtermprop FORMAT on FORMAT.cvterm_id = C.cvterm_id
        LEFT JOIN (
          SELECT cvterm_id, STRING_AGG(value, '<br />') AS aliases
          FROM chado.cvtermprop
          WHERE type_id = :alias
          GROUP BY cvterm_id
        ) ALIAS on ALIAS.cvterm_id = C.cvterm_id
        LEFT JOIN (
          SELECT value AS categories, cvterm_id
          FROM chado.cvtermprop
          WHERE type_id = :categories
        ) CAT on CAT.cvterm_id = C.cvterm_id
         LEFT JOIN (
          SELECT value AS default, cvterm_id
          FROM chado.cvtermprop
          WHERE type_id = :default
        ) DEF on DEF.cvterm_id = C.cvterm_id
        LEFT JOIN (
          SELECT value AS minimum, cvterm_id
          FROM chado.cvtermprop
          WHERE type_id = :minimum
        ) MIN on MIN.cvterm_id = C.cvterm_id
        LEFT JOIN (
          SELECT value AS maximum, cvterm_id
          FROM chado.cvtermprop
          WHERE type_id = :maximum
        ) MAX on MAX.cvterm_id = C.cvterm_id
        LEFT JOIN (
          SELECT value AS data_unit, cvterm_id
          FROM chado.cvtermprop
          WHERE type_id = :data_unit
        ) DU on DU.cvterm_id = C.cvterm_id
        LEFT JOIN (
          SELECT CR.object_id, C.name AS trait_category
          FROM chado.cvterm C
            INNER JOIN chado.cvterm_relationship CR on CR.subject_id = C.cvterm_id
          WHERE CR.type_id = :is_a
        ) TRAIT_CAT on TRAIT_CAT.object_id = C.cvterm_id
      WHERE C.cvterm_id = :cvterm_id AND FORMAT.type_id = :format
    ";
    $args = array(
      ':cvterm_id'  => $cvterm_id,
      ':alias'      => $alias,
      ':format'     => $format,
      ':categories' => $categories,
      ':default'    => $default,
      ':minimum'    => $minimum,
      ':maximum'    => $maximum,
      ':data_unit'  => $data_unit,
      ':is_a'       => $is_a,
    );
    $obj        = db_query($sql, $args)->fetchObject();
    $name       = $obj->name;
    $alias      = $obj->alias;
    $format     = $obj->format;
    $alias      = $obj->alias;
    $def        = $obj->definition;
    $categories = $obj->categories;
    $default    = $obj->default;
    $minimum    = $obj->minimum;
    $maximum    = $obj->maximum;
    $data_unit  = $obj->data_unit;

    // Updates properties.
    $alias    = ($alias) ? $alias : '<em>N/A</em>';
    $def      = ($def) ? "<textarea style='width:100%;' rows=3 READONLY>$def</textarea>" : '<em>N/A</em>';
  }

  // Creates the details table.
  $rows = array();
  if ($trait_category) {
    $rows[] = array('Trait&nbsp;category', $trait_category);
  }
  $rows[] = array(array('data' => 'Name', 'width' => '100'), $name);
  $rows[] = array('Alias', $alias);
  $rows[] = array('Format', $format);
  if ($categories) {
    $rows[] = array('Categories', $categories);
  }
  if ($default || $default == '0') {
    $rows[] = array('Default Value', $default);
  }
  if ($minimum || $minimum == '0') {
    $rows[] = array('Minimum', $minimum);
  }
  if ($maximum || $maximum == '0') {
    $rows[] = array('Maximum', $maximum);
  }
  if ($data_unit) {
    $rows[] = array('Data Unit', $data_unit);
  }
  $rows[] = array('Definition', $def);
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}
