<?php
/**
 * @file
 */
/**
 * Results for search accession by genotype form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sag_result_form($form, &$form_state, $list_id = NULL) {

  // Local variables for form properties.
  $min_height = 500;
  $callback   = 'bims_panel_add_on_sag_result_form_ajax_callback';
  $wrapper    = 'bims-panel-add-on-sag-result-form';

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $accession    = $bims_program->getBIMSLabel('accession');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_sag_result');
  $bims_panel->init($form);

  // Search the filter in SESSION.
  $bims_list = NULL;
  if ($list_id == 'SESSION_FILTER' || !$list_id) {
    $list_id = NULL;

    // Gets the filter.
    $filter = bims_get_session('SESSION_FILTER');
    if (!$filter) {
      $form['session'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => FALSE,
        '#title'        => 'SESSION EXPIRED',
        '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
      );
      $form['session']['session_expire'] = array(
        '#markup' => '<em>Session expired.</em>',
      );
      return $form;
    }
  }

  // Checks the list ID.
  else {
    $bims_list = BIMS_LIST::byID($list_id);
    if (!$bims_list) {
      $form['list_id'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => FALSE,
        '#title'        => 'Invalid List ID',
        '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
      );
      $form['list_id']['invalid'] = array(
        '#markup' => "<em>The list ID ($list_id) is not valid.</em>",
      );
      return $form;
    }
    else {
      $filter = $bims_list->getPropByKey('filter');
    }
  }

  // Saves the BIMS_LIST and the filter.
  $form['bims_list'] = array(
    '#type'   => 'value',
    '#value'  => $bims_list,
  );
  $form['filter'] = array(
    '#type'   => 'value',
    '#value'  => $filter,
  );

  // Gets the number of phenotypic and  genotypic data.
  $count_pd   = $filter['count_pd'];
  $count_gd   = $filter['count_gd'];
  $no_data_pd = ($filter['count_pd']) ? FALSE : TRUE;
  $no_data_gd = ($filter['count_gd']) ? FALSE : TRUE;

  // Gets the preference.
  $preference = $filter['preference'];

  // Adds the columns options for phenotype.
  if (!$no_data_pd) {
    $form['opt_pcolumn'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Column options for phenotype',
    );

    // Adds accession properties.
    _bims_panel_add_on_result_form_opt_pcolumn_accession_prop($form['opt_pcolumn'], 'bims_aosag_pc_ap', $preference, $bims_program);

    // Adds sample properties.
    _bims_panel_add_on_result_form_opt_pcolumn_sample_prop($form['opt_pcolumn'], 'bims_aosag_pc_sp', $preference, $bims_program);

    // Adds the trait selection.
    _bims_panel_add_on_result_form_opt_pcolumn_trait($form['opt_pcolumn'], 'bims_aosag_pc_trait', $preference);
    $form['opt_pcolumn']['#theme'] = 'bims_panel_add_on_result_form_opt_pcolumn';
  }

  // Adds the columns options for genotype.
  $form['opt_gcolumn'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Column options for genotype',
  );

  // Adds accession properties.
  _bims_panel_add_on_result_form_opt_gcolumn_accession_prop($form['opt_gcolumn'], 'bims_aosag_gc_ap', $preference, $bims_program);

  // Adds marker properties.
  _bims_panel_add_on_result_form_opt_gcolumn_marker_prop($form['opt_gcolumn'], 'bims_aosag_gc_mp', $preference, $bims_program);

  // Adds genotype properties.
  _bims_panel_add_on_result_form_opt_gcolumn_genotype_prop($form['opt_gcolumn'], 'bims_aosag_gc_gp', $preference, $bims_program);
  $form['opt_gcolumn']['#theme'] = 'bims_panel_add_on_result_form_opt_gcolumn';

  // Adds 'Save Search' section.
  if (!$list_id) {
    $form['save'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Save Search',
    );

    // Shows the message for anonymous user.
    if ($bims_user->isAnonymous()) {
      $form['save']['save_msg'] = array(
        '#markup' => '<em>You are required to have an account to save the results.</em>',
      );
    }
    else {
      $form['save']['save_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'save_btn',
        '#value'      => 'Save',
        '#disabled'   => $no_data_gd,
        '#attributes' => array('style' => 'width:140px;'),
        '#ajax'       => array(
          'callback' => $callback,
          'wrapper'  => 'bims-panel-add-on-sag-result-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['save']['list_name'] = array(
        '#type'       => 'textfield',
        '#name'       => 'list_name',
        '#attributes' => array('style' => 'width:300px;'),
      );
      $form['save']['list_desc'] = array(
        '#type'       => 'textarea',
        '#name'       => 'list_desc',
        '#rows'       => 2,
        '#attributes' => array('style' => 'width:300px;height:45px;'),
      );
    }
    $form['save']['#theme'] = 'bims_panel_add_on_result_form_save';
  }

  // Adds 'Download Data' section.
  $form['download'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Download Data',
  );

  // Adds download 'phenotype'.
  if (!$no_data_pd) {
    $form['download']['file_form_phenotype'] = array(
      '#type'           => 'radios',
      '#options'        => array('wide' => 'Wide Form', 'long' => 'Long Form'),
      '#default_value'  => 'wide',
      '#attributes'     => array('style' => 'width:10px;'),
    );
    $form['download']['stats'] = array(
      '#type'           => 'checkbox',
      '#title'          => 'Statistics',
      '#default_value'  => TRUE,
      '#attributes'     => array('style' => 'width:10px;'),
    );
    $form['download']['download_phenotype_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'download_phenotype_btn',
      '#value'      => 'Download',
      '#attributes' => array('style' => 'width:140px'),
      '#ajax'       => array(
        'callback' => $callback,
        'wrapper'  => 'bims-panel-add-on-sag-result-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds download 'genotype'.
  if (!$no_data_gd) {
    $file_form_genotype = array(
      'long'            => 'Long Form',
      'wide_marker'     => 'Wide Form (Marker columns)',
      'wide_accession'  => "Wide Form ($accession columns)",
      'vcf'             => 'VCF',
    );
    if ($bims_user->getName() != 'ltaein') {
      unset($file_form_genotype['vcf']);
    }

    $form['download']['file_form_genotype'] = array(
      '#type'           => 'radios',
      '#options'        => $file_form_genotype,
      '#default_value'  => 'long',
      '#attributes'     => array('style' => 'width:10px;'),
    );
    $form['download']['download_genotype_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'download_genotype_btn',
      '#value'      => 'Download',
      '#disabled'   => $no_data_gd,
      '#attributes' => array('style' => 'width:140px'),
      '#ajax'       => array(
        'callback' => $callback,
        'wrapper'  => 'bims-panel-add-on-sag-result-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['download']['accession'] = array(
    '#type'   => 'value',
    '#value'  => $accession . 's',
  );
  $form['download']['#theme'] = 'bims_panel_add_on_result_form_download';

  // Adds 'Stats' section.
  if (FALSE){
  //if (!$no_data_pd) {
    $form['stats'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => "Statistics",
    );

    // Gets the stats.
    $stats_arr = array();
    if (array_key_exists('stats_arr', $filter)) {
      $stats_arr = $filter['stats_arr'];
    }

    // Adds 'Calc Stats' button if $stats_arr is empty.
    if (empty($stats_arr)) {
      $session_id = $bims_user->getSessionID('sap-stats');
      $form['stats']['stats_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'stats_btn',
        '#value'      => 'Compute Statistics',
        '#attributes' => array('style' => 'width:200px'),
        '#ajax'       => array(
          'callback' => $callback,
          'wrapper'  => 'bims-panel-add-on-sag-result-form',
          'effect'   => 'fade',
          'method'   => 'replace',
          'progress' =>  array(
            'type'      => 'bar',
            'message'   => t('Computing the stats ...'),
            'url'       => url("bims/ajax_progressbar_sr_stats/$session_id"),
            'interval'  => '1000', // progress bar will refresh in 1 (1000ms) second.
          ),
        ),
      );
    }

    // Shows the stats.
    else {

      // Gets the selected cvterm ID.
      $cvterm_id = NULL;
      if (isset($form_state['values']['trait_stats']['trait_list'])) {
        $cvterm_id = $form_state['values']['trait_stats']['trait_list']['selection'];
      }

      // Adds the stats section.
      $form['trait_stats'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => TRUE,
        '#title'        => 'Statistics',
      );
      _bims_panel_add_on_result_form_add_stats($filter['program_id'], $form['trait_stats'], $callback, $wrapper, $stats_arr, $cvterm_id);
    }
  }

  // Adds 'Results' section.
  $count = $filter['count_gd'];
  $form['result_table'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => "Results : matched $count genotypic measurements",
  );

  // Adds the description.
  $form['result_table']['description'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Descriptions',
  );
  _bims_panel_add_on_result_form_get_search_desc($form['result_table']['description'], $filter);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-sag-result-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_sag_result_form_submit';
  $form['#theme'] = 'bims_panel_add_on_sag_result_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sag_result_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Compute Statistics' was clicked.
  if ($trigger_elem == 'stats_btn') {

    // Gets the session ID.
    $session_id = $bims_user->getSessionID('sag-stats');

    // Calls the drush command to calculates the stats.
    $drush = bims_get_config_setting('bims_drush_binary');
    $cmd = "$drush bims-compute-sr-stats $session_id";
    exec("$cmd > /dev/null 2>/dev/null");
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sag_result_form_validate($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' was clicked.
  if ($trigger_elem == 'save_btn') {

    // Gets the form values.
    $form_state['values']['save']['list_name'] = trim($form_state['input']['list_name']);
    $form_state['values']['save']['list_desc'] = trim($form_state['input']['list_desc']);
    $list_name = $form_state['values']['save']['list_name'];
    $filter    = $form_state['values']['filter'];
    $bims_list = $form_state['values']['bims_list'];

    // Checks list name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-]+$/", $list_name)) {
      $msg = 'Invalid name. Please type valid name. ' .
             'Valid name : non-empty, alpha-numeric, dash and underscore.';
      form_set_error('list_prop][list_name', $msg);
      return;
    }

    // Gets the current list name and list type.
    $cur_list_name = '';
    $list_type = $filter['list_type'];
    if ($bims_list) {
      $cur_list_name  = $bims_list->getName();
      $list_type      = $bims_list->getType();
    }

    // Checks for a duplication if list names are different.
    if ($cur_list_name != $list_name) {
      $keys = array(
        'type'        => $list_type,
        'program_id'  => $bims_user->getProgramID(),
        'user_id'     => $bims_user->getUserID(),
        'name'        => $list_name,
      );
      $bims_list = BIMS_LIST::byKey($keys);
      if ($bims_list) {
        form_set_error('list_prop][list_name', "The name ($list_name) has been used. Please choose other name.");
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sag_result_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_list';

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the form values.
  $filter     = $form_state['values']['filter'];
  $bims_list  = $form_state['values']['bims_list'];

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' was clicked.
  if ($trigger_elem == 'save_btn') {

    // Gets the list name and descriptions.
    $list_name  = trim($form_state['values']['save']['list_name']);
    $list_desc  = trim($form_state['values']['save']['list_desc']);

    // Sets the properties of the list.
    $prop = array(
      'filter'        => $filter,
      'stats_status'  => 'computing',
    );
    $details = array(
      'name'        => $list_name,
      'type'        => 'sample',
      'program_id'  => $bims_user->getProgramID(),
      'user_id'     => $bims_user->getUserID(),
      'create_date' => date("Y-m-d G:i:s"),
      'description' => $list_desc,
      'prop'        => json_encode($prop),
    );

    // Saves the search in BIMS_LIST.
    $bims_list = new BIMS_LIST($details);
    if ($bims_list->insert()) {
      drupal_set_message("The search has been saved in your account");
    }
    else {
      drupal_set_message("Error : Failed to save the search", 'error');
    }
  }

  // If 'Download' for genotype was clicked.
  else if ($trigger_elem == 'download_genotype_btn') {

    // Gets options.
    $file_form = $form_state['values']['download']['file_form_genotype'];

    // Updates the preference.
    $preference = $filter['preference'];
    foreach ((array)$form_state['values']['opt_gcolumn']['accession_prop']['gc_accessionprop']['values'] as $field => $value) {
      $preference['gc_accessionprop'][$field]['value'] = $value;
    }
    foreach ((array)$form_state['values']['opt_gcolumn']['marker_prop']['markerprop']['values'] as $field => $value) {
      $preference['markerprop'][$field]['value'] = $value;
    }
    foreach ((array)$form_state['values']['opt_gcolumn']['genotype_prop']['genotypeprop']['values'] as $field => $value) {
      $preference['genotypeprop'][$field]['value'] = $value;
    }
    $filter['preference'] = $preference;
    $filter['list_type'] = 'genotype';

    // Sets the file type.
    $file_type = 'csv';
    $ext       = 'csv';
    if ($file_form == 'vcf') {
      $file_type  = 'vcf';
      $ext        = 'vcf';
    }

    // Gets the filepath of the downloand file.
    $args = array(
      'type'  => 'download',
      'ext'   => $ext,
      'keys'  => array($program_id, 'genotype'),
      'time'  => TRUE,
    );
    $filepath = $bims_user->getFilepath($args);

    // Creates a downloading file.
    $param = array(
      'file_type' => $file_type,
      'file_form' => $file_form,
      'filter'    => $filter,
      'status'    => '0%',
      'data_type' => 'SNP genotype',
    );
    $details = array(
      'filepath'    => $filepath,
      'filename'    => basename($filepath),
      'type'        => 'download',
      'filesize'    => 0,
      'user_id'     => $bims_user->getUserID(),
      'program_id'  => $program_id,
      'description' => 'Genotyping data file',
      'submit_date' => date("Y-m-d G:i:s"),
      'prop'        => json_encode($param),
    );
    $bims_file = new BIMS_FILE($details);
    if ($bims_file->insert()) {

      // Call drush command to perform comparison.
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd = "$drush bims-download " . $bims_file->getFileID();
      exec("$cmd > /dev/null 2>/dev/null &");
      drupal_set_message("Downloading file will be created in your account");
    }
    else {
      drupal_set_message("Error : Failed to create a downloading file", 'error');
    }
  }

  // If 'Download' for phenotype was clicked.
  else if ($trigger_elem == 'download_phenotype_btn') {

    // Gets options.
    $file_form  = $form_state['values']['download']['file_form_phenotype'];
    $stats      = $form_state['values']['download']['stats'];

    // Updates the preference.
    $preference = $filter['preference'];
    foreach ((array)$form_state['values']['opt_pcolumn']['accession_prop']['pc_accessionprop']['values'] as $field => $value) {
      $preference['pc_accessionprop'][$field]['value'] = $value;
    }
    foreach ((array)$form_state['values']['opt_pcolumn']['sample_prop']['sampleprop']['values'] as $field => $value) {
      $preference['sampleprop'][$field]['value'] = $value;
    }
    foreach ((array)$form_state['values']['opt_pcolumn']['trait']['descriptor']['values'] as $field => $value) {
      $preference['descriptor'][$field]['value'] = $value;
    }
    $filter['preference'] = $preference;

    // Creates a downloading file.
    $param = array(
      'file_type' => 'csv',
      'file_form' => $file_form,
      'stats'     => $stats,
    );
    $filter['list_type'] = 'sample';
    $file = BIMS_LIST::saveContents($bims_user, $filter, $param);
    if ($file) {

      // Redirects to the download link.
      $url = 'bims/download_file/' . $file->fid;
      bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
    }
    else {
      drupal_set_message("Error : Failed to create a downloading file", 'error');
    }
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_sag_result_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds 'Column options for phenotype' section.
  if (array_key_exists('opt_pcolumn', $form)) {
    $layout .= "<div style='width:100%;'>" . drupal_render($form['opt_pcolumn']) . "</div>";
  }

  // Adds 'column options for genotype' section.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['opt_gcolumn']) . "</div>";

  // Adds 'Save Search' section.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['save']) . "</div>";

  // Adds 'Download Data' section.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['download']) . "</div>";

  // Adds 'Statistics' section.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['stats']) . "</div>";

  // Adds 'Results' section.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['result_table']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}
