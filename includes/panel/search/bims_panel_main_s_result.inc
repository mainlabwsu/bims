<?php
/**
 * @file
 */
/**
 * Search result panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_s_result_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 300;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets BIMS_LIST.
  $bims_list = NULL;
  $list_id = '';
  if (isset($form_state['values']['saved_search']['selection'])) {
    $list_id = $form_state['values']['saved_search']['selection'];
    $bims_list = BIMS_LIST::byID($list_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_s_result');
  if (!$bims_panel->init($form, TRUE)) {
    return $form;
  }

  // Lists all the saved searches.
  $form['saved_search'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Saved Searches',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets the saved results.
  $options = $bims_user->getSavedData('search_result', BIMS_OPTION);
  if (empty($options)) {
    $form['saved_search']['no_list'] = array(
      '#markup' => "No saved search is found.",
    );
  }
  else {
    $form['saved_search']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#validated'      => TRUE,
      '#size'           => 12,
      '#default_value'  => $list_id,
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_s_result_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-s-result-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // List details.
  $form['result_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  if ($bims_list) {
    $form['result_details']['table']['report_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'report_btn',
      '#value'      => 'Generate Report',
      '#attributes' => array('style' => 'width:150px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_s_result_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-s-result-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['result_details']['table']['delete_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'delete_btn',
      '#value'      => 'Delete',
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_s_result_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-s-result-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['result_details']['table']['edit_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'edit_btn',
      '#value'      => 'Edit',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_s_result_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-s-result-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    hide($form['result_details']['table']['edit_btn']);
    $form['result_details']['table']['view_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'view_btn',
      '#value'      => 'View',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_s_result_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-s-result-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Hides some buttons.
    if ($bims_list->getUserID() != $bims_user->getUserID()) {
      hide($form['result_details']['table']['delete_btn']);
      hide($form['result_details']['table']['edit_btn']);
    }
  }
  $form['result_details']['table']['bims_list'] = array(
    '#type'   => 'value',
    '#value'  => $bims_list,
  );
  $form['result_details']['table']['#theme'] = 'bims_panel_main_s_result_form_details_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-s-result-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_s_result_form_submit';
  $form['#theme'] = 'bims_panel_main_s_result_form';
  return $form;
}

/**
 * Deletes the list.
 *
 * @param integer $list_id
 *
 * @return boolean
 */
function _bims_panel_main_s_result_form_delete_list($list_id) {

  $transaction = db_transaction();
  try {
    $bims_list = BIMS_LIST::byKey(array('list_id' => $list_id));
    if (!$bims_list->delete()) {
      throw new Exception("Error : Failed to delete the list");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_s_result_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets BIMS_LIST.
  $bims_list = $form['result_details']['table']['bims_list']['#value'];
  if ($bims_list) {
    $list_id = $bims_list->getListID();

    // If "Delete" was clicked.
    if ($trigger_elem == 'delete_btn') {

      // Deletes the list.
      _bims_panel_main_s_result_form_delete_list($list_id);

      // Updates panels.
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_s_result')));
    }

    // If 'Edit' was clicked.
    else if ($trigger_elem == 'edit_btn') {

      // Edit the list.
      $url = "bims/load_main_panel/bims_panel_add_on_list_edit/$list_id";
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_list_edit', 'Edit List', $url)));
    }

    // If 'Delete' was clicked.
    if ($trigger_elem == 'delete_btn') {

      // Deletes the current list.
      if (!$bims_list->delete()) {
        drupal_set_message("Error : Failed to delete the list", 'error');
      }
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_s_result')));
    }

    // If "View" was clicked.
    else if ($trigger_elem == 'view_btn') {
      $filter_type = $bims_list->getFilterType();
      $url = "bims/load_main_panel/bims_panel_add_on_sag_result/" . $bims_list->getListID();
      if ($filter_type == 'SEARCH_PHENOTYPE') {
        $url = "bims/load_main_panel/bims_panel_add_on_sap_result/" . $bims_list->getListID();
      }
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sap_result', 'Search Results', $url)));
    }

    // If "Report" was clicked.
    else if ($trigger_elem == 'report_btn') {
      $url = "bims/load_main_panel/bims_panel_add_on_accession_report/list/$list_id";
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_accession_report', "Report", $url)));
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_s_result_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_s_result_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets values from the form_state.
  $bims_list = $form_state['values']['result_details']['table']['bims_list'];

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Download' was clicked.
  if ($trigger_elem == 'download_btn') {

    // Creates a downloading file.
    $filter = $bims_list->getPropByKey('filter');
    $file = BIMS_LIST::saveContents($bims_user, $filter, array('file_type' => 'csv'));
    if ($file) {

      // Redirects to the download link.
      $url = 'bims/download_file/' . $file->fid;
      bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
    }
    else {
      BIMS_MESSAGE::addMsg("Error : Failed to create a downloading file", 'error');
    }
  }
}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_s_result_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), 'Create a new list.');
  $rows[] = array(drupal_render($element['merge_btn']), 'Merge lists.');
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the list information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_s_result_form_details_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_LIST.
  $bims_list = $element['bims_list']['#value'];

  // Initializes the properties.
  $name         = '--';
  $filter_type  = '--';
  $saved_date   = '--';
  $desc         = '--';
  $actions      = '';

  // Updates the properites.
  if ($bims_list) {
    $name       = $bims_list->getName();
    $type       = $bims_list->getType();
    $desc       = "<textarea cols=50 rows=3 READONLY>" . $bims_list->getDescription() . "</textarea>";
    $saved_date = $bims_list->getCreateDate();

    // Adds buttons to the action row.
    $actions = '';
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }
    if (array_key_exists('view_btn', $element)) {
      $actions .= drupal_render($element['view_btn']);
    }

    // Gets the filter.
    $filter_type = $bims_list->getFilterLabel();
  }

  // Adds the list info table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'style' => 'width:100px;'), $name);
  $rows[] = array('Search Type', $filter_type);
  $rows[] = array('Saved Date', $saved_date);
  $rows[] = array('Description', $desc);
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }

  // Creates the details table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the list form.
 *
 * @param $variables
 */
function theme_bims_panel_main_s_result_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "Lists" list.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['saved_search']) . "</div>";

  // Adds "Manage Location".
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['result_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}