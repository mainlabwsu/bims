<?php
/**
 * @file
 */
/**
 * Search Accession by Genotype Accession form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_saga_genotype_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 540;

  // Gets BIMS_USER and BIMS_RPOGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Gets and sets the chosen item.
  $selected = array();
  if (array_key_exists('values', $form_state) && array_key_exists('#title', $form_state['triggering_element'])) {
    if ($form_state['triggering_element']['#id'] == 'sag_aprop') {
      $value = trim($form_state['values']['choose_item']['aprop']);
      if ($value) {
        $selected = array('type' => 'aprop', 'value' => $value);
      }
    }
    else if ($form_state['triggering_element']['#id'] == 'sag_cprop') {
      $value = trim($form_state['values']['choose_item']['cprop']);
      if ($value) {
        $selected = array('type' => 'cprop', 'value' => $value);
      }
    }
  }
  $form['selected'] = array(
    '#type'   => 'value',
    '#value'  => $selected,
  );

  // Gets the filters.
  $filters     = NULL;
  $no_filters  = TRUE;
  if (array_key_exists('values', $form_state)) {
    $filters = $form_state['values']['filters'];
    if (sizeof($filters) > 1) {
      $no_filters = FALSE;
    }
  }
  else {
    $filters = BIMS_FILTER::initFilters($bims_user, 'SEARCH_GENOTYPE_ACCESSION');
  }

  // Saves BIMS_PROGRAM and the filters.
  $form['filters'] = array(
    '#type'   => 'value',
    '#value'  => $filters,
  );
  $form['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_saga_genotype');
  $bims_panel->init($form);

  // Adds "Choose property" section.
  $form['choose_item'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Choose property',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Checks if mview exists.
  $mview_g  = new BIMS_MVIEW_GENOTYPE(array('node_id' => $bims_program->getProgramID()));
  $mview_ga = new BIMS_MVIEW_GENOTYPE_ACCESSION(array('node_id' => $bims_program->getProgramID()));
  if (!$mview_g->hasData() || !$mview_ga->hasData()) {
    $form['choose_item']['no_data'] = array(
      '#markup' => '<p><em>There is no genotyping data (SNP) associated with this program.</em></p>',
    );
  }
  else {

    // Search by accession properties.
    $options = BIMS_FILTER_ACCESSIONPROP::getOptions($filters);
    $size = (sizeof($options) > 15) ? 15 : sizeof($options);
    $form['choose_item']['aprop'] = array(
      '#id'         => 'sag_aprop',
      '#type'       => 'select',
      '#title'      => t('Properties'),
      '#options'    => $options,
      '#size'       => $size,
      '#attributes' => array('style' => 'width:100%;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_saga_genotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-saga-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Search by custom properties.
    $options = BIMS_FILTER_CUSTOMPROP::getOptions($filters);
    if (!empty($options)) {
      $size = (sizeof($options) > 15) ? 15 : sizeof($options);
      $form['choose_item']['cprop'] = array(
        '#id'         => 'sag_cprop',
        '#type'       => 'select',
        '#title'      => t('Custom Properties'),
        '#options'    => $options,
        '#size'       => $size,
        '#attributes' => array('style' => 'width:100%;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_saga_genotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-saga-genotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds the reset button.
    $form['choose_item']['reset_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'reset_btn',
      '#value'      => 'Reset',
      '#attributes' => array('style' => 'margin-top:10px;width:150px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_saga_genotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-saga-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds 'Set filter' section.
  $form['set_filter'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Set filter',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Displays the chosen criteria.
  BIMS_FILTER::displayCriteria($form['set_filter'], $filters);

  // Adds the filter form.
  if (empty($selected)) {
    $form['set_filter']['selected_prop_title'] = array(
      '#markup' => '<p>Please choose a property of accession or genotype on the left sidebar.</p>',
    );
  }
  else {
    $disabled = FALSE;

    // Gets the arguments.
    $args = array(
      'bims_user'       => $bims_user,
      'property'        => $selected['value'],
      'filters'         => $filters,
    );

    // Accession properties.
    if ($selected['type'] == 'aprop') {
      $disabled = BIMS_FILTER_ACCESSIONPROP::getFormFilter($form['set_filter'], $args);
    }

    // Custom properties.
    if ($selected['type'] == 'cprop') {
      $disabled = BIMS_FILTER_CUSTOMPROP::getFormFilter($form['set_filter'], $args);
    }

    // Adds 'Add' button.
    $form['set_filter']['apply_filter_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'apply_filter_btn',
      '#value'      => 'Add',
      '#disabled'   => $disabled,
      '#attributes' => array('style' => 'width:150px;margin-top:10px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_saga_genotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-saga-genotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds 'Search results' section.
  $form['search'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Genotype Search results',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets the descriptions.
  $last = end($filters);
  $desc_arr = json_decode($last['descs'], TRUE);

  // Gets the label for accession.
  $accession = $bims_program->getBIMSLabel('accession');
  $form['search']['filter_table']['accession'] = array(
    '#type'   => 'value',
    '#value'  => $accession,
  );

  // Creates links for removing and viewing.
  $cur_idx = 0;
  foreach ($filters as $idx => $filter) {
    if ($idx) {
      $cur_idx = $idx;
      $filter_name = ucfirst($filter['name']);
      $count = $filter['count_gd'];

      $view_btn = "<button onclick='bims.add_main_panel(\"bims_panel_add_on_sag_result\",\"Genotype Search Results\");'>view</button>";
      $action_links = "
        <img onclick='bims.add_main_panel(\"bims_panel_add_on_sag_result\",\"Genotype Search Results\");' src='/sites/all/modules/bims/theme/img/bims_open_tab.png' style='width:20px;float:right;maring-right:13px;' />
        <img onclick='bims.add_main_panel(\"bims_panel_add_on_sag_result\",\"Genotype Search Results\");' src='/sites/all/modules/bims/theme/img/bims_arrow_left_24.png' style='width:20px;float:right;maring-right:13px;' />
      ";

      $action_links = '';
      $form['search']['filter_table'][$idx] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => TRUE,
        '#collapsible'  => TRUE,
        '#title'        => "<b>$filter_name : matched $count genotypic <span title='measurements'>meas.</span></b>$action_links",
        '#attributes'   => array('style' => 'padding:3px;border:none;margin-top:0px;margin-bottom:0px;min-height:25px;background:rgba(0,0,0,0)'),
      );

      // Adds the 'Remove', 'View' and 'Analyze' buttons.
      $form['search']['filter_table'][$idx]['remove'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_remove_' . $idx,
        '#value'      => 'Remove',
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_saga_genotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-saga-genotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['view'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_view_' . $idx,
        '#value'      => 'View',
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_saga_genotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-saga-genotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['analyze'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_analyze_' . $idx,
        '#value'      => 'Analyze',
        '#disabled'   => TRUE,
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_saga_genotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-saga-genotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['filter'] = array(
        '#type'   => 'value',
        '#value'  => $filter,
      );
    }
  }
  $form['search']['filter_table'][$cur_idx]['#collapsed'] = FALSE;
  $form['search']['filter_table']['#theme'] = 'bims_panel_main_saga_genotype_form_filter_table';

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-saga-genotype-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_saga_genotype_form_submit';
  $form['#theme'] = 'bims_panel_main_saga_genotype_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_saga_genotype_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the variables.
  $filters      = $form['filters']['#value'];
  $bims_program = $form['bims_program']['#value'];

  // If 'aprop' was clicked.
  if ($trigger_elem == 'choose_item[aprop]') {
    if (array_key_exists('cprop', $form['choose_item'])) {
      $form['choose_item']['cprop']['#value'] = '';
    }
  }

  // If 'cprop' was clicked.
  else if ($trigger_elem == 'choose_item[cprop]') {
    $form['choose_item']['aprop']['#value'] = '';
  }

  // If 'Remove' was clicked.
  else if ($trigger_elem == 'apply_filter_btn' || preg_match("/^filter_remove_/", $trigger_elem)) {
    $form['choose_item']['aprop']['#value'] = '';
    if (array_key_exists('cprop', $form['choose_item'])) {
      $form['choose_item']['cprop']['#value'] = '';
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_saga_genotype_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'apply_filter_btn') {

    // Gets the values from the form.
    $selected = $form_state['values']['selected'];
    $args = array(
      'property'  => $selected['value'],
      'base_path' => 'set_filter',
    );

    // Checks the values in the form_state.
    if ($selected['type'] == 'aprop') {
      BIMS_FILTER_ACCESSIONPROP::checkFormFilter($form_state['values']['set_filter'], $args);
    }
    else if ($selected['type'] == 'cprop') {
      BIMS_FILTER_CUSTOMPROP::checkFormFilter($form_state['input']['set_filter'], $args);
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_saga_genotype_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'apply_filter_btn') {

    // Gets the values from the form.
    $selected = $form_state['values']['selected'];
    $filters  = $form_state['values']['filters'];

    // Applies the filter.
    $args = array(
      'bims_user'     => $bims_user,
      'bims_program'  => $bims_program,
      'property'      => $selected['value'],
    );
    if ($selected['type'] == 'aprop') {
      if (BIMS_FILTER_ACCESSIONPROP::addFormFilter($filters, $form_state['values']['set_filter'], $args)) {
        $form_state['values']['filters'] = $filters;
        $form_state['values']['choose_item']['aprop'] = '';
      }
      else {
        drupal_set_message('Error : Failed to add filters.', 'error');
      }
    }
    else if ($selected['type'] == 'cprop') {
      if (BIMS_FILTER_CUSTOMPROP::addFormFilter($filters, $form_state['values']['set_filter'], $args)) {
        $form_state['values']['filters'] = $filters;
        $form_state['values']['choose_item']['cprop'] = '';
      }
      else {
        drupal_set_message('Error : Failed to add filters.', 'error');
      }
    }
  }

  // If 'Analyze' was clicked.
  else if (preg_match("/^filter_analyze_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Gets the filters and stores it in the SESSION.
    $filters = $form_state['values']['filters'];
    bims_set_session('SESSION_FILTER', $filters[$idx]);

    // Analyzes the current data group.
    $url = "bims/load_main_panel/bims_panel_add_on_da_genotype_analysis";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_da_genotype_analysis', 'Genotype Analysis', $url)));
  }

  // If 'View' was clicked.
  else if (preg_match("/^filter_view_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Gets the filters and stores it in the SESSION.
    $filters = $form_state['values']['filters'];
    bims_set_session('SESSION_FILTER', $filters[$idx]);

    // Views the results.
    $url = "bims/load_main_panel/bims_panel_add_on_sag_result";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sag_result', 'Genotype Search Results', $url)));
  }

  // If 'Remove' was clicked.
  else if (preg_match("/^filter_remove_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Updates the filters.
    $filters = $form_state['values']['filters'];
    array_splice($filters, $idx, 1);
    BIMS_FILTER::updateFilters($filters);
    $form_state['values']['filters'] = $filters;
  }

  // If 'Reset' was clicked.
  else if ($trigger_elem == 'reset_btn') {

    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_saga_genotype')));
  }
}

/**
 * Theme function for the filters table.
 *
 * @param $variables
 */
function theme_bims_panel_main_saga_genotype_form_filter_table($variables) {
  $element = $variables['element'];

  // Lists the filters.
  $rows = array();
  $idx = 1;
  while (array_key_exists($idx, $element)) {
    $rows []= array(
      drupal_render($element[$idx])
    );
    $idx++;
  }

  // Adds the headers.
  $headers = array(
    array('data' => 'Filtered By', 'width' => '100%', 'colspan' => 2),
  );

  // Creates the table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => 'Please add filter.',
    'param'       => array(),
  );

  // Adds the description.
  $desc = '';
  if (!empty($rows)) {
    $accession = $element['accession']['#value'];
    $desc = "<div>If you click 'View', you can see the list of accessions along with the properties and genotype you chose. From that page you can view/download phenotypic/genotype data of the filtered $accession.</div>";
  }
  return bims_theme_table($table_vars) . $desc;
}

/**
 * Theme function for the search by genotype form.
 *
 * @param $variables
 */
function theme_bims_panel_main_saga_genotype_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "Choose property" section.
  $layout .= "<div style='float:left;width:20%;'>" . drupal_render($form['choose_item']) . "</div>";

  // Adds "Set filter" section.
  $layout .= "<div style='float:left;width:43%;margin-left:5px;min-width:370px;'>" . drupal_render($form['set_filter']) . "</div>";

  // Adds 'Search results' section.
  $layout .= "<div style='float:left;width:35%;margin-left:5px;'>" . drupal_render($form['search']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}