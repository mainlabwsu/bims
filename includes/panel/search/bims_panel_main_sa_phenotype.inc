<?php
/**
 * @file
 */
/**
 * Search Accession by Phenotype panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_phenotype_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 540;

  // Gets BIMS_USER and BIMS_RPOGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Gets and sets the chosen item.
  $selected = array();
  if (array_key_exists('values', $form_state) && array_key_exists('#title', $form_state['triggering_element'])) {
    if ($form_state['triggering_element']['#id'] == 'sap_aprop') {
      $value = trim($form_state['values']['choose_item']['aprop']);
      if ($value) {
        $selected = array('type' => 'aprop', 'value' => $value);
      }
    }
    else if ($form_state['triggering_element']['#id'] == 'sap_cprop') {
      $value = trim($form_state['values']['choose_item']['cprop']);
      if ($value) {
        $selected = array('type' => 'cprop', 'value' => $value);
      }
    }
    else {
      $value = $form_state['values']['choose_item']['traits'];
      if ($value) {
        $selected = array('type' => 'trait', 'value' => $value);
      }
    }
  }
  $form['selected'] = array(
    '#type'   => 'value',
    '#value'  => $selected,
  );

  // Gets the filters.
  $filters     = NULL;
  $no_filters  = TRUE;
  if (array_key_exists('values', $form_state)) {
    $filters = $form_state['values']['filters'];
    if (sizeof($filters) > 1) {
      $no_filters = FALSE;
    }
  }
  else {
    $filters = BIMS_FILTER::initFilters($bims_user, 'SEARCH_PHENOTYPE');
  }

  // Saves BIMS_PROGRAM and the filters.
  $form['filters'] = array(
    '#type'   => 'value',
    '#value'  => $filters,
  );
  $form['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_sa_phenotype');
  $bims_panel->init($form);

  // Adds "Choose property" section.
  $form['choose_item'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Choose property',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Checks if mview exists.
  $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $bims_program->getProgramID()));
  if (!$mview_phenotype || !$mview_phenotype->exists()) {
    $msg = '<p><em>There is no phenotyping data associated with this program.</em></p>';
    if (FALSE) {
      $msg .= '<p>The materialized view for the phenotyping data does not exist.' .
        '<br /><br />Make sure data have been uploaded and the materialized ' .
        ' views have been updated.</p>';
    }
    if (!$mview_phenotype->exists()) {
      $form['choose_item']['no_data'] = array('#markup' => $msg);
    }
  }
  else {

    // Search by accession properties.
    $options = BIMS_FILTER_ACCESSIONPROP::getOptions($filters);
    $size = (sizeof($options) > 15) ? 15 : sizeof($options);
    $form['choose_item']['aprop'] = array(
      '#id'         => 'sap_aprop',
      '#type'       => 'select',
      '#title'      => t('Properties'),
      '#options'    => $options,
      '#size'       => $size,
      '#attributes' => array('style' => 'width:100%;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sa-phenotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Search by custom properties.
    $options = BIMS_FILTER_CUSTOMPROP::getOptions($filters);
    if (!empty($options)) {
      $size = (sizeof($options) > 15) ? 15 : sizeof($options);
      if ($size == 1) {
        $options = array('' => 'Choose a property below') + $options;
      }
      $form['choose_item']['cprop'] = array(
        '#id'         => 'sap_cprop',
        '#type'       => 'select',
        '#title'      => t('Custom Properties'),
        '#options'    => $options,
        '#size'       => $size,
        '#attributes' => array('style' => 'width:100%;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-phenotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Search by traits.
    $options = BIMS_FILTER_TRAIT::getOptions($filters);
    $size = (sizeof($options) > 12) ? 12 : sizeof($options);
    if ($size) {
      if ($size == 1) {
        $size = 2;
      }
      $form['choose_item']['traits'] = array(
        '#id'         => 'sap_traits',
        '#type'       => 'select',
        '#title'      => t('Traits'),
        '#options'    => $options,
        '#size'       => $size,
        '#attributes' => array('style' => 'width:100%;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-phenotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds the reset button.
    $form['choose_item']['reset_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'reset_btn',
      '#value'      => 'Reset',
      '#attributes' => array('style' => 'margin-top:10px;width:150px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sa-phenotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds 'Set filter' section.
  $form['set_filter'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Set filter',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Displays the chosen criteria.
  _bims_panel_main_sa_phenotype_form_display_criteria($form['set_filter'], $filters);

  // Adds the filter form.
  if (empty($selected)) {
    $form['set_filter']['selected_aprop_title'] = array(
      '#markup' => '<p>Please choose a property or trait on the left sidebar.</p>',
    );
  }
  else {
    $disabled = FALSE;

    // Accession properties.
    if($selected['type'] == 'aprop') {
      $args = array(
        'bims_user' => $bims_user,
        'filters'   => $filters,
        'property'  => $selected['value']
      );
      $disabled = BIMS_FILTER_ACCESSIONPROP::getFormFilter($form['set_filter'], $args);
    }

    // Custom properties.
    else if($selected['type'] == 'cprop') {
      $args = array(
        'bims_user' => $bims_user,
        'filters'   => $filters,
        'property'  => $selected['value']
      );
      $disabled = BIMS_FILTER_CUSTOMPROP::getFormFilter($form['set_filter'], $args);
    }

    // Traits.
    else if ($selected['type'] == 'trait') {
      $args = array(
        'bims_user' => $bims_user,
        'cvterm_id' => $selected['value'],
        'filters'   => $filters,
      );
      $disabled = BIMS_FILTER_TRAIT::getFormFilter($form['set_filter'], $args);
    }

    // Adds the comments if no match found.
    $prefix = '';
    if ($disabled) {
      if ($no_filters) {
        $prefix = '<div><em>No associated ' . $selected['value'] . ' found</em></div>';
      }
      else {
        $prefix = '<div><em>No associated ' . $selected['value'] . ' found for the current selections</em></div>';
      }
    }

    // Adds 'Add' button.
    $form['set_filter']['apply_filter_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'apply_filter_btn',
      '#value'      => 'Add',
      '#disabled'   => $disabled,
      '#prefix'     => $prefix,
      '#attributes' => array('style' => 'width:150px;margin-top:10px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sa-phenotype-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    if (0) {
      $form['set_filter']['apply_filter_op'] = array(
        '#type'           => 'radios',
        '#name'           => 'apply_filter_op',
        '#options'        => array('AND' => 'AND', 'OR' => 'OR'),
        '#default_value'  => 'AND',
        '#prefix'         => '<div style="height:10px;">&nbsp;</div>',
        '#attributes'     => array('style' => 'width:20px;'),
      );
      hide($form['set_filter']['apply_filter_op']);
    }
  }

  // Adds 'Search results' section.
  $form['search'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Phenotype search results',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets the descriptions.
  $last = end($filters);
  $desc_arr = json_decode($last['descs'], TRUE);

  // Gets the label for accession.
  $accession = $bims_program->getBIMSLabel('accession');
  $form['search']['filter_table']['accession'] = array(
    '#type'   => 'value',
    '#value'  => $accession,
  );

  // Creates links for removing and viewing.
  $cur_idx = 0;
  foreach ($filters as $idx => $filter) {
    if ($idx) {
      $cur_idx = $idx;
      $filter_name = ucfirst($filter['name']);
      $count = $filter['count_pd'];
      $disabled = $count ? FALSE : TRUE;

      $view_btn = "<button onclick='bims.add_main_panel(\"bims_panel_add_on_sap_result\",\"Phenotype Search Results\");'>view</button>";
      $action_links = "
        <img onclick='bims.add_main_panel(\"bims_panel_add_on_sap_result\",\"Phenotype Search Results\");' src='/sites/all/modules/bims/theme/img/bims_open_tab.png' style='width:20px;float:right;maring-right:13px;' />
        <img onclick='bims.add_main_panel(\"bims_panel_add_on_sap_result\",\"Phenotype Search Results\");' src='/sites/all/modules/bims/theme/img/bims_arrow_left_24.png' style='width:20px;float:right;maring-right:13px;' />
      ";

      $action_links = '';
      $form['search']['filter_table'][$idx] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => TRUE,
        '#collapsible'  => TRUE,
        '#title'        => "<b>$filter_name : matched $count phenotypic <span title='measurements'>meas.</span></b>$action_links",
        '#attributes'   => array('style' => 'padding:3px;border:none;margin-top:0px;margin-bottom:0px;min-height:25px;background:rgba(0,0,0,0)'),
      );

      // Adds the 'Remove', 'View' and 'Analyze' buttons.
      $form['search']['filter_table'][$idx]['remove'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_remove_' . $idx,
        '#value'      => 'Remove',
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-phenotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['view'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_view_' . $idx,
        '#value'      => 'View',
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-phenotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['analyze'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_analyze_' . $idx,
        '#value'      => 'Analyze',
        '#disabled'   => $disabled,
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_phenotype_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-phenotype-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['filter'] = array(
        '#type'   => 'value',
        '#value'  => $filter,
      );
    }
  }
  $form['search']['filter_table'][$cur_idx]['#collapsed'] = FALSE;
  $form['search']['filter_table']['#theme'] = 'bims_panel_main_sa_phenotype_form_filter_table';

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-sa-phenotype-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_sa_phenotype_form_submit';
  $form['#theme'] = 'bims_panel_main_sa_phenotype_form';
  return $form;
}

/**
 * Returns the chosen critera in a table.
 *
 * @param array $form
 * @param array $filters
 */
function _bims_panel_main_sa_phenotype_form_display_criteria(&$form, $filters) {
  $size = sizeof($filters);
  if ($size > 1) {

    // Gets the all criteria.
    $desc_arr = json_decode($filters[$size-1]['descs'], TRUE);
    $rows = array();
    foreach ($desc_arr as $idx => $items) {
      foreach ($items as $name => $desc) {
        $str_length = strlen($desc);
        if ($str_length > 60) {
          $desc = "<textarea rows='2' style='width:100%;max-width:600px;'>$desc</textarea>";
        }
        $rows []= array(ucfirst($name), $desc);
      }
    }
    $table_vars = array(
      'header'      => array(array('data' => 'Filtered By', 'width' => '80'), 'Values'),
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%'),
      'sticky'      => TRUE,
      'empty'       => 'No description',
      'param'       => array(),
    );

    // Adds the criteria table to the form.
    $form['criteria'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'You have chosen :',
      '#attributes'   => array('style' => 'width:100%'),
      '#description'  => bims_theme_table($table_vars),
    );
  }
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_phenotype_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the variables.
  $filters      = $form['filters']['#value'];
  $bims_program = $form['bims_program']['#value'];

  // If 'aprop' was clicked.
  if ($trigger_elem == 'choose_item[aprop]') {
    $form['choose_item']['traits']['#value'] = '';
    if (array_key_exists('cprop', $form['choose_item'])) {
      $form['choose_item']['cprop']['#value'] = '';
    }
  }

  // If 'cprop' was clicked.
  if ($trigger_elem == 'choose_item[cprop]') {
    $form['choose_item']['aprop']['#value'] = '';
    $form['choose_item']['traits']['#value'] = '';
  }

  // If 'trait' was clicked.
  else if ($trigger_elem == 'choose_item[traits]') {
    $form['choose_item']['aprop']['#value'] = '';
    if (array_key_exists('cprop', $form['choose_item'])) {
      $form['choose_item']['cprop']['#value'] = '';
    }

    // Updates the trait dropdown list.
    $form['choose_item']['traits']['#options'] = BIMS_FILTER_TRAIT::getOptions($filters);
  }

  // If 'Remove' was clicked.
  else if ($trigger_elem == 'apply_filter_btn' || preg_match("/^filter_remove_/", $trigger_elem)) {
    $form['choose_item']['aprop']['#value'] = '';
    if (array_key_exists('cprop', $form['choose_item'])) {
      $form['choose_item']['cprop']['#value'] = '';
    }
    $form['choose_item']['traits']['#value'] = '';

    // Updates the trait dropdown list.
    $form['choose_item']['traits']['#options'] = BIMS_FILTER_TRAIT::getOptions($filters);
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_phenotype_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'apply_filter_btn') {

    // Gets the values from the form.
    $selected = $form_state['values']['selected'];
    $args = array(
      'property'  => $selected['value'],
      'base_path' => 'set_filter',
    );

    // Checks the values in the form_state.
    if ($selected['type'] == 'aprop') {
      BIMS_FILTER_ACCESSIONPROP::checkFormFilter($form_state['input']['set_filter'], $args);
    }
    else if ($selected['type'] == 'cprop') {
      BIMS_FILTER_CUSTOMPROP::checkFormFilter($form_state['input']['set_filter'], $args);
    }
    else if ($selected['type'] == 'trait') {
      BIMS_FILTER_TRAIT::checkFormFilter($form_state['input']['set_filter'], $args);
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_phenotype_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'apply_filter_btn') {

    // Gets the values from the form.
    $selected = $form_state['values']['selected'];
    $filters  = $form_state['values']['filters'];
    //$op     = (sizeof($filters) == 1) ? '' : $form_state['values']['set_filter']['apply_filter_op'];

    // Applies the filter.
    $args = array(
      'property'      => $selected['value'],
      'bims_user'     => $bims_user,
      'bims_program'  => $bims_program,
    );
    if ($selected['type'] == 'aprop') {
      if (BIMS_FILTER_ACCESSIONPROP::addFormFilter($filters, $form_state['values']['set_filter'], $args)) {
        $form_state['values']['filters'] = $filters;
        $form_state['values']['choose_item']['aprop'] = '';
      }
      else {
        drupal_set_message("Error : Failed to add filters", 'error');
      }
    }
    else if ($selected['type'] == 'cprop') {
      if (BIMS_FILTER_CUSTOMPROP::addFormFilter($filters, $form_state['values']['set_filter'], $args)) {
        $form_state['values']['filters'] = $filters;
        $form_state['values']['choose_item']['cprop'] = '';
      }
      else {
        drupal_set_message('Error : Failed to add filters.', 'error');
      }
    }
    else if ($selected['type'] == 'trait') {
      $args['cvterm_id'] = $selected['value'];
      if (BIMS_FILTER_TRAIT::addFormFilter($filters, $form_state['input']['set_filter'], $args)) {
        $form_state['values']['filters'] = $filters;
        $form_state['values']['choose_item']['traits'] = '';
      }
      else {
        drupal_set_message("Error : Failed to add filters", 'error');
      }
    }
  }

  // If 'Analyze' was clicked.
  else if (preg_match("/^filter_analyze_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Gets the filters and stores it in the SESSION.
    $filters = $form_state['values']['filters'];
    bims_set_session('SESSION_FILTER', $filters[$idx]);

    // Analyzes the current data group.
    $url = "bims/load_main_panel/bims_panel_add_on_da_phenotype_analysis";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_da_phenotype_analysis', 'Phenotype Analysis', $url)));
  }

  // If 'View' was clicked.
  else if (preg_match("/^filter_view_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Gets the filters and stores it in the SESSION.
    $filters = $form_state['values']['filters'];
    bims_set_session('SESSION_FILTER', $filters[$idx]);

    // Views the results.
    $url = "bims/load_main_panel/bims_panel_add_on_sap_result";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sap_result', 'Phenotype Search Results', $url)));
  }

  // If 'Remove' was clicked.
  else if (preg_match("/^filter_remove_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Updates the filters.
    $filters = $form_state['values']['filters'];
    array_splice($filters, $idx, 1);
    BIMS_FILTER::updateFilters($filters);
    $form_state['values']['filters'] = $filters;
  }

  // If 'Reset' was clicked.
  else if ($trigger_elem == 'reset_btn') {

    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_sa_phenotype')));
  }
}

/**
 * Theme function for the filters table.
 *
 * @param $variables
 */
function theme_bims_panel_main_sa_phenotype_form_filter_table($variables) {
  $element = $variables['element'];

  // Lists the filters.
  $rows = array();
  $idx = 1;
  while (array_key_exists($idx, $element)) {
    $rows []= array(
      drupal_render($element[$idx])
    );
    $idx++;
  }

  // Adds the headers.
  $headers = array(
    array('data' => 'Filtered By', 'width' => '100%', 'colspan' => 2),
  );

  // Creates the table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => 'Please add filter.',
    'param'       => array(),
  );

  // Adds the description.
  $desc = '';
  if (!empty($rows)) {
    $accession = $element['accession']['#value'];
    $desc = "<div>If you click 'View', you can see the list of accessions along with the properties and trait values you chose. From that page you can view/download phenotypic data for only the filtered phenotyping samples as well as all the phenotypic measurements from the filtered $accession.</div>";
  }
  return bims_theme_table($table_vars) . $desc;
}

/**
 * Theme function for the search by phenotype form.
 *
 * @param $variables
 */
function theme_bims_panel_main_sa_phenotype_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "Choose property" section.
  $layout .= "<div style='float:left;width:20%;'>" . drupal_render($form['choose_item']) . "</div>";

  // Adds "set_filter".
  $layout .= "<div style='float:left;width:43%;margin-left:5px;min-width:370px;'>" . drupal_render($form['set_filter']) . "</div>";

  // Adds 'Search results' section.
  $layout .= "<div style='float:left;width:35%;margin-left:5px;'>" . drupal_render($form['search']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}