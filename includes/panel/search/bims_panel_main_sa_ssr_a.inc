<?php
/**
 * @file
 */
/**
 * Search Accession by Genotype SSR allele form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_ssr_a_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 440;

  // Gets BIMS_USER and BIMS_RPOGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
  $program_id   = $bims_program->getProgramID();

  // Gets BIMS_MVIEW_SSR.
  $mview_ssr = new BIMS_MVIEW_SSR(array('node_id' => $program_id));

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Gets the chosen marker.
  $selected_marker = NULL;
  if (array_key_exists('values', $form_state)) {
    $marker_name = trim($form_state['values']['choose_marker']['marker']);
    $selected_marker = $mview_ssr->getByKey(array('marker' => $marker_name));
  }

  // Saves variables.
  $form['selected_marker'] = array(
    '#type'   => 'value',
    '#value'  => $selected_marker,
  );
  $form['mview_ssr'] = array(
    '#type'   => 'value',
    '#value'  => $mview_ssr,
  );

  // Gets the filters.
  $filters     = NULL;
  $no_filters  = TRUE;
  if (array_key_exists('values', $form_state)) {
    $filters = $form_state['values']['filters'];
    if (sizeof($filters) > 1) {
      $no_filters = FALSE;
    }
  }
  else {
    $filters = BIMS_FILTER::initFilters($bims_user, 'SEARCH_SSR_ALLELE');
  }

  // Saves BIMS_PROGRAM and the filters.
  $form['filters'] = array(
    '#type'   => 'value',
    '#value'  => $filters,
  );
  $form['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_sa_ssr_a');
  $bims_panel->init($form);

  // Adds "Choose Marker" section.
  $form['choose_marker'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Choose Marker',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Checks if mview exists.
  if (!$mview_ssr->hasData()) {
    $form['choose_marker']['no_marker'] = array(
      '#markup' => '<p><em>There is no genotyping data (SSR) associated with this program.</em></p>',
    );
  }
  else {

    // Adds auto complete for markers.
    $form['choose_marker']['marker'] = array(
      '#type'               => 'textfield',
      '#title'              => t('Marker'),
      '#description'        => t("Please type the name of marker"),
      '#attributes'         => array('style' => 'width:200px;'),
      '#autocomplete_path'  => "bims/bims_autocomplete_ssr_marker/$program_id",
    );

    // Adds the reset button.
    $form['choose_marker']['allele_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'allele_btn',
      '#value'      => 'Show Alleles',
      '#attributes' => array('style' => 'margin-top:10px;width:150px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_sa_ssr_a_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sa-ssr-a-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds 'Allele' section.
  $form['set_filter'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Set Allele',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Displays the chosen criteria.
  BIMS_FILTER::displayCriteria($form['set_filter'], $filters);

  // Adds 'Set Allele' section.
  if (!$selected_marker) {
    $form['set_filter']['no_marker'] = array(
      '#markup' => "<p><em>Please choose a marker on the left panel.</em></p>",
    );
  }
  else {

    // Gets the alleles.
    $alleles = $mview_ssr->getAlleles($selected_marker, TRUE);

    // Gets the arguments.
    $args = array(
      'bims_crop' => $bims_user->getCrop(),
      'alleles'   => $alleles,
    );

    // Accession properties.
    $disabled = BIMS_FILTER_GENOTYPE_ALLELE::getFormFilter($form['set_filter'], $args);

    // Adds 'Add' button.
    $form['set_filter']['apply_filter_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'apply_filter_btn',
      '#value'      => 'Add',
      '#disabled'   => $disabled,
      '#attributes' => array('style' => 'width:150px;margin-top:10px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_sa_ssr_a_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-sa-ssr-a-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds 'Search Results' section.
  $form['search'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Genotype Search Results',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets the descriptions.
  $last = end($filters);
  $desc_arr = json_decode($last['descs'], TRUE);

  // Gets the label for accession.
  $accession = $bims_program->getBIMSLabel('accession');
  $form['search']['filter_table']['accession'] = array(
    '#type'   => 'value',
    '#value'  => $accession,
  );

  // Creates links for removing and viewing.
  $cur_idx = 0;
  foreach ($filters as $idx => $filter) {
    if ($idx) {
      $cur_idx = $idx;
      $filter_name = ucfirst($filter['name']);
      $count = $filter['count_gd'];

      $view_btn = "<button onclick='bims.add_main_panel(\"bims_panel_add_on_sag_result\",\"Genotype Search Results\");'>view</button>";
      $action_links = "
        <img onclick='bims.add_main_panel(\"bims_panel_add_on_sag_result\",\"Genotype Search Results\");' src='/sites/all/modules/bims/theme/img/bims_open_tab.png' style='width:20px;float:right;maring-right:13px;' />
        <img onclick='bims.add_main_panel(\"bims_panel_add_on_sag_result\",\"Genotype Search Results\");' src='/sites/all/modules/bims/theme/img/bims_arrow_left_24.png' style='width:20px;float:right;maring-right:13px;' />
      ";

      $action_links = '';
      $form['search']['filter_table'][$idx] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => TRUE,
        '#collapsible'  => TRUE,
        '#title'        => "<b>$filter_name : matched $count genotypic <span title='measurements'>meas.</span></b>$action_links",
        '#attributes'   => array('style' => 'padding:3px;border:none;margin-top:0px;margin-bottom:0px;min-height:25px;background:rgba(0,0,0,0)'),
      );

      // Adds the 'Remove', 'View' and 'Analyze' buttons.
      $form['search']['filter_table'][$idx]['remove'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_remove_' . $idx,
        '#value'      => 'Remove',
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_ssr_a_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-ssr-a-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['view'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_view_' . $idx,
        '#value'      => 'View',
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_ssr_a_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-ssr-a-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['analyze'] = array(
        '#type'       => 'submit',
        '#name'       => 'filter_analyze_' . $idx,
        '#value'      => 'Analyze',
        '#disabled'   => TRUE,
        '#attributes' => array('style' => 'width:75px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_sa_ssr_a_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-sa-ssr-a-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
      $form['search']['filter_table'][$idx]['filter'] = array(
        '#type'   => 'value',
        '#value'  => $filter,
      );
    }
  }
  $form['search']['filter_table'][$cur_idx]['#collapsed'] = FALSE;
  $form['search']['filter_table']['#theme'] = 'bims_panel_main_sa_ssr_a_form_filter_table';

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-sa-ssr-a-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_sa_ssr_a_form_submit';
  $form['#theme'] = 'bims_panel_main_sa_ssr_a_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_ssr_a_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the variables.
  $filters      = $form['filters']['#value'];
  $bims_program = $form['bims_program']['#value'];

  // If 'aprop' was clicked.
  if ($trigger_elem == 'choose_item[aprop]') {
    if (array_key_exists('cprop', $form['choose_item'])) {
      $form['choose_item']['cprop']['#value'] = '';
    }
  }

  // If 'cprop' was clicked.
  else if ($trigger_elem == 'choose_item[cprop]') {
    $form['choose_item']['aprop']['#value'] = '';
  }

  // If 'Remove' was clicked.
  else if ($trigger_elem == 'apply_filter_btn' || preg_match("/^filter_remove_/", $trigger_elem)) {
    $form['choose_item']['aprop']['#value'] = '';
    if (array_key_exists('cprop', $form['choose_item'])) {
      $form['choose_item']['cprop']['#value'] = '';
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_ssr_a_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Show Allele' was clicked.
  if ($trigger_elem == 'allele_btn') {

    $marker_name = trim($form_state['values']['choose_marker']['marker']);
    if (!$marker_name) {
      form_set_error('choose_marker][marker', "Please type marker name");
      return;
    }
    $mview_ssr = $form['mview_ssr']['#value'];
    $marker = $mview_ssr->getByKey(array('marker' => $marker_name));
    if (!$marker) {
      form_set_error('choose_marker][marker', "The marker '$marker_name' does not exist");
      return;
    }
  }

  // If 'Add' was clicked.
  else if ($trigger_elem == 'apply_filter_btn') {

    // Gets the values from the form.
    $selected_marker  = trim($form_state['values']['choose_marker']['marker']);
    $selected_alleles = array();
    foreach ((array)$form_state['values']['set_filter'] as $name => $elem) {
      if (preg_match("/^allele-(\d+)/", $name, $matches)) {
        $selected_alleles[$matches[1]] = $elem;
      }
    }

    // Checks the values in the form_state.
    $args = array(
      'base_path' => 'set_filter',
      'marker'    => $selected_marker,
      'alleles'   => $selected_alleles,
    );
    BIMS_FILTER_GENOTYPE_ALLELE::checkFormFilter($form_state['values']['set_filter'], $args);
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_sa_ssr_a_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add' was clicked.
  if ($trigger_elem == 'apply_filter_btn') {

    // Gets the values from the form.
    $filters          = $form_state['values']['filters'];
    $selected_marker  = trim($form_state['values']['choose_marker']['marker']);
    $selected_alleles = array();
    foreach ((array)$form_state['values']['set_filter'] as $name => $elem) {
      if (preg_match("/^allele-(\d+)/", $name, $matches)) {
        $selected_alleles[$matches[1]] = $elem;
      }
    }

    // Applies the filter.
    $args = array(
      'bims_user'     => $bims_user,
      'bims_program'  => $bims_program,
      'marker'        => $selected_marker,
      'alleles'       => $selected_alleles,
    );

    if (BIMS_FILTER_GENOTYPE_ALLELE::addFormFilter($filters, $form_state['values']['set_filter'], $args)) {
      $form_state['values']['filters'] = $filters;
      //$form_state['values']['choose_marker']['marker'] = '';
    }
    else {
      drupal_set_message('Error : Failed to add filters.', 'error');
    }
  }

  // If 'Analyze' was clicked.
  else if (preg_match("/^filter_analyze_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Gets the filters and stores it in the SESSION.
    $filters = $form_state['values']['filters'];
    bims_set_session('SESSION_FILTER', $filters[$idx]);

    // Analyzes the current data group.
    $url = "bims/load_main_panel/bims_panel_add_on_da_ssr_analysis";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_da_ssr_analysis', 'Genotype Analysis', $url)));
  }

  // If 'View' was clicked.
  else if (preg_match("/^filter_view_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Gets the filters and stores it in the SESSION.
    $filters = $form_state['values']['filters'];
    bims_set_session('SESSION_FILTER', $filters[$idx]);

    // Views the results.
    $url = "bims/load_main_panel/bims_panel_add_on_sag_result";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sag_result', 'Genotype Search Results', $url)));
  }

  // If 'Remove' was clicked.
  else if (preg_match("/^filter_remove_(\d+)/", $trigger_elem, $matches)) {
    $idx = $matches[1];

    // Updates the filters.
    $filters = $form_state['values']['filters'];
    array_splice($filters, $idx, 1);
    BIMS_FILTER::updateFilters($filters);
    $form_state['values']['filters'] = $filters;
  }

  // If 'Reset' was clicked.
  else if ($trigger_elem == 'reset_btn') {

    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_sa_ssr_a')));
  }
}

/**
 * Theme function for the filters table.
 *
 * @param $variables
 */
function theme_bims_panel_main_sa_ssr_a_form_filter_table($variables) {
  $element = $variables['element'];

  // Lists the filters.
  $rows = array();
  $idx = 1;
  while (array_key_exists($idx, $element)) {
    $rows []= array(
      drupal_render($element[$idx])
    );
    $idx++;
  }

  // Adds the headers.
  $headers = array(
    array('data' => 'Filtered By', 'width' => '100%', 'colspan' => 2),
  );

  // Creates the table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => 'Please add filter.',
    'param'       => array(),
  );

  // Adds the description.
  $desc = '';
  if (!empty($rows)) {
    $accession = $element['accession']['#value'];
    $desc = "<div>If you click 'View', you can see the list of accessions along with the properties and genotype you chose. From that page you can view/download phenotypic/genotype data of the filtered $accession.</div>";
  }
  return bims_theme_table($table_vars) . $desc;
}

/**
 * Theme function for the search by genotype form.
 *
 * @param $variables
 */
function theme_bims_panel_main_sa_ssr_a_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "Marker" section.
  $layout .= "<div style='float:left;width:25%;'>" . drupal_render($form['choose_marker']) . "</div>";

  // Adds "Allele filter" section.
  $layout .= "<div style='float:left;width:38%;margin-left:5px;'>" . drupal_render($form['set_filter']) . "</div>";

  // Adds 'Search results' section.
  $layout .= "<div style='float:left;width:35%;margin-left:5px;'>" . drupal_render($form['search']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}