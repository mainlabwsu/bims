<?php
/**
 * @file
 */
/**
 * Adds the statistics of the results. Shows this section if the stats has
 * been calculated.
 *
 * @param integer $program_id
 * @param array $form
 * @param string $callback
 * @param string $wrapper
 * @param array $stats_arr
 * @param integer $cvterm_id_def
 */
function _bims_panel_add_on_result_form_add_stats($program_id, &$form, $callback, $wrapper, $stats_arr, $cvterm_id_def = NULL) {

  // Local variables.
  $min_height = 220;
  $size       = 9;

  // Adds trait selection.
  $form['trait_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Statistical Traits',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Gets the descriptors.
  $options = array();
  foreach ((array)$stats_arr as $cvterm_id => $stats) {
    if (!$cvterm_id_def) {
      $cvterm_id_def = $cvterm_id;
    }
    $options[$cvterm_id] = $stats['name'];
  }
  $form['trait_list']['selection'] = array(
    '#type'           => 'select',
    '#options'        => $options,
    '#default_value'  => $cvterm_id_def,
    '#size'           => $size,
    '#empty_options'  => 'No statistical trait found',
    '#attributes'     => array('style' => 'width:100%;'),
    '#ajax'           => array(
      'callback' => $callback,
      'wrapper'  => $wrapper,
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Adds trait stats.
  $form['trait_stats'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Statistical Information',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['trait_stats']['table'] = array(
    '#markup' => BIMS_STATS::getStatsTable($program_id, $stats_arr[$cvterm_id_def], $cvterm_id_def),
  );
  $form['#theme'] = 'bims_panel_add_on_result_trait_stats';
}

/**
 * Theme function for the phenotype column options accession properties.
 *
 * @param $variables
 */
 function theme_bims_panel_add_on_result_form_opt_pcolumn_accession_prop($variables) {
  return _theme_bims_panel_add_on_result_form_opt_column($variables['element'], array('pc_accessionprop'), "/^[^#]/");
 }

/**
 * Adds checkboxes for the accession properties.
 *
 * @param array $form
 * @param string $fieldset_id
 * @param array $preference
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_result_form_opt_pcolumn_accession_prop(&$form, $fieldset_id, $preference, BIMS_PROGRAM $bims_program) {

  // Gets the accession label.
  $accession_label = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Adds the fieldset.
  $form['accession_prop'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => "$accession_label Properties",
    '#attributes'   => array('style' => ''),
  );
  $form['accession_prop']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Adds accession label.
  $form['accession_prop']['accession'] = array(
    '#type'   => 'value',
    '#value'  => $accession_label,
  );

  // Adds the checkboxes for accessionprop.
  foreach ((array)$preference['pc_accessionprop'] as $field => $info) {
    $form['accession_prop']['pc_accessionprop']['values'][$field] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['value'],
    );
  }
  $form['accession_prop']['#theme'] = 'bims_panel_add_on_result_form_opt_pcolumn_accession_prop';
}

/**
 * Theme function for the phenotype column options for sample properties.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_opt_pcolumn_sample_prop($variables) {
  return _theme_bims_panel_add_on_result_form_opt_column($variables['element'], array('sampleprop'), "/^[^#]/");
}

/**
 * Adds checkboxes for the sample properties.
 *
 * @param array $form
 * @param string $fieldset_id
 * @param array $preference
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_result_form_opt_pcolumn_sample_prop(&$form, $fieldset_id, $preference, BIMS_PROGRAM $bims_program) {

  // Adds the fieldset.
  $form['sample_prop'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Sample Properties',
    '#attributes'   => array('style' => ''),
  );
  $form['sample_prop']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Adds the checkboxes for sampleprop.
  foreach ((array)$preference['sampleprop'] as $field => $info) {
    $form['sample_prop']['sampleprop']['values'][$field] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['value'],
    );
  }
  $form['sample_prop']['#theme'] = 'bims_panel_add_on_result_form_opt_pcolumn_sample_prop';
}

/**
 * Theme function for the column options.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_opt_pcolumn_trait($variables) {
  return _theme_bims_panel_add_on_result_form_opt_column($variables['element'], array('descriptor'), "/^t\d+$/");
}

/**
 * Adds checkboxes for the traits.
 *
 * @param array $form
 * @param string $fieldset_id
 * @param array $preference
 */
function _bims_panel_add_on_result_form_opt_pcolumn_trait(&$form, $fieldset_id, $preference) {

  // Adds the fieldset.
  $form['trait'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Traits',
    '#attributes'   => array('style' => ''),
  );
  $form['trait']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Adds the checkboxes.
  foreach ((array)$preference['descriptor'] as $field => $info) {
    $form['trait']['descriptor']['values'][$field] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['value'],
    );
  }
  $form['trait']['#theme'] = 'bims_panel_add_on_result_form_opt_pcolumn_trait';
}

/**
 * Theme function for the column options.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_opt_gcolumn_accession_prop($variables) {
  return _theme_bims_panel_add_on_result_form_opt_column($variables['element'], array('gc_accessionprop'), "/^[^#]/");
}

/**
 * Theme function for the column options.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_opt_gcolumn_marker_prop($variables) {
  return _theme_bims_panel_add_on_result_form_opt_column($variables['element'], array('markerprop'), "/^[^#]/");
}

/**
 * Theme function for the column options.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_opt_gcolumn_genotype_prop($variables) {
  return _theme_bims_panel_add_on_result_form_opt_column($variables['element'], array('genotypeprop'), "/^[^#]/");
}

/**
 * Adds checkboxes for the accession properties.
 *
 * @param array $form
 * @param string $fieldset_id
 * @param array $preference
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_result_form_opt_gcolumn_accession_prop(&$form, $fieldset_id, $preference, BIMS_PROGRAM $bims_program) {

  // Gets the accession label.
  $accession_label = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Adds the fieldset.
  $form['accession_prop'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => "$accession_label Properties",
    '#attributes'   => array('style' => ''),
  );
  $form['accession_prop']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );
  $form['accession_prop']['accession'] = array(
    '#type'   => 'value',
    '#value'  => $accession_label,
  );

  // Adds the checkboxes for accessionprop.
  foreach ((array)$preference['gc_accessionprop'] as $field => $info) {
    $form['accession_prop']['gc_accessionprop']['values'][$field] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['value'],
    );
  }
  $form['accession_prop']['#theme'] = 'bims_panel_add_on_result_form_opt_gcolumn_accession_prop';
}

/**
 * Adds checkboxes for the marker properties.
 *
 * @param array $form
 * @param string $fieldset_id
 * @param array $preference
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_result_form_opt_gcolumn_marker_prop(&$form, $fieldset_id, $preference, BIMS_PROGRAM $bims_program) {

  // Adds the fieldset.
  $form['marker_prop'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Marker Properties',
    '#attributes'   => array('style' => ''),
  );
  $form['marker_prop']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Adds the checkboxes for markerprop.
  foreach ((array)$preference['markerprop'] as $field => $info) {
    $form['marker_prop']['markerprop']['values'][$field] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['value'],
    );
  }
  $form['marker_prop']['#theme'] = 'bims_panel_add_on_result_form_opt_gcolumn_marker_prop';
}

/**
 * Adds checkboxes for the genotype properties.
 *
 * @param array $form
 * @param string $fieldset_id
 * @param array $preference
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_result_form_opt_gcolumn_genotype_prop(&$form, $fieldset_id, $preference, BIMS_PROGRAM $bims_program) {

  // Adds the fieldset.
  $form['genotype_prop'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Genotype Properties',
    '#attributes'   => array('style' => ''),
  );
  $form['genotype_prop']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Adds the checkboxes for genotypeprop.
  foreach ((array)$preference['genotypeprop'] as $field => $info) {
    $form['genotype_prop']['genotypeprop']['values'][$field] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['value'],
    );
  }
  $form['genotype_prop']['#theme'] = 'bims_panel_add_on_result_form_opt_gcolumn_genotype_prop';
}

/**
 * Theme function for the cross column options for cross properties.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_opt_ccolumn_cross_prop($variables) {
  return _theme_bims_panel_add_on_result_form_opt_column($variables['element'], array('crossprop'), "/^[^#]/");
}

/**
 * Adds checkboxes for the cross properties.
 *
 * @param array $form
 * @param string $fieldset_id
 * @param array $preference
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_result_form_opt_ccolumn_cross_prop(&$form, $fieldset_id, $preference, BIMS_PROGRAM $bims_program) {

  // Adds the fieldset.
  $form['cross_prop'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Cross Properties',
    '#attributes'   => array('style' => ''),
  );
  $form['cross_prop']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Adds the checkboxes for crossprop.
  foreach ((array)$preference['crossprop'] as $field => $info) {
    $form['cross_prop']['crossprop']['values'][$field] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['value'],
    );
  }
  $form['cross_prop']['#theme'] = 'bims_panel_add_on_result_form_opt_ccolumn_cross_prop';
}

/**
 * Adds the description.
 *
 * @param array $form
 * @param array $filter
 */
function _bims_panel_add_on_result_form_get_search_desc(&$form, $filter) {

  // Parses the description.
  $desc_arr = json_decode($filter['descs'], TRUE);
  $rows = array();
  foreach ($desc_arr as $idx => $items) {
    foreach ($items as $name => $desc) {
      $str_length = strlen($desc);
      if ($str_length > 60) {
        $desc = "<textarea rows='2' style='width:100%;max-width:600px;'>$desc</textarea>";
      }
      $rows []= array(ucfirst($name), $desc);
    }
  }
  $table_vars = array(
    'header'      => array(array('data' => 'Filtered By', 'width' => '80'), 'Values'),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%'),
    'sticky'      => TRUE,
    'empty'       => 'No description',
    'param'       => array(),
  );
  $form['desc'] = array(
    '#markup' => bims_theme_table($table_vars),
  );
}

/**
 * Helper function for theme function for the column options.
 *
 * @param array $element
 * @param array $ids
 * @param string $regex
 *
 * @return string
 */
function _theme_bims_panel_add_on_result_form_opt_column($element, $ids, $regex) {

  // Adds a checkbox for 'Check All'.
  $fieldset_id = $element['fieldset_id']['#value'];
  $check_all = "<div style=''><input style='' type='checkbox' onchange='bims.bims_select_all(this, \"$fieldset_id\");'> Check All / None</div>";

  // Adds stock property section.
  $num_cols = 6;
  $row      = array();
  $rows     = array();
  $count    = 0;
  foreach ($ids as $id) {
    foreach ((array)$element[$id]['values'] as $key => $checkbox) {
      if (preg_match($regex, $key)) {
        $count++;
        $row []= drupal_render($checkbox);
        if (($count % $num_cols) == 0) {
          $rows []= $row;
          $row = array();
        }
      }
    }
  }
  if (!empty($row)) {
    $size = sizeof($row);
    if ($size < $num_cols) {
      for ($i = $size; $i < $num_cols; $i++) {
        $row []= '&nbsp;';
      }
    }
    $rows []= $row;
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
  );
  return $check_all . bims_theme_table($table_vars);
}

/**
 * Theme function for the column options for phenotype.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_opt_pcolumn($variables) {
  $element = $variables['element'];
  $layout = '';
  if (array_key_exists('accession_prop', $element)) {
    $layout  .= "<div style=''>" . drupal_render($element['accession_prop']) . "</div>";
  }
  if (array_key_exists('sample_prop', $element)) {
    $layout  .= "<div style=''>" . drupal_render($element['sample_prop']) . "</div>";
  }
  if (array_key_exists('trait', $element)) {
    $layout  .= "<div style=''>" . drupal_render($element['trait']) . "</div>";
  }
  if (array_key_exists('update_btn', $element)) {
    $layout  .= "<div style=''>" . drupal_render($element['update_btn']) . "</div>";
  }
  return $layout;
 }

 /**
  * Theme function for the genotype column options for genotype.
  *
  * @param $variables
  */
function theme_bims_panel_add_on_result_form_opt_gcolumn($variables) {
  $element = $variables['element'];
  $layout = '';
  if (array_key_exists('accession_prop', $element)) {
    $layout  .= "<div style=''>" . drupal_render($element['accession_prop']) . "</div>";
  }
  if (array_key_exists('marker_prop', $element)) {
    $layout .= "<div style=''>" . drupal_render($element['marker_prop']) . "</div>";
  }
  if (array_key_exists('genotype_prop', $element)) {
    $layout .= "<div style=''>" . drupal_render($element['genotype_prop']) . "</div>";
  }
  return $layout;
}

/**
 * Theme function for the trait stats.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_trait_stats($variables) {
  $element = $variables['element'];

  // Adds the trait list.
  $layout = "<div style='float:left;width:40%;'>" . drupal_render($element['trait_list']) . "</div>";

  // Adds the trait stats.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($element['trait_stats']) . "</div>";
  return $layout;
}

/**
 * Theme function for the save section.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_save($variables) {
  $element = $variables['element'];

  // Adds the 'save list' row.
  $rows = array();
  if (array_key_exists('save_btn', $element)) {

    // Adds the description.
    $desc = "<div style='padding-top:5px;padding-bottom:5px;'>Please provide a name and description to save the results. After your search is saved, your saved results is shown in 'Search Results' page.</div>";
    $rows []= array($desc);

    // Adds the form to save the results.
    $list = "<div style='float:left;width:80px;margin-top:3px;'>Name</div><div style='float:left;'>" . drupal_render($element['list_name']) . "</div><div style='float:left;margin-left:15px;'>" . drupal_render($element['save_btn']) . "</div><br clear='all'>";
    $list .= "<div style='float:left;width:80px;'>Description</div><div style='float:left;width:60px;'>" . drupal_render($element['list_desc']) . "</div></div><br clear='all'>";
    $rows []= array(array('data' => $list, 'style' => 'width:100%;'));
  }
  else if (array_key_exists('save_msg', $element)) {
    $rows []= array(drupal_render($element['save_msg']));
  }

  // Creates the data table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
  );
  return bims_theme_table($table_vars);
}

/**
 * Theme function for the download section.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_result_form_download($variables) {
  $element = $variables['element'];

  // Gets the variables.
  $accession = $element['accession']['#value'];

  // Adds 'Download' section for phenotype.
  $download_tables = '';
  if (array_key_exists('download_phenotype_btn', $element)) {
    $msg_file_form  = "<em>Please choose a file form before downloading phenotypic data. A file in a wide form has descriptor columns. Evaluation data are placed in these columns.<br/> Meanwhile, a file in long form has one evaluation value for each row. The default is 'wide' form.</em>";
    $msg_stats      = "<em>Please uncheck if you want to excldue the statistical information about the downloading data at the top of the file.</em>";
    $msg_download   = "<em>Click 'Download' to  data in the table below (only the phenotypic values within the parameter you chose).</em>";
    $download_row = array(
      array(drupal_render($element['file_form_phenotype']), $msg_file_form),
      array(drupal_render($element['stats']), $msg_stats),
      array(
        array(
          'data' => drupal_render($element['download_phenotype_btn']),
          'width' => 150,
        ),
        $msg_download,
      ),
    );
    $header = array(array('data' => 'Phenotypic data', 'width' => 200), '&nbsp;');
    $table_vars = array(
      'header'      => $header,
      'rows'        => $download_row,
      'attributes'  => array(),
      'empty'       => "<em>The matched $accession don't have phenotypic data.</em>",
      'sticky'      => TRUE,
    );
    $download_tables .= bims_theme_table($table_vars);
  }

  // Adds 'Download' section for genotype.
  if (array_key_exists('download_genotype_btn', $element)) {
    $msg_file_form  = "<em>Please choose a file form before downloading genotypic data.</em>";
    $msg_download   = "<em>Click 'Download' to download genotypic data.</em>";
    $download_row = array(
      array(drupal_render($element['file_form_genotype']), $msg_file_form),
      array(
        array(
          'data' => drupal_render($element['download_genotype_btn']),
          'width' => 150,
        ),
        $msg_download,
      ),
    );
    $header = array(array('data' => 'Genotypic data', 'width' => 200), '&nbsp;');
    $table_vars = array(
      'header'      => $header,
      'rows'        => $download_row,
      'attributes'  => array(),
      'empty'       => "<em>The matched $accession don't have genotypic data.</em>",
      'sticky'      => TRUE,
    );
    $download_tables .= bims_theme_table($table_vars);
  }

  // Adds 'Download' section for cross.
  if (array_key_exists('download_cross_btn', $element)) {
    $msg_file_form  = "<em>Please choose a file form before downloading cross data.</em>";
    $msg_stats      = "<em>Please uncheck if you want to excldue the statistical information about the downloading data at the top of the file.</em>";
    $msg_download   = "<em>Click 'Download' to download cross data.</em>";
    $download_row = array(
      array(drupal_render($element['stats']), $msg_stats),
      array(
        array(
          'data' => drupal_render($element['download_cross_btn']),
          'width' => 150,
        ),
        $msg_download,
      ),
    );
    $header = array(array('data' => 'Cross data', 'width' => 200), '&nbsp;');
    $table_vars = array(
      'header'      => $header,
      'rows'        => $download_row,
      'attributes'  => array(),
      'empty'       => "<em>No matched cross.</em>",
      'sticky'      => TRUE,
    );
    $download_tables .= bims_theme_table($table_vars);
  }

  // Adds the phenotype and genotype sections.
  $rows = array(array($download_tables));

  // Creates the data table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
  );
  return bims_theme_table($table_vars);
}

