<?php
/**
 * @file
 */
/**
 * Data Analysis - Phenotype Analysis add on form.
 *
 * @param array $form
 * @param array $form_state
 * @param string $id
 */
function bims_panel_add_on_da_phenotype_analysis_form($form, &$form_state, $stats_id = '') {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Local variables for form properties.
  $accession  = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
  $min_height = 430;

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_da_phenotype_analysis');
  $bims_panel->init($form);

  // Searches the filter in BIMS_LIST.
  $filter = NULL;
  if (preg_match("/^\d+$/", $stats_id)) {
    $bims_list = BIMS_LIST::byID($stats_id);
    if ($bims_list) {
      $filter = $bims_list->getPropByKey('filter');
    }
    else {
      $form['error'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => FALSE,
        '#title'        => 'Invalid BIMS_LIST',
        '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
      );
      $form['error']['invalid_list_id'] = array(
        '#markup' => '<em>The provided ID of the list is invalid.</em>',
      );
      return $form;
    }
  }

  // Searches the filter in SESSION.
  if (!$filter) {
    $filter = bims_get_session('SESSION_FILTER');
    if (!$filter) {
      $form['error'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => FALSE,
        '#title'        => 'SESSION EXPIRED',
        '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
      );
      $form['error']['session_expire'] = array(
        '#markup' => '<em>Session expired.</em>',
      );
      return $form;
    }
  }

  // Saves the filter.
  $form['filter'] = array(
    '#type'   => 'value',
    '#value'  => $filter,
  );

  // Gets the stats_arr and save it.
  $stats_arr = _bims_panel_add_on_da_phenotype_analysis_form_get_stats_arr($stats_id, $bims_user);
  $form['stats_arr'] = array(
    '#type'   => 'value',
    '#value'  => $stats_arr,
  );

  // Sets the types of the category.
  $cat_opts = array(
    'data_year'   => 'Data Year',
    'cross'       => 'Cross',
    'trial'       => 'Trial',
    'location'    => 'Location',
    'accession'   => $accession,
  );
  $cat_labels = array(
    'data_year'   => array('s' => 'Data Year', 'pl' => 'Data Years'),
    'cross'       => array('s' => 'Cross', 'pl' => 'Crosses'),
    'trial'       => array('s' => 'Trial', 'pl' => 'Trials'),
    'location'    => array('s' => 'Location', 'pl' => 'Locations'),
    'accession'   => array('s' => $accession, 'pl' => $accession . 's'),
  );

  // Category section.
  $form['category'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Categories'),
    '#description'  => '',
    '#attributes'   => array('style' => 'min-width:200px;min-height:' . $min_height . 'px;'),
  );
  _bims_panel_add_on_da_phenotype_analysis_form_category($cat_opts, $cat_labels, $form['category'], $bims_user, $stats_arr, $filter);

  // Trait selection.
  $form['trait'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Traits'),
    '#attributes'   => array('style' => 'min-width:200px;min-height:' . $min_height . 'px;'),
  );
  _bims_panel_add_on_da_phenotype_analysis_form_trait($cat_labels, $form['trait'], $stats_arr);

  // Stats section.
  $sid_str = ($bims_user->getName() == 'ltaein') ? "&nbsp;&nbsp;&nbsp;&nbsp;drush bims-compare $stats_id" : '';
  $form['stats'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Statistics' . $sid_str,
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    '#prefix' => '<div id="bims-panel-add-on-da-phenotype-analysis-form-stats">',
    '#suffix' => '</div>',
  );
  _bims_panel_add_on_da_phenotype_analysis_form_stats($cat_labels, $form['stats'], $stats_arr);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-da-phenotype-analysis-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_da_phenotype_analysis_form_submit';
  $form['#theme'] = 'bims_panel_add_on_da_phenotype_analysis_form';
  return $form;
}

/**
 * Initializes the stats_arr.
 *
 * @param string $stats_id
 * @param BIMS_USER $bims_user
 *
 * @return array
 */
function _bims_panel_add_on_da_phenotype_analysis_form_get_stats_arr($stats_id, $bims_user) {

  // Gets BIMS_VARIABLE.
  if ($stats_id) {
    return BIMS_VARIABLE::getVariable($stats_id);
  }

  // Gets some variables.
  $program_id = $bims_user->getProgramID();
  $user_id    = $bims_user->getUserID();

  // Sets the IDs.
  $id         = "$user_id-$program_id";
  $session_id = "da-progress-$id";
  $stats_id   = "da-stats-$id";

  // Creates stats_arr.
  return array(
    'stats'             => array(),
    'active_descriptor' => array(),
    'params'            => array(
      'program_id'  => $program_id,
      'category'    => '',
      'selection'   => array(),
      'cvterm_id'   => '',
      'session_id'  => $session_id,
      'stats_id'    => $stats_id,
    ),
  );
}

/**
 * Category section.
 *
 * @param array $cat_opts
 * @param array $cat_labels
 * @param array $form
 * @param BIMS_USER $bims_user
 * @param array $stats_arr
 * @param array $filter
 */
function _bims_panel_add_on_da_phenotype_analysis_form_category($cat_opts, $cat_labels, &$form, $bims_user, $stats_arr, $filter) {

  // Gets the default values from the session.
  $def_category   = $stats_arr['params']['category'];
  $def_selection  = $stats_arr['params']['selection'];
  $session_id     = $stats_arr['params']['session_id'];

  // Adds the criteria.
  $desc_arr = json_decode($filter['descs']);
  $rows = array();
  foreach ($desc_arr as $idx => $items) {
    foreach ($items as $name => $desc) {
      $row = '<div><b>' . ucfirst($name) .'</b></div>';
      $str_length = strlen($desc);
      if ($str_length > 60) {
        $row .= "<textarea rows='2' style='width:100%;max-width:600px;'>$desc</textarea>";
      }
      else {
        $row .= $desc;
      }
      $rows []= array($row);
    }
  }
  $table_vars = array(
    'header'      => array(array('data' => 'Filtered By')),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%'),
    'sticky'      => TRUE,
    'empty'       => 'No description',
    'param'       => array(),
  );
  $form['criteria'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'You have chosen :',
    '#attributes'   => array('style' => 'width:100%'),
    '#description'  => bims_theme_table($table_vars),
  );

  // Adds the types of category.
  $form['types'] = array(
    '#type'           => 'select',
    '#options'        => $cat_opts,
    '#size'           => sizeof($cat_opts),
    '#default_value'  => $def_category,
    '#attributes'     => array('style' => 'width:100%;'),
    '#ajax'           => array(
      'callback' => 'bims_panel_add_on_da_phenotype_analysis_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-da-phenotype-analysis-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Adds the selection.
  if (!$def_category) {
    $form['types']['#suffix'] = '<div style="margin:5px 0px 15px 0px;"><em>Please choose a category.</em></div>';
  }
  else {

    // Gets the selections.
    $options = _bims_panel_add_on_da_phenotype_analysis_form_get_selection($bims_user, $def_category, $filter);
    $num_options = sizeof($options);
    if ($num_options) {
      if ($num_options > 10) {
        $num_options = 10;
      }
      $form['selection'] = array(
        '#type'           => 'select',
        '#options'        => $options,
        '#title'          => $cat_labels[$def_category]['pl'],
        '#size'           => $num_options,
        '#default_value'  => $def_selection,
        '#multiple'       => TRUE,
        '#attributes'     => array('style' => 'width:100%;'),
      );
      if (empty($def_selection)) {
        $item_lc = strtolower($cat_labels[$def_category]['pl']);
        $form['selection']['#suffix'] = "<div style='margin:2px 0px 15px 0px;'><em>Please make selection(s). Use Ctl or Shift key to select multiple $item_lc.</em></div>";
      }
      $form['compare_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'compare_btn',
        '#value'      => 'Compare',
        '#attributes' => array('style' => 'display:block;margin:0 auto;width:160px;margin-bottom:20px;'),
        '#ajax'       => array(
          'callback' => 'bims_panel_add_on_da_phenotype_analysis_form_ajax_callback',
          'wrapper'  => 'bims-panel-add-on-da-phenotype-analysis-form',
          'effect'   => 'fade',
          'method'   => 'replace',
          'progress' =>  array(
            'type'      => 'bar',
            'message'   => t('Initializing comparison ...'),
            'url'       => url('bims/ajax_progressbar_sa/' . $session_id),
            'interval'  => '1000', // progress bar will refresh in 1 second.
          ),
        ),
      );
    }
    else {
      $selected_category = $cat_labels[$def_category]['s'];
      $form['selection'] = array(
        '#markup' => "<div><em>No selection available for '$selected_category'.</em></div>",
      );
    }
  }
}

/**
 * Returns the selections of the provided type.
 *
 * @param BIMS_USER $bims_user
 * @param string $category
 * @param array $filter
 *
 * @return array
 */
function _bims_panel_add_on_da_phenotype_analysis_form_get_selection(BIMS_USER $bims_user, $category, $filter) {
  $options = array();

  // Gets the mview for BIMS_MVIEW_PHENOTYPE.
  $program_id = $bims_user->getProgramID();
  $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
  $mviewp = $mview_phenotype->getMView();

  // Sets the filter condition.
  $filter_conds = '';
  $args = array();
  if ($filter) {
    $key  = $filter['key'];
    $sql  = $filter['sql'];
    $args = $filter['args'];
    $filter_conds = " AND $key IN (SELECT $key FROM ($sql) PA) ";
  }

  // Data Year.
  if ($category == 'data_year') {
    $sql = "
      SELECT DISTINCT MV.data_year
      FROM {$mviewp} MV
      WHERE MV.data_year IS NOT NULL AND MV.data_year != '' $filter_conds
      ORDER BY MV.data_year
    ";
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {
      $options[$obj->data_year] = $obj->data_year;
    }
  }

  // Cross.
  else if ($category == 'cross') {
    $sql = "
      SELECT DISTINCT MV.cross_number, MV.nd_experiment_id
      FROM {$mviewp} MV
      WHERE MV.cross_number IS NOT NULL AND MV.cross_number != '' $filter_conds
      ORDER BY MV.cross_number
    ";
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {
      $options[$obj->nd_experiment_id] = $obj->cross_number;
    }
  }

  // Trial.
  else if ($category == 'trial') {
    $sql = "
      SELECT DISTINCT MV.dataset_name, MV.project_id
      FROM {$mviewp} MV
      WHERE MV.dataset_name IS NOT NULL AND MV.dataset_name != '' $filter_conds
      ORDER BY MV.dataset_name
    ";
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {
      $options[$obj->project_id] = $obj->dataset_name;
    }
  }

  // Location.
  else if ($category == 'location') {
    $sql = "
      SELECT DISTINCT MV.site_name, MV.nd_geolocation_id
      FROM {$mviewp} MV
      WHERE MV.site_name IS NOT NULL AND MV.site_name != '' $filter_conds
      ORDER BY MV.site_name
    ";
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {
      $options[$obj->nd_geolocation_id] = $obj->site_name;
    }
  }

  // Accession List.
  else if ($category == 'accession') {
    $sql = "
      SELECT DISTINCT MV.accession, MV.stock_id
      FROM {$mviewp} MV
      WHERE 1=1 $filter_conds
      ORDER BY MV.accession
    ";
    $results = db_query($sql, $args);
    while ($obj = $results->fetchObject()) {
      $options[$obj->stock_id] = $obj->accession;
    }
  }
  return $options;
}

/**
 * Adds the traits section.
 *
 * @param array $cat_labels
 * @param array $form
 * @param array $stats_arr
 */
function _bims_panel_add_on_da_phenotype_analysis_form_trait($cat_labels, &$form, $stats_arr) {

  // Gets the default values from the session.
  $def_category   = $stats_arr['params']['category'];
  $def_selection  = $stats_arr['params']['selection'];
  $def_cvterm_id  = $stats_arr['params']['cvterm_id'];

  // Lists the traits.
  if (!empty($def_selection) && $def_category) {

    // Gets the stored stats.
    $stats = $stats_arr['stats'];
    if (empty($stats)) {
      $item = $cat_labels[$def_category]['s'];
      $form['selection'] = array(
        '#markup' => "<div><em>The selection of '<b>$item</b>' are not available.</em></div>",
      );
    }
    else {

      // Lists the active descriptors for the group.
      $options = $stats_arr['active_descriptor'];
      $size = sizeof($options);
      $form['selection'] = array(
        '#type'           => 'select',
        '#options'        => $options,
        '#size'           => ($size > 17) ? 17 : $size,
        '#default_value'  => $def_cvterm_id,
        '#description'    => t("Please choose a trait"),
        '#attributes'     => array('style' => 'width:100%;'),
        '#ajax'       => array(
          'callback' => 'bims_panel_add_on_da_phenotype_analysis_form_ajax_callback',
          'wrapper'  => 'bims-panel-add-on-da-phenotype-analysis-form-stats',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
  }
  else if ($def_category) {
    $item = $cat_labels[$def_category]['s'];
    $form['selection'] = array(
      '#markup' => "<div><em>The selection for '<b>$item</b>' has not been made.</em></div>",
    );
  }
  else {
    $form['selection'] = array(
      '#markup' => '<div><em>The category has not been chosen yet.</em></div>',
    );
  }
}

/**
 * Adds Stats section.
 *
 * @param array $cat_labels
 * @param array $form
 * @param array $stats_arr
 */
function _bims_panel_add_on_da_phenotype_analysis_form_stats($cat_labels, &$form, $stats_arr) {

  // Gets the default values from the session.
  $program_id     = $stats_arr['params']['program_id'];
  $def_category   = $stats_arr['params']['category'];
  $def_selection  = $stats_arr['params']['selection'];
  $def_cvterm_id  = $stats_arr['params']['cvterm_id'];
  $stats_id       = $stats_arr['params']['stats_id'];

  // Shows the stats.
  $err_msg = '';
  if (!empty($def_selection) && $def_category) {
    $form['chart'] = array(
      '#markup' => '<div style="float:right;font-size:10pt;"><em>Click legends to hide bars.</em></div><br clear="all"><canvas id="bims_chart_compare" style="width:100%;"></canvas><div id="bims_chart_compare_table">&nbsp</div>',
    );
  }
  else if ($def_category) {
    $label = $cat_labels[$def_category]['s'];
    $err_msg = "The selection for '<b>$label</b>' has not been made";
  }
  else {
    $err_msg = 'The category has not been chosen yet';
  }
  if ($err_msg) {
    $form['chart'] = array(
      '#markup' => "<div><em>$err_msg.</em></div>",
    );
  }
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_da_phenotype_analysis_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the stats array.
  $stats_arr  = $form['stats_arr']['#value'];
  $stats_id   = $stats_arr['params']['stats_id'];

  // If 'type' was changed.
  if ($trigger_elem == 'category[types]') {

    // Updates the category in stats_arr.
    $category = $form_state['values']['category']['types'];
    $stats_arr['params']['category']  = $category;
    $stats_arr['params']['selection'] = array();
    $stats_arr['params']['cvterm_id'] = '';
    BIMS_VARIABLE::setVariable($stats_id, $stats_arr);

    // Refresh page.
    $url = "bims/load_main_panel/bims_panel_add_on_da_phenotype_analysis/$stats_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_da_phenotype_analysis', 'Phenotype Analysis', $url)));
  }

  // If 'trait' was changed.
  else if ($trigger_elem == 'trait[selection]') {

    // Updates the cvterm ID in stats_arr.
    $cvterm_id = $form_state['values']['trait']['selection'];
    $stats_arr['params']['cvterm_id'] = $cvterm_id;
    BIMS_VARIABLE::setVariable($stats_id, $stats_arr);

    // Loads the chart.
    $params = "chart_grouped:$program_id:bar:$stats_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_chart", array('bims_chart_compare', $params)));
    return $form['stats'];
  }

  // If 'Compare' was clicked.
  else if ($trigger_elem == 'compare_btn') {

    // Gets the selection.
    $selection = $form_state['values']['category']['selection'];
    if (!empty($selection)) {

      // Adds the properties.
      $stats_arr['params']['selection'] = $selection;
      $stats_arr['params']['filter'] = $form_state['values']['filter'];

      // Saves the stats arr.
      BIMS_VARIABLE::setVariable($stats_id, $stats_arr);

      // Call drush command to perform comparison.
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd = "$drush bims-compare-group $stats_id > /dev/null 2>/dev/null";
      $pid = exec($cmd, $output, $return_var);
      if ($return_var) {
        drupal_set_message('Error : Failed to compare', 'error');
      }
      else {

        // Refreshes the page on the completion of the computation.
        $url = "bims/load_main_panel/bims_panel_add_on_da_phenotype_analysis/$stats_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_da_phenotype_analysis', 'Phenotype Analysis', $url)));
      }
    }
  }
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_da_phenotype_analysis_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Compare' was clicked.
  if ($trigger_elem == 'compare_btn') {
    $category   = $form_state['values']['category']['types'];
    $selection  = $form_state['values']['category']['selection'];
    if (empty($selection)) {
      form_set_error('group][selection', t("Please choose at least one $category."));
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_da_phenotype_analysis_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_add_on_da_phenotype_analysis_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the group section.
  $layout .= "<div style='float:left;width:25%;'>" . drupal_render($form['category']) . "</div>";

  // Adds the trait section.
  $layout .= "<div style='float:left;width:22%;margin-left:5px;'>" . drupal_render($form['trait']) . "</div>";

  // Adds the stats section.
  $layout .= "<div style='float:left;width:52%;margin-left:5px;'>" . drupal_render($form['stats']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
 }