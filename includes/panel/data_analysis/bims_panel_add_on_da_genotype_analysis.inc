<?php
/**
 * @file
 */
/**
 * Data Analysis - Genotype Analysis add on form.
 *
 * @param array $form
 * @param array $form_state
 * @param string $id
 */
function bims_panel_add_on_da_genotype_analysis_form($form, &$form_state, $stats_id = '') {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Local variables for form properties.
  $accession  = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
  $min_height = 430;

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_da_phenotype_analysis');
  $bims_panel->init($form);

  // Search the filter in BIMS_LIST.
  $filter = NULL;
  if (preg_match("/^\d+$/", $stats_id)) {
    $bims_list = BIMS_LIST::byID($stats_id);
    if ($bims_list) {
      $filter = $bims_list->getPropByKey('filter');
    }
    else {
      $form['error'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => FALSE,
        '#title'        => 'Invalid BIMS_LIST',
        '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
      );
      $form['error']['invalid_list_id'] = array(
        '#markup' => '<em>The provided ID of the list is invalid.</em>',
      );
      return $form;
    }
  }

  // Searches the filter in SESSION.
  if (!$filter) {
    $filter = bims_get_session('SESSION_FILTER');
    if (!$filter) {
      $form['error'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => FALSE,
        '#title'        => 'SESSION EXPIRED',
        '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
      );
      $form['error']['session_expire'] = array(
        '#markup' => '<em>Session expired.</em>',
      );
      return $form;
    }
  }

  // Saves the filter.
  $form['filter'] = array(
    '#type'   => 'value',
    '#value'  => $filter,
  );

  // Category section.
  $form['category'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Genotype'),
    '#description'  => 'Coming soon...',
    '#attributes'   => array('style' => 'min-width:200px;min-height:' . $min_height . 'px;'),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-da-genotype-analysis-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_da_genotype_analysis_form_submit';
  $form['#theme'] = 'bims_panel_add_on_da_genotype_analysis_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_da_genotype_analysis_form_ajax_callback($form, $form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_da_genotype_analysis_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_da_genotype_analysis_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_add_on_da_genotype_analysis_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  $layout .= drupal_render_children($form);
  return $layout;
 }