<?php
/**
 * @file
*/
/**
 * Legend panel form.
 *
 * @param array $form
 * @param array $form_state
*/
function bims_panel_add_on_legend_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Legend.
  $form['legend'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Tree Legend',
  );
  $form['legend']['table'] = array(
    '#markup' => bims_legend_table(),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-legend-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_legend_form_submit';
  $form['#theme'] = 'bims_panel_add_on_legend_form';
  return $form;
}

/**
 * Creates tree legend table.
 *
 * @return string
 */
function bims_legend_table() {

  // Adds tree legend.
  $legends = array(
    'bims-program'      => 'Program',
    'bims-sub-program'  => 'Sub-Program',
    'bims-trial'        => 'Trial',
    'bims-trial-ro'     => 'Trial READONLY',
    'bims-stock'        => 'Stock',
  );
  $rows = array();
  foreach ($legends as $class => $desc) {
    $rows []= array("<div class='jstree-default'><div class='$class'>&nbsp;</div></div>", $desc);
  }

  $table_vars = array(
    'header'      => array('icon', 'description'),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
  );
  return theme('table', $table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_legend_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_legend_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_legend_form_submit($form, &$form_state) {}

