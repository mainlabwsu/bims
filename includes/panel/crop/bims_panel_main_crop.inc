<?php
/**
 * @file
 */
/**
 * Crop panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_crop_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_crop');
  $bims_panel->init($form);

  // Shows crop images in a table.
  $form['crop_images'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Select a crop',
  );
  $form['crop_images']['images'] = array(
    '#markup' => bims_panel_main_crop_form_get_image_table($bims_user),
  );

  $form['#prefix'] = '<div id="bims-panel-main-crop-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_crop_form_submit';
  return $form;
}

/**
 * Creates and returns the crop images table.
 */
function bims_panel_main_crop_form_get_image_table(BIMS_USER $bims_user) {

  // Gets the current crop_id.
  $crop_id_curr = $bims_user->getCropID();

  // Gets all crops.
  $crops = BIMS_CROP::getCrops(BIMS_CLASS);
  $rows = array();
  foreach ($crops as $crop) {

    // Gets the number of programs.
    $num_pub_programs = $crop->getNumPrograms('public');
    $num_prv_programs = $crop->getNumPrograms('private');

    // Lists all the crops.
    $image_link = '<a href="javascript:void(0);" onclick="bims.set_crop(\'' . $crop->getCropID() . '\')">' . $crop->getLabel() . '</a>';
    if ($crop->getCropID() == $crop_id_curr) {
      $image_link = '<b>' . $crop->getLabel() . '</b>&nbsp;&nbsp;&nbsp;(<em>Selected</em>)';
    }
    $rows[] = array(
      $image_link,
      $num_pub_programs,
      //$num_prv_programs,
    );
  }

  // Sets the headers.
  $headers = array(
    'Crop',
    '# Public Programs',
    //'# Private Programs'
  );

  // Creates the image table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'empty'       => 'No crop available. Please ask admin to add crops.',
    'sticky'      => TRUE,
  );
  return theme('table', $table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_crop_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_crop_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_crop_form_submit($form, &$form_state) {}