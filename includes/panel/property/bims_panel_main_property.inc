<?php
/**
 * @file
 */
/**
 * Property panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_property_form($form, &$form_state) {
  global $user;

  // Local variables for form properties.
  $min_height = 340;

  // Gets BIMS_USER and crop_id and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the chosen cvterm ID.
  $cvterm_id = NULL;
  if (isset($form_state['values']['property_list']['selection'])) {
    $cvterm_id = $form_state['values']['property_list']['selection'];
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_property');
  $bims_panel->init($form);

  // Adds admin menu.
  $form['property_admin']['menu'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Property',
    '#attributes'   => array(),
  );
  /*
  $url = "bims/load_main_panel/bims_panel_add_on_property_add";
  $form['property_admin']['menu']['add_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_property_add', 'Add Property', '$url'); return (false);",
    )
  );*/
  $form['property_admin']['menu']['#theme'] = 'bims_panel_main_property_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['property_admin']);

  // Lists the all properties.
  $form['property_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Property Selection',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Property selection.
  $opt_props = $bims_program->getCustomProps(BIMS_OPTION);
  if (empty($opt_props)) {
    $form['property_list']['selection']= array(
      '#markup' => '<em>No property found for this program.</em>',
    );
  }
  else {
    $form['property_list']['selection']= array(
      '#type'           => 'select',
      '#options'        => $opt_props,
      '#default_value'  => $cvterm_id,
      '#size'           => 11,
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => 'bims_panel_main_property_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-property-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Property details.
  $form['property_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Property Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Stores $cvterm_id and type.
  $form['property_details']['table']['cvterm_id'] = array(
    '#type'   => 'value',
    '#value'  => $cvterm_id,
  );

  // Adds action buttons.
  if (bims_can_admin_action($bims_user)) {
    $form['property_details']['table']['edit_btn'] = array(
      '#name'       => 'edit_btn',
      '#type'       => 'button',
      '#value'      => 'Edit',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_property_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-property-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['property_details']['table']['delete_btn'] = array(
      '#name'       => 'delete_btn',
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#disabled'   => !$bims_user->isOwner(),
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_property_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-property-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['property_details']['table']['#theme'] = 'bims_panel_main_property_form_property_table';

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-property-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_property_form_submit';
  $form['#theme'] = 'bims_panel_main_property_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_property_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and program ID.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the cvterm ID.
  $cvterm_id = $form_state['values']['property_list']['selection'];

  // If 'Edit' was clicked.
  if ($trigger_elem == 'edit_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_property_edit/$cvterm_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_property_edit', 'Edit Program', $url)));
  }

  // If 'Delete' was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Deletes the property.
    if (_bims_panel_main_property_form_delete_property($program_id, $cvterm_id)) {

      // Updates the panels.
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_property")));
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Deletes the property.
 *
 * @param integer $program_id
 * @param integer $cvterm_id
 *
 * @return boolean
 */
function _bims_panel_main_property_form_delete_property($program_id, $cvterm_id) {

  $transaction = db_transaction();
  try {

    // Deletes the cvterm.
    $bims_cvterm = new BIMS_CVTERM($cvterm_id, $program_id);
    if (!$bims_cvterm->deleteCvterm($cvterm_id)) {
      throw new Exception("Error : Failed to delete the property");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_property_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_property_form_submit($form, &$form_state) {}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_property_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates the admin menu table.
  $rows = array();
  if (array_key_exists('add_btn', $element)) {
    $rows[] = array(drupal_render($element['add_btn']), 'Create a new property.');
  }
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
    'empty'       => '<em>No menu</em>',
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the property information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_property_form_property_table($variables) {
  $element = $variables['element'];

  // Gets the cvterm of the property.
  $cvterm_id  = $element['cvterm_id']['#value'];
  $cvterm     = MCL_CHADO_CVTERM::byID($cvterm_id);

  // Initializes the properties.
  $name       = '--';
  $data_type  = '';
  $definition = '';
  $actions    = '';

  // Updates the properites.
  if ($cvterm) {
    $name       = $cvterm->getName();
    $definition = $cvterm->getDefinition();

    // Gets the data type.
    $cvterm_id_format = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'format')->getCvtermID();
    $data_type = $cvterm->getPropByTypeID($cvterm_id_format);

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }
  }

  // Adds the description of the program.
  $definition_ta = "<textarea rows='3' readonly class='bims-description' style='width:100%;'>$definition</textarea>";

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'style' => 'width:90px;'), $name);
  if ($data_type) {
    $rows[] = array('Data Type', $data_type);
  }
  $rows[] = array('Definition', $definition_ta);
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }

  // Creates the details table.
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_property_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('property_admin', $form)) {
    $layout.= "<div style='float:left;width:100%;'>" . drupal_render($form['property_admin']) . '</div>';
  }

  // Adds the property list.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['property_list']) . "</div>";

  // Adds the property info. table.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['property_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}