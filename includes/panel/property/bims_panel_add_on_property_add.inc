<?php
/**
 * @file
 */
/**
 * Property add panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_add_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_property_add');
  $bims_panel->init($form);

  // Properties a property.
  $form['property_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Create Property',
  );
  $form['property_prop']['name'] = array(
    '#title'        => t('Name'),
    '#type'         => t('textfield'),
    '#description'  => t("Please specify the name of property."),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $options = array();
  $form['property_prop']['type'] = array(
    '#type'           => 'radios',
    '#title'          => t('Type'),
    '#options'        => $options,
    '#description'    => t("Please specify the type of property."),
    '#attributes'     => array('style' => 'width:20px;'),
  );
  $form['property_prop']['definition'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Definition'),
    '#description'  => t("Please provide a definition of a property."),
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['property_prop']['create_btn'] = array(
    '#name'       => 'create_btn',
    '#type'       => 'submit',
    '#value'      => t("Create a new property"),
    '#attributes' => array('style' => 'width:165px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_property_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-property-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['property_prop']['cancel_create_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_create_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_property_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-property-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-property-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_property_add_form_submit';
  $form['#theme'] = 'bims_panel_add_on_property_add_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_add_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_add_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Create' button was clicked.
  if ($trigger_elem == 'create_btn') {

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO tables.
    $bc_project = new BIMS_CHADO_PROJECT($program_id);

    // Checks property name
    $name = trim($form_state['values']['property_prop']['name']);

    // Validates the property name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('trial_prop][name', "Invalid property name : $error");
      return;
    }

    // Checks the property name for duplication.
    $project = $bc_project->byTKey('project', array('name' => $prefix . $name));
    if ($project) {
      form_set_error('property_prop][name', "The property ($name) has been taken. Please choose other name.");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Local variables.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

    // If the 'Create' button was clicked.
  if ($trigger_elem == 'create_btn') {

    // Property properties.
    $name       = trim($form_state['values']['property_prop']['name']);
    $definition = trim($form_state['values']['property_prop']['definition']);
    $type       = $form_state['values']['property_prop']['type'];

    // Adds a property.
    $details = array(
      'name'        => $name,
      'definition'  => $definition,
      'type'        => $property_type,
    );
    if ($bims_program->addProperty($details)) {
      drupal_set_message("$name was created successfully.");
    }
    else {
      drupal_set_message("Error : Failed to create a property.", 'error');
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_property_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_property')));
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_property_add_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "property".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['property_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
