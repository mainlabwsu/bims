<?php
/**
 * @file
 */
/**
 * Property edit panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_edit_form($form, &$form_state, $cvterm_id) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the cvterm.
  $cvterm = NULL;
  if ($cvterm_id) {
    $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves the property.
  $form['property'] = array(
    '#type'   => 'value',
    '#value'  => $cvterm,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_property_edit');
  $bims_panel->init($form);

  // Properties of the property.
  $form['property_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Edit Property',
  );
  $form['property_prop']['name'] = array(
    '#title'          => t('Name'),
    '#type'           => t('textfield'),
    '#default_value'  => $cvterm->getName(),
    '#description'    => t("Please specify the name of property."),
    '#attributes'     => array('style' => 'width:300px;'),
  );
  $form['property_prop']['definition'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Definition'),
    '#default_value'  => $cvterm->getDefinition(),
    '#description'    => t("Please provide a definition of a property."),
    '#rows'           => 3,
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['property_prop']['edit_btn']= array(
    '#name'       => 'edit_btn',
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_property_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-property-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['property_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_property_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-property-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-property-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_property_edit_form_submit';
  $form['#theme'] = 'bims_panel_add_on_property_edit_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_edit_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_btn') {

    // Gets the current name.
    $property           = $form_state['values']['property'];
    $cur_property_name  = $property->getName();

    // Checks property name
    $name = $form_state['values']['property_prop']['name'];

    // Checks property name
    if ($cur_property_name != $name) {

      // Validates the property name.
      $error = bims_validate_name($name);
      if ($error) {
        form_set_error('property_prop][name', "Invalid property name : $error");
        return;
      }

      // Checks for duplication.
      $cvterm = MCL_CHADO_CVTERM::getCvterm($name, $property->getCvID());
      if ($cvterm) {
        form_set_error('property_prop][name', "Property name ($name) is alrady used");
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_property_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;


  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  $no_error = TRUE;
  if ($trigger_elem == 'edit_btn') {

    // Trial properties.
    $property   = $form_state['values']['property'];
    $name       = trim($form_state['values']['property_prop']['name']);
    $definition = trim($form_state['values']['property_prop']['definition']);

    // Sets the properties.
    $property->setName($name);
    $property->setDefinition($definition);
    if (!$property->update()) {
      drupal_set_message("Error : Failed to edit the property.", 'error');
      $no_error = FALSE;
    }
  }

  // Updates panels.
  if ($no_error) {
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_property_edit')));
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_property')));
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_property_edit_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "property".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['property_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
