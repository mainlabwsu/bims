<?php
/**
 * @file
 */
/**
 * Edit cross panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_edit_form($form, &$form_state, $cross_id) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets BIMS_MVIEW_CROSS.
  $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $bims_user->getProgramID()));
  $cross    = $bm_cross->getCross($cross_id);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_accession_edit');
  $bims_panel->init($form);

  // Saves cross_id.
  $form['mview_cross'] = array(
    '#type'   => 'value',
    '#value'  => $cross,
  );

  // Properties of the cross.
  $form['cross_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Edit Cross',
    '#attributes'   => array(),
  );
  $form['cross_prop']['cross_number'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Cross Number'),
    '#description'    => t("Please provide a cross number. Please use alphabets, dash and underscore"),
    '#default_value'  => $cross->cross_number,
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['cross_prop']['m_stock'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Maternal Parent'),
    '#description'        => t("Please type the name of maternal parent"),
    '#default_value'      => $cross->maternal,
    '#attributes'         => array('style' => 'width:400px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id",
  );
  $form['cross_prop']['p_stock'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Paternal Parent'),
    '#description'        => t("Please type the name of paternal parent"),
    '#default_value'      => $cross->paternal,
    '#attributes'         => array('style' => 'width:400px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id",
  );
  $form['cross_prop']['edit_cross_btn'] = array(
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#name'       => 'edit_cross_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_cross_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-cross-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['cross_prop']['cancel_edit_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_edit_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_cross_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-cross-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add_on-cross-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_cross_edit_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_edit_form_validate($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_cross_btn') {

    // Gets properties of the cross.
    $cross_number = trim($form_state['values']['cross_prop']['cross_number']);
    $maternal     = trim($form_state['values']['cross_prop']['m_stock']);
    $paternal     = trim($form_state['values']['cross_prop']['p_stock']);
    $cross        = $form_state['values']['mview_cross'];

    // Validates the cross number.
    $error = bims_validate_name($cross_number);
    if ($error) {
      form_set_error('cross_prop][name', "Invalid cross number : $error");
      return;
    }

    // Gets BIMS_CHADO_ACCESSION and BIMS_CHADO_CROSS.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
    $bc_cross     = new BIMS_CHADO_CROSS($program_id);

    // Checks the cross number for duplication.
    if ($cross->cross_number != $cross_number) {
      $cross = $bc_cross->ByCrossNumber($cross_number);
      if ($cross) {
        form_set_error('cross_prop][cross_number', "The cross number '$cross_number' has been taken. Please choose other name.");
        return;
      }
    }

    // Checks the parents.
    if ($maternal) {
      if (!$bc_accession->getAccessionByName($maternal)) {
        form_set_error('cross_prop][m_stock', "$maternal does not exist.");
        return;
      }
    }
    if ($paternal) {
      if (!$bc_accession->getAccessionByName($paternal)) {
        form_set_error('cross_prop][p_stock', "$paternal does not exist.");
        return;
      }
    }
  }
}

/**
 * Updates the cross.
 *
 * @param array $form_state
 * @param object $cross
 *
 * @return boolean
 */
function _bims_panel_add_on_cross_edit_form_edit_cross($form_state, $cross) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets properties of the cross.
  $cross_number = trim($form_state['values']['cross_prop']['cross_number']);
  $maternal     = trim($form_state['values']['cross_prop']['m_stock']);
  $paternal     = trim($form_state['values']['cross_prop']['p_stock']);

  // Updates the dup_key.
  $dup_key = $bims_program->getPrefix() . $bims_program->getCrossProjectID() . '.' . $cross_number;

  // Prepares for updating the cross.
  $details = array(
    'nd_experiment_id'  => $cross->nd_experiment_id,
    'cross_number'      => $cross_number,
    'dup_key'           => $dup_key,
    'maternal'          => $maternal,
    'paternal'          => $paternal,
   );

  // Updates the properties for BIMS_CHADO_CROSS.
  $bc_cross = new BIMS_CHADO_CROSS($program_id);
  if (!$bc_cross->updateCross($details)) {
    drupal_set_message("Error : Failed to update the BIMS_CHADO_CROSS", 'error');
    return FALSE;
  }

  // Updates properties for BIMS_MVIEW_CROSS.
  $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
  if (!$bm_cross->updateCross($details)) {
    drupal_set_message("Error : Failed to update the BIMS_MVIEW_CROSS", 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_cross';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_cross_btn') {

    // Gets the cross.
    $cross = $form_state['values']['mview_cross'];

    // Updates the cross.
    $transaction = db_transaction();
    try {

      // Edits the cross.
      if (!_bims_panel_add_on_cross_edit_form_edit_cross($form_state, $cross)) {
        throw new Exception("Error : Failed to update the cross");
      }
      $load_primary_panel .= '/' . $cross->nd_experiment_id;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Removes the panel.
//  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_cross_edit')));
//  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_cross_edit_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  $layout .= drupal_render_children($form);
  return $layout;
}
