<?php
/**
 * @file
 */
/**
 * Add cross panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_add_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_user->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_cross_add');
  $bims_panel->init($form);

  // Property for a cross.
  $form['property'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Add a new property",
    '#collapsed'   => TRUE,
    '#collapsible' => TRUE,
  );
  $form['property']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Property Name'),
    '#description'  => t("Please provide an name of a property."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['property']['desc'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please provide a descrpition of a property."),
    '#rows'         => 3,
    '#attributes'   => array('style' => 'width:400px;'),
  );
  /*
  $options = $bims_program->getProperties('corss', 'custom', BIMS_OPTION);
  if (empty($options)) {
    $form['property']['list'] = array(
      '#markup' => '<div><b>No property found</b></div>',
    );
  }
  else {
    $form['property']['list'] = array(
      '#type'       => 'select',
      '#options'    => $options,
      '#size'       => 10,
      '#attributes' => array('style' => 'width:300px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_cross_add_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-cross-add-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    if ($bims_user->isOwner() || $bims_user->isEditUser()) {
      $form['property']['remove_property_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'remove_property_btn',
        '#value'      => "Remove property",
        '#attributes' => array('style' => 'width:180px;'),
        '#ajax'   => array(
          'callback' => "bims_panel_add_on_cross_add_form_ajax_callback",
          'wrapper'  => 'bims-panel-add-on-cross-add-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

  }*/
  $form['property']['add_property_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_property_btn',
    '#value'      => "Add property",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_cross_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-cross-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  //$form['property']['#theme'] = 'bims_panel_add_on_cross_add_form_property';

  // Properties of a cross.
  $form['cross_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Create Cross',
    '#attributes'   => array(),
  );
  $form['cross_prop']['cross_number'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Cross Number'),
    '#description'  => t("Please provide a cross number. Please use alphabets, dash and underscore"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['cross_prop']['cross_date'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Cross Date'),
    '#description'  => t("Please provide cross date."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['cross_prop']['m_stock'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Maternal Parent'),
    '#description'        => t("Please type the name of maternal parent"),
    '#attributes'         => array('style' => 'width:400px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id",
  );

  $form['cross_prop']['m_loc'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Maternal Location'),
    '#description'  => t("Please type the name of maternal location"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['cross_prop']['f_stock'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Paternal Parent'),
    '#description'        => t("Please type the name of paternal parent"),
    '#attributes'         => array('style' => 'width:400px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id",
  );
  $form['cross_prop']['f_loc'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Paternal Location'),
    '#description'  => t("Please type the name of paternal location"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['cross_prop']['comments'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Comments'),
    '#rows'          => 2,
    '#description'  => t("Please type the comments on the cross"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['cross_prop']['add_cross_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_cross_btn',
    '#value'      => 'Create',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_cross_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-cross-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['cross_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_cross_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-cross-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-cross-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_cross_add_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_add_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_add_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Add' was clicked.
  if ($trigger_elem == 'add_cross_btn') {

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO table.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
    $bc_cross     = new BIMS_CHADO_CROSS($program_id);

    // Gets the properties of the cross.
    $cross_number = trim($form_state['values']['cross_prop']['cross_number']);
    $maternal     = trim($form_state['values']['cross_prop']['m_stock']);
    $paternal     = trim($form_state['values']['cross_prop']['f_stock']);

    // Validates the cross number.
    $error = bims_validate_name($cross_number);
    if ($error) {
      form_set_error('cross_prop][name', "Invalid cross number : $error");
      return;
    }

    // Checks the cross number for duplication.
    $cross = $bc_cross->byCrossNumber($cross_number);
    if ($cross) {
      form_set_error('cross_prop][cross_number', "The cross number '$cross_number' has been taken. Please choose other name.");
      return;
    }

    // Checks the parents.
    if ($maternal) {
      if (!$bc_accession->byUniquename($prefix . $maternal)) {
        form_set_error('cross_prop][m_stock', "$maternal does not exist.");
      }
    }
    if ($paternal) {
      if (!$bc_accession->byUniquename($prefix . $paternal)) {
        form_set_error('cross_prop][f_stock', "$paternal does not exist.");
      }
    }
  }
}

/**
 * Adds a cross property.
 *
 * @param BIMS_PROGRAM $bims_program
 * @param array $form_state
 *
 * @return boolean
 */
function _bims_panel_add_on_cross_add_cross_prop(BIMS_PROGRAM $bims_program, $form_state) {

  // Gets the program ID.
  $program_id = $bims_program->getProgramID();

  // Gets the name of the property.
  $name = trim($form_state['values']['property']['name']);
  $desc = trim($form_state['values']['property']['desc']);

  // Adds the cvterm.
  $cv = $bims_program->getCv('cross');
  $cvterm = MCL_CHADO_CVTERM::addCvterm(NULL, 'SITE_DB', $cv->getName(), $name, $desc);
  if (!$cvterm) {
    return  FALSE;
  }

  // Adds the column to the mview.
  $bims_mview = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
  $mview      = $bims_mview->getMView();
  $field      = 'p' . $cvterm->getCvtermID();
  if (!db_field_exists($mview, $field)) {
    db_add_field($mview, $field, array('type' => 'text'));
  }
  return TRUE;
}

/**
 * Adds a cross.
 *
 * @param BIMS_PROGRAM $bims_program
 * @param array $form_state
 *
 * @return MCL_CHADO_ND_EXPERIMENT
 */
function _bims_panel_add_on_cross_add_cross(BIMS_PROGRAM $bims_program, $form_state) {

  // Gets the program ID.
  $program_id = $bims_program->getProgramID();

  // Gets the prefix.
  $prefix = $bims_program->getPrefix();

  // Gets BIMS_CHADO.
  $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
  $bc_site      = new BIMS_CHADO_SITE($program_id);
  $bc_cross     = new BIMS_CHADO_CROSS($program_id);

  // Gets cross project ID.
  $project_id_cross = $bims_program->getCrossProjectID();

  // Gets cvterms.
  $cvterms = array(
    'maternal_parent' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'maternal_parent')->getCvtermID(),
    'paternal_parent' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'paternal_parent')->getCvtermID(),
  );

  // Gets the properties of the cross.
  $cross_number = trim($form_state['values']['cross_prop']['cross_number']);
  $cross_date   = trim($form_state['values']['cross_prop']['cross_date']);
  $maternal     = trim($form_state['values']['cross_prop']['m_stock']);
  $maternal_loc = trim($form_state['values']['cross_prop']['m_loc']);
  $paternal     = trim($form_state['values']['cross_prop']['f_stock']);
  $paternal_loc = trim($form_state['values']['cross_prop']['f_loc']);
  $comments     = trim($form_state['values']['cross_prop']['comments']);

  // Adds a cross.
  $details = array(
    'dup_key'           => $prefix . $project_id_cross . '.' . $cross_number,
    'cross_number'      => $cross_number,
    'project_id_cross'  => $project_id_cross,
  );
  $nd_experiment_id = $bc_cross->addCross(NULL, $details);
  if ($nd_experiment_id) {
    $bc_cross->setNdExperimentID($nd_experiment_id);

    // Adds properties.
    $bc_cross->addProp(NULL, 'SITE_CV', 'cross_date', $cross_date);
    $bc_cross->addProp(NULL, 'SITE_CV', 'mother_location', $maternal_loc);
    $bc_cross->addProp(NULL, 'SITE_CV', 'father_location', $paternal_loc);
    $bc_cross->addProp(NULL, 'SITE_CV', 'comments', $comments);

    // Initialzes the details for MVIEW.
    $details_mview = array(
      'nd_experiment_id'  => $nd_experiment_id,
      'cross_number'      => $cross_number,
      'progeny'           => '[]',
      'comments'          => $comments,
    );

    // Adds a maternal parent.
    if ($maternal) {
      $details_mview['maternal'] = $maternal;
      $maternal = $prefix . $maternal;
      $maternal_parent = $bc_accession->byUniquename($maternal);
      $bc_cross->addParent(NULL, 'm', $maternal_parent, $cvterms['maternal_parent']);
      $details_mview['stock_id_m'] = $maternal_parent->stock_id;
    }

    // Adds a paternal parent.
    if ($paternal) {
      $details_mview['paternal'] = $paternal;
      $paternal = $prefix . $paternal;
      $paternal_parent = $bc_accession->byUniquename($paternal);
      $bc_cross->addParent(NULL, 'p', $paternal_parent, $cvterms['paternal_parent']);
      $details_mview['stock_id_p'] = $paternal_parent->stock_id;
    }

    // Adds the new cross to MVIEW.
    $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
    $bm_cross->addCross($details_mview);
  }
  else {
    drupal_set_message("Error : Failed to add a new cross", 'error');
    return NULL;
  }
  return $nd_experiment_id;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_cross';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  $transaction = db_transaction();
  try {

    // If "Add Property" was clicked.
    if ($trigger_elem == 'add_property_btn') {

      // Adds the cross property.
      if (_bims_panel_add_on_cross_add_cross_prop($bims_program, $form_state)) {
        drupal_set_message("New property has been added");
      }
      else {
        throw new Exception("Error : Failed to add a new property");
      }
    }

    // If the 'Add cross' button was clicked.
    else if ($trigger_elem == 'add_cross_btn') {

      // Adds a new cross.
      $nd_experiment_id = _bims_panel_add_on_cross_add_cross($bims_program, $form_state);
      if (!$nd_experiment_id) {
        throw new Exception("Error : Failed to add a new cross");
      }
      $load_primary_panel = "$load_primary_panel/$nd_experiment_id";
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_cross_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}

/**
 * Theme function for the property form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_cross_add_form_property($variables) {
  $form = $variables['element'];

  // Adds "property" section.
  $layout = "<div style='float:left;width:100%;'>" . drupal_render($form['property']) . "</div>";
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_cross_add_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  $layout .= drupal_render_children($form);
  return $layout;
}
