<?php
/**
 * @file
 */
/**
 * Cross delete panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_delete_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 500;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_cross_delete');
  $bims_panel->init($form);

  // Adds cross table.
  $form['cross'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Cross Mass Deletion',
    '#attributes'   => array(),
  );

  // Gets the ranged crosses.
  $bims_mview = new BIMS_MVIEW_CROSS(array('node_id' => $bims_program->getProgramID()));
  $headers = array(
    array('field' => 'nd_experiment_id', 'data' => 'nd_experiment_id', 'header' => '&nbsp;'),
    array('field' => 'cross_number',     'data' => 'cross_number',     'header' => 'Cross&nbsp;Number', 'sort' => 'asc'),
    array('field' => 'maternal',         'data' => 'maternal',         'header' => 'Maternal'),
    array('field' => 'paternal',         'data' => 'paternal',         'header' => 'Paternal'),
    array('field' => 'cross_date',       'data' => 'cross_date',       'header' => 'Cross&nbsp;Date')
  );
  $pt_var = array(
    'data_only' => TRUE,
    'id'        => 'bims_panel_add_on_cross_delete',
    'num_pages' => 10,
    'mview'     => $bims_mview->getMView(),
    'headers'   => $headers,
    'flag'      => BIMS_OBJECT,
    'tab'       => "Delete Crosses",
  );

  // Creates the checkboxes.
  $crosses = bims_create_pager_table($pt_var);
  foreach ((array)$crosses as $cross) {
    $form['cross']['cross_elem'][$cross->nd_experiment_id] = array(
      '#title'          => '',
      '#type'           => 'checkbox',
      '#default_value'  => 0,
      '#attributes'     => array('style' => 'margin-left:10px;'),
    );
    $form['cross']['cross_info'][$cross->nd_experiment_id] = array(
      '#type'   => 'value',
      '#value'  => $cross,
    );
  }
  $form['cross']['delete_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'delete_btn',
    '#value'      => "Delete",
    '#attributes' => array(
      'style' => 'width:180px;',
      'class' => array('use-ajax', 'bims-confirm'),
    ),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_cross_delete_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-cross-delete-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['cross']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['cross']['pt_var'] = array(
    '#type'   => 'value',
    '#value'  => $pt_var,
  );
  $form['cross']['#theme'] = 'bims_panel_add_on_cross_delete_form_cross_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-cross-delete-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_cross_delete_form_submit';
  $form['#theme'] = 'bims_panel_add_on_cross_delete_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_delete_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Delete" was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Gets the chosen crosses.
    $cross_elem = $form_state['values']['cross']['cross_elem'];
    $del_nd_experiment_ids = array();
    foreach ((array)$cross_elem as $nd_experiment_id => $elem) {
      if ($elem) {
        $del_nd_experiment_ids []= $nd_experiment_id;
      }
    }

    // Deletes the crosses.
    if (!empty($del_nd_experiment_ids)) {

      // Deletes the crosses.
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd = "bims-delete-data $program_id cross --id=" . implode(":", $del_nd_experiment_ids);
      $drush .= " $cmd > /dev/null 2>/dev/null  & echo $!";
      $pid = exec($drush, $output, $return_var);
      drupal_set_message("Submit the job to delete crosses");

      // Updates the panels.
      $url = "bims/load_main_panel/bims_panel_add_on_cross_delete";
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_cross')));
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_cross_delete', 'Delete Crosses', $url)));
    }
  }
  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_delete_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Delete" was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Checks the chosen crosses.
    $cross_elem = $form_state['values']['cross']['cross_elem'];
    $count = 0;
    foreach ((array)$cross_elem as $nd_experiment_id => $elem) {
      if ($elem) {
        $count++;
      }
    }
    if (!$count) {
      form_set_error('cross][cross_elem', "Please choose at least one");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_cross_delete_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_cross_delete_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds cross info.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['cross']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_cross_delete_form_cross_table($variables) {
  $element = $variables['element'];

  // Gets the parameters.
  $cross_elem   = $element['cross_elem'];
  $cross_info   = $element['cross_info'];
  $delete_btn   = $element['delete_btn'];
  $bims_program = $element['bims_program']['#value'];
  $pt_var       = $element['pt_var']['#value'];
  $id           = $pt_var['id'];
  $tab          = $pt_var['tab'];
  $headers      = $pt_var['headers'];
  $num_pages    = $pt_var['num_pages'];
  $element      = 0;

  // Lists the chosen crosses.
  $rows = array();
  foreach ((array)$cross_elem as $nd_experiment_id => $elem) {
    if (!preg_match("/^(\d+)$/", $nd_experiment_id)) {
      continue;
    }

    // Gets the cross object.
    $obj = $cross_info[$nd_experiment_id]['#value'];

    // Adds a cross.
    $row = array(
      drupal_render($elem),
      $obj->cross_number,
      $obj->maternal,
      $obj->paternal,
      $obj->cross_date,
    );
    $rows []= $row;
  }

  // Params for the sortable table and the pager.
  $param = array(
    'url' => "bims/load_main_panel/$id" . '?',
    'id'  => $id,
    'tab' => $tab,
  );

  // Creates the table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => 'No cross found',
    'param'       => $param,
  );
  $table = bims_theme_table($table_vars);

  // Creates the pager.
  $pager_vars = array(
    'tags'        => array(),
    'element'     => $element,
    'parameters'  => array(),
    'quantity'    => $num_pages,
    'param'       => $param,
  );
  $pager = bims_theme_pager($pager_vars);

  // Creates the delete button.
  $delete_btn = '<div>' . drupal_render($delete_btn) . '</div>';

  // Shows the table with a pager.
  return $delete_btn . $table . $pager;
}