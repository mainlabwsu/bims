<?php
/**
 * Cross panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_cross_form($form, &$form_state, $nd_experiment_id = NULL, $filter = NULL) {

  // Local variables for form properties.
  $min_height = 370;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets BIMS_MVIEW_CROSS.
  $bm_cross = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));

  // Gets the cross object.
  $cross = NULL;
  if (isset($form_state['values']['cross_list']['selection'])) {
    $nd_experiment_id = $form_state['values']['cross_list']['selection'];
  }
  if (preg_match("/^\d+$/", $nd_experiment_id)) {
    $cross = $bm_cross->getCross($nd_experiment_id);
  }

  // Checks if it has a cross group.
  $flag_cross_group = $bm_cross->hasCrossGroup();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_cross');
  $bims_panel->init($form);

  // Adds the admin menu.
  $form['cross_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Cross',
    '#attributes'   => array(),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_cross_add";
  $form['cross_admin']['menu']['add_btn'] = array(
    '#name'       => 'add_btn',
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_cross_add', 'Add Cross', '$url'); return (false);",
    ),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_cross_delete";
  $form['cross_admin']['menu']['mass_delete_btn'] = array(
    '#name'       => 'mass_delete_btn',
    '#type'       => 'button',
    '#value'      => 'Delete',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_cross_delete', 'Delete Crosses', '$url'); return (false);",
    ),
  );
  $form['cross_admin']['menu']['#theme'] = 'bims_panel_main_cross_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['cross_admin']);

  // Lists the all crosses.
  $form['cross_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Cross',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Cross type section.
  $def_values = array('cross');
  if ($flag_cross_group) {
    $opt_filter = array(
      'cross_group' => 'Cross&nbsp;Groups',
      'cross'       => 'Crosses',
    );
    if ($filter) {
      $def_values = explode(':', $filter);
    }
    $form['cross_list']['filter'] = array(
      '#title'          => 'Cross Groups',
      '#type'           => 'checkboxes',
      '#options'        => $opt_filter,
      '#default_value'  => $def_values,
      '#prefix'         => '<div style="padding:5px;border:1px solid lightgray;">',
      '#attributes'     => array('style' => 'backgroud-color:#EEEEEE;'),
    );
    $form['cross_list']['update_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'update_btn',
      '#value'      => 'Update',
      '#attributes' => array('style' => 'width:100px;'),
      '#suffix'     => '</div>',
      '#ajax'       => array(
        'callback' => "bims_panel_main_cross_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-cross-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Cross selection.
  $options    = _bims_panel_main_cross_form_get_opt_cross($bims_program, $bm_cross, $def_values);
  $num_cross  = sizeof($options);
  if (!$num_cross) {
    $form['cross_list']['no_cross'] = array(
      '#markup' => 'No cross is associated with the current program.',
    );
  }
  else {

    // Adds 'Find' textfield.
    if ($num_cross > BIMS_DDL_MAX_LIST) {
      $form['cross_list']['cross_auto'] = array(
        '#type'               => 'textfield',
        '#attributes'         => array('style' => 'width:250px;'),
        '#autocomplete_path'  => "bims/bims_autocomplete_cross/$program_id",
      );
      $form['cross_list']['cross_details_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'cross_details_btn',
        '#value'      => 'Details',
        '#attributes' => array('style' => 'width:100px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_cross_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-cross-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds the cross list.
    if ($num_cross > BIMS_DDL_MAX_LIST) {
      $msg = "You have <b>$num_cross</b> crosses.<br />" .
        "The maximum  number of $num_cross listed in a dropdown list is <b>" . BIMS_DDL_MAX_LIST . "</b>.<br />
        Please use the auto-complete textbox above to find a cross.";
      $form['cross_list']['selection'] = array(
        '#markup' => "<div>$msg</div>",
      );
    }
    else {
      $desc = "Please select a cross from the list<br />" .
        "<em><b>*</b></em> indicates it has the progenies.";
      if ($flag_cross_group) {
        $desc .= "<br /><em><b>+</b></em> indicates cross group.";
      }
      $form['cross_list']['selection'] = array(
        '#type'           => 'select',
        '#options'        => $options,
        '#size'           => 14,
        '#prefix'         => "You have <b>$num_cross</b> crosses",
        '#description'    => $desc,
        '#default_value'  => $nd_experiment_id,
        '#attributes'     => array('style' => 'width:100%;'),
        '#ajax'           => array(
          'callback' => "bims_panel_main_cross_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-cross-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
  }
  $form['cross_list']['#theme'] = 'bims_panel_main_cross_form_cross_list';

  // Cross details.
  $form['cross_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Cross Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  if ($cross) {
    $form['cross_details']['table']['view_btn'] = array(
      '#type'       => 'button',
      '#value'      => 'View',
      '#name'       => 'view_btn',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_cross_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-cross-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['cross_details']['table']['prop'] = array(
      '#type'         => 'fieldset',
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Properties',
      '#attributes'   => array('style' => 'width:90%;'),
    );
    $form['cross_details']['table']['prop']['table'] = array(
      '#markup' => '<em>No propery found</em>',
    );

    // Adds action buttons.
    if (bims_can_admin_action($bims_user)) {

      // Adds 'Edit' button.
      if ($cross->chado) {
        $form['cross_details']['table']['edit_btn'] = array(
          '#type'       => 'button',
          '#value'      => 'Edit',
          '#name'       => 'edit_btn',
          '#attributes' => array('style' => 'width:90px;'),
          '#ajax'       => array(
            'callback' => "bims_panel_main_cross_form_ajax_callback",
            'wrapper'  => 'bims-panel-main-cross-form',
            'effect'   => 'fade',
            'method'   => 'replace',
          ),
        );
      }

      // Adds 'Delete' button.
      $form['cross_details']['table']['delete_btn'] = array(
        '#type'       => 'button',
        '#value'      => 'Delete',
        '#name'       => 'delete_btn',
        '#disabled'   => !$bims_user->canEdit(),
        '#attributes' => array(
          'style' => 'width:90px;',
          'class' => array('use-ajax', 'bims-confirm'),
        ),
        '#ajax'       => array(
          'callback' => "bims_panel_main_cross_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-cross-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
  }
  $form['cross_details']['table']['mview_cross'] = array(
    '#type'   => 'value',
    '#value'  => $cross,
  );
  $form['cross_details']['table']['bims_mview_cross'] = array(
    '#type'   => 'value',
    '#value'  => $bm_cross,
  );
  $form['cross_details']['table']['#theme'] = 'bims_panel_main_cross_form_cross_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-cross-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_cross_form_submit';
  return $form;
}

/**
 * Gets the options for the crosses.
 *
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_MVIEW_CROSS $bm_cross
 * @param array $def_values
 *
 * @return array
 */
function _bims_panel_main_cross_form_get_opt_cross(BIMS_PROGRAM $bims_program, BIMS_MVIEW_CROSS $bm_cross, $def_values) {

  // Gets the crosses.
  $crosses      = $bm_cross->getCrosses(BIMS_OBJECT, $def_values);
  $public_chado = $bims_program->isPublicChado();
  $opt_cross    = array();
  foreach ($crosses as $cross) {
    $symbols = '';
    if (!$public_chado) {
      $symbols .= $cross->chado ? '#' : '';
    }
    $symbols .= $cross->num_progeny ? '*' : '';
    if ($symbols) {
      $symbols = " $symbols";
    }
    $plus = $cross->num_cross ? '+ ' : '';
    $opt_cross[$cross->nd_experiment_id] = $plus . $cross->cross_number . $symbols;
  }
  return $opt_cross;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_cross_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Update" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets the chosen descriptor groups.
    $filters = $form_state['values']['cross_list']['filter'];
    $filter_arr = array();
    foreach ((array)$filters as $type => $bool) {
      if ($bool) {
        $filter_arr []= $type;
      }
    }

    // Updates panels.
    $filter_str = implode(':', $filter_arr);
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_cross/NC/$filter_str")));
  }
  else {

    // Gets nd_experiment_id from the dropdownlist.
    $nd_experiment_id = NULL;
    if (array_key_exists('selection', $form_state['values']['cross_list'])) {
      $nd_experiment_id = $form_state['values']['cross_list']['selection'];
    }

    // Gets the nd_experiment_id from the form.
    if (!$nd_experiment_id) {
      $cross = $form_state['values']['cross_details']['table']['mview_cross'];
      $nd_experiment_id = $cross->nd_experiment_id;
    }
    if ($nd_experiment_id) {

      // If 'View' was clicked.
      if ($trigger_elem == 'view_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_phenotype_cross/$nd_experiment_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_phenotype_cross', 'Phenotype by Cross', $url)));
      }

      // If "Edit" was clicked.
      else if ($trigger_elem == 'edit_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_cross_edit/$nd_experiment_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_cross_edit', 'Edit Cross', $url)));
      }

      // If "Delete" was clicked.
      else if ($trigger_elem == 'delete_btn') {

        // Deletes the cross.
        if (_bims_panel_main_cross_form_delete_cross($program_id, $nd_experiment_id)) {
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_cross')));
        }
      }
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Deletes the cross.
 *
 * @param integer $program_id
 * @param integer $nd_experiment_id
 *
 * @return boolean
 */
function _bims_panel_main_cross_form_delete_cross($program_id, $nd_experiment_id) {

  $transaction = db_transaction();
  try {

    // Deletes the cross.
    $bc_cross = new BIMS_CHADO_CROSS($program_id);
    if (!$bc_cross->deleteCrosses(array($nd_experiment_id))) {
      throw new Exception("Error : Failed to delete the cross");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_cross_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_cross_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_cross';

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Details" was clicked.
  if ($trigger_elem == 'cross_details_btn') {

    // Gets the cross.
    $cross_number = trim($form_state['values']['cross_list']['cross_auto']);
    $bm_cross     = new BIMS_MVIEW_CROSS(array('node_id' => $program_id));
    $cross        = $bm_cross->byCrossNumber($cross_number);
    if ($cross) {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_cross/' . $cross->nd_experiment_id)));
    }
    else {
      drupal_set_message("Cross '$cross_number' does not exists.");
    }
  }
}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_cross_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), 'Add a new cross / property.');
//  $rows[] = array(drupal_render($element['mass_delete_btn']), 'Delete Multiple Crosses.');
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the cross list.
 *
 * @param $variables
 */
function theme_bims_panel_main_cross_form_cross_list($variables) {
  $element = $variables['element'];

  // Adds the no cross message.
  $layout = '';
  if (array_key_exists('no_cross', $element)) {
    $layout .= "<div>" . drupal_render($element['no_cross']) . "</div>";
  }
  else {

    // Adds autocompute.
    if (array_key_exists('cross_auto', $element)) {
      $layout .= '<div style="float:left;width:40px;margin-top:8px;"><em><b>Find</b></em></div>' .
        ' <div style="float:left;">' . drupal_render($element['cross_auto']) .
        '</div><div style="float:left;margin-left:10px;">' . drupal_render($element['cross_details_btn']) . '</div><br clear="all" />';
    }

    // Adds the cross filter.
    if (array_key_exists('filter', $element)) {
      $layout .= '<div>' . drupal_render($element['filter']);
      $layout .= drupal_render($element['update_btn']) . '</div>';
    }

    // Adds the cross list.
    $layout .= '<div>' . drupal_render($element['selection']) . '</div>';
  }
  return $layout;
}

/**
 * Theme function for the cross details table.
 *
 * @param $variables
 */
function theme_bims_panel_main_cross_form_cross_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_MVIEW_CROSS.
  $bm_cross = $element['bims_mview_cross']['#value'];
  $cross    = $element['mview_cross']['#value'];

  // Initializes the properties.
  $cross_number     = '--';
  $paternal         = '--';
  $maternal         = '--';
  $num_progeny      = '--';
  $num_breeding     = '--';
  $progeny          = '--';
  $view_btn         = '--';
  $comments         = '';
  $properties       = '';
  $num_child_cross  = '';
  $child_cross      = '';
  $cross_group_row  = '';
  $actions          = '';

  // Updates the properites.
  if ($cross) {
    $nd_experiment_id = $cross->nd_experiment_id;
    $cross_number     = $cross->cross_number;
    $num_progeny      = $cross->num_progeny;
    $num_breeding     = $cross->num_breeding;
    $paternal         = ($cross->paternal) ? $cross->paternal : '<em>N/A</em>';
    $maternal         = ($cross->maternal) ? $cross->maternal : '<em>N/A</em>';
    $view_btn         = ($num_breeding) ? drupal_render($element['view_btn']) : '--';

    // Child Cross Group.
    $num_child_cross = $cross->num_cross;
    $total_progeny = 0;
    if ($num_child_cross) {
      $cross_number .= ' (<em>Cross Group</em>)';

      // Gets the child crosses.
      $child_cross_arr = $bm_cross->getChildCross($cross->nd_experiment_id);
      $link_arr = array();
      foreach ($child_cross_arr as $child_cross) {
        $ne_id  = $child_cross->nd_experiment_id;
        $label  = $child_cross->cross_number;
        $link_arr []= "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel('bims_panel_main_cross/$ne_id/cross')\">$label</a>";
      }
      $child_cross = implode("<br />", $link_arr);
    }

    // Cross Group.
    if ($cross->cross_group_id) {
      $cross_group_id = $cross->cross_group_id;
      $cross_group    = $cross->cross_group;
      $cross_group_row = "<a href=\"javascript:void(0);\" onclick=\"bims.load_primary_panel('bims_panel_main_cross/$cross_group_id/cross_group:cross')\">$cross_group</a>";;
    }

    // Lists the all progenies.
    $progenies = $bm_cross->getProgenies($nd_experiment_id, BIMS_OPTION);
    if ($progenies) {
      $options    = '';
      $count      = 0;
      $accession  = '';
      foreach ($progenies as $stock_id => $name) {
        $count++;
        $options .= "<option value='$stock_id'>$name</option>";
      }

      // Updates the size.
      if ($count < 1) {
        $progeny = '<em>No progeny for this cross</em>';
      }
      else if ($count == 1) {
        $progeny = $name;
      }
      else {
        $size = 5;
        if ($count < 5) {
          $size = $count;
        }
        $progeny = "<SELECT size='$size' style='width:250px;'>$options</SELECT>";
      }
    }
    else {
      $progeny = '<em>No progeny for this cross</em>';
    }

    // Adds the properties.
    $rows = array();
    $bims_program = BIMS_PROGRAM::byID($cross->node_id);
    $props = $bims_program->getProperties('cross', 'details', BIMS_FIELD);
    if (!empty($props)) {
      foreach ($props as $field => $cvterm_name) {
        if (property_exists($cross, $field)) {
          $value = $cross->{$field};
          if ($value) {
            $rows []= array($cvterm_name, $value);
          }
        }
      }
    }
    if (!empty($rows)) {
      $table_vars = array(
        'header'      => array(array('data' => 'name', 'width' => 100), 'value'),
        'rows'        => $rows,
        'attributes'  => array(),
      );
      $element['prop']['table']['#markup'] = theme('table', $table_vars);
      $properties = drupal_render($element['prop']);
    }

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }

    // Adds the comments.
    if ($cross->comments) {
      $comments = '<textarea style="width:100%;" rows=2 READONLY>' . $cross->comments . '</textarea>';
    }
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Cross Number', 'width' => '100'), $cross_number);
  $rows[] = array('Maternal Parent', $maternal);
  $rows[] = array('Paternal Parent', $paternal);
  if ($num_child_cross) {
    $rows[] = array('# cross', $num_child_cross);
    $rows[] = array('Crosses', $child_cross);
  }
  if ($cross_group_row) {
    $rows[] = array('Cross&nbsp;Group', $cross_group_row);
  }
  if ($num_progeny) {
    $rows[] = array('# progenies', $num_progeny);
  }
  if ($num_breeding) {
    $rows[] = array('# data point', $num_breeding);
  }
  $rows[] = array('Progenies', $progeny);
  if ($view_btn != '--') {
    $rows[] = array('Statistics / Data', $view_btn);
  }
  if ($comments) {
    $rows[] = array('Comments', $comments);
  }
  if ($properties) {
    $rows[] = array('Properties', $properties);
  }
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }

  // Creates the cross details table.
  $table_vars = array(
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_cross_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('cross_admin', $form)) {
    $layout .= "<div>" . drupal_render($form['cross_admin']) . "</div>";
  }

  // Adds "cross" list.
  $layout .= "<div style='float:left;width:44%;'>" . drupal_render($form['cross_list']) . "</div>";

  // Adds the cross info table.
  $layout .= "<div style='float:left;width:55%;margin-left:5px;'>" . drupal_render($form['cross_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}