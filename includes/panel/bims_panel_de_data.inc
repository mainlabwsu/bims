<?php
/**
 * @file
 */
/**
 * DE - Data Edit form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_de_data_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_de_data');
  $bims_panel->init($form);

  // Data edit.
  $form['data_edit'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Data Edit',
    '#attributes'   => array('style' => 'min-height:360px;'),
  );
  $markup = "<div>You can edit the details of trait descriptor, location, cross,
    accession or trial in corresponding sections under '<em>Manage Breeding</em>'.
    The editing functionality of phenotype data is under development.</div>";

  $form['data_edit']['desc'] = array(
    '#markup' => $markup,
  );
  $form['#prefix'] = '<div id="bims-panel-de-data-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_de_data_form_submit';
  $form['#theme'] = 'bims_panel_de_data_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_de_data_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_de_data_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_de_data_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
 function theme_bims_panel_de_data_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the program list.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['data_edit']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
 }