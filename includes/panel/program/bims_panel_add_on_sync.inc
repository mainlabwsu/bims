<?php
/**
 * @file
 */
/**
 * Synchronize data panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sync_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_sync');
  $bims_panel->init($form);

  // Gets and saves  all types of the materialized views.
  $mview_types = array(
    'descriptor' => array(
      'label' => 'Descriptor',
      'desc'  => '',
     ),
    'location' => array(
      'label' => 'Location',
      'desc'  => '',
     ),
    'image' => array(
      'label' => 'Image',
      'desc'  => '',
     ),
    'stock' => array(
      'label' => 'Accession',
      'desc'  => '',
     ),
    'cross' => array(
      'label' => 'Cross',
      'desc'  => '',
     ),
    'marker' => array(
      'label' => 'Marker',
      'desc'  => '',
     ),
    'trial' => array(
      'label' => 'Trial',
      'desc'  => '',
     ),
    'phenotype' => array(
      'label' => 'Phenotypic data',
      'desc'  => '',
     ),
    'genotype' => array(
      'label' => 'Genotypic data',
      'desc'  => '',
     ),
  );

  // Lists the mview types.
  $form['sync'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Synchronize data by types',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  foreach ($mview_types as $type => $info) {
    $form['sync']['mview_type']['checkbox'][$type] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['label'],
      '#default_value'  => 0,
    );
  }
  $form['sync']['mview_type']['mview_types'] = array(
    '#type'   => 'value',
    '#value'  => $mview_types,
  );
  $form['sync']['mview_type']['#theme'] = 'bims_panel_add_on_sync_form_mview_table';
  $form['sync']['sync_data_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'sync_data_btn',
    '#value'      => 'Sync Data',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_sync_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-sync-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['sync']['sync_all_data_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'sync_all_data_btn',
    '#value'      => 'Sync All Data',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_sync_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-sync-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['sync']['close_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'close_btn',
    '#value'      => 'Close',
    '#attributes' => array('style' => 'width:150px;'),
    '#prefix'     => '<br />',
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_sync_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-sync-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-sync-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_sync_form_submit';
  $form['#theme'] = 'bims_panel_add_on_sync_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sync_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sync_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Sync Data' was clicked.
  if ($trigger_elem == 'sync_data_btn') {
    $count = 0;
    foreach ($form_state['values']['sync']['mview_type']['checkbox'] as $elem => $value) {
      $count += $value;
    }
    if ($count == 0) {
      form_set_error('bims_working_dir', "Please choose at least one");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sync_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Sync All Data' was clicked.
  if ($trigger_elem == 'sync_all_data_btn') {

  }
  else if ($trigger_elem == 'sync_data_btn') {

    // Creates the command line.
    $mviews = array();
    $msg = '';
    foreach ($form_state['values']['sync']['mview_type']['checkbox'] as $elem => $value) {
      if ($value) {
        $mviews []= $elem;
        $msg .= $form['sync']['mview_type']['checkbox'][$elem]['#title'] . '<br />';
      }
    }

    // Runs the command.
    if (!empty($mviews)) {
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd  = "$drush bims-update-mviews $program_id " . implode(':', $mviews) . ' > /dev/null 2>/dev/null & echo $!';
      $pid  = exec($cmd, $output, $return_var);
      $msg  = "The following materialized view will be updated. [PID=$pid]<br />" . implode(', ', $mviews);
      BIMS_MESSAGE::setMsg($msg);
      if ($return_var) {
        drupal_set_message("Error : Failed to update materialized views", 'error');
      }
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_program')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_sync')));
}

/**
 * Theme function for the location information table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_sync_form_mview_table($variables) {
  $element = $variables['element'];

  // Gets the mview types.
  $mview_types  = $element['mview_types']['#value'];
  $checkbox     = $element['checkbox'];

  // Adds all the mview types
  $rows = array();
  foreach ($mview_types as $type => $info) {
    $rows []= array(drupal_render($checkbox[$type]), $info['desc']);
  }

  // Sets the headers.
  $headers = array(
    array('data' => '&nbspType', 'style' => 'width:300px;'),
    array('data' => 'Description', 'style' => 'width:75%'),
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_sync_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "sync".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['sync']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}