<?php
/**
 * @file
 */
/**
 * Program edit panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_edit_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_program_edit');
  $bims_panel->init($form);

  // Changes the access of the program.
  $form['program_access'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Edit the access',
    '#collapsed'   => FALSE,
    '#collapsible' => TRUE,
  );
  $access     = $bims_program->getAccessByType(BIMS_STRING);
  $access_int = $bims_program->getAccessByType(BIMS_INT);
  $form['program_access']['desc'] = array(
    '#markup' => "<div style='margin-bottom:20px'>Your program is currently <b><em>$access</em></b>.</div>",
  );

  // Private Program.
  if ($access_int != BIMS_ACCESS_PRIVATE) {
    $desc ="
      <div style='width:600px;margin:10px 0px 10px 0px;'><b>BISM Private Program </b> :</div>
    ";
    $desc = '';
    $form['program_access']['private']['private_desc'] = array(
      '#markup' => $desc,
    );
    $form['program_access']['private']['private_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'private_btn',
      '#value'      => 'Make it private',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_program_edit_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-program-edit-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Public Program.
  if ($access_int != BIMS_ACCESS_PUBLIC) {
    $desc ="
      <div style='width:600px;margin:10px 0px 10px 0px;'><b>Public Program </b> : ... semi-public program.</div>
    ";
    $desc = '';
    $form['program_access']['public']['public_desc'] = array(
      '#markup' => $desc,
    );
    $form['program_access']['public']['public_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'public_btn',
      '#value'      => 'Make it public',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_program_edit_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-program-edit-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Site Public Program.

  //if ($access_int != BIMS_ACCESS_SITE_PUBLIC) {
  if (FALSE) {
    $desc ="
      <div style='width:600px;margin:10px 0px 10px 0px;'><b>Site Public Program : </em></b> <br /><em>Notes</em> : It may take some time to change
      the access of your program to <em><b>public</b></em>.
      Please check the activity of your program on the program panel. If the
      activity of the program is '<em><b>Changing Access</b></em>', BIMS is working on changing the access.</div>
    ";
    $form['program_access']['site_public']['site_public_desc'] = array(
      '#markup' => $desc,
    );
    $form['program_access']['site_public']['site_public_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'site_public_btn',
      '#value'      => 'Make it site public',
      '#attributes' => array('style' => 'width:200px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_program_edit_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-program-edit-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Properties of the program.
  $form['program_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Edit the program',
    '#collapsed'   => FALSE,
    '#collapsible' => TRUE,
  );
  $form['program_prop']['name'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Program Name'),
    '#description'    => t("Please provide a program name. Please use alphabets, dash and underscore"),
    '#default_value'  => $bims_program->getName(),
  );
  $form['program_prop']['description'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Description'),
    '#description'    => t("Please provide a description"),
    '#default_value'  => $bims_program->getDescription(),
  );
  $form['program_prop']['update_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'update_btn',
    '#value'      => 'Update',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_program_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-program-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['program_prop']['cancel_edit_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_edit_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_program_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-program-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-program-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_program_edit_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_edit_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Update' was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets BIMS_USER and BIMS_PROGRAM.
    $bims_user = getBIMS_USER();
    $bims_program = $bims_user->getProgram();

    // Gets program name.
    $name = trim($form_state['values']['program_prop']['name']);

    // Validates the program name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('program_prop][name', "Invalid program name : $error");
      return;
    }

    // Checks the program name for duplication in the program. Checks only
    // if the current name is different from the new one.
    if ($bims_program->getName() != $name) {
      $keys = array(
        'name'    => $name,
        'owner_id' => $bims_user->getUserID(),
      );
      $bims_program = BIMS_PROGRAM::byKey($keys);
      if ($bims_program) {
        form_set_error('program_prop][program_name', "The name ($name) has been taken. Please choose other name.");
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Make it private' was clicked.
  if ($trigger_elem == 'private_btn') {

    // If the current access is 'public', remove the data from Chado first.
    if ($bims_program->getAccess() == BIMS_ACCESS_SITE_PUBLIC) {


    }
    $bims_program->setAccess(BIMS_ACCESS_PRIVATE);
    $bims_program->update();
  }

  // If 'Make it public' was clicked.
  else if ($trigger_elem == 'public_btn') {

    // If the current access is 'public', remove the data from Chado first.
    if ($bims_program->getAccess() == BIMS_ACCESS_SITE_PUBLIC) {

    }
    $bims_program->setAccess(BIMS_ACCESS_PUBLIC);
    $bims_program->update();
  }

  // If 'Make it site public' was clicked.
  else if ($trigger_elem == 'site_public_btn') {


    // Moves the data to chodo schema.
    if (TRUE) {



    }
    $bims_program->setAccess(BIMS_ACCESS_SITE_PUBLIC);
    $bims_program->update();
  }

  // If 'Update' was clicked.
  else if ($trigger_elem == 'update_btn') {

    // Gets program properties.
    $name         = trim($form_state['values']['program_prop']['name']);
    $description  = trim($form_state['values']['program_prop']['description']);

    // Sets and updates program properties.
    $bims_program->setName($name);
    $bims_program->setDescription($description);
    if ($bims_program->update()) {
      drupal_set_message("The program has been updated");
    }
    else {
      drupal_set_message('Error : Failed to update the properties of the program', 'error');
    }

    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_top", array()));
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_program_edit')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_program')));
}