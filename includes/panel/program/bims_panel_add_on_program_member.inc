<?php
/**
 * @file
 */
/**
 * Program member panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_member_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 260;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_program_member');
  $bims_panel->init($form);

  // Adds manage program member.
  $form['members'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Current members',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Lists the current members.
  $form['members']['member_list']= array(
    '#type'       => 'select',
    '#title'      => t('Current members'),
    '#options'    => bims_get_opt_member_list($bims_program),
    '#size'       => 6,
    '#validated'  => TRUE,
    '#attributes' => array('style' => 'width:70%;background-color:#FFFFFF;'),
  );
  $form['members']['remove_member_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Remove Member',
    '#name'       => 'remove_member_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => 'bims_panel_add_on_program_member_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-program-member-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Manages members.
  $form['manage_members'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Add Members',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Lists non-members.
  $form['manage_members']['non_member_name']= array(
    '#type'               => 'textfield',
    '#title'              => t('Non-member'),
    '#attributes'         => array('style' => 'width:240px;'),
    '#autocomplete_path'  => 'bims/bims_autocomplete_non_program_member/' . $bims_program->getProgramID(),
    '#maxlength'          => 60,
  );
  $form['manage_members']['permission_list']= array(
    '#type'           => 'radios',
    '#title'          => t('Permission'),
    '#options'        => array('READ' => 'READ ONLY', 'EDIT' => 'EDIT'),
    '#default_value'  => 'READ',
    '#attributes'     => array('style' => 'width:10px;'),
  );
  $form['manage_members']['add_member_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Add Member',
    '#name'       => 'add_member_btn',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_program_member_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-program-member-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-program-member-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_program_member_form_submit';
  $form['#theme'] = 'bims_panel_add_on_program_member_form';
  return $form;
}

/**
 * Retruns the list of the current members.
 *
 * @param BIMS_PROGRAM $bims_program
 *
 * @return array of user objects.
 */
function bims_get_opt_member_list(BIMS_PROGRAM $bims_program) {
  $member_list = array();
  $member_objs = $bims_program->getMembers(BIMS_OBJECT);
  foreach ($member_objs as $obj) {
    $member_list[$obj->user_id] = $obj->name . ' - ' . $obj->permission;
  }
  return $member_list;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_member_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Remove Member" was clicked.
  if ($trigger_elem == "remove_member_btn") {

    // Gets user_id of the user to be deleted.
    $user_id = $form_state['values']['members']['member_list'];
    if (!bims_is_empty_string($user_id)) {
      $drupal_user = PUBLIC_USERS::byKey(array('uid' => $user_id));
      if ($bims_program->removeMember($user_id)) {
        drupal_set_message($drupal_user->getName() . " has been removed");

        // Updates the member list.
        $form['members']['member_list']['#options'] = bims_get_opt_member_list($bims_program);
      }
      else {
        drupal_set_message("Error : Failed to remove a user", 'error');
      }
    }
    else {
      drupal_set_message("Please choose a member to be removed");
    }
  }

  // If "Add Member" was clicked.
  else if ($trigger_elem == "add_member_btn") {
    $username  = $form_state['values']['manage_members']['non_member_name'];
    $user = PUBLIC_USERS::byKey(array('name' => $username));
    if ($user) {

      // Add the user as a member.
      $permission = $form_state['values']['manage_members']['permission_list'];
      if ($bims_program->addMember($user->getUid(), $permission)) {
        drupal_set_message("$username has been added as a member");
      }
      else{
        drupal_set_message("Error : Failed to add a user", 'error');
      }

      // Updates the member list.
      $form['members']['member_list']['#options'] = bims_get_opt_member_list($bims_program);
    }
    else {
      drupal_set_message("'<b>$username</b>' is not auth user.");
    }

    // Updates the form elements.
    $form['manage_members']['non_member_name']['#value']  = '';
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_member_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_member_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_program_member_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "members".
  $layout .= "<div style='float:left;width:45%;'>" . drupal_render($form['members']) . "</div>";

  // Adds "manage members".
  $layout .= "<div style='float:left;margin-left:5px;width:54%;'>" . drupal_render($form['manage_members']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}