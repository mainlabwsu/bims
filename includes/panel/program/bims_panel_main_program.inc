<?php
/**
 * @file
 */
/**
 * Program panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_program_form($form, &$form_state) {
  global $user;

  // Local variables for form properties.
  $min_height = 340;

  // Gets BIMS_USER and crop_id and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $crop_id = $bims_user->getCropID();

  // Gets BIMS_PROGRAM.
  $bims_program = NULL;
  $program_id = $bims_user->getProgramID();
  if (! $program_id && array_key_exists('values', $form_state)) {
    $program_id = $form_state ['values'] ['program_list'] ['selection'];
  }
  if ($program_id) {
    $bims_program = BIMS_PROGRAM::byID($program_id);
  }

  // Gets the progarm options from the existing programs.
  $params = array (
      'crop_id' => $crop_id,
      'user_id' => $bims_user->getUserID(),
      'public' => TRUE,
      'bims' => bims_get_config_setting('bims_public_program')
  );
  $programs = BIMS_PROGRAM::getPrograms($params);
  $program_options = array ();
  foreach ($programs as $pg) {
    $program_options [$pg->getProgramID()] = $pg->getName();
  }

  // Creates a form.
  $form = array ();
  $form ['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_program');
  $bims_panel->init($form);

  // Adds admin menu.
  $form ['program_admin'] ['menu'] = array (
      '#type' => 'fieldset',
      '#collapsed' => ! empty($program_options),
      '#collapsible' => TRUE,
      '#title' => 'Admin Menu for Program',
      '#attributes' => array ()
  );

  // Only the breeders can create a program.
  if ($bims_user->isBreeder()) {
    $url = 'bims/load_main_panel/bims_panel_add_on_program_add';
    $form ['program_admin'] ['menu'] ['add_btn'] = array (
        '#type' => 'button',
        '#value' => 'Create',
        '#attributes' => array (
            'style' => 'width:90px;',
            'onclick' => "bims.add_main_panel('bims_panel_add_on_program_add', 'Add Program', '$url'); return (false);"
        )
    );
  }

  // Only the admin can create a public Chado program.
  if ($bims_user->isAdmin()) {
    $url = 'bims/load_main_panel/bims_panel_add_on_program_add/chado';
    $form ['program_admin'] ['menu'] ['add_chado_btn'] = array (
      '#type' => 'button',
      '#value' => 'Create',
      '#attributes' => array (
        'style' => 'width:90px;',
        'onclick' => "bims.add_main_panel('bims_panel_add_on_program_add', 'Add Program', '$url'); return (false);"
      )
    );
  }

  // Adds member button.
  if ($bims_user->isOwner() || $bims_user->isEditUser()) {
    $form ['program_admin'] ['menu'] ['member_btn'] = array (
      '#name' => 'member_btn',
      '#type' => 'button',
      '#value' => 'Members',
      '#attributes' => array (
        'style' => 'width:90px;'
      ),
      '#ajax' => array (
        'callback' => 'bims_panel_main_program_form_ajax_callback',
        'wrapper' => 'bims-panel-main-program-form',
        'effect' => 'fade',
        'method' => 'replace'
      )
    );

    // Adds sync button.
    if ($bims_user->isBIMSAdmin()) {
      $form ['program_admin'] ['menu'] ['sync_btn'] = array (
        '#name' => 'sync_btn',
        '#type' => 'button',
        '#value' => 'Sync',
        '#attributes' => array (
          'style' => 'width:90px;'
        ),
        '#ajax' => array (
          'callback' => 'bims_panel_main_program_form_ajax_callback',
          'wrapper' => 'bims-panel-main-program-form',
          'effect' => 'fade',
          'method' => 'replace'
        )
      );
    }
  }
  $form ['program_admin'] ['menu'] ['#theme'] = 'bims_panel_main_program_form_admin_menu_table';
  if ($bims_user->isAnonymous()) {
    hide($form ['program_admin'] ['menu']);
  }

  // Lists the all programs.
  $form ['program_list'] = array (
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
      '#title' => 'Program Selection',
      '#attributes' => array (
          'style' => 'min-height:' . $min_height . 'px;'
      )
  );

  // Program selection.
  $form ['program_list'] ['selection'] = array (
      '#type' => 'select',
      '#options' => $program_options,
      '#default_value' => $program_id,
      '#size' => 11,
      '#attributes' => array (
          'style' => 'width:100%;'
      ),
      '#ajax' => array (
          'callback' => 'bims_panel_main_program_form_ajax_callback',
          'wrapper' => 'bims-panel-main-program-form',
          'effect' => 'fade',
          'method' => 'replace'
      )
  );

  // Program details.
  $form ['program_details'] = array (
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
      '#title' => 'Program Details',
      '#attributes' => array (
          'style' => 'min-height:' . $min_height . 'px;'
      )
  );

  // Stores $bims_program.
  $form ['program_details'] ['table'] ['bims_program'] = array (
      '#type' => 'value',
      '#value' => $bims_program
  );

  // Adds action buttons.
  if (bims_can_admin_action($bims_user)) {
    $form ['program_details'] ['table'] ['edit_btn'] = array (
        '#name' => 'edit_btn',
        '#type' => 'button',
        '#value' => 'Edit',
        '#attributes' => array (
            'style' => 'width:90px;'
        ),
        '#ajax' => array (
            'callback' => 'bims_panel_main_program_form_ajax_callback',
            'wrapper' => 'bims-panel-main-program-form',
            'effect' => 'fade',
            'method' => 'replace'
        )
    );
    $form ['program_details'] ['table'] ['delete_btn'] = array (
        '#name' => 'delete_btn',
        '#type' => 'button',
        '#value' => 'Delete',
        '#disabled' => ! $bims_user->isOwner(),
        '#attributes' => array (
            'style' => 'width:90px;',
            'class' => array (
                'use-ajax',
                'bims-confirm'
            )
        ),
        '#ajax' => array (
            'callback' => 'bims_panel_main_program_form_ajax_callback',
            'wrapper' => 'bims-panel-main-program-form',
            'effect' => 'fade',
            'method' => 'replace'
        )
    );
  }

  // Gets the member table.
  $member_table = _bims_panel_main_program_form_get_member_table($bims_user, $bims_program);
  if ($member_table) {
    $form ['program_details'] ['table'] ['member_table'] = array (
        '#type' => 'fieldset',
        '#collapsed' => TRUE,
        '#collapsible' => TRUE,
        '#title' => 'Program members',
        '#attributes' => array (
            'style' => 'width:100%;'
        )
    );
    $form ['program_details'] ['table'] ['member_table'] ['table'] = array (
        '#markup' => $member_table
    );
  }
  $form ['program_details'] ['table'] ['#theme'] = 'bims_panel_main_program_form_program_table';

  // Sets form properties.
  $form ['#prefix'] = '<div id="bims-panel-main-program-form">';
  $form ['#suffix'] = '</div>';
  $form ['#submit'] [] = 'bims_panel_main_program_form_submit';
  $form ['#theme'] = 'bims_panel_main_program_form';
  return $form;
}

/**
 * Returns the program members in a table.
 * Returns an empty if non-member.
 *
 * @param BIMS_USER $bims_user
 * @param BIMS_PROGRAM $bims_program
 *
 * @return string
 */
function _bims_panel_main_program_form_get_member_table(BIMS_USER $bims_user, BIMS_PROGRAM $bims_program = NULL) {

  // Checks the program.
  if (! $bims_program) {
    return '';
  }

  // Checks the user.
  if (! $bims_program->isMember($bims_user->getUserID())) {
    return '';
  }

  // Gets all members.
  $rows = array ();
  if ($bims_program) {
    $members = $bims_program->getMembers(BIMS_OBJECT);
    foreach ($members as $member) {
      $rows [] = array (
          $member->permission,
          $member->name
      );
    }
  }
  $table_vars = array (
      'header' => array (
          array (
              'data' => 'Permission',
              'width' => '10%'
          ),
          'Name'
      ),
      'rows' => $rows,
      'attributes' => array (),
      'empty' => '<em>None</em>'
  );
  return theme('table', $table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_program_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and program ID.
  $bims_user = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state ['triggering_element'] ['#name'];

  // If the 'program_list' was changed.
  if ($trigger_elem == 'program_list[selection]') {

    // Gets the chosen program ID.
    $program_id = $form_state ['values'] ['program_list'] ['selection'];
    $bims_program = BIMS_PROGRAM::byID($program_id);
    $form ['program_details'] ['table'] ['bims_program'] ['#value'] = $bims_program;

    // Updates user-program.
    $bims_user->setProgramID($program_id);

    // Updates the related panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_top", array ()));
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_sidebar", array ()));
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array (
        "bims_panel_main_program"
    )));
  }

  // If 'View' was clicked.
  else if ($trigger_elem == 'view_tree_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_trial_tree/$program_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array (
        'bims_panel_add_on_trial_tree',
        'Trial Tree',
        $url
    )));
  }

  // If 'Member' was clicked.
  else if ($trigger_elem == 'member_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_program_member/$program_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array (
        'bims_panel_add_on_program_member',
        'Members',
        $url
    )));
  }

  // If 'Sync' was clicked.
  else if ($trigger_elem == 'sync_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_sync/$program_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array (
        'bims_panel_add_on_sync',
        'Sync Data',
        $url
    )));
  }

  // If 'Edit' was clicked.
  else if ($trigger_elem == 'edit_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_program_edit/$program_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array (
        'bims_panel_add_on_program_edit',
        'Edit Program',
        $url
    )));
  }

  // If 'Delete' was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Sets the program node as an orphant.
    $bims_program = $bims_user->getProgram();
    $program_name = $bims_program->getName();
    $bims_program->setOrphant(TRUE);

    // Updates user-program.
    $bims_user->setProgramID('');

    // Uses the drush command 'bims-delete-program' to delete the program.
    $drush = bims_get_config_setting('bims_drush_binary');
    $cmd = "$drush bims-delete-program $program_id > /dev/null 2>/dev/null &";
    $pid = exec($cmd, $output, $return_var);
    if ($return_var) {
      BIMS_MESSAGE::addMsg("Error : Failed to delete the program", 'error');
    }
    else {

      // Updates the related panels.
      drupal_set_message("The program ($program_name) has been deleted");
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_top", array ()));
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_sidebar", array ()));
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array (
          "bims_panel_main_program"
      )));
    }
  }

  // Handles ajax callback.
  $bims_panel = $form ['bims_panel'] ['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_program_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_program_form_submit($form, &$form_state) {
}

/**
 * Theme function for the program admin menu table.
 *
 * @param
 *          $variables
 */
function theme_bims_panel_main_program_form_admin_menu_table($variables) {
  $element = $variables ['element'];

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates the admin menu table.
  $rows = array ();
  if (array_key_exists('add_btn', $element)) {
    $rows [] = array (
      drupal_render($element ['add_btn']),
      'Create a new private program.'
    );
  }
  if (array_key_exists('add_chado_btn', $element)) {
    $rows [] = array (
      drupal_render($element ['add_chado_btn']),
      'Create a new public Chado program.'
    );
  }
  if (array_key_exists('member_btn', $element)) {
    $rows [] = array (
      drupal_render($element ['member_btn']),
      'Manage program members.'
    );
  }
  if (array_key_exists('sync_btn', $element)) {
    $rows [] = array (
      drupal_render($element ['sync_btn']),
      'Manage synchronization of data'
    );
  }
  $table_vars = array (
    'header' => array (
      array (
        'data' => 'Action',
        'width' => '10%'
      ),
      'Description'
    ),
    'rows' => $rows,
    'attributes' => array (),
    'empty' => '<em>No menu</em>'
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the program information table.
 *
 * @param
 *          $variables
 */
function theme_bims_panel_main_program_form_program_table($variables) {
  $element = $variables ['element'];

  // Gets BIMS_PROGRAM.
  $bims_program = $element ['bims_program'] ['#value'];

  // Initializes the properties.
  $owner = '--';
  $name = '--';
  $access = '--';
  $created = '--';
  $desc = '';
  $actions = '';
  $activity = '';

  // Updates the properites.
  if ($bims_program) {
    $name = $bims_program->getName();
    $created = $bims_program->getCreateDate();
    $access = $bims_program->getAccessByType(BIMS_STRING);
    $desc = $bims_program->getDescription();
    $activity = $bims_program->getActivity();

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element ['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element ['edit_btn']);
    }

    // Show owner if the program is private.
    if ($bims_program->isPrivate()) {
      $owner = $bims_program->getOwnerName();
    }
  }

  // Adds the description of the program.
  $description_ta = "<textarea rows='3' readonly class='bims-description' style='width:100%;'>$desc</textarea>";

  // Creates the details table.
  $rows = array ();
  $rows [] = array (
      array (
          'data' => 'Name',
          'style' => 'width:90px;'
      ),
      $name
  );
  if ($owner != '--') {
    $rows [] = array (
        'Owner',
        $owner
    );
  }
  $rows [] = array (
      'Access',
      $access
  );
  $rows [] = array (
      'Created',
      $created
  );
  $rows [] = array (
      'Descriptionte a new public Chado program.',
      $description_ta
  );
  if ($actions) {
    $rows [] = array (
        'Actions',
        $actions
    );
  }
  if (array_key_exists('member_table', $element)) {
    $rows [] = array (
        'Members',
        drupal_render($element ['member_table'])
    );
  }
  if ($activity) {
    $rows [] = array (
        'Activity',
        $activity
    );
  }

  // Creates the details table.
  $table_vars = array (
      'header' => array (),
      'rows' => $rows,
      'attributes' => array ()
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the form.
 *
 * @param
 *          $variables
 */
function theme_bims_panel_main_program_form($variables) {
  $form = $variables ['form'];

  // Adds the panel components.
  $bims_panel = $form ['bims_panel'] ['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('program_admin', $form)) {
    $layout .= "<div style='float:left;width:100%;'>" . drupal_render($form ['program_admin']) . '</div>';
  }

  // Don't show the program select if no program is available.
  if (empty($form ['program_list'] ['selection'] ['#options'])) {
    return $layout;
  }

  // Adds warning message to select a program.
  $bims_user = getBIMS_USER();
  $program_id = $bims_user->getProgramID();
  if (!$program_id) {
    $layout .= "<div style='margin:5px;'><b><em>Please choose a program from the list below as your working program.</em></b></div>";
  }

  // Adds the program list.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form ['program_list']) . "</div>";

  // Adds the program info. table.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form ['program_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}