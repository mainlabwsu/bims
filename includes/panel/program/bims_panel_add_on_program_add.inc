<?php
/**
 * @file
 */
/**
 * Program add panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_add_form($form, &$form_state, $chado = NULL) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Sets access flag.
  $access = $chado ? TRUE : FALSE;
  $form['chado'] = array(
    '#type'   => 'value',
    '#value'  => $access,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_program_add');
  $bims_panel->init($form);

  // Properties of a program.
  $form['program_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Add a new program',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['program_prop']['program_name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Program Name'),
    '#description'  => t("Please provide a program name. Please use alphabets, dash and underscore"),
    '#required'       => TRUE,
  );

  // Adds BIMS columns scection.
  $form['program_prop']['fb_cols'] = array(
    '#markup' => _bims_panel_add_on_program_add_form_get_description(),
  );
  $def_bims_cols = BIMS_PROGRAM::getDefBIMSCols();
  $form['program_prop']['accession'] = array(
    '#type'           => 'textfield',
    '#title'          => t('1. Accession'),
    '#description'    => t("Please provide a column name for accession"),
    '#default_value'  => $def_bims_cols['accession'],
    '#attributes'     => array('style' => 'width:200px;'),
    '#required'       => TRUE,
  );
  $form['program_prop']['unique_id'] = array(
    '#type'           => 'textfield',
    '#title'          => t('2. Unique identifier'),
    '#default_value'  => $def_bims_cols['unique_id'],
    '#description'    => t("Please provide a column name for unique identifier"),
    '#attributes'     => array('style' => 'width:200px;'),
    '#required'       => TRUE,
  );
  $form['program_prop']['primary_order'] = array(
    '#type'           => 'textfield',
    '#title'          => t('3. Primary order'),
    '#description'    => t("Please provide a column name for primary order"),
    '#default_value'  => $def_bims_cols['primary_order'],
    '#attributes'     => array('style' => 'width:200px;'),
    '#required'       => TRUE,
  );
  $form['program_prop']['secondary_order'] = array(
    '#type'           => 'textfield',
    '#title'          => t('4. Secondary order'),
    '#description'    => t("Please provide a column name for secondary order"),
    '#default_value'  => $def_bims_cols['secondary_order'],
    '#attributes'     => array('style' => 'width:200px;'),
    '#required'       => TRUE,
  );
  $form['program_prop']['description'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please provide a description of your program"),
    '#rows'         => 2,
  );
  $form['program_prop']['add_program_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_program_btn',
    '#value'      => 'Create',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_program_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-program-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['program_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_program_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-program-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-program-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_program_add_form_submit';
  return $form;
}

/**
 * Returns the description about columns.
 */
function _bims_panel_add_on_program_add_form_get_description() {

  // Adds the description.
  $desc = "<div>BIMS required to set the following values.</div>";

  // Sets the headers.
  $headers = array(
    array('data' => 'Required columns', 'width' => 120),
    array('data' => 'Default values', 'width' => 100),
    'Description',
  );

  // Adds rows.
  $bims_def_cols = BIMS_PROGRAM::getDefBIMSCols();
  $rows = array(
    array('Accession', $bims_def_cols['accession'], ''),
    array('Unique ID', $bims_def_cols['unique_id'], ''),
    array('Primary&nbsp;order', $bims_def_cols['primary_order'], ''),
    array('Secondary&nbsp;order', $bims_def_cols['secondary_order'], ''),
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:70%'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  $desc .= bims_theme_table($table_vars);
  return $desc;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_add_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Cancel' was clicked.
  if ($trigger_elem == 'cancel_btn') {
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_program_add')));
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_add_form_validate($form, &$form_state) {
  global $user;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Add' was clicked.
  if ($trigger_elem == 'add_program_btn') {

    // Gets program name.
    $name = trim($form_state['values']['program_prop']['program_name']);

    // Validates the program name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('program_prop][name', "Invalid program name : $error");
      return;
    }

    // Checks the program name for duplication in bims_program.
    $keys = array(
      'name'      => $name,
      'owner_id'  => $user->uid,
    );
    $bims_program = BIMS_PROGRAM::byKey($keys);
    if ($bims_program) {
      form_set_error('program_prop][program_name', "The name ($name) has been taken. Please choose other name.");
      return;
    }

    // Checks the BIMS columns.
    $accession        = trim($form_state['values']['program_prop']['accession']);
    $unique_id        = trim($form_state['values']['program_prop']['unique_id']);
    $primary_order    = trim($form_state['values']['program_prop']['primary_order']);
    $secondary_order  = trim($form_state['values']['program_prop']['secondary_order']);
    $check_unique = array();
    if (!preg_match("/^[a-zA-z0-9_\- \.]+$/", $accession)) {
      form_set_error('program_prop][accession', "Invalid accession");
      return;
    }
    if (!preg_match("/^[a-zA-z0-9_\- ]+$/", $unique_id)) {
      form_set_error('program_prop][unique_id', "Invalid unique_id");
      return;
    }
    if (!preg_match("/^[a-zA-z0-9_\- ]+$/", $primary_order)) {
      form_set_error('program_prop][primary_order', "Invalid primary_order");
      return;
    }
    if (!preg_match("/^[a-zA-z0-9_\- ]+$/", $secondary_order)) {
      form_set_error('program_prop][secondary_order', "Invalid secondary_order");
      return;
    }

    // Checks uniqueness for FB columns.
    $check_unique = array($unique_id => TRUE);
    if (array_key_exists($primary_order, $check_unique)) {
      form_set_error('program_prop][primary_order', "Please avoid to use the same name.");
      return;
    }
    $check_unique[$primary_order] = TRUE;
    if (array_key_exists($secondary_order, $check_unique)) {
      form_set_error('program_prop][secondary_order', "Please avoid to use the same name'.");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_program_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Add' was clicked.
  if ($trigger_elem == 'add_program_btn') {

    // Gets BIMS_USER.
    $bims_user = getBIMS_USER();

    // Gets the properties of the program.
    $chado            = $form_state['values']['chado'];
    $name             = trim($form_state['values']['program_prop']['program_name']);
    $desc             = trim($form_state['values']['program_prop']['description']);
    $accession        = trim($form_state['values']['program_prop']['accession']);
    $unique_id        = trim($form_state['values']['program_prop']['unique_id']);
    $primary_order    = trim($form_state['values']['program_prop']['primary_order']);
    $secondary_order  = trim($form_state['values']['program_prop']['secondary_order']);

    // Adds the project.
    $transaction = db_transaction();
    try {

      // Sets the access.
      $access = $chado ? BIMS_ACCESS_PUBLIC_CHADO : BIMS_ACCESS_PRIVATE;

      // Adds a new program.
      $details = array(
        'name'        => $name,
        'description' => $desc,
        'crop_id'     => $bims_user->getCropID(),
        'owner_id'    => $bims_user->getUserID(),
        'access'      => $access,
      );
      $bims_program = BIMS_PROGRAM::addProgram($details);
      if (!$bims_program) {
        throw new Exception("Fail to add a new program");
      }

      // Sets the current program to the this program.
      $bims_user->setProgramID($bims_program->getProgramID());
      if (!$bims_user->update()) {
        throw new Exception("Error : Failed to update program_id in user properties");
      }

      // Updates the BIMS columns.
      $bims_cols = array(
        'accession'       => $accession,
        'unique_id'       => $unique_id,
        'primary_order'   => $primary_order,
        'secondary_order' => $secondary_order,
      );
      if (!$bims_program->setBIMSCols($bims_cols)) {
        throw new Exception("Error : Failed to update BIMS columns");
      }
      BIMS_MESSAGE::addMsg('A new program has been added');
    }
    catch (\Exception $e) {
      $transaction->rollback();
      $error = $e->getMessage();
      throw new DrupalUpdateException($error);
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_top", array()));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_sidebar", array()));
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_program_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_program')));
}
