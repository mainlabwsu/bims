<?php
/**
 * @file
 */
/**
 * Genotype panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $node_id
 */
function bims_panel_add_on_genotype_form($form, &$form_state, $node_id) {

  // Gets BIMS_USER and BIMS_TRIAL.
  $bims_user = getBIMS_USER();
  $trial = BIMS_TRIAL::byID($node_id);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_genotype');
  $bims_panel->init($form);

  // Adds "Genotype".
  $form['genotype'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Genotype',
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-genotype-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_genotype_form_submit';
  $form['#theme'] = 'bims_panel_add_on_genotype_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_genotype_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_genotype_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_genotype_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_genotype_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "Genotype".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['genotype']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
