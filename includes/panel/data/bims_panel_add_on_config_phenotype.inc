<?php
/**
 * @file
 */
/**
 * Config phenotype data panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $node_id
 */
function bims_panel_add_on_config_phenotype_form($form, &$form_state) {

  // Gets BIMS_PROGRAM and BIMS_USER.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_config_phenotype');
  $bims_panel->init($form);

  // Adds "config".
  $form['config'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Phenotyping Data Configuration',
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-config-phenotype-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_config_phenotype_form_submit';
  $form['#theme'] = 'bims_panel_add_on_config_phenotype_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_config_phenotype_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_config_phenotype_form_validate($form, &$form_state) {



}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_config_phenotype_form_submit($form, &$form_state) {





}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_config_phenotype_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "Config".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['config']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
