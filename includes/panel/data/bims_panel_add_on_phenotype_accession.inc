<?php
/**
 * @file
 */
/**
 * Accession phenotype panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $stock_id
 */
function bims_panel_add_on_phenotype_accession_form($form, &$form_state, $stock_id) {

  // Local variables for form properties.
  $min_height = 380;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_phenotype_accession');
  $bims_panel->init($form);

  // Gets BIMS_MVIEW_STOCK.
  $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getNodeID()));
  $accession = $mview_stock->getStock($stock_id, BIMS_OBJECT);

  // Gets BIMS_MVIEW_PHENOTYPE.
  $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $bims_program->getNodeID()));

  // Gets the selected cvterm ID.
  $cvterm_id = NULL;
  if (isset($form_state['values']['trait_stats']['trait_list'])) {
    $cvterm_id = $form_state['values']['trait_stats']['trait_list']['selection'];
  }

  // Gets the active descriptor list and set the default cvterm ID.
  $options = BIMS_MVIEW_STOCK_STATS::getStatDescriptors($stock_id, BIMS_OPTION);
  if (!$cvterm_id) {
    $cvterm_id = key($options);
  }

  // Error : Accession not found.
  if (!$accession) {
    $form['accession_info'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Accession Phenotyping Data',
    );
    $form['accession_info']['table'] = array(
      '#markup' => "<em>Accession [$stock_id] not found</em>",
    );
  }

  // Warn : No data found.
  else if (!$mview_phenotype->exists()) {
    $form['accession_info'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Accession Phenotyping Data',
    );
    $form['accession_info']['table'] = array(
      '#markup' => "<em>No data found for this accession [$stock_id]</em>",
    );
  }

  // Shows the accession pheonotype.
  else {

    // Adds the trait stats.
    $form['trait_stats'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Trait Statistics for ' . $accession->name,
    );

    // Adds trait selection.
    $form['trait_stats']['trait_list'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Statistical Traits',
      '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    );
    $form['trait_stats']['trait_list']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#default_value'  => $cvterm_id,
      '#size'           => 9,
      '#empty_options'  => 'No statistical trait found',
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_add_on_phenotype_accession_form_ajax_callback',
        'wrapper'  => 'bims-panel-add-on-phenotype-accession-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['trait_stats']['trait_list']['info'] = array(
      '#markup' => BIMS_MVIEW_DESCRIPTOR::getTraitTable($cvterm_id),
    );

    // Adds trait stats.
    $form['trait_stats']['trait_stats'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Statistical Information',
      '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    );
    $form['trait_stats']['trait_stats']['table'] = array(
      '#markup' => BIMS_MVIEW_STOCK_STATS::getStatsTable($cvterm_id, $bims_program->getNodeID(), $stock_id),
    );
    $form['trait_stats']['#theme'] = 'bims_panel_add_on_phenotype_accession_form_trait_stats';

    // Adds the phenotyping data.
    $form['data'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Phenotyping data for ' . $accession->name,
    );
    $form['data']['download_btn'] = array(
      '#name'       => 'download_btn',
      '#type'       => 'submit',
      '#value'      => t("Download data"),
      '#attributes' => array('style' => 'width:165px;'),
      '#suffix'     => '<br /><br />',
      '#ajax'   => array(
        'callback' => 'bims_panel_add_on_phenotype_accession_form_ajax_callback',
        'wrapper'  => 'bims-panel-add-on-phenotype-accession-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['data']['phenotype_data'] = array(
      '#markup' => _bims_panel_add_on_phenotype_accession_data_table($bims_user, $mview_phenotype, $accession),
    );
  }
  hide($form['data']['download_btn']);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-phenotype-accession-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_phenotype_accession_form_submit';
  $form['#theme'] = 'bims_panel_add_on_phenotype_accession_form';
  return $form;
}

/**
 * Creates a table to display phenotyping data.
 *
 * @param BIMS_USER $bims_user
 * @param BIMS_MVIEW_PHENOTYPE $mview_phenotype
 * @param object $accession
 *
 * @return string
 */
function _bims_panel_add_on_phenotype_accession_data_table($bims_user, $mview_phenotype, $accession) {

  // Gets the precision.
  $pricison_format = bims_get_config_setting('bims_precision_format');

  // Gets BIMS columns.
  $bims_program     = $bims_user->getProgram();
  $bims_cols        = $bims_program->getBIMSCols();

  // Adds the base columns.
  $headers = array();
  $headers []= array('data' => 'accession', 'field' => 'accession', 'header' => $bims_cols['accession']);
  $headers []= array('data' => 'unique_id', 'field' => 'unique_id', 'header' => $bims_cols['unique_id']);
  $headers []= array('data' => 'primary_order', 'field' => 'primary_order', 'header' => $bims_cols['primary_order']);
  $headers []= array('data' => 'secondary_order', 'field' => 'secondary_order', 'header' => $bims_cols['secondary_order']);

  // Adds descriptor columns.
  $config = $bims_user->getConfig('phenotype_descriptor_view');
  $params = array(
    'flag'  => BIMS_VARIOUS,
    'type'  => 'accession',
    'id'    => $accession->stock_id,
  );
  $active_descriptors = $bims_program->getActiveDescriptors($params);
  foreach ((array)$active_descriptors as $cvterm_id => $info) {
    if ((array_key_exists($cvterm_id, $config) && $config[$cvterm_id]) || !array_key_exists($cvterm_id, $config)) {
      $headers []= array('data' => "t$cvterm_id", 'field' => "t$cvterm_id", 'header' => $info['name'], 'format' => $info['format']);
    }
  }

  // Pager variables.
  $num_pages = 10;
  $element   = 0;

  // Gets phenotype data.
  $fields = NULL;
  if (function_exists('array_column')) {
    $fields = array_column($headers, 'field');
  }
  else {
    $fields = array_map(function($element) { return $element['field'];}, $headers);
  }
  $fields[] = 'sample_id';
  $query = db_select($mview_phenotype->getMView(), 'MV')
    ->fields('MV', $fields)
    ->condition('stock_id', $accession->stock_id, '=')
    ->extend('PagerDefault')->limit($num_pages)
    ->extend('TableSort')->orderByHeader($headers);
  $results = $query->execute();

  // Populates the table rows.
  $rows = array();
  while ($assoc = $results->fetchAssoc()) {

    $row = array();
    foreach ($headers as $header) {
      $value = $assoc[$header['field']];

      // Updates the precisoin of the value.
      if ($header['field'] == 'accession') {
        $stock_id   = $accession->stock_id;
        $sample_id  = $assoc['sample_id'];
        $value = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_sample\",\"Sample\",\"bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id\"); return (false);'>$value</a>";
      }
      else if ($header['field'] == 'sample_id') {
        continue;
      }
      else if ($value && array_key_exists('format', $header) && ($header['format'] == 'numeric' || $header['format'] == 'percent')) {
        $value = sprintf($pricison_format, $value);
      }
      $row []= $value;
    }
    $rows []= $row;
  }

  // Params for the sortable table and the pager.
  $stock_id = $accession->stock_id;
  $url = "bims/load_main_panel/bims_panel_add_on_phenotype_accession/$stock_id?";
  $param = array(
    'url' => $url,
    'id'  => 'bims_panel_add_on_phenotype_accession',
    'tab' => 'Phenotype',
  );

  // Creates the data table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => 'There is no phenotyping data.',
    'param'       => $param,
  );
  $table = bims_theme_table($table_vars);

  // Creates the pager.
  $pager_vars = array(
    'tags'        => array(),
    'element'     => $element,
    'parameters'  => array(),
    'quantity'    => $num_pages,
    'param'       => $param,
  );
  $pager = bims_theme_pager($pager_vars);
  return $table . $pager;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_accession_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'trait_list' was changed.
  if ($trigger_elem == 'stats_info[trait_list]') {
    $accession = $form['accession']['#value'];
    $cvterm_id = $form_state['values']['stats_info']['trait_list'];

    // Gets BIMS_MVIEW_STOCK_STATS.
    $keys = array(
      'node_id'   => $accession->node_id,
      'stock_id'  => $accession->stock_id,
      'cvterm_id' => $cvterm_id,
    );
    $stock_stats = BIMS_MVIEW_STOCK_STATS::byKey($keys);
    $table = _bims_phenotype_accession_get_trait_stats_table($stock_stats);
    $form['stats_info']['trait_stats']['#markup'] = $table;
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_accession_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_accession_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Download" was clicked.
  if ($trigger_elem == 'download_btn') {



  }
  else {
    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_add_on_phenotype_accession')));
  }
}

/**
 * Theme function for the trait stats.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_phenotype_accession_form_trait_stats($variables) {
  $element = $variables['element'];

   // Adds the trait list.
  $layout = "<div style='float:left;width:40%;'>" . drupal_render($element['trait_list']) . "</div>";

  // Adds the trait stats.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($element['trait_stats']) . "</div>";
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_phenotype_accession_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "accession error".
  if (array_key_exists('accession_info', $form)) {
    $layout .= "<div style='width:100%;'>" . drupal_render($form['accession_info']) . "</div>";
  }
  else {
    // Adds "trait stats".
    $layout .= "<div style='width:100%'>" . drupal_render($form['trait_stats']) . "</div>";

    // Adds "phenotype data".
    $layout .= "<div style='width:100%;'>" . drupal_render($form['data']) . "</div>";
  }
  $layout .= drupal_render_children($form);
  return $layout;
}

