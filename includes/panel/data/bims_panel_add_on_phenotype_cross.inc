<?php
/**
 * @file
 */
/**
 * Cross phenotype panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $nd_experiment_id
 */
function bims_panel_add_on_phenotype_cross_form($form, &$form_state, $nd_experiment_id) {

  // Local variables for form properties.
  $min_height = 380;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_phenotype_cross');
  $bims_panel->init($form);

  // Gets BIMS_MVIEW_CROSS.
  $details = array(
    'node_id'           => $bims_program->getNodeID(),
    'nd_experiment_id'  => $nd_experiment_id,
  );
  $bm_cross = new BIMS_MVIEW_CROSS($details);
  $cross    = $bm_cross->getCross($nd_experiment_id);
  $form['cross'] = array(
    '#type' => 'value',
    '#value' => $cross,
  );

  // Gets BIMS_MVIEW_PHENOTYPE.
  $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $bims_program->getNodeID()));

  // Gets the selected cvterm ID.
  $cvterm_id = NULL;
  if (isset($form_state['values']['trait_stats']['trait_list'])) {
    $cvterm_id = $form_state['values']['trait_stats']['trait_list'];
  }

  // Saves nd_experiment_id.
  $form['nd_experiment_id'] = array(
    '#type' => 'value',
    '#value' => $nd_experiment_id,
  );

  // Gets the active descriptor list and set the default cvterm ID.
  $options = BIMS_MVIEW_CROSS_STATS::getStatDescriptors($nd_experiment_id, BIMS_OPTION);
  if (!$cvterm_id) {
    $cvterm_id = key($options);
  }

  // Error : Cross not found.
  if (!$cross) {
    $form['cross_info'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Cross Phenotyping Data',
    );
    $form['cross_info']['table'] = array(
      '#markup' => "<em>Cross [$nd_experiment_id] not found</em>",
    );
  }

  // Warn : No data found.
  else if (!$mview_phenotype->exists()) {
    $form['cross_info'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Cross Phenotyping Data',
    );
    $form['cross_info']['table'] = array(
      '#markup' => "<em>No data found for this cross [$nd_experiment_id]</em>",
    );
  }

  // Shows the cross pheonotype.
  else {

    // Adds the trait stats.
    $form['cross_stats'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Trait Statistics for ' . $cross->cross_number,
    );

    // Adds trait selection.
    $form['cross_stats']['trait_list'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Statistical Traits',
      '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    );
    $form['cross_stats']['trait_list']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#default_value'  => $cvterm_id,
      '#size'           => 9,
      '#empty_options'  => 'No statistical trait found',
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_add_on_phenotype_cross_form_ajax_callback',
        'wrapper'  => 'bims-panel-add-on-phenotype-cross-form-trait-stats',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['cross_stats']['trait_list']['info'] = array(
      '#markup' => BIMS_MVIEW_DESCRIPTOR::getTraitTable($cvterm_id),
    );

    // Adds cross stats.
    $form['cross_stats']['trait_stats'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Statistical Information',
      '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
      '#prefix'       => '<div id="bims-panel-add-on-phenotype-cross-form-trait-stats">',
      '#suffix'       => '</div>',
    );
    $form['cross_stats']['trait_stats']['stats'] = array(
      '#markup' => _bims_panel_add_on_phenotype_cross_form_stats($cvterm_id, $bims_program->getProgramID(), $nd_experiment_id),
    );
    $form['cross_stats']['#theme'] = 'bims_panel_add_on_phenotype_cross_form_cross_stats';

    // Adds the phenotyping data.
    $form['data'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Phenotyping data for ' . $cross->cross_number,
    );
    $form['data']['download_btn'] = array(
      '#name'       => 'download_btn',
      '#type'       => 'submit',
      '#value'      => t("Download data"),
      '#attributes' => array('style' => 'width:165px;'),
      '#suffix'     => '<br /><br />',
      '#ajax'   => array(
        'callback' => 'bims_panel_add_on_phenotype_cross_form_ajax_callback',
        'wrapper'  => 'bims-panel-add-on-phenotype-cross-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['data']['phenotype_data'] = array(
      '#markup' => _bims_panel_add_on_phenotype_cross_data_table($bims_user, $mview_phenotype, $cross),
    );
  }
  hide($form['data']['download_btn']);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-phenotype-cross-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_phenotype_cross_form_submit';
  $form['#theme'] = 'bims_panel_add_on_phenotype_cross_form';
  return $form;
}

/**
 * Display statistics of phenotyping data.
 *
 * @param integer $cvterm_id
 * @param integer $program_id
 * @param integer $nd_experiment_id
 *
 * @return string
 */
function _bims_panel_add_on_phenotype_cross_form_stats($cvterm_id, $program_id, $nd_experiment_id) {
  $stats_table = BIMS_MVIEW_CROSS_STATS::getStatsTable($cvterm_id, $program_id, $nd_experiment_id);
  return $stats_table;

  // Gets the descriptor.
  $descrptor = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id);
  $format = $descrptor->getFormat();

  // Adds the tags for chart and/or table.
  $stats = '';
  if (preg_match("/(boolean|categorical|multicat)/", $format)) {
    $stats .= '<canvas id="bims_chart_cross" style="width:100%;"></canvas>';
  }
  else {
    $stats = '<canvas id="bims_chart_cross" style="width:100%;"></canvas><div id="bims_chart_cross_table">&nbsp</div>';
  }
  return $stats;
}


/**
 * Creates a table to display phenotyping data.
 *
 * @param BIMS_USER $bims_user
 * @param BIMS_MVIEW_PHENOTYPE $mview_phenotype
 * @param object $cross
 *
 * @return string
 */
function _bims_panel_add_on_phenotype_cross_data_table($bims_user, $mview_phenotype, $cross) {

  // Gets the precision.
  $pricison_format = bims_get_config_setting('bims_precision_format');

  // Gets the BIMS columns.
  $bims_program = $bims_user->getProgram();
  $bims_cols    = $bims_program->getBIMSCols();

  // Adds the base columns.
  $headers = array();
  $headers []= array('data' => 'accession', 'field' => 'accession', 'header' => $bims_cols['accession'], 'sort' => 'asc');
  $headers []= array('data' => 'unique_id', 'field' => 'unique_id', 'header' => $bims_cols['unique_id']);
  $headers []= array('data' => 'primary_order', 'field' => 'primary_order', 'header' => $bims_cols['primary_order']);
  $headers []= array('data' => 'secondary_order', 'field' => 'secondary_order', 'header' => $bims_cols['secondary_order']);

  // Adds descriptor columns.
  $config = $bims_user->getConfig('phenotype_descriptor_view');
  $params = array(
    'flag'  => BIMS_VARIOUS,
    'type'  => 'cross',
    'id'    => $cross->nd_experiment_id,
  );
  $active_descriptors = $bims_program->getActiveDescriptors($params);
  foreach ((array)$active_descriptors as $cvterm_id => $info) {
    if ((array_key_exists($cvterm_id, $config) && $config[$cvterm_id]) || !array_key_exists($cvterm_id, $config)) {
      $headers []= array('data' => "t$cvterm_id", 'field' => "t$cvterm_id", 'header' => $info['name'], 'format' => $info['format']);
    }
  }

  // Pager variables.
  $num_pages = 10;
  $element   = 0;

  // Gets phenotype data.
  $fields = NULL;
  if (function_exists('array_column')) {
    $fields = array_column($headers, 'field');
  }
  else {
    $fields = array_map(function($element) { return $element['field'];}, $headers);
  }

  $query = db_select($mview_phenotype->getMView(), 'MV')
    ->fields('MV', $fields)
    ->condition('nd_experiment_id', $cross->nd_experiment_id, '=')
    ->extend('PagerDefault')->limit($num_pages)
    ->extend('TableSort')->orderByHeader($headers);
  $results = $query->execute();

  // Populates the table rows.
  $rows = array();
  while ($assoc = $results->fetchAssoc()) {

    $row = array();
    foreach ($headers as $header) {
      $value = $assoc[$header['field']];

      // Updates the precisoin of the value.
      if ($value && array_key_exists('format', $header) && ($header['format'] == 'numeric' || $header['format'] == 'percent')) {
        $value  = sprintf($pricison_format, $value);
      }
      $row []= $value;
    }
    $rows []= $row;
  }

  // Params for the sortable table and the pager.
  $nd_experiment_id = $cross->nd_experiment_id;
  $url = "bims/load_main_panel/bims_panel_add_on_phenotype_cross/$nd_experiment_id?";
  $param = array(
    'url' => $url,
    'id'  => 'bims_panel_add_on_phenotype_cross',
    'tab' => 'Phenotype',
  );

  // Creates the data table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => 'There is no phenotyping data.',
    'param'       => $param,
  );
  $table = bims_theme_table($table_vars);

  // Creates the pager.
  $pager_vars = array(
    'tags'        => array(),
    'element'     => $element,
    'parameters'  => array(),
    'quantity'    => $num_pages,
    'param'       => $param,
  );
  $pager = bims_theme_pager($pager_vars);
  return $table . $pager;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_cross_form_ajax_callback($form, &$form_state) {

  // Gets program ID.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];
/*
  // If the 'trait_list' was changed.
  if ($trigger_elem == 'stats_info[trait_list]') {


    $cross      = $form['cross']['#value'];
    $cvterm_id  = $form_state['values']['stats_info']['trait_list'];

    // Gets BIMS_MVIEW_CROSS_STATS.
    $keys = array(
      'node_id'           => $cross->node_id,
      'nd_experiment_id'  => $cross->nd_experiment_id,
      'cvterm_id'         => $cvterm_id,
    );
    $cross_stats = BIMS_MVIEW_CROSS_STATS::byKey($keys);
    $table = _bims_phenotype_cross_get_trait_stats_table($cross_stats);
    $form['stats_info']['trait_stats']['#markup'] = $table;
  }
  else */
  if ($trigger_elem == 'cross_stats[trait_list][selection]') {

    // Gets cvterm_id.
    $cvterm_id        = $form_state['values']['cross_stats']['trait_list']['selection'];
    $nd_experiment_id = $form_state['values']['nd_experiment_id'];
    if ($cvterm_id && $nd_experiment_id) {
      $mview_descriptor = BIMS_MVIEW_DESCRIPTOR::byKey(array('cvterm_id' => $cvterm_id));
      $format = $mview_descriptor->getFormat();

      // Loads the chart.
      if (preg_match("/(boolean|categorical|multicat)/", $format)) {
        $params = "chart_cross:$program_id:doughnut:$cvterm_id:$nd_experiment_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "load_chart", array('bims_chart_trait', $params)));
      }
      else if ($mview_descriptor->isNumeric() || $format == 'date') {
        $params = "chart_cross:$program_id:bar:$cvterm_id:$nd_experiment_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "load_chart", array('bims_chart_trait', $params)));
      }
    }

    $form['cross_stats']['trait_stats']['stats']['#markup'] = _bims_panel_add_on_phenotype_cross_form_stats($cvterm_id, $program_id, $nd_experiment_id);


    // Refresh the details section.
    return $form['cross_stats']['trait_stats'];
  }
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_cross_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_cross_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Download" was clicked.
  if ($trigger_elem == 'download_btn') {



  }
  else {
    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_add_on_phenotype_cross')));
  }
}

/**
 * Theme function for the cross stats.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_phenotype_cross_form_cross_stats($variables) {
  $element = $variables['element'];

   // Adds the trait list.
  $layout = "<div style='float:left;width:40%;'>" . drupal_render($element['trait_list']) . "</div>";

  // Adds the trait stats.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($element['trait_stats']) . "</div>";
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_phenotype_cross_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "cross error".
  if (array_key_exists('cross_info', $form)) {
    $layout .= "<div style='width:100%;'>" . drupal_render($form['cross_info']) . "</div>";
  }
  else {
    // Adds "trait stats".
    $layout .= "<div style='width:100%'>" . drupal_render($form['cross_stats']) . "</div>";

    // Adds "phenotype data".
    $layout .= "<div style='width:100%;'>" . drupal_render($form['data']) . "</div>";
  }
  $layout .= drupal_render_children($form);
  return $layout;
}

