<?php
/**
 * @file
 */
/**
 * Trial phenotype data panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $node_id
 */
function bims_panel_add_on_phenotype_trial_form($form, &$form_state, $node_id) {

  // Local variables for form properties.
  $min_height = 380;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets BIMS_TRIAL and BIMS_MVIEW_PHENOTYPE.
  $trial            = BIMS_TRIAL::byID($node_id);
  $mview_phenotype  = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $trial->getRootID()));

  // Gets the selected cvterm ID.
  $cvterm_id = NULL;
  if (isset($form_state['values']['trait_stats']['trait_list'])) {
    $cvterm_id = $form_state['values']['trait_stats']['trait_list']['selection'];
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_phenotype_trial');
  $bims_panel->init($form);

  // Error : Trial not found / The materialized view not found.
  if (!$trial || !$mview_phenotype->exists()) {
    $form['trial_info'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Trial Phenotyping Data',
    );
    if (!$trial) {
      $msg = "<em>Trial [$node_id] not found</em>";
    }
    else {
      $msg = "There is no phenotyping data.</em>";
    }
    $form['trial_info']['error'] = array(
      '#markup' => $msg,
    );
  }

  // Shows the trial pheonotype.
  else {

    // Adds the trait stats.
    $form['trait_stats'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => ' Trait Statistics for ' . $trial->getName(),
    );

   // Gets the active descriptor list and set the default cvterm ID.
    $options = BIMS_MVIEW_PHENOTYPE_STATS::getStatDescriptors($node_id, BIMS_OPTION);
    if (!$cvterm_id) {
      $cvterm_id = key($options);
    }

    // Adds trait selection.
    $form['trait_stats']['trait_list'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Statistical Traits',
      '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    );
    $form['trait_stats']['trait_list']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#default_value'  => $cvterm_id,
      '#size'           => 9,
      '#empty_options'  => 'No statistical trait found',
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => "bims_panel_add_on_phenotype_trial_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-phenotype-trial-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['trait_stats']['trait_list']['info'] = array(
      '#markup' => BIMS_MVIEW_DESCRIPTOR::getTraitTable($cvterm_id),
    );

    // Adds trait stats.
    $form['trait_stats']['trait_stats'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Statistical Information',
      '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
    );
    $form['trait_stats']['trait_stats']['table'] = array(
      '#markup' => BIMS_MVIEW_PHENOTYPE_STATS::getStatsTable($cvterm_id, $node_id),
    );
    $form['trait_stats']['#theme'] = 'bims_panel_add_on_phenotype_trial_form_trait_stats';

    // Adds the phenotyping data.
    if (FALSE) {
      $form['data'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => TRUE,
        '#title'        => 'Phenotyping data for ' . $trial->getName(),
      );
      $form['data']['phenotype_data'] = array(
        '#markup' => _bims_panel_add_on_phenotype_trial_data_table($bims_user, $mview_phenotype, $trial),
      );
    }
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-phenotype-trial-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_phenotype_trial_form_submit';
  $form['#theme'] = 'bims_panel_add_on_phenotype_trial_form';
  return $form;
}

/**
 * Creates a table to display phenotyping data.
 *
 * @param BIMS_USER $bims_user
 * @param BIMS_MVIEW_PHENOTYPE $mview_phenotype
 * @param BIMS_TRIAL $trial
 *
 * @return string
 */
function _bims_panel_add_on_phenotype_trial_data_table(BIMS_USER $bims_user, BIMS_MVIEW_PHENOTYPE $mview_phenotype, BIMS_TRIAL $trial) {

  // Local variables.
  $node_id          = $trial->getNodeID();
  $pricison_format  = bims_get_config_setting('bims_precision_format');

  // Pager variables.
  $num_pages = 10;
  $element   = 0;

  // Sets the headers.
  $headers = array();

  // Gets the BIMS columns.
  $bims_program = $bims_user->getProgram();
  $bims_cols    = $bims_program->getBIMSCols();

  // Adds the base columns.
  $headers []= array('data' => 'accession', 'field' => 'accession', 'header' => $bims_cols['accession'], 'sort' => 'asc');
  $headers []= array('data' => 'unique_id', 'field' => 'unique_id', 'header' => $bims_cols['unique_id']);
  $headers []= array('data' => 'primary_order', 'field' => 'primary_order', 'header' => $bims_cols['primary_order']);
  $headers []= array('data' => 'secondary_order', 'field' => 'secondary_order', 'header' => $bims_cols['secondary_order']);
  $headers []= array('data' => 'site_name', 'field' => 'site_name', 'header' => 'Location');

  // Adds descriptor columns. Adds only active descriptors except ones that this user
  // want to exclude.
  $config = $bims_user->getConfig('phenotype_descriptor_view');
  $params = array(
    'flag'  => BIMS_VARIOUS,
    'type'  => 'trial',
    'id'    => $trial->getNodeID(),
  );
  $active_descriptors = $bims_program->getActiveDescriptors($params);
  foreach ($active_descriptors as $cvterm_id => $info) {
    if ((array_key_exists($cvterm_id, $config) && $config[$cvterm_id]) || !array_key_exists($cvterm_id, $config)) {
      $headers []= array('data' => "t$cvterm_id", 'field' => "t$cvterm_id", 'header' => $info['name'], 'format' => $info['format']);
    }
  }

  // Gets phenotype data.
  $fields = NULL;
  if (function_exists('array_column')) {
    $fields = array_column($headers, 'field');
  }
  else {
    $fields = array_map(function($element) { return $element['field'];}, $headers);
  }
  $query = db_select($mview_phenotype->getMView(), 'MV')
    ->fields('MV', $fields)
    ->condition('node_id', $node_id)
    ->extend('PagerDefault')->limit($num_pages)
    ->extend('TableSort')->orderByHeader($headers);
  $results = $query->execute();

  // Populates table rows.
  $rows = array();
  while ($assoc = $results->fetchAssoc()) {
    $row = array();
    foreach ($headers as $header) {
      $value = $assoc[$header['field']];

      // Updates the precisoin of the value.
      if ($value && ($header['format'] == 'numeric' || $header['format'] == 'percent')) {
        $value  = sprintf($pricison_format, $value);
      }
      $row []= $value;
    }
    $rows []= $row;
  }

  // Params for the sortable table and the pager.
  $url = "bims/load_main_panel/bims_panel_add_on_phenotype_trial/$node_id?";
  $param = array(
    'url' => $url,
    'id'  => 'bims_panel_add_on_phenotype_trial',
    'tab' => 'Phenotype by Trial',
  );

  // Creates the data table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => 'There is no phenotyping data.',
    'param'       => $param,
  );
  $table = bims_theme_table($table_vars);

  // Creates the pager.
  $pager_vars = array(
    'tags'        => array(),
    'element'     => $element,
    'parameters'  => array(),
    'quantity'    => $num_pages,
    'param'       => $param,
  );
  $pager = bims_theme_pager($pager_vars);
  return $table . $pager;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_trial_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Returns excel sheet array.
 *
 * @param BIMS_TRIAL $trial
 *
 * @return array
 */
function _bims_get_excel_sheet(BIMS_TRIAL $trial) {

  // Gets BIMS_MVIEW_PHENOTYPE.
  $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $trial->getRootID()));
  $mview = $mview_phenotype->getMView();

  // Creates SQL statement.
  $sql = "SELECT MV.* FROM {$mview} MV ORDER BY M.dataset_name, M.stock_uniquename";

  // Creates headers.
  $headers = array(
    'A' => array('field' => 'dataset_name',     'width' => 40, 'heading' => 'Trial', 'type' => 'text'),
    'B' => array('field' => 'stock_uniquename', 'width' => 40, 'heading' => 'Accession', 'type' => 'text'),
    'C' => array('field' => 'site_name',        'width' => 40, 'heading' => 'site_name',  'type' => 'text'),
  );

  // Adds sheet to sheets.
  $sheet = array(
    'name'    => 'Phenotype Data',
    'headers' => $headers,
    'args'    => array(),
  );
  return array($sheet);
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_trial_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_phenotype_trial_form_submit($form, &$form_state) {}

/**
 * Theme function for the trait stats.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_phenotype_trial_form_trait_stats($variables) {
  $element = $variables['element'];

   // Adds the trait list.
  $layout = "<div style='float:left;width:40%;'>" . drupal_render($element['trait_list']) . "</div>";

  // Adds the trait stats.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($element['trait_stats']) . "</div>";
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_phenotype_trial_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "trial error".
  if (array_key_exists('trial_info', $form)) {
    $layout .= "<div style='width:100%;'>" . drupal_render($form['trial_info']) . "</div>";
  }
  else {
    // Adds "trait stats".
    $layout .= "<div style='width:100%'>" . drupal_render($form['trait_stats']) . "</div>";

    // Adds "phenotype data".
    $layout .= "<div style='width:100%;'>" . drupal_render($form['data']) . "</div>";
  }
  $layout .= drupal_render_children($form);
  return $layout;
}

