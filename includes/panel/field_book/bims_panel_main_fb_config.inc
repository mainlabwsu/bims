<?php
/**
 * @file
 */
/**
 * Field Book configuration panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_config_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_fb_config');
  if (!$bims_panel->init($form, TRUE)) {
    return $form;
  }

  $form['config'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Field Book Configuration',
  );
  $form['config']['markup'] = array(
   '#markup' => '<br />',
  );

  // Field Book required columns
  $form['config']['custom_cols'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Field Book custom columns',
  );
  $accession = $bims_program->getBIMSLabel('accession');
  $markup = "Please choose custom fields from the 3 sections (trait, $accession and sample properties). If the trait is checked, you can choose the trait values of the ongoing trials to be displayed in the input file to note that the phenotyping for the plant is done already. The trait descriptors for phenotyping will be provided to Frield Book App via the trait file.";
  $form['config']['custom_cols']['markup'] = array(
    '#markup' => "<div>$markup</div><br />",
  );

  // Shows the lists of traits / accession properties / sample properties.
  _bims_panle_main_fb_config_add_list($form['config']['custom_cols'], $bims_user, 'trait', 'cvterm', 'Traits');
  _bims_panle_main_fb_config_add_list($form['config']['custom_cols'], $bims_user, 'accession', 'text', "$accession properties");
  _bims_panle_main_fb_config_add_list($form['config']['custom_cols'], $bims_user, 'sample', 'cvterm', 'Sample properties');

  // Field Book required columns
  $form['config']['req_cols'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Field Book required columns',
  );
  $markup = "Field Book input file requires 4 columns : <em><b>'Unique Identifier'</b>, '<b>Primary order</b>', <b>'Secondary order'</b> and <b>'Accession'</b></em>.<div style='margin-top:10px;'>Please change the default names of these columns.</div>";
  $form['config']['req_cols']['markup'] = array(
    '#markup' => "$markup<br />",
  );
  _bims_panle_main_fb_config_add_req_cols($form['config']['req_cols'], $bims_program);

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-fb-config-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_fb_config_form_submit';
  $form['#theme'] = 'bims_panel_main_program_form';
  return $form;
}

/**
 * Adds the list of traits.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panle_main_fb_config_add_req_cols(&$form, BIMS_PROGRAM $bims_program) {

  // Updates the values of the required columns. Gets the saved BIMS columns.
  $bims_cols            = $bims_program->getBIMSCols();
  $def_accession        = $bims_cols['accession'];
  $def_unique_id        = $bims_cols['unique_id'];
  $def_primary_order    = $bims_cols['primary_order'];
  $def_secondary_order  = $bims_cols['secondary_order'];

  // Form elements.
  $form['accession'] = array(
    '#type'           => 'textfield',
    '#title'          => t('1. Accession'),
    '#description'    => t("Please provide a column name for accession"),
    '#default_value'  => $def_accession,
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['unique_id'] = array(
    '#type'           => 'textfield',
    '#title'          => t('2. Unique identifier'),
    '#default_value'  => $def_unique_id,
    '#description'    => t("Please provide a column name for unique identifier"),
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['primary_order'] = array(
    '#type'           => 'textfield',
    '#title'          => t('3. Primary order'),
    '#description'    => t("Please provide a column name for primary order"),
    '#default_value'  => $def_primary_order,
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['secondary_order'] = array(
    '#type'           => 'textfield',
    '#title'          => t('4. Secondary order'),
    '#description'    => t("Please provide a column name for secondary order"),
    '#default_value'  => $def_secondary_order,
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['req_col_save_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'req_col_save_btn',
    '#value'      => 'Save',
    '#attributes' => array('style' => 'width:200px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_main_fb_config_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-config-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['#theme'] = 'bims_panel_main_fb_config_form_column_table';
}

/**
 * Adds the list of the provided type.
 *
 * @param array $form
 * @param BIMS_USER $bims_user
 * @param string $col_type
 * @param string $item_type
 * @param string $label
 */
function _bims_panle_main_fb_config_add_list(&$form, BIMS_USER $bims_user, $col_type, $item_type, $label) {

  // Adds a fieldset for the provied type.
  $form[$col_type] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => $label,
  );

  // Gets the list of items of the provided column type.
  $items = _bims_panle_main_fb_config_get_items($bims_user, $col_type, $item_type);
  if (empty($items)) {
    $form[$col_type]['no_data'] = array(
      '#markup' => '<div>No ' . strtolower($label) . ' to choose.</div>',
    );
    return;
  }
  foreach ($items as $id => $info) {
    $form[$col_type]['ids'][$id] = array(
      '#type'           => 'checkbox',
      '#title'          => $info['name'],
      '#default_value'  => $info['bool'],
    );
  }

  // Adds 'Save' button'
  $name = 'config_save_btn_' . $col_type;
  $form[$col_type][$name] = array(
    '#type'       => 'submit',
    '#name'       => $name,
    '#value'      => 'Save',
    '#attributes' => array('style' => 'width:200px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_fb_config_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-config-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form[$col_type]['#theme'] = 'bims_panel_main_fb_config_add_list_table';
}

/**
 * Returns the list of items of the provided type.
 *
 * @param BIMS_USER $bims_user
 * @param string $col_type
 * @param string $item_type
 *
 * @return array
 */
function _bims_panle_main_fb_config_get_items(BIMS_USER $bims_user, $col_type, $item_type) {

  // Gets the stored config list if exists.
  $bims_program = $bims_user->getProgram();
  $cv_id        = $bims_program->getCvID('sample');
  $config_list  = $bims_program->getPropByKey('config_' . $col_type);
  if (!$config_list) {
    $config_list = array();
  }

  // Returns the traits.
  $item_list = array();
  if ($col_type == 'trait') {
    $item_list = $bims_program->getDescriptors(NULL, BIMS_OPTION);
  }

  // Returns the accession properties.
  else if ($col_type == 'accession') {
    $item_list = BIMS_MVIEW_STOCK::getFBProperties();
  }

  // Returns the sample properties.
  else if ($col_type == 'sample') {
    $item_list = BIMS_FIELD_BOOK::getSampleProp($cv_id);
  }

  // Saves the information.
  $items = array();
  if (!empty($item_list)) {
    foreach ($item_list as $id => $value) {
      $bool = array_key_exists($id, $config_list) ?  $config_list[$id] : FALSE;
      $items[$id] = array(
        'name' => $value,
        'bool' => $bool,
      );
    }
  }
  return $items;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_config_form_ajax_callback($form, $form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' was clicked.
  if (preg_match("/^config_save_btn_(.*)$/", $trigger_elem, $matches)) {
    $col_type = $matches[1];

    $key = 'config_' . $col_type;
    $checkboxes = $form_state['values']['config']['custom_cols'][$col_type]['ids'];
    $config_list = array();
    foreach ($checkboxes as $id => $value) {
      $config_list[$id] = ($value) ? TRUE : FALSE;
    }
    $bims_program->setPropByKey($key, $config_list);
    $bims_program->update();
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_config_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' was clicked.
  if (preg_match("/^config_save_btn_(.*)$/", $trigger_elem, $matches)) {
    $col_type = $matches[1];
    $num_checked = 0;
    $checkboxes = $form_state['values']['config']['custom_cols'][$col_type]['ids'];
    foreach ($checkboxes as $id => $value) {
      if ($value) {
        $num_checked++;
      }
    }
    if ($num_checked == 0) {
      form_set_error('config][custom_cols][' . $col_type, "Please check at least one for " . ucfirst($col_type));
      return;
    }
  }

  // if 'Save' (req_col_save_btn) was clicked.
  else if ($trigger_elem == 'req_col_save_btn') {
    $elem_ids = array(
      'unique_id'       => 'Unique Identifier',
      'primary_order'   => 'Primary Order',
      'secondary_order' => 'Secondary Order',
      'accession'       => 'Accession',
//      'primary_order_start'       => 'Primary order start',
//      'primary_order_repeat'      => 'Primary order',
//      'primary_order_increment'   => 'Primary order increment',
//      'secondary_order_start'     => 'Secondary order start',
//      'secondary_order_increment' => 'Secondary order increment',
    );
    foreach ($elem_ids as $elem_id => $label) {
      $elem_value = $form_state['values']['config']['req_cols'][$elem_id];
      if (!$elem_value) {
        form_set_error('config][req_cols][' . $elem_id, "Please type value at $label");
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_config_form_submit($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // if 'Save' was clicked.
  if ($trigger_elem == 'req_col_save_btn') {

    // Saves properites as project prop.
    $elem_ids = array('unique_id', 'primary_order', 'secondary_order', 'accession');
    $bims_cols = array();
    foreach ($elem_ids as $elem_id) {
      $elem_value = $form_state['values']['config']['req_cols'][$elem_id];
      $bims_cols[$elem_id] = $elem_value;
    }
    if (!$bims_program->setBIMSCols($bims_cols)) {
      drupal_set_message("Error : Failed to update Field Book properties", 'error');
    }
    else {
      BIMS_MESSAGE::setMsg("The configuration of the Field Book required columns saved.");
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_sidebar", array()));
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_fb_config')));
    }
  }
}

/**
 * Theme function for the required column table.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_config_add_list_table($variables) {
  $element = $variables['element'];

  // Populates the table.
  $row      = array();
  $rows     = array();
  $count    = 0;
  $num_col  = 4;
  if (array_key_exists('ids', $element)) {
    foreach ($element['ids'] as $cvterm_id => $checkbox) {
      if (!preg_match("/^\d+$/", $cvterm_id)) {
        continue;
      }
      $count++;
      $row []= drupal_render($checkbox);
      if (($count % $num_col) == 0) {
        $rows []= $row;
        $row = array();
      }
    }
    if (!empty($row)) {
      $size = sizeof($row);
      if ($size < $num_col) {
        for ($i = $size; $i < $num_col; $i++) {
          $row []= '&nbsp;';
        }
      }
      $rows []= $row;
    }
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'empty'       => '<div style="margin-top:12px;height:25px;">This program currently does not have ' . strtolower($element['#title']) . '</div>',
    'attributes'  => array('style' => 'width:100%;'),
  );
  return  theme('table', $table_vars);
}

/**
 * Theme function for the required column table.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_config_form_column_table($variables) {
  $element = $variables['element'];

  // Creates field book columns table.
  $tag = "<div>" . drupal_render($element['markup']) . "</div>";
  $tag .= "<div style='float:left;'>" . drupal_render($element['accession']) . "</div><br clear='all'>";
  $tag .= "<div style='float:left;'>" . drupal_render($element['unique_id']) . "</div><br clear='all'>";
  $tag .= "<div style='float:left;'>" . drupal_render($element['primary_order']) . "</div><br clear='all'>";
  $tag .= "<div style='float:left;'>" . drupal_render($element['secondary_order']) . "</div><br clear='all'>";
  $tag .= "<div>" . drupal_render($element['req_col_save_btn']) . "</div>";
  return $tag;
}