<?php
/**
 * @file
 */
/**
 * Field Book upload exported trait file panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_trait_form($form, &$form_state, $file_id = '') {

  // Local variables for form properties.
  $min_height = 440;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets file_id.
  $bims_file = NULL;
  if (isset($form_state['values']['uploads']['upload_list'])) {
    $file_id = $form_state['values']['uploads']['upload_list'];
    $bims_file = BIMS_FILE::byID($file_id);
  }
  if (!$bims_file && $file_id) {
    $bims_file = BIMS_FILE::byID($file_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_fb_upload_field');
  if (!$bims_panel->init($form, TRUE, TRUE)) {
    return $form;
  }

  // Adds the uploads.
  $form['uploads'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Uploaded files',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $args = array(
    'type'          => 'ULT',
    'by_user_id'    => $bims_user->getUserID(),
    'program_id'    => $bims_user->getProgramID(),
    'date'          => TRUE,
  );
  $options = BIMS_FILE::getFiles($args, BIMS_OPTION);
  if (empty($options)) {
    $options = array('' => 'No uploaded trait file found');
  }
  $form['uploads']['upload_list'] = array(
    '#type'           => 'select',
    '#options'        => $options,
    '#size'           => 8,
    '#attributes'     => array('style' => 'width:100%;'),
    '#default_value'  => $file_id,
    '#ajax'           => array(
      'callback' => "bims_panel_main_fb_upload_trait_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-upload-trait-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Adds action buttons.
  if ($bims_file) {
    $form['uploads']['table']['view_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'view_btn',
      '#value'      => 'View',
      '#attributes' => array('style' => 'width:80px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_fb_upload_trait_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-fb-upload-trait-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['uploads']['table']['delete_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'delete_btn',
      '#value'      => 'Delete',
      '#attributes' => array(
        'style' => 'width:80px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_fb_upload_trait_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-fb-upload-trait-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['uploads']['table']['bims_file'] = array(
    '#type' => 'value',
    '#value' => $bims_file,
  );
  $form['uploads']['table']['#theme'] = 'bims_panel_main_fb_upload_trait_form_upload_table';

  // Upload the exported trait file.
  $form['upload'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Upload Exported Field Book Trait File',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['upload']['desc'] = array(
    '#markup' => "<div style='margin:5px 0px 10px 0px;'>Please browse your trait file and hit '<b>Upload</b>' button. Then add a description of your data file (<em>optional</em>) and click '<b>Submit</b>' button to start uploading your trait file. Only the new traits will be uploaded to the database.</div>",
  );
  $form['upload']['browse'] = array(
    '#type'               => 'managed_file',
    '#name'               => 'upload_file_btn',
    '#title'              => t('Trait File'),
    '#description'        => t('Please upload your trait file (.trt)'),
    '#upload_location'    => $bims_user->getUserDirURL(),
    '#upload_validators'  => array(
      'file_validate_extensions' => array('trt'),
      // Pass the maximum file size in bytes
      //'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
    ),
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_main_fb_upload_trait_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-upload-trait-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['upload']['file_desc'] = array(
    '#type'       => 'textarea',
    '#name'       => 'file_desc',
    '#title'      => 'Description of your file',
    '#rows'       => 3,
    '#attributes' => array('style' => 'max-width:450px;'),
  );
  $form['upload']['start_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'start_btn',
    '#value'      => 'Submit',
    '#suffix'     => '<br /><br />',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_main_fb_upload_trait_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-upload-trait-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-fb-upload-trait-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_fb_upload_trait_form_submit';
  $form['#theme'] = 'bims_panel_main_fb_upload_trait_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_trait_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_trait_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_trait_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'View' was clicked.
  if ($trigger_elem == 'view_btn') {

    // Gets BIMS_FILE.
    $bims_file = $form_state['values']['uploads']['table']['bims_file'];
    if ($bims_file) {
      $mcl_job_id = $bims_file->getPropByKey('mcl_job_id');
      $url = 'bims/load_main_panel/bims_panel_add_on_job_upload/' . $bims_file->getFileID();
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_job_upload', 'Upload', $url)));
    }
  }

  // If 'Delete' was clicked.
  else if ($trigger_elem == 'delete_btn') {

    // Gets BIMS_FILE.
    $bims_file = $form_state['values']['uploads']['table']['bims_file'];
    if ($bims_file) {

      // Deletes BIMS_FILE.
      if (!$bims_file->delete()) {
        BIMS_MESSAGE::addMsg("Failed to delete BIMS file.");
      }

      // Updates the panels.
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_fb_upload_trait')));
    }
  }

  // If 'Start' was clicked.
  else if ($trigger_elem == 'start_btn') {

    // Gets BIMS_USER, BIMS_PROGRAM and MCL_USER.
    $bims_user    = getBIMS_USER();
    $bims_program = $bims_user->getProgram();
    $mcl_user     = $bims_user->getMCL_USER();
    $bims_file_id = '';

    // Gets the values from the form_state.
    $description = trim($form_state['values']['upload']['file_desc']);
    $drupal_file = file_load($form_state['values']['upload']['browse']);

    // Uploads the file.
    if ($drupal_file) {

      // Moves the file to 'upload' directory.
      $filepath = BIMS_FILE::moveFile($bims_user, $drupal_file, 'upload');

      // Sets the email option.
      $send_email = $bims_user->getEmailOption('upload');

      // Sets the upload parameters.
      $param = array(
        'type'          => 'ULT',
        'program_id'    => $bims_program->getProgramID(),
        'cv_arr'        => $bims_program->getCvArr(),
        'template'      => 'fb_trait',
        'valid_formats' => BIMS_FIELD_BOOK::getFormats(),
        'send_email'    => $send_email,
      );
      $args = array(
        'mcl_user'    => $mcl_user,
        'filepath'    => $filepath,
        'description' => $description,
        'param'       => $param,
      );
      $bims_file = BIMS_FILE::upload($args);
      if (!$bims_file) {
        BIMS_MESSAGE::setMsg("Failed to upload file.");
      }
      else {
        $bims_file_id = '/' . $bims_file->getFileID();
      }
    }

    // Updates the panels.
    $url = 'bims_panel_main_fb_upload_trait' . $bims_file_id;
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($url)));
  }
}

/**
 * Theme function for the upload table.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_upload_trait_form_upload_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_FILE.
  $bims_file = $element['bims_file']['#value'];

  // Creates a table for uploaded file.
  return BIMS_FILE::createInfoTable($element, $bims_file);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_upload_trait_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "uploads".
  $layout .= "<div style='float:left;width:45%;'>" . drupal_render($form['uploads']) . "</div>";

  // Adds 'upload'.
  $layout .= "<div style='float:left;width:54%;margin-left:5px;'>" . drupal_render($form['upload']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}