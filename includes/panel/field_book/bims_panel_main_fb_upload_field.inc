<?php
/**
 * @file
 */
/**
 * Field Book upload exported field file panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_field_form($form, &$form_state, $file_id = '') {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the file and inspection info.
  $bims_file      = NULL;
  $inspected      = NULL;
  $inspect_status = FALSE;
  if (array_key_exists('values', $form_state)) {

    // Updates file info.
    if (isset($form_state['values']['uploads']['upload_list'])) {
      $file_id = $form_state['values']['uploads']['upload_list'];
      $bims_file = BIMS_FILE::byID($file_id);
    }

    // Update inspected info.
    if (isset($form_state['values']['inspected'])) {
      $inspected = $form_state['values']['inspected'];
      $inspect_status = $inspected['status'];
    }
  }
  if (!$bims_file && $file_id) {
    $bims_file = BIMS_FILE::byID($file_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_fb_upload_field');
  if (!$bims_panel->init($form, TRUE, TRUE)) {
    return $form;
  }

  // Saves $inspected.
  $form['inspected'] = array(
    '#type' => 'value',
    '#value' => $inspected,
  );

  // Adds the uploads.
  $form['uploads'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Uploaded files',
    '#attributes'   => array('style' => 'height:540px;'),
  );
  $args = array(
    'type'          => 'ULF',
    'by_user_id'    => $bims_user->getUserID(),
    'program_id'    => $bims_user->getProgramID(),
    'date'          => TRUE,
  );
  $options = BIMS_FILE::getFiles($args, BIMS_OPTION);
  if (empty($options)) {
    $options = array('' => 'No uploaded field file found');
  }
  $form['uploads']['upload_list'] = array(
    '#type'           => 'select',
    '#options'        => $options,
    '#size'           => 12,
    '#attributes'     => array('style' => 'width:100%;'),
    '#default_value'  => $file_id,
    '#ajax'           => array(
      'callback' => "bims_panel_main_fb_upload_field_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-upload-field-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Adds action buttons.
  if ($bims_file) {
    $form['uploads']['table']['delete_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'delete_btn',
      '#value'      => 'Delete',
      '#attributes' => array(
        'style' => 'width:80px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_fb_upload_field_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-fb-upload-field-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['uploads']['table']['view_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'view_btn',
      '#value'      => 'View',
      '#attributes' => array('style' => 'width:80px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_fb_upload_field_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-fb-upload-field-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['uploads']['table']['bims_file'] = array(
    '#type' => 'value',
    '#value' => $bims_file,
  );
  $form['uploads']['table']['#theme'] = 'bims_panel_main_fb_upload_field_form_uploads_table';

  // Inspect file section.
  $height = ($inspect_status) ? '' : 'height:540px;';
  $form['inspect'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => $inspect_status,
    '#collapsible'  => TRUE,
    '#title'        => 'Inspect Exported Field Book Field File',
    '#attributes'   => array('style' => $height),
  );
  $req_field_table = _bims_panel_main_fb_upload_field_form_req_field_table($bims_program);
  $form['inspect']['desc'] = array(
    '#markup' => "<div style='margin:5px 0px 10px 0px;'>Please browse your field file and hit '<b>Upload</b>' button. Then click '<b>Inspect</b>' button to start inspecting your field file. BIMS checks if 4 required columns listed in the table below exist in your uploading file.</div>" .
      "<div style='margin:8px;'>$req_field_table</div>",
  );
  $form['inspect']['browse'] = array(
    '#type'               => 'managed_file',
    '#name'               => 'upload_file_btn',
    '#title'              => t('Field File'),
    '#description'        => t('Please choose your field file (.csv)'),
    '#upload_location'    => $bims_user->getUserDirURL(),
    '#upload_validators'  => array(
      'file_validate_extensions' => array('csv'),
      // Pass the maximum file size in bytes
      //'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
    ),
    '#attributes' => array('style' => 'width:60%;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_fb_upload_field_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-upload-field-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['inspect']['inspect_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'inspect_btn',
    '#value'      => 'Inspect',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_main_fb_upload_field_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-upload-field-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Shows the message for the Field Book columns.
  if ($inspected && !$inspect_status) {
    $form['inspect']['req_cols_fb'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Field Book required columns',
      '#attributes'   => array(),
    );
    $msg = 'Error';
    if (array_key_exists('error', $inspected)) {
      $msg = $inspected['error'];
    }
    else if (array_key_exists('req_cols', $inspected)) {
      $msg = $inspected['req_cols'];
    }
    $form['inspect']['req_cols_fb']['msg'] = array('#markup' => $msg);
  }
  $form['inspect']['#theme'] = 'bims_panel_main_fb_upload_field_form_inspect_table';

  // Upload file section.
  if ($inspect_status) {
    $form['upload'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => !$inspect_status,
      '#collapsible'  => TRUE,
      '#title'        => 'Upload an exported field file',
      '#attributes'   => array('style' => ''),
    );
    $form['upload']['desc'] = array(
      '#markup' => '',
    );

    // Adds checkboxes for the unknown columns in the file.
    $fb_unknown_cols = array();
    if ($inspected) {
      $fb_unknown_cols = $inspected['fb_unknown_cols'];
    }
    if (!empty($fb_unknown_cols)) {
      $form['upload']['other_cols'] = array(
        '#type'         => 'fieldset',
        '#collapsed'    => FALSE,
        '#collapsible'  => TRUE,
        '#title'        => 'Field Book other columns',
        '#attributes'   => array(),
      );
      $form['upload']['other_cols']['desc'] = array(
        '#markup' => 'There are unknown columns in your field file. Please check ' .
          'the columns you want to store as sample properties.<br />'
      );
      foreach ($fb_unknown_cols as $idx => $col_name) {
        $form['upload']['other_cols']['unknown_cols'][$idx] = array(
          '#type'           => 'checkbox',
          '#title'          => $col_name,
          '#default_value'  => FALSE,
        );
      }
    }

    // Adds location selection.
    $mview_location = new BIMS_MVIEW_LOCATION(array('node_id' => $bims_program->getProgramID()));
    $options = $mview_location->getLocations(BIMS_OPTION);
    $form['upload']['location_list'] = array(
      '#type'       => 'select',
      '#title'      => 'Locations',
      '#options'    => $options,
      '#attributes' => array('style' => 'width:230px;'),
    );

    // Adds trial selection.
    $no_trial = FALSE;
    $options = $bims_program->getTrials(BIMS_OPTION, 'phenotype');
    if (!empty($options)) {
    $form['upload']['trial_list'] = array(
      '#type'       => 'select',
      '#title'      => 'Trials',
      '#options'    => $options,
      '#attributes' => array('style' => 'width:230px;'),
    );
    }
    else {
      $no_trial = TRUE;
      $form['upload']['trial_list'] = array(
        '#markup' => '<div style="margin-top:20px;margin-bottom:20px;"><em><span style="color:red;"><b>*</b></span>&nbsp;This program does not have trial. Please add a trial.</em></div>',
      );
    }

    // Adds the description of the file.
    $form['upload']['file_desc'] = array(
      '#type'       => 'textarea',
      '#name'       => 'file_desc',
      '#title'      => 'Description of your file',
      '#rows'       => 2,
      '#attributes' => array('style' => 'width:100%;'),
    );
    $disabled = (($inspected && $inspected['status']) && !$no_trial) ? FALSE : TRUE;
    $form['upload']['upload_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'upload_btn',
      '#value'      => 'Submit',
      '#disabled'   => $disabled,
      '#attributes' => array('style' => 'width:180px;'),
      '#ajax'   => array(
        'callback' => "bims_panel_main_fb_upload_field_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-fb-upload-field-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['upload']['#theme'] = 'bims_panel_main_fb_upload_field_form_upload_table';
  }

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-fb-upload-field-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_fb_upload_field_form_submit';
  $form['#theme'] = 'bims_panel_main_fb_upload_field_form';
  return $form;
}

/**
 * Returns a table with 4 required columns.
 *
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_main_fb_upload_field_form_req_field_table(BIMS_PROGRAM $bims_program) {

  // Gets the BIMS columns.
  $bims_cols = $bims_program->getBIMSCols();
  $rows = array(
    array('Accession', 'accession', $bims_cols['accession']),
    array('Unique ID', 'unique_id', $bims_cols['unique_id']),
    array('Primary&nbsp;order', 'primary_order', $bims_cols['primary_order']),
    array('Secondary&nbsp;order', 'secondary_order', $bims_cols['secondary_order']),
  );
  $table_vars = array(
    'header'      => array(array('data' => 'Required columns', 'width' => 120), 'Default', 'Program specific'),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  return bims_theme_table($table_vars);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_field_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_field_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Inspect' was clicked.
  if ($trigger_elem == 'inspect_btn') {
    if (!$form_state['values']['inspect']['browse']) {
      form_set_error('add_user][add_user_auto', t('Please choose a file'));
    }
  }

  // If 'Upload' was clicked.
  else if ($trigger_elem == 'upload_btn') {
    if (!$form_state['values']['upload']['trial_list']) {
      form_set_error('upload][trial_list', t('Please choose a trial'));
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_upload_field_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'View' was clicked.
  if ($trigger_elem == 'view_btn') {

    // Gets job_id for MCL.
    $bims_file = $form_state['values']['uploads']['table']['bims_file'];
    if ($bims_file) {
      $mcl_job_id = $bims_file->getPropByKey('mcl_job_id');
      $url = 'bims/load_main_panel/bims_panel_add_on_job_upload/' . $bims_file->getFileID();
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_job_upload', 'Upload', $url)));
    }
  }

  // If 'Delete' was clicked.
  else if ($trigger_elem == 'delete_btn') {

    // Gets BIMS_FILE.
    $bims_file = $form_state['values']['uploads']['table']['bims_file'];
    if ($bims_file) {

      // Deletes BIMS_FILE.
      if (!$bims_file->delete()) {
        BIMS_MESSAGE::addMsg("Failed to delete BIMS file.");
      }

      // Updates the panels.
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_fb_upload_field')));
    }
  }

  // If 'Inspect' was clicked.
  else if ($trigger_elem == 'inspect_btn') {

    // Gets MCL_USER.
    $mcl_user = $bims_user->getMCL_USER();

    // Gets the values from the form_state.
    $drupal_file = file_load($form_state['values']['inspect']['browse']);
    if ($drupal_file) {

      // Moves the file to 'upload' directory.
      $filepath = BIMS_FILE::moveFile($bims_user, $drupal_file, 'upload');

      // Inspect the uploaded file.
      $inspected = BIMS_FILE::inspectFieldFile($bims_program, $filepath);

      // Saves file_managed.fid.
      $inspected['mcl_user'] = $mcl_user;
      $form_state['values']['inspected'] = $inspected;
    }
  }

  // If 'Upload' was clicked.
  else if ($trigger_elem == 'upload_btn') {

    // Gets the values from the form_state.
    $inspected    = $form_state['values']['inspected'];
    $description  = trim($form_state['values']['upload']['file_desc']);
    $location_id  = $form_state['values']['upload']['location_list'];
    $trial_id     = $form_state['values']['upload']['trial_list'];
    $sample_props = $form_state['values']['upload']['other_cols']['unknown_cols'];
    $bims_file_id = '';

    // Gets the sample properties.
    $sample_properties = array();
    foreach ((array)$sample_props as $idx => $value) {
      if ($value) {
        $label = $form['upload']['other_cols']['unknown_cols'][$idx]['#title'];
        $sample_properties []= $label;
      }
    }

    // Gets the dataset name.
    $bims_trial = BIMS_TRIAL::byID($trial_id);

    // Gets the site name.
    $site_name = '';
    if ($location_id) {
      $mview = new BIMS_MVIEW_LOCATION(array('node_id' => $bims_program->getNodeID()));
      $mview_location = $mview->getLocation($location_id);
      $site_name = $mview_location->name;
    }

    // Sets email option.
    $send_email = $bims_user->getEmailOption('upload');

    // Sets the upload parameters.
    $param = array(
      'program_id'      => $inspected['program_id'],
      'cv_arr'          => $inspected['cv_arr'],
      'template'        => $inspected['template'],
      'type'            => 'ULF',
      'site_name'       => $site_name,
      'trial_name'      => $bims_trial->getName(),
      'sample_props'    => $sample_properties,
      'send_email'      => $send_email,
    );
    $args = array(
      'mcl_user'    => $inspected['mcl_user'],
      'filepath'    => $inspected['filepath'],
      'description' => $description,
      'param'       => $param,
    );
    $bims_file = BIMS_FILE::upload($args);
    if (!$bims_file) {
      BIMS_MESSAGE::setMsg("Failed to upload file.");
    }
    else {
      $bims_file_id = '/' . $bims_file->getFileID();
    }

    // Updates the panels.
    $url = 'bims_panel_main_fb_upload_field' . $bims_file_id;
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($url)));
  }
}

/**
 * Theme function for the uploaded_file table.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_upload_field_form_uploads_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_FILE.
  $bims_file = $element['bims_file']['#value'];

  // Creates a table for uploaded file.
  return BIMS_FILE::createInfoTable($element, $bims_file);
}

/**
 * Theme function for the upload table.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_upload_field_form_inspect_table($variables) {
  $element = $variables['element'];

  // Adds the form elements.
  $layout = "<div>" . drupal_render($element['desc']) . "</div>";
  $layout .= "<div style='float:left;width:80%;'>" . drupal_render($element['browse']) . "</div>";
  $layout .= "<div style='margin-bottom:10px;'>" . drupal_render($element['inspect_btn']). "</div>";
  if (array_key_exists('req_cols_fb', $element)) {
    $layout .= "<div style=''>" . drupal_render($element['req_cols_fb']). "</div>";
  }
  return $layout;
}

/**
 * Theme function for the upload table.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_upload_field_form_upload_table($variables) {
  $element = $variables['element'];
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_upload_field_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "uploads".
  $layout .= "<div style='float:left;width:45%;'>" . drupal_render($form['uploads']) . "</div>";

  // Adds 'upload'.
  $layout .= "<div style='float:left;width:54%;margin-left:5px;'>" . drupal_render($form['inspect']) . "</div>";
  $layout .= "<div style='float:left;width:54%;margin-left:5px;'>" . drupal_render($form['upload']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}