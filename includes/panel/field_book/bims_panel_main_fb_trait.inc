<?php
/**
 * @file
 */
/**
 * Field Book trait file panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_trait_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_fb_trait');
  $bims_panel->init($form);

  $form['traits'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Generate a trait file',
  );
  $form['traits']['markup'] = array(
    '#markup' => 'Please choose traits from the list below before generating a trait file for Field Book<br /><br />',
  );
  $form['traits']['generate_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'generate_btn',
    '#value'      => 'Generate',
    '#attributes' => array('style' => 'width:140px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_fb_trait_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-trait-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['traits']['save_trait_cb'] = array(
    '#type'           => 'checkbox',
    '#default_value'  => FALSE,
    '#title'          => 'Please check if you want to save the trait file in you account.',
  );
  $fieldset_id = 'bims_fbt_trait';
  $form['traits']['trait_list_fs'] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Traits',
  );
  $form['traits']['trait_list_fs']['table'] = array(
    '#type'   => 'value',
    '#value'  => NULL,
  );
  $form['traits']['trait_list_fs']['table']['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );
  // Gets traits.
  $num_traits = _bims_panel_main_fb_get_traits($form, $bims_user);
  $disabled = ($num_traits) ? FALSE : TRUE;
  $form['traits']['generate_btn']['#disabled'] = $disabled;
  $form['traits']['trait_list_fs']['table']['#theme'] = 'bims_panel_main_fb_trait_form_trait_table';

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-fb-trait-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_fb_trait_form_submit';
  $form['#theme'] = 'bims_panel_main_fb_trait_form';
  return $form;
}

function _bims_panel_main_fb_get_traits(&$form, BIMS_USER $bims_user) {

  // Gets the descriptors for this program.
  $bims_program = $bims_user->getProgram();
  $descriptors = $bims_program->getDescriptors(NULL, BIMS_CLASS);
  $trait_form_elems = array();
  $traits = array();
  foreach ($descriptors as $descriptor) {

    // Stores the descriptor information.
    $prop_arr     = $descriptor->getPropArr();
    $categories   = array_key_exists('categories', $prop_arr)   ? $prop_arr['categories'] : '';
    $defaultValue = array_key_exists('defaultValue', $prop_arr) ? $prop_arr['defaultValue'] : '';
    $minimum      = array_key_exists('minimum', $prop_arr)      ? $prop_arr['minimum'] : '';
    $maximum      = array_key_exists('maximum', $prop_arr)      ? $prop_arr['maximum'] : '';
    $traits[$descriptor->getCvtermID()] = array(
      'name'          => $descriptor->getName(),
      'format'        => $descriptor->getFormat(),
      'categories'    => $categories,
      'defaultValue'  => $defaultValue,
      'minimum'       => $minimum,
      'maximum'       => $maximum,
      'details'       => $descriptor->getDefinition(),
    );

    // Creates form element for the descriptor.
    $trait_form_elems[$descriptor->getCvtermID()]= array(
      '#type'           => 'checkbox',
      '#default_value'  => FALSE,
    );
  }
  $form['traits']['trait_list_fs']['table']['trait_form_elems'] = $trait_form_elems;
  $form['traits']['trait_list_fs']['table']['traits'] = array(
    '#type' => 'value',
    '#value' => $traits,
  );
  return sizeof($traits);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_trait_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_trait_form_validate($form, &$form_state) {

  // Checks if user choose at least one trait.
  $count = 0;
  $form_elems = $form_state['values']['traits']['trait_list_fs']['table']['trait_form_elems'];
  foreach ($form_elems as $cvterm_id => $value) {
    if ($value) {
      $count++;
    }
  }
  if ($count == 0) {
    form_set_error('traits][trait_list_fs][table', "Please choose at least one trait.");
  }
}

/**
 * Generates a trait file.
 *
 * @param array $form_state
 * @param array $form_state
 * @param array $form_state
 *
 * @return boolean
 */
function _bims_panel_main_fb_trait_form_generate_file($form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user = getBIMS_USER();
  $user_id   = $bims_user->getUserID();

  // Gets the form values.
  $save_flag  = $form_state['values']['traits']['save_trait_cb'];
  $traits     = $form_state['values']['traits']['trait_list_fs']['table']['traits'];
  $form_elems = $form_state['values']['traits']['trait_list_fs']['table']['trait_form_elems'];

  // Opens the file.
  $filename = "trait-$user_id-" . date("Gis-Y-m-d") . '.trt';
  $tmp_file = bims_get_config_setting('bims_tmp_dir') . "/$filename";
  if (!($fdw = fopen($tmp_file, 'w'))) {
    return '';
  }

  // Adds the headers.
  $headers = array("trait", "format", "defaultValue", "minimum", "maximum",
                   "details", "categories", "isVisible", "realPosition");
  fputcsv($fdw, $headers);

  // Gets the selected traits.
  $count = 0;
  foreach ($form_elems as $cvterm_id => $value) {
    if ($value) {
      $count++;
      $row = array(
        $traits[$cvterm_id]['name'],
        $traits[$cvterm_id]['format'],
        $traits[$cvterm_id]['defaultValue'],
        $traits[$cvterm_id]['minimum'],
        $traits[$cvterm_id]['maximum'],
        $traits[$cvterm_id]['details'],
        $traits[$cvterm_id]['categories'],
        'TRUE',
        $count,
      );
      fputcsv($fdw, $row);
    }
  }
  fclose($fdw);

  // Creates and saves the trait file in the user space.
  $filepath = $bims_user->getPath('trait', TRUE) . "/$filename";
  $file = file_save_data(file_get_contents($tmp_file), $filepath);

  // Saves the file in the trait directory.
  if ($save_flag) {
    $prop = array(
      'file_id' => $file->fid,
      'status'  => array('label' => 'completed', 'numeric' => 100),
    );
    $details = array(
      'filename'    => $filename,
      'filepath'    => $filepath,
      'type'        => 'FBT',
      'filesize'    => filesize($filepath),
      'user_id'     => $bims_user->getUserID(),
      'program_id'  => $bims_user->getProgramID(),
      'description' => 'Trait file for Field Book',
      'submit_date' => date("Y-m-d G:i:s"),
      'prop'        => json_encode($prop),
    );
    $bims_file = new BIMS_FILE($details);
    if (!$bims_file->insert()) {
      BIMS_MESSAGE::setMsg("Failed to save a trait file.");
    }
  }

  // Redirects to the download link.
  $url = "bims/download_file/" . $file->fid;
  bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
  return TRUE;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_trait_form_submit($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Generate' was clicked.
  if ($trigger_elem == 'generate_btn') {
    if (!_bims_panel_main_fb_trait_form_generate_file($form_state, $bims_user)) {
      drupal_set_message("Error : Failed to generate a trait file.", 'error');
    }
  }
}

/**
 * Theme function for the trait table.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_trait_form_trait_table($variables) {
  $element = $variables['element'];

  // Adds a checkbox for 'Check All'.
  $fieldset_id = $element['fieldset_id']['#value'];
  $check_all = "<input style='' type='checkbox' title='Select All / None' onchange='bims.bims_select_all(this, \"$fieldset_id\");' />";


  // Gets the trait info.
  $trait_form_elems = $element['trait_form_elems'];
  $traits = $element['traits']['#value'];

  // Creates the trait table.
  $rows = array();
  foreach ($trait_form_elems as $cvterm_id => $form_elem) {
    if (preg_match("/^\d+$/", $cvterm_id)) {
      $rows []= array(
        drupal_render($form_elem),
        $traits[$cvterm_id]['name'],
        $traits[$cvterm_id]['format'],
        $traits[$cvterm_id]['details'],
      );
    }
  }
  $headers = array(
    array('data' =>  $check_all, 'width' => '1'),
    array('data' => 'Name', 'width' => '100'),
    array('data' => 'Format', 'width' => '100'),
    'Details'
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'empty'       => 'No trait is associated with the current program.',
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}