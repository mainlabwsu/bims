<?php
/**
 * @file
 */
/**
 * Field Book field file panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_field_form($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the default tab.
  $default_tab = 'fb_new_trial';
  if (array_key_exists('storage', $form_state)) {
    $default_tab = $form_state['storage']['default_tab'];
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_fb_field');
  if (!$bims_panel->init($form, TRUE)) {
    return $form;
  }

  $form['field'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Generate a field file',
  );

  // Adds the description.
  $markup = '<p>Generate an input file for Field Book.<br /><br />';
  $form['field']['markup'] = array(
    '#markup' => $markup,
  );

  // Setup the vertical tabs.
  $form['field']['field_tabs'] = array(
    '#type' => 'vertical_tabs',
    '#default_tab' => $default_tab,
  );

  // Adds form elements for a new trial.
  _bims_panel_main_fb_add_form_new_trial($form['field']['field_tabs']['trial_new'], $bims_user);

  // Adds form elements for the existing trial.
  _bims_panel_main_fb_add_form_existing_trial($form['field']['field_tabs']['trial_existing'], $bims_user);

  // Adds form elements for the cross.
  _bims_panel_main_fb_add_form_cross($form['field']['field_tabs']['cross'], $bims_user);

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-fb-field-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_fb_field_form_submit';
  $form['#theme'] = 'bims_panel_main_program_form';
  return $form;
}

/**
 * Adds form elements for a new trial.
 *
 * @param array $form
 * @param BIMS_USER $bims_user
 */
function _bims_panel_main_fb_add_form_new_trial(&$form, BIMS_USER $bims_user) {

  // Gets BIMS_PROGRAM.
  $bims_program = $bims_user->getProgram();
  $accession = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // New trial tab.
  $form = array(
    '#id'           => 'fb_new_trial',
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#group'        => 'field_tabs',
    '#title'        => 'New Trial',
  );

  // Adds 'generate' button.
  $form['generate_btn_new'] = array(
    '#type'       => 'submit',
    '#name'       => 'generate_btn_new',
    '#value'      => 'Generate',
    '#attributes' => array('style' => 'width:140px;margin-bottom:10px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_fb_field_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-field-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['save_file_cb'] = array(
    '#type'           => 'checkbox',
    '#default_value'  => FALSE,
    '#title'          => 'Please check if you want to save the field file in you account.',
  );

  // Adds the accession columns.
  $form['accession_col'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => "$accession column by a list",
  );

  // Accession list.
  $args = array(
    'type'        => 'sample',
    'user_id'     => $bims_user->getUserID(),
    'program_id'  => $bims_program->getProgramID(),
    'flag'        => BIMS_OPTION,
  );
  $options = BIMS_LIST::getLists($args);
  if (empty($options)) {
    $create_disabled = TRUE;
    $options = array('' => 'No accession list found');
  }
  $form['accession_col']['list_cols']['list_list'] = array(
    '#type'       => 'select',
    '#options'    => $options,
    '#attributes' => array('style' => 'width:200px;'),
  );
  $form['accession_col']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['accession_col']['#theme'] = 'bims_panel_main_fb_field_form_accession_column';

  // Custom column selection.
  $form['custom_cols'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Field Book custom columns',
  );

  $config_link = '<a href="javascript:void(0);" onclick="bims.add_main_panel(\'bims_panel_add_on_config\', \'Configuration\', \'bims/load_main_panel/bims_panel_add_on_config/field_book\'); return (false);"><b>click here</b></a>';
  $markup = "Please choose custom fields from '$accession properties' section. The chosen fields become columns in a new input file. Please $config_link to set default custom fields.";
  $form['custom_cols']['desc'] = array(
    '#markup' => "<div>$markup</div><br />",
  );

  // Adds columns for accession properties
  _bims_panel_main_fb_add_form_add_list('new', $form['custom_cols'], $bims_user, 'fb_accession', "$accession properties");
}

/**
 * Adds form elements for the existing trial.
 *
 * @param array $form
 * @param BIMS_USER $bims_user
 */
function _bims_panel_main_fb_add_form_existing_trial(&$form,BIMS_USER $bims_user) {

  // Gets BIMS_PROGRAM.
  $bims_program = $bims_user->getProgram();
  $accession = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Existing trial tab.
  $form = array(
    '#id'           => 'fb_existing_trial',
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#group'        => 'field_tabs',
    '#title'        => 'Existing Trial',
  );

  // Adds 'generate' button.
  $form['generate_btn_existing'] = array(
    '#type'       => 'submit',
    '#name'       => 'generate_btn_existing',
    '#value'      => 'Generate',
    '#attributes' => array('style' => 'width:140px;margin-bottom:10px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_fb_field_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-field-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['save_file_cb'] = array(
    '#type'           => 'checkbox',
    '#default_value'  => FALSE,
    '#title'          => 'Please check if you want to save the field file in you account.',
  );

  // Adds BIMS columns.
  $form['accession_col'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => "$accession column by a trial",
  );
  $form['accession_col']['#theme'] = 'bims_panel_main_fb_field_form_accession_column';
  $form['accession_col']['trial_cols']['trial_list'] = array(
    '#type'       => 'select',
    '#options'    => $bims_program->getTrials(BIMS_OPTION, 'phenotype'),
    '#attributes' => array('style' => 'width:200px;'),
  );
  $form['accession_col']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );

  // Custom column selection.
  $form['custom_cols'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Field Book custom columns',
  );

  $accession = $bims_program->getBIMSLabel('accession');
  $config_link = '<a href="javascript:void(0);" onclick="bims.add_main_panel(\'bims_panel_add_on_config\', \'Configuration\', \'bims/load_main_panel/bims_panel_add_on_config/field_book\'); return (false);"><b>click here</b></a>';
  $markup = "Please choose custom fields from the three sections below (Trait, $accession and sample properties).  The chosen fields become columns in a new input file. Please $config_link to set default custom fields.";
  $form['custom_cols']['desc'] = array(
    '#markup' => "<div>$markup</div><br />",
  );

  // Adds columns for traits / accession properties / sample properties.
  _bims_panel_main_fb_add_form_add_list('exist', $form['custom_cols'], $bims_user, 'fb_default', 'Required columns', TRUE, FALSE);
  _bims_panel_main_fb_add_form_add_list('exist', $form['custom_cols'], $bims_user, 'fb_trait', 'Traits');
  _bims_panel_main_fb_add_form_add_list('exist', $form['custom_cols'], $bims_user, 'fb_accession', ucfirst($accession) . ' properties');
  _bims_panel_main_fb_add_form_add_list('exist', $form['custom_cols'], $bims_user, 'fb_sample', 'Sample properties');
}

/**
 * Adds form elements for cross.
 *
 * @param array $form
 * @param BIMS_USER $bims_user
 */
function _bims_panel_main_fb_add_form_cross(&$form, BIMS_USER $bims_user) {

  // Gets BIMS_PROGRAM.
  $bims_program = $bims_user->getProgram();
  $accession = $bims_program->getBIMSLabel('accession');

  // Cross tab.
  $form = array(
    '#id'           => 'fb_cross',
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#group'        => 'field_tabs',
    '#title'        => 'Cross',
  );
  $form['generate_btn_cross'] = array(
    '#type'       => 'submit',
    '#name'       => 'generate_btn_cross',
    '#value'      => 'Generate',
    '#attributes' => array('style' => 'width:140px;margin-bottom:10px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_fb_field_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-field-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['save_file_cb'] = array(
    '#type'           => 'checkbox',
    '#default_value'  => FALSE,
    '#title'          => 'Please check if you want to save the field file in you account.',
  );

  $form['cross_file'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Upload a cross file',
  );

  $form['cross_file']['upload']['browse'] = array(
    '#type'               => 'managed_file',
    '#title'              => t('Cross File'),
    '#description'        => t('Please upload your cross information file (.csv)'),
    '#upload_location'    => $bims_user->getUserDirURL(),
    '#upload_validators'  => array(
      'file_validate_extensions' => array('csv'),
      // Pass the maximum file size in bytes
      //'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),
    ),
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_main_fb_field_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-fb-field-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['cross_file']['prefix'] = array(
    '#type'         => 'textfield',
    '#title'        => 'Prefix',
    '#description'  => "Please provide the prefix to Unique Identifier.",
    '#attributes'   => array('style' => 'width:140px;margin-bottom:10px;'),
  );
  $form['cross_file']['add_progeny'] = array(
    '#title'          => 'Upload to database',
    '#type'           => 'checkbox',
    '#default_value'  => 0,
    '#description'    => t("Please check if you'd like upload progenies to databse."),
    '#attributes'     => array(),
  );
  hide($form['cross_file']['add_progeny']);
}
/**
 * Adds the list (text) of the provided type.
 *
 * @param string $prefix
 * @param array $form
 * @param BIMS_USER $bims_user
 * @param string $col_type
 * @param string $label
 * @param boolean $checked
 * @param boolean $collapsed
 */
function _bims_panel_main_fb_add_form_add_list($prefix, &$form, BIMS_USER $bims_user, $col_type, $label, $checked = FALSE, $collapsed = TRUE) {

  // Sets the ID for the fieldset.
  $fieldset_id = 'bims_mfb_add_list_' . $prefix . $col_type;

  // Adds a fieldset for provied type.
  $form[$col_type] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => $collapsed,
    '#collapsible'  => TRUE,
    '#title'        => $label,
  );
  $form[$col_type]['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Gets the list of items of the provided column type.
  $items = _bims_panel_main_fb_add_form_get_items($bims_user, $col_type);
  if (empty($items)) {
    $config_link = '<a href="javascript:void(0);" onclick="bims.add_main_panel(\'bims_panel_add_on_config\', \'Configuration\', \'bims/load_main_panel/bims_panel_add_on_config/field_book\'); return (false);"><b>click here</b></a>';
    $form[$col_type]['no_data'] = array(
      '#markup' => '<div>No ' . strtolower($label) . " to choose. Please add $label.",
    );
    return;
  }
  foreach ($items as $id => $info) {
    $name     = $info['name'];
    $default  = $info['default'];
    $form[$col_type]['ids'][$id] = array(
      '#type'           => 'checkbox',
      '#title'          => $name,
      '#default_value'  => $checked,
    );
    if ($checked) {
      $form[$col_type]['ids'][$id]['#default_value'] = $checked;
    }
    else {
      $form[$col_type]['ids'][$id]['#default_value'] = $default;
    }
  }
  $form[$col_type]['#theme'] = 'bims_panel_main_fb_field_form_custom_column';
}

/**
 * Returns the list of items of the provided item type.
 *
 * @param BIMS_USER $bims_user
 * @param string $col_type
 *
 * @return array
 */
function _bims_panel_main_fb_add_form_get_items(BIMS_USER $bims_user, $col_type) {

  // Gets BIMS_PROGRAM.
  $bims_program = $bims_user->getProgram();

  // Gets the columns for the provided type.
  $items = array();
  if ($col_type == 'fb_default') {
    $items = $bims_program->getProperties('field_book', 'default', BIMS_FIELD);
  }
  else if ($col_type == 'fb_trait') {
    $items = $bims_program->getDescriptors(NULL, BIMS_FIELD);
  }
  else if ($col_type == 'fb_accession') {
    $items = $bims_program->getProperties('accession', 'default_custom', BIMS_FIELD);
  }
  else if ($col_type == 'fb_sample') {
    $items = $bims_program->getProperties('sample', 'default_custom', BIMS_FIELD);
  }

  // Gets the default values from the config.
  $config = $bims_program->getPropByKey('config');
  $def_arr = array();
  if (!empty($config) && array_key_exists($col_type, $config)) {
    $def_arr = $config[$col_type];
  }

  // Adds the default values to $items.
  $ret_items = array();
  foreach ((array)$items as $field => $name) {
    if (array_key_exists($field, $def_arr)) {
      $ret_items[$field] = array('name' => $name, 'default' => TRUE);
    }
    else {
      $ret_items[$field] = array('name' => $name, 'default' => FALSE);
    }
  }
  return $ret_items;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_field_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_field_form_validate($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Generate" (new) was clicked.
  if ($trigger_elem == 'generate_btn_new') {

    // Checks the accession list.
    $accession = $bims_program->getBIMSLabel('accession');
    $list_id = $form_state['values']['field']['field_tabs']['trial_new']['accession_col']['list_cols']['list_list'];
    if (!$list_id) {
      form_set_error('field][field_tabs][trial_new][accession_col][list_cols][list_list', "Please choose an $accession");
      return;
    }
  }

  // If "Generate" (existing) was clicked.
  else if ($trigger_elem == 'generate_btn_existing') {

    // Checks the trial.
    $trial_list = $form_state['values']['field']['field_tabs']['trial_existing']['accession_col']['trial_cols']['trial_list'];
    if (!$trial_list) {
      form_set_error('field][field_tabs][trial_existing][accession_col][trial_cols][trial_list', 'Please choose trial');
      return;
    }
  }

  // If "Generate" (cross) was clicked.
  if ($trigger_elem == 'generate_btn_cross') {
    $fid = $form_state['values']['field']['field_tabs']['cross']['cross_file']['upload']['browse'];
    if (!$fid) {
      form_set_error('bims', 'No file selected');
      return;
    }
  }

  // If "Generate" (existing) was clicked.
  else if ($trigger_elem == 'generate_btn_cross') {

    // Gets the uploaded file.
    $drupal_file = file_load($form_state['values']['field']['field_tabs']['cross']['upload']['browse']);
    if ($drupal_file) {
      $err_msg = BIMS_FIELD_BOOK::checkFieldFileByCross($bims_user, $drupal_file);
      if ($err_msg) {
        form_set_error('bims', $err_msg);
        return;
      }
    }
    else {
      form_set_error('bims', 'File not found');
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_fb_field_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $err_msg = '';

  // Gets BIMS_USER.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Adds FB required columns to $prop_cols.
  $fb_req_cols = $bims_program->getBIMSCols();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Generate" (by cross) was clicked.
  if ($trigger_elem == 'generate_btn_cross') {

    // Gets the uploaded file.
    $drupal_file = file_load($form_state['values']['field']['field_tabs']['cross']['cross_file']['upload']['browse']);
    if ($drupal_file) {

      // Sets the properties.
      $param = array(
        'drupal_file'       => $drupal_file,
        'cross_project_id'  => $bims_program->getCrossProjectID(),
        'prefix'            => trim($form_state['values']['field']['field_tabs']['cross']['cross_file']['prefix']),
        'add_progeny'       => $form_state['values']['field']['field_tabs']['cross']['cross_file']['add_progeny'],
        'fb_req_cols'       => $fb_req_cols,
        'save_file'         => $form_state['values']['field']['field_tabs']['cross']['save_file_cb'],
      );

      // Generates field file by cross.
      $err_msg = BIMS_FIELD_BOOK::generateFieldFileByCross($bims_user, $param);
      if ($err_msg) {
        BIMS_MESSAGE::setMsg("Failed to generate an input file from cross file.<br />$err_msg");
      }

      // Delete the file after processed.
      file_delete($drupal_file);
    }
    else {
      BIMS_MESSAGE::setMsg("Error : Failed to read the uploaded file.");
    }
  }

  // If "Generate" (by trial) was clicked.
  else if (preg_match("/^generate_btn/", $trigger_elem)) {

    // If "Generate" (new trial) was clicked.
    if ($trigger_elem == 'generate_btn_new') {

      // Adds the accession properties.
      $prop_cols = array();
      $custom_cols = $form_state['values']['field']['field_tabs']['trial_new']['custom_cols']['fb_accession']['ids'];
      foreach ((array)$custom_cols as $field => $value) {
        if ($value) {
          $prop_cols[$field] = $form['field']['field_tabs']['trial_new']['custom_cols']['fb_accession']['ids'][$field]['#title'];
        }
      }

      // Sets the properties.
      $param = array(
        'program_id'  => $program_id,
        'list_id'     => $form_state['values']['field']['field_tabs']['trial_new']['accession_col']['list_cols']['list_list'],
        'fb_req_cols' => $fb_req_cols,
        'prop_cols'   => $prop_cols,
        'save_file'   => $form_state['values']['field']['field_tabs']['trial_new']['save_file_cb'],
      );

      // Generates a field file.
      $err_msg = BIMS_FIELD_BOOK::generateFieldFileByListNew($bims_user, $param);
    }

    // If "Generate" (existing trial) was clicked.
    else {

      // Adds property colums.
      $prop_cols = $fb_req_cols;

      // Adds the Field Book required columns.
      $custom_cols = $form_state['values']['field']['field_tabs']['trial_existing']['custom_cols']['fb_default']['ids'];
      foreach ((array)$custom_cols as $field => $value) {
        if (!$value) {
          unset($prop_cols[$field]);
        }
      }

      // Adds the accession properties.
      $custom_cols = $form_state['values']['field']['field_tabs']['trial_existing']['custom_cols']['fb_accession']['ids'];
      foreach ((array)$custom_cols as $field => $value) {
        if ($value) {
          $prop_cols[$field] = $form['field']['field_tabs']['trial_existing']['custom_cols']['fb_accession']['ids'][$field]['#title'];
        }
      }

      // Adds the samppe properties.
      $custom_cols = $form_state['values']['field']['field_tabs']['trial_existing']['custom_cols']['fb_sample']['ids'];
      foreach ((array)$custom_cols as $field => $value) {
        if ($value) {
          $prop_cols[$field] = $form['field']['field_tabs']['trial_existing']['custom_cols']['fb_sample']['ids'][$field]['#title'];
        }
      }

      // Adds the traits.
      $custom_cols = $form_state['values']['field']['field_tabs']['trial_existing']['custom_cols']['fb_trait']['ids'];
      foreach ((array)$custom_cols as $field => $value) {
        if ($value) {
          $prop_cols[$field] = $form['field']['field_tabs']['trial_existing']['custom_cols']['fb_trait']['ids'][$field]['#title'];
        }
      }

      // Sets the properties.
      $param = array(
        'program_id'  => $program_id,
        'trial_id'  => $form_state['values']['field']['field_tabs']['trial_existing']['accession_col']['trial_cols']['trial_list'],
        'prop_cols' => $prop_cols,
        'save_file' => $form_state['values']['field']['field_tabs']['trial_existing']['save_file_cb'],
      );

      // Generates a field file.
      $err_msg = BIMS_FIELD_BOOK::generateFieldFileByListExisting($bims_user, $param);
    }
  }

  // Shows the error message if not empty.
  if ($err_msg) {
    BIMS_MESSAGE::setMsg("Failed to generate an input field file to Field Book<br />$err_msg");
  }
}

/**
 * Theme function for the accession column.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_field_form_accession_column($variables) {
  $element = $variables['element'];

  // Gets BIMS cols.
  $bims_program = $element['bims_program']['#value'];
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Gets the type of the input field.
  $type = (array_key_exists('list_cols', $element)) ? 'new' : 'existing';

  // Adds rows.
  $headers = array();
  $rows = array();
  $desc_section = '';
  if ($type == 'new') {
    $desc_section = 'You have chosen to create an input file for a new trial.';

    // Sets the headers.
    $headers = array(
      array('data' => "$accession list" , 'width' => 160),
      array('data' => 'Description'),
    );

    // Adds the accession / corss lists.
    $desc = 'Please choose the list of accession or cross. Currently the accession list is available. You can create an accession list from search results.';
    $rows []= array(
      drupal_render($element['list_cols']['list_list']),
      'Please choose a list.',
    );
  }
  else {
    $desc_section = 'When you use multiple devices for phenotyping and you want to merge the data, you can load the exported file to BIMS and then recreate input file with a indication (a trait) which have been already phenotyped. The exported file in database format, however, only contains the unique_id that have been phenotyped, so please make sure you upload the original field book input file for the trial first before you load the exported database file.  Please choose a trial in the dropdown list below.';

    // Sets the headers.
    $headers = array(
      array('data' => 'Trial', 'width' => 160),
      array('data' => 'Description'),
    );

    // Adds the existing trials.
    $desc = 'Please choose a trial from the list.';
    $rows []= array(
      drupal_render($element['trial_cols']['trial_list']),
      'Please chose a trial.',
    );
  }

  // Creates the table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:98%'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  return "<p style='width:98%'>$desc_section</p>" . bims_theme_table($table_vars);
}

/**
 * Theme function for the custom columns.
 *
 * @param $variables
 */
function theme_bims_panel_main_fb_field_form_custom_column($variables) {
  $element = $variables['element'];

  // No data.
  if (array_key_exists('no_data', $element)) {
    return drupal_render($element['no_data']);
  }

  // Adds accession properties.
  return _theme_bims_panel_main_fb_field_form_create_table($element);
}

/**
 * Helper function for theming the columns in a table.
 *
 * @param array $element
 *
 * @return string
 */
function _theme_bims_panel_main_fb_field_form_create_table($element) {

  // Adds a checkbox for 'Check All'.
  $fieldset_id = $element['fieldset_id']['#value'];
  $check_all = "<div style=''><input style='' type='checkbox' onchange='bims.bims_select_all(this, \"$fieldset_id\");'> Check All / None</div>";

  // Populates the table.
  $row        = array();
  $rows       = array();
  $count      = 0;
  $num_col    = 4;
  $max_width  = 400;
  foreach ((array)$element['ids'] as $id => $checkbox) {
    if (!preg_match("/^#/", $id)) {
      $count++;
      $td  = drupal_render($checkbox);
      $row []= array('data' => $td, 'width' => $max_width);
      if (($count % $num_col) == 0) {
        $rows []= $row;
        $row = array();
      }
    }
  }
  if (!empty($row)) {
    $size = sizeof($row);
    if ($size < $num_col) {
      for ($i = $size; $i < $num_col; $i++) {
        $row []= array('data' => '&nbsp;', 'width' => $max_width);
      }
    }
    $rows []= $row;
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'empty'       => '',
    'attributes'  => array('style' => 'width:100%;'),
  );
  return $check_all . theme('table', $table_vars);
}
