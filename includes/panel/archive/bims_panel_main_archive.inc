<?php
/**
 * @file
 */
/**
 * Manage archive form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_archive_form($form, &$form_state, $archive_id = NULL) {

  // Local variables for form properties.
  $min_height = 430;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $accession    = $bims_user->getBIMSLabel('accession', BIMS_UCFIRST);

  // Gets BIMS_FILE.
  $bims_archive  = NULL;
  if (isset($form_state['values']['archive_list']['selection'])) {
    $archive_id = $form_state['values']['archive_list']['selection'];
  }
  if ($archive_id) {
    $bims_archive = BIMS_ARCHIVE::byID($archive_id);
  }

  // Create a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_archive');
  $bims_panel->init($form);

  // Lists the all archives.
  $form['archive_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Archives'),
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Archive selection.
  $options = BIMS_ARCHIVE::getArchives($bims_user->getUserID(), $bims_program->getProgramID(), BIMS_OPTION);
  if (empty($options)) {
    $form['archive_list']['no_archive'] = array(
      '#markup' => 'No archive is associated with the current program.<br />',
    );
  }
  else {
    $form['archive_list']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#size'           => 8,
      '#default_value'  => $archive_id,
      '#description'    => t("Click an archive to see the details"),
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_archive_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-archive-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds 'Advanced options'.
  $form['archive_list']['archive_options'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => t('Advanced Options'),
  );
  $data_types = array(
    'dataset'         => 'Trial',
    'accession'       => $accession,
    'cross'           => 'Cross',
    'progeny'         => 'Progeny',
    'descriptor'      => 'Trait',
    'image'           => 'Image',
    'marker'          => 'Marker',
    'site'            => 'Site',
    'phenotype'       => 'Phenotype',
    'genotype_snp'    => 'SNP',
    'genotype'        => 'SSR',
    'haplotype_block' => 'Haplotype Block',
    'haplotype'       => 'Haplotype',
    'haplotype_snp'   => 'Haplotype SNP',
    'property'        => 'Property',
  );
  foreach ($data_types as $type => $label) {

    // Adds a checkbox if the data exists.
    if ($bims_program->hasData('BIMS_CHADO', $type)) {
      $form['archive_list']['archive_options']['cbs'][$type] = array(
        '#title'          => $label,
        '#type'           => 'checkbox',
        '#default_value'  => TRUE,
      );
      if ($type == 'phenotype') {
        $form['archive_list']['archive_options']['rbs']['phenotype'] = array(
          '#type'           => 'radios',
          '#options'        => array('_long_form' => 'Long Form', '_wide_form' => 'Wide Form'),
          //'#options'        => array('long_form' => 'Long Form', 'wide_form' => 'Wide Form', '_fb' => 'FieldBook'),
          '#default_value'  => '_wide_form',
          '#attributes'     => array('style' => 'width:10px;'),
        );
      }
      else if ($type == 'genotype_snp') {
        $form['archive_list']['archive_options']['rbs']['genotype_snp'] = array(
          '#type'           => 'radios',
          '#options'        => array('_long_form' => 'Long Form', '_wide_form_mc' => 'Wide Form - Marker columns', '_wide_form_ac' => "Wide Form - $accession columns"),
          '#default_value'  => '_long_form',
          '#attributes'     => array('style' => 'width:10px;'),
        );
      }
    }
  }
  $form['archive_list']['archive_options']['data_types'] = array(
    '#type'   => 'value',
    '#value'  => $data_types
  );
  $form['archive_list']['archive_options']['#theme']  = 'bims_panel_main_archive_form_archive_options_table';
  $form['archive_list']['description'] = array(
    '#type'       => 'textarea',
    '#title'      => t('Please type the description'),
    '#rows'       => 2,
    '#attributes' => array('style' => 'width:400px;'),
  );

  // Adds 'Archive' button.
  $form['archive_list']['archive_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'archive_btn',
    '#value'      => 'Archive Data',
    '#attributes' => array('style' => 'width:180px;margin:15px 0px 10px 0px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_main_archive_form_ajax_callback",
      'wrapper'  => 'bims-panel-main-archive-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Archive details.
  $form['archive_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => t('Archive Details'),
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['archive_details']['table']['bims_archive'] = array(
    '#type'   => 'value',
    '#value'  => $bims_archive,
  );

  // Adds action buttons.
  if ($bims_archive && $bims_user->isOwner()) {
    $form['archive_details']['table']['edit_btn'] = array(
      '#name'       => 'edit_btn',
      '#type'       => 'button',
      '#value'      => 'Edit',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_archive_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-archive-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['archive_details']['table']['delete_btn'] = array(
      '#name'       => 'delete_btn',
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => 'bims_panel_main_archive_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-archive-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Adds the refresh button if status is processing.
    if ($bims_archive->getStatusLabel() == 'processing') {
      $form['archive_details']['table']['refresh_btn'] = array(
        '#name'       => 'refresh_btn',
        '#type'       => 'button',
        '#value'      => 'Refresh',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => 'bims_panel_main_archive_form_ajax_callback',
          'wrapper'  => 'bims-panel-main-archive-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
  }
  $form['archive_details']['table']['#theme'] = 'bims_panel_main_archive_form_archive_table';

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-main-archive-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_archive_form_submit';
  $form['#theme'] = 'bims_panel_main_archive_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_archive_form_ajax_callback($form, $form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets archive ID.
  $archive_id = $form_state['values']['archive_list']['selection'];
  if ($archive_id) {

    // If "Edit" was clicked.
    if ($trigger_elem == 'edit_btn') {
      $url = "bims/load_main_panel/bims_panel_add_on_archive_edit/$archive_id";
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_archive_edit', 'Edit Archive', $url)));
    }

    // If "Refresh" was clicked.
    else if ($trigger_elem == 'refresh_btn') {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_archive/$archive_id")));
    }

    // If "Delete" was clicked.
    else if ($trigger_elem == 'delete_btn') {

      // Delete the archvie file.
      $bims_archive = BIMS_ARCHIVE::byID($archive_id);
      if ($bims_archive && $bims_archive->delete()) {
        drupal_set_message("The archive has been deleted.");
      }
      else {
        drupal_set_message("Error : Failed to delete the archive.");
      }

      // Updates panels.
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_archive')));
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_archive_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Archive Data' was clicked.
  if ($trigger_elem == 'archive_btn') {
    $count = 0;
    $form_elems = $form_state['values']['archive_list']['archive_options']['cbs'];
    foreach ($form_elems as $elem => $value) {
      if ($value) {
        $count++;
      }
    }
    if ($count == 0) {
      form_set_error('archive_list][archive_options][cbs', "Please choose at least one data type.");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_archive_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // If "Archive" was clicked.
  if ($trigger_elem == 'archive_btn') {

    // Gets the form values.
    $description  = trim($form_state['values']['archive_list']['description']);
    $form_elems   = $form_state['values']['archive_list']['archive_options']['cbs'];

    // Sets the parameters.
    $param_arr = array();
    foreach ($form_elems as $elem => $value) {
      if ($value) {
        $label = $form['archive_list']['archive_options']['cbs'][$elem]['#title'];
        if (preg_match("/(phenotype|genotype_snp)/", $elem)) {
          $opt        = $form_state['values']['archive_list']['archive_options']['rbs'][$elem];
          $opt_label  = $form['archive_list']['archive_options']['rbs'][$elem]['#options'][$opt];
          $param_arr[$elem . $opt] = "$label $opt_label";
        }
        else {
          $param_arr[$elem] = $label;
        }
      }
    }

    // Creates an archive.
    $details = array(
      'program_id'  => $program_id,
      'user_id'     => $bims_user->getUserID(),
      'params'      => json_encode($param_arr),
      'description' => $description,
      'status'      => 0,
      'submit_date' => date("Y-m-d H:i:s"),
    );
    $bims_archive = BIMS_ARCHIVE::addArchive($details);
    if ($bims_archive) {

      // Runs the archiving job.
      if ($bims_archive->runArchive()) {
        drupal_set_message("The archiving job has been submitted.");
      }
      else {
        drupal_set_message("Error : Failed to run the archiving job.");
      }
    }
    else {
      drupal_set_message("Error : Failed to add archive.");
    }
  }

  // Updates panel
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_archive_form')));
}

/**
 * Theme function for the advanced options table.
 *
 * @param array $variables
 */
function theme_bims_panel_main_archive_form_archive_options_table($variables) {
  $element = $variables['element'];

  // Gets all data types.
  $data_types = $element['data_types']['#value'];

  // Adds the types of data.
  $rows = array();
  foreach ($data_types as $type => $label) {
    if (!array_key_exists($type, $element['cbs'])) {
      continue;
    }
    $form_type   = '&nbsp;';
    if ($type == 'phenotype') {
      $form_type = drupal_render($element['rbs']['phenotype']);
    }
    else if ($type == 'genotype_snp') {
      $form_type = drupal_render($element['rbs']['genotype_snp']);
    }
    $rows []= array(array('data' => drupal_render($element['cbs'][$type]), 'width' => 1), $form_type);
  }

  // Creates 'advanced options' table.
  $table_vars = array(
    'header'      => array('<span style="margin-left:20px;">Data&nbsp;Type</span>', 'Form&nbsp;options'),
    'rows'        => $rows,
    'empty'       => '<em>No data to be archived.</em>',
    'attributes'  => array('style' => 'width:100%'),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the archive table.
 *
 * @param array $variables
 */
function theme_bims_panel_main_archive_form_archive_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_ARCHIVE.
  $bims_archive = $element['bims_archive']['#value'];

  // Initializes the properties.
  $archive_id     = '--';
  $name           = '--';
  $submit_date    = '--';
  $param_list     = '--';
  $complete_date  = '';
  $desc           = '';
  $status         = '--';
  $actions        = '';
  $download_files = '';

  // Updates the properites.
  if ($bims_archive) {
    $archive_id     = $bims_archive->getArchiveID();
    $name           = $bims_archive->getName();
    $params         = $bims_archive->getParams();
    $submit_date    = $bims_archive->getSubmitDate();
    $complete_date  = $bims_archive->getCompleteDate();
    $desc           = $bims_archive->getDescription();
    $status         = $bims_archive->getStatusLabel();

    // Updates download files.
    if ($status == 'completed') {
      $file_json = $bims_archive->getDownloadFiles();
      $file_arr = json_decode($file_json, TRUE);
      $items = array();
      foreach ($file_arr as $num => $filename) {
        $filepath = $bims_archive->getWorkingDir() . "/$filename";
        if (file_exists($filepath)) {
          $items []= l('Download', "bims/download_archive_file/$archive_id-$num", array('attributes' => array('target' => '_blank')));
        }
        else {
          $items []= "<em>$filename not found</em>";
        }
      }
      $download_files = theme('item_list', array('items' => $items));
    }

    // Updates the params.
    $param_arr = json_decode($params, TRUE);
    $param_str = implode("\n", array_values($param_arr));
    if ($param_str) {
      $param_list = "<textarea class='bims-textarea-400' style='width:100%;' rows=3 READONLY>$param_str</textarea>";
    }
  }

  // Adds them in textarea.
  $desc = "<textarea class='bims-textarea-400' style='width:100%;' rows=3 READONLY>$desc</textarea>";

  // Adds the action buttons.
  if (array_key_exists('delete_btn', $element)) {
    $actions .= drupal_render($element['delete_btn']);
  }
  if (array_key_exists('edit_btn', $element)) {
    $actions .= drupal_render($element['edit_btn']);
  }
  if (array_key_exists('refresh_btn', $element)) {
    $actions .= drupal_render($element['refresh_btn']);
  }
  if (!$actions) {
    $actions = '--';
  }

  // Creates the file information table.
  $rows = array();

  $rows []= array(array('data' => 'Archive&nbsp;ID', 'width' => '120'), $archive_id);
  $rows []= array('Name', $name);
  $rows []= array('Data&nbsp;Types', $param_list);
  $rows []= array('Submit&nbsp;Date', $submit_date);
  $rows []= array('Status', $status);
  if ($complete_date) {
    $rows []= array('Complete&nbsp;Date', $complete_date);
  }
  $rows []= array('Description', $desc);
  if ($download_files) {
    $rows []= array('Archive&nbsp;File(s)', $download_files);
  }
  $rows []= array('Actions', $actions);
  $table_vars = array(
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%'),
  );
  $form['file_info'] = array(
    '#markup' => theme('table', $table_vars),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the form.
 *
 * @param array $variables
 */
function theme_bims_panel_main_archive_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('archive_admin', $form)) {
    $layout.= "<div style='float:left;width:100%;'>" . drupal_render($form['archive_admin']) . '</div>';
  }

  // Adds the archive list.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['archive_list']) . "</div>";

  // Adds the archive details
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['archive_details']) . "</div>";

  // Renders form elements.
  $layout .= drupal_render_children($form);
  return $layout;
}