<?php
/**
 * @file
 */
/**
 * Archive edit panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_archive_edit_form($form, &$form_state, $file_id) {

  // Gets BIMS_USER and BIMS_FILE.
  $bims_user = getBIMS_USER();
  $bims_file = BIMS_FILE::byID($file_id);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves the file.
  $form['bims_file'] = array(
    '#type'   => 'value',
    '#value'  => $bims_file,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_archive_edit');
  $bims_panel->init($form);

  // Properties of the archive.
  $form['archive_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Edit Archive',
  );
  $form['archive_prop']['description'] = array(
    '#title'          => t('Description'),
    '#type'           => 'textarea',
    '#default_value'  => $bims_file->getDescription(),
    '#description'    => t("Please type the description of the archive."),
    '#rows'           => 2,
    '#attributes'     => array('style' => 'width:300px;'),
  );
  $form['archive_prop']['edit_btn']= array(
    '#name'       => 'edit_btn',
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_archive_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-archive-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['archive_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_archive_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-archive-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-archive-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_archive_edit_form_submit';
  $form['#theme'] = 'bims_panel_add_on_archive_edit_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_archive_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_archive_edit_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_btn') {}
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_archive_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_btn') {

    // Archive properties.
    $bims_file    = $form_state['values']['bims_file'];
    $description  = trim($form_state['values']['archive_prop']['description']);

    // Sets the properties.
    $bims_file->setDescription($description);
    if ($bims_file->update()) {
      drupal_set_message("The description was updated successfully.");
    }
    else {
      drupal_set_message("Error : Failed to edit the archive.", 'error');
    }
  }
  else {

    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_archive_edit')));
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_archive')));
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_archive_edit_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "archive".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['archive_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
