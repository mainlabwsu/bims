<?php
/**
 * @file
 */
/**
 * Configuration panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_config_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 340;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_config');
  $bims_panel->init($form);

  // Shows all links to config add on panels.
  $form['config'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Configuration Pages',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['config']['links'] = array(
    '#markup' => _bims_panel_main_config_form_config_page_link(),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-config-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_config_form_submit';
  return $form;
}

/**
 * Lists all links to the configuration page.
 *
 * @return string
 */
function _bims_panel_main_config_form_config_page_link() {

  // Sets th links of the configuration papges.
  $link_bims  = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_config\",\"BIMS / Field Book Configuration\",\"bims/load_main_panel/bims_panel_add_on_config/bims\"); return (false);'><b>BIMS / Field Book</b></a>";
  $link_sap   = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_config\",\"Search Configuration\",\"bims/load_main_panel/bims_panel_add_on_config/sap\"); return (false);'><b>Search Phenotype Category</b></a>";
  $link_sc    = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_config\",\"Search Configuration\",\"bims/load_main_panel/bims_panel_add_on_config/sc\"); return (false);'><b>Search Cross Category</b></a>";
  $link_sag   = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_config\",\"Search Configuration\",\"bims/load_main_panel/bims_panel_add_on_config/sag\"); return (false);'><b>Search Genotype Category</b></a>";
  $link_fb    = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_config\",\"Field Book Configuration\",\"bims/load_main_panel/bims_panel_add_on_config/field_book\"); return (false);'><b>Field Book</b></a>";
  $link_to    = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_config\",\"Trait Order Configuration\",\"bims/load_main_panel/bims_panel_add_on_config/trait_order\"); return (false);'><b>Trait Order</b></a>";

  // Lists the links.
  $code = <<< CLASS_CODE
  <li style='margin-top:35px;'>$link_bims</li>
  <p style='margin-left:15px;'>
    The configuration page of BIMS and FieldBook (<em>Required fields</em>).
  </p>
  <li style='margin-top:20px;'>$link_sap</li>
  <p style='margin-left:15px;'>
    The configuration page of search is used to customerize phenotype search properties.
  </p>
  <li style='margin-top:20px;'>$link_sc</li>
  <p style='margin-left:15px;'>
    The configuration page of search is used to customerize cross search properties.
  </p>
  <li style='margin-top:20px;'>$link_sag</li>
  <p style='margin-left:15px;'>
    The configuration page of search is used to customerize genotype search properties.
  </p>
  <li style='margin-top:20px;'>$link_fb</li>
  <p style='margin-left:15px;'>
    The configuration page of Field Book is used to customerize columns used in Field Book files.
  </p>

  <li style='margin-top:15px;'>$link_to</li>
  <p style='margin-left:15px;'>
    The traits (descriptors) are ordered alphaberically as default. This configuration page
    is used to customerize the order of descriptors.
  </p>
  <p style='margin-top:30px;'><em>Please click an item title to open a configuration page</em>.</p>
CLASS_CODE;
  return $code;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_config_form_ajax_callback($form, $form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_config_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_config_form_submit($form, &$form_state) {}