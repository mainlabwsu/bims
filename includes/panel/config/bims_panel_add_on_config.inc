<?php
/**
 * @file
 */
/**
 * Configuration panel.
 *
 * @param array $form
 * @param array $form_state
 * @param string $category
 * @param string $type
 */
function bims_panel_add_on_config_form($form, &$form_state, $category = '') {

  // Local variables for form properties.
  $min_height = 240;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Sets the params for seach categories.
  $cvterm_id_searchable = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'searchable')->getCvtermID();
  $cvterm_id_format     = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'format')->getCvtermID();

  $options = array(
    'text'        => 'Text Search',
    'categorical' => 'Categorical',
    'numeric'     => 'Numeric',
    'counter'     => 'Counter',
  );
  $search_category_param = array(
    'bims_program'  => $bims_program,
    'searchable'    => $cvterm_id_searchable,
    'format'        => $cvterm_id_format,
    'options'       => $options,
  );

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_config');
  if (!$bims_panel->init($form, TRUE, TRUE)) {
    return $form;
  }

  // Configuration for 'BIMS / Field Book'.
  if ($category == 'bims') {
    $form['bims'] = array(
      '#id'           => 'bims',
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'BIMS / Filed Book (<em>Required fields</em>)',
    );
    $markup = "BIMS / Field Book requires 4 columns : <em><b>'Accession'</b>, <b>'Unique Identifier'</b>, '<b>Primary order</b>' and <b>'Secondary order'</b></em>.<div style='margin-top:10px;'>Please change the default names of these columns.</div>";
    $form['bims']['req_cols'] = array(
      '#markup' => "$markup<br />",
    );
    _bims_panel_add_on_config_form_field_book_req_cols($form['bims'], $bims_program);
  }

  // Configuration for 'Trait Order'.
  else if ($category == 'trait_order') {
    $form['trait_order'] = array(
      '#id'           => 'trait_order',
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Trait Order',
    );
    _bims_panel_add_on_config_form_trait_order_table($form['trait_order'], $bims_program);
    $form['trait_order']['#theme']= 'bims_panel_add_on_config_form_trait_order_table';
  }

  // Configuration for 'Search Phenotype'.
  else if ($category == 'sap') {
    $desc = "<div>The custom properties associated with the phenotypic data " .
      "are listed. Please check on the properties you want to search on.</div>";
    $form['sap'] = array(
      '#id'           => 'sap',
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Search Phenotype Properties',
      '#description'  => $desc,
    );
    $search_category_param['id'] = 'sap';
    $search_category_param['cv_types'] = array('sample');
    _bims_panel_add_on_config_form_table($form['sap'], $search_category_param);
    $form['sap']['#theme']= 'bims_panel_add_on_config_form_sap_table';
  }

  // Configuration for 'Search Cross'.
  else if ($category == 'sc') {
    $desc = "<div>The custom properties associated with the cross data " .
        "are listed. Please check on the properties you want to search on.</div>";
    $form['sc'] = array(
      '#id'           => 'sc',
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Search Cross Properties',
      '#description'  => $desc,
    );
    $search_category_param['id'] = 'sc';
    $search_category_param['cv_types'] = array('cross');
    _bims_panel_add_on_config_form_table($form['sc'], $search_category_param);
    $form['sc']['#theme']= 'bims_panel_add_on_config_form_sc_table';
  }

  // Configuration for 'Search Genotype'.
  else if ($category == 'sag') {
    $desc = "<div>The custom properties associated with the genotypic data " .
      "are listed. Please check on the properties you want to search on.</div>";
    $form['sag'] = array(
      '#id'           => 'sag',
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Search Genotype Properties',
      '#description'  => $desc,
    );
    $search_category_param['id'] = 'sag';
    $search_category_param['cv_types'] = array('genotype');
    _bims_panel_add_on_config_form_table($form['sag'], $search_category_param);
    $form['sag']['#theme']= 'bims_panel_add_on_config_form_sag_table';
  }

  // Configuration for 'Field Book'.
  else if ($category == 'field_book') {
    $form['field_book'] = array(
      '#id'           => 'field_book',
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Field Book Configuration',
    );

    // Field Book custom columns
    $form['field_book']['custom_cols'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => TRUE,
      '#title'        => 'Field Book Custom Columns',
    );
    $accession = $bims_program->getBIMSLabel('accession');
    $markup = "Please choose custom fields from the 3 sections (trait, $accession and sample properties). If the trait is checked, you can choose the trait values of the ongoing trials to be displayed in the input file to note that the phenotyping for the plant is done already. The trait descriptors for phenotyping will be provided to Frield Book App via the trait file.";
    $form['field_book']['custom_cols']['markup'] = array(
      '#markup' => "<div>$markup</div><br />",
    );

    // Shows the lists of traits / accession properties / sample properties.
    _bims_panel_add_on_config_form_field_book_add_list($form['field_book']['custom_cols'], $bims_user, 'fb_trait', 'cvterm', 'Traits');
    _bims_panel_add_on_config_form_field_book_add_list($form['field_book']['custom_cols'], $bims_user, 'fb_accession', 'text', ucfirst($accession) . ' properties');
    _bims_panel_add_on_config_form_field_book_add_list($form['field_book']['custom_cols'], $bims_user, 'fb_sample', 'cvterm', 'Sample properties');
  }

  // Error : no category.
  else {
    $form['error'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => 'Error',
      '#description'  => t("Error : unknown category specifed"),
    );
  }

  // Sets form properties.
  $form['#prefix'] = '<div id="bims-panel-add-on-config-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_config_form_submit';
  $form['#theme'] = 'bims_panel_add_on_config_form';
  return $form;
}

/**
 * Generates the sortable table to order the descriptors.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_config_form_trait_order_table(&$form, BIMS_PROGRAM $bims_program) {

  // Add the save button.
  $form['trait_order_save_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'trait_order_save_btn',
    '#value'      => 'Save Order',
    '#attributes' => array('style' => 'width:120px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_config_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-config-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Gets all the descriptors for this program.
  $descriptors = $bims_program->getDescriptors(NULL, BIMS_OBJECT);

  // Populates the table with descriptors.
  $rows         = array();
  $row_elements = array();
  $total        = sizeof($descriptors);
  $weight       = $total * (-1) - 1;
  foreach ($descriptors as $descriptor) {
    $weight++;

    $form['descriptors'][$descriptor->cvterm_id] = array(
      'name' => array(
        '#markup' => check_plain($descriptor->name),
      ),
      'format' => array(
        '#markup' => $descriptor->format,
      ),
      'definition' => array(
        '#markup' => check_plain($descriptor->definition),
      ),
      'weight' => array(
        '#type'           => 'weight',
        '#title_display'  => 'invisible',
        '#default_value'  => $weight,
        '#delta'          => $total,
        '#attributes' => array('class' => array('bims-trait-order-weight')),
      ),
    );
  }
}

/**
 * Generates the property table.
 *
 * @param array $form
 * @param array $params
 */
function _bims_panel_add_on_config_form_table(&$form, $params) {

  // Gets the parameters.
  $id             = $params['id'];
  $bims_program   = $params['bims_program'];
  $id_searchable  = $params['searchable'];
  $id_format      = $params['format'];
  $options        = $params['options'];
  $cv_types       = $params['cv_types'];

  // Adds the properties.
  $disabled = TRUE;
  foreach ($cv_types as $cv_type) {
    $cvterms = $bims_program->getProperyByCV($cv_type, BIMS_CLASS);
    foreach ((array)$cvterms as $cvterm) {
      $disabled   = FALSE;
      $name       = $cvterm->getName();
      $cvterm_id  = $cvterm->getCvtermID();
      $format     = $cvterm->getPropByTypeID($id_format);
      $searchable = $cvterm->getPropByTypeID($id_searchable);

      // Sets the defaulat values.
      $cb_default = $searchable ? TRUE : FALSE;
      $rb_default = $format ? $format : 'text';
      $form[$id][$cvterm_id]['cb'] = array(
        '#title'          => $name,
        '#type'           => 'checkbox',
        '#default_value'  => $cb_default,
      );
      $form[$id][$cvterm_id]['rb'] = array(
        '#type'           => 'radios',
        '#options'        => $options,
        '#default_value'  => $rb_default,
      );
    }
  }

  // Add the save button.
  $elem_name = $id . '_save_btn';
  $form[$elem_name] = array(
    '#type'       => 'submit',
    '#name'       => $elem_name,
    '#value'      => 'Save Configuration',
    '#disabled'   => $disabled,
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_config_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-config-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
}

/**
 * Field Book required columns.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 */
function _bims_panel_add_on_config_form_field_book_req_cols(&$form, BIMS_PROGRAM $bims_program) {

  // Updates the values of the required columns. Gets the saved BIMS columns.
  $bims_cols            = $bims_program->getBIMSCols();
  $def_accession        = $bims_cols['accession'];
  $def_unique_id        = $bims_cols['unique_id'];
  $def_primary_order    = $bims_cols['primary_order'];
  $def_secondary_order  = $bims_cols['secondary_order'];

  // Form elements.
  $form['accession'] = array(
    '#type'           => 'textfield',
    '#title'          => t('1. Accession'),
    '#description'    => t("Please provide a column name for accession"),
    '#default_value'  => $def_accession,
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['unique_id'] = array(
    '#type'           => 'textfield',
    '#title'          => t('2. Unique identifier'),
    '#default_value'  => $def_unique_id,
    '#description'    => t("Please provide a column name for unique identifier"),
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['primary_order'] = array(
    '#type'           => 'textfield',
    '#title'          => t('3. Primary order'),
    '#description'    => t("Please provide a column name for primary order"),
    '#default_value'  => $def_primary_order,
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['secondary_order'] = array(
    '#type'           => 'textfield',
    '#title'          => t('4. Secondary order'),
    '#description'    => t("Please provide a column name for secondary order"),
    '#default_value'  => $def_secondary_order,
    '#attributes'     => array('style' => 'width:200px;'),
  );
  $form['save_btn_field_book_req_cols'] = array(
    '#type'       => 'submit',
    '#name'       => 'save_btn_field_book_req_cols',
    '#value'      => 'Save',
    '#attributes' => array('style' => 'width:200px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_config_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-config-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
}

/**
 * Adds the list of the provided type.
 *
 * @param array $form
 * @param BIMS_USER $bims_user
 * @param string $col_type
 * @param string $item_type
 * @param string $label
 */
function _bims_panel_add_on_config_form_field_book_add_list(&$form, BIMS_USER $bims_user, $col_type, $item_type, $label) {

  // Sets the ID for the fieldset.
  $fieldset_id = 'bims_aoc_' . $col_type;

  // Adds a fieldset for the provied type.
  $form[$col_type] = array(
    '#id'           => $fieldset_id,
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => $label,
  );
  $form[$col_type]['fieldset_id'] = array(
    '#type'   => 'value',
    '#value'  => $fieldset_id,
  );

  // Gets the list of items of the provided column type.
  $items = _bims_panel_add_on_config_form_field_book_get_items($bims_user, $col_type, $item_type);
  if (empty($items)) {
    $form[$col_type]['no_data'] = array(
      '#markup' => '<div>No ' . strtolower($label) . ' to choose.</div>',
    );
    return;
  }
  $options = array();
  $default_values = array();
  foreach ($items as $id => $info) {
    $options[$id] = $info['name'];
    if ($info['bool']) {
      $default_values []= $id;
    }
  }
  $form[$col_type]['ids'] = array(
    '#type'           => 'checkboxes',
    '#options'        => $options,
    '#default_value'  => $default_values,
  );

  // Adds 'Save' button'
  $name = 'save_btn_field_book_' . $col_type;
  $form[$col_type][$name] = array(
    '#type'       => 'submit',
    '#name'       => $name,
    '#value'      => 'Save',
    '#attributes' => array('style' => 'width:200px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_config_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-config-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form[$col_type]['name'] = array(
    '#type'   => 'value',
    '#value'  => $name,
  );
  $form[$col_type]['#theme'] = 'bims_panel_add_on_config_form_cvterm_table';
}

/**
 * Returns the list of items of the provided type.
 *
 * @param BIMS_USER $bims_user
 * @param string $col_type
 * @param string $item_type
 *
 * @return array
 */
function _bims_panel_add_on_config_form_field_book_get_items(BIMS_USER $bims_user, $col_type, $item_type) {

  // Gets the stored config list if exists.
  $bims_program = $bims_user->getProgram();
  $config_list  = $bims_program->getConfig($col_type);

  // Returns the traits.
  $item_list = array();
  if ($col_type == 'fb_trait') {
    $item_list = $bims_program->getDescriptors(NULL, BIMS_FIELD);
  }

  // Returns the accession properties.
  else if ($col_type == 'fb_accession') {
    $item_list = $bims_program->getProperties('accession', 'default_custom', BIMS_FIELD);
  }

  // Returns the sample properties.
  else if ($col_type == 'fb_sample') {
    $item_list = $bims_program->getProperties('sample', 'default_custom', BIMS_FIELD);
  }

  // Saves the information.
  $items = array();
  if (!empty($item_list)) {
    foreach ($item_list as $id => $name) {
      $bool = array_key_exists($id, $config_list) ?  TRUE : FALSE;
      $items[$id] = array(
        'name' => $name,
        'bool' => $bool,
      );
    }
  }
  return $items;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_config_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_config_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' (Field Book required columns) was clicked.
  if ($trigger_elem == 'save_btn_field_book_req_cols') {
    $cols = array(
      'unique_id'       => 'Unique Identifier',
      'primary_order'   => 'Primary Order',
      'secondary_order' => 'Secondary Order',
      'accession'       => 'Accession',
    );
    $check_unique = array();
    foreach ($cols as $col => $label) {
      $value = $form_state['values']['bims'][$col];
      $error = bims_validate_name($value, FALSE, FALSE);
      if ($error) {
        form_set_error('bims][' . $col, $error);
        return;
      }
      if ($col != 'accession') {
        $check_unique[$value] = $col;
      }
    }

    // Checks uniqueness for FB columns.
    if (sizeof($check_unique) != 3) {
      form_set_error('bims', "Please avoid to use the same names.");
      return;
    }
  }

  // If 'Save' (FB custom columns) was clicked.
  else if (preg_match("/^save_btn_field_book_(.*)$/", $trigger_elem, $matches)) {
    $col_type = $matches[1];
    $num_checked = 0;
    $checkboxes = $form_state['values']['field_book']['custom_cols'][$col_type]['ids'];
    foreach ($checkboxes as $id => $value) {
      if ($value) {
        $num_checked++;
      }
    }
    if ($num_checked == 0) {
      form_set_error('field_book][custom_cols][' . $col_type, "Please check at least one for " . ucfirst($col_type));
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_config_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Save' was clicked  (phenotype).
  if ($trigger_elem == 'sap_save_btn') {

    // Save the search configurations.
    if (_bims_panel_add_on_config_form_save_config('sap', $form_state['values']['sap'])) {
      drupal_set_message("Search configurations have been saved.");
    }
    else {
      drupal_set_message("Error : Faield to save 'Search Phenotype' configuration.", 'error');
    }
  }

  // If 'Save' was clicked (cross).
  else if ($trigger_elem == 'sc_save_btn') {

    // Save the search configurations.
    if (_bims_panel_add_on_config_form_save_config('sc', $form_state['values']['sc'])) {
      drupal_set_message("Search configurations have been saved.");
    }
    else {
      drupal_set_message("Error : Faield to save 'Search Cross' configuration.", 'error');
    }
  }

  // If 'Save' was clicked (genotype).
  else if ($trigger_elem == 'sag_save_btn') {

    // Save the search configurations.
    if (_bims_panel_add_on_config_form_save_config('sag', $form_state['values']['sag'])) {
      drupal_set_message("Search configurations have been saved.");
    }
    else {
      drupal_set_message("Error : Faield to save 'Search Genotype' configuration.", 'error');
    }
  }

  // If 'Save Order' was clicked.
  else if ($trigger_elem == 'trait_order_save_btn') {

    // Updates the rank for the descriptors.
    foreach ($form_state['values']['trait_order']['descriptors'] as $cvterm_id => $item) {
      $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id);
      if ($descriptor) {
        $descriptor->updateRank($item['weight']);
      }
    }
  }

  // If 'Save' (FB required columns) was clicked.
  else if ($trigger_elem == 'save_btn_field_book_req_cols') {

    // Saves FB required columns.
    $elem_ids = array('unique_id', 'primary_order', 'secondary_order', 'accession');
    $bims_cols = array();
    foreach ($elem_ids as $elem_id) {
      $elem_value = $form_state['values']['bims'][$elem_id];
      $bims_cols[$elem_id] = $elem_value;
    }
    if (!$bims_program->setBIMSCols($bims_cols)) {
      drupal_set_message("Error : Failed to update Field Book required columns", 'error');
    }
    else {
      BIMS_MESSAGE::setMsg("The Field Book required columns have been saved.");
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_sidebar", array()));
    }
  }

  // If 'Save' (FB custom columns) was clicked.
  else if (preg_match("/^save_btn_field_book_(.*)$/", $trigger_elem, $matches)) {
    $col_type = $matches[1];
    $checkboxes = $form_state['values']['field_book']['custom_cols'][$col_type]['ids'];
    $custom_cols = array();
    foreach ($checkboxes as $id => $value) {
      if ($value) {
        $title = $form['field_book']['custom_cols'][$col_type]['ids'][$id]['#title'];
        $custom_cols[$id] = $title;
      }
    }
    $bims_program->setConfig($col_type, $custom_cols);
  }
}

/**
 * Saves search cross configuration.
 *
 * @param string $type
 * @param array $form
 *
 * @return boolean
 */
function _bims_panel_add_on_config_form_save_config($type, $form_state) {

  // Gets the cvterm ID.
  $cvterm_id_searchable = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'searchable')->getCvtermID();
  $cvterm_id_format     = MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'format')->getCvtermID();

  // Updates the search configuration.
  $transaction = db_transaction();
  try {

    // Saves the cross search config.
    foreach ((array)$form_state[$type] as $cvterm_id => $elems) {
      $cvterm = MCL_CHADO_CVTERM::byID($cvterm_id);

      // Updates the format of the property.
      if (!$cvterm->updatePropByTypeID(NULL, $cvterm_id_format, $elems['rb'])) {
        throw new Exception("Error : Failed to update the format of the property");
      }

      // Updates 'searchable' of the property.
      if ($elems['cb']) {
        if (!$cvterm->updatePropByTypeID(NULL, $cvterm_id_searchable, $elems['rb'])) {
          throw new Exception("Error : Failed to update the $type property");
        }
      }
      else {
        if (!$cvterm->emptyPropByTypeID(NULL, $cvterm_id_searchable)) {
          throw new Exception("Error : Failed to empty the $type property");
        }
      }
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Theme function for the table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_config_form_cvterm_table($variables) {
  $element = $variables['element'];

  // Adds a checkbox for 'Check All'.
  $fieldset_id = $element['fieldset_id']['#value'];
  $check_all = "<div style=''><input style='' type='checkbox' onchange='bims.bims_select_all(this, \"$fieldset_id\");'> Check All / None</div>";

  // Populates the table.
  $row      = array();
  $rows     = array();
  $count    = 0;
  $num_col  = 4;
  if (array_key_exists('ids', $element)) {
    foreach ((array)$element['ids'] as $id => $checkbox) {
      if (!preg_match("/^#/", $id)) {
        $count++;
        $row []= drupal_render($checkbox);
        if (($count % $num_col) == 0) {
          $rows []= $row;
          $row = array();
        }
      }
    }
    if (!empty($row)) {
      $size = sizeof($row);
      if ($size < $num_col) {
        for ($i = $size; $i < $num_col; $i++) {
          $row []= '&nbsp;';
        }
      }
      $rows []= $row;
    }
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'empty'       => '<div style="margin-top:12px;height:25px;">This program currently does not have ' . strtolower($element['#title']) . '</div>',
    'attributes'  => array('style' => 'width:100%;'),
  );
  $layout = $check_all . theme('table', $table_vars);

  // Adds 'Save' button.
  $btn_name = $element['name']['#value'];
  $layout .= drupal_render($element[$btn_name]);
  return $layout;
}

/**
 * Theme function for the table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_config_form_sap_table($variables) {
  $element = $variables['element'];

  // Add the accession properties.
  $accession = '';

  // Adds the cross properties.
  $cross = '';

  // Adds the location properties.
  $location = '';

  // Adds the trial properties.
  $trial = '';

  // Adds the phenotype properties.
  $props = $element['sap'];
  $rows = array();
  foreach ((array)$props as $cvterm_id => $elem) {
    if (preg_match("/^\d+$/", $cvterm_id)) {
      $rows []= array(drupal_render($elem['cb']), drupal_render($elem['rb']));
    }
  }
  $headers = array(
    array('data' => 'Phenotype Property', 'width' => 220),
    array('data' => 'Data Type', 'width' => 140),
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'empty'       => 'No phenotype property found',
    'param'       => array(),
  );
  $table = bims_theme_table($table_vars);

  // Adds the save button.
  $save_btn = drupal_render($element['sap_save_btn']);

  // Sets the layout.
  $layout = "<div style='width:45%;'>";
  $layout .= "<div style='width:100%;'>$table</div>";
  $layout .= "<div style='width:100%;'>$save_btn</div></div>";
  return $layout;
}

/**
 * Theme function for the table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_config_form_sc_table($variables) {
  $element = $variables['element'];

  // Adds the cross properties.
  $props = $element['sc'];
  $rows = array();
  foreach ((array)$props as $cvterm_id => $elem) {
    if (preg_match("/^\d+$/", $cvterm_id)) {
      $rows []= array(drupal_render($elem['cb']), drupal_render($elem['rb']));
    }
  }
  $headers = array(
    array('data' => 'Cross Property', 'width' => 220),
    array('data' => 'Data Type', 'width' => 140),
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'empty'       => 'No cross property found',
    'param'       => array(),
  );
  $table = bims_theme_table($table_vars);

  // Adds the save button.
  $save_btn = drupal_render($element['sc_save_btn']);

  // Sets the layout.
  $layout = "<div style='width:45%;'>";
  $layout .= "<div style='width:100%;'>$table</div>";
  $layout .= "<div style='width:100%;'>$save_btn</div></div>";
  return $layout;
}

/**
 * Theme function for the table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_config_form_sag_table($variables) {
  $element = $variables['element'];

  // Add the accession properties.
  $accession = '';

  // Adds the trial properties.
  $trial = '';

  // Gets the genotype properties.
  $pprops = $element['sag'];
  $rows = array();
  foreach ((array)$pprops as $cvterm_id => $elem) {
    if (preg_match("/^\d+$/", $cvterm_id)) {
      $rows []= array(drupal_render($elem['cb']), drupal_render($elem['rb']));
    }
  }
  $headers = array(
    array('data' => 'Genotype Property', 'width' => 220),
    array('data' => 'Data Type', 'width' => 140),
  );
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
    'sticky'      => TRUE,
    'empty'       => 'No genotype property found',
    'param'       => array(),
  );
  $table = bims_theme_table($table_vars);

  // Adds the save button.
  $save_btn = drupal_render($element['sag_save_btn']);

  // Sets the layout.
  $layout = "<div style='width:45%;'>";
  $layout .= "<div style='width:100%;'>$table</div>";
  $layout .= "<div style='width:100%;'>$save_btn</div></div>";
  return $layout;
}

/**
 * Theme function for the trait order table.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_config_form_trait_order_table($variables) {
  $element = $variables['element'];

  // Populates the table rows with descriptors.
  $rows = array();
  foreach (element_children($element['descriptors']) as $id) {

    // Sets the calss.
    $element['descriptors'][$id]['weight']['#attributes']['class'] = array(
      'trait-order-weight',
    );

    // Adds a row.
    $rows[] = array(
      'data' => array(
        drupal_render($element['descriptors'][$id]['name']),
        drupal_render($element['descriptors'][$id]['format']),
        drupal_render($element['descriptors'][$id]['definition']),
        drupal_render($element['descriptors'][$id]['weight']),
      ),
      'class' => array('draggable'),
    );
  }

  // Sets the headers.
  $header = array(
    'Name',
    'Format',
    'Definition',
    'Weight',
  );
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('id' => 'bims-trait-order', 'style' => 'width:100%;'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  $layout = drupal_render_children($element);
  return $layout . bims_theme_table($table_vars);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_config_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds 'Search Phenotype' section.
  if (array_key_exists('sap', $form)) {
    $layout .= "<div style='float:left;width:100%;'>" . drupal_render($form['sap']) . "</div>";
  }

  // Adds 'Search Genotype' section.
  if (array_key_exists('sag', $form)) {
    $layout .= "<div style='float:left;width:100%;'>" . drupal_render($form['sag']) . "</div>";
  }

  // Adds 'Field Book' section.
  if (array_key_exists('field_book', $form)) {
    $layout .= "<div style='float:left;width:100%;'>" . drupal_render($form['field_book']) . "</div>";
  }

  // Adds 'Trait Order' section.
  if (array_key_exists('trait_order', $form)) {
    $layout .= "<div style='float:left;width:100%;'>" . drupal_render($form['trait_order']) . "</div>";
  }
  $layout .= drupal_render_children($form);
  return $layout;
}