<?php
/**
 * Trial panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trial_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 360;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets node_id.
  $bims_trial = NULL;
  if (isset($form_state['values']['trial_list']['selection'])) {
    $node_id = $form_state['values']['trial_list']['selection'];
    $bims_trial = BIMS_TRIAL::byID($node_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_trial');
  $bims_panel->init($form);

  // Adds the admin menu.
  $form['trial_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Trial',
    '#attributes'   => array(),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_trial_add";
  $form['trial_admin']['menu']['add_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_trial_add', 'Add Trial', '$url'); return (false);",
    )
  );
  $form['trial_admin']['menu']['#theme'] = 'bims_panel_main_trial_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['trial_admin']);

  // Lists the all trials.
  $form['trial_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Trials',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Trial selection.
  $options = $bims_program->getTrials(BIMS_OPTION_DEPTH);
  if (empty($options)) {
    $form['trial_list']['no_trial'] = array(
      '#markup' => 'No trial is associated with the current program.',
    );
  }
  else {
    $desc = "Please select a trial from the list.";
    if (!$bims_program->isPublicChado()) {
      $desc .= "<br />(* indicates imported trials)";
    }
    $form['trial_list']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#size'           => 13,
      '#description'    => $desc,
      '#attributes'     => array('style' => 'width:98%;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_trial_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-trial-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds the trial details.
  $form['trial_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Trial Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Adds buttons.
  if ($bims_trial) {
    $form['trial_details']['table']['view_btn'] = array(
      '#type'       => 'button',
      '#value'      => 'View',
      '#name'       => 'view_btn',
      '#attributes' => array('style' => 'width:90px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_trial_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-trial-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Adds action buttons.
    if (bims_can_admin_action($bims_user)) {

      // Adds 'Edit' button.
      if (!$bims_trial->isChado()) {
        $form['trial_details']['table']['edit_btn'] = array(
          '#type'       => 'button',
          '#value'      => 'Edit',
          '#name'       => 'edit_btn',
          '#attributes' => array('style' => 'width:90px;'),
          '#ajax'       => array(
            'callback' => "bims_panel_main_trial_form_ajax_callback",
            'wrapper'  => 'bims-panel-main-trial-form',
            'effect'   => 'fade',
            'method'   => 'replace',
          ),
        );
      }

      // Adds 'Delete' button.
      $form['trial_details']['table']['delete_btn'] = array(
        '#type'       => 'button',
        '#value'      => 'Delete',
        '#name'       => 'delete_btn',
        '#disabled'   => !$bims_user->canEdit(),
        '#attributes' => array(
          'style' => 'width:90px;',
          'class' => array('use-ajax', 'bims-confirm'),
        ),
        '#ajax'       => array(
          'callback' => "bims_panel_main_trial_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-trial-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
  }
  $form['trial_details']['table']['bims_trial'] = array(
    '#type'   => 'value',
    '#value'  => $bims_trial,
  );
  $form['trial_details']['table']['bims_user'] = array(
    '#type'   => 'value',
    '#value'  => $bims_user,
  );
  $form['trial_details']['table']['#theme'] = 'bims_panel_main_trial_form_trial_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-trial-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_trial_form_submit';
  $form['#theme'] = 'bims_panel_main_trial_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trial_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the node ID.
  $node_id = $form_state['values']['trial_list']['selection'];

  // If 'View' was clicked.
  if ($trigger_elem == 'view_btn') {
    if ($node_id) {
      $url = "bims/load_main_panel/bims_panel_add_on_phenotype_trial/$node_id";
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_phenotype_trial', 'Phenotype by Trial', $url)));
    }
  }

  // If "Edit" or "Delete" was clicked.
  else if ($trigger_elem == 'edit_btn'  || $trigger_elem == 'delete_btn') {

    // Checks the node ID.
    if (!$node_id) {
      drupal_set_message("Please select a trial");
    }
    else {

      // If "Edit" was clicked.
      if ($trigger_elem == 'edit_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_trial_edit/$node_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_trial_edit', 'Edit Trial', $url)));
      }

      // If "Delete" was clicked.
      else if ($trigger_elem == 'delete_btn') {

        // Deletes the trial.
        if (_bims_panel_main_trial_form_delete_trial($bims_program, $node_id)) {
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_trial')));
        }
      }
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Deletes the trial.
 *
 * @param BIMS_PROGRAM $bims_program
 * @param integer $node_id
 *
 * @return boolean
 */
function _bims_panel_main_trial_form_delete_trial(BIMS_PROGRAM $bims_program, $node_id) {

  $transaction = db_transaction();
  try {

    // Deletes the trial.
    $trial = BIMS_TRIAL::byID($node_id);
    if (!$trial->deleteTrial()) {
      throw new Exception("Error : Failed to delete the trial");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trial_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_trial_form_submit($form, &$form_state) {}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_trial_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), 'Add a new trial.');
   $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the trial details table.
 *
 * @param $variables
 */
function theme_bims_panel_main_trial_form_trial_table($variables) {
  $element = $variables['element'];

  // Gets BIMS_TRIAL.
  $bims_trial = $element['bims_trial']['#value'];
  $bims_user  = $element['bims_user']['#value'];

  // Initializes the properties.
  $name         = '--';
  $type         = '--';
  $sub_type     = '--';
  $num_data     = '--';
  $data_row     = '--';
  $properties   = '';
  $actions      = '';

  // Updates the properites.
  if ($bims_trial) {
    if ($bims_user->isAdmin()) {
      $name = sprintf("<span title ='Project ID : %d'>%s</span>", $bims_trial->getProjectID(), $bims_trial->getName());
    }
    else {
      $name = $bims_trial->getName();
    }

    // Gets the number of data.
    $num_data = $bims_trial->getPropByKey('num_data');
    $num_data = $num_data ? $num_data : '0';

    // Adds the buttons.
    $data_row = '';
    if ($num_data) {
      if (array_key_exists('view_btn', $element)) {
        $data_row .= drupal_render($element['view_btn']);
      }
    }
    else {
      $data_row = '<em>No data point</em>';
    }

    // Adds the properties.
    $prop_rows = array();
    if (!empty($prop_rows)) {
      $table_vars = array(
        'header'      => array(array('data' => 'name', 'width' => 100), 'value'),
        'rows'        => $prop_rows,
        'attributes'  => array(),
      );
      $element['prop']['table']['#markup'] = theme('table', $table_vars);
      $properties = drupal_render($element['prop']);
    }

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }

    // Gets the type and sub-type of the trial.
    $type     = ucfirst($bims_trial->getProjectType());
    $sub_type = ucfirst($bims_trial->getProjectSubType());
    if ($sub_type == 'Haplotype') {
      $num_data = 'NA';
    }
  }

  // Updates the 'Statistics / Data' row.
  $label = 'Statistics / Data';
  if ($type == 'Genotype') {
    $label = 'Data';
    $data_row = '<em>N/A</em>';
    $data_row = '';
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'width' => '100'), BIMS_PROGRAM::rmPrefix($name));
  $rows[] = array('Type', $type);
  $rows[] = array('Sub&nbsp;Type', $sub_type);
  if ($num_data != 'NA') {
    $rows[] = array('# data points', $num_data);
  }
  if ($data_row) {
    $rows[] = array($label, $data_row);
  }
  if ($properties) {
    $rows[] = array('Properties', $properties);
  }
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }
  $table_vars = array(
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_trial_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('trial_admin', $form)) {
    $layout.= "<div style='float:left;width:100%;'>" . drupal_render($form['trial_admin']) . '</div>';
  }

  // Adds trial list.
  $layout .= "<div style='float:left;width:44%;'>" . drupal_render($form['trial_list']) . "</div>";

  // Adds the trial info table.
  $layout .= "<div style='float:left;width:55%;margin-left:5px;'>" . drupal_render($form['trial_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
