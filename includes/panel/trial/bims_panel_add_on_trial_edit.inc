<?php
/**
 * @file
 */
/**
 * Trial edit panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_edit_form($form, &$form_state, $node_id) {

  // Gets BIMS_USER and BIMS_TRIAL.
  $bims_user = getBIMS_USER();
  $trial = BIMS_TRIAL::byID($node_id);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves the trial.
  $form['trial'] = array(
    '#type'   => 'value',
    '#value'  => $trial,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_trial_edit');
  $bims_panel->init($form);

  // Properties of the trial.
  $form['trial_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Edit Trial',
  );
  $form['trial_prop']['name'] = array(
    '#title'          => t('Name'),
    '#type'           => t('textfield'),
    '#default_value'  => $trial->getName(),
    '#description'    => t("Please specify a name of trial."),
    '#attributes'     => array('style' => 'width:300px;'),
  );
  $form['trial_prop']['edit_btn']= array(
    '#name'       => 'edit_btn',
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_trial_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trial-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['trial_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_trial_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trial-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-trial-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_trial_edit_form_submit';
  $form['#theme'] = 'bims_panel_add_on_trial_edit_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_edit_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_btn') {

    // Gets the current name.
    $trial          = $form_state['values']['trial'];
    $cur_trial_name = $trial->getName();

    // Checks trial name
    $name = trim($form_state['values']['trial_prop']['name']);
    if ($cur_trial_name != $name) {

      // Validates the trial name.
      $error = bims_validate_name($name);
      if ($error) {
        form_set_error('trial_prop][name', "Invalid trial name : $error");
        return;
      }
      // Checks for duplication.
      $bims_user = getBIMS_USER();
      $bims_program = $bims_user->getProgram();
      $bims_trial = $bims_program->getTrialByName($name);
      if ($bims_trial) {
        form_set_error('trial_prop][name', "Trial name ($name) is alrady used");
        return;
      }
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_btn') {

    // Trial properties.
    $trial  = $form_state['values']['trial'];
    $name   = trim($form_state['values']['trial_prop']['name']);

    // Sets the properties.
    $trial->setName($name);
    if ($trial->update()) {

      // Updates the name on the phenotype mview.
      $mview_phenotype = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $trial->getProgramID()));
      if ($mview_phenotype) {
        if ($mview_phenotype->updateMViewTrialName($trial->getNodeID(), $name)) {

          // Updates panels.
          bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_trial_edit')));
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_trial')));
        }
        else {
          drupal_set_message("Error : Failed to edit the trial (mview).", 'error');
        }
      }
    }
    else {
      drupal_set_message("Error : Failed to edit the trial.", 'error');
    }
  }
  else {

    // Updates panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_trial_edit')));
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_trial')));
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_trial_edit_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "trial".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['trial_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
