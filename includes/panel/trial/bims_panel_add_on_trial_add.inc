<?php
/**
 * @file
 */
/**
 * Trial add panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_add_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_trial_add');
  $bims_panel->init($form);

  // Property for a trial.
  $form['property'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Add a new property",
    '#collapsed'   => TRUE,
    '#collapsible' => TRUE,
  );
  $form['property']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Property Name'),
    '#description'  => t("Please provide an name of a property."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['property']['desc'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please provide a description of a property."),
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['property']['add_property_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_property_btn',
    '#value'      => "Add property",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_trial_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trial-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Properties a trial.
  $form['trial_prop'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Create Trial',
  );
  $form['trial_prop']['name'] = array(
    '#title'        => t('Name'),
    '#type'         => t('textfield'),
    '#description'  => t("Please specify a name of the trial."),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $options = array(
    'phenotype'     => 'Phenotype',
    'genotype_SNP'  => 'Genotype / SNP',
    'genotype_SSR'  => 'Genotype / SSR',
    'cross'         => 'Cross'
  );
  $form['trial_prop']['type'] = array(
    '#type'           => 'radios',
    '#title'          => t('Type / Sub Type'),
    '#options'        => $options,
    '#default_value'  => 'phenotype',
    '#description'    => t("Please specify a type of the trial."),
    '#attributes'     => array('style' => 'width:20px;'),
  );
  $form['trial_prop']['description'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please provide a description of a trial."),
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['trial_prop']['create_btn'] = array(
    '#name'       => 'create_btn',
    '#type'       => 'submit',
    '#value'      => t("Create a new trial"),
    '#attributes' => array('style' => 'width:165px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_trial_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trial-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['trial_prop']['cancel_create_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_create_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_trial_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-trial-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-trial-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_trial_add_form_submit';
  $form['#theme'] = 'bims_panel_add_on_trial_add_form';
  return $form;
}

/**
 * Returns the options for the trial list.
 *
 * @param BIMS_USER $bims_user
 *
 * @return array
 */
function _bims_get_opt_trial($bims_user) {
  $bims_program = $bims_user->getProgram();
  return $bims_program->getTrialAvailable($bims_user, 'phenotype');
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_add_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_add_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Create' button was clicked.
  if ($trigger_elem == 'create_btn') {

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO tables.
    $bc_project = new BIMS_CHADO_PROJECT($program_id);

    // Validates the trial name.
    $name = trim($form_state['values']['trial_prop']['name']);
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('trial_prop][name', "Invalid trial name : $error");
      return;
    }

    // Checks the trial name for duplication.
    $project = $bc_project->byTKey('project', array('name' => $prefix . $name));
    if ($project) {
      form_set_error('trial_prop][name', "The trial ($name) has been taken. Please choose other name.");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_trial_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Local variables.
  $bims_user    = getBIMS_USER();
  $program_id   = $bims_user->getProgramID();
  $crop_id      = $bims_user->getCropID();
  $crop         = BIMS_CROP::byKey(array('crop_id' => $crop_id));
  $bims_program = BIMS_PROGRAM::byID($program_id);

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

    // If the 'Create' button was clicked.
  if ($trigger_elem == 'create_btn') {

    // Trial properties.
    $name = trim($form_state['values']['trial_prop']['name']);
    $type = $form_state['values']['trial_prop']['type'];
    $desc = trim($form_state['values']['trial_prop']['description']);

    // Parses out the type and sub type.
    $tmp = explode('_', $type);
    $project_type = $tmp[0];
    $sub_type     = ($tmp[1]) ? $tmp[1] : '';

    // Adds a trial.
    $details = array(
      'name'              => $name,
      'type'              => 'TRIAL',
      'crop_id'           => $crop_id,
      'crop'              => $crop->getName(),
      'root_id'           => $program_id,
      'description'       => $desc,
      'owner_id'          => $bims_user->getUserID(),
      'contact'           => $bims_user->getName(),
      'access'            => $bims_program->getAccess(),
      'project_type'      => $project_type,
      'project_sub_type'  => $sub_type,
    );
    if ($bims_program->addTrial($details)) {
      drupal_set_message("$name was created successfully.");
    }
    else {
      drupal_set_message("Error : Failed to create a trial.", 'error');
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_trial_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_trial')));
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_trial_add_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds "trial".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['property']) . "</div>";

  // Adds "trial".
  $layout .= "<div style='width:100%;'>" . drupal_render($form['trial_prop']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
