<?php
/**
 * @file
 */
/**
 * Sample panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sample_form($form, &$form_state, $stock_id, $sample_id = NULL, $param = NULL) {

  // Local variables for form properties.
  $min_height = 420;

  // Gets BIMS_USER, BIMS_PROGRAM and BIMS_MVIEW_PHENOTYPE.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_sample');
  $bims_panel->init($form);

  // Gets the samples of the accession.
  $sample_objs = $bims_program->getSamples('phenotype', $stock_id, BIMS_OBJECT);
  $num_samples = sizeof($sample_objs);

  // Updates the sample_id. Assign the first sample_id if the sample_id is
  // empty and the accession has more than one sample.
  if ($num_samples && !$sample_id) {
    $sample_id = $sample_objs[0]->sample_id;
  }

  // Saves stock_id, sample_id and param.
  $form['stock_id'] = array(
    '#type' => 'value',
    '#value' => $stock_id,
  );
  $form['sample_id'] = array(
    '#type' => 'value',
    '#value' => $sample_id,
  );
  $form['program_id'] = array(
    '#type' => 'value',
    '#value' => $program_id,
  );
  $form['param'] = array(
    '#type' => 'value',
    '#value' => $param,
  );

  // Properties of the accession / samples.
  $accession_label = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
  $form['sample_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => "$accession_label / Samples Properties",
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
    '#attributes'  => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  _bims_panel_add_on_sample_form_get_accession_samples($form['sample_prop'], $bims_program, $accession_label, $stock_id, $sample_id, $sample_objs);

  // Adds the data section.
  $form['sample_data'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Property / Evaluation Data",
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
    '#attributes'  => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  if ($num_samples) {
    _bims_panel_add_on_sample_form_get_sample_data($form['sample_data'], $bims_program, $bims_user, $stock_id, $sample_id, $param);
  }
  else {
    $form['sample_data']['no_sample'] = array(
      '#markup' => "<div style=''><em>No sample found</em></div>",
    );
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-sample-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_sample_form_submit';
  $form['#theme'] = 'bims_panel_add_on_sample_form';
  return $form;
}

/**
 * Returns the accession / samples information.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_MVIEW_PHENOTYPE $mview_phenotype
 * @param string $accession_label
 * @param integer $stock_id
 * @param integer $sample_id
 * @param array $sample_objs
 */
function _bims_panel_add_on_sample_form_get_accession_samples(&$form, BIMS_PROGRAM $bims_program, $accession_label, $stock_id, $sample_id, $sample_objs = array()) {

  // Gets the BIMS columns.
  $bims_cols = $bims_program->getBIMSCols();

  // Gets all samples.
  $num_samples = sizeof($sample_objs);
  if ($num_samples == 0) {
    $num_samples = '<em>No sample</em>';
  }

  // Gets the accession.
  $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getNodeID()));
  $accession = $mview_stock->getStock($stock_id, BIMS_OBJECT);

  // Gets the accession properties.
  $cross_number = $accession->cross_number;
  $maternal     = $accession->maternal;
  $paternal     = $accession->paternal;

  // Populates the table.
  $rows = array();
  $rows []= array($accession_label, $accession->name);
  $rows []= array('Organism', $accession->genus . ' ' . $accession->species);
  $rows []= array('Type', $accession->type);
  if ($cross_number) {
    $rows []= array('Cross Number', $cross_number);
  }
  if ($maternal || $paternal) {
    $m = ($maternal) ? $maternal : '<em>N/A</em>';
    $f = ($paternal) ? $paternal : '<em>N/A</em>';
    $rows []= array('parents', "$m x $f");
  }
  $rows []= array('# of samples', $num_samples);

  // Creates the accession table.
  $table_vars = array(
    'header'      => array(array('data' => "$accession_label", 'colspan' => 2)),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $form['accession_table'] = array(
    '#markup' => theme('table', $table_vars),
  );

  // Adds the sample table if a sample exists.
  if (!empty($sample_objs)) {

    // Adds the DDL for samples.
    $options = array();
    foreach ($sample_objs as $sample_obj) {
      $label = $sample_obj->unique_id;
      $options[$sample_obj->sample_id] = $label;

      // Updates the sample ID.
      if (!$sample_id) {
        $sample_id = $sample_obj->sample_id;
      }
    }

    // Adds sample DDL.
    $form['sample_ddl'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#default_value'  => $sample_id,
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'       => array(
        'callback' => 'bims_panel_add_on_sample_form_ajax_callback',
        'wrapper'  => 'bims-panel-add-on-sample-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );

    // Gets and populates the table with sample properties.
    $rows = array();
    $sample = $bims_program->getSample('phenotype', $sample_id);
    $rows []= array('Trial', $sample->dataset_name);
    $rows []= array($bims_cols['unique_id'], $sample->unique_id);
    $rows []= array($bims_cols['primary_order'], $sample->primary_order);
    $rows []= array($bims_cols['secondary_order'], $sample->secondary_order);

    // Creates the sample table.
    $table_vars = array(
      'header'      => array(array('data' => 'Sample', 'colspan' => 2)),
      'rows'        => $rows,
      'attributes'  => array(),
    );
    $form['sample_table'] = array(
      '#markup' => theme('table', $table_vars),
    );
  }
}

/**
 * Returns the sample information.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_USER $bims_user
 * @param integer $stock_id
 * @param integer $sample_id
 * @param string $param
 *
 * @return string
 */
function _bims_panel_add_on_sample_form_get_sample_data(&$form, BIMS_PROGRAM $bims_program, BIMS_USER $bims_user, $stock_id, $sample_id, $param = NULL) {

  // Gets the sample data.
  $sample_data = $bims_program->getSample('phenotype', $sample_id, BIMS_ASSOC);

  // Sets the flag for edit.
  $edit_flag = FALSE;
  if ($param) {
    if ($bims_user->canEdit()) {
      $edit_flag = TRUE;
    }
  }

  // Adds the sample propery table.
  $tables = '';
  $props = $bims_program->getProperties('sample', 'default_custom', BIMS_FIELD);
  if (!empty($props)) {
    $rows = array();
    foreach ($props as $field => $label) {
      $edit_link = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_sample\",\"Sample\",\"bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id/prop:$field\"); return (false);'><span class='bims_edit'>&nbsp;</span></a>";
      $value = '';
      if (array_key_exists($field, $sample_data)) {
        $value = $sample_data[$field];
      }
      $rows []= $edit_flag ? array($edit_link, $label, $value) : array($label, $value);
    }

    // Sets the headers.
    $header = array(
      array('data' => '', 'width' => 1),
      array('data' => 'Property', 'width' => 120),
      'Value',
    );
    if (!$edit_flag) {
      $header = array(array('data' => 'Property', 'width' => 120), 'Value');
    }
    $table_vars = array(
      'header'      => $header,
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%;'),
    );
    $tables .= theme('table', $table_vars);
  }

  // Adds the evaluation data table.
  $descriptors = $bims_program->getDescriptors(NULL, BIMS_OBJECT);
  if (!empty($descriptors)) {
    $rows = array();
    foreach ($descriptors as $descriptor_obj) {
      $field = 't' . $descriptor_obj->cvterm_id;
      $edit_link = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_sample\",\"Sample\",\"bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id/eval:$field\"); return (false);'><span class='bims_edit'>&nbsp;</span></a>";
      $value = '';
      if (array_key_exists($field, $sample_data)) {
        $value = $sample_data[$field];
      }
      $rows []= $edit_flag ? array($edit_link, $descriptor_obj->name, $value) : array($descriptor_obj->name, $value);
    }

    // Sets the headers.
    $header = array(
      array('data' => '', 'width' => 1),
      array('data' => 'Trait', 'width' => 120),
      'Value',
    );
    if (!$edit_flag) {
      $header = array(array('data' => 'Property', 'width' => 120), 'Value');
    }
    $table_vars = array(
      'header'      => $header,
      'rows'        => $rows,
      'attributes'  => array('style' => 'width:100%;'),
    );
    $tables .= theme('table', $table_vars);
  }
  if ($tables == '') {
    $tables = '<div><em><b>No data found for this sample</b></em></div>';
  }
  $form['data_point']['eval_data'] = array(
    '#markup' => "<div style='overflow-y:auto;width:100%;height:250px;'>$tables</div>",
  );

  // Adds edit section.
  if ($bims_user->canEdit()) {
    _bims_panel_add_on_sample_form_add_edit_section($form, $bims_program, $bims_user, $stock_id, $sample_id, $sample_data, $param);
  }
}

/**
 * Adds the edit data section.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_USER $bims_user
 * @param integer $stock_id
 * @param integer $sample_id
 * @param array $sample_data
 * @param string $param
 *
 * @return string
 */
function _bims_panel_add_on_sample_form_add_edit_section(&$form, BIMS_PROGRAM $bims_program, BIMS_USER $bims_user, $stock_id, $sample_id, $sample_data, $param) {

  // Adds the data edit section.
  $form['data_point']['fs'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Edit Data",
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
    '#attributes'  => array('style' => 'min-height:90px;'),
  );

  // Adds the form element for the data.
  if (!$param) {
    $form['data_point']['fs']['edit_mode_btn']= array(
      '#name'       => 'edit_mode_btn',
      '#type'       => 'submit',
      '#value'      => 'Edit Data',
      '#attributes' => array('style' => 'width:160px;'),
      '#ajax'   => array(
        'callback' => "bims_panel_add_on_sample_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-sample-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    return;
  }
  else if ($param == 'EDIT') {
    $form['data_point']['fs']['edit_data'] = array(
      '#markup' => "<div style='margin-bottom:15px;'><em>Please click a edit icon or 'Cancel' button.</em></div>",
    );

    // Adds 'Cancel' button.
    $form['data_point']['fs']['cancel_btn']= array(
      '#name'       => 'cancel_btn',
      '#type'       => 'submit',
      '#value'      => 'Cancel',
      '#attributes' => array('style' => 'width:160px;'),
      '#ajax'   => array(
        'callback' => "bims_panel_add_on_sample_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-sample-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    return;
  }

  // Edits the evaluation data.
  if (preg_match("/^eval:t(\d+)$/", $param, $matches)) {
    $cvterm_id  = $matches[1];
    $field      = "t$cvterm_id";
    $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($bims_program->getProgramID(), $cvterm_id);
    $format     = $descriptor->getFormat();
    $title      = $descriptor->getName();
    if ($descriptor->isNumeric()) {
      $form['data_point']['fs']['edit_data'] = array(
        '#title'          => $title,
        '#type'           => 'textfield',
        '#description'    => 'Please enter a new value',
        '#default_value'  => array_key_exists($field, $sample_data) ? $sample_data[$field] : '',
        '#attributes'     => array('style' => 'min-width:200px;'),
      );
    }
    if ($format == 'date') {
      $form['data_point']['fs']['edit_data'] = array(
        '#title'          => $title,
        '#type'           => 'textfield',
        '#description'    => 'Please enter a new date',
        '#default_value'  => array_key_exists($field, $sample_data) ? $sample_data[$field] : '',
        '#attributes'     => array('style' => 'width:200px;'),
      );
    }
    else if ($format == 'categorical') {
      $options = $descriptor->getOptCategorical(array('' => 'Leave as empty'));
      $form['data_point']['fs']['edit_data'] = array(
        '#title'          => $title,
        '#type'           => 'select',
        '#options'        => $options,
        '#description'    => 'Please pick a new value',
        '#default_value'  => array_key_exists($field, $sample_data) ? $sample_data[$field] : '',
        '#attributes'     => array('style' => 'min-width:150px;'),
      );
    }
    else if ($format == 'multicat') {
      $options = $descriptor->getOptCategorical();
      $default_value = preg_split("/:/", $sample_data[$field], -1, PREG_SPLIT_NO_EMPTY);
      $form['data_point']['fs']['edit_data'] = array(
        '#title'          => $title,
        '#type'           => 'checkboxes',
        '#options'        => $options,
        '#multiple'       => TRUE,
        '#description'    => 'Please choose values',
        '#default_value'  => $default_value,
        '#attributes'     => array('style' => 'backgroud-color:#EEEEEE;'),
      );
    }
    else if ($format == 'boolean') {
      $options = array('TRUE' => 'TRUE', 'FALSE' => 'FALSE', '' => 'Leave as empty');
      $form['data_point']['fs']['edit_data'] = array(
        '#title'          => $title,
        '#type'           => 'radios',
        '#options'        => $options,
        '#description'    => 'Please pick a new value',
        '#default_value'  => array_key_exists($field, $sample_data) ? $sample_data[$field] : '',
      );
    }
    else {
      $form['data_point']['fs']['edit_data'] = array(
        '#title'          => $title,
        '#type'           => 'textfield',
        '#description'    => 'Please enter a new value',
        '#default_value'  => array_key_exists($field, $sample_data) ? $sample_data[$field] : '',
        '#attributes'     => array('style' => 'width:100%'),
      );
    }
  }

  // Edits the sample default properties.
  else if (preg_match("/^prop:p(\d+)$/", $param, $matches)) {
    $cvterm_id  = $matches[1];
    $field      = "p$cvterm_id";
    $form['data_point']['fs']['edit_data'] = array(
      '#title'          => $props[$field],
      '#type'           => 'textfield',
      '#description'    => 'Please enter a new value',
      '#default_value'  => array_key_exists($field, $sample_data) ? $sample_data[$field] : '',
    );
  }

  // Edits the sample custom properties.
  else {
    $field  = preg_replace("/^prop:/", '', $param);
    $cvterm = MCL_CHADO_CVTERM::getCvterm('SITE_CV', $field);
    if ($cvterm) {
      $cvterm_id = $cvterm->getCvtermID();
      $form['data_point']['fs']['edit_data'] = array(
        '#title'          => $field,
        '#type'           => 'textfield',
        '#description'    => 'Please enter a new value',
        '#default_value'  => array_key_exists($field, $sample_data) ? $sample_data[$field] : '',
      );
    }
    else {
      $form['data_point']['fs']['no_data'] = array(
        '#markup' => '<div><em>Error : propery cannot be found</em></div>',
      );
    }
  }

  // Adds 'Update' button.
  $form['data_point']['fs']['edit_btn']= array(
    '#name'       => 'edit_btn',
    '#type'       => 'submit',
    '#value'      => 'Update',
    '#disabled' => TRUE,
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_sample_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-sample-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Adds 'Cancel' button.
  $form['data_point']['fs']['cancel_btn']= array(
    '#name'       => 'cancel_btn',
    '#type'       => 'submit',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:160px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_sample_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-sample-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sample_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'program_list' was changed.
  if ($trigger_elem == 'sample_prop[sample_ddl]') {

    // Gets stock_id of the accesion and sample.
    $stock_id   = $form_state['values']['stock_id'];
    $sample_id  = $form_state['values']['sample_prop']['sample_ddl'];

    // Updates the panels.
    $url = "bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sample', 'Sample', $url)));
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sample_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Update' was clicked.
  if ($trigger_elem == 'edit_btn') {
    $program_id = $form_state['values']['program_id'];
    $param      = $form_state['values']['param'];
    $value      = trim($form_state['values']['sample_data']['data_point']['fs']['edit_data']);

    // Updates the data.
    $err_msg = _bims_panel_add_on_sample_form_validate_data($program_id, $param, $value);
    if ($err_msg) {
      form_set_error('sample_data][data_point][fs][edit_data', t("Error : $err_msg."));
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_sample_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_mode_btn') {
    $stock_id   = $form_state['values']['stock_id'];
    $sample_id  = $form_state['values']['sample_id'];


    // Updates the panels.
    $url = "bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id/EDIT";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sample', 'Sample', $url)));
  }

  // If the 'Update' was clicked.
  else if ($trigger_elem == 'edit_btn') {
    $stock_id   = $form_state['values']['stock_id'];
    $sample_id  = $form_state['values']['sample_id'];
    $param      = $form_state['values']['param'];
    $value      = trim($form_state['values']['sample_data']['data_point']['fs']['edit_data']);

    // Updates the data.
    _bims_panel_add_on_sample_form_update_data($form, $param, $value);

    // Updates the panels.
    $url = "bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sample', 'Sample', $url)));
  }

  // If the 'Update' was clicked.
  else if ($trigger_elem == 'cancel_btn') {
    $stock_id   = $form_state['values']['stock_id'];
    $sample_id  = $form_state['values']['sample_id'];

    // Updates the panels.
    $url = "bims/load_main_panel/bims_panel_add_on_sample/$stock_id/$sample_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_sample', 'Sample', $url)));
  }
}

/**
 * Updates the data.
 *
 * @param integer $program_id
 * @param string $param
 * @param string $value
 *
 * @return string $err_msg
 */
function _bims_panel_add_on_sample_form_validate_data($program_id, $param, $value) {
  $err_msg   = '';
  $cvterm_id = '';
  $field     = '';

  // Updates cvterm ID and field name.
  if (preg_match("/^eval:t(\d+)$/", $param, $matches)) {
    $cvterm_id  = $matches[1];
    $field      = "t$cvterm_id";
    $descriptor = BIMS_MVIEW_DESCRIPTOR::byID($program_id, $cvterm_id);
    $format     = $descriptor->getFormat();
    $title      = $descriptor->getName();
    if ($descriptor->isNumeric()) {

    }
  }
  else if (preg_match("/^prop:p(\d+)$/", $param, $matches)) {
    $cvterm_id  = $matches[1];
    $field      = "p$cvterm_id";
  }
  else {
    $field  = preg_replace("/^prop:/", '', $param);
    $cvterm = MCL_CHADO_CVTERM::getCvterm('SITE_CV', $field);
    if ($cvterm) {
      $cvterm_id = $cvterm->getCvtermID();
    }
  }




  return $err_msg;
}

/**
 * Updates the data.
 *
 * @param array $form_state
 * @param string $param
 * @param string $value
 */
function _bims_panel_add_on_sample_form_update_data($form, $param, $value) {
  $cvterm_id = '';
  $field     = '';

  // Updates cvterm ID and field name.
  if (preg_match("/^eval:t(\d+)$/", $param, $matches)) {
    $cvterm_id  = $matches[1];
    $field      = "t$cvterm_id";
  }
  else if (preg_match("/^prop:p(\d+)$/", $param, $matches)) {
    $cvterm_id  = $matches[1];
    $field      = "p$cvterm_id";
  }
  else {
    $field  = preg_replace("/^prop:/", '', $param);
    $cvterm = MCL_CHADO_CVTERM::getCvterm('SITE_CV', $field);
    if ($cvterm) {
      $cvterm_id = $cvterm->getCvtermID();
    }
  }

  // Updates the data if the data has been changed.
  if ($cvterm_id && $field) {

    // Gets the old value and compare it.
    $old_value = trim($form['sample_data']['data_point']['fs']['edit_data']['#default_value']);
    if ($old_value != $value) {



    }
  }
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_sample_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds sample properties.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['sample_prop']) . "</div>";

  // Adds sample data.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['sample_data']) . "</div><br clear='all'>";
  $layout .= drupal_render_children($form);
  return $layout;
}