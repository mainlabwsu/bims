<?php
/**
 * @file
 */
/**
 * Accession edit panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_edit_form($form, &$form_state, $stock_id) {

  // Gets BIMS_USER and BIMS_CROP.
  $bims_user    = getBIMS_USER();
  $bims_crop    = $bims_user->getCrop();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets BIMS_MVIEW_STOCK.
  $mview_stock  = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));
  $stock_obj    = $mview_stock->getStock($stock_id, BIMS_OBJECT);

  // Gets the label for accession.
  $accession_l = $bims_program->getBIMSLabel('accession');

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves the stock object.
  $form['stock_obj'] = array(
    '#type'   => 'value',
    '#value'  => $stock_obj,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_accession_edit');
  $bims_panel->init($form);

  // Properties of the accession.
  $form['accession_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => "$accession_l Properties",
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['accession_prop']['name'] = array(
    '#type'           => 'textfield',
    '#title'          => $accession_l,
    '#description'    => t("Please provide an accession. Please avoid using any special characters excepts dash, slash and underscore"),
    '#default_value'  => $stock_obj->name,
    '#attributes'     => array('style' => 'width:400px;'),
  );

  // Gets the alias.
  $mview_alias = new BIMS_MVIEW_ALIAS(array('node_id' => $program_id));
  $aliases = $mview_alias->getAlias('accession', $stock_obj->stock_id, BIMS_STRING, ' / ');
  $alias = ($aliases) ? $aliases : '';
  $form['accession_prop']['alias'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Alias'),
    '#description'    => t("Please provide aliases"),
    '#default_value'  => $alias,
    '#description'    => t("Please provide aliases. Please separates by backslash for multple aliases."),
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['accession_prop']['type_id'] = array(
    '#type'           => 'select',
    '#title'          => t('Type'),
    '#options'        => MCL_DATA_VALID_TYPE::getOptions('stock_type'),
    '#default_value'  => $stock_obj->type_id,
    '#description'    => t("Please choose a type of the accession"),
    '#attributes'     => array('style' => 'width:250px;'),
  );
  $opt_organism = $bims_crop->getOrganisms(BIMS_OPTION);
  if (!array_key_exists($stock_obj->organism_id, $opt_organism)) {
    $organism = MCL_CHADO_ORGANISM::byID($stock_obj->organism_id);
    $opt_organism[$stock_obj->organism_id] = $organism->getGenus() . ' ' . $organism->getSpecies();
  }
  $form['accession_prop']['organism_id'] = array(
    '#type'           => 'select',
    '#title'          => t('Organism'),
    '#options'        => $opt_organism,
    '#default_value'  => $stock_obj->organism_id,
    '#description'    => t("Please choose an organism of the accession"),
    '#attributes'     => array('style' => 'width:250px;'),
  );
  $form['accession_prop']['m_stock'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Maternal Parent'),
    '#default_value'      => $stock_obj->maternal,
    '#description'        => t("Please type the name of maternal parent"),
    '#attributes'         => array('style' => 'width:250px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id"
  );
  $form['accession_prop']['p_stock'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Paternal Parent'),
    '#default_value'      => $stock_obj->paternal,
    '#description'        => t("Please type the name of paternal parent"),
    '#attributes'         => array('style' => 'width:250px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id"
  );
  $form['accession_prop']['edit_accession_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'edit_accession_btn',
    '#value'      => 'Update',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_accession_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-accession-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['accession_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_accession_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-accession-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-accession-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_accession_edit_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_edit_form_validate($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the label for accession.
  $accession_l = $bims_program->getBIMSLabel('accession');

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_accession_btn') {

    // Gets the value from the form.
    $stock_obj    = $form_state['values']['stock_obj'];
    $name         = trim($form_state['values']['accession_prop']['name']);
    $organism_id  = $form_state['values']['accession_prop']['organism_id'];
    $maternal     = trim($form_state['values']['accession_prop']['m_stock']);
    $paternal     = trim($form_state['values']['accession_prop']['p_stock']);

    // Validates the accession name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('accession_prop][name', "Invalid $accession_l name : $error");
      return;
    }

    // Gets BIMS_CHADO_ACCESSION.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);

    // Checks the accession name for duplication.
    if ($stock_obj->name != $name) {
      $accession = $bc_accession->getAccessionByName($name);
      if ($accession) {
        form_set_error('accession_prop][name', "The name ($name) has been taken. Please choose other name.");
      }
    }

    // Checks the parents.
    $old_name = $stock_obj->name;
    if ($maternal) {
      if ($maternal == $old_name) {
        form_set_error('accession_prop][m_stock', "Please not use old name '$old_name'.");
      }
      if (!$bc_accession->getAccessionByName($maternal)) {
        form_set_error('accession_prop][m_stock', "$maternal does not exist.");
      }
    }
    if ($paternal) {
      if ($paternal == $old_name) {
        form_set_error('accession_prop][p_stock', "Please not use old name '$old_name'.");
      }
      if (!$bc_accession->getAccessionByName($paternal)) {
        form_set_error('accession_prop][p_stock', "$paternal does not exist.");
      }
    }
  }
}

/**
 * Updates the accession.
 *
 * @param array $form_state
 * @param object $stock_obj
 *
 * @return boolean
 */
function _bims_panel_add_on_accession_edit_form_edit_accession($form_state, $stock_obj) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $stock_id     = $stock_obj->stock_id;

  // Gets the properties of the accession.
  $name         = trim($form_state['values']['accession_prop']['name']);
  $alias        = trim($form_state['values']['accession_prop']['alias']);
  $organism_id  = $form_state['values']['accession_prop']['organism_id'];
  $type_id      = $form_state['values']['accession_prop']['type_id'];
  $maternal     = trim($form_state['values']['accession_prop']['m_stock']);
  $paternal     = trim($form_state['values']['accession_prop']['p_stock']);

  // Gets BIMS_CHADO_ACCESSION.
  $bc_accession = new BIMS_CHADO_ACCESSION($program_id);

  // Prepares for updating the accession.
  $organism = MCL_CHADO_ORGANISM::byID($organism_id);
  $type     = MCL_CHADO_CVTERM::byID($type_id);

  // Adds the properties.
  $details = array(
    'stock_id'    => $stock_id,
    'uniquename'  => $bims_program->getPrefix() . $name,
    'name'        => $name,
    'organism_id' => $organism_id,
    'genus'       => $organism->getGenus(),
    'species'     => $organism->getSpecies(),
    'type_id'     => $type_id,
    'type'        => $type->getName(),
    'alias'       => $alias,
    'maternal'    => $maternal,
    'paternal'    => $paternal,
  );

  // Adds the parents.
  $m_accession  = $bc_accession->byName($maternal);
  $p_accession  = $bc_accession->byName($paternal);
  if ($m_accession) {
    $details['stock_id_m'] = $m_accession->stock_id;
  }
  if ($p_accession) {
    $details['stock_id_p'] = $p_accession->stock_id;
  }

  // Updates the stock in BIMS.
  if (!$bc_accession->updateAccession($details)) {
    drupal_set_message("Error : Failed to update the BIMS_CHADO_ACCESSION", 'error');
    return FALSE;
  }

  // Updates the stock in the mview.
  $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
  if (!$mview_stock->updateMView(FALSE, 'stock', array($stock_id), TRUE)) {
    drupal_set_message("Error : Failed to update the mviews", 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_accession';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets the stock object.
  $stock_obj = $form_state['values']['stock_obj'];

  // If "Edit" was clicked.
  if ($trigger_elem == 'edit_accession_btn') {

    $transaction = db_transaction();
    try {

      // Edits the accession.
      if (!_bims_panel_add_on_accession_edit_form_edit_accession($form_state, $stock_obj)) {
        throw new Exception("Error : Failed to update the accession");
      }
      $load_primary_panel .= '/' . $stock_obj->stock_id;
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Removes the panel.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_accession_edit')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}