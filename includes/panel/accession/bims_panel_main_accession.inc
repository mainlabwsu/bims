<?php
/**
 * @file
 */
/**
 * accession panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_accession_form($form, &$form_state, $stock_id = NULL) {

  // Local variables for form properties.
  $min_height = 400;

  // Gets BIMS_MVIEW_STOCK.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $mview_stock  = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
  $mview_image  = new BIMS_MVIEW_IMAGE(array('node_id' => $program_id));

  // Gets the label for accession.
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
  $accession_lc = strtolower($accession);

  // Gets the stock object.
  $stock_obj  = NULL;
  if (isset($form_state['values']['accession_list']['selection'])) {
    $stock_id = $form_state['values']['accession_list']['selection'];
  }
  if ($stock_id) {
    $stock_obj = $mview_stock->getStock($stock_id, BIMS_OBJECT);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_accession');
  $bims_panel->init($form);

  // Adds the admin menu.
  $form['accession_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => "$accession Admin Menu",
    '#attributes'   => array(),
  );

  // Adds the "Add" button.
  $url = "bims/load_main_panel/bims_panel_add_on_accession_add";
  $form['accession_admin']['menu']['add_btn'] = array(
    '#name'       => 'add_btn',
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_accession_add', 'Add $accession', '$url'); return (false);",
    ),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_accession_delete";
  $form['accession_admin']['menu']['mass_delete_btn'] = array(
    '#name'       => 'mass_delete_btn',
    '#type'       => 'button',
    '#value'      => 'Delete',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_accession_delete', 'Delete $accession', '$url'); return (false);",
    ),
  );
  $form['accession_admin']['menu']['accession'] = array(
    '#type' => 'value',
    '#value' => $accession_lc,
  );
  $form['accession_admin']['menu']['#theme'] = 'bims_panel_main_accession_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['accession_admin']);

  // Lists the all accession.
  $form['accession_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => $accession,
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // accession selection.
  $options = _bims_panel_main_accession_form_get_opt_accession($bims_program, $mview_stock);
  $num_accesion = sizeof($options);
  if (!$num_accesion) {
    $form['accession_list']['no_accession'] = array(
      '#markup' => "No $accession_lc is associated with the current program.",
    );
  }
  else {

    // Adds 'Find' textfield.
    if ($num_accesion > BIMS_DDL_MIN_AUTO_FILL) {
      $form['accession_list']['accession_auto'] = array(
        '#type'               => 'textfield',
        '#attributes'         => array('style' => 'width:250px;'),
        '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id",
      );
      $form['accession_list']['accession_details_btn'] = array(
        '#type'       => 'submit',
        '#name'       => 'accession_details_btn',
        '#value'      => 'Details',
        '#attributes' => array('style' => 'width:100px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_accession_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-accession-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds the accession list.
    if ($num_accesion > BIMS_DDL_MAX_LIST) {
      $msg = "You have <b>$num_accesion</b> $accession" . 's.<br />' .
        "The maximum  number of $accession listed in a dropdown list is <b>" . BIMS_DDL_MAX_LIST . "</b>.<br />
        Please use the auto-complete textbox above to find a $accession.";
      $form['accession_list']['selection'] = array(
        '#markup' => "<div>$msg</div>",
      );
    }
    else {
      $form['accession_list']['selection'] = array(
        '#type'           => 'select',
        '#options'        => $options,
        '#size'           => 14,
        '#default_value'  => $stock_id,
        '#prefix'         => "You have <b>$num_accesion</b> $accession" . 's',
        '#description'    => "Please choose an $accession_lc to view the details.",
        '#attributes'     => array('style' => 'width:100%;'),
        '#ajax'           => array(
          'callback' => "bims_panel_main_accession_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-accession-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds 'Download Sites' button.
    $accession_lc .= 's';
    $suffix = "
      <div>Please click the button to download a list of $accession_lc
      and their phenotyped locations.</div>
    ";
    $form['accession_list']['download_stock_sites_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'download_stock_sites_btn',
      '#value'      => 'Download all sites',
      '#attributes' => array('style' => 'width:150px;margin-top:5px;'),
      '#suffix'     => $suffix,
      '#ajax'       => array(
        'callback' => "bims_panel_main_accession_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-accession-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['accession_list']['#theme'] = 'bims_panel_main_accession_form_accession_list';

  // Accession details.
  $form['accession_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => "$accession Details",
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['accession_details']['table']['stock_obj'] = array(
    '#type'   => 'value',
    '#value'  => $stock_obj,
  );
  $form['accession_details']['table']['program_id'] = array(
    '#type'   => 'value',
    '#value'  => $program_id,
  );
  $num_image = ($stock_obj) ? $mview_image->getNumImage('stock', $stock_obj->stock_id) : 0;
  $form['accession_details']['table']['num_image'] = array(
    '#type'   => 'value',
    '#value'  => $num_image,
  );

  // Adds data buttons.
  if ($stock_obj && $stock_obj->num_sample) {

    // Adds 'view' button.
    if ($stock_obj->num_breeding) {
      $form['accession_details']['table']['view_btn'] = array(
        '#type'       => 'button',
        '#value'      => 'View',
        '#name'       => 'view_btn',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_accession_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-accession-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }
    $form['accession_details']['table']['prop'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Properties',
      '#attributes'   => array('style' => 'width:90%;'),
    );
    $form['accession_details']['table']['prop']['table'] = array(
      '#markup' => '<em>No propery found</em>',
    );
  }

  // Adds report button.
  if ($stock_obj) {
    $form['accession_details']['table']['report_btn'] = array(
      '#name'       => 'report_btn',
      '#type'       => 'button',
      '#value'      => 'Generate Report',
      '#attributes' => array('style' => 'width:150px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_accession_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-accession-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds action buttons.
  if ($stock_obj && bims_can_admin_action($bims_user)) {

    // Adds 'Edit' button.
    if (!$stock_obj->chado) {
      $form['accession_details']['table']['edit_btn'] = array(
        '#name'       => 'edit_btn',
        '#type'       => 'button',
        '#value'      => 'Edit',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_accession_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-accession-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds 'Delete' button.
    $form['accession_details']['table']['delete_btn'] = array(
      '#name'       => 'delete_btn',
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#disabled'   => !$bims_user->canEdit(),
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_accession_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-accession-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  if ($stock_obj) {
    $form['accession_details']['table']['prop'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Properties',
      '#attributes'   => array('style' => 'width:90%;'),
    );
    $form['accession_details']['table']['prop']['table'] = array(
      '#markup' => '<em>No propery found</em>',
    );
  }
  $form['accession_details']['table']['#theme'] = 'bims_panel_main_accession_form_accession_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-accession-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_accession_form_submit';
  return $form;
}

/**
 * Gets the options for the accessions.
 *
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_MVIEW_STOCK $mview_stock
 *
 * @return array
 */
function _bims_panel_main_accession_form_get_opt_accession(BIMS_PROGRAM $bims_program, BIMS_MVIEW_STOCK $mview_stock) {

  // Gets the accessions.
  $stocks       = $mview_stock->getStocks(BIMS_OBJECT);
  $public_chado = $bims_program->isPublicChado();
  $opt_stock    = array();
  foreach ($stocks as $stock) {
    $star = '';
    if (!$public_chado) {
      $star = $stock->chado ? ' *' : '';
    }
    $opt_stock[$stock->stock_id] = $stock->name . $star;
  }
  return $opt_stock;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_accession_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $accession    = $bims_program->getBIMSLabel('accession');

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Download all sites" was clicked.
  if ($trigger_elem == 'download_stock_sites_btn') {
    $url = "bims/download_stock_sites/$program_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
  }
  else {

    // Gets stock_id from the dropdownlist.
    $stock_id = NULL;
    if (array_key_exists('selection', $form_state['values']['accession_list'])) {
      $stock_id = $form_state['values']['accession_list']['selection'];
    }

    // Gets the stock ID from the form.
    if (!$stock_id) {
      $stock_obj = $form_state['values']['accession_details']['table']['stock_obj'];
      $stock_id = $stock_obj->stock_id;
    }
    if ($stock_id) {

      // If 'View' was clicked.
      if ($trigger_elem == 'view_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_phenotype_accession/$stock_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_phenotype_accession', "Phenotype by $accession", $url)));
      }

      // If "Edit" was clicked.
      else if ($trigger_elem == 'edit_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_accession_edit/$stock_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_accession_edit', "Edit $accession", $url)));
      }

      // If "Delete" was clicked.
      else if ($trigger_elem == 'delete_btn') {

        // Deletes the accession.
        if (_bims_panel_main_accession_form_delete_accession($program_id, $stock_id)) {
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_accession')));
        }
      }

      // If "Report" was clicked.
      else if ($trigger_elem == 'report_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_accession_report/$stock_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_accession_report', "Report", $url)));
      }
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Deletes the accession.
 *
 * @param integer $program_id
 * @param integer $stock_id
 *
 * @return boolean
 */
function _bims_panel_main_accession_form_delete_accession($program_id, $stock_id) {

  $transaction = db_transaction();
  try {

    // Deletes the accesion.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
    if (!$bc_accession->deleteAccessions(array($stock_id))) {
      throw new Exception("Error : Failed to delete the accession");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_accession_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_accession_form_submit($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_user->getProgramID();

  // Gets the accession label.
  $accession_l = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Details" was clicked.
  if ($trigger_elem == 'accession_details_btn') {

    // Gets the accession.
    $name = trim($form_state['values']['accession_list']['accession_auto']);
    $mview_stock  = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
    $stock_id = $mview_stock->getStockIDByName($name);
    if ($stock_id) {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_accession/$stock_id")));
    }
    else {
      drupal_set_message("$accession_l '$name' does not exists.");
    }
  }
}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_accession_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $accession = $element['accession']['#value'];
  $rows[] = array(drupal_render($element['add_btn']), "Add a new $accession / property.");
//  $rows[] = array(drupal_render($element['mass_delete_btn']), "Delete Multiple $accession.");
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the accession list.
 *
 * @param $variables
 */
function theme_bims_panel_main_accession_form_accession_list($variables) {
  $element = $variables['element'];

  // Adds the no accession message.
  $layout = '';
  if (array_key_exists('no_accession', $element)) {
    $layout .= "<div>" . drupal_render($element['no_accession']) . "</div>";
  }
  else {
    if (array_key_exists('accession_auto', $element)) {
      $layout .= '<div style="float:left;width:40px;margin-top:8px;"><em><b>Find</b></em></div>' .
        ' <div style="float:left;">' . drupal_render($element['accession_auto']) .
        '</div><div style="float:left;margin-left:10px;">' . drupal_render($element['accession_details_btn']) . '</div><br clear="all" />';
    }
    $layout .= '<div>' . drupal_render($element['selection']) . '</div>';
    $layout .= '<div>' . drupal_render($element['download_stock_sites_btn']) . '</div>';
  }
  return $layout;
}

/**
 * Theme function for the accession information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_accession_form_accession_table($variables) {
  $element = $variables['element'];

  // Gets stock and image objects.
  $program_id = $element['program_id']['#value'];
  $stock_obj  = $element['stock_obj']['#value'];
  $num_image  = $element['num_image']['#value'];

  // Gets BIMS_MVIEW_STOCK.
  $bm_stock = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));

  // Initializes the properties.
  $name         = '--';
  $alias        = '';
  $grin_id      = '';
  $genus        = '--';
  $species      = '';
  $cross_number = '';
  $parents      = '--';
  $num_samples  = '--';
  $image_row    = '--';
  $dp_row       = '--';
  $report       = '--';
  $data_row     = '';
  $mutation     = '';
  $selfing      = '';
  $location     = '';
  $comments     = '';
  $properties   = '';
  $actions      = '';

  // Shows the current available types for an accession.
  $accession_types = MCL_DATA_VALID_TYPE::getOptions('stock_type');
  $type = '<SELECT style="width:250px;">';
  foreach ((array)$accession_types as $accession_type) {
    $type .= "<OPTION>$accession_type</OPTION>";
  }
  $type .= '</SELECT>';

  // Updates the properites.
  $data_point_arr = array();
  if ($stock_obj) {
    $stock_id     = $stock_obj->stock_id;
    $name         = $stock_obj->name;
    $type         = $stock_obj->type;
    $genus        = $stock_obj->genus;
    $species      = $stock_obj->species;
    $grin_id      = $stock_obj->grin_id;
    $num_samples  = $stock_obj->num_sample;
    $cross_number = $stock_obj->cross_number;

    // Updates the parent row.
    if (!$stock_obj->maternal && !$stock_obj->paternal) {
      $parents = '<em>N/A</em>';
    }
    else {
      $maternal = ($stock_obj->maternal) ? $stock_obj->maternal : '<em>N/A</em>';
      $paternal = ($stock_obj->paternal) ? $stock_obj->paternal : '<em>N/A</em>';
      $parents = "$maternal x $paternal";
    }

    // Adds mutation / selfing parent.
    if ($stock_obj->selfing) {
      $selfing = $stock_obj->selfing;
    }
    if ($stock_obj->mutation) {
      $mutation = $stock_obj->mutation;
    }

    // Adds the locations.
    $locations = $bm_stock->getStockLocations($stock_id, BIMS_OPTION);
    if (!empty($locations)) {
      $height = sizeof($locations) > 1 ? 40 : 20;
      $loc_list = implode("\n", $locations);
      $location = "<textarea style='height:$height'px;width:100px;'>$loc_list</textarea>";
    }

    // Updates the alias.
    $mview_alias = new BIMS_MVIEW_ALIAS(array('node_id' => $program_id));
    $aliases = $mview_alias->getAlias('accession', $stock_obj->stock_id, BIMS_STRING, ' / ');
    if ($aliases) {
      $alias = $aliases;
    }

    // Adds 'Image' link.
    $image_row = '';
    if ($num_image) {
      $url =  'bims/load_main_panel/bims_panel_add_on_accession_image/' . $stock_obj->stock_id;
      $image_row = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_accession_image\",\"Photos\",\"$url\"); return (false);'>[View $num_image photos]</em></a>";
    }

    // Adds 'View' buttons.
    if (array_key_exists('view_btn', $element)) {
      $data_row = drupal_render($element['view_btn']);
    }

    // Adds the properties.
    $rows = array();
    $bims_program = BIMS_PROGRAM::byID($stock_obj->node_id);
    $props = $bims_program->getProperties('accession', 'details', BIMS_FIELD);
    if (!empty($props)) {
      foreach ($props as $field => $cvterm_name) {
        if (property_exists($stock_obj, $field)) {
          $value = $stock_obj->{$field};
          if ($value) {
            $rows []= array($cvterm_name, $value);
          }
        }
      }
    }
    if (!empty($rows)) {
      $table_vars = array(
        'header'      => array(array('data' => 'name', 'width' => 100), 'value'),
        'rows'        => $rows,
        'attributes'  => array(),
      );
      $element['prop']['table']['#markup'] = theme('table', $table_vars);
      $properties = drupal_render($element['prop']);
    }

    // Adds the report button.
    $report = drupal_render($element['report_btn']);

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }

    // Adds the comments.
    if ($stock_obj->comments) {
      $comments = '<textarea style="width:100%;" rows=2 READONLY>' . $stock_obj->comments . '</textarea>';
    }

    // Gets all data points.
    if ($stock_obj->num_breeding) {
      $data_point_arr []= 'Phenotype : ' . $stock_obj->num_breeding;
    }
    if ($stock_obj->num_ssr) {
      $data_point_arr []= 'SSR : ' . $stock_obj->num_ssr;
    }
    if ($stock_obj->num_snp) {
      $data_point_arr []= 'SNP : ' . $stock_obj->num_snp;
    }
  }

  // Updates the data point row.
  if (empty($data_point_arr)) {
    $dp_row = '<em>No associated data</em>';
  }
  else {
    $dp_row = '<textarea style="width:100%;" rows=2 READONLY>' . implode("\n", $data_point_arr) . '</textarea>';
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'width' => '100'), $name);
  if ($alias) {
    $rows[] = array('Alias', $alias);
  }
  if ($grin_id) {
    $rows[] = array('GRIN ID', $grin_id);
  }
  $rows[] = array('Type', $type);
  $rows[] = array('Organism', "$genus $species");
  $rows[] = array('Parents', $parents);
  if ($mutation) {
    $rows[] = array('Mutation parent', $mutation);
  }
  if ($selfing) {
    $rows[] = array('Selfing parent', $selfing);
  }
  if ($cross_number) {
    $rows[] = array('cross_number', $cross_number);
  }
  $rows[] = array('# data points', $dp_row);
  if ($location) {
    $rows[] = array('Locations', $location);
  }
  if ($image_row) {
    $rows[] = array('Photos', $image_row);
  }
  if ($comments) {
    $rows[] = array('Comments', $comments);
  }
  if ($properties) {
    $rows[] = array('Properties', $properties);
  }
  if ($data_row) {
    $rows[] = array('Statistics / Data', $data_row);
  }
  //$rows[] = array('Report', $report);
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_accession_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('accession_admin', $form)) {
    $layout .= "<div>" . drupal_render($form['accession_admin']) . "</div>";
  }

  // Adds "accession" list.
  $layout .= "<div style='float:left;width:44%;'>" . drupal_render($form['accession_list']) . "</div>";

  // Adds the accession info table.
  $layout .= "<div style='float:left;width:55%;margin-left:5px;'>" . drupal_render($form['accession_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
