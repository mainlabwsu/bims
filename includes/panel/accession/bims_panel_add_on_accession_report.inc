<?php
/**
 * @file
 */
/**
 * Accession report panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_report_form($form, &$form_state, $type, $id) {

  // Local variables for form properties.
  $min_height = 500;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_accession_report');
  $bims_panel->init($form);

  // Gets the accession.
  if ($type == 'stock') {
    $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));
    $accession = $mview_stock->getStock($stock_id, BIMS_OBJECT);
    $name = $accession->name;
  }
  else if ($type == 'list') {

  }

  // Adds accession table.
  $form['accession'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Generate Report',
  );


  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-accession-report-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_accession_report_form_submit';
  $form['#theme'] = 'bims_panel_add_on_accession_report_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_report_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_report_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_report_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_accession_report_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds accession info.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['accession']) . "</div>";

  // Adds image table.
 // $layout .= "<div style='width:100%;'>" . drupal_render($form['report_table']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}
