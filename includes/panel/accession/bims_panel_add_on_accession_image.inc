<?php
/**
 * @file
 */
/**
 * Accession image panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_image_form($form, &$form_state, $stock_id) {

  // Local variables for form properties.
  $min_height = 500;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_accession_image');
  $bims_panel->init($form);

  // Gets the accession.
  $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));
  $accession = $mview_stock->getStock($stock_id, BIMS_OBJECT);
  $name = $accession->name;

  // Gets BIMS_MVIEW_IMAGE and BIMS_MVIEW_IMAGE_TAG.
  $mview_image      = new BIMS_MVIEW_IMAGE(array('node_id' => $bims_program->getProgramID()));
  $mview_image_tag  = new BIMS_MVIEW_IMAGE_TAG(array('node_id' => $bims_program->getProgramID()));

  // Adds accession table.
  $form['accession'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => $name,
  );

  // Gets all images associated with this accession.
  $images = $mview_image->getImages('stock', $stock_id);
  if (empty($images)) {
    $form['accession']['image_table'] = array(
      '#markup' => "<em>No photo found for $name</em>",
    );
  }
  else{

    // Adds accession table.
    _bims_panel_add_on_accession_image_form_get_image_table($form['accession'], $mview_image_tag, $images);

    // Displays photos.
    $form['accession_photo'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => FALSE,
      '#collapsible'  => FALSE,
      '#title'        => "Photos associated with $name",
      '#attributes'   => array('style' => 'height:430px;'),
    );
    _bims_panel_add_on_accession_image_form_show_image($form['accession_photo'], $bims_program, $images);
  }

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-accession-image-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_accession_image_form_submit';
  $form['#theme'] = 'bims_panel_add_on_accession_image_form';
  return $form;
}

/**
 * Adds the image table.
 *
 * @param array $form
 * @param BIMS_MVIEW_IMAGE_TAG $mview_image_tag
 * @param array $images
 */
function _bims_panel_add_on_accession_image_form_get_image_table(&$form, BIMS_MVIEW_IMAGE_TAG $mview_image_tag, $images) {

  // Adds tags selection.
  $options = $mview_image_tag->getImageTags('stock', BIMS_OPTION);
  $num_options = sizeof($options);
  $size = ($num_options > 5) ? 5 : $num_options;
  $form['crop']['manage_organism']['delete_org_list'] = array(
    '#type'       => 'select',
    '#title'      => "Photo Tags",
    '#options'    => $options,
    '#size'       => $size,
    '#multiple'   => TRUE,
    '#attributes' => array('style' => 'width:200px;'),
  );



}

/**
 * Displays the photos.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param array $images
 */
function _bims_panel_add_on_accession_image_form_show_image(&$form, $bims_program, $images) {

  // Adds the parameters for the image slider.
  $params = array();
  drupal_add_js(array('bims_img_slider_params' => json_encode($params)), 'setting');

  // Prepares the image container.
  $div_image = '<div data-u="slides" style="cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width: 600px; height: 300px;">';
  foreach ($images as $image) {
    $photo_path = $bims_program->getPath('photo');
    $filepath   = "$photo_path/" . $image->filename;
    $filepath_t = "$photo_path/t-" . $image->filename;
    if (file_exists($filepath) && file_exists($filepath_t) ) {
      $div_image .= "<div><img data-u='image' src='$filepath' />";
      $div_image .= "<img data-u='thumb' src='$filepath_t' /></div>";
    }
  }
  $div_image .= '</div>';

  // Adds thumbnail navigator.
  $div_thumb = "
    <div data-u='thumbnavigator' class='bims-image-thumb' style='width: 600px; height: 100px; left:0px; bottom: -100px;'>
      <div data-u='slides'>
        <div data-u='prototype' class='p' style='width:200px;height:100px;'>
          <div class=w>
            <div data-u='thumbnailtemplate' class='t'></div>
          </div>
          <div class=c></div>
        </div>
      </div>
    </div>
  ";

  // Adds the image container.
  $style = 'position: relative; top: 0px; left: 0px; width: 600px; height: 300px';
  $form['image_container'] = array(
    '#markup' => "<div id='bims_img_slider_accession' style='$style'>$div_image $div_thumb</div>",
  );

}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_image_form_ajax_callback($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_image_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_image_form_submit($form, &$form_state) {}

/**
 * Theme function for the list options.
 *
 * @param $variables

function theme_bims_panel_add_on_accession_image_form_list_options($variables) {
  $element = $variables['element'];

   // List name.
  $layout = "<div style='float:left;width:40%;'>" . drupal_render($element['list_name']) . "</div>";

  // Adds the trait stats.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;margin-top:-8px;'>" . drupal_render($element['list_desc']) . "</div>";
  $layout .= "<div style=''>" . drupal_render($element['save_list_btn']) . "</div>";
  return $layout;
} */

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_accession_image_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds accession info.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['accession']) . "</div>";

  // Adds image table.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['image_table']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}
