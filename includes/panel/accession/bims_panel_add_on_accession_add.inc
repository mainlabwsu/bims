<?php
/**
 * @file
 */
/**
 * Accession add panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_add_form($form, &$form_state) {

  // Gets BIMS_USER, BIMS_PROGRAM and BIMS_CROP.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $bims_crop    = $bims_user->getCrop();
  $program_id   = $bims_program->getProgramID();

  // Gets accession label.
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
  $accession_lc = strtolower($accession);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_accession_add');
  $bims_panel->init($form);

  // Property for an accession / progreny.
  $form['property'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Add a new property",
    '#collapsed'   => TRUE,
    '#collapsible' => TRUE,
  );
  $form['property']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Property Name'),
    '#description'  => t("Please provide an name of a property."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['property']['type'] = array(
    '#type'           => 'radios',
    '#options'        => array('accession' => $accession, 'progeny' => 'Progeny'),
    '#default_value'  => 'accession',
    '#attributes'     => array('style' => 'width:20px;'),
  );
  $form['property']['desc'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please provide a description of a property."),
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['property']['add_property_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_property_btn',
    '#value'      => "Add property",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_accession_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-accession-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Properties of an accession.
  $form['accession_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Add a new $accession_lc",
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['accession_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => $accession,
    '#description'  => t("Please provide an accession. Please avoid using any special characters excepts dash, slash and underscore"),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['accession_prop']['type_id'] = array(
    '#type'         => 'select',
    '#title'        => t('Type'),
    '#options'      => MCL_DATA_VALID_TYPE::getOptions('stock_type'),
    '#description'  => t("Please choose a type of the accession"),
    '#attributes' => array('style' => 'width:250px;'),
  );
  $form['accession_prop']['organism_id'] = array(
    '#type'         => 'select',
    '#title'        => t('Organism'),
    '#options'      => $bims_crop->getOrganisms(BIMS_OPTION),
    '#description'  => t("Please choose an organism of the accession"),
    '#attributes' => array('style' => 'width:250px;'),
  );
  $form['accession_prop']['stock_m'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Maternal Parent'),
    '#description'        => t("Please type the name of maternal parent"),
    '#attributes'         => array('style' => 'width:250px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id",
  );
  $form['accession_prop']['stock_p'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Paternal Parent'),
    '#description'        => t("Please type the name of paternal parent"),
    '#attributes'         => array('style' => 'width:250px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_accession/$program_id",
  );
  $form['accession_prop']['add_accession_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_accession_btn',
    '#value'      => "Add $accession_lc",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_accession_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-accession-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['accession_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_accession_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-accession-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-accession-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_accession_add_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_add_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_add_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_user->getProgramID();

  // Gets accession label.
  $accession_label = $bims_program->getBIMSLabel('accession');

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add Property" was clicked.
  if ($trigger_elem == 'add_property_btn') {

    // Gets the name of the property.
    $name = trim($form_state['values']['property']['name']);

    // Validates the property name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('property][name', "Invalid property name : $error");
      return;
    }

    // Checks for duplication.
    if ($bims_program->checkProperty('accession', $name)) {
      form_set_error('property][name', "Propey nanme ($name) has aleady existed.");
      return;
    }
  }

  // If "Add Accession" was clicked.
  else if ($trigger_elem == 'add_accession_btn') {

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO table.
    $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
    $bc_cross     = new BIMS_CHADO_CROSS($program_id);

    // Gets the properties of the accession.
    $name         = trim($form_state['values']['accession_prop']['name']);
    $organism_id  = $form_state['values']['accession_prop']['organism_id'];
    $maternal     = trim($form_state['values']['accession_prop']['stock_m']);
    $paternal     = trim($form_state['values']['accession_prop']['stock_p']);

    // Validates the accession name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('accession_prop][name', "Invalid property name : $error");
      return;
    }

    // Checks the accession name for duplication.
    $uniquename = $prefix . $name;
    $accession = $bc_accession->byTKey('accession', array('uniquename' => $uniquename, 'organism_id' => $organism_id));
    if ($accession) {
      form_set_error('accession_prop][name', "The $accession_label ($name, $organism_id) has been taken. Please choose other name.");
    }

    // Checks the parents.
    if ($maternal) {
      if (!$bc_accession->byUniquename($prefix . $maternal)) {
        form_set_error('accession_prop][stock_m', "$maternal does not exist.");
      }
    }
    if ($paternal) {
      if (!$bc_accession->byUniquename($prefix . $paternal)) {
        form_set_error('accession_prop][stock_p', "$paternal does not exist.");
      }
    }
  }
}

/**
 * Adds an accession.
 *
 * @param array $form_state
 *
 * @return integer
 */
function _bims_panel_add_on_accession_add_accession($form_state) {

  // Gets the BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the prefix.
  $prefix = $bims_program->getPrefix();

  // Gets BIMS_CHADO tables.
  $bc_accession = new BIMS_CHADO_ACCESSION($program_id);
  $bc_site      = new BIMS_CHADO_SITE($program_id);
  $bc_cross     = new BIMS_CHADO_CROSS($program_id);

  // Gets cvterms.
  $cvterms = array(
    'is_a_maternal_parent_of' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'is_a_maternal_parent_of')->getCvtermID(),
    'is_a_paternal_parent_of' => MCL_CHADO_CVTERM::getCvterm('SITE_CV', 'is_a_paternal_parent_of')->getCvtermID(),
  );

  // Gets the properties of the accession.
  $name         = trim($form_state['values']['accession_prop']['name']);
  $type_id      = $form_state['values']['accession_prop']['type_id'];
  $organism_id  = $form_state['values']['accession_prop']['organism_id'];
  $maternal     = trim($form_state['values']['accession_prop']['stock_m']);
  $paternal     = trim($form_state['values']['accession_prop']['stock_p']);

  // Adds an accession.
  $details = array(
    'uniquename'  => $prefix . $name,
    'name'        => $name,
    'organism_id' => $organism_id,
    'type_id'     => $type_id,
  );
  $stock_id = $bc_accession->addAccession(NULL, $details);
  if ($stock_id) {
    $bc_accession->setStockID($stock_id);

    // Sets the accession properties.
    $type     = MCL_CHADO_CVTERM::byID($type_id);
    $organism = MCL_CHADO_ORGANISM::byKey(array('organism_id' => $organism_id));
    $details_mview = array(
      'stock_id'    => $stock_id,
      'uniquename'  => $details['uniquename'],
      'name'        => $details['name'],
      'type_id'     => $type_id,
      'type'        => $type->getName(),
      'organism_id' => $organism_id,
      'genus'       => $organism->getGenus(),
      'species'     => $organism->getSpecies(),
    );

    // Adds parents.
    if ($maternal) {
      $bc_accession->addParent(NULL, $prefix . $maternal, $organism_id, $cvterms['is_a_maternal_parent_of']);
      $maternal_parent = $bc_accession->byUniquename($prefix . $maternal);
      $details_mview['maternal']    = $maternal_parent->name;
      $details_mview['stock_id_m']  = $maternal_parent->stock_id;
    }
    if ($paternal) {
      $bc_accession->addParent(NULL, $prefix . $paternal, $organism_id, $cvterms['is_a_paternal_parent_of']);
      $paternal_parent = $bc_accession->byUniquename($prefix . $paternal);
      $details_mview['paternal']    = $paternal_parent->name;
      $details_mview['stock_id_m']  = $paternal_parent->stock_id;
    }

    // Adds the accession to MVIEW.
    $mview_stock = new BIMS_MVIEW_STOCK(array('node_id' => $program_id));
    if (!$mview_stock->addStock($details_mview)) {
      drupal_set_message("Error : Failed to add a new mview stock", 'error');
      return NULL;
    }
  }
  else {
    drupal_set_message("Error : Failed to add a new stock", 'error');
    return NULL;
  }
  return $stock_id;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_accession';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add Property" was clicked.
  if ($trigger_elem == 'add_property_btn') {

    // Gets BIMS_USER and BIMS_PROGRAM.
    $bims_user    = getBIMS_USER();
    $bims_program = $bims_user->getProgram();

    // Gets the name of the property.
    $name = trim($form_state['values']['property']['name']);
    $desc = trim($form_state['values']['property']['desc']);
    $type = $form_state['values']['property']['type'];

    $transaction = db_transaction();
    try {

      // Adds the cvterm.
      $cv = $bims_program->getCv($type);
      if (MCL_CHADO_CVTERM::addCvterm(NULL, 'SITE_DB', $cv->getName(), $name, $desc)) {
        drupal_set_message("New property has been added");
      }
      else {
        throw new Exception("Error : Failed to add a new property");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // If "Add accession" was clicked.
  else if ($trigger_elem == 'add_accession_btn') {

    $transaction = db_transaction();
    try {

      // Adds a new accession.
      $stock_id = _bims_panel_add_on_accession_add_accession($form_state);
      if (!$stock_id) {
        throw new Exception("Error : Failed to add a new accession");
      }
      $load_primary_panel = "$load_primary_panel/$stock_id";
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Updates the panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_accession_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}