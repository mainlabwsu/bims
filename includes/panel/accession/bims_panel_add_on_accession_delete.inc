<?php
/**
 * @file
 */
/**
 * Accession delete panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_delete_form($form, &$form_state) {

  // Local variables for form properties.
  $min_height = 500;

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_accession_delete');
  $bims_panel->init($form);

  // Adds accession table.
  $title = "$accession Mass Deletion";
  $form['accession'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => $title,
    '#attributes'   => array(),
  );

  // Gets the ranged accessions.
  $bims_mview = new BIMS_MVIEW_STOCK(array('node_id' => $bims_program->getProgramID()));
  $headers = array(
    array('field' => 'stock_id', 'data' => 'stock_id', 'header' => '&nbsp;'),
    array('field' => 'name',     'data' => 'name',     'header' => $accession, 'sort' => 'asc'),
    array('field' => 'genus',    'data' => 'genus',    'header' => 'Genus'),
    array('field' => 'species',  'data' => 'species',  'header' => 'Species'),
    array('field' => 'type',     'data' => 'type',     'header' => 'Type'),
  );
  $pt_var = array(
    'data_only' => TRUE,
    'id'        => 'bims_panel_add_on_accession_delete',
    'num_pages' => 10,
    'mview'     => $bims_mview->getMView(),
    'headers'   => $headers,
    'flag'      => BIMS_OBJECT,
    'tab'       => "Delete $accession",
  );

  // Creates the checkboxes.
  $accessions = bims_create_pager_table($pt_var);
  foreach ((array)$accessions as $stock) {
    $form['accession']['stock_elem'][$stock->stock_id] = array(
      '#title'          => '',
      '#type'           => 'checkbox',
      '#default_value'  => 0,
      '#attributes'     => array('style' => 'margin-left:10px;'),
    );
    $form['accession']['stock_info'][$stock->stock_id] = array(
      '#type'   => 'value',
      '#value'  => $stock,
    );
  }
  $form['accession']['delete_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'delete_btn',
    '#value'      => "Delete",
    '#attributes' => array(
      'style' => 'width:180px;',
      'class' => array('use-ajax', 'bims-confirm'),
    ),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_accession_delete_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-accession-delete-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['accession']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );
  $form['accession']['pt_var'] = array(
    '#type'   => 'value',
    '#value'  => $pt_var,
  );
  $form['accession']['#theme'] = 'bims_panel_add_on_accession_delete_form_accession_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-accession-delete-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_accession_delete_form_submit';
  $form['#theme'] = 'bims_panel_add_on_accession_delete_form';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_delete_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $accession    = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Delete" was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Gets the chosen accessions.
    $stock_elem = $form_state['values']['accession']['stock_elem'];
    $del_stock_ids = array();
    foreach ((array)$stock_elem as $stock_id => $elem) {
      if ($elem) {
        $del_stock_ids []= $stock_id;
      }
    }

    // Deletes the accesions.
    if (!empty($del_stock_ids)) {

    // Deletes the accesions.s
      $drush = bims_get_config_setting('bims_drush_binary');
      $cmd = "bims-delete-data $program_id stock --id=" . implode(":", $del_stock_ids);
      $drush .= " $cmd > /dev/null 2>/dev/null  & echo $!";
      $pid = exec($drush, $output, $return_var);
      drupal_set_message("Submit the job to delete $accession");

      // Updates the panels.
      $url = "bims/load_main_panel/bims_panel_add_on_accession_delete";
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_accession')));
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_accession_delete', "Delete $accession", $url)));
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_delete_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Delete" was clicked.
  if ($trigger_elem == 'delete_btn') {

    // Checks the chosen accessions.
    $stock_elem = $form_state['values']['accession']['stock_elem'];
    $count = 0;
    foreach ((array)$stock_elem as $stock_id => $elem) {
       if ($elem) {
        $count++;
      }
    }
    if (!$count) {
      form_set_error('accession][stock_elem', "Please choose at least one");
      return;
    }
  }
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_accession_delete_form_submit($form, &$form_state) {}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_accession_delete_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds accession info.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['accession']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_accession_delete_form_accession_table($variables) {
  $element = $variables['element'];

  // Gets the parameters.
  $stock_elem   = $element['stock_elem'];
  $stock_info   = $element['stock_info'];
  $delete_btn   = $element['delete_btn'];
  $bims_program = $element['bims_program']['#value'];
  $pt_var       = $element['pt_var']['#value'];
  $id           = $pt_var['id'];
  $tab          = $pt_var['tab'];
  $headers      = $pt_var['headers'];
  $num_pages    = $pt_var['num_pages'];
  $element      = 0;

  // Lists the chosen accessions.
  $rows = array();
  foreach ((array)$stock_elem as $stock_id => $elem) {
    if (!preg_match("/^(\d+)$/", $stock_id)) {
      continue;
    }

    // Gets the stock object.
    $obj = $stock_info[$stock_id]['#value'];

    // Adds a accession.
    $row = array(
      drupal_render($elem),
      $obj->name,
      $obj->genus,
      $obj->species,
      $obj->type,
    );
    $rows []= $row;
  }

  // Params for the sortable table and the pager.
  $accession = $bims_program->getBIMSLabel('accession', BIMS_UCFIRST);
  $param = array(
    'url' => "bims/load_main_panel/$id" . '?',
    'id'  => $id,
    'tab' => $tab,
  );

  // Creates the table.
  $table_vars = array(
    'header'      => $headers,
    'rows'        => $rows,
    'attributes'  => array(),
    'sticky'      => TRUE,
    'empty'       => "No $accession found",
    'param'       => $param,
  );
  $table = bims_theme_table($table_vars);

  // Creates the pager.
  $pager_vars = array(
    'tags'        => array(),
    'element'     => $element,
    'parameters'  => array(),
    'quantity'    => $num_pages,
    'param'       => $param,
  );
  $pager = bims_theme_pager($pager_vars);

  // Creates the delete button.
  $delete_btn = '<div>' . drupal_render($delete_btn) . '</div>';

  // Shows the table with a pager.
  return $delete_btn . $table . $pager;
}

