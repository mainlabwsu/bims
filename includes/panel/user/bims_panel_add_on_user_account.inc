<?php
/**
 * @file
 */
/**
 * User Account panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_user_account_form($form, &$form_state, $arg_str = '') {

  // Local variables for form properties.
  $min_height_user  = 260;
  $min_height_files = 400;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Parses 'args' if exists.
  $args = array();
  if ($arg_str) {}

  // Gets file_id.
  $bims_file = NULL;
  $file_id = '';
  if (isset($form_state['values']['files']['list'])) {
    $file_id = $form_state['values']['files']['list'];
    $bims_file = BIMS_FILE::byID($file_id);
  }

  // Gets the selected file type.
  $file_type = '';
  if (isset($form_state['values']['files']['file_types'])) {
    $file_type = $form_state['values']['files']['file_types'];
  }
  if (array_key_exists('file_type', $args)) {
    $file_type = $args['file_type'];
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_user_account');
  if (!$bims_panel->init($form, TRUE)) {
    return $form;
  }

  // Adds the user information section.
  $user_info_collapsed = FALSE;
  $form['user_info'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => $user_info_collapsed,
    '#collapsible'  => TRUE,
    '#title'        => 'User Information',
  );
  $form['user_info']['#theme'] = 'bims_panel_add_on_user_account_form_user_info';

  // Adds the user profile table.
  $form['user_info']['user_profile'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'User Profile',
    '#attributes'   => array('style' => 'min-height:' . $min_height_user . 'px;'),
  );
  $form['user_info']['user_profile']['table'] = array(
    '#markup' => bims_user_profile_table($bims_user),
  );
  if ($bims_user->isAnonymous()) {
    return $form;
  }

  // Adds the contact name if this user does not have one.
  $username = $bims_user->getName();
  $notes = "
    The username '<em><b>$username</b></em>' is your account name of this site. We
    recommend to use this username in '<em>Contact</em>' or '<em>Evaluators</em>' column in Excel data templates
    as well as '<em>Person</em>' in your Field Book App.
  ";
  $form['user_info']['user_profile']['username'] = array(
    '#markup' => "<div class='bims-rounded-div' style='height:80px;'>$notes</div>",
  );

  // Email preference.
  $form['user_info']['email'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Preference',
    '#attributes'   => array('style' => 'min-height:' . $min_height_user . 'px;'),
  );
  _bims_panel_add_on_user_account_form_preference_table($form['user_info']['email'], $bims_user);

  // Adds 'Download files' section.
  $form['download_files'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => TRUE,
    '#title'        => 'Dowload Files',
   );
  _bims_panel_add_on_user_account_form_download_files($form['download_files'], $bims_user);

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-user-account-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_user_account_form_submit';
  $form['#theme'] = 'bims_panel_add_on_user_account_form';
  return $form;
}

/**
 * Generates download files table.
 *
 * @param array $form
 * @param BIMS_USER $bims_user
 *
 * @return string
 */
function _bims_panel_add_on_user_account_form_download_files(&$form, $bims_user) {

  // Gets all the download files.
  $details = array(
    'type'        => 'download',
    'program_id'  => $bims_user->getProgramID(),
    'user_id'     => $bims_user->getUserID()
  );
  $bims_files = BIMS_FILE::getFiles($details, BIMS_CLASS);
  $form['files'] = array();
  foreach((array)$bims_files as $bims_file) {
    $file_id = $bims_file->getFileID();
    $form['files'][$file_id]['bims_file'] = array(
      '#type' => 'value',
      '#value' => $bims_file,
    );
    $form['files'][$file_id]['delete_cb'] = array(
      '#id'   => 'cb-' . $file_id,
      '#type' => 'checkbox',
    );
    $form['files'][$file_id]['download_btn'] = array(
      '#id'         => 'bims-dl-' . $file_id,
      '#type'       => 'submit',
      '#name'       => 'download_btn-' . $file_id,
      '#value'      => 'Download',
      '#attributes' => array('style' => 'width:85px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_user_account_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-user-account-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
    $form['files'][$file_id]['delete_btn'] = array(
      '#id'         => 'bims-del-' . $file_id,
      '#type'       => 'submit',
      '#name'       => 'delete_btn-' . $file_id,
      '#value'      => 'Delete',
      '#attributes' => array(
        'style' => 'width:85px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_add_on_user_account_form_ajax_callback",
        'wrapper'  => 'bims-panel-add-on-user-account-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['delete_cb_btn'] = array(
    '#id'         => 'delete_cb_btn',
    '#name'       => 'delete_cb_btn',
    '#type'       => 'submit',
    '#value'      => 'Delete checked',
    '#attributes' => array(
      'style' => 'width:140px;',
      'class' => array('use-ajax', 'bims-confirm'),
    ),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_user_account_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-user-account-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['refresh_btn'] = array(
    '#name'       => 'refresh_btn',
    '#type'       => 'button',
    '#value'      => 'Refresh',
    '#attributes' => array('style' => 'margin-left:10px;width:140px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_user_account_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-user-account-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['#theme'] = 'bims_panel_add_on_user_account_form_download_files';
}

/**
 * Generates user profile table.
 *
 * @param BIMS_USER $bims_user
 *
 * @return string
 */
function bims_user_profile_table(BIMS_USER $bims_user) {

  // Checks if this user is a breeder.
  $breeder = ($bims_user->isBreeder()) ? 'Yes' : 'No';

  // Table rows.
  $rows = array(
    array('Username', $bims_user->getName()),
    array('E-mail', $bims_user->getMail()),
    array('Breeder', $breeder),
    array('Programs', 'N/A'),
  );

  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:100%;'),
  );
  return theme('table', $table_vars);
}

/**
 * Generates the preference table.
 *
 * @param array $form
 * @param BIMS_USER $bims_user
 *
 * @return string
 */
function _bims_panel_add_on_user_account_form_preference_table(&$form, BIMS_USER $bims_user) {

  // Upload option.
  $form['upload_cb'] = array(
    '#type'           => 'checkbox',
    '#title'          => 'Data Upload',
    '#prefix'         => '<b>E-mail</b>',
    '#description'    => "Check if you'd like recieve a notification email when uploading data is completed.<br />'Data Upload' includes uploading Excel template, Field Book exported trait / field files",
    '#default_value'  => $bims_user->getEmailOption('upload'),
  );

  // Instruction option.
  $form['instruction_cb'] = array(
    '#type'           => 'checkbox',
    '#title'          => 'Instruction closed',
    '#prefix'         => '<b>Instruction</b>',
    '#description'    => "Check if you'd like always have an instruction closed on a page.",
    '#default_value'  => $bims_user->getInstructionOption(),
  );

  // Adds 'Save' button.
  $form['save_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'save_btn',
    '#value'      => 'Save',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_user_account_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-user-account-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_user_account_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Refresh' was clicked.
  if ($trigger_elem == 'refresh_btn') {
    $url = "bims/load_main_panel/bims_panel_add_on_user_account";
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_user_account', 'User Account', $url)));
  }

  // If 'Download' was clicked.
  else if (preg_match("/^download_btn/", $trigger_elem)) {
    $elem_id  = $form_state['triggering_element']['#id'];
    $file_id  = str_replace('bims-dl-', '', $elem_id);
    $url      = "bims/download_bims_file/$file_id";
    bims_add_ajax_command(ajax_command_invoke(NULL, "download_file", array($url)));
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_user_account_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_user_account_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Save" was clicked.
  if ($trigger_elem == 'save_btn') {

    // Gets preference options.
    $upload_cb      = ($form_state['values']['user_info']['email']['upload_cb']) ? TRUE : FALSE;
    $instruction_cb = ($form_state['values']['user_info']['email']['instruction_cb']) ? TRUE : FALSE;

    // Updates the preference.
    $bims_user->updateEmailOption('upload', $upload_cb);
    $bims_user->updateInstructionOption($instruction_cb);

    // Updates the panels.
    bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_user_account', 'User Account')));
  }

  // If 'Delete checked' was clicked.
  else if (preg_match("/^delete_cb_btn/", $trigger_elem)) {
    $files =  $form_state['values']['download_files']['files'];
    $num_del_files = 0;
    foreach ((array)$files as $file_id => $info) {
      if ($info['delete_cb'] == 1) {
        $num_del_files++;
        $bims_file = $info['bims_file'];
        if (!$bims_file->delete()) {
          drupal_set_message("Error : Failed to delete the download file [$file_id].", 'error');
        }
      }
    }
    if ($num_del_files) {
      drupal_set_message("$num_del_files files have been deleted");
    }
    else {
      drupal_set_message("No file has been deleted");
    }
  }

  // If 'Delete' was clicked.
  else if (preg_match("/^delete_btn/", $trigger_elem)) {
    $elem_id  = $form_state['triggering_element']['#id'];
    $file_id  = str_replace('bims-del-', '', $elem_id);
    $bims_file = BIMS_FILE::byID($file_id);
    if ($bims_file->delete()) {
      bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_user_account', 'User Account')));
    }
    else {
      drupal_set_message('Error : Failed to delete the download file.', 'error');
    }
  }
}

/**
 * Theme function for 'Download files'.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_user_account_form_download_files($variables) {
  $element = $variables['element'];

  // Sets the table header.
  $header = array(
    array('data' => '', 'width' => 0),
    array('data' => 'Filename', 'width' => 350),
    array('data' => 'Filesize', 'width' => 50),
    array('data' => 'Data&nbsp;Type', 'width' => 100),
    array('data' => 'Create&nbsp;Date', 'width' => 130),
    array('data' => 'Status', 'width' => 40),
    array('data' => 'Delete', 'width' => 90),
    array('data' => 'Download', 'width' => 90),
  );

  // Adds the files.
  $rows = array();
  foreach ((array)$element['files'] as $file_id => $info) {
    if (!preg_match("/^\d+$/", $file_id, $matches)) {
      continue;
    }
    $bims_file    = $info['bims_file']['#value'];
    $delete_cb    = $info['delete_cb'];
    $delete_btn   = $info['delete_btn'];
    $download_btn = $info['download_btn'];
    $data_type    = $bims_file->getPropByKey('data_type');
    $status       = $bims_file->getPropByKey('status');

    // Adds a file row.
    $rows []= array(
      drupal_render($delete_cb),
      $bims_file->getFilename(),
      $bims_file->getFilesize(TRUE),
      $data_type,
      $bims_file->getSubmitDate(),
      $status,
      drupal_render($delete_btn),
      drupal_render($download_btn),
    );
  }

  // Creates a file table.
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'empty'       => "<div>No download file found</div>",
    'attributes'  => array('style' => 'width:100%;'),
  );
  $layout = theme('table', $table_vars);

  // Adds 'Refresh' button.
  $layout .= drupal_render($element['delete_cb_btn']);
  $layout .= drupal_render($element['refresh_btn']);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_user_account_form_user_info($variables) {
  $element = $variables['element'];

  // Adds 'User Profile' and 'Preference'.
  $layout = "<div style='float:left;width:50%;'>" . drupal_render($element['user_profile']) . "</div>";
  $layout .= "<div style='float:left;width:49%;margin-left:5px;'>" . drupal_render($element['email']) . "</div><br clear='all'>";
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_add_on_user_account_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds 'User Information' section.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['user_info']) . "</div>";

  // Adds 'User Files' section.
  $layout .= "<div style='width:100%;'>" . drupal_render($form['files']) . "</div>";
  $layout .= drupal_render_children($form);
  return $layout;
}

