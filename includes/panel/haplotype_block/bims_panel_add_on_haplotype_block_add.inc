<?php
/**
 * @file
 */
/**
 * Haplotype block add panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_haplotype_block_add_form($form, &$form_state) {

  // Gets BIMS_USER, BIMS_PROGRAM and BIMS_CROP.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $bims_crop    = $bims_user->getCrop();
  $program_id   = $bims_program->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_haplotype_block_add');
  $bims_panel->init($form);

  // Property for an haplotype block properties.
  $form['property'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Add a new property",
    '#collapsed'   => TRUE,
    '#collapsible' => TRUE,
  );
  $form['property']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Property Name'),
    '#description'  => t("Please provide an name of a property."),
    '#attributes'   => array('style' => 'width:250px;'),
  );
  $form['property']['add_property_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_property_btn',
    '#value'      => "Add Property",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_haplotype_block_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-haplotype-block-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Properties of a haplotype block.
  $form['haplotype_block_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Add a new haplotype block',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['haplotype_block_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => 'Haplotype Block',
    '#description'  => t("Please provide a name of the haplotype block. Please avoid using any special characters excepts dash, slash and underscore"),
    '#attributes'   => array('style' => 'width:250px;'),
    '#required'     => TRUE,
  );
  $form['haplotype_block_prop']['organism_id'] = array(
    '#type'         => 'select',
    '#title'        => t('Organism'),
    '#options'      => $bims_crop->getOrganisms(BIMS_OPTION),
    '#description'  => t("Please choose an organism of the haplotype block"),
    '#attributes' => array('style' => 'width:250px;'),
  );
  $form['haplotype_block_prop']['start'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Start'),
    '#description'  => t("Please provide start position."),
    '#attributes'   => array('style' => 'width:100px;'),
  );
  $form['haplotype_block_prop']['stop'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Stop'),
    '#description'  => t("Please provide stop position."),
    '#attributes'   => array('style' => 'width:100px;'),
  );
  $opt_strand = array(
    ''        => 'N/A',
    'forward' => '+ (forward)',
    'reverse' => '- (reverse)',
  );
  $form['haplotype_block_prop']['strand'] = array(
    '#type'         => 'select',
    '#title'        => t('Strand'),
    '#options'      => $opt_strand,
    '#description'  => t("Please choose a strand."),
    '#attributes'   => array('style' => 'width:100px;'),
  );
  $opt_trait = BIMS_CVTERM::getTraits(BIMS_OPTION);
  $form['haplotype_block_prop']['trait'] = array(
    '#type'         => 'select',
    '#title'        => t('Trait'),
    '#description'  => t("Please choose a trait."),
    '#options'      => $opt_trait,
    '#attributes'   => array('style' => 'width:200px;'),
    '#required'     => TRUE,
  );
  $form['haplotype_block_prop']['chromosome'] = array(
    '#type'               => 'textfield',
    '#title'              => t('Chromosome'),
    '#description'        => t("Please provide a chromosome."),
    '#attributes'         => array('style' => 'width:250px;'),
    '#autocomplete_path'  => "bims/bims_autocomplete_chromosome/$program_id",
    '#required'           => TRUE,
  );
  $form['haplotype_block_prop']['add_hb_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_btn',
    '#value'      => "Add",
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_haplotype_block_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-haplotype-block-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['haplotype_block_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:150px;'),
    '#ajax'       => array(
      'callback' => "bims_panel_add_on_haplotype_block_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-haplotype-block-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-haplotype-block-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_haplotype_block_add_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_haplotype_block_add_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_haplotype_block_add_form_validate($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add Property" was clicked.
  if ($trigger_elem == 'add_property_btn') {

    // Gets the name of the property.
    $name = trim($form_state['values']['property']['name']);

    // Checks the haplotype block name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-\/\\][a-zA-z0-9_\-\/\\ ]*$/", $name)) {
      form_set_error('property][name', "Invalid property name. Please use alpha-numeric, dash and underscore.");
      return;
    }

    // Checks for duplication.
    if ($bims_program->checkProperty('haplotype_block', $name)) {
      form_set_error('property][name', "Propey nanme ($name) has aleady existed.");
      return;
    }
  }

  // If "Add" was clicked.
  else if ($trigger_elem == 'add_btn') {

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO table.
    $bc_feature = new BIMS_CHADO_FEATURE($program_id);

    // Gets the properties of the haplotype block.
    $name         = trim($form_state['values']['haplotype_block_prop']['name']);
    $organism_id  = $form_state['values']['haplotype_block_prop']['organism_id'];
    $trait        = trim($form_state['values']['haplotype_block_prop']['trait']);
    $chromosome   = trim($form_state['values']['haplotype_block_prop']['chromosome']);

    // Checks the haplotype block name for empty or alpha-numeric, dash and underscore.
    if (!preg_match("/^[a-zA-z0-9_\-]+$/", $name)) {
      form_set_error('haplotype_block_prop][name', "Invalid haplotype block. Please use alpha-numeric, dash and underscore.");
      return;
    }

    // Checks the haplotype block name for duplication.
    $uniquename = $prefix . $name;
    $haplotype_block = $bc_feature->byTKey('feature', array('uniquename' => $uniquename, 'organism_id' => $organism_id));
    if ($haplotype_block) {
      form_set_error('haplotype_block_prop][name', "The haplotype block ($name, $organism_id) has been taken. Please choose other name.");
    }

    // Checks the chromosome.
    $cvterm = MCL_CHADO_CVTERM::getCvterm('sequence', 'chromosome');
    $keys = array(
      'uniquename'  => $chromosome,
      'type_id'     => $cvterm->getCvtermID(),
    );
    $chr = MCL_CHADO_FEATURE::byKey($keys);
    if (!$chr) {
      form_set_error('haplotype_block_prop][chromosome', "Invalid chromosome. Please type a valid chromosome.");
      return;
    }
  }
}

/**
 * Adds a haplotype block.
 *
 * @param array $form_state
 *
 * @return integer
 */
function _bims_panel_add_on_haplotype_block_add_haplotype_block($form_state) {

  // Gets the BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the prefix.
  $prefix = $bims_program->getPrefix();

  // Gets BIMS_CHADO tables.
  $bc_feature = new BIMS_CHADO_FEATURE($program_id);

  //Gets the cvterm of 'haplotype_block'.
  $cvterm_id_hb = MCL_CHADO_CVTERM::getCvterm('sequence', 'haplotype_block')->getCvtermID();

  // Gets the properties of the haplotype block.
  $name           = trim($form_state['values']['haplotype_block_prop']['name']);
  $organism_id    = $form_state['values']['haplotype_block_prop']['organism_id'];
  $trait_id       = $form_state['values']['haplotype_block_prop']['trait'];
  $chromosome     = $form_state['values']['haplotype_block_prop']['chromosome'];
  $start          = trim($form_state['values']['haplotype_block_prop']['start']);
  $stop           = trim($form_state['values']['haplotype_block_prop']['stop']);
  $strand         = trim($form_state['values']['haplotype_block_prop']['strand']);

  // Adds a haplotype block.
  $details = array(
    'uniquename'  => $prefix . $name,
    'name'        => $name,
    'organism_id' => $organism_id,
    'type_id'     => $cvterm_id_hb,
  );
  $feature_id = $bc_feature->addFeature(NULL, $details);
  if ($feature_id) {

    // Sets the haplotype block properties.
    $organism   = MCL_CHADO_ORGANISM::byKey(array('organism_id' => $organism_id));
    $trait      = MCL_CHADO_CVTERM::byID($trait_id);
    $chromosome = MCL_CAHDO_FEATURE::byKey(array('uniquename' => $chromosome));
    $strand     = bims_get_strand($strand, 'symbol');

    // Updates the properties of the haplotype block in BIMS_CHADO.




    // Adds the haplotype block to MVIEW.
    $details_mview  = array(
      'feature_id'      => $feature_id,
      'uniquename'      => $details['uniquename'],
      'name'            => $details['name'],
      'type_id'         => $details['type_id'],
      'type'            => 'haplotype_block',
      'organism_id'     => $organism_id,
      'genus'           => $organism->getGenus(),
      'species'         => $organism->getSpecies(),
      'trait_id'        => $trait->getCvtermID(),
      'trait'           => $trait->getName(),
      'chromosome_fid'  => $chromosome->getFeatureID(),
      'chromosome'      => $chromosome,
      'start'           => $start,
      'stop'            => $stop,
      'strand'          => $strand,
    );
    $mview_hb = new BIMS_MVIEW_HAPLOTYPE_BLOCK(array('node_id' => $program_id));
    if (!$mview_hb->addHaplotypeBlock($details_mview)) {
      drupal_set_message("Error : Failed to add a new haplotype block to BIMS_MVIEW_HAPLOTYPE_BLOCK", 'error');
      return NULL;
    }
  }
  else {
    drupal_set_message("Error : Failed to add a new haplotype block", 'error');
    return NULL;
  }
  return $feature_id;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_haplotype_block_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_haplotype_block';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If 'Add Property' was clicked.
  if ($trigger_elem == 'add_property_btn') {

    // Gets BIMS_USER and BIMS_PROGRAM.
    $bims_user    = getBIMS_USER();
    $bims_program = $bims_user->getProgram();

    // Gets the name of the property.
    $name = trim($form_state['values']['property']['name']);
    $desc = trim($form_state['values']['property']['desc']);
    $type = $form_state['values']['property']['type'];

    $transaction = db_transaction();
    try {

      // Adds the cvterm.
      $cv = $bims_program->getCv($type);
      if (MCL_CHADO_CVTERM::addCvterm(NULL, 'SITE_DB', $cv->getName(), $name, $desc)) {
        drupal_set_message("New property has been added");
      }
      else {
        throw new Exception("Error : Failed to add a new property");
      }
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // If 'Add' was clicked.
  else if ($trigger_elem == 'add_btn') {

    $transaction = db_transaction();
    try {

      // Adds a new haplotype block.
      $feature_id = _bims_panel_add_on_haplotype_block_add_haplotype_block($form_state);
      if (!$feature_id) {
        throw new Exception("Error : Failed to add a new haplotype block");
      }
      $load_primary_panel = "$load_primary_panel/$feature_id";
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Updates the panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_haplotype_block_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}