<?php
/**
 * Haplotype Block panel form.
 *
 * @param array $form
 * @param array $form_state
 * @param integer $feature_id
 * @param string $filter_list
 */
function bims_panel_main_haplotype_block_form($form, &$form_state, $feature_id = NULL, $filter_list = '') {

  // Local variables for form properties.
  $min_height = 380;

  // Gets BIMS_MVIEW_HAPLOTYPE_BLOCK.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();
  $mview_hb     = new BIMS_MVIEW_HAPLOTYPE_BLOCK(array('node_id' => $program_id));

  // Gets the haplotype block object.
  $hb_obj = NULL;
  if (isset($form_state['values']['hb_list']['selection'])) {
    $feature_id = $form_state['values']['hb_list']['selection'];
  }
  if ($feature_id) {
    $hb_obj = $mview_hb->getHaplotypeBlock($feature_id, BIMS_OBJECT);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_haplotype_block');
  $bims_panel->init($form);

  // Adds the admin menu.
  $form['hb_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Haplotype Block',
    '#attributes'   => array(),
  );

  // Adds the "Add" button.
  $url = "bims/load_main_panel/bims_panel_add_on_haplotype_block_add";
  $form['hb_admin']['menu']['add_btn'] = array(
    '#name'       => 'add_btn',
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_haplotype_block_add', 'Add Haplotype Block', '$url'); return (false);",
    ),
  );
  $form['hb_admin']['menu']['#theme'] = 'bims_panel_main_haplotype_block_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['hb_admin']);

  // Lists the all haplotype blocks.
  $form['hb_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Haplotype Block',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  _bims_panel_main_haplotype_block_form_add_hb_list($form['hb_list'], $bims_program, $mview_hb, $feature_id, $filter_list);
  $form['hb_list']['#theme'] = 'bims_panel_main_haplotype_block_form_haplotype_block_list';

  // Haplotype Block details.
  $form['hb_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => "Haplotype Block Details",
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['hb_details']['table']['hb_obj'] = array(
    '#type'   => 'value',
    '#value'  => $hb_obj,
  );
  $form['hb_details']['table']['num_image'] = array(
    '#type'   => 'value',
    '#value'  => 0,
  );
  $form['hb_details']['table']['program_id'] = array(
    '#type'   => 'value',
    '#value'  => $program_id,
  );

  // Adds action buttons.
  if ($hb_obj && bims_can_admin_action($bims_user)) {

    // Adds 'Edit' button.
    if (!$hb_obj->chado) {
      $form['hb_details']['table']['edit_btn'] = array(
        '#name'       => 'edit_btn',
        '#type'       => 'button',
        '#value'      => 'Edit',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_haplotype_block_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-haplotype-block-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds 'Delete' button.
    $form['hb_details']['table']['delete_btn'] = array(
      '#name'       => 'delete_btn',
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#disabled'   => !$bims_user->canEdit(),
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_haplotype_block_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-haplotype-block-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  if ($hb_obj) {
    $form['hb_details']['table']['prop'] = array(
      '#type'         => 'fieldset',
      '#collapsed'    => TRUE,
      '#collapsible'  => TRUE,
      '#title'        => 'Properties',
      '#attributes'   => array('style' => 'width:90%;'),
    );
    $form['hb_details']['table']['prop']['table'] = array(
      '#markup' => '<em>No propery found</em>',
    );
  }
  $form['hb_details']['table']['#theme'] = 'bims_panel_main_haplotype_block_form_haplotype_block_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-haplotype-block-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_haplotype_block_form_submit';
  return $form;
}

/**
 * Adds the list of haplotype blocks.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_MVIEW_HAPLOTYPE_BLOCK $mview_hb
 * @param integer $feature_id
 * @param string $filter_str
 */
function _bims_panel_main_haplotype_block_form_add_hb_list(&$form, BIMS_PROGRAM $bims_program, BIMS_MVIEW_HAPLOTYPE_BLOCK $mview_hb, $feature_id, $filter_str) {
  $program_id = $bims_program->getProgramID();

  // Checks the haplotype block data in the mview.
  if (!$mview_hb->hasData()) {
    $form['no_hb'] = array(
      '#markup' => "No haplotype block is associated with the current program.",
    );
    return;
  }

  // Adds the filter options.
  /*
  $form['filter'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Filter',
    '#attributes'   => array('style' => 'width:100%;'),
  );

  // Sets the flter list.
  $filter_list = array();
  if ($filter_str) {
    $filter_list = explode(':', $filter_str);
  }

  // Adds the filter options if needs.
  if ($bims_program->needFilter('haplotype_block')) {
    $filter_opts = $bims_program->getFilterOpts('haplotype_block');

    // Sets the default haplotype block group.
    if (empty($filter_list)) {
      $filter_list []= bims_get_first_elem($filter_opts, BIMS_ARRAY_KEY);
    }

    // Adds the filter options.
    $form['filter']['filter_list'] = array(
      '#title'          => 'Haplotype Block Groups',
      '#type'           => 'checkboxes',
      '#options'        => $filter_opts,
      '#multiple'       => TRUE,
      '#description'    => 'Please choose haplotype block group(s)',
      '#default_value'  => $filter_list,
      '#prefix'         => '<div style="padding:5px;border:1px solid lightgray;">',
      '#attributes'     => array('style' => 'backgroud-color:#EEEEEE;'),
    );
    $form['filter']['update_btn'] = array(
      '#type'       => 'submit',
      '#value'      => 'Update',
      '#name'       => 'update_btn',
      '#attributes' => array('style' => 'width:180px;'),
      '#suffix'     => '</div>',
      '#ajax'       => array(
        'callback' => "bims_panel_main_haplotype_block_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-haplotype-block-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }*/

  // Gets the filtered haplotype blocks.
  $opt_hb = $bims_program->getFilteredData('haplotype_block', $filter_list);
  $num_hb = sizeof($opt_hb);

  // No haplotype block found after filterd.
  if ($num_hb == 0) {
    $form['no_filtered_hb'] = array(
      '#markup' => "No haplotype block found with the filtered conditions.",
    );
    return;
  }

  // Adds auto-complete.
  if ($num_hb > BIMS_DDL_MAX_LIST) {
    $form['hb_auto'] = array(
      '#type'               => 'textfield',
      '#attributes'         => array('style' => 'width:250px;'),
      '#autocomplete_path'  => "bims/bims_autocomplete_haplotype_block/$program_id/$filter_str",
    );
    $form['hb_details_btn'] = array(
      '#type'       => 'submit',
      '#name'       => 'hb_details_btn',
      '#value'      => 'Details',
      '#attributes' => array('style' => 'width:100px;'),
      '#ajax'       => array(
        'callback' => "bims_panel_main_haplotype_block_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-haplotype-block-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Adds the haplotype block list.
  if ($num_hb > BIMS_DDL_MAX_LIST) {
    $msg = "You have <b>$num_hb</b> haplotype blocks.<br />" .
      "The maximum  number of haplotype blocks listed in a dropdown list is <b>" . BIMS_DDL_MAX_LIST . "</b>.<br />
      Please use the auto-complete textbox above to find a haplotype block.";
    $form['selection'] = array(
      '#markup' => "<div>$msg</div>",
    );
  }
  else {
    $form['selection'] = array(
      '#type'           => 'select',
      '#options'        => $opt_hb,
      '#size'           => 14,
      '#default_value'  => $feature_id,
      '#prefix'         => "You have <b>$num_hb</b> haplotype blocks",
      '#description'    => "Please choose a haplotype block to view the details.",
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => "bims_panel_main_haplotype_block_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-haplotype-block-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_haplotype_block_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Update" was clicked.
  if ($trigger_elem == 'update_btn') {

    // Gets the chosen descriptor groups.
    $filter_list = $form_state['values']['hb_list']['filter']['filter_list'];
    $filter_arr = array();
    foreach ((array)$filter_list as $filter => $bool) {
      if ($bool) {
        $filter_arr []= $filter;
      }
    }

    // Updates panels.
    $filter_str = implode(':', $filter_arr);
    bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_haplotype_block//$filter_str")));
  }
  else {

  // Gets feature_id from the dropdownlist.
    $feature_id = NULL;
    $feature_id = $form_state['values']['hb_list']['selection'];
    if (array_key_exists('selection', $form_state['values']['hb_list'])) {
      $feature_id = $form_state['values']['hb_list']['selection'];
    }

    // Gets the feature_id from the form.
    if (!$feature_id) {
      $hb_obj = $form_state['values']['hb_details']['table']['hb_obj'];
      $feature_id = $hb_obj->feature_id;
    }
    if ($feature_id) {

      // If "Edit" was clicked.
      if ($trigger_elem == 'edit_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_haplotype_block_edit/$feature_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_haplotype_block_edit', "Edit Haplotype Block", $url)));
      }

      // If "Delete" was clicked.
      else if ($trigger_elem == 'delete_btn') {

        // Deletes the haplotype block.
        if(_bims_panel_main_haplotype_block_form_delete_haplotype_block($program_id, $feature_id)) {
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_haplotype_block')));
        }
      }
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Deletes the haplotype_block.
 *
 * @param integer $program_id
 * @param integer $feature_id
 *
 * @return boolean
 */
function _bims_panel_main_haplotype_block_form_delete_haplotype_block($program_id, $feature_id) {

  $transaction = db_transaction();
  try {

    // Deletes the haplotype block.
    $bc_feature = new BIMS_CHADO_FEATURE($program_id);
    if (!$bc_feature->deleteFeatures(array($feature_id))) {
      throw new Exception("Error : Failed to delete the haplotype block.");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_haplotype_block_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_haplotype_block_form_submit($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Details" was clicked.
  if ($trigger_elem == 'hb_details_btn') {

    // Gets the haplotype block.
    $name       = trim($form_state['values']['hb_list']['hb_auto']);
    $mview_hb   = new BIMS_MVIEW_HAPLOTYPE_BLOCK(array('node_id' => $program_id));
    $feature_id = $mview_hb->getFeatureIDByName($name);
    if ($feature_id) {
      bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array("bims_panel_main_haplotype_block/$feature_id")));
    }
    else {
      drupal_set_message("The haplotype block '$name' does not exists.");
    }
  }
}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_haplotype_block_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), "Add a new haplotype block / property.");
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the haplotype block list.
 *
 * @param $variables
 */
function theme_bims_panel_main_haplotype_block_form_haplotype_block_list($variables) {
  $element = $variables['element'];

  // Displays 'No haplotype block' message.
  if (array_key_exists('no_hb', $element)) {
    return '<div>' . drupal_render($element['no_hb']) . '</div>';
  }

  // Adds the filter.
  $layout = "<div>" . drupal_render($element['filter']) . "</div>";

  // Displays 'No filtered haplotype block' message.
  if (array_key_exists('no_filtered_hb', $element)) {
    $layout .= "<div>" . drupal_render($element['filter']) . "</div>";
    return $layout;
  }

  // Adds autocompute.
  $layout .= '<div style="float:left;width:40px;margin-top:8px;"><em><b>Find</b></em></div>' .
    ' <div style="float:left;">' . drupal_render($element['hb_auto']) .
    '</div><div style="float:left;margin-left:10px;">' . drupal_render($element['hb_details_btn']) . '</div>';

  // Adds the haplotype block list.
  $layout .= '<div>' . drupal_render($element['selection']) . '</div>';
  return $layout;
}

/**
 * Theme function for the haplotype block information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_haplotype_block_form_haplotype_block_table($variables) {
  $element = $variables['element'];

  // Gets haplotype block object.
  $program_id = $element['program_id']['#value'];
  $hb_obj     = $element['hb_obj']['#value'];

  // Gets BIMS_PROGRAM.
  $bims_program = BIMS_PROGRAM::byID($program_id);

  // Initializes the properties.
  $name           = '--';
  $alias          = '';
  $genus          = '--';
  $species        = '';
  $data_row       = '--';
  $location_row   = '--';
  $trait          = '--';
  $chromosome     = '--';
  $comments       = '';
  $properties     = '';
  $actions        = '';
  $image_row      = '';

  // Updates the properites.
  if ($hb_obj) {
    $name     = $hb_obj->name;
    $genus    = $hb_obj->genus;
    $species  = $hb_obj->species;
    $trait    = $hb_obj->trait;

    // Updates the alias.
    $mview_alias = new BIMS_MVIEW_ALIAS(array('node_id' => $program_id));
    $aliases = $mview_alias->getAlias('haplotype_block', $hb_obj->feature_id, BIMS_STRING, ' / ');
    if ($aliases) {
      $alias = $aliases;
    }

    // Adds 'View' buttons.
    $data_row = '';
    if (array_key_exists('view_btn', $element)) {
      $data_row .= drupal_render($element['view_btn']);
    }

    // Adds the properties.
    $rows = array();
    $props = $bims_program->getProperties('haplotype_block', 'details', BIMS_FIELD);
    if (!empty($props)) {
      foreach ($props as $field => $cvterm_name) {
        if (property_exists($hb_obj, $field)) {
          $value = $hb_obj->{$field};
          if ($value) {
            $rows []= array($cvterm_name, $value);
          }
        }
      }
    }
    if (!empty($rows)) {
      $table_vars = array(
        'header'      => array(array('data' => 'name', 'width' => 100), 'value'),
        'rows'        => $rows,
        'attributes'  => array(),
      );
      $element['prop']['table']['#markup'] = theme('table', $table_vars);
      $properties = drupal_render($element['prop']);
    }

    // Adds the action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }

    // Adds the comments.
    if ($hb_obj->comments) {
      $comments = '<textarea style="width:100%;" rows=2 READONLY>' . $hb_obj->comments . '</textarea>';
    }

    // Updates the location.
    if (($hb_obj->loc_start || $hb_obj->loc_start == '0') &&
        ($hb_obj->loc_stop || $hb_obj->loc_stop == '0')) {
      $chr    = $hb_obj->chromosome;
      $start  = $hb_obj->loc_start;
      $stop   = $hb_obj->loc_stop;
      $strand = bims_get_strand($hb_obj->strand, 'symbol');
      $location_row = "<textarea style='padding:3px;height:43px;width:100%;font-family: Monospace;font-size:10.5px;'>Chromosome : $chr\nPositions  : $start - $stop ($strand)</textarea>";
    }
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'width' => '100'), $name);
  if ($alias) {
    $rows[] = array('Alias', $alias);
  }
  $rows[] = array('Organism', "$genus $species");
  $rows[] = array('Trait', $trait);
  if ($location_row) {
    $rows[] = array('Location', $location_row);
  }
  if ($image_row) {
    $rows[] = array('Photos', $image_row);
  }
  if ($comments) {
    $rows[] = array('Comments', $comments);
  }
  if ($properties) {
    $rows[] = array('Properties', $properties);
  }
  if ($data_row) {
//    $rows[] = array('Genotyic Data', $data_row);
  }
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }
  $table_vars = array(
    'header'      => array(),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  $layout = theme('table', $table_vars);
  return $layout;
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_haplotype_block_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('hb_admin', $form)) {
    $layout .= "<div>" . drupal_render($form['hb_admin']) . "</div>";
  }

  // Adds "haplotype block".
  $layout .= "<div style='float:left;width:44%;'>" . drupal_render($form['hb_list']) . "</div>";

  // Adds 'haplotype block Details'.
  $layout .= "<div style='float:left;width:55%;margin-left:5px;'>" . drupal_render($form['hb_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
