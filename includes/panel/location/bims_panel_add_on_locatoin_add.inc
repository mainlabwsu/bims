<?php
/**
 * @file
*/
/**
 * Location add panel form.
*
* @param $form
* @param $form_state
*/
function bims_panel_add_on_location_add_form($form, &$form_state) {

  // Gets BIMS_USER.
  $bims_user = getBIMS_USER();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_location_add');
  $bims_panel->init($form);

  // Property for a location.
  $form['property'] = array(
    '#type'        => 'fieldset',
    '#title'       => "Add a new property",
    '#collapsed'   => TRUE,
    '#collapsible' => TRUE,
  );
  $form['property']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Property Name'),
    '#description'  => t("Please provide an name of a property."),
    '#attributes'   => array('style' => 'width:400px;'),
  );
  $form['property']['desc'] = array(
    '#type'         => 'textarea',
    '#title'        => t('Description'),
    '#description'  => t("Please provide a description of a property."),
    '#rows'       => 3,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['property']['add_property_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_property_btn',
    '#value'      => "Add property",
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_location_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-location-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Properties of a location.
  $form['location_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Add a new location',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['location_prop']['name'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Location Name'),
    '#description'  => t("Please provide a location name. Please use alphabets, dash and underscore"),
     '#attributes'  => array('style' => 'width:300px;'),
  );

  $form['location_prop']['type'] = array(
    '#type'         => 'select',
    '#title'        => t('Type'),
    '#options'      => MCL_DATA_VALID_TYPE::getOptions('location_type'),
    '#description'  => t("Please choose a type of the location"),
    '#attributes'   => array('style' => 'width:180px;'),
  );
  $form['location_prop']['latitude'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Latitude'),
    '#description'  => t("Please provide the latitude of the location in real number"),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $form['location_prop']['longitude'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Longitude'),
    '#description'  => t("Please provide the longitude of the location in real number"),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $form['location_prop']['altitude'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Altitude'),
    '#description'  => t("Please provide the altitude of the location in real number"),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $form['location_prop']['country'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Country'),
    '#description'  => t("Please provide the country of the location"),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $form['location_prop']['state'] = array(
    '#type'           => 'textfield',
    '#title'        => t('State'),
    '#description'  => t("Please provide the state of the location"),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $form['location_prop']['region'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Region'),
    '#description'  => t("Please provide the region of the location"),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $form['location_prop']['address'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Address'),
    '#description'  => t("Please provide the address of the location"),
    '#attributes'   => array('style' => 'width:300px;'),
  );
  $form['location_prop']['add_location_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'add_location_btn',
    '#value'      => 'Add a new location',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_location_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-location-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['location_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_location_add_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-location-add-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-location-add-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_location_add_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_add_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_add_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add" was clicked.
  if ($trigger_elem == 'add_location_btn') {

    // Gets BIMS_USER and BIMS_PROGRAM.
    $bims_user    = getBIMS_USER();
    $bims_program = $bims_user->getProgram();
    $program_id   = $bims_program->getProgramID();

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO table.
    $bc_site = new BIMS_CHADO_SITE($program_id);

    // Gets properties.
    $name       = trim($form_state['values']['location_prop']['name']);
    $latitude   = trim($form_state['values']['location_prop']['latitude']);
    $longitude  = trim($form_state['values']['location_prop']['longitude']);
    $altitude   = trim($form_state['values']['location_prop']['altitude']);

    // Validates the location name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('location_prop][name', "Invalid location name : $error");
      return;
    }

    // Checks for latitude, longitude and altitude.
    if ($latitude && !bims_is_real($latitude)) {
      form_set_error('location_prop][latitude', "latitude must be real");
      return;
    }
    if ($longitude && !bims_is_real($longitude)) {
      form_set_error('location_prop][longitude', "longitude must be real");
      return;
    }
    if ($altitude && !bims_is_real($altitude)) {
      form_set_error('location_prop][altitude', "altitude must be real");
      return;
    }

    // Checks the description for duplication.
    $description = $prefix . $name;
    $nd_geolocation = $bc_site->byTKey('site', array('description' => $description));
    if ($nd_geolocation) {
      form_set_error('location_prop][name', "The location ($name) has been taken. Please choose other name.");
    }
  }
}

/**
 * Adds a location.
 *
 * @param array $form_state
 *
 * @return integer
 */
function _bims_panel_add_on_location_add_location($form_state) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the prefix.
  $prefix = $bims_program->getPrefix();

  // Gets BIMS_CHADO table.
  $bc_site = new BIMS_CHADO_SITE($program_id);

  // Gets properties.
  $name           = trim($form_state['values']['location_prop']['name']);
  $type           = trim($form_state['values']['location_prop']['type']);
  $latitude       = trim($form_state['values']['location_prop']['latitude']);
  $longitude      = trim($form_state['values']['location_prop']['longitude']);
  $altitude       = trim($form_state['values']['location_prop']['altitude']);
  $geodetic_datum = trim($form_state['values']['location_prop']['geodetic_datum']);
  $country        = trim($form_state['values']['location_prop']['country']);
  $state          = trim($form_state['values']['location_prop']['state']);
  $region         = trim($form_state['values']['location_prop']['region']);
  $address        = trim($form_state['values']['location_prop']['address']);

  // Adds a location.
  $details = array(
    'description'     => $prefix . $name,
    'geodetic_datum'  => $geodetic_datum,
  );
  if ($latitude) {
    $details['latitude'] = $latitude;
  }
  if ($longitude) {
    $details['longitude'] = $longitude;
  }
  if ($altitude) {
    $details['altitude'] = $altitude;
  }
  $nd_geolocation_id = $bc_site->addSite(NULL, $details);
  if (!$nd_geolocation_id) {
    drupal_set_message("Error : Failed to add a new location to BIMS CHADO", 'error');
    return NULL;
  }
  $bc_site->setNdGeolocationID($nd_geolocation_id);

  // Adds properties.
  $bc_site->addProp(NULL, 'SITE_CV', 'type', $type);
  $bc_site->addProp(NULL, 'SITE_CV', 'country', $country);
  $bc_site->addProp(NULL, 'SITE_CV', 'state', $state);
  $bc_site->addProp(NULL, 'SITE_CV', 'region', $region);
  $bc_site->addProp(NULL, 'SITE_CV', 'address', $address);
  $bc_site->addProp(NULL, 'SITE_CV', 'country', $country);

  // Inserts into mview.
  $mview_location = new BIMS_MVIEW_LOCATION(array('node_id' => $program_id));
  $details = array(
    'nd_geolocation_id' => $nd_geolocation_id,
    'description'       => $prefix . $name,
    'name'              => $name,
    'type'              => $type,
    'latitude'          => $latitude,
    'longitude'         => $longitude,
    'altitude'          => $altitude,
    'country'           => $country,
    'state'             => $state,
    'region'            => $region,
    'address'           => $address,
  );
  if (!$mview_location->addLocation($details)) {
    drupal_set_message("Error : Failed to add a new location to MVIEW", 'error');
    return NULL;
  }
  return $nd_geolocation_id;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_add_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_location';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Add" was clicked.
  if ($trigger_elem == 'add_location_btn') {

    $transaction = db_transaction();
    try {

      // Adds a location.
      $nd_geolocation_id = _bims_panel_add_on_location_add_location($form_state);
      if (!$nd_geolocation_id) {
        throw new Exception("Error : Failed to add a new location");
      }
      $load_primary_panel = "$load_primary_panel/$nd_geolocation_id";
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage());
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_location_add')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}