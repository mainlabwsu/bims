<?php
/**
 * @file
 */
/**
 * Location view panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_view_form($form, &$form_state, $location_id = NULL) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_location_view');
  $bims_panel->init($form);

  // Adds the google map.
  $form['google_map'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Locations',
    '#attributes'   => array('style' => ''),
  );
  _bims_panel_add_on_location_view_form_map($form['google_map'], $bims_program, $location_id);
  $form['close_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'close_btn',
    '#value'      => 'Close',
    '#attributes' => array('style' => 'width: 120px;'),
    '#ajax'       => array(
      'callback' => 'bims_panel_add_on_location_view_form_ajax_callback',
      'wrapper'  => 'bims-panel-add-on-location-view-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-location-view-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_location_view_form_submit';
  return $form;
}

/**
 * Creates google map page.
 *
 * @param array $form
 * @param BIMS_PROGRAM $bims_program
 * @param integer $location_id
 */
function _bims_panel_add_on_location_view_form_map(&$form, BIMS_PROGRAM $bims_program, $location_id = NULL) {

  // Gets and sets the default google map variables.
  $bims_map       = new BIMS_MAP();
  $def_latitude   = '';
  $def_longitude  = '';
  $def_zoom       = $bims_map->getDefZoom();
  $def_icon       = $bims_map->getDefIcon();
  $map_img_path   = $bims_map->getMapImgPath();

  // Adds all locations as markers.
  $markers    = array();
  $rows       = array();
  $count      = 0;
  $locations  = $bims_program->getLocations(BIMS_OBJECT, TRUE);
  foreach ($locations as $location) {
    $marker_id  = $location->nd_geolocation_id;
    $label      = $location->name;
    $type       = $location->type;
    $latitude   = $location->latitude;
    $longitude  = $location->longitude;
    $notes      = '';

    // Sets the coordinates as default.
    if (++$count == 1) {
      $def_latitude   = $latitude;
      $def_longitude  = $longitude;
    }
    if ($marker_id == $location_id) {
      $def_latitude   = $latitude;
      $def_longitude  = $longitude;
      $def_zoom       = 10;
    }

    // Adds the site to the table.
    $rows []= array(
      $label,
      $type,
      $latitude,
      $longitude,
      "<a href='javascript:void(0);' onclick='bims.map_focus(\"bims_map_loc\", $marker_id, 10); return (false);'>Locate</a>"
    );

    // Adds the site to the map.
    $markers []= array(
      'marker_id' => $marker_id,
      'latitude'  => $latitude,
      'longitude' => $longitude,
      'icon'      => "$map_img_path/$def_icon",
      'content'   => "<b>$label</b><br/><em>$type</em><br/>$notes",
    );
  }

  // Adds the parameters for the map.
  $params = array(
    'latitude'  => $def_latitude,
    'longitude' => $def_longitude,
    'zoom'      => $def_zoom,
    'markers'   => $markers,
  );
  drupal_add_js(array('bims_map_params' => json_encode($params)), 'setting');

  // Adds google map.
  $form['map'] = array(
    '#markup' => '<div id="bims_map_loc" style="width:100%;max-height:400px;min-height:400px;"></div>',
  );

  // Lists all locations in the table.
  $header = array('Name', 'Type', 'Latitude', 'Longitude', 'Action');
  $table_vars = array(
    'header'      => $header,
    'rows'        => $rows,
    'attributes'  => array('style' => 'width:90%;'),
    'sticky'      => TRUE,
    'param'       => array(),
  );
  $form['user_table'] = array(
    '#markup' => bims_theme_table($table_vars),
  );
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_view_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_view_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_view_form_submit($form, &$form_state) {

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_location_view')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_location')));
}
