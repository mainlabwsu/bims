<?php
/**
 * @file
 */
/**
 * Location edit panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_edit_form($form, &$form_state, $location_id) {

  // Gets BIMS_USER.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Saves location_id.
  $form['location_id'] = array(
    '#type'   => 'value',
    '#value'  => $location_id,
  );

  // Initialzes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'add_on_location_edit');
  $bims_panel->init($form);

  // Gets the properties of the location.
  $mview = new BIMS_MVIEW_LOCATION(array('node_id' => $program_id));
  $mview_location = $mview->getLocation($location_id);

  // Properties of the location.
  $form['location_prop'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Edit the location',
    '#collapsed'   => FALSE,
    '#collapsible' => FALSE,
  );
  $form['location_prop']['name'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Location Name'),
    '#description'    => t("Please provide a location name. Please use alphabets, dash and underscore"),
    '#default_value'  => $mview_location->name,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['location_prop']['type'] = array(
    '#type'          => 'select',
    '#title'         => t('Type'),
    '#options'       => MCL_DATA_VALID_TYPE::getOptions('location_type'),
    '#default_value' => $mview_location->type,
    '#description'   => t("Please choose a type of the location"),
    '#attributes' => array('style' => 'width:200px;'),
  );
  $form['location_prop']['latitude'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Latitude'),
    '#description'    => t("Please provide the latitude of the location in real number"),
    '#default_value'  => $mview_location->latitude,
    '#attributes'     => array('style' => 'width:400px;'),
  );
  $form['location_prop']['longitude'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Longitude'),
    '#description'    => t("Please provide the longitude of the location in real number"),
    '#default_value'  => $mview_location->longitude,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['location_prop']['altitude'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Altitude'),
    '#description'    => t("Please provide the altitude of the location in real number"),
    '#default_value'  => $mview_location->altitude,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['location_prop']['country'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Country'),
    '#description'    => t("Please provide the country of the location"),
    '#default_value'  => $mview_location->country,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['location_prop']['state'] = array(
    '#type'           => 'textfield',
    '#title'          => t('State'),
    '#description'    => t("Please provide the state of the location"),
    '#default_value'  => $mview_location->state,
    '#attributes' => array('style' => 'width:200px;'),
  );
  $form['location_prop']['region'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Region'),
    '#description'    => t("Please provide the region of the location"),
    '#default_value'  => $mview_location->region,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['location_prop']['address'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Address'),
    '#description'    => t("Please provide the address of the location"),
    '#default_value'  => $mview_location->address,
    '#attributes' => array('style' => 'width:400px;'),
  );
  $form['location_prop']['edit_location_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'edit_location_btn',
    '#value'      => 'Update',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_location_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-location-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );
  $form['location_prop']['cancel_btn'] = array(
    '#type'       => 'submit',
    '#name'       => 'cancel_btn',
    '#value'      => 'Cancel',
    '#attributes' => array('style' => 'width:180px;'),
    '#ajax'   => array(
      'callback' => "bims_panel_add_on_location_edit_form_ajax_callback",
      'wrapper'  => 'bims-panel-add-on-location-edit-form',
      'effect'   => 'fade',
      'method'   => 'replace',
    ),
  );

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-add-on-location-edit-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_add_on_location_edit_form_submit';
  return $form;
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_edit_form_ajax_callback($form, &$form_state) {

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_edit_form_validate($form, &$form_state) {

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

   // If 'Edit' button was clicked.
  if ($trigger_elem == 'edit_location_btn') {

    // Gets BIMS_USER and BIMS_PROGRAM.
    $bims_user    = getBIMS_USER();
    $bims_program = $bims_user->getProgram();
    $program_id   = $bims_program->getProgramID();

    // Gets the prefix.
    $prefix = $bims_program->getPrefix();

    // Gets BIMS_CHADO table.
    $bc_site = new BIMS_CHADO_SITE($program_id);

    // Gets the properties of the location.
    $name         = trim($form_state['values']['location_prop']['name']);
    $location_id  = $form_state['values']['location_id'];

    // Validates the location name.
    $error = bims_validate_name($name);
    if ($error) {
      form_set_error('location_prop][name', "Invalid location name : $error");
      return;
    }

    // Gets the current location name.
    $cur_location = $bc_site->byTKey('site', array('nd_geolocation_id' => $location_id));
    $cur_loc_name = $cur_location->description;

    // Updates the location name.
    $loc_name = $prefix . $name;

    // Checks for a duplication if location names are different.
    if ($cur_loc_name != $loc_name ) {

      // Check the location name for duplication.
      $nd_geolocation = $bc_site->byTKey('site', array('description' => $loc_name));
      if ($nd_geolocation) {
        form_set_error('location_prop][name', "The name ($name) has been taken. Please choose other name.");
      }
    }

    // Checks for latitude, longitude and altitude.
    $latitude   = trim($form_state['values']['location_prop']['latitude']);
    $longitude  = trim($form_state['values']['location_prop']['longitude']);
    $altitude   = trim($form_state['values']['location_prop']['altitude']);
    if ($latitude && !bims_is_real($latitude)) {
      form_set_error('location_prop][latitude', "latitude must be real");
    }
    if ($longitude && !bims_is_real($longitude)) {
      form_set_error('location_prop][longitude', "longitude must be real");
    }
    if ($altitude && !bims_is_real($altitude)) {
      form_set_error('location_prop][altitude', "altitude must be real");
    }
  }
}

/**
 * Edits the location.
 *
 * @param array $form_state
 * @param integer $location_id
 *
 * @return boolean
 */
function _bims_panel_add_on_location_edit_form_edit_location($form_state, $location_id) {

  // Gets BIMS_USER and BIMS_PROGRAM.
  $bims_user    = getBIMS_USER();
  $bims_program = $bims_user->getProgram();
  $program_id   = $bims_program->getProgramID();

  // Gets the properties of the location.
  $name         = trim($form_state['values']['location_prop']['name']);
  $latitude     = trim($form_state['values']['location_prop']['latitude']);
  $longitude    = trim($form_state['values']['location_prop']['longitude']);
  $altitude     = trim($form_state['values']['location_prop']['altitude']);
  $type         = trim($form_state['values']['location_prop']['type']);
  $country      = trim($form_state['values']['location_prop']['country']);
  $state        = trim($form_state['values']['location_prop']['state']);
  $region       = trim($form_state['values']['location_prop']['region']);
  $address      = trim($form_state['values']['location_prop']['address']);
  $description  = $bims_program->getPrefix() . $name;

  // Sets properties for nd_geolocation.
  $details = array(
    'nd_geolocation_id' => $location_id,
    'name'              => $name,
    'description'       => $description,
    'latitude'          => $latitude,
    'longitude'         => $longitude,
    'altitude'          => $altitude,
    'type'              => $type,
    'country'           => $country,
    'state'             => $state,
    'region'            => $region,
    'address'           => $address,
  );

  // Updates BIMS_CHADO table.
  $bc_site = new BIMS_CHADO_SITE($program_id);
  if (!$bc_site->updateSite($details)) {
    drupal_set_message("Error : Failed to update the site", 'error');
    return FALSE;
  }

  // Updates the phenotype mview.
  $bims_mview = new BIMS_MVIEW_PHENOTYPE(array('node_id' => $program_id));
  db_update($bims_mview->getMView())
    ->fields(array('site_name' => $name))
    ->condition('nd_geolocation_id', $location_id, '=')
    ->execute();

  // Updates the location mview.
  $bims_mview = new BIMS_MVIEW_LOCATION(array('node_id' => $program_id));
  if (!$bims_mview->update($details)) {
    drupal_set_message("Error : Failed to update the mview location", 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_add_on_location_edit_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $load_primary_panel = 'bims_panel_main_location';

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // Gets location_id.
  $location_id = $form_state['values']['location_id'];

  // If the 'Edit' was clicked.
  if ($trigger_elem == 'edit_location_btn') {

    $transaction = db_transaction();
    try {

      // Edits the location.
      if (!_bims_panel_add_on_location_edit_form_edit_location($form_state, $location_id)) {
        throw new Exception("Error : Failed to edit the location");
      }
      $load_primary_panel .= "/$location_id";
    }
    catch (Exception $e) {
      $transaction->rollback();
      drupal_set_message($e->getMessage(), 'error');
      return FALSE;
    }
  }

  // Updates panels.
  bims_add_ajax_command(ajax_command_invoke(NULL, "remove_tab", array('bims_panel_add_on_trait_edit')));
  bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array($load_primary_panel)));
}