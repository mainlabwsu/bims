<?php
/**
 * Location panel form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_location_form($form, &$form_state, $nd_geolocation_id = NULL) {

  // Local variables for form properties.
  $min_height = 290;

  // Gets BIMS_USER, BIMS_PROGRAM and BIMS_MVIEW_LOCATION.
  $bims_user      = getBIMS_USER();
  $bims_program   = $bims_user->getProgram();
  $mview_location = new BIMS_MVIEW_LOCATION(array('node_id' => $bims_program->getProgramID()));

  // Gets the location object.
  $location_obj = NULL;
  if (isset($form_state['values']['location_list']['selection'])) {
    $nd_geolocation_id = $form_state['values']['location_list']['selection'];
  }
  if ($nd_geolocation_id != '') {
    $location_obj = $mview_location->getLocation($nd_geolocation_id);
  }

  // Creates a form.
  $form = array();
  $form['#tree'] = TRUE;

  // Initializes the panel.
  $bims_panel = new BIMS_PANEL($bims_user, 'main_location');
  $bims_panel->init($form);

  // Adds the admin menu.
  $form['location_admin'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => TRUE,
    '#collapsible'  => TRUE,
    '#title'        => 'Admin Menu for Location',
    '#attributes'   => array(),
  );
  $url = "bims/load_main_panel/bims_panel_add_on_location_add";
  $form['location_admin']['menu']['add_btn'] = array(
    '#type'       => 'button',
    '#value'      => 'Add',
    '#attributes' => array(
      'style'   => 'width:90px;',
      'onclick' => "bims.add_main_panel('bims_panel_add_on_location_add', 'Add Location', '$url'); return (false);",
    )
  );
  $form['location_admin']['menu']['#theme'] = 'bims_panel_main_location_form_admin_menu_table';
  bims_show_admin_menu($bims_user, $form['location_admin']);

  // Lists the all locations.
  $form['location_list'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Locations',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );

  // Location selection.
  $ret_values = _bims_panel_main_location_form_get_opt_location($bims_program, $mview_location);
  $options    = $ret_values['opt_location'];
  $imported   = $ret_values['imported'];
  $num_locations = sizeof($options);
  if (!$num_locations) {
    $form['location_list']['no_location'] = array(
      '#markup' => 'No location is associated with the current program.',
    );
  }
  else {
    $desc = "Please select a location from the list.";
    if ($imported) {
      $desc .= "<br />(* indicates imported locations)";
    }
    $form['location_list']['selection'] = array(
      '#type'           => 'select',
      '#options'        => $options,
      '#size'           => 12,
      '#prefix'         => "You have <b>$num_locations</b> locations",
      '#default_value'  => $nd_geolocation_id,
      '#description'    => $desc,
      '#attributes'     => array('style' => 'width:100%;'),
      '#ajax'           => array(
        'callback' => 'bims_panel_main_location_form_ajax_callback',
        'wrapper'  => 'bims-panel-main-location-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }

  // Location details.
  $form['location_details'] = array(
    '#type'         => 'fieldset',
    '#collapsed'    => FALSE,
    '#collapsible'  => FALSE,
    '#title'        => 'Location Details',
    '#attributes'   => array('style' => 'min-height:' . $min_height . 'px;'),
  );
  $form['location_details']['table']['location_obj'] = array(
    '#type'   => 'value',
    '#value'  => $location_obj,
  );
  $form['location_details']['table']['bims_program'] = array(
    '#type'   => 'value',
    '#value'  => $bims_program,
  );

  // Adds action buttons.
  if ($location_obj && bims_can_admin_action($bims_user)) {

    // Adds 'Edit' button.
    if (!$location_obj->chado) {
      $form['location_details']['table']['edit_btn'] = array(
        '#type'       => 'button',
        '#value'      => 'Edit',
        '#name'       => 'edit_btn',
        '#attributes' => array('style' => 'width:90px;'),
        '#ajax'       => array(
          'callback' => "bims_panel_main_location_form_ajax_callback",
          'wrapper'  => 'bims-panel-main-location-form',
          'effect'   => 'fade',
          'method'   => 'replace',
        ),
      );
    }

    // Adds 'Delete' button.
    $form['location_details']['table']['delete_btn'] = array(
      '#type'       => 'button',
      '#value'      => 'Delete',
      '#name'       => 'delete_btn',
      '#disabled'   => !$bims_user->canEdit(),
      '#attributes' => array(
        'style' => 'width:90px;',
        'class' => array('use-ajax', 'bims-confirm'),
      ),
      '#ajax'       => array(
        'callback' => "bims_panel_main_location_form_ajax_callback",
        'wrapper'  => 'bims-panel-main-location-form',
        'effect'   => 'fade',
        'method'   => 'replace',
      ),
    );
  }
  $form['location_details']['table']['#theme'] = 'bims_panel_main_location_form_location_table';

  // Sets properties of the form.
  $form['#prefix'] = '<div id="bims-panel-main-location-form">';
  $form['#suffix'] = '</div>';
  $form['#submit'][] = 'bims_panel_main_location_form_submit';
  return $form;
}

/**
 * Gets the options for the locations.
 *
 * @param BIMS_PROGRAM $bims_program
 * @param BIMS_MVIEW_LOCATION $mview_location
 *
 * @return array
 */
function _bims_panel_main_location_form_get_opt_location(BIMS_PROGRAM $bims_program, BIMS_MVIEW_LOCATION $mview_location) {

  // Gets the locations.
  $locations    = $mview_location->getLocations(BIMS_OBJECT);
  $opt_location = array();
  $imported     = FALSE;
  foreach ($locations as $location) {
    $star = '';
    if ($location->chado) {
      $star     = ' *';
      $imported = TRUE;
    }
    $opt_location[$location->nd_geolocation_id] = $location->name . $star;
  }
  return array('imported' => $imported, 'opt_location' => $opt_location);
}

/**
 * Ajax function which returns the form via ajax.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_location_form_ajax_callback($form, &$form_state) {

  // Gets BIMS_MVIEW_STOCK.
  $bims_user  = getBIMS_USER();
  $program_id = $bims_user->getProgramID();

  // Gets the trigger element.
  $trigger_elem = $form_state['triggering_element']['#name'];

  // If "Edit" or "Delete" was clicked.
  if ($trigger_elem == 'edit_btn' || $trigger_elem == 'delete_btn') {

    // Gets location_id.
    $nd_geolocation_id = $form_state['values']['location_list']['selection'];
    if ($nd_geolocation_id) {

      // If "Edit" was clicked.
      if ($trigger_elem == 'edit_btn') {
        $url = "bims/load_main_panel/bims_panel_add_on_location_edit/$nd_geolocation_id";
        bims_add_ajax_command(ajax_command_invoke(NULL, "add_main_panel", array('bims_panel_add_on_location_edit', 'Edit Location', $url)));
      }

      // If "Delete" was clicked.
      else if ($trigger_elem == 'delete_btn') {

        // Deletes the location.
        if (_bims_panel_main_location_form_delete_location($program_id, $nd_geolocation_id)) {
          bims_add_ajax_command(ajax_command_invoke(NULL, "load_primary_panel", array('bims_panel_main_location')));
        }
      }
    }
    else {
      drupal_set_message("Please select a location");
    }
  }

  // Handles ajax callback.
  $bims_panel = $form['bims_panel']['#value'];
  $bims_panel->handleAjaxCallback($form, $form_state);
  return $form;
}

/**
 * Deletes the location.
 *
 * @param integer $program_id
 * @param integer $nd_geolocation_id
 *
 * @return boolean
 */
function _bims_panel_main_location_form_delete_location($program_id, $nd_geolocation_id) {

  $transaction = db_transaction();
  try {

    // Deletes the location.
    $bc_site = new BIMS_CHADO_SITE($program_id);
    if (!$bc_site->deleteSites(array($nd_geolocation_id))) {
      throw new Exception("Error : Failed to delete the location");
    }
  }
  catch (Exception $e) {
    $transaction->rollback();
    drupal_set_message($e->getMessage(), 'error');
    return FALSE;
  }
  return TRUE;
}

/**
 * Validates the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_location_form_validate($form, &$form_state) {}

/**
 * Submits the form.
 *
 * @param array $form
 * @param array $form_state
 */
function bims_panel_main_location_form_submit($form, &$form_state) {}

/**
 * Theme function for the admin menu table.
 *
 * @param $variables
 */
function theme_bims_panel_main_location_form_admin_menu_table($variables) {
  $element = $variables['element'];

  // Creates the admin menu table.
  $rows = array();
  $rows[] = array(drupal_render($element['add_btn']), 'Add a new location / property.');
  $table_vars = array(
    'header'      => array(array('data' => 'Action', 'width' => '10%'), 'Description'),
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the location information table.
 *
 * @param $variables
 */
function theme_bims_panel_main_location_form_location_table($variables) {
  $element = $variables['element'];

  // Gets location object..
  $location_obj = $element['location_obj']['#value'];
  $bims_program = $element['bims_program']['#value'];

  // Initializes the properties.
  $name         = '--';
  $long_name    = '--';
  $latitude     = '--';
  $longitude    = '--';
  $altitude     = '--';
  $address_row  = '--';
  $region       = '--';
  $map          = '';
  $properties   = '';
  $actions      = '';

  // Shows the current available types for a location.
  $location_types =  MCL_DATA_VALID_TYPE::getOptions('location_type');
  $type = '<SELECT style="width:250px;">';
  foreach ((array)$location_types as $location_type) {
    $type .= "<OPTION>$location_type</OPTION>";
  }
  $type .= '</SELECT>';

  // Updates the properites.
  if ($location_obj) {
    $location_id  = $location_obj->nd_geolocation_id ;
    $na           = '<em>N/A</em>';
    $name         = $location_obj->name;
    $long_name    = ($location_obj->long_name)  ? $location_obj->long_name  : $na;
    $type         = ($location_obj->type)       ? $location_obj->type       : $na;
    $latitude     = ($location_obj->latitude)   ? $location_obj->latitude   : $na;
    $longitude    = ($location_obj->longitude)  ? $location_obj->longitude  : $na;
    $altitude     = ($location_obj->altitude)   ? $location_obj->altitude   : $na;
    $region       = ($location_obj->region)     ? $location_obj->region     : $na;

    // Updates the google map.
    if ($location_obj->latitude && $location_obj->longitude) {
      $map = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_location_view\",\"Locations\",\"bims/load_main_panel/bims_panel_add_on_location_view/$location_id\"); return (false);'>Locate&nbsp;<span class='bims_google_map'>&nbsp;</span></a>";
    }

    // Sets the address row.
    $address    = ($location_obj->address)    ? $location_obj->address    : '';
    $country    = ($location_obj->country)    ? $location_obj->country    : '';
    $state      = ($location_obj->state)      ? $location_obj->state      : '';
    $address_row = '';
    if ($address) {
      $address_row .= $address;
    }
    if ($state) {
      $address_row .= " $state";
    }
    if ($country) {
      $address_row .= " $country";
    }
    if (!$address_row) {
      $address_row = $na;
    }

    // Adds the properties.
    $rows = array();
    $bims_program = BIMS_PROGRAM::byID($location_obj->node_id);
    $props = $bims_program->getProperties('site', 'details', BIMS_FIELD);
    if (!empty($props)) {
      foreach ($props as $field => $cvterm_name) {
        if (property_exists($location_obj, $field)) {
          $value = $location_obj->{$field};
          if ($value) {
            $rows []= array($cvterm_name, $value);
          }
        }
      }
    }
    if (!empty($rows)) {
      $table_vars = array(
        'header'      => array(array('data' => 'name', 'width' => 100), 'value'),
        'rows'        => $rows,
        'attributes'  => array(),
      );
      $element['prop']['table']['#markup'] = theme('table', $table_vars);
      $properties = drupal_render($element['prop']);
    }

    // Adds action buttons.
    if (array_key_exists('delete_btn', $element)) {
      $actions .= drupal_render($element['delete_btn']);
    }
    if (array_key_exists('edit_btn', $element)) {
      $actions .= drupal_render($element['edit_btn']);
    }
  }
  else {

    // Adds all locations.
    if ($bims_program->hasCoordinates()) {
      $map = "<a href='javascript:void(0);' onclick='bims.add_main_panel(\"bims_panel_add_on_location_view\",\"Locations\",\"bims/load_main_panel/bims_panel_add_on_location_view\"); return (false);'>All locations&nbsp;<span class='bims_google_map'>&nbsp;</span></a>";
    }
  }

  // Creates the details table.
  $rows = array();
  $rows[] = array(array('data' => 'Name', 'width' => '100'), $name);
  $rows[] = array('Long Name',  $long_name);
  $rows[] = array('Type', $type);
  $rows[] = array('Latitude', $latitude);
  $rows[] = array('Longitude', $longitude);
  if ($map) {
    $rows[] = array('Google Map', $map);
  }
  $rows[] = array('Altitude', $altitude);
  $rows[] = array('Address', $address_row);
  $rows[] = array('Region', $region);
  if ($properties) {
    $rows[] = array('Properties', $properties);
  }
  if ($actions) {
    $rows[] = array('Actions', $actions);
  }

  $table_vars = array(
    'rows'        => $rows,
    'attributes'  => array(),
  );
  return theme('table', $table_vars);
}

/**
 * Theme function for the form.
 *
 * @param $variables
 */
function theme_bims_panel_main_location_form($variables) {
  $form = $variables['form'];

  // Adds the panel components.
  $bims_panel = $form['bims_panel']['#value'];
  $layout = $bims_panel->show($form);

  // Adds the admin menu.
  if (array_key_exists('location_admin', $form)) {
    $layout.= "<div style='width:100%;'>" . drupal_render($form['location_admin']) . '</div>';
  }

  // Adds the location list.
  $layout .= "<div style='float:left;width:40%;'>" . drupal_render($form['location_list']) . "</div>";

  // Adds the location info table.
  $layout .= "<div style='float:left;width:59%;margin-left:5px;'>" . drupal_render($form['location_details']) . "</div>";

  $layout .= drupal_render_children($form);
  return $layout;
}
