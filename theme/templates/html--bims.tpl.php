<?php
$mod_path = drupal_get_path('module', 'bims');
$lib_path = '/sites/all/libraries';
$js_path  = '/sites/all/modules/bims/theme/js';
?>

<html lang="<?php print $language->language?>">
  <head>
    <meta charset="utf-8">
    <title>BIMS</title>

    <!--  Add in stylesheets -->
    <?php print $styles ?>
    <link rel="stylesheet" href="<?php print $lib_path?>/dojo/dijit/themes/claro/claro.css" media="screen">
    <link rel="stylesheet" href="<?php print $lib_path?>/jstree/themes/default/style.min.css" media="screen">
    <link rel="stylesheet" href="<?php print $mod_path?>/theme/css/bims.css" media="screen">
    <link rel="stylesheet" href="<?php print $mod_path?>/theme/css/bims_ui.css" media="screen">
    <link rel="stylesheet" href="<?php print $mod_path?>/theme/css/bims_custom.css" media="screen">

    <!--  Add in javascript -->
    <?php print $scripts;
      if (preg_match("/localhost/", $_SERVER['HTTP_HOST'])) {
        $lib_path = '/drupal7/' . $lib_path;
      }
    ?>
    <script>
      var dojoConfig = {
        baseUrl: "<?php print drupal_get_path('module', 'bims')?>/theme/js/",
        packages: [
            { name: "dojo",  location: "<?php print $lib_path?>/dojo/dojo" },
            { name: "dijit", location: "<?php print $lib_path?>/dojo/dijit" },
            { name: "dojox", location: "<?php print $lib_path?>/dojo/dojox" },
            { name: "bims" }
        ],
        parseOnLoad: true,
      }
    </script>
    <script src="<?php print $js_path?>/Chart.min.js"></script>
    <script src="<?php print $lib_path?>/dojo/dojo/dojo.js"></script>
    <script src="<?php print $mod_path?>/theme/js/bims.js"></script>
    <script src="<?php print $mod_path?>/theme/js/autocomplete.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVQbwGqJ1OLvkc43L3avKwrR4tbLKXSDY"></script>
    <script src="<?php print $js_path?>/markerclusterer.js"></script>
    <script src="<?php print $js_path?>/jssor.slider.min.js"></script>

    <?php print $head ?>
  </head>
  <body class="claro">
    <?php print $page ?>
  </body>
</html>