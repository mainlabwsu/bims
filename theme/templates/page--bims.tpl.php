<?php
global $user;
$mod_path       = drupal_get_path('module', 'bims');
$log_link_url   = 'user/login';
$log_link_label = 'Log in';
if ($user->uid) {
  $log_link_url   = 'user/logout';
  $log_link_label = 'Log out';
}
?>
<div id="spinner"><img src="<?php print $mod_path ?>/theme/img/ajax_waiting.gif"></div>
<div id="loadingOverlay" class="loadingOverlay pageOverlay">
  <img src="<?php print $mod_path ?>/theme/img/ajax_waiting.gif">
</div>
<div id="appLayout" class="demoLayout" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'">
  <div id="headerRow" class="edgePanel" data-dojo-type="dijit/MenuBar" data-dojo-props="region: 'top'">
    <div id="bimsTopPanel" class="top-panel"></div>
    <!--
    <div style="float:left;margin-left:10px;padding:4px;background-color:white;width:500px;border:1px solid lightgrey;"> We are experiencing slowness on BIMS. We are working to resolve the issue. Thank you for your patience!</div>
    -->
    <div id="main-links">
      <a href="javascript:void(0);" title="Open configuration panel" onclick="bims.load_primary_panel('bims_panel_main_config'); return (false);">
        <img src="<?php print $mod_path ?>/theme/img/bims_config.png" height="20" width="20" style="margin-right:5px;">
      </a>
      <a href="javascript:void(0);" title="Open user account panel" onclick="bims.add_main_panel('bims_panel_add_on_user_account', 'User Account', 'bims/load_main_panel/bims_panel_add_on_user_account'); return (false);">
        <img src="<?php print $mod_path ?>/theme/img/user_account.png" height="20" width="20" style="margin-right:5px;">
      </a>
      <?php if (property_exists($user, 'name')) { ?>
        Hello <?php print $user->name;
      }
      else { ?>
        Hello Anonymous
      <?php } ?>
         <?php //print l("Account Page", 'user', array('attributes' => array('target' => '_blank')))?> |
         <?php print l("Site Home", '<front>', array('attributes' => array('target' => '_blank')))?> |
         <?php print l($log_link_label, $log_link_url)?>
       </div>
  </div>
  <div id="contentSection"
       class="centerPanel"
       data-dojo-type="dijit/layout/TabContainer"
       data-dojo-props="region: 'center', tabPosition: 'top'">
    <?php
      if ($messages): ?>
        <div id="console" class="clearfix" style="max-height:350px;overflow:scroll"><?php print $messages; ?></div><?php
      endif;
      //print drupal_render($page['content'])?>
<!--
    <div id="welcome-tab"
      data-dojo-type="dijit/layout/ContentPane"
      data-dojo-props='title:"Welcome!", iconClass:"dijitIconApplication", closable:true'>
      <div id="welcome-container">
        Welcom-container
      </div>
    </div>
-->
    <div id="first-tab"
         data-dojo-type="dijit/layout/ContentPane"
         data-dojo-props='title:"Program", iconClass:"dijitIconApplication"'>
    </div>
  </div>

  <!--  Gets sidebars -->
  <?php $sidebars = BIMS_SIDEBAR::getSidebars(); ?>
  <div id="leftCol" class="edgePanel" data-dojo-type="dijit/layout/AccordionContainer" data-dojo-props="region: 'left', splitter: true">
    <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>About BIMS</span>">
      <div id="bimsAboutSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        <?php print $sidebars['about']; ?>
      </div>
    </div>
    <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Help</span>">
      <div id="bimsHelpSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
         <?php print $sidebars['help']; ?>
      </div>
    </div>
    <?php
    // Archive.
    $tag = $sidebars['archive'];
    if ($tag) {
      $code = <<< HTML_TAG
        <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Archive</span>">
        <div id="bimsArchiveSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        $tag</div></div>
HTML_TAG;
      print $code;
    }

    // Manage Breeding.
    $tag = $sidebars['breeding'];
    if ($tag) {
      $code = <<< HTML_TAG
        <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Manage Breeding</span>" selected="true">
        <div id="bimsManageBreedingSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        $tag</div></div>
HTML_TAG;
      print $code;
    }

    // Data Import.
    $tag = $sidebars['data_import'];
    if ($tag) {
      $code = <<< HTML_TAG
        <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Data Import</span>">
        <div id="bimsDataImportSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        $tag</div></div>
HTML_TAG;
      print $code;
    }

    // Search.
    $tag = $sidebars['search'];
    if ($tag) {
      $code = <<< HTML_TAG
        <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Search</span>">
        <div id="bimsSearchSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        $tag</div></div>
HTML_TAG;
      print $code;
    }

    // Field Book Management.
    $tag = $sidebars['fieldbook'];
    if ($tag) {
      $code = <<< HTML_TAG
        <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Field Book Management</span>">
        <div id="bimsFieldBookSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        $tag</div></div>
HTML_TAG;
      print $code;
    }

    // Data Analysis.
    $tag = $sidebars['data_analysis'];
    if ($tag) {
      $code = <<< HTML_TAG
        <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Data Analysis</span>">
        <div id="bimsDataAnalysisSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        $tag</div></div>
HTML_TAG;
      print $code;
    }

    // Breeding Tools.
    $tag = $sidebars['breeding_tools'];
    if ($tag) {
      $code = <<< HTML_TAG
        <div data-dojo-type="dijit/layout/ContentPane" title="<span class='bims-sidebar-title'>Breeding Tools</span>">
        <div id="bimsBreedingToolsSidebar" data-dojo-type="dijit/layout/BorderContainer" data-dojo-props="design: 'headline'" style="overflow: auto">
        $tag</div></div>
HTML_TAG;
      print $code;
    }
    ?>
  </div>

  <!--
  <div id="footerRow"
       class="edgePanel"
       data-dojo-type="dijit/layout/ContentPane"
       data-dojo-props="region: 'bottom', splitter: true">
    <div id="bims-status-bar">Welcome to BIMS. Status notifications will appear here.</div>
  </div> -->
</div>