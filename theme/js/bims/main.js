/**
 * This file defines the BIMS DOJO module. It imports all the required
 * modules and manages the UI interface.
 */

define(
  // The module dependencies.
  [
    "dojo/dom",
    "dojo/on",
    "dojo/dom-style",
    "dijit/registry",
    "dijit/layout/BorderContainer",
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dojox/gfx",
    "dojox/gfx/filters",
    "dojo/_base/xhr",
    "dojo/json",
    "dojo/query",
    "dojo/NodeList-manipulate",
  ],

  // The callback function that gets run to initialize the module. The variables
  // passed to the function provide access to the modules required above, and
  // are in scope for all internal functions to this module.
  function(dom, on, domStyle, registry, BorderContainer, ContentPane,
      Button, gfx, filters, xhr, JSON, query){

    // A counter that indicates the number of AJAX calls waiting to be returned.
    var num_ajax_waiting = 0;
    var spinner_disabled;

    // The name of the panel that is currently loaded in the primary tab
    // if the load_primary_panel() is called with "current" then this
    // panel will be reloaded.
    var current_primary_panel;

    // An array of the open tabs.
    var open_tabs;

    // Google maps.
    var bims_map_loc       = null;
    var bims_map_loc_arr   = [];
    var bims_map_loc_info  = null;
    var bims_map_site      = null;
    var bims_map_site_arr  = [];
    var bims_map_site_info = null;

    // Image sliders.
    var bims_img_slider_accession = null;
    var bims_img_slider_sample    = null;
    var bims_img_slider_trait     = null;

    // Sets the timeout.
    var timeout = 8000;
    timeout = 15000;

    /*******************************************************/
    /* SPINNER */
    /*******************************************************/   
    /**
     * A function to manage the display of the spinner indicating data is
     * loading on the page.  Various AJAX calls can be made asynchronosly
     * so the set_spimmer_on() keeps a running total of the number of calls
     * to turn on the spinner.  When the set_spinner_off() function is called
     * it decrements the counter. When the counter reaches zero, the spinner
     * is hidden.
     */
    function set_spinner_on() {
      if (!spinner_disabled && num_ajax_waiting == 0) {
        domStyle.set("spinner", "visibility", "visible");
      }
      num_ajax_waiting++;
    }

    /**
     * Decrements the spinner counter and hides the spinner when it reaches zero.
     */
    function set_spinner_off() {
      num_ajax_waiting--;
      if (num_ajax_waiting < 0) {
        num_ajax_waiting = 0;
      }
      if (num_ajax_waiting == 0) {
        domStyle.set("spinner", "visibility", "hidden");
      }
    }

    /**
     * Disables display of the spinner.
     */
    function disable_spinner() {
      spinner_disabled = true;
    }

    /**
     * Enables display of the spinner.
     */
    function enable_spinner() {
      spinner_disabled = false;
    }

    /*******************************************************/
    /* TAB */
    /*******************************************************/
    /**
     * Adds a new tab to the tab list.
     */
    function add_tab(tab_id, title, icon, can_close, index, selected) {

      // Gets the content section with the tabs.
      var tc = registry.byId("contentSection");
      if (!tc) {
        return;
      }

      // Checks if this tab already exists.
      var tab = registry.byId(tab_id);

      if (tab == null) {
        // Adds a new tab to the contentSection.
        var tab = new ContentPane({
           id: tab_id,
           title: title,
           closable: can_close,
           iconClass: icon
        });
        tc.addChild(tab);
      }
      tab.set('title', title);

      // If this tab is to be selected then make it so.
      if (selected) {
        tc.selectChild(tab_id);
      }

      // Adds tab to the array excepts the following tab.
      if (tab_id != 'bims_panel_add_on_user_account') {
        open_tabs[open_tabs.length] = tab_id;
      }
      return tab;
    }

    /**
     * See if this tab already exists.
     */
    function tab_exists(tab_id){
      var tab = registry.byId(tab_id);
      return (tab) ? true : false;
    }

    /**
     * Set tab focused.
     */
    function focus_tab(tab_id){
      var tab = registry.byId(tab_id);
      if  (tab) {

        // Gets the content section with the tabs.
        var tc = registry.byId("contentSection");
        if (!tc) {
          return;
        }

        // Sets the focus.
        tc.selectChild(tab_id);
      }
    }

    /**
     * Removes all tabs except the primary tab.
     */
    function reset_tabs(){
      for (i = 0; i < open_tabs.length; ++i) {
        remove_tab(open_tabs[i]);
      }
    }

    /**
     * Function for removing a tab.
     */
    function remove_tab(tab_id) {
      var tc = registry.byId("contentSection");
      if (tc) {
        tab = registry.byId(tab_id)
        if (tab) {
          tc.removeChild(tab);
          registry.remove(tab_id);
        }
      }
    }

    /**
     * Makes a tab visible.
     */
    function show_tab(tab_id) {
      var tc = registry.byId("contentSection");
      if (tc) {
        tab = registry.byId(tab_id);
        if (tab) {
          tc.selectChild(tab_id);
        }
      }
    }

    /*******************************************************/
    /* INIT */
    /*******************************************************/
    /**
     * Intializes the bims application.
     */
    function init() {
      dojox.gfx.registry = {};
      open_tabs = [];
      num_ajax_waiting = 0;
      spinner_disabled = false;

      // Load page all components (top, sidebar and panel).
      load_top();
      load_sidebar();
      load_primary_panel('bims_panel_main_program');
    }

    /**
     * Intializes the image slider.
     * 
     * @param string id
     * @param string params_json
     */
    function init_image_slider(id, params_json) {
      (function ($) {	
        // Gets the parameters.
        //var params    = JSON.parse(params_json);
        //var p_zoom    = parseFloat(params.zoom);

        // Sets the options and initializes the slider.
        var options = {
          $DragOrientation: 1,
          $FillMode: 1,
          $AutoPlay: 0,
          $Loop: 0,
          $ThumbnailNavigatorOptions: {
            $Class: $JssorThumbnailNavigator$,
            $Loop: 0,
            $ChanceToShow: 2
          }
        };
        if (id == 'bims_img_slider_accession') {
          bims_img_slider_accession = new $JssorSlider$(id, options);
        }
        else if (id == 'bims_img_slider_sample') {
          bims_img_slider_sample = new $JssorSlider$(id, options);
        }
        else if (id == 'bims_img_slider_trait') {
          bims_img_slider_trait = new $JssorSlider$(id, options);
        }
      }(jQuery));
    }

    /**
     * Intializes the google map.
     * 
     * @param string id
     * @param string params_json
     */
    function init_map(id, params_json) {

      // Gets the parameters.
      var params    = JSON.parse(params_json);
      var p_zoom    = parseFloat(params.zoom);
      var p_lat     = parseFloat(params.latitude);
      var p_lng     = parseFloat(params.longitude);
      var p_markers = params.markers;

      // Initializes the google map.
      var bims_map = new google.maps.Map(document.getElementById(id), {
        zoom : p_zoom,
        center : new google.maps.LatLng(p_lat, p_lng),
        gestureHandling: 'cooperative'
      });

      // Adds the markers.
      markers = map_add_markers(id, bims_map, p_markers);

      // Adds marker clusters.
      var markerCluster = new MarkerClusterer(bims_map, markers, {imagePath: '/sites/all/modules/bims/theme/img/google_map/m'});
    
      // Update the bims_map.
      if (id == 'bims_map_site') {
        bims_map_site = bims_map;
      }
      else {
        bims_map_loc = bims_map;
      }
    }

    // Function for adding a marker to the page.
    function map_add_markers(id, bims_map, p_markers) {
      markers = [];
      p_markers.forEach((p_marker) => {

        // Create a new marker.
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(p_marker.latitude, p_marker.longitude),
          icon: p_marker.icon,
          id: p_marker.marker_id,
        });

        // Adds the listener.
        google.maps.event.addListener(marker, 'click', function() {

          if (id == 'bims_map_site') {
            // Closes the info window if opens.
            if (bims_map_site_info) {
               bims_map_site_info.close();
            }

            // Opens the info window.
            bims_map_site_info = new google.maps.InfoWindow({content:p_marker.content});
            bims_map_site_info.open(bims_map, marker);
          }
          else {
            // Closes the info window if opens.
            if (bims_map_loc_info) {
            	bims_map_loc_info.close();
            }

            // Opens the info window.
            bims_map_loc_info = new google.maps.InfoWindow({content:p_marker.content});
            bims_map_loc_info.open(bims_map, marker);
          }
        });

        // Adds the marker.
        marker.setMap(bims_map);
        markers.push(marker);

        // Adds the marker to the global array for later use.
        if (id == 'bims_map_site') {
          bims_map_site_arr[p_marker.marker_id] = marker;
        }
        else {
          bims_map_loc_arr[p_marker.marker_id] = marker;
        }
      });
      return markers;
    }

    // Focuses on the marker of the provided ID.
    function map_focus(site_name, marker_id, zoom) {

      // Sets bims_map.
      var bims_map = null;
      var marker   = null;
      if (site_name == 'bims_map_site') {
        bims_map = bims_map_site;
        marker   = bims_map_site_arr[marker_id];
      }
      else {
        bims_map = bims_map_loc;
        marker   = bims_map_loc_arr[marker_id];
      }

      // Sets the attribute of the marker.
      marker.setAnimation(google.maps.Animation.DROP);

      // Updates the zoom and the coordinates.
      if (zoom) {
        bims_map.setZoom(zoom);
      }
      bims_map.setCenter(marker.getPosition());
    }

    // Relocates the map by the coordinates.
    function map_relocate(site_name, latitude, longitude, zoom) {

      // Sets bims_map.
      var bims_map = (site_name == 'bims_map_site') ? bims_map_site : bims_map_loc;

      // Updates the zoom and the coordinates.
      if (zoom) {
        bims_map.setZoom(zoom);
      }
      bims_map.setCenter(new google.maps.LatLng(latitude, longitude));
    }

    /**
     * Intializes the chart.
     * 
     * @param string elem_id
     * @param string params_json
     */
    function init_chart(elem_id, params_json) {

      // Gets the parameters.
      var params    = JSON.parse(params_json);
      var p_type    = params.type;
      var p_data    = params.data;
      var p_options = params.options ? params.options : '{}';
      var p_table   = params.table ? params.table : '';

      // Parses the data.
      var p_data_arr = JSON.parse(p_data);
      var p_options_arr = JSON.parse(p_options);

      // Updates the callback function.
      if (p_options_arr.hasOwnProperty('scales')) {
        if (p_options_arr.scales.hasOwnProperty('yAxes')) {
          if (!(typeof(p_options_arr.scales.yAxes[0]) === 'undefined')) {
            if (p_options_arr.scales.yAxes[0].ticks.hasOwnProperty('callback')) {
              var cb = p_options_arr.scales.yAxes[0].ticks.callback;
              p_options_arr.scales.yAxes[0].ticks.callback = new Function('value', 'index', cb);
            }
          }
        }
        if (p_options_arr.scales.hasOwnProperty('xAxes')) {
          if (!(typeof(p_options_arr.scales.xAxes[0]) === 'undefined')) {
            if (p_options_arr.scales.xAxes[0].ticks.hasOwnProperty('callback')) {
              var cb = p_options_arr.scales.xAxes[0].ticks.callback;
              p_options_arr.scales.xAxes[0].ticks.callback = new Function('value', 'index', cb);
            }
          }
        }
      }

      // Draws the chart.
      var ctx = document.getElementById(elem_id).getContext('2d');
      var chart = new Chart(ctx, {

        // Sets the type of the chart.
        type : p_type,

        // Sets the data.
        data : p_data_arr,

        // Sets the options.
        options : p_options_arr
      });

      // Adds the table.
      if (p_table) {
        document.getElementById(elem_id + '_table').innerHTML = p_table;
      }
    }

    /*******************************************************/
    /* Operation for jsTree */
    /*******************************************************/
    /**
     * Operations on the trial tree.
     * 
     * - Download
     * 
     */
    function bims_tree_op(param) {
      set_spinner_on();
      xhr.get({
        url:  Drupal.settings["basePath"] + "bims/bims_ajax_tree_op/" + param,
        handleAs:"json",
        timeout: timeout,
        load: function(obj) {
          alert("Download file will be saved in your account.\n\nPlease go to your BIMS account page for the status.");
        },
        error: function() {
          alert("Fail to create the download file");
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /**
     * Updates the trial tree.
     */
    function update_trial(code, json) {
      var tree_json = JSON.stringify(json);

      // Updates the trial tree via ajax.
      set_spinner_on();
      xhr.post({
        url:  Drupal.settings["basePath"] + "bims/bims_ajax_update_trial",
        content: { code: code, tree_json: tree_json },
        handleAs:"json",
        timeout: timeout,
        load: function(obj) {
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /*******************************************************/
    /* SET Crop */
    /*******************************************************/
    function set_crop(crop_id) {
      xhr.get({
        url:  Drupal.settings["basePath"] + "bims/bims_ajax_set_crop/" + crop_id,
        handleAs:"json",
        timeout: timeout,
        load: function(obj) {
          load_primary_panel('bims_panel_main_program');
          load_top();
          load_sidebar();
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /*******************************************************/
    /* Make Ajax call */
    /*******************************************************/
    /**
     *  Make an AJAX call.
     */
    function ajax_call(url) {
      set_spinner_on();
      xhr.get({
        url:  Drupal.settings["basePath"] + url,
        handleAs:"json",
        timeout: timeout,
        load: function(obj) {
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /*******************************************************/
    /* AJAX Helpers */
    /*******************************************************/
    /**
     *  An AJAX call to update the primary panel.
     */
    function ajax_update_primary_panel(url) {
      xhr.get({
        url: Drupal.settings["basePath"] + url,
        handleAs:"json",
        timeout: timeout,
        load: function(json) {

          // Sets tab title.
          registry.byId("first-tab").set("title", json.title);

          // Replaces the contents of the center tab with the panel.
          var panel = dom.byId("first-tab");
          panel.innerHTML = json.content;

          // In the event the content has AJAX enabled content
          // we need to reset the Drupal.settings object and
          // attach behaviors.
          jQuery.extend(Drupal.settings, json.settings);
          Drupal.attachBehaviors();

          // Executes any javascript that might have been added.
          var scripts = panel.getElementsByTagName('script');
          for (var i = 0; i < scripts.length; i++) {
            eval(scripts[i].innerHTML);
          }
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /**
     *  An AJAX call to update the primary panel.
     */
    function ajax_update_main_panel(tab, title, url) {
      xhr.get({
        url: Drupal.settings["basePath"] + url,
        handleAs:"json",
        timeout: timeout,
        load: function(json) {

          // Sets tab title.
          tab.set("title", title);

          // Replaces the contents of the center tab with the panel.
          var panel = dom.byId(tab.get('id'));
          panel.innerHTML = json.content;

          // In the event the content has AJAX enabled content
          // we need to reset the Drupal.settings object and
          // attach behaviors.
          jQuery.extend(Drupal.settings, json.settings);
          Drupal.attachBehaviors();
          
          // Initializes google map.
          if (url.match(/who_is_using$/)) {
            init_map('bims_map_site', Drupal.settings.bims_map_params);
          }
          else if (url.match(/bims_panel_add_on_location_view/)) {
            init_map('bims_map_loc', Drupal.settings.bims_map_params);
          }

          // Initializes image slider.
          else if (url.match(/bims_panel_add_on_accession_image/)) {
            init_image_slider('bims_img_slider_accession', Drupal.settings.bims_img_slider_params);
          }

          // Execute any javascript that might have been added.
          var scripts = panel.getElementsByTagName('script');
          for (var i = 0; i < scripts.length; i++) {
            eval(scripts[i].innerHTML);
          }
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /**
     *  An AJAX call to update the panel.
     */
    function ajax_update_div_panel(dom_id, url) {
      set_spinner_on();
      xhr.get({
        url: Drupal.settings["basePath"] + url,
        handleAs:"json",
        timeout: timeout,
        load: function(json) {

          // Replaces the contents of the center tab with the panel.
          var panel = dom.byId(dom_id);
          panel.innerHTML = json.content;

          // In the event the content has AJAX enabled content
          // we need to reset the Drupal.settings object and
          // attach behaviors.
          jQuery.extend(Drupal.settings, json.settings);
          Drupal.attachBehaviors();

          // Executes any javascript that might have been added.
          var scripts = panel.getElementsByTagName('script');
          for (var i = 0; i < scripts.length; i++) {
            eval(scripts[i].innerHTML);
          }
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /*******************************************************/
    /* LOAD PRIMARY PANEL */
    /*******************************************************/
    /**
     *  An AJAX call to load the primary panel.
     */
    function load_primary_panel(panel_name) {
      set_spinner_on();

      // Updates panel name. If the panel_name is "current" then the user wants
      // to reload the currently loaded panel.
      if (panel_name == "current") {
        panel_name = current_primary_panel;
      }
      else {
        //if (current_primary_panel != panel_name) {}
        reset_tabs();
        current_primary_panel = panel_name;
      }

      // Sets the focus on the primary panel.
      focus_tab('first-tab');

      // Calls an ajax call.
      url = "bims/load_primary_panel/" + panel_name;
      ajax_update_primary_panel(url);
    }

    /*******************************************************/
    /* LOAD TOP PANEL */
    /*******************************************************/
    function load_top() {
      set_spinner_on();
      xhr.get({
        url: Drupal.settings["basePath"] + "bims/bims_ajax_load_top/bims_top_panel",
        handleAs:"json",
        timeout: timeout,
        load: function(json) {

          // Replaces the contents of the center tab with the panel.
          var panel = dom.byId('bimsTopPanel');
          panel.innerHTML = json.content;

          // In the event the content has AJAX enabled content
          // we need to reset the Drupal.settings object and
          // attach behaviors.
          jQuery.extend(Drupal.settings, json.settings);
          Drupal.attachBehaviors();

          // Executes any javascript that might have been added.
          var scripts = panel.getElementsByTagName('script');
          for (var i = 0; i < scripts.length; i++) {
            eval(scripts[i].innerHTML);
          }
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /*******************************************************/
    /* LOAD SIDEBARS */
    /*******************************************************/
    /**
     * Updates all the content on the left sidebar.
     */
    function load_sidebar() {
      set_spinner_on();
      xhr.get({
        url: Drupal.settings["basePath"] + "bims/bims_ajax_load_sidebar",
        handleAs:"json",
        timeout: timeout,
        load: function(json) {

          // Updates the contents of the sidebar.
          var pages = {
            'about'          : 'bimsAboutSidebar',
            'help'           : 'bimsHelpSidebar',
            'archive'        : 'bimsArchiveSidebar',
            'breeding'       : 'bimsManageBreedingSidebar',
            'data_import'    : 'bimsDataImportSidebar',
            'search'         : 'bimsSearchSidebar',
            'fieldbook'      : 'bimsFieldBookSidebar',
            'data_analysis'  : 'bimsDataAnalysisSidebar',
            'breeding_tools' : 'bimsBreedingToolsSidebar'
          }
          for (var key in pages) {
            content = json.content[key];
            if (content) {
              dom.byId(pages[key]).innerHTML = content;
            }
          }

          // In the event the content has AJAX enabled content
          // we need to reset the Drupal.settings object and
          // attach behaviors.
          jQuery.extend(Drupal.settings, json.settings);
          Drupal.attachBehaviors();
          set_spinner_off();
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /*******************************************************/
    /* ADD NEW MAIN PANEL */
    /*******************************************************/
    /**
     *  An AJAX call to show panel.
     */
    function add_main_panel(tab_id, title, url) {

      // Creates the tab and add the contents.
      var tab = add_tab(tab_id, title, 'dijitIconDocuments', true, 0, true)
      if (tab) {
        ajax_update_main_panel(tab, title, url);
      }
    }

    /*******************************************************/
    /* DOWNLOAD FILES */
    /*******************************************************/
    /**
     *  An AJAX call to download an excel file.
     */
    function download_excel(url) {
      window.open(url);
    }
    
    /**
     *  An AJAX call to download a file.
     */
    function download_file(url) {
      window.open(url);
    }
    
    /*******************************************************/
    /* LOAD CHART */
    /*******************************************************/
    /**
     *  An AJAX call to load the chart.
     */
    function load_chart(elem_id, params) {
      var url = 'bims/load_chart/' + params;
      xhr.get({
        url: Drupal.settings["basePath"] + url,
        handleAs:"json",
        timeout: timeout,
        load: function(json) {

          // In the event the content has AJAX enabled content
          // we need to reset the Drupal.settings object and
          // attach behaviors.
          jQuery.extend(Drupal.settings, json.settings);
          Drupal.attachBehaviors();

          // Initializes the chart.
          init_chart(elem_id, Drupal.settings.bims_chart_params);
        },
        error: function() {
          set_spinner_off();
          handle_ajax_error();
        }
      });
    }

    /*******************************************************/
    /* ERRORS */
    /*******************************************************/
    /**
     * Show an error messeage on the popup window.
     */
    function handle_ajax_error(msg) {
      //alert('bims failed to connect to the database and may not function ' +
      //    'properly. Please reload or try again later. If the problem ' +
      //    'persists, please contact the site administrator.');
    }

    /**
     *  Checks all checkboxes.
     */ 
    function bims_select_all(checkbox, fieldset_id) {
	
     (function ($) {
      $('#' + fieldset_id + ' input:checkbox').each(function() {
        if (checkbox.checked) {
          $(this).prop('checked', true);
        }
        else {
          $(this).prop('checked', false);
        }
       });
      }(jQuery));
    }

    /**
     * Return the list of public facing functions.
     */
    return {
      init: init,

      // Functions to enable and disable the spinner.
      disable_spinner : disable_spinner,
      enable_spinner  : enable_spinner,

      // Loads : panel, top, sidebar or add on panel.
      load_primary_panel : load_primary_panel,
      load_top           : load_top,
      load_sidebar       : load_sidebar,
      add_main_panel     : add_main_panel,
      download_excel     : download_excel,
      download_file      : download_file,

      // Functions for trial tree.
      update_trial : update_trial,
      bims_tree_op : bims_tree_op,

      // Javascript libraries.
      init_chart        : init_chart,
      init_map          : init_map,
      init_image_slider : init_image_slider,
      map_focus         : map_focus,
      map_relocate      : map_relocate,
      load_chart        : load_chart,

      // Sets crop.
      set_crop : set_crop,

      // Removes tab.
      remove_tab : remove_tab,

      // Checkbox.
      bims_select_all : bims_select_all,
    }
  }
);
