/**
 * The bims object is set within the bims_ui.js script. It is an
 * instance of the bims dojo module
 */
var bims;
/**
 * Commands executed using JQuery
 */
(function($) {

  // Create the dojo BIMS object and assign it to the bims variable above.
  require(["dojo/dom", "dojo/dom-style", "dojo/ready", "bims", "dojo/domReady!"], 
    function (dom, domStyle, ready, arg_bims) {

      // parse the DOM to convert dijit elements
      // parser.parse();

      // once all dependencies have copmleted exeucte the following
      ready(function(){
        bims = arg_bims;
        bims.init();
        domStyle.set("spinner", "visibility", "hidden");
        $("#loadingOverlay").fadeOut(1200);
      });
    }
  );

  // Deal with functionality that integrates with Drupal.
  Drupal.behaviors.bims = {
    attach: function (context, settings) {

      // Adds DND for photo files.
      bims_add_dnd_photo();

      // Adds the tree.
//      bims_add_tree();

      // Global variables.
      var update_tree_flag = false;
      var types_TT = {
        "PROGRAM"        : { "icon" : "bims-program",        "valid_children" : ["SUB_PROGRAM", "TRIAL"]},
        "SUB_PROGRAM"    : { "icon" : "bims-sub-program",    "valid_children" : ["SUB_PROGRAM", "TRIAL"]},
        "TRIAL"          : { "icon" : "bims-trial",          "valid_children" : []},
        "TRIAL_RO"       : { "icon" : "bims-trial-ro",       "valid_children" : []},
        "default"        : { "icon" : "jstree-file",         "valid_children" : []},
      };

      var types_BLT = {
        "PROGRAM"  : { "icon" : "bims-program", "valid_children" : ["STOCK", "PROGRAM"]},
        "STOCK"    : { "icon" : "bims-stock",   "valid_children" : ["STOCK", "TRIAL"]},
        "TRIAL"    : { "icon" : "bims-trial",   "valid_children" : []},
        "default"  : { "icon" : "jstree-file",  "valid_children" : []},
      };

      var types_CT = {
        "PROGRAM"  : { "icon" : "bims-program", "valid_children" : ["CROSS"]},
        "CROSS"    : { "icon" : "bims-cross",   "valid_children" : []},
        "default"  : { "icon" : "jstree-file",  "valid_children" : []},
      };
      
      /*******************************************
       * jsTree : Cross Tree (CT).
       *******************************************/
      $('#BIMS-CT').jstree({
          
        // "plugins".
        "plugins" : [ "contextmenu", "types"],

        // "core".
        "core" : {
          "multiple" : false,
          "data" : {
            "url" : function() { return "bims/bims_ajax_load_cross/" + $("#BIMS-CT").attr("data-code"); },
            "dataType" : "json",
            "beforeSend" : function() { update_tree_flag = false; },
            "success" : function() { update_tree_flag = true; },
          },
          "check_callback" : function(o, n, p, i, m) {
            return true;
          },
          "error" : function (err) {
            /*if(err.id === 'unique_04') {console.log(err.data.text);}*/
          },
          "themes" : {
            "responsive" : false,
            "variant" : "small",
            "stripes" : true
          },
        },

        // "types".
        "types" : types_CT,

        // "contextmenu".
        "contextmenu" : {
          "items": function(node) {
            var items = _get_menu_items_CT(node);
            return _filter_menu_items_CT(items, node.type);
          }
        },
      });

      // Returns the menu items for cross tree.
      function _get_menu_items_CT(node) {
        var items = {};
        return items;
      }

      // Filters out menu items.
      function _filter_menu_items_CT(items, node_type) {
//        if (node_type == 'STOCK' || node_type == 'PROGRAM') {
          delete items;
//        }
        return items;
      }

      /*******************************************
       * jsTree : Breeding Line Tree (BLT).
       *******************************************/
      $('#BIMS-BLT').jstree({

        // "plugins".
        "plugins" : [ "contextmenu", "types"],

        // "core".
        "core" : {
          "multiple" : false,
          "data" : {
            "url" : function() { return "bims/bims_ajax_load_breed_line/" + $("#BIMS-BLT").attr("data-code"); },
            "dataType" : "json",
            "beforeSend" : function() { update_tree_flag = false; },
            "success" : function() { update_tree_flag = true; },
          },
          "check_callback" : function(o, n, p, i, m) {
            return true;
          },
          "error" : function (err) {
            /*if(err.id === 'unique_04') {console.log(err.data.text);}*/
          },
          "themes" : {
            "responsive" : false,
            "variant" : "small",
            "stripes" : true
          },
        },

        // "types".
        "types" : types_BLT,

        // "contextmenu".
        "contextmenu" : {
          "items": function(node) {
            var items = _get_menu_items_BLT(node);
            return _filter_menu_items_BLT(items, node.type);
          }
        },
      });

      // Returns the menu items for breeding line tree.
      function _get_menu_items_BLT(node) {
        var items = {};
        return items;
      }

      // Filters out menu items.
      function _filter_menu_items_BLT(items, node_type) {
//        if (node_type == 'STOCK' || node_type == 'PROGRAM') {
          delete items;
//        }
        return items;
      }

      // Adds onclick function.
      $('#BIMS-BLT').on("select_node.jstree", function (e, data) {
        if (data.node.type == 'TRIAL') {
          var matched = /-(\d+)$/.exec(data.node.id);
          var node_id = matched[1];
          bims.add_main_panel(
            'bims_panel_add_on_trial_stats',
            'Trial Statistics',
            'bims/load_main_panel/bims_panel_add_on_trial_stats/' + node_id
          );
        }
      });

      /*******************************************
       * jsTree : Trial Tree (TT) with RO permission.
       *******************************************/
      $('#BIMS-TREE-RO').jstree(_get_ro_tree_prop('BIMS-TREE-RO'));
      $('#BIMS-TREE-ROT').jstree(_get_ro_tree_prop('BIMS-TREE-ROT'));

      // Creates a read-only tree.
      function _get_ro_tree_prop(id) {
        var prop = {

          // "plugins".
          "plugins" : ["types"],

          // "core".
          "core" : {
            "data" : {
              "url" : function() { return "bims/bims_ajax_load_trial/" + $('#' + id).attr("data-code"); },
              "dataType" : "json"
            },
            "multiple" : function() { return false; },
            "themes" : {
              "responsive" : false,
              "variant" : "small",
              "stripes" : true
            },
          },

          // "types".
          "types" : types_TT,
        };
        return prop;
      }

      /*******************************************
       * Adds Trial Tree (TT) with RW permission.
       *******************************************/
      $('#BIMS-TREE-RW').jstree({

        // "plugins".
        "plugins" : [ "contextmenu", "dnd", "unique", "types"],

        // "core".
        "core" : {
          "multiple" : false,
          "data" : {
            "url" : function() { return "bims/bims_ajax_load_trial/" + $("#BIMS-TREE-RW").attr("data-code"); },
            "dataType" : "json",
            "beforeSend" : function() { update_tree_flag = false; },
            "success" : function() { update_tree_flag = true; },
          },
          "check_callback" : function(o, n, p, i, m) {
            return true;
          },
          "error" : function (err) {
            /*if(err.id === 'unique_04') {console.log(err.data.text);}*/
          },
          "themes" : {
            "responsive" : false,
            "variant" : "small",
            "stripes" : true
          },
        },

        // "dnd".
        "dnd" : {
          "is_draggable" : function(node) {
            node_type = node[0].type;
            if (node_type.match(/_RO$/)) {
              return false;
            }
            return true;
          }
        },

        // "types".
        "types" : types_TT,

        // "unique".
        "unique" : {
          "duplicate" : function (name, counter) {
            return name + ' ' + counter;
          }
        },

        // "contextmenu".
        "contextmenu" : {
          "items": function(node) {
            var items = _get_menu_items_TT(node);
            return _filter_menu_items_TT(items, node.type);
          }
        },
      });

      // Returns the type of node.
      function get_node_type(node) {
        var type = 'Trial';
        if (node.type == 'PROGRAM') {
          type = 'Program';
        }
        else if (node.type == 'SUB_PROGRAM') {
          type = 'Sub-Program';
        }
        else if (node.type == 'TRIAL') {
          type = 'Trial';
        }
        return type;
      }

      // Returns the menu items for trial tree.
      function _get_menu_items_TT(node) {
        var items = {

          // Menu : "Create".
          "create": {
            "separator_before" : false,
            "separator_after" : false,
            "label" : "Create",
            "submenu" : {

              // BRANCH (SUB_PROGRAM).
              "branch" : {
                "separator_after" : false,
                "label" : function () {
                  return 'Sub-Program'; 
                },
                "action" : function (data) {
                  var inst = $.jstree.reference(data.reference);
                  var obj  = inst.get_node(data.reference);
                  var node_type = '';
                  var text = '';
                  if (obj.type == 'PROGRAM' || obj.type == 'SUB_PROGRAM') {
                    node_type = 'SUB_PROGRAM';
                    text = 'New sub-program';
                  }
                  else {
                    node_type = 'TRIAL';
                    text = 'New trial';
                  }
                  inst.create_node(obj, { type : node_type, text : text }, "last", function (new_node) {
                    setTimeout(function () { inst.edit(new_node); }, 0);
                  });
                }
              },

              // TRIAL.
              "trial" : {
                "separator_after" : false,
                "label" : "Trial",
                "action" : function (data) {
                  var inst = $.jstree.reference(data.reference);
                  var obj = inst.get_node(data.reference);
                  inst.create_node(obj, { type : "TRIAL", text : "New trial" }, "last", function (new_node) {
                    setTimeout(function () { inst.edit(new_node); }, 0);
                  });
                }
              }
            },
          },

          // Menu : "View".
          "view": {
            "separator_before" : false,
            "separator_after" : false,
            "label" : "View",
            "action": function (data) {
              bims.add_main_panel(
                'bims_panel_add_on_trial_stats',
                'Trial Statistics',
                'bims/load_main_panel/bims_panel_add_on_trial_stats/' + node.id
              );
            }
          },

          // Menu : "Rename".
          "rename": {
            "separator_before" : false,
            "separator_after" : false,
            "label" : "Rename",
            "action" : function (data) {
              var inst = $.jstree.reference(data.reference);
              inst.edit(node);
            }
          },

          // Menu : "Edit".
          "edit": {
            "separator_before": false,
            "separator_after": false,
            "label": "Edit",
            "action": function (data) {
              var inst = $.jstree.reference(data.reference);
              var obj  = inst.get_node(data.reference);
              if (obj.type == 'PROGRAM') {
                bims.add_main_panel(
                  'bims_panel_add_on_program_edit',
                  'Edit Program',
                  'bims/load_main_panel/bims_panel_add_on_program_edit/' + node.id
                );
              }
              else if (obj.type == 'TRIAL') {
                bims.add_main_panel(
                  'bims_panel_add_on_trial_edit',
                  'Edit Trial',
                  'bims/load_main_panel/bims_panel_add_on_trial_edit/' + node.id
                );
              }
            }
          },

          // Menu : "Remove".
          "remove": {
            "separator_before" : false,
            "separator_after" : false,
            "label" : "Remove",
            "action" : function (data) {
              var inst = $.jstree.reference(data.reference);
              inst.delete_node(node);
            }
          },
        };
        return items;
      }

      // Filters out menu items.
      function _filter_menu_items_TT(items, node_type) {

        // Removes "Create" from TRIAL.
        delete items.create.submenu.trial;
        
 
        // Filters out by node type.
        if (node_type.match(/_RO$/)) {
          delete items.create;
          delete items.edit;
          delete items.remove;
        }
        else if (node_type.match(/^PROGRAM$/)) {
          delete items.rename;
          delete items.view;
          delete items.remove;
        }
        else if (node_type.match(/^SUB_PROGRAM$/)) {
          delete items.view;
          delete items.edit;
          delete items.remove;
        }
        else if (node_type == 'TRIAL') {
          delete items.view;
          delete items.edit;
          delete items.rename;
          delete items.create;
          delete items.remove;
        }
        return items;
      }

      // Adds onclick function for the trial tree.
      $('#BIMS-TREE-RW').on("select_node.jstree", function (e, data) {
        if (data.node.type == 'TRIAL') {
          var matched = /(\d+)$/.exec(data.node.id);
          var node_id = matched[1];
          bims.add_main_panel(
            'bims_panel_add_on_phenotype_trial',
            'Trial Phenotype',
            'bims/load_main_panel/bims_panel_add_on_phenotype_trial/' + node_id
          );
        }
      });

      // Adds onclick function for the cross tree.
      $('#BIMS-CT').on("select_node.jstree", function (e, data) {
        if (data.node.type == 'CROSS') {
          var matched = /(\d+)$/.exec(data.node.id);
          var nd_experiment_id = matched[1];
          bims.add_main_panel(
            'bims_panel_add_on_phenotype_cross',
            'Cross Phenotype',
            'bims/load_main_panel/bims_panel_add_on_phenotype_cross/' + nd_experiment_id
          );
        }
      });

      // Adds event-handler : 'Download' button.
      $('#bims-panel-main-trial-download-btn').on("mousedown", function() {
        var tree = $('#BIMS-TREE-RO').jstree(true);

        // Gets all selected nodes and append to param.
        var selected = tree.get_selected();
        if (!selected.length) {
          alert("Please select a trial");
        }
        else {
          var nodes = '';
          selected.forEach(function(node){
            nodes += node + ':';
          });

          // Calls AJAX to download.
          bims.bims_tree_op('download-' + nodes);
        }
      });

      // Event-handler - 'Rest' button : clears the selections.
      $('#bims-panel-main-trial-reset-btn').on("mousedown", function() {
        var instance = $('#BIMS-TREE-RO').jstree(true);
        instance.deselect_all();
      });

      // Event-handler - 'Update' button on the tree panel
      $('#bims-panel-update-trial-btn').on("mousedown", function(e) {
        var code = $("#BIMS-TREE-RW").attr("data-code");
        var tree = $('#BIMS-TREE-RW').jstree(true);
        var json = tree.get_json('#', { 'flat': false });

        // Call AJAX to update the trial tree.
        bims.update_trial(code, json);
      });

      // Adds a confirmation dialog before perform an ajax action.
      $('.bims-confirm').each(function(e) {
        Drupal.ajax[this.id].beforeSend = function (xmlhttprequest, options) {
          if(confirm('Please click "OK" to confirm this action?')){
            return true;
          }
          if (typeof(xmlhttprequest.readyState) != 'undefined') {
            xmlhttprequest.abort(e);
          }
          return false;
        };
      });

      // Adds click event for Opening / Closing the hidden div.
      $('.hide-div-icon').click(function(e) {
        var tmp = document.getElementById(e.target.id.substring(5));
        var div = $(tmp);
        if (div.is(":visible")) {
          e.target.className = 'hide-div-icon-open';
          div.hide();
        }
        else {
          e.target.className = 'hide-div-icon-close';
          div.show();
        }
      });

      // Hides all the popup by default.
      $(".popup-div").hide();

      // When the popup link is clicked, show the contents in the box.
      $(".popup-link").click(function(e) {
        $(".popup-div").hide();
        $("#popup-div-" + e.target.id).show();
      });

      // When the [Close] button is clicked, close the popup.
      $(".popup-close").click(function(e) {
        $(".popup-div").hide();
      });
    },
  }; // end Drupal.behaviors.bims

  // In order to execute functions after a Drupal AJAX callback we must create
  // new JQuery functions that serve as wrappers.  These functions
  // will be specified for execution in the Drupal callback functions.

  // Wrapper for updating the top panel.
  $.fn.load_top = function() {
    bims.load_top();
  };

  // Wrapper for updating the sidebar.
  $.fn.load_sidebar = function(sidebar_name) {
    bims.load_sidebar(sidebar_name);
  };

  // Wrapper for updating the primary panel.
  $.fn.load_primary_panel = function(panel_name) {
    bims.load_primary_panel(panel_name);
  };

  // Wrapper for adding main panel.
  $.fn.add_main_panel = function(tab_id, title, url) {
    bims.add_main_panel(tab_id, title, url);
  };

  // Wrapper for adding main panel.
  $.fn.download_excel = function(url) {
    bims.download_excel(url);
  };

  // Wrapper for removing tab.
  $.fn.remove_tab = function(tab_id) {
    bims.remove_tab(tab_id);
  };

  // Wrapper for download a file.
  $.fn.download_file = function(file_id) {
    bims.download_file(file_id);
  };

  // Wrapper for download a file.
  $.fn.load_chart = function(elem_id, params) {
    bims.load_chart(elem_id, params);
  };

  // Wrapper for download a file.
  $.fn.bims_select_all = function(checkbox_id) {
    bims_select_all(checkbox_id);
  };

  /*
  // Add a timer to refresh bims after 30 seconds.
  setInterval(function() {
    if (bims) {
      bims.disable_spinner();
//      bims.load_sidebar();
      bims.enable_spinner();
    }
  }, 30000);
  */

  /**
   * Attach the cross window messaging listener so that
   * apps in iFrames can use the bims object.
   */
  if (window.addEventListener){
    addEventListener("message", cwm_listener, false)
  }
  else {
    attachEvent("onmessage", cwm_listener)
  }

  /**
   * Prototypes.
   */
  // Uppercase at the first character.
  String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  }
  
  // Trim a string.
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
  }
  
  /**
   * Cross window messaging listener.
   *
   * @param event
   */
  function cwm_listener(event){
    var data = $.parseJSON(event.data);
    var func = data.function;
    var args = data.args;
    var call = "bims." + func + "( ";
    for (index = 0; index < args.length; ++index) {
      call += "'" + args[index] + "',";
    }
    // Remove tailing comma.
    call = call.substring(0, call.length - 1);
    call += ')';
    eval(call);
  }

  /**
   * Adds drag and drop for photo files.
   */
  function bims_add_dnd_photo() {
	  /*
	  var dropzone = document.getElementById('dropzone');
	  dropzone.ondrop = function(e) {
	    var length = e.dataTransfer.items.length;
	    for (var i = 0; i < length; i++) {
	      var entry = e.dataTransfer.items[i].webkitGetAsEntry();
	      if (entry.isFile) {
	    	  console.log('a');
	         // do whatever you want
	      } else if (entry.isDirectory) {
	         // do whatever you want
	          console.log('b');
	      }
	    }
	  };
	  */
   /*
    // Gets DnD element.
    var dnd = $("#bims_dnd_photo");
    
   
    dnd.on('dragenter', function (e) {
      e.stopPropagation();
      e.preventDefault();
      $(this).css('border', '2px solid #0B85A1');
    });
    dnd.on('dragover', function (e) {
      e.stopPropagation();
      e.preventDefault();
    });
    dnd.on('drop', function (e) {
      $(this).css('border', '2px dotted #0B85A1');
      e.preventDefault();
      var files = e.originalEvent.dataTransfer.files;

      // Gets filenames and checks them.
      bims_dnd_photo_check_files(files, dnd);
    });

    $(document).on('dragenter', function (e) {
      e.stopPropagation();
      e.preventDefault();
    });
    $(document).on('dragover', function (e) {
      e.stopPropagation();
      e.preventDefault();
      $(this).css('border', '2px dotted #0B85A1');
    });
    $(document).on('drop', function (e) {
      e.stopPropagation();
      e.preventDefault();
    });*/
  }
  
  /**
   * Checks the uploaded photo files.
   * 
   * @param files
   * @param obj
   */
  function bims_dnd_photo_check_files(files, obj) {
    for (var i = 0; i < files.length; i++) {
      //var fd = new FormData();
      //fd.append('file', files[i]);
   
      // var status = new createStatusbar(obj); //Using this we can set progress.
      //status.setFileNameSize(files[i].name,files[i].size);
      //sendFileToServer(fd,status);
      console.log(files[i])
    }
    console.log(obj);
    
    
    
  }
})(jQuery);
